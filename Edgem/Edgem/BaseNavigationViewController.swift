//
//  BaseNavigationViewController.swift
//  Edgem
//
//  Created by Hipster on 27/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit

class BaseNavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.isTranslucent = false
        self.navigationBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Status Bar Methods
    
    //    override var preferredStatusBarStyle: UIStatusBarStyle {
    //        return (self.topViewController?.preferredStatusBarStyle)!
    //    }
    
    override var prefersStatusBarHidden: Bool {
        return (self.topViewController?.prefersStatusBarHidden)!
    }
    
    //MARK: - Orientation
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
}


extension BaseNavigationViewController: UIGestureRecognizerDelegate {
    //MARK: - UINavigationControllerDelegate Methods
    
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        //self.interactivePopGestureRecognizer?.isEnabled = true
         self.interactivePopGestureRecognizer?.isEnabled = false
        super.pushViewController(viewController, animated: animated)
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        //Write any custom functionality if required in future
    }
    
}
