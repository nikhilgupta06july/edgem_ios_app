//
//  AppStrings.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 13/08/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation


// ChatViewController
// =======================

let student_made_offer = "You have made an offer, please wait for tutor's response within 24 hours.".localized

let tutor_made_offer = "You have made an offer, please wait for student's response within 24 hours.".localized

let student_got_offer = "Tutor has made an offer and is awaiting your response. Please respond within 24 hours otherwise offer will be automatically cancelled. ".localized

let tutor_got_offer = "Student has made an offer and is awaiting your response. Please respond within 24 hours otherwise offer will be automatically cancelled. ".localized


let tutor_accept_offer_show_in_stu_scr = "Tutor has accepted your offer. Please make payment within 24 hours for tuition to commence otherwise it will be automatically cancelled.".localized

let tutor_accept_offer_show_in_tut_scr = "You have accepted the student's offer. Student has 24 hours to make payment otherwise it will be automatically cancelled. You will be notified once payment is successful."

let student_accept_offer_shown_in_stu_scr = "You have accepted the tutor's offer. Please make payment within 24 hours for tuition to commence otherwise it will be automatically cancelled."

let student_accept_offer_shown_in_tut_scr = "Student has accepted your offer. Student has 24 hours to make payment otherwise it will be automatically cancelled. You will be notified once payment is successful."

let tutor_counter_ofr_tut_scr = "You have made a counter offer, please wait for student's response."

let tutor_counter_ofr_stu_scr = "Tutor has made a counter offer and is awaiting your response."

let stu_counter_ofr_stu_scr = "You have made a counter offer, please wait for tutor's response."

let stu_counter_ofr_tut_scr = "Student has made a counter offer and is awaiting your response."

let payment_has_done_tut_scr = "Student has made payment and you can start chatting. Click on the calendar icon at the top right corner to arrange your first lesson with the student. "

let payment_has_done_stu_scr = "Payment is successful and you can start chatting. Click on the calendar icon at the top right corner to arrange your first lesson with the tutor."

let payment_not_done_stu_scr = "No payment was made within 24 hours and offer has been automatically cancelled. If you wish to proceed with the booking, please make an offer again. "

let payment_not_done_tut_scr = "No payment was made within 24 hours and offer has been automatically cancelled."

let offer_not_accept_24hr_stu_scr = "Offer was not accepted within 24 hours and has been automatically cancelled. If you wish to proceed with the booking, please make a new offer."

let offer_not_accept_24hr_tut_scr = "Offer was not accepted within 24 hours and has been automatically cancelled. If you wish to proceed with the booking, please make a new offer."

let you_have_reject_this_offer = "You have rejected this offer."

let tut_reject_this_offer = "Your offer has been rejected by the tutor. Please make a new offer. "

let stu_reject_this_offer = "Your offer has been rejected by the student. Please make a new offer. "

let who_sch_who_scr_class_prefix = "You have arranged next lesson:"

let stu_sch_stu_scr_class_sufix = "Please wait for tutor to confirm."

let tut_sch_tut_scr__sch_class_sufix = "Please wait for student to confirm."

let stu_sch_tut_scr = "Student has arranged the next lesson:"

let tut_sch_stu_scr = "Tutor has arranged the next lesson:"

let stu_cancel_bfr_tut_end_stu_scr = "Tuition has been cancelled and chat is now disabled."

let stu_cancel_bfr_tut_end_tut_scr = "Student has cancelled tuition and chat is now disabled."

let tut_cancel_bfr_tut_end_stu_scr = "Tutor has cancelled tuition and chat is now disabled."

let tut_cancel_bfr_tut_end_tut_scr = "Tuition has been cancelled and chat is now disabled. "

let payment_done_msg = "Payment is done. Click on the calendar icon to arrange your first lesson with the tutor.\nChatbox will be enabled 24 hours before the start of the first lesson."

let payment_done_msg_tut = "Payment is done. Click on the calendar icon to arrange your first lesson with the student.\nChatbox will be enable 24 hours before the start of the first lesson."
