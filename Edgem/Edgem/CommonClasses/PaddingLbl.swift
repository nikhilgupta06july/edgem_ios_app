//
//  PaddingLabel.swift
//  Edgem
//
//  Created by Namespace on 16/10/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class PaddingLbl: UILabel{
    
        var topInset: CGFloat = 1.0
        var bottomInset: CGFloat = 1.0
        var leftInset: CGFloat = 2.0
        var rightInset: CGFloat = 2.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
    
}
