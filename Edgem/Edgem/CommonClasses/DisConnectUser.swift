//
//  DisConnectUser.swift
//  Edgem
//
//  Created by Namespace on 19/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import RxSwift



class DisConnectUser:NSObject {

    class func disConnectUser(_ id: String) {
        
        let userObserver = ApiManager.shared.apiService.disconnectUser(id)
        let _ = userObserver.subscribe(onNext: {(message) in
        
            DispatchQueue.main.async {
              print(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
             
            })
        })
    }
    
}

class PrintJSONOnConsole{
    
    static func printJSON(dict data: [String:AnyObject]){
        
        #if DEBUG
        do {
            let json = try JSONSerialization.data(withJSONObject: data, options: [])
            let string1 = String(data: json, encoding: String.Encoding.utf8) ?? "Data could not be printed"
            print("PARAM: \(string1)")
        }catch{
            print(error.localizedDescription)
        }
        #endif
    }
    
}

func print<T: Any>(value: T){
    if Global.DEBUG{
        print(value)
    }
}
