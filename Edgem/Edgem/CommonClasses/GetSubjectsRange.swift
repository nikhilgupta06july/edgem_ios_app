//
//  GetSubjectsRange.swift
//  Edgem
//
//  Created by Namespace on 25/10/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation


class GetSubjectsRange{

    class func get(from levels: [Level]) -> String {
        
        var upperRate = ""
        var lowerRate  = ""
        
        let rates_in_double = levels.map{ _level -> Double  in
            
            if let _price = _level.price{
                return (_price as NSString).doubleValue
            }
            return 0.0
            }.filter{ $0 != 0.0}
        
        
        let sorted_rates = rates_in_double.sorted { (v1, v2) -> Bool in
            v1 > v2
        }
        
        if sorted_rates.count > 1{
            upperRate = sorted_rates[0].isWholeNumber()
            lowerRate  =  sorted_rates[sorted_rates.count-1].isWholeNumber()
            
            if upperRate == lowerRate{
                return "$\(upperRate)/hr"
            }else{
                return "$\(lowerRate)-$\(upperRate)/hr"
            }
        }else if sorted_rates.count == 1{
            upperRate = sorted_rates[0].isWholeNumber()
            return "$\(upperRate)/hr"
        }
        return ""
    }
}
