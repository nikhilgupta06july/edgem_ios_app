//
//  CommonFunctions.swift
//  Edgem
//
//  Created by Hipster on 29/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation
import AMPopTip


class LabelWithPadding: UILabel{
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 0, left: 20, bottom: 0, right: 5)
        super.drawText(in: rect.inset(by: insets))
    }
}

class ButtonWithImage: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageView?.frame = CGRect(x: (self.frame.width)-30, y: 11, width: 20, height: 20)
            self.titleLabel?.frame = CGRect(x: 10, y: 10, width: (self.frame.midX), height: 20)
        }
    }
}

class SetUpToolTip {
    
    func initialSetUp(_ popTip: PopTip) {
        popTip.font = UIFont(name: QuicksandMedium, size: 15)!
        popTip.shouldDismissOnTap = true
        popTip.shouldDismissOnTapOutside = true
        popTip.shouldDismissOnSwipeOutside = true
        popTip.edgeMargin = 5
        popTip.offset = 2
        popTip.bubbleOffset = 0
        popTip.edgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        popTip.arrowRadius = 0
        popTip.bubbleColor = toolTipColor
        popTip.cornerRadius = 4
    }
    
}

//class SetGradiant{
//    static let shared = SetGradiant()
//    private init() {}
//    
//    func setGradients(_ gradient: CAGradientLayer,_ tableView: UITableView){
//        gradient = CAGradientLayer()
//        gradient.frame = (tableView.superview?.frame) ?? .null
//        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
//        gradient.locations = [0.0, 0.05, 0.98, 1.0]
//        tableView.backgroundColor = .clear
//        tableView.superview?.layer.mask = gradient
//    }
//}

class DrawDottedLine{
    static let shared = DrawDottedLine()
    private init(){}
    
    func drawDottedLines(start p0: CGPoint, end p1: CGPoint) -> CAShapeLayer {
        let shapelayer = CAShapeLayer()
        shapelayer.strokeColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1).cgColor
        shapelayer.lineWidth = 1
        shapelayer.lineDashPattern = [7, 3]
        
        let path =  CGMutablePath()
        path.addLines(between: [p0, p1])
        shapelayer.path = path
        return shapelayer
    }
}

class MessageTextView: UITextView {
    
    override var intrinsicContentSize: CGSize {
        let originalContentSize = super.intrinsicContentSize
        return CGSize(width: originalContentSize.width + 80, height: originalContentSize.height)
    }
    
}

class DateHeaderLabel:UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(red: 226.0/255.0, green: 226.0/255.0, blue: 226.0/255.0, alpha: 1.0)
        textColor = theamGrayColor
        textAlignment = .center
        font = UIFont(name: SourceSansProRegular, size: 15)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        let originalContentSize = super.intrinsicContentSize
        let height = originalContentSize.height + 12
        layer.cornerRadius = height / 2
        layer.masksToBounds = true
        return CGSize(width: originalContentSize.width + 40, height: originalContentSize.height + 10)
    }
}

class SelectQualificationLabel:UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.clear
        textColor = theamBlackColor
        textAlignment = .left
        font = UIFont(name: QuicksandBold, size: 18)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
