//
//  AppDelegate.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 01/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import CoreData
import Fabric
import Crashlytics
import IQKeyboardManagerSwift
import GooglePlaces
import FBSDKLoginKit
import GoogleMaps
import GooglePlacePicker
import DropDown
import Firebase
import GoogleSignIn
import FirebaseAuth
import UserNotifications
import SocketIO
import BitlySDK
import Stripe


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
   
    var window: UIWindow?
    var user: User?
    var qualification: [Qualification]?
    var levelPagination: LevelPagination?
    var subjectListing: [Subject]?
    var bookingDetails: [BookingDetails]?
    var idList: [Int]? = [Int]()
    let gcmMessageIDKey = "gcm.message_id"
    var shouldShowPopUPForSubjec = false
    var manager : SocketManager!
    @objc var socket : SocketIOClient!
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        Fabric.with([Crashlytics.self])
        IQKeyboardManager.shared.enable = true
        DropDown.startListeningToKeyboard()
        
        // Set the Google Place API's autocomplete UI control
        GMSPlacesClient.provideAPIKey(GoogleApiKey)
        GMSServices.provideAPIKey(GoogleApiKey)
        
        FirebaseApp.configure()
        
        //
        GIDSignIn.sharedInstance()?.clientID = FirebaseApp.app()?.options.clientID                              // -----------> Client ID is an ID through which we can make calls to Google API from our iOS Application
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        // [START register_for_notifications]
        //self.checkForNotificationPermissions()
        // [End register_for_notifications]
        

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
        // [END register_for_notifications]
        
        //[START Bitly initialization]
        Bitly.initialize(bitly_auth_token)
        //[END Bitly initialization]
        
        //[START configuring Stripe with publishable key]
        STPPaymentConfiguration.shared().publishableKey = PUBLISHABLE_API_KEY
        //[END configuring Stripe with publishable key]
        
        // ----------- INITIAL NAVIGATION -----------
        let splashViewController = SplashViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        let navigationController = BaseNavigationViewController(rootViewController: splashViewController)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    
        
        // [START connect socket]
        if UserStore.shared.isLoggedIn{
            // SocketIOManager.sharedInstance.conectSocket()
            conectSocket()
            
        }
        // [END connect socket]
        return true
    }
    
    private func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        print("Continue User Activity called: ")
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            let url = userActivity.webpageURL!
            print(url.absoluteString)
            //handle url and open whatever page you want to open.
        }
        return true
    }
//
//
//
//    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
//                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
//
////        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
////            // ...
////        }
//
//        return true
//        //handled
//    }

    
     func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {

        if let urlScheme = url.scheme, urlScheme == "com.edgem"{
           // if url.host == "details"{
                var parameters: [String: String] = [:]
                URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
                    parameters[$0.name] = $0.value
                }
                handleNavigationOfScreens(parameters)
           // }
            //Navigation to detail screen
            return true
        }
        
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
         GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
        return facebookDidHandle
    }
    
    //Navigation on tapping deeplinks
    func handleNavigationOfScreens(_ data: [String: String]){
        
        if let topController = self.window!.visibleViewController(), !(topController is SplashViewController){
        guard data["type"] == "profile", UserStore.shared.isLoggedIn else{
            guard data["type"] == "invite", !UserStore.shared.isLoggedIn else{
                return
            }
            let signupViewController = SignUpOptionViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
           signupViewController.userRegistration.referralCode = data["token"] ?? ""
            topController.navigationController?.pushViewController(signupViewController, animated: true)
            return
        }
        if data["user_type"] == tutorType{
            let tutorCompleteProfileViewController = TutorCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            
            tutorCompleteProfileViewController.tutorID = Int(data["id"]!)
            
            tutorCompleteProfileViewController.isFromNotification = true
            
            topController.navigationController?.pushViewController(tutorCompleteProfileViewController, animated: true)
            
        }else{
           
            let studentCompleteProfileViewController = StudentCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            
            studentCompleteProfileViewController.studentID = Int(data["id"]!)
        topController.navigationController?.pushViewController(studentCompleteProfileViewController, animated: true)
        }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                self.handleNavigationOfScreens(data)
            })
            return
        }
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(
            application,
            open: url,
            sourceApplication: sourceApplication,
            annotation: annotation)
        
        GIDSignIn.sharedInstance().handle(url,
                                          sourceApplication: sourceApplication,
                                          annotation: annotation)
        
        return facebookDidHandle
    }
    
      // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
         Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print(value: "Message ID: \(messageID)")
        }
        

         PrintJSONOnConsole.printJSON(dict: userInfo as! [String : AnyObject])
        
        if let info = userInfo as? [String:AnyObject], let apsDict = info["aps"] as? [String:AnyObject], let content_available = apsDict["content-available"] as? String, content_available == "1"{
            
            guard let window = UIApplication.shared.keyWindow else { return }
            
            guard let rootViewController = window.rootViewController else { return }
            
            guard let tabController =  rootViewController as? BaseTabBarViewController else{ return }
            
            let tabArray = tabController.tabBar.items as NSArray!
            
            var alertTabItem: UITabBarItem?
            
            if let notiType = info["noti_type"] as? String, notiType == "chat_notify"{
                alertTabItem = (tabArray?.object(at: 2) as! UITabBarItem)
            }else if let notiType = info["noti_type"] as? String, notiType == "activity_notify"{
                alertTabItem = (tabArray?.object(at: 3) as! UITabBarItem)
            }
            
            guard let tabItem = alertTabItem else{ return }
            
            tabItem.badgeValue = "●"
            tabItem.badgeColor = .clear
            tabItem.setBadgeTextAttributes([NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.red], for: .normal)

        }
    }

    // [END receive_message]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(value: "Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // --- Socket Implementation
    
    func conectSocket(){
        manager = SocketManager(socketURL: URL(string: "\(socketUrl)\(PathURl.Edgem_Socket.rawValue)")!, config: [.log(true), .compress, .forceNew(true), .reconnects(true)])
        socket = manager.defaultSocket
        socket.connect()
        
        if let topController = self.window!.visibleViewController(), (topController is ChatViewController){
            NotificationCenter.default.post(name: .reconnectSocketSubscriber, object: nil, userInfo:nil)
        }
    }
    
    func disconectSocket(){
    
        manager = SocketManager(socketURL: URL(string: "\(baseUrl)\(PathURl.Edgem_Socket.rawValue)")!, config: [.log(true), .compress, .forceNew(true)])
        socket = manager.defaultSocket
        socket.disconnect()
        
    }
    
    @objc func connectToSocketChannel() {
        
        
        if let _ = AppDelegateConstant.socket{
            
            guard AppDelegateConstant.socket.status == .connected else{
                perform(#selector(connectToSocketChannel), with: nil, afterDelay: 1)
                return
            }
        }else{
            AppDelegateConstant.conectSocket()
            guard AppDelegateConstant.socket.status == .connected else{
                perform(#selector(connectToSocketChannel), with: nil, afterDelay: 1)
                return
            }
        }
        var token = ""
        if UserStore.shared.isLoggedIn{
            token = UserStore.shared.token
        }
        let headers = ["Authorization":  "Bearer " + token,
                       "User": String(UserStore.shared.userID ?? 0)]
        
        let tempHeader = ["headers":headers]
        let subscribeData = ["channel": "private-edgem-chat",
                             "name": "subscribe",
                             "auth": tempHeader] as [String : Any]
        
        #if DEBUG
        do {
            let json = try JSONSerialization.data(withJSONObject: subscribeData, options: [])
            let string1 = String(data: json, encoding: String.Encoding.utf8) ?? "Data could not be printed"
            print(string1)
        }catch{
            print(error.localizedDescription)
        }
        #endif
        
        AppDelegateConstant.socket.emitWithAck("subscribe", subscribeData).timingOut(after: 2, callback: { (data) in
            print("subscribed")
                        
            AppDelegateConstant.socket.on("userConnected") {data, ack in
                
                print("==userConnected=====")
                if let tempData = (data[1] as? [String:AnyObject]), let id = tempData["user_online"] as? Int{
                    
                    if Global.idArrays.contains(id) == false{
                        Global.idArrays.append(id)
                    }
                    
                }
                
                print(Global.idArrays)
                NotificationCenter.default.post(name: .userConnected, object: nil)
            }
            
            AppDelegateConstant.socket.on("userDisconnected") {data, ack in
                
                print("==userDisconnected=====")
                if let tempData = (data[1] as? [String:AnyObject]), let id = tempData["user_disconnected"] as? Int{
                    
                    if Global.idArrays.contains(id) == true{
                        let filteredID = Global.idArrays.filter{$0 != id}
                        Global.idArrays = filteredID
                    }
                    
                }
                print(Global.idArrays)
                NotificationCenter.default.post(name: .userConnected, object: nil)
            }
            
        })
        AppDelegateConstant.socket.connect()
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        // With swizzling disabled you must set the APNs token here.
         Messaging.messaging().apnsToken = deviceToken
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        // [START dissconnect socket]
//        if UserStore.shared.isLoggedIn{
//            // SocketIOManager.sharedInstance.disconectSocket()
//            disconectSocket()
//            DisConnectUser.disConnectUser()
//        }
         //[END dissconnect socket]
        
    }
    

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        // [START dissconnect socket]
        if UserStore.shared.isLoggedIn{
           // SocketIOManager.sharedInstance.disconectSocket()
            disconectSocket()
            DisConnectUser.disConnectUser("\(0)")
        }
        // [END dissconnect socket]
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        // [START connect socket]
        if UserStore.shared.isLoggedIn{
            //SocketIOManager.sharedInstance.conectSocket()
            conectSocket()
            connectToSocketChannel()
        }
//        // [END connect socket]
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
//        // [START connect socket]
//        if UserStore.shared.isLoggedIn{
//            //SocketIOManager.sharedInstance.conectSocket()
//            conectSocket()
//            connectToSocketChannel()
//        }
//        // [END connect socket]
    }
    
    

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        disconectSocket()
         DisConnectUser.disConnectUser("\(0)")
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Edgem")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

    // MARK:- Helping Functions
extension AppDelegate{
    
    func checkForNotificationPermissions() {
        
        if #available(iOS 10.0, *) {
            
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings { (settings) in
                
                switch settings.authorizationStatus {
                    
                case .notDetermined :
                    UserStore.shared.isNotificationUnAuthorized = false
                     // Authorization request has not been made yet
                    
                case .denied:
                    UserStore.shared.isNotificationUnAuthorized = true
                    // User has denied authorization.
                    // You could tell them to change this in Settings
                    
                case .authorized:
                    UserStore.shared.isNotificationUnAuthorized = false
                    self.registerForPushNotifications()
                    // User has given authorization.
                    
                default:
                    break
                }
                
            }
            
        }else{
            // Fallback on earlier versions
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                UserStore.shared.isNotificationUnAuthorized = false
                self.registerForPushNotifications()
                
            } else {
                UserStore.shared.isNotificationUnAuthorized = true
            }
        }
        
    }
    
    func registerForPushNotifications() {
        
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {granted, error in
                    guard granted else{
                        UserStore.shared.isNotificationUnAuthorized = true
                        return
                    }
                    DispatchQueue.main.sync {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
            })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
}


// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    
     // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
         Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        //print(userInfo)
         PrintJSONOnConsole.printJSON(dict: userInfo as! [String : AnyObject])
        // Change this to your preferred presentation option
        completionHandler([.alert])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        //print(userInfo)
        PrintJSONOnConsole.printJSON(dict: userInfo as! [String : AnyObject])
        
        completionHandler()
    }
}

// [END ios_10_message_handling]


extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        UserStore.shared.deviceToken = fcmToken
        UserStore.shared.isNotificationUnAuthorized = false
        Utilities.sendDeviceToken(fcmToken)
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: .deviceTokenReceived, object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
