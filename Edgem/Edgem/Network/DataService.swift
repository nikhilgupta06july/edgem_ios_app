//
//  DataService.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 13/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation
//import UIKit
import RxSwift
import Unbox

// MARK: - APIClient (Convenient Resource Methods)

class DataService: EdgemService {

    func facebookLogInWithDict(_ dict: [String : AnyObject]) -> Observable<User> {
        print("PARAMS: \(dict)")
        return Observable.create{ (observer)  in
            let logInObserver = ApiManager.post(pathString: .UserLogin, parameters: dict)
            _ = logInObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .UserLogin, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userData = dataDict["user_data"] as? [String : AnyObject]{
                                do {
                                    let user: User = try unbox(dictionary: userData)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                },
                    onError:{ (error) in
                        observer.onError(error)
                }
            )
            return Disposables.create{}
        }
    }
    
    func loginUserWithEmail(_ email: String, password: String) -> Observable<User> {
        
        let dict = ["identity": email, "password": password] as Dictionary<String, AnyObject>
        print("PARAM: \(dict)")
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(pathString: .UserLogin, parameters: dict)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .UserLogin, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userData = dataDict["user_data"] as? [String : AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userData)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: { (error) in
                    observer.onError(error)
                })
            
            return Disposables.create {}
        }
    }
    
//    func createAccountWithDetails(_ details: [String: AnyObject]) -> Observable<Int> {
//        return Observable.create{ observer in
//            let loginObserver = ApiManager.post(pathString: .AddUser, parameters: details)
//            _ = loginObserver.retry(2)
//                .subscribe(
//                    onNext: {(responseObject) in
//                        ApiManager.validateResponse(forResponse: responseObject, path: .AddUser, successCompletionHandler: { (success, json, error) in
//                            if success == true, let userObject = json, let data = userObject["data"] as? [String: AnyObject], let id = data["id"] as? Int {
//                                observer.on(.next(id))
//                            }else if success == false, let error_ = error {
//                                observer.onError(error_)
//                            } else {
//                                observer.onError(ResponseError.unknownError as NSError)
//                            }
//                        })
//                }, onError: {(error) in
//                    observer.onError(error)
//                })
//            return Disposables.create {}
//        }
//    }
    
    func sentOTPToMobileNumber(_ mobile: String,_ userID: String, _ otp: String) -> Observable<EmailUserID> {
        var params = [String : AnyObject]()

        var api: PathURl = .Blank
        if mobile == ""{
            api = .VerifyForgotPassword
        }else{
            api = .ForgotPassword
        }
   
        params = ["contact_number" : mobile , "user_id" : userID, "otp" : otp] as [String:AnyObject]
        print("PARAMS: \(params)")
        //let params = ["contact_number": mobile] as [String: AnyObject]
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(pathString: api, parameters: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: api, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let data = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let emailUserID: EmailUserID = try unbox(dictionary: data)
                                    observer.on(.next(emailUserID))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
        
    }
    
    func checkUserNameAvailibility(_ username: String) -> Observable<String>{
        let dict: [String : AnyObject] = ["username":username] as [String : AnyObject]
        print("PARAMS: \(dict)")
        return Observable.create{ observer in
            let verifyUserNameObserver = ApiManager.get(path: .verifyUserName, params: dict)
            _ = verifyUserNameObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .verifyUserName, successCompletionHandler: { (success, json, error) in
                            if success == true, let data = json{
                                observer.onNext(data["message"] as! String)
                            }
                             else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func verifyReferralCode(_ referralCode: String) -> Observable<String>{
       let dict: [String : AnyObject] = ["referral_code":referralCode] as [String : AnyObject]
        
        print("PARAMS: \(dict)")
        
        return Observable.create{ observer in
            let verifyUserNameObserver = ApiManager.post(pathString: .VerifyReferralCode, parameters: dict)
            _ = verifyUserNameObserver.retry(2)
                .subscribe(
                    onNext: { (responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .VerifyReferralCode, successCompletionHandler: { (success, json, error) in
                            if success == true, let data = json{
                                observer.onNext(data["message"] as! String)
                            }
                            else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
        
    }
    
     //MARK: -------------------------  Verify SocialID --------------------------------
    
    func verifySocialID(_ socialID: String, _ socialPlateForm: String, email: String) -> Observable<UserSocialData> {
        var dict: [String : AnyObject] = [String:AnyObject]()
        if socialPlateForm == "fb"{
             dict = ["fb_id" : socialID,
                        "email" : email] as [String : AnyObject]
        }else if socialPlateForm == "gmail"{
             dict = ["gl_id" : socialID,
                        "email" : email] as [String : AnyObject]
        }
       
        print("PARAMS: \(dict)")
        
        return Observable.create{ observer in
            let verifyUserNameObserver = ApiManager.post(pathString: .VerifySocialID, parameters: dict)
            _ = verifyUserNameObserver.retry(2)
                .subscribe(
                    onNext: { (responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .VerifySocialID, successCompletionHandler: { (success, json, error) in
                            if success == true, let data = json{
                                do {
                                    let userSocialData: UserSocialData = try unbox(dictionary: data)
                                    observer.on(.next(userSocialData))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            }else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
        
    }
    
    func verifyEmail(_ email: String) -> Observable<String>{
        let dict: [String : AnyObject] = ["username":email] as [String : AnyObject]
        print("PARAMS: \(dict)")
        return Observable.create{ observer in
            let verifyUserNameObserver = ApiManager.get(path: .VerifyEmail, params: dict)
            _ = verifyUserNameObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .VerifyEmail, successCompletionHandler: { (success, json, error) in
                             if success == true, let data = json{
                               observer.onNext(data["message"] as! String)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func verifyContact(_ contact: String) -> Observable<String>{
        let dict: [String : AnyObject] = ["contact_number":contact] as [String : AnyObject]
        print("PARAMS: \(dict)")
        return Observable.create{ observer in
            let logInObserver = ApiManager.post(pathString: .VerifyMobile, parameters: dict)
            _ = logInObserver.retry(2)
                .subscribe(
                    onNext: {responseObject in
                        ApiManager.validateResponse(forResponse: responseObject, path: .VerifyMobile, successCompletionHandler: { (success, json, error) in
                            if success == true, let data = json{
 
                                observer.onNext(data["message"] as! String)
                                
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: { error in
                    observer.onError(error)
                }
            )
            return Disposables.create {}
        }
    }
    
    func creatAccountWithDetail(_ details: [String : AnyObject]) -> Observable<Int>{
        print("PARAMS: \(details)")
        return Observable.create{ observer in
            let logInObserver = ApiManager.post(pathString: .signup, parameters: details)
             _ = logInObserver.retry(2)
                .subscribe(
                    onNext: {responseObject in
                        ApiManager.validateResponse(forResponse: responseObject, path: .signup, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = (json), let data = userObject["data"] as? [String: AnyObject], let id = data["user_id"] as? Int{
                                print(id)
                               observer.onNext(id)
                            }else if success == false, let error_ = error {
                               observer.onError(error_)
                            }else{
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: { error in
                    observer.onError(error)
                }
            )
            return Disposables.create {}
        }
    }
    
    //MARK: ------------------Save Tutor Location service --------------------------
    
    func saveLocationAttributes(_ attributes: [String:AnyObject]) -> Observable <Address>{
        print("PARAMS: \(attributes)")
        return Observable.create{ observer in
            let logInObserver = ApiManager.post(pathString: .AddAddress, parameters: attributes)
            _ = logInObserver.retry(2)
                .subscribe(
                    onNext: {responseObject in
                        ApiManager.validateResponse(forResponse: responseObject, path: .AddAddress, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = (json), let data = userObject["data"] as? NSArray {
                                print(data)
                                var title = ""
                                var addr = ""
                                var lat = ""
                                var long = ""
                                var id = ""
                                var location = [LocationData]()
                                var address = Address()
                                if data.count>0{
                                    for index in 0..<data.count{
                                         title = (data[index] as! NSDictionary)["address_label"] as? String ??  ""
                                         addr = (data[index] as! NSDictionary)["address_location"] as? String ?? ""
                                         lat =  (data[index] as! NSDictionary)["lattitude"] as? String ?? ""
                                         long = (data[index] as! NSDictionary)["longitude"] as? String ?? ""
                                         id = "\((data[index] as! NSDictionary)["address_id"] as! Int)"
                                        location.append(LocationData(profilImage: "home", title: title, address: addr, lat: lat, long: long, addressId: id))
                                    }
                                    address.addr = location
                                }
                                observer.onNext(address)
                            }else if success == false, let error_ = error {
                                observer.onError(error_)
                            }else{
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: { error in
                    observer.onError(error)
                }
            )
            return Disposables.create {}
        }
    }
    
    func verifyOTP(_ otp: String, forUserID userID: Int) -> Observable<User>{
        let dict = ["user_id": userID,
                "otp": otp] as Dictionary<String, AnyObject>
         print("PARAMS: \(dict)")
        return Observable.create{  observer in
            let loginObserver = ApiManager.post(pathString: .AccountActivate, parameters: dict)
            _ = loginObserver.retry(2)
            .subscribe(
                onNext: {(responseObject) in
                    ApiManager.validateResponse(forResponse: responseObject, path: .AccountActivate, successCompletionHandler: { (success, json, error) in
                        if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userData = dataDict["user_data"] as?  [String: AnyObject]{
                            do {
                                let user: User = try unbox(dictionary: userData)
                                                                print(user)
                                observer.on(.next(user))
                            } catch {
                                observer.onError(ResponseError.unboxParseError)
                            }
                        } else if success == false, let error_ = error {
                             observer.onError(error_)
                        }else {
                            observer.onError(ResponseError.unknownError as NSError)
                        }
                    })
            }, onError:{(error) in
                observer.onError(error)
            })
        return Disposables.create {}
    }
}
    
    func EditSavedLocation(){
        
    }
    
    //MARK: -------------------------- Get Saved Location Service -------------------------
    
    func userTempAddresses(_ email: String)->Observable<Address>{
        let params = ["username" : email] as [String:AnyObject]
        print("PARAMS: \(params)")
        return Observable.create{ observer in
            let loginObserver = ApiManager.get(path: .UserTempAddresses, params: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .UserTempAddresses, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let data = userObject["data"] as? NSArray {
                                var title = ""
                                var addr = ""
                                var lat = ""
                                var long = ""
                                var id = ""
                                var location = [LocationData]()
                                var address = Address()
                                if data.count>0{
                                    for index in 0..<data.count{
                                        title = (data[index] as! NSDictionary)["address_label"] as? String ?? ""
                                        addr = (data[index] as! NSDictionary)["address_location"] as? String ?? ""
                                        lat =  (data[index] as! NSDictionary)["lattitude"] as? String ?? ""
                                        long = (data[index] as! NSDictionary)["longitude"] as? String ?? ""
                                        id = "\((data[index] as! NSDictionary)["address_id"] as! Int)"
                                        location.append(LocationData(profilImage: "home", title: title, address: addr, lat: lat, long: long, addressId: id))
                                    }
                                    address.addr = location
                                }
                                 observer.onNext(address)
                            } else if success == true, let userObject = json, let data = userObject["data"] as? String{
                                if data == "No address to show!" || data == "No address to show" || data == "No address to show."{
                                     observer.onNext(Address())
                                }
                                
                            }
                            else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
        
    }
    
    func resendOTPForID(_ userID: Int) -> Observable<[String: AnyObject]> {
        let params = ["id": "\(userID)"] as [String: AnyObject]
        return Observable.create{ observer in
            let loginObserver = ApiManager.get(path: .resendOTP, params: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .resendOTP, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json {
                                observer.on(.next(userObject))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    //MARK: ------------------------    Update Password Service -----------------------
    
    func updatePassword(_ params: [String:AnyObject]) -> Observable<String>{
        print("PARAMS: \(params)")
        
        return Observable.create({ (observer) -> Disposable in
            let updatePasswordObserver = ApiManager.post(pathString: .PasswordUpdate, parameters: params)
             _ = updatePasswordObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .PasswordUpdate, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    //MARK: -------------------------   Fetch Dashboard Data --------------------------------
    
    func fetchDashboardDetails() -> Observable<DashboardResponseData> {
        var dict = [String : Any]()
        if UserStore.shared.isLoggedIn, let userID = UserStore.shared.userID {
            dict["id"] = "\(userID)"
        }
        print("PARAM: \(dict)")
        return Observable.create{ observer in
            let dashboardObserver = ApiManager.get(path: .Dashboard, params: dict as Dictionary<String, AnyObject>)
            _ = dashboardObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .Dashboard, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let jsonData = try? JSONSerialization.data(withJSONObject: dataDict, options: [])
                                    let dashboardResponseDataObject = try? JSONDecoder().decode(DashboardResponseData.self, from: jsonData!)
                                    if let response = dashboardResponseDataObject{
                                         observer.on(.next(response))
                                    }
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    //MARK: -------------------------   Fetch User Detail --------------------------------
    
    func fetchUserDetails()->Observable<User>{
        var userId = 0
        if let uId = UserStore.shared.userID{
            userId = uId
        }
        let dict: [String : Any] = ["id" : "\(userId)"]
        return Observable.create({ (observer) -> Disposable in
            let userObserver = ApiManager.get(path: .userDetails, params: dict as Dictionary<String, AnyObject>)
            _ = userObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .userDetails, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user_data"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userDict)
                                    User.setAppdelegateUser(user: user)
                                    observer.onNext(user)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
             return Disposables.create {}
        })
    }
    
     //MARK: -------------------------   Fetch All Subjects --------------------------------
    
    func fetchSubjects(_ page: Int) -> Observable<SubjectPagination> {
        
        let dict = ["page" : page] as [String : Any?]
        print("PARAM: \(dict)")
        return Observable.create({ (observer) -> Disposable in
            let userObserver = ApiManager.get(path: .Subjects, params: dict as [String:AnyObject])
            _ = userObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .Subjects, successCompletionHandler: { (success, json, error) in
                            if success == true, let dataObject = json {
                                do {
                                    let subjectPagination: SubjectPagination = try unbox(dictionary: dataObject)
                                    observer.on(.next(subjectPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
     //MARK: -------------------------   Fetch All Qualifications --------------------------------
    
    func fetchQualification()->Observable<[Qualification]>{
        return Observable.create{ observer in
            let qualificationObserver = ApiManager.get(path: .Qualifications, params: nil)
            _ = qualificationObserver.retry(2)
                .subscribe(onNext: { (responseObject) in
                    ApiManager.validateResponse(forResponse: responseObject, path: .Qualifications, successCompletionHandler: { (success, json, error) in
                        if success == true, let data = json, let qualificationArray = data["data"] as? [[String:AnyObject]]{

                            var qualifications = [Qualification]()
                            for qualificationDict in qualificationArray{
                                do{
                                    let qualification:Qualification = try unbox(dictionary: qualificationDict)
                                    qualifications.append(qualification)
                                 }catch{
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            }
                             observer.on(.next(qualifications))
                        }else if success == false, let error_ = error{
                             observer.onError(error_)
                        }else{
                             observer.onError(ResponseError.unknownError as NSError)
                        }
                    })
                }, onError: { (error) in
                    observer.onError(error)
                })
            return Disposables.create()
        }
    }
    
    //MARK: -------------------------   Fetch All Levels --------------------------------
    
    func fetchLevels(_ page: Int)->Observable<LevelPagination>{
        
        let dict = ["page" : ""] as [String : Any?]
        print("PARAM: \(dict)")
        
        return Observable.create{ observer in
            let levelObserver = ApiManager.get(path: .Levels, params: dict as Dictionary<String, AnyObject>)
             _ = levelObserver.retry(2)
                .subscribe(onNext: { (responseobject) in
                    ApiManager.validateResponse(forResponse: responseobject, path: .Levels, successCompletionHandler: { (success, json, error) in
                        if success == true, let data = json{
                            do{
                                let levelPagination: LevelPagination = try unbox(dictionary: data)
                                observer.on(.next(levelPagination))
                            }catch{
                                observer.onError(ResponseError.unboxParseError)
                            }
                            
                        }else if success == false, let error_ = error{
                            observer.onError(error_)
                        }else{
                            observer.onError(ResponseError.unknownError as NSError)
                        }
                    })
                }, onError: { (error) in
                    observer.onError(error)
                })
        return Disposables.create()
        }
    }
    
     //MARK: -------------------------   Fetch Tutor Filter Results --------------------------------

    func fetchTutorFilterResult(_ filterDict: [String:AnyObject])->Observable<TutorPagination>{
        
        print("PARAM: \(filterDict)")
        
        return Observable.create{ observer in
            let filterObserver = ApiManager.get(path: .Filter_Tutor, params: filterDict)
            _ = filterObserver.retry(2)
                .subscribe(onNext: { (responseobject) in
                    ApiManager.validateResponse(forResponse: responseobject, path: .Filter_Tutor, successCompletionHandler: { (success, json, error) in
                        if success == true, let userObject = json, let data = userObject["data"] as? [String:AnyObject]{
                            do{
                                let tutorPagination: TutorPagination = try unbox(dictionary: data)
                                observer.on(.next(tutorPagination))
                            }catch{
                                observer.onError(ResponseError.unboxParseError)
                            }
                            
                        }else if success == false, let error_ = error{
                            observer.onError(error_)
                        }else{
                            observer.onError(ResponseError.unknownError as NSError)
                        }
                    })
                }, onError: { (error) in
                    observer.onError(error)
                })
            return Disposables.create()
        }
    }
    
    //MARK: -------------------------   Fetch Booking details --------------------------------
    
    func fetchBookingDetails(_ page: String)->Observable<BookingPagination>{
        
        let paramDict = ["page" : page] as [String:AnyObject]
        
         PrintJSONOnConsole.printJSON(dict: paramDict)
        
        return Observable.create{ observer in
            let filterObserver = ApiManager.get(path: .ListOffer, params: paramDict)
            _ = filterObserver.retry(2)
                .subscribe(onNext: { (responseobject) in
                    ApiManager.validateResponse(forResponse: responseobject, path: .ListOffer, successCompletionHandler: { (success, json, error) in
                        if success == true, let userObject = json, let data = userObject["data"] as? [String:AnyObject]{
                            do{
                                let bookingPagination: BookingPagination = try unbox(dictionary: data)
                                observer.on(.next(bookingPagination))
                            }catch{
                                observer.onError(ResponseError.unboxParseError)
                            }
                            
                        }else if success == false, let error_ = error{
                            observer.onError(error_)
                        }else{
                            observer.onError(ResponseError.unknownError as NSError)
                        }
                    })
                }, onError: { (error) in
                    observer.onError(error)
                })
            return Disposables.create()
        }
    }
    
    //MARK: -------------------------   Fetch Student Filter Results --------------------------------
    
    func fetchStudentFilterResult(_ filterDict: [String:AnyObject])->Observable<StudentPagination>{

        PrintJSONOnConsole.printJSON(dict: filterDict)
        
        return Observable.create{ observer in
            let filterObserver = ApiManager.get(path: .Filter_Student, params: filterDict)
            _ = filterObserver.retry(2)
                .subscribe(onNext: { (responseobject) in
                    ApiManager.validateResponse(forResponse: responseobject, path: .Filter_Student, successCompletionHandler: { (success, json, error) in
                        if success == true, let userObject = json, let data = userObject["data"] as? [String:AnyObject]{
                            do{
                                let studentPagination: StudentPagination = try unbox(dictionary: data)
                                observer.on(.next(studentPagination))
                            }catch{
                                observer.onError(ResponseError.unboxParseError)
                            }
                            
                        }else if success == false, let error_ = error{
                            observer.onError(error_)
                        }else{
                            observer.onError(ResponseError.unknownError as NSError)
                        }
                    })
                }, onError: { (error) in
                    observer.onError(error)
                })
            return Disposables.create()
        }
    }
    
    //MARK: -------------------------  Update User Info  --------------------------------
    
    func updateUserInfo(_ params: [String:AnyObject]) -> Observable<User>{
        
     PrintJSONOnConsole.printJSON(dict: params)
        
        return Observable.create({ (observer) -> Disposable in
            let updateUserInfoObserver = ApiManager.post(pathString: .UpdateUserInfo, parameters: params)
            _ = updateUserInfoObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .UpdateUserInfo, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user_data"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userDict)
                                    observer.onNext(user)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    //MARK: -------------------------  Update Tutor Info  --------------------------------
    
    func updateTutorInfo(_ params: [String:AnyObject]) -> Observable<User>{
        
        PrintJSONOnConsole.printJSON(dict: params)
        
        return Observable.create({ (observer) -> Disposable in
            let updateUserInfoObserver = ApiManager.post(pathString: .TutorProfileUpd, parameters: params)
            _ = updateUserInfoObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .TutorProfileUpd, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user_data"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userDict)
                                    observer.onNext(user)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    //MARK: -------------------------  Add Bookmarks  --------------------------------
    
    func addBookmarks(_ params: [String:AnyObject]) -> Observable<String>{
        
        print("PARAM: \(params)")
        
        return Observable.create({ (observer) -> Disposable in
            let updateUserInfoObserver = ApiManager.post(pathString: .AddBookMark, parameters: params)
            _ = updateUserInfoObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .AddBookMark, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
        
    }
    
    //MARK: -------------------------  Remove Bookmarks  --------------------------------
    
    func removeBookmarks(_ params: [String:AnyObject]) -> Observable<String>{
        
        print("PARAM: \(params)")
        
        return Observable.create({ (observer) -> Disposable in
            let updateUserInfoObserver = ApiManager.post(pathString: .RemoveBookmark, parameters: params)
            _ = updateUserInfoObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .RemoveBookmark, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
        
    }
    
    //MARK: -------------------------  Get Bookmarks Student  --------------------------------
    
    func getBookmarkedStudent(_ page: String) -> Observable<BookMarkedStudent>{
        
        print("PARAM: \(page)")
        let paramDict = ["page" : page] as [String:AnyObject]
        
        return Observable.create({ (observer) -> Disposable in
            let updateUserInfoObserver = ApiManager.get(path: .GetBookmarks, params: paramDict)
            _ = updateUserInfoObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .GetBookmarks, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let data = userObject["data"] as? [String:AnyObject]{
                                do{
                                    let studentPagination: BookMarkedStudent = try unbox(dictionary: data)
                                    observer.on(.next(studentPagination))
                                }catch{
                                    observer.onError(ResponseError.unboxParseError)
                                }
                                
                            }else if success == false, let error_ = error{
                                observer.onError(error_)
                            }else{
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
        
    }
    
    
    //MARK: -------------------------  Get Bookmarks Tutor  --------------------------------
    
    func getBookmarkedTutor(_ page: String) -> Observable<BookMarkedTutor>{
        
        print("PARAM: \(page)")
        let paramDict = ["page" : page] as [String:AnyObject]
        
        return Observable.create({ (observer) -> Disposable in
            let updateUserInfoObserver = ApiManager.get(path: .GetBookmarks, params: paramDict)
            _ = updateUserInfoObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .GetBookmarks, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let data = userObject["data"] as? [String:AnyObject]{
                                do{
                                    let tutorPagination: BookMarkedTutor = try unbox(dictionary: data)
                                    observer.on(.next(tutorPagination))
                                }catch{
                                    observer.onError(ResponseError.unboxParseError)
                                }
                                
                            }else if success == false, let error_ = error{
                                observer.onError(error_)
                            }else{
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
        
    }
    
    //MARK: -------------------------  Get Student Level and Subject  --------------------------------
    
    func getStudentLavelNSubject(_ userID: String) -> Observable<AddLevelSubject>{
        
        print("PARAM: \(userID)")
        let paramDict = ["user_id" : userID] as [String:AnyObject]
        
        return Observable.create({ (observer) -> Disposable in
            let updateUserInfoObserver = ApiManager.get(path: .getStudentlevelNSubject, params: paramDict)
            _ = updateUserInfoObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .getStudentlevelNSubject, successCompletionHandler: { (success, json, error) in
                            if success == true, let data = json, let subject_level = data["data"] as? [String:AnyObject]{
                                do{
                                    let addLevelSubject: AddLevelSubject = try unbox(dictionary: subject_level)
                                    
                                    observer.on(.next(addLevelSubject))
                                }catch{
                                    observer.onError(ResponseError.unboxParseError)
                                }
                                
                            }else if success == false, let error_ = error{
                                observer.onError(error_)
                            }else{
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
        
    }
    
    
    //MARK: -------------------------  LogOut User  --------------------------------
    
    func UserLogout( _ deviceToken: String) -> Observable<String>{
        
      
        let paramDict = ["device_token" : deviceToken] as [String:AnyObject]
          print("PARAM: \(paramDict)")
        return Observable.create({ (observer) -> Disposable in
            let updateUserInfoObserver = ApiManager.post(pathString: .UserLogout, parameters: paramDict)
            _ = updateUserInfoObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .UserLogout, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
        
    }
    
    //MARK: -------------------------  Save level and Subject Info  --------------------------------
    
    func saveLevelAndSubject(_ params: [String:AnyObject]) -> Observable<AddLevelSubjectForStudent>{
        
         PrintJSONOnConsole.printJSON(dict: params)
        
        return Observable.create({ (observer) -> Disposable in
            let updateUserInfoObserver = ApiManager.post(pathString: .postStudentlevelNSubject, parameters: params)
            _ = updateUserInfoObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .postStudentlevelNSubject, successCompletionHandler: { (success, json, error) in
                           if success == true, let data = json, let subject_level = data["data"] as? [String:AnyObject] {
                                do{
                                    let userLavelAndSubject: AddLevelSubjectForStudent = try unbox(dictionary: subject_level)
                                    
                                    observer.on(.next(userLavelAndSubject))
                                }catch{
                                    observer.onError(ResponseError.unboxParseError)
                                }
                                
                            } else if success == false, let error_ = error {
                                
                                    observer.onError(error_)
                                
                            } else {
                                
                                   observer.onError(ResponseError.unknownError as NSError)
                                
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    
    //MARK: ------------------------    Change Password Service -----------------------
    
    func changePassword(_ params: [String:AnyObject]) -> Observable<String>{
        
        print("PARAMS: \(params)")
        
        return Observable.create({ (observer) -> Disposable in
            let updatePasswordObserver = ApiManager.post(pathString: .ChangePassword, parameters: params)
            _ = updatePasswordObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .ChangePassword, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
     //MARK: ------------------------    Submit Contact Us Service -----------------------
    
    func submitContactUs(_ params: [String:AnyObject]) -> Observable<String>{
        
        print("PARAMS: \(params)")
        
        return Observable.create({ (observer) -> Disposable in
            let updatePasswordObserver = ApiManager.post(pathString: .ContactRequest, parameters: params)
            _ = updatePasswordObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .ContactRequest, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
//    //MARK: -------------------------  Add card service  --------------------------------
//
//    func addCard(_ params: [String:AnyObject]) -> Observable<[StoreUserCard]>{
//
//        print(params)
//
//        return Observable.create({ (observer) -> Disposable in
//            let updateUserInfoObserver = ApiManager.post(pathString: .storeUserCard, parameters: params)
//            _ = updateUserInfoObserver.retry(2)
//                .subscribe(
//                    onNext: {(responseObject) in
//                        ApiManager.validateResponse(forResponse: responseObject, path: .storeUserCard, successCompletionHandler: { (success, json, error) in
//                            if success == true, let data = json, let storeUserCardArray = data["data"] as? [[String:AnyObject]] {
//
//                                var storCards = [StoreUserCard]()
//
//                                for cardDict in storeUserCardArray{
//                                    do{
//                                        let card:StoreUserCard = try unbox(dictionary: cardDict)
//                                        storCards.append(card)
//                                    }catch{
//                                        observer.onError(ResponseError.unboxParseError)
//                                    }
//                                }
//                                observer.on(.next(storCards))
//                            } else if success == false, let error_ = error {
//
//                                observer.onError(error_)
//
//                            } else {
//
//                                observer.onError(ResponseError.unknownError as NSError)
//
//                            }
//                        })
//                }, onError: {(error) in
//                    observer.onError(error)
//                })
//            return Disposables.create {}
//        })
//    }
    
    
    //MARK: -------------------------  user details service  --------------------------------
    
    func getUserDetails(_ dict: [String : AnyObject]) -> Observable<User> {
        
        return Observable.create({ (observer) -> Disposable in
            let userObserver = ApiManager.get(path: .userDetailsData, params: dict as Dictionary<String, AnyObject>)
            _ = userObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .userDetailsData, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userDict = dataDict["user_data"] as? [String: AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userDict)
                                    observer.onNext(user)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    //MARK: -------------------------  bank details service  --------------------------------
    
    func getBankDetails() -> Observable<BankDetails> {
        
        return Observable.create({ (observer) -> Disposable in
            let getBankDetailObserver = ApiManager.get(path: .ViewBankDetails)
            _ = getBankDetailObserver.retry(2)
                .subscribe(onNext: { (responseObject) in
                    ApiManager.validateResponse(forResponse: responseObject, path: .ViewBankDetails, successCompletionHandler: { (success, json, error) in
                        if success == true, let userObject = json, let dataDict = userObject["data"] as? [String:AnyObject], let bankDetailData = dataDict["bank_detail"] as? [String:AnyObject] {
                            
                            do{
                                let bankDetails: BankDetails = try unbox(dictionary: bankDetailData)
                                 observer.on(.next(bankDetails))
                            }catch{
                                 observer.onError(ResponseError.unboxParseError)
                            }
                            
                        }
                    })
                }, onError: { (error) in
                      observer.onError(error)
                })
             return Disposables.create {}
        })
    }

//    func getBankDetails() -> Observable<BankDetails> {
//
//        return Observable.create({ (observer) -> Disposable in
//            let userObserver = ApiManager.get(path: .ViewBankDetails, params: nil)
//            _ = userObserver.retry(2)
//                .subscribe(
//                    onNext: {(responseObject) in
//                        ApiManager.validateResponse(forResponse: responseObject, path: .ViewBankDetails, successCompletionHandler: { (success, json, error) in
//                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject],let userData = dataDict["bank_detail"] as?  [String: AnyObject] {
//
//                                    do {
//                                        let user: User = try unbox(dictionary: userData)
//                                        observer.on(.next(user))
//                                    } catch {
//                                        observer.onError(ResponseError.unboxParseError)
//                                    }
//
//                            } else if success == false, let error_ = error {
//                                observer.onError(error_)
//                            } else {
//                                observer.onError(ResponseError.unknownError as NSError)
//                            }
//                        })
//                }, onError: {(error) in
//                    observer.onError(error)
//                })
//            return Disposables.create {}
//        })
//    }
    
     //MARK: -------------------------  manage bank details  --------------------------------
    
    func manageBankDetails(_ dict: [String : String]) -> Observable<BankDetails> {
 
        print("PARAMS: \(dict)")
        return Observable.create{  observer in
            let loginObserver = ApiManager.post(pathString: .ManageBankDetails, parameters: dict as Dictionary<String, AnyObject>)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .ManageBankDetails, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userData = dataDict["bank_detail"] as?  [String: AnyObject]{
                                do {
                                    let bankDetail: BankDetails = try unbox(dictionary: userData)
                                    observer.on(.next(bankDetail))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            }else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError:{(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
      //MARK: -------------------------  get user rating   --------------------------------
    
    func getUserRating(_ dict: [String : AnyObject]) -> Observable<RatingPagination> {
        
        return Observable.create({ (observer) -> Disposable in
            let userObserver = ApiManager.get(path: .UserRatingData, params: dict as Dictionary<String, AnyObject>)
            _ = userObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .UserRatingData, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let ratingPagination: RatingPagination = try unbox(dictionary: dataDict)
                                    observer.onNext(ratingPagination)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    //MARK: -------------------------  Post Tutor Subject Rate   --------------------------------
    
    func postTutorSubjectsRate(_ params: [String: AnyObject]) -> Observable<String>{
        
        PrintJSONOnConsole.printJSON(dict: params)
        
        return Observable.create({ (observer) -> Disposable in
            
            let postObserver = ApiManager.post(pathString: .PostTutorSubjectRate, parameters: params)
            _ = postObserver.retry(2)
                .subscribe(onNext: {(responseObject) in
                    ApiManager.validateResponse(forResponse: responseObject, path: .PostTutorSubjectRate, successCompletionHandler: { (success, json, error) in
                        if success == true, let userObject = json, let message = userObject["message"] as? String {
                            
                            do{
                                let userSubject: [Subject] = try unbox(dictionaries: userObject["data"] as! [UnboxableDictionary])
                                AppDelegateConstant.user?.subjects = userSubject
                                observer.on(.next(message))
                            }catch {
                                observer.onError(ResponseError.unboxParseError)
                            }
                            
            
                        } else if success == false, let error_ = error {
                            observer.onError(error_)
                        } else {
                            observer.onError(ResponseError.unknownError as NSError)
                        }
                    })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    func addPaymentCards(_ params: [String: AnyObject]) -> Observable<[StoreUserCard]>{
        
        return Observable.create({ (observer) -> Disposable in
            
            let postObserver = ApiManager.post(pathString: .storeUserCard, parameters: params)
            _ = postObserver.retry(2)
                .subscribe(onNext: {(responseObject) in
                    ApiManager.validateResponse(forResponse: responseObject, path: .storeUserCard, successCompletionHandler: { (success, json, error) in
                        if success == true, let userObject = json,let data = userObject["data"] as? [String:AnyObject], let cardArray = data["cards"] as? [[String:AnyObject]] {
                            
                            var userCards = [StoreUserCard]()
                            for card in cardArray{
                                do{
                                    let cardDetail:StoreUserCard = try unbox(dictionary: card)
                                    userCards.append(cardDetail)
                                }catch{
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            }
                            observer.on(.next(userCards))
                            
                        } else if success == false, let error_ = error {
                            observer.onError(error_)
                        } else {
                            observer.onError(ResponseError.unknownError as NSError)
                        }
                    })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    func fetchUserPaymentCardDetails()-> Observable<[StoreUserCard]> {
        
        var userId = 0
        if let uId = UserStore.shared.userID{
            userId = uId
        }
        
        let dict: [String : Any] = ["id" : "\(userId)"]
        
        return Observable.create({ (observer) -> Disposable in
            let userCardsObserver = ApiManager.get(path: .UsersCards, params: dict as Dictionary<String, AnyObject>)
            _ = userCardsObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .UsersCards, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json,let data = userObject["data"] as? [String:AnyObject], let cardArray = data["cards"] as? [[String:AnyObject]] {
                                
                                var userCards = [StoreUserCard]()
                                for card in cardArray{
                                    do{
                                        let cardDetail:StoreUserCard = try unbox(dictionary: card)
                                        userCards.append(cardDetail)
                                    }catch{
                                        observer.onError(ResponseError.unboxParseError)
                                    }
                                }
                                observer.on(.next(userCards))
                                
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    func removeUserPaymentCardDetails(_ cardID: String)-> Observable<String> {
        
         let dict: [String : Any] = ["id" : "\(cardID)"]
        
        return Observable.create({ (observer) -> Disposable in
            let userCardsObserver = ApiManager.get(path: .DeleteUserCard, params: dict as Dictionary<String, AnyObject>)
            _ = userCardsObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .DeleteUserCard, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json,let msg = userObject["message"] as? String {
                                
                                observer.on(.next(msg))
                                
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    func addTutorQualificationNExperiences(_ params: [String: AnyObject]) -> Observable<User> {
        
      PrintJSONOnConsole.printJSON(dict: params)
        
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(pathString: .TutorAddQualification, parameters: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .TutorAddQualification, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let userData = dataDict["user_data"] as? [String : AnyObject] {
                                do {
                                    let user: User = try unbox(dictionary: userData)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: { (error) in
                    observer.onError(error)
                })
            
            return Disposables.create {}
        }
    }
    
    func makeOffer(_ params: [String: AnyObject]) -> Observable<MakeOfferRequst> {
        
        PrintJSONOnConsole.printJSON(dict: params)
        
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(pathString: .MakeOffer, parameters: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .MakeOffer, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    
                                    let jsonData = try? JSONSerialization.data(withJSONObject: dataDict, options: [])
                                    let offerObject: MakeOfferRequst = try! JSONDecoder().decode(MakeOfferRequst.self, from: jsonData!)
                                    observer.on(.next(offerObject))
                                    
                                } catch{
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: { (error) in
                    observer.onError(error)
                })
            
            return Disposables.create {}
        }
    }
    
    func editUpdateOffer(_ params: [String: AnyObject]) -> Observable<MakeOfferRequst> {
        
        print(value: "PARAM: \(params)")
        
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(pathString: .EditUpdateOffer, parameters: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .EditUpdateOffer, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    
                                    let jsonData = try? JSONSerialization.data(withJSONObject: dataDict, options: [])
                                    let offerObject: MakeOfferRequst = try! JSONDecoder().decode(MakeOfferRequst.self, from: jsonData!)
                                    observer.on(.next(offerObject))
                                    
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: { (error) in
                    observer.onError(error)
                })
            
            return Disposables.create {}
        }
    }
    
    func counterOffer(_ params: [String: AnyObject]) -> Observable<MakeOfferRequst> {
        
        print("PARAM: \(params)")
        
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(pathString: .CounterOffer, parameters: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .CounterOffer, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    
                                    let jsonData = try? JSONSerialization.data(withJSONObject: dataDict, options: [])
                                    let offerObject: MakeOfferRequst = try! JSONDecoder().decode(MakeOfferRequst.self, from: jsonData!)
                                    observer.on(.next(offerObject))
                                    
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: { (error) in
                    observer.onError(error)
                })
            
            return Disposables.create {}
        }
    }
    
    func cancleOffer(_ params: [String: AnyObject], status: Bool) -> Observable<String> {
        
       print("PARAM: \(params)")
        var  appropriateCancleOfferAPI: PathURl!
        if status == true {
            
            appropriateCancleOfferAPI = PathURl.CancelPaidOffer
            
        }else{
            
            appropriateCancleOfferAPI = PathURl.CancelOffer
            
        }
        
        return Observable.create{ observer in
            let notificationObserver = ApiManager.get(path: appropriateCancleOfferAPI, params: params as Dictionary<String, AnyObject>)
            _ = notificationObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .CancelOffer, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func sendMessage(_ params: [String: AnyObject]) -> Observable<ChatMessage> {
        
        PrintJSONOnConsole.printJSON(dict: params)
        
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(pathString: .SendMessage, parameters: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .SendMessage, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let chatMessage: ChatMessage = try unbox(dictionary: dataDict)
                                    observer.onNext(chatMessage)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: { (error) in
                    observer.onError(error)
                })
            
            return Disposables.create {}
        }
    }
    
    func TutorArrivalStatus(_ params: [String: AnyObject])-> Observable<MakeOfferRequst> {
        
        print("PARAM: \(params)")
        
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(pathString: .CheckinForClass, parameters: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .CheckinForClass, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    
                                    let jsonData = try? JSONSerialization.data(withJSONObject: dataDict, options: [])
                                    let offerObject: MakeOfferRequst = try! JSONDecoder().decode(MakeOfferRequst.self, from: jsonData!)
                                    observer.on(.next(offerObject))
                                    
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: { (error) in
                    observer.onError(error)
                })
            
            return Disposables.create {}
        }
    }
    
    func endTution(_ params: [String: AnyObject])-> Observable<MakeOfferRequst>{
        
        print("PARAM: \(params)")
        
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(pathString: .FinishClass, parameters: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .FinishClass, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    
                                    let jsonData = try? JSONSerialization.data(withJSONObject: dataDict, options: [])
                                    let offerObject: MakeOfferRequst = try! JSONDecoder().decode(MakeOfferRequst.self, from: jsonData!)
                                    observer.on(.next(offerObject))
                                    
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: { (error) in
                    observer.onError(error)
                })
            
            return Disposables.create {}
        }
    }
    
    func manageSchedule(_ params: [String: AnyObject])-> Observable<MakeOfferRequst>{
        
        print("PARAM: \(params)")
        
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(pathString: .ManageSchedule, parameters: params)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .ManageSchedule, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    
                                    let jsonData = try? JSONSerialization.data(withJSONObject: dataDict, options: [])
                                    let offerObject: MakeOfferRequst = try! JSONDecoder().decode(MakeOfferRequst.self, from: jsonData!)
                                    observer.on(.next(offerObject))
                                    
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: { (error) in
                    observer.onError(error)
                })
            
            return Disposables.create {}
        }
    }
    
    func getAllMessages(_ params: [String: AnyObject]) -> Observable<ChatPagination> {
        
        print("PARAM: \(params)")
        
        return Observable.create{ observer in
            let notificationObserver = ApiManager.get(path: .GetAllMessages, params: params as Dictionary<String, AnyObject>)
            _ = notificationObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .GetAllMessages, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do{
                                    let chatPagination: ChatPagination = try unbox(dictionary: dataDict)
                                    observer.on(.next(chatPagination))
                                }catch{
                                    observer.onError(ResponseError.unboxParseError)
                                }
                                
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func acceptOffer(_ params: [String: AnyObject]) -> Observable<MakeOfferRequst> {
        
        print("PARAM: \(params)")
        
        return Observable.create{ observer in
            let notificationObserver = ApiManager.get(path: .AcceptOffer, params: params as Dictionary<String, AnyObject>)
            _ = notificationObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .AcceptOffer, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    
                                    let jsonData = try? JSONSerialization.data(withJSONObject: dataDict, options: [])
                                     print(value: jsonData)
                                    let offerObject: MakeOfferRequst = try! JSONDecoder().decode(MakeOfferRequst.self, from: jsonData!)
                                    observer.on(.next(offerObject))
                                    
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func toggleNotifications(_ isOn: Bool, deviceName: String, userId: String, deviceId: String, deviceToken: String) -> Observable<String> {
        var dict = ["notification_status": isOn ? "1" : "0",
                    "device_id": deviceId,
                    "device_token": deviceToken,
                    "device_type": "iPhone",
                    "device_name": "iOS"] as Dictionary<String, AnyObject>
    
        if userId != "0"{
            dict["user_id"] = userId as AnyObject
        }
        
        print(dict)
        return Observable.create{ observer in
            let notificationObserver = ApiManager.post(pathString: .notifications, parameters: dict)
            _ = notificationObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .notifications, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func toggleNotificationStatusOff(_ status: String, userID: String) -> Observable<String> {
        let dict = ["user_id": userID,
                    "status": status] as Dictionary<String, AnyObject>
        return Observable.create{ observer in
            let notificationObserver = ApiManager.get(path: .notificationStatus, params: dict as Dictionary<String, AnyObject>)
            _ = notificationObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .notificationStatus, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func proseedPayment(_ params: [String : AnyObject]) -> Observable<MakeOfferRequst> {
        
     PrintJSONOnConsole.printJSON(dict: params)
        
        return Observable.create{ observer in
            let paymentObserver = ApiManager.post(pathString: .PayForOffer, parameters: params)
            _ = paymentObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .PayForOffer, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    
                                    let jsonData = try? JSONSerialization.data(withJSONObject: dataDict, options: [])
                                    let offerObject: MakeOfferRequst = try! JSONDecoder().decode(MakeOfferRequst.self, from: jsonData!)
                                    observer.on(.next(offerObject))
                                    
                                } catch{
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func getTutorSubjects(userID: String) -> Observable<[TutorSubjects]> {
    
        let dict = ["tutor_id": userID] as Dictionary<String, AnyObject>
        
        return Observable.create{ observer in
            let notificationObserver = ApiManager.get(path: .TutorSubjectOffered, params: dict as Dictionary<String, AnyObject>)
            _ = notificationObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .TutorSubjectOffered, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let subjectsArray = dataDict["subjects"] as? [[String : AnyObject]] {
                                
                                var tutorSubjects = [TutorSubjects]()
                                for subject in subjectsArray{
                                    do{
                                        let subjectDetail:TutorSubjects = try unbox(dictionary: subject)
                                        tutorSubjects.append(subjectDetail)
                                    }catch{
                                        observer.onError(ResponseError.unboxParseError)
                                    }
                                }
                                observer.on(.next(tutorSubjects))
                                
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func getUserTransactions(_ page: String)->Observable<UserTransactions>{
        
        print("PARAM: \(page)")
        let paramDict = ["page" : page] as [String:AnyObject]
        
        return Observable.create{ observer in
            let filterObserver = ApiManager.get(path: .WalletGetTransactions, params: paramDict)
            _ = filterObserver.retry(2)
                .subscribe(onNext: { (responseobject) in
                    ApiManager.validateResponse(forResponse: responseobject, path: .WalletGetTransactions, successCompletionHandler: { (success, json, error) in
                        if success == true, let userObject = json, let data = userObject["data"] as? [String:AnyObject]{
                            do{
                                let userTransactions: UserTransactions = try unbox(dictionary: data)
                                observer.on(.next(userTransactions))
                            }catch{
                                observer.onError(ResponseError.unboxParseError)
                            }
                            
                        }else if success == false, let error_ = error{
                            observer.onError(error_)
                        }else{
                            observer.onError(ResponseError.unknownError as NSError)
                        }
                    })
                }, onError: { (error) in
                    observer.onError(error)
                })
            return Disposables.create()
        }
    }
    
    func getUserActivities(_ page: String)->Observable<UserActivities>{
        
        print("PARAM: \(page)")
        let paramDict = ["page" : page] as [String:AnyObject]
        
        return Observable.create{ observer in
            let filterObserver = ApiManager.get(path: .GetUserActivity, params: paramDict)
            _ = filterObserver.retry(2)
                .subscribe(onNext: { (responseobject) in
                    ApiManager.validateResponse(forResponse: responseobject, path: .GetUserActivity, successCompletionHandler: { (success, json, error) in
                        if success == true, let userObject = json, let data = userObject["data"] as? [String:AnyObject]{
                            do{
                                let userActivities: UserActivities = try unbox(dictionary: data)
                                observer.on(.next(userActivities))
                            }catch{
                                observer.onError(ResponseError.unboxParseError)
                            }
                            
                        }else if success == false, let error_ = error{
                            observer.onError(error_)
                        }else{
                            observer.onError(ResponseError.unknownError as NSError)
                        }
                    })
                }, onError: { (error) in
                    observer.onError(error)
                })
            return Disposables.create()
        }
    }
    
    func removeUserActivity(_ params: [String : AnyObject]) -> Observable<String> {
        print(params)
        return Observable.create{ observer in
            
            let paymentObserver = ApiManager.post(pathString: .DeleteUserActivity, parameters: params)
            _ = paymentObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .DeleteUserActivity, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create ()
        }
    }
    
    func checkUserWeeklyAvailibility(_ params: [String : AnyObject])-> Observable<WeeklyAvailibility> {
        
        print(params)
        
        return Observable.create{ observer in
            
            let paymentObserver = ApiManager.post(pathString: .CheckUserWeeklyAvailability, parameters: params)
            _ = paymentObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .CheckUserWeeklyAvailability, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                
                                do{
                                    let availibilty: WeeklyAvailibility = try unbox(dictionary: dataDict)
                                    observer.on(.next(availibilty))
                                }catch{
                                    observer.onError(ResponseError.unboxParseError)
                                }
                                
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create ()
        }
        
    }
    
    func postUserAvailibility(_ params: [String : AnyObject])-> Observable<UserAvailibility>{
        print(params)
        return Observable.create{ observer in
            
            let paymentObserver = ApiManager.post(pathString: .PostUserAvailability, parameters: params)
            _ = paymentObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .PostUserAvailability, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                
                                do{
                                    let availibilty: UserAvailibility = try unbox(dictionary: dataDict)
                                    observer.on(.next(availibilty))
                                }catch{
                                    observer.onError(ResponseError.unboxParseError)
                                }
                                
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create ()
        }
    }
    
    func getUserAvailibility(_ userID: String)-> Observable<UserAvailibility> {
        
        let paramDict = ["user_id" : userID] as [String:AnyObject]
        
        print(paramDict)
        
        return Observable.create{ observer in
            let filterObserver = ApiManager.get(path: .GetUserAvailability, params: paramDict)
            _ = filterObserver.retry(2)
                .subscribe(onNext: { (responseobject) in
                    ApiManager.validateResponse(forResponse: responseobject, path: .GetUserAvailability, successCompletionHandler: { (success, json, error) in
                        if success == true, let userObject = json, let data = userObject["data"] as? [String:AnyObject]{
                            do{
                                let availibility: UserAvailibility = try unbox(dictionary: data)
                                observer.on(.next(availibility))
                            }catch{
                                observer.onError(ResponseError.unboxParseError)
                            }
                            
                        }else if success == false, let error_ = error{
                            observer.onError(error_)
                        }else{
                            observer.onError(ResponseError.unknownError as NSError)
                        }
                    })
                }, onError: { (error) in
                    observer.onError(error)
                })
            return Disposables.create()
        }
    }
    
    func getUserBooking(_ userID: String)-> Observable<UserBooking> {
        
        let paramDict = ["user_id" : userID] as [String:AnyObject]
        
        print(paramDict)
        
        return Observable.create{ observer in
            let filterObserver = ApiManager.get(path: .GetUserBooking, params: paramDict)
            _ = filterObserver.retry(2)
                .subscribe(onNext: { (responseobject) in
                    ApiManager.validateResponse(forResponse: responseobject, path: .GetUserBooking, successCompletionHandler: { (success, json, error) in
                        if success == true, let userObject = json, let data = userObject["data"] as? [String:AnyObject]{
                            do{
                                let availibility: UserBooking = try unbox(dictionary: data)
                                observer.on(.next(availibility))
                            }catch{
                                observer.onError(ResponseError.unboxParseError)
                            }
                            
                        }else if success == false, let error_ = error{
                            observer.onError(error_)
                        }else{
                            observer.onError(ResponseError.unknownError as NSError)
                        }
                    })
                }, onError: { (error) in
                    observer.onError(error)
                })
            return Disposables.create()
        }
    }
    
    func disconnectUser(_ id: String) -> Observable<String> {

        var dict = [String : Any]()
        dict["id"] = "\(id)"
        print("PARAM: \(dict)")
        
        return Observable.create{ observer in
            let verifyUserNameObserver = ApiManager.get(path: .DisconnectUser, params: dict as Dictionary<String, AnyObject>)
            _ = verifyUserNameObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .DisconnectUser, successCompletionHandler: { (success, json, error) in
                            if success == true, let data = json{
                                observer.onNext(data["message"] as! String)
                            }
                            else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func scheduleClass(_ params: [String : AnyObject])-> Observable<String> {
        
        print(params)
        
        return Observable.create{ observer in
            
            let paymentObserver = ApiManager.post(pathString: .ScheduleClass, parameters: params)
            _ = paymentObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .ScheduleClass, successCompletionHandler: { (success, json, error) in
                            if success == true, let data = json{
                                observer.onNext(data["message"] as! String)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create ()
        }
    }
    
    func requestOTP(_ params: [String : AnyObject])-> Observable<String>{
        
        print(params)
        
        return Observable.create{ observer in
            
            let paymentObserver = ApiManager.post(pathString: .RequestOtp, parameters: params)
            _ = paymentObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .RequestOtp, successCompletionHandler: { (success, json, error) in
                            if success == true, let data = json{
                                observer.onNext(data["message"] as! String)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create ()
        }
    }
    
    func verifyRequestedOtp(_ params: [String : AnyObject])-> Observable<String>{
        
        print(params)
        
        return Observable.create{ observer in
            
            let paymentObserver = ApiManager.post(pathString: .VerifyRequestedOtp, parameters: params)
            _ = paymentObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .VerifyRequestedOtp, successCompletionHandler: { (success, json, error) in
                            if success == true, let data = json{
                                observer.onNext(data["message"] as! String)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create ()
        }
    }
    
    func submitReview(_ params: [String:AnyObject]) -> Observable<String>{
        print("PARAMS: \(params)")
        
        return Observable.create({ (observer) -> Disposable in
            let updatePasswordObserver = ApiManager.post(pathString: .RateUser, parameters: params)
            _ = updatePasswordObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .RateUser, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let message = userObject["message"] as? String {
                                observer.on(.next(message))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    func getBank() -> Observable<Banks> {
        
        return Observable.create({ (observer) -> Disposable in
            let userObserver = ApiManager.get(path: .GetBanks, params: nil)
            _ = userObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .GetBanks, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                    let banks: Banks = try unbox(dictionary: dataDict)
                                    observer.onNext(banks)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    func getRecommendedTutors() -> Observable<RecommendedTutors> {
        
        return Observable.create({ (observer) -> Disposable in
            let userObserver = ApiManager.get(path: .RecommendedTutors, params: nil)
            _ = userObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .RecommendedTutors, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject] {
                                do {
                                     let recommendedTutors: RecommendedTutors = try unbox(dictionary: dataDict)
                                     observer.onNext(recommendedTutors)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }
    
    func getSchoolList(_ params: [String:AnyObject]) -> Observable<SchoolList>{
        
         PrintJSONOnConsole.printJSON(dict: params)
        
        return Observable.create({ (observer) -> Disposable in
            
            let updatePasswordObserver = ApiManager.post(pathString: .GetFilteredSchool, parameters: params)
            
            _ = updatePasswordObserver.retry(2)
                
                .subscribe(
                    
                    onNext: {(responseObject) in
                        
                        ApiManager.validateResponse(forResponse: responseObject, path: .GetFilteredSchool, successCompletionHandler: { (success, json, error) in
                            
                            if success == true, let userObject = json, let schoolList = userObject["data"] as? [String:AnyObject] {
                                
                                do {
                                    let schoolList: SchoolList = try unbox(dictionary: schoolList)
                                    observer.onNext(schoolList)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                                
                            } else if success == false, let error_ = error {
                                
                                observer.onError(error_)
                                
                            } else {
                                
                                observer.onError(ResponseError.unknownError as NSError)
                                
                            }
                            
                        })
                }, onError: {(error) in
                    
                    observer.onError(error)
                    
                })
            
            return Disposables.create {}
            
        })
    }
    
    func getBookingDetail(_ param: [String:AnyObject]) -> Observable<BookingInfo> {
        
         PrintJSONOnConsole.printJSON(dict: param)
        
        return Observable.create({ (observer) -> Disposable in
            let userObserver = ApiManager.get(path: .GetBookingDetails, params: param)
            _ = userObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .GetBookingDetails, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json, let dataDict = userObject["data"] as? [String: AnyObject], let booking =  dataDict["booking"] as? [String: AnyObject]{
                                do {
                                    let bookingInfo: BookingInfo = try unbox(dictionary: booking)
                                    observer.onNext(bookingInfo)
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        })
    }

}
