//
//  SocketIOManager.swift
//  Edgem
//
//  Created by Namespace on 17/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import SocketIO

class SocketIOManager: NSObject {

    static let sharedInstance = SocketIOManager()
    var manager : SocketManager!
    var window = UIWindow()
    @objc var socket : SocketIOClient?
    
    override init() {
        super.init()
//        manager = SocketManager(socketURL: URL(string: "\(socketUrl)\(PathURl.Edgem_Socket.rawValue)")!, config: [.log(true), .compress, .forceNew(true), .reconnects(true)])
//        socket = manager.defaultSocket
//        socket.on(clientEvent: .connect) {data, ack in
//            print("socket connected \(data)")
//            
////            let id:String = getDataFromPreference(key: ID)
////            let identifier:String = getDataFromPreference(key: IDENTIFIER)
////            self.socket.emit(
////                "client join", ["id":clientId, "identifier":identifier]
////            )
//            
//        }
//        
//        socket.on("messageSent") { data, ack in
//            print("message Sent \(data)")
//        }
        
    }
    
    // --- Socket Implementation
    
    func conectSocket(){
        manager = SocketManager(socketURL: URL(string: "\(socketUrl)\(PathURl.Edgem_Socket.rawValue)")!, config: [.log(true), .compress, .forceNew(true), .reconnects(true)])
        socket = manager.defaultSocket
        socket?.connect()
        
        if let topController = self.window.visibleViewController(), (topController is ChatViewController){
            NotificationCenter.default.post(name: .reconnectSocketSubscriber, object: nil, userInfo:nil)
        }
    }
    
    func disconectSocket(){
        manager = SocketManager(socketURL: URL(string: "\(baseUrl)\(PathURl.Edgem_Socket.rawValue)")!, config: [.log(true), .compress, .forceNew(true)])
        socket = manager.defaultSocket
        socket?.disconnect()
        
    }
}
