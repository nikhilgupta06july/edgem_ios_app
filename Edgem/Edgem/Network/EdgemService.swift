//
//  EdgemService.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 13/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation
import RxSwift

protocol EdgemService {
    
    func loginUserWithEmail(_ email: String, password: String) -> Observable<User>
    
    func sentOTPToMobileNumber(_ mobile: String,_ userID: String, _ otp: String) -> Observable<EmailUserID>
    
    func checkUserNameAvailibility(_ username: String) -> Observable<String>
    
    func verifyEmail(_ email: String) -> Observable<String>
    
    func verifyContact(_ contact: String) -> Observable<String>
    
    func creatAccountWithDetail(_ details: [String : AnyObject]) -> Observable<Int>
    
    func verifyOTP(_ otp: String, forUserID userID: Int) -> Observable<User>
    
    func resendOTPForID(_ userID: Int) -> Observable<[String: AnyObject]>
    
    func facebookLogInWithDict(_ dict: [String:AnyObject]) -> Observable<User>
    
    func saveLocationAttributes(_ attributes: [String:AnyObject]) -> Observable <Address>
    
    func userTempAddresses(_ email: String)->Observable<Address>
    
    func updatePassword(_ params: [String:AnyObject]) -> Observable<String>
    
    func fetchDashboardDetails() -> Observable<DashboardResponseData>
    
    func fetchUserDetails()->Observable<User>
    
    func fetchSubjects(_ page: Int) -> Observable<SubjectPagination>
    
    func fetchQualification()->Observable<[Qualification]>
    
    func fetchLevels(_ page: Int)->Observable<LevelPagination>
    
    func fetchTutorFilterResult(_ filterDict: [String:AnyObject])->Observable<TutorPagination>
    
    func fetchStudentFilterResult(_ filterDict: [String:AnyObject])->Observable<StudentPagination>
    
   // func updateUserInfo(_ params: [String:AnyObject]) -> Observable<String>
    func updateUserInfo(_ params: [String:AnyObject]) -> Observable<User>
    
    //func verifySocialID(_ socialID: String, _ socialPlateForm: String) -> Observable<UserSocialData>
    func verifySocialID(_ socialID: String, _ socialPlateForm: String, email: String) -> Observable<UserSocialData>
    
    func addBookmarks(_ params: [String:AnyObject]) -> Observable<String>
    
    func removeBookmarks(_ params: [String:AnyObject]) -> Observable<String>
    
     func getBookmarkedStudent(_ page: String) -> Observable<BookMarkedStudent>
    
     func getBookmarkedTutor(_ page: String) -> Observable<BookMarkedTutor>
    
     func getStudentLavelNSubject(_ page: String) -> Observable<AddLevelSubject>
    
    //func saveLevelAndSubject(_ params: [String:AnyObject]) -> Observable<AddLevelSubject>
    func saveLevelAndSubject(_ params: [String:AnyObject]) -> Observable<AddLevelSubjectForStudent>
     func changePassword(_ params: [String:AnyObject]) -> Observable<String>
    
    func submitContactUs(_ params: [String:AnyObject]) -> Observable<String>
    
   // func addCard(_ params: [String:AnyObject]) -> Observable<[StoreUserCard]>
    
     func getUserDetails(_ dict: [String : AnyObject]) -> Observable<User>
    
    func manageBankDetails(_ dict: [String : String]) -> Observable<BankDetails>
    
    func getUserRating(_ dict: [String : AnyObject]) -> Observable<RatingPagination>
    
    func postTutorSubjectsRate(_ params: [String: AnyObject]) -> Observable<String>
    
    func addPaymentCards(_ params: [String: AnyObject]) -> Observable<[StoreUserCard]>
    
    func fetchUserPaymentCardDetails()-> Observable<[StoreUserCard]>
    
    func removeUserPaymentCardDetails(_ cardID: String)-> Observable<String>
    
    func updateTutorInfo(_ params: [String:AnyObject]) -> Observable<User>
    
    func addTutorQualificationNExperiences(_ params: [String: AnyObject]) -> Observable<User>
    
    func makeOffer(_ params: [String: AnyObject]) -> Observable<MakeOfferRequst>
    
    func editUpdateOffer(_ params: [String: AnyObject]) -> Observable<MakeOfferRequst>
    
   // func cancleOffer(_ params: [String: AnyObject]) -> Observable<String>
    func cancleOffer(_ params: [String: AnyObject], status: Bool) -> Observable<String>
    
     func toggleNotifications(_ isOn: Bool, deviceName: String, userId: String, deviceId: String, deviceToken: String) -> Observable<String>
    
     func toggleNotificationStatusOff(_ status: String, userID: String) -> Observable<String>
    
    func fetchBookingDetails(_ page: String)->Observable<BookingPagination>
    
     func getTutorSubjects(userID: String) -> Observable<[TutorSubjects]>
    
     func counterOffer(_ params: [String: AnyObject]) -> Observable<MakeOfferRequst>
    
     func acceptOffer(_ params: [String: AnyObject]) -> Observable<MakeOfferRequst>
    
      func UserLogout( _ deviceToken: String) -> Observable<String>
    
    func proseedPayment(_ params: [String : AnyObject]) -> Observable<MakeOfferRequst>
    
    func getAllMessages(_ params: [String: AnyObject]) -> Observable<ChatPagination>
    
    func sendMessage(_ params: [String: AnyObject]) -> Observable<ChatMessage>
    
    func getBankDetails() -> Observable<BankDetails>
    
    func getUserActivities(_ page: String)->Observable<UserActivities>
    
     func getUserTransactions(_ page: String)->Observable<UserTransactions>
    
     func verifyReferralCode(_ referralCode: String) -> Observable<String>
    
    func removeUserActivity(_ params: [String : AnyObject]) -> Observable<String>
    
    func postUserAvailibility(_ params: [String : AnyObject])-> Observable<UserAvailibility>
    
    func getUserAvailibility(_ userID: String)-> Observable<UserAvailibility>
    
    func getUserBooking(_ userID: String)-> Observable<UserBooking>
    
    func checkUserWeeklyAvailibility(_ params: [String : AnyObject])-> Observable<WeeklyAvailibility>
    
    func TutorArrivalStatus(_ params: [String: AnyObject])-> Observable<MakeOfferRequst>
    
    func endTution(_ params: [String: AnyObject])-> Observable<MakeOfferRequst>
    
    func manageSchedule(_ params: [String: AnyObject])-> Observable<MakeOfferRequst>
    
      func disconnectUser(_ id: String) -> Observable<String>
    
     func scheduleClass(_ params: [String : AnyObject])-> Observable<String>
    
    func requestOTP(_ params: [String : AnyObject])-> Observable<String>
    
    func verifyRequestedOtp(_ params: [String : AnyObject])-> Observable<String>
    
     func submitReview(_ params: [String:AnyObject]) -> Observable<String>
    
      func getBank() -> Observable<Banks>
    
    func getRecommendedTutors() -> Observable<RecommendedTutors>
    
    func getSchoolList(_ params: [String:AnyObject]) -> Observable<SchoolList>
    
   func getBookingDetail(_ param: [String:AnyObject]) -> Observable<BookingInfo>
}
