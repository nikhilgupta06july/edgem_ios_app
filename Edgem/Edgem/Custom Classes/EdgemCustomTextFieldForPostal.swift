//
//  EdgemCustomTextField.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 12/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

//@IBDesignable
public class EdgemCustomTextFieldForPostal: SkyFloatingLabelTextField {
    
    public var iconImage: UIImageView!
    public var iconWidth: CGFloat = 20.0
    
    @IBInspectable
    public var icon: String? {
        didSet {
            iconImage?.image = UIImage(named: icon ?? "tick")
        }
    }
    
    public var textFont: UIFont? {
        didSet {
            self.titleFont = textFont ?? textFieldDefaultFont
            self.placeholderFont = textFont ?? textFieldDefaultFont
            self.font = textFont ?? textFieldDefaultFont
        }
    }
    
    // MARK: Initializers
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        createIconImage()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createIconImage()
    }
    
    // MARK: Creating the icon label
    
    func createIconImage() {
        iconImage = UIImageView()
        iconImage.contentMode = .scaleAspectFit
        iconImage.isHidden = true
        addSubview(iconImage)
    }
    
    // MARK: Handling the icon color
    
    override public func updateColors() {
        super.updateColors()
    }
    
    
    // MARK: Custom layout overrides
    
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.textRect(forBounds: bounds)
        rect.origin.x = 10
        rect.size.width -= iconWidth
        return rect
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.textRect(forBounds: bounds)
        rect.origin.x = 10
        rect.size.width -= iconWidth
        return rect
    }
    
    override public func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.placeholderRect(forBounds: bounds)
        rect.origin.x = 10
        rect.size.width -= iconWidth
        return rect
    }
    
    override public func titleLabelRectForBounds(_ bounds: CGRect, editing: Bool) -> CGRect {
        var rect = super.titleLabelRectForBounds(bounds, editing: true)
        rect.origin.x = 10
        rect.size.width -= iconWidth
        return rect
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        let textWidth: CGFloat = bounds.size.width
        
        iconImage.frame = CGRect(
            x: textWidth - iconWidth-4,
            y: bounds.size.height - textHeight(),
            width: iconWidth,
            height: textHeight()
        )
    }
    
    // MARK: Actions
    
    func hideImage() {
        iconImage.isHidden = true
    }
    
    func showImage() {
        iconImage.isHidden = false
    }
    
    func setImage(name: String) {
        iconImage?.image = UIImage(named: name)
    }
    
}
