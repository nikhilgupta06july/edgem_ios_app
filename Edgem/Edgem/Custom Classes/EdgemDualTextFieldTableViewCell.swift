//
//  EdgemDualTextFieldTableViewCell.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 17/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit

class EdgemDualTextFieldTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var textFieldOne: EdgemCustomTextField!
    @IBOutlet weak var textFieldTwo: EdgemCustomTextField!
    @IBOutlet weak var errorMessageLabelOne: UILabel!
    @IBOutlet weak var errorMessageLabelTwo: UILabel!
    
    class func cellIdentifier() -> String {
        return "EdgemDualTextFieldTableViewCell"
    }
    
    func configureCellWithTitles(title: [String], index: Int) {
        textFieldOne.tag = index
        //textFieldOne.placeholder = title[0]
        textFieldOne.title = ""//title[0]
        textFieldOne.text = title[0]
        textFieldOne.tintColor = UIColor.clear
        textFieldOne.textFont = textFieldMediumFont
        textFieldOne.placeholderFont = textFieldDefaultFont
        textFieldOne.titleFont = textFieldDefaultFont
        
        textFieldTwo.tag = index
        textFieldTwo.placeholder = title[1]
        textFieldTwo.title = title[1]
        textFieldTwo.textFont = textFieldMediumFont
        textFieldTwo.placeholderFont = textFieldDefaultFont
        textFieldTwo.titleFont = textFieldDefaultFont
        
    }
    
    func configureUpdateProfileCellForRowAtIndex(title: [String], index: Int) {
        textFieldOne.tag = index
        //textFieldOne.placeholder = title[0]
        textFieldOne.title = ""//title[0]
        textFieldOne.text = title[0]
        textFieldOne.tintColor = UIColor.clear
        textFieldOne.textFont = textFieldMediumFont
        textFieldOne.placeholderFont = textFieldDefaultFont
        textFieldOne.titleFont = UIFont(name: QuicksandRegular, size: 13.0)!
        
        textFieldTwo.tag = index
        textFieldTwo.placeholder = title[1]
        textFieldTwo.title = title[1]
        textFieldTwo.textFont = textFieldMediumFont
        textFieldTwo.placeholderFont = textFieldDefaultFont
        textFieldTwo.titleFont = UIFont(name: QuicksandRegular, size: 13.0)!
        
    }
    
}

// MARK: - Validations

extension EdgemDualTextFieldTableViewCell {
    
    // MARK: Country Code
    func validateCountryCode(text: String) {
        if text.isEmptyString() {
            errorMessageLabelOne.text = ValidationErrorMessage.countryCode.description()
        } else {
            errorMessageLabelOne.text = ""
        }
    }
    
    
        // MARK: Contact Number
        func validatePhone(text: String) {
            if text.isEmptyString() {
                if textFieldTwo != nil {
                    textFieldTwo.hideImage()
                }
                errorMessageLabelTwo.text = ValidationErrorMessage.phoneEmpty.description()
            } else if !text.isValidPhoneNumber() {
                textFieldTwo.hideImage()
                errorMessageLabelTwo.text = ValidationErrorMessage.phoneInvalid.description()
            } else {
                textFieldTwo.showImage()
                errorMessageLabelTwo.text = ""
            }
        }
    
    
    // MARK: City
    func validateCity(text: String) {
        //        if text.isEmptyString() {
        //            textFieldOne.hideImage()
        //            errorMessageLabelOne.text = "Please enter City."
        //        } else {
        //            textFieldOne.showImage()
        //            errorMessageLabelOne.text = ""
        //        }
    }
    
    // MARK: Zip Code
    func validateZip(text: String) {
        //        if !text.isValidPostalCode() {
        //            textFieldTwo.hideImage()
        //            errorMessageLabelTwo.text = ValidationErrorMessage.postalCode.description()
        //        } else {
        //            if text.isEmpty {
        //                textFieldTwo.hideImage()
        //            } else {
        //                textFieldTwo.showImage()
        //            }
        //            errorMessageLabelTwo.text = ""
        //        }
    }
}
