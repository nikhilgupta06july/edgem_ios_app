//
//  EdgemTextFieldTableViewCell.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 14/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit

protocol CreateUserViewControllerDelegate{
    func showHideNextButton(_ status: Bool)
}

protocol EdgemTextFieldTableViewCellDelegate {
    func valueChangedForTextField(_ textField: EdgemCustomTextField)
}

protocol RefreshButtonTappedDelegate{
    func refreshBtnTapped()
}

class EdgemTextFieldTableViewCell: UITableViewCell {
    
    @IBOutlet weak var customTextFieldForPostal: EdgemCustomTextFieldForPostal!
    @IBOutlet weak var customTextField: EdgemCustomTextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var correctIcon: UIImageView!
    @IBOutlet weak var refreshBtn: UIButton!
    
    var delegate: EdgemTextFieldTableViewCellDelegate?
    var refreshDelegate: RefreshButtonTappedDelegate?
    var createUserViewControllerDelegate: CreateUserViewControllerDelegate!
    
    class func cellIdentifier() -> String {
        return "EdgemTextFieldTableViewCell"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        errorLabel.text = ""
    }
    
    func configureCellForRowAtIndex(_ index: Int, withText text: String) {
        //correctIcon.image = nil
        if customTextField != nil{
            customTextField.tag = index
            customTextField.placeholder = text
            customTextField.title = text
            customTextField.textFont = textFieldMediumFont
            customTextField.placeholderFont = textFieldDefaultFont
            customTextField.titleFont = textFieldDefaultFont
        }
    }
    
    func configureCellForRowAtIndexForEmail(_ index: Int, withText text: String) {
        //correctIcon.image = nil
        if customTextField != nil{
            customTextField.tag = 2
            customTextField.placeholder = text
            customTextField.title = text
            customTextField.textFont = textFieldMediumFont
            customTextField.placeholderFont = textFieldDefaultFont
            customTextField.titleFont = textFieldDefaultFont
        }
    }
    
    func configureCellForRowAtIndexForTutor(_ index: Int, withText text: String) {
        //correctIcon.image = nil
        if customTextField != nil{
            customTextField.tag = index
            customTextField.placeholder = text
            customTextField.title = text
            if index == 0 ||  index == 1  || index == 2 || index == 3{
                 customTextField.textFont = textFieldRegularFont
            }else{
                 customTextField.textFont = textFieldMediumFont
            }
            customTextField.placeholderFont = textFieldDefaultFont
            customTextField.titleFont = textFieldDefaultFont
        }
    }
    
    func configurePostalCellForRowAtIndex(_ index: Int, withText text: String) {
        //correctIcon.image = nil
        if customTextFieldForPostal != nil{
            customTextFieldForPostal.tag = index
            customTextFieldForPostal.placeholder = text
            customTextFieldForPostal.title = text
            customTextFieldForPostal.textFont = textFieldMediumFont
            customTextFieldForPostal.placeholderFont = textFieldDefaultFont
            customTextFieldForPostal.titleFont = textFieldDefaultFont
        }
    }
    
    func configureUpdateProfileCellForRowAtIndex(_ index: Int, withText text: String, currentIndex c_index: Int) {
        //correctIcon.image = nil
        if customTextField != nil{
            customTextField.tag = index
            customTextField.placeholder = text
            customTextField.title = text
            customTextField.textFont = textFieldMediumFont
            customTextField.placeholderFont = textFieldDefaultFont
            customTextField.titleFont = UIFont(name: QuicksandRegular, size: 13.0)!
            if c_index == 3 && index == 0{
                 refreshBtn.isHidden = false
            }else{
                 refreshBtn.isHidden = true
            }
        }
    }
    
    
    
    @IBAction func valueChangedInTextField(_ sender: EdgemCustomTextField) {
        delegate?.valueChangedForTextField(sender)
    }
    
    @IBAction func refreshBtnTapped(_ sender: UIButton) {
        refreshDelegate?.refreshBtnTapped()
    }
}

// MARK: - Validations

extension EdgemTextFieldTableViewCell {
//
//    func validateConfirmEmail(text: String, email: String) {
//        if text.isEmptyString() {
//            customTextField.hideImage()
//            errorLabel.text = ValidationErrorMessage.emailEmpty.description()
//        } else if !text.isValidEmailID() {
//            customTextField.hideImage()
//            errorLabel.text = ValidationErrorMessage.emailInvalid.description()
//        } else if text != email {
//            customTextField.hideImage()
//            errorLabel.text = ValidationErrorMessage.emailMismatch.description()
//        } else {
//            customTextField.showImage()
//            errorLabel.text = ""
//        }
//    }
//
//
    
    // MARK: Account Number
    func validateAccountNumber(text: String) {
        if text.isEmptyString() {
            errorLabel.text = ValidationErrorMessage.accountEmpty.description()
        } else if !text.isValidAccountNumber() {
            errorLabel.text = ValidationErrorMessage.accountInvalid.description()
        } else {
            //customTextField.showImage()
            errorLabel.text = ""
        }
    }
    
     // MARK: reAccount Number
    func validateConfirmAccount(text: String, account: String) {
        if text.isEmptyString() {
            errorLabel.text = ValidationErrorMessage.accountEmpty.description()
            
        } else if !text.isValidAccountNumber() {
            errorLabel.text = ValidationErrorMessage.accountInvalid.description()
        } else if text != account {
            errorLabel.text = ValidationErrorMessage.accountMissmatch.description()
        } else {
            errorLabel.text = ""
        }
    }
    
     // MARK: bank Name
    func validateBankName(text: String) {
        if text.isEmptyString() {
            //customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.bankName.description()
        } else {
            //customTextField.showImage()
            errorLabel.text = ""
        }
    }
    

    // MARK: First Name
    func validateFirstName(text: String) {
        if text.isEmptyString() {
            //customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.firstName.description()
        } else {
            //customTextField.showImage()
            errorLabel.text = ""
        }
    }
    
    // MARK: Parent First Name
    func validateParentFirstName(text: String) {
        if text.isEmptyString() {
            //customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.firstName.description()
        } else {
            //customTextField.showImage()
            errorLabel.text = ""
        }
    }
    
    // MARK: Child First Name
    func validateChildFirstName(text: String) {
        if text.isEmptyString() {
            //customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.firstName.description()
        } else {
            //customTextField.showImage()
            errorLabel.text = ""
        }
    }

    // MARK: Last Name
    func validateLastName(text: String) {
        if text.isEmptyString() {
            //customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.lastName.description()
        } else {
            //customTextField.showImage()
            errorLabel.text = ""
        }
    }
    
    // MARK: Relation With Child
    func validateRelationWithChild(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.relationWithChild.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }

    // MARK: Gender
    func validateGenderName(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.gender.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }

    // MARK: Birthday
    func validateBirthday(text: String) {

        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = "dd-MMM-yyyy"
        let date = inputDateFormatter.date(from: text)
        if text.isEmptyString() || date == nil {
            errorLabel.text = ValidationErrorMessage.birthday.description()
        } else {
            errorLabel.text = ""
        }
    }
    
    
    // MARK: Email
    func validateEmail(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.emailEmpty.description()
        } else if !text.isValidEmailID() {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.emailInvalid.description()
        } else {
            customTextField.showImage()
            errorLabel.text = ""
        }
    }
    
    //MARK: Contact
    func validateContact(text: String) {
                if text.isEmptyString() {
                    customTextField.hideImage()
                    errorLabel.text = ValidationErrorMessage.phoneEmpty.description()
                } else if !text.isValidPhoneNumber() {
                    customTextField.hideImage()
                    errorLabel.text = ValidationErrorMessage.phoneInvalid.description()
                } else {
                    //customTextField.showImage()
                    errorLabel.text = ""
                }
    }

    // MARK: User Name
    func validateUserName(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            correctIcon.backgroundColor = UIColor.clear
            correctIcon.image = nil
            errorLabel.text = ValidationErrorMessage.displayName.description()
        }else if !text.isValidUserName(){
            //customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.displayName.description()
        } else {
            //customTextField.showImage()
            correctIcon.backgroundColor = UIColor.clear
             correctIcon.image = nil
            errorLabel.text = ""
        }
    }

        // MARK: Password
        func validatePassword(text: String) {
            if text.isEmptyString() {
                customTextField.hideImage()
                correctIcon.image = nil
                errorLabel.text = ValidationErrorMessage.passwordEmpty.description()
            } else if !text.isValidPassword() {
                customTextField.hideImage()
                correctIcon.image = nil
                errorLabel.text = ValidationErrorMessage.passwordInvalid.description()
            } else {
                customTextField.showImage()
                correctIcon.image = UIImage(named: "correctIcon")
                errorLabel.text = ""
            }
        }
    
    // MARK: Profile Password
    func validateProfilePassword(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            correctIcon.image = nil
            errorLabel.text = ValidationErrorMessage.passwordEmpty.description()
        } else if !text.isValidPassword() {
            customTextField.hideImage()
            correctIcon.image = nil
            errorLabel.text = ValidationErrorMessage.passwordInvalid.description()
        } else {
            customTextField.showImage()
            //correctIcon.image = UIImage(named: "correctIcon")
            errorLabel.text = ""
        }
    }
    
    // MARK:  New Password
    func validateNewPassword(text: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            correctIcon.image = nil
            errorLabel.text = ValidationErrorMessage.passwordEmpty.description()
        } else if !text.isValidPassword() {
            customTextField.hideImage()
            correctIcon.image = nil
            errorLabel.text = ValidationErrorMessage.passwordInvalid.description()
        } else {
            customTextField.showImage()
            correctIcon.image = UIImage(named: "correctIcon")
            errorLabel.text = ""
        }
    }
   
    
    // MARK: Confirm Password
    
    func validateConfirmPassword(text: String, password: String) {
        if text.isEmptyString() {
            customTextField.hideImage()
            correctIcon.image = nil
            errorLabel.text = ValidationErrorMessage.passwordEmpty.description()
            
        } else if !text.isValidPassword() {
            customTextField.hideImage() 
            correctIcon.image = nil
            //createUserViewControllerDelegate.showHideNextButton(false)
            errorLabel.text = ValidationErrorMessage.passwordInvalid.description()
        } else if text != password {
            customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.passwordMismatch.description()
            customTextField.textColor = themeRedColor
             correctIcon.image = UIImage(named: "incorrectIcon")
//            createUserViewControllerDelegate.showHideNextButton(false)
            //customTextField.icon = "incorrectIcon"
            customTextField.showImage()
        } else {
            //customTextField.showImage()
            customTextField.textColor = theamBlackColor
            //customTextField.icon = "correctIcon"
            correctIcon.image = UIImage(named: "correctIcon")
            customTextField.showImage()
//            createUserViewControllerDelegate.showHideNextButton(true)
            errorLabel.text = ""
        }
    }
    
    
    // MARK: Postal Code
//    func validatePostalCode(text: String){
//        if text.isEmptyString(){
//            //customTextField.hideImage()
//            errorLabel.text = ValidationErrorMessage.postalCodeEmpty.description()
//        }else {
//            customTextFieldForPostal.showImage()
//            errorLabel.text = ""
//        }
//    }
    
    func validatePostalCode(text: String) {
        if text.isEmptyString() {
            //customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.postalCodeEmpty.description()
        } else if !text.isValidPostalCode(){
            //customTextField.hideImage()
            errorLabel.text = ValidationErrorMessage.postalCode.description()
        } else {
//            customTextFieldForPostal.showImage()
            errorLabel.text = ""
        }
    }
    
    // MARK: Address1
    func validateAddress1(text: String){
        if text.isEmptyString(){
            customTextFieldForPostal.hideImage()
            errorLabel.text = ValidationErrorMessage.address1.description()
        }else {
            //customTextField.showImage()
            errorLabel.text = ""
        }
    }
    
    // MARK: Address2
    func validateAddress2(text: String){
        if text.isEmptyString(){
            customTextFieldForPostal.hideImage()
            errorLabel.text = ValidationErrorMessage.address2.description()
        }else {
            customTextFieldForPostal.showImage()
            errorLabel.text = ""
        }
    }
    
    // MARK: MRT/LRT
    func validateMRT(text: String){
        if text.isEmptyString(){
            customTextFieldForPostal.hideImage()
            errorLabel.text = ValidationErrorMessage.mrtlrtstation.description()
        }else {
            //customTextField.showImage()
            errorLabel.text = ""
        }
    }
    

//    // MARK: Country
//    func validateCountry(text: String) {
//        if text.isEmptyString() {
//            errorLabel.text = ValidationErrorMessage.country.description()
//        } else {
//            errorLabel.text = ""
//        }
//    }

    func validateConfirmPhone(text: String, phone: String) {
//        if text.isEmptyString() {
//            customTextField.hideImage()
//            errorLabel.text = ValidationErrorMessage.phoneEmpty.description()
//        } else if !text.isValidPhoneNumber() {
//            customTextField.hideImage()
//            errorLabel.text = ValidationErrorMessage.phoneInvalid.description()
//        } else if text != phone {
//            customTextField.hideImage()
//            errorLabel.text = ValidationErrorMessage.phoneMismatch.description()
//        } else {
//            customTextField.showImage()
//            errorLabel.text = ""
//        }
    }
//
//    // MARK: Student Name
//    func validateStudentName(text: String) {
//        if text.isEmptyString() {
//            customTextField.hideImage()
//            errorLabel.text = ValidationErrorMessage.studentName.description()
//        } else {
//            customTextField.showImage()
//            errorLabel.text = ""
//        }
//    }
//
//
}
