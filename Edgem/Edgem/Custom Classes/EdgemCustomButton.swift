//
//  EdgemCustomButton.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 12/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit

//@IBDesignable
class EdgemCustomButton: UIButton {
    
    @IBInspectable
    public var customButtonType: Int = 0 {
        didSet {
            setUpButton()
        }
    }
    
    @IBInspectable
    public var themeColor: UIColor? = UIColor(red: 57/255.0, green: 163/255.0, blue: 180/255.0, alpha: 1) {
        didSet {
            setUpButton()
        }
    }
    
    // MARK: Initializers
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: Custom layout overrides
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.layer.shadowPath = UIBezierPath(roundedRect:
            self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 5
        self.layer.masksToBounds = false
        self.layer.shadowOpacity = 0.12
        
    }
    
    func setUpButton() {
        let buttonHeight: CGFloat = bounds.size.height
        
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = buttonHeight/2
        setButtonStyle()
    }
    
    func setButtonStyle() {
        switch customButtonType {
        case CustomButtonType.clear.rawValue:
            self.backgroundColor = UIColor.clear
            self.setTitleColor(UIColor.white, for: .normal)
            break
        case CustomButtonType.white.rawValue:
            self.backgroundColor = UIColor.white
            self.setTitleColor(themeColor, for: .normal)
            break
        case CustomButtonType.dual.rawValue:
            self.backgroundColor = UIColor.clear
            
            self.addTarget(self, action: #selector(holdRelease), for: UIControl.Event.touchUpInside)
            self.addTarget(self, action: #selector(holdRelease), for: UIControl.Event.touchDragExit);
            self.addTarget(self, action: #selector(holdDown), for: UIControl.Event.touchDown)
            
            break
        default:
            break
        }
    }
    
    //target functions
    @objc func holdDown() {
        self.backgroundColor = UIColor.white
        self.setTitleColor(themeColor, for: .normal)
    }
    
    @objc func holdRelease() {
        self.backgroundColor = UIColor.clear
        self.setTitleColor(UIColor.white, for: .normal)
    }
}
