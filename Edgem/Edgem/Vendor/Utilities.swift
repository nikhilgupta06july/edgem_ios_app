//
//  Utilities.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 02/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation
import RxSwift
import Reachability


class Utilities: NSObject {
    
    static let shared = Utilities()
    var reachability: Reachability?
    
    fileprivate override init() {
        super.init()
}
    
    class func showHUD(forView view: UIView!, excludeViews: [UIView?]) {
        DispatchQueue.main.async {
            let hudView = MBProgressHUD.showAdded(to: view, animated: true)
            view.bringSubviewToFront(hudView)
            if !excludeViews.isEmpty {
                for subView in excludeViews {
                    view.bringSubviewToFront(subView!)
                }
            }
        }
    }
    
    class func hideHUD(forView view: UIView!) {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: view, animated: true);
        }
    }
    
    class func logoutUser() {
        DispatchQueue.main.async(execute: {
            UserStore.shared.isLoggedIn = false
            UserStore.shared.userID = nil
            UserStore.shared.userEmail = nil
            UserStore.shared.bitlyURL = nil
            AppVariables.emailAndContact?.contact = nil
            AppVariables.emailAndContact?.email = nil
            //UserStore.shared.isNotificationsStatusSet = false
            
        })
    }
    
    class func getTimeZone() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "Z"
        var timeZone = dateFormatter.string(from: Date())
        timeZone.insert(":", at: timeZone.index(timeZone.startIndex, offsetBy: 3))
        return timeZone
    }
    
    //MARK:- Reachability
    fileprivate func initializeReachability() {
        let reachability = Reachability()
        self.reachability = reachability
        
        do {
            try self.reachability!.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func setupReachability() {
        self.initializeReachability()
    }
    
    func isNetworkReachable() -> Bool {
        if let netReachability = reachability {
            return netReachability.connection != .none
        } else {
            self.setupReachability()
            return false
        }
    }
    
    //MARK:- Date Formatter
    
    //"dd/MM/yyyy"
    class func getFormattedDate(_ date: Date, dateFormat: String = "dd/MM/yyyy") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
    
    class func getFormatedDate(_ date: Date, dateFormat: String = "dd/MM/yyyy") -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
    
    
    
    class func localToUTC(date:String, fromFormat: String, toFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        //dateFormatter.date
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = toFormat
        
        return dateFormatter.string(from: dt!)
    }
    
    class func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat
        
        return dt != nil ? dateFormatter.string(from: dt!) : ""
    }
    
    func configureToShowDateFormatter(for date: Date)->String {
        let formatter = DateFormatter()
        switch true {
        case Calendar.current.isDateInToday(date) || Calendar.current.isDateInYesterday(date):
            formatter.doesRelativeDateFormatting = true
            formatter.dateStyle = .short
            formatter.timeStyle = .short
        case Calendar.current.isDate(date, equalTo: Date(), toGranularity: .weekOfYear):
            formatter.dateFormat = "EEEE h:mm a"
        case Calendar.current.isDate(date, equalTo: Date(), toGranularity: .year):
            formatter.dateFormat = "E, d MMM, h:mm a"
        default:
            formatter.dateFormat = "MMM d, yyyy, h:mm a"
        }
        return formatter.string(from: date)
    }
        
    class func sendDeviceToken(_ deviceToken: String?) {
        
//        guard Utilities.shared.isNetworkReachable() else {
//            return
//        }
        
        var userID = 0
        if UserStore.shared.isLoggedIn{
            if let USER_ID = UserStore.shared.userID  {
                userID = USER_ID
            }
        }
        
        let deviceName = UIDevice.current.name
        guard !UserStore.shared.deviceToken.isEmpty || deviceToken != nil else {
            return
        }
        
        UIPasteboard.general.string = UserStore.shared.deviceToken
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
            let notificationObserver = ApiManager.shared.apiService.toggleNotifications(true, deviceName: deviceName, userId: String(userID), deviceId: uniqueIdentifier, deviceToken: UserStore.shared.deviceToken)
            let _ = notificationObserver.subscribe(onNext: {(message) in
            
            UserStore.shared.isNotificationsStatus = true
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
        })
    }
    
    class func hasPassword() -> Bool {
        let email = UserStore.shared.touchIDEmail ?? ""
        if email.isEmpty {
            return false
        }
        
        do {
            let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                    account: email,
                                                    accessGroup: KeychainConfiguration.accessGroup)
            let keychainPassword = try passwordItem.readPassword()
            if !keychainPassword.isEmpty {
                return true
            } else {
                return false
            }
        } catch {
            return false
        }
    }

}



