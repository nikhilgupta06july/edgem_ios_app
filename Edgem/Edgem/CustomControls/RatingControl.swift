//
//  RatingControl.swift
//  Rating
//
//  Created by Hipster on 25/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol RatingControlDelegate{
    func starRating(_ rating: Double)
    func fromStarRating(_ from: Bool)
}

@IBDesignable class RatingControl: UIStackView {
    
    // MARK: ------- Properties
    
    private var ratingButtons = [UIButton]()
    var ratingControlDelegate: RatingControlDelegate?
    
    var rating: Double = 0{
        didSet{
            updateButtonSelectionStates()
        }
    }
    
    @IBInspectable var starSize: CGSize = CGSize(width: 40.0, height: 40.0){
        didSet{
            setupButtons()
        }
    }
    @IBInspectable var starCount: Int = 5{
        didSet{
            setupButtons()
        }
    }
    
    
    // MARK: ------- Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupButtons()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        
        setupButtons()
    }
    
    // MARK: --------- Button Action
    
    @objc func ratingButtonTapped(button: UIButton){
        ratingControlDelegate?.fromStarRating(true)
        guard let index = ratingButtons.index(of: button) else{
                  fatalError("The button, \(button), is not in the ratingButtons array: \(ratingButtons)")
        }
        
        // Calculate the rating of the selected button
        
        let selectedRating = Double(index) + 1
        if selectedRating == rating{
            rating = 0
        }else{
            rating = selectedRating
        }
        
    }
    
    // MARK: --------- Private Methods
    
    func returnRating()->Double{
        return rating
    }
    
    private func setupButtons(){
        
        // Clear any existing buttons
        
        for button in ratingButtons{
            removeArrangedSubview(button)
            button.removeFromSuperview()
        }
        
        ratingButtons.removeAll()
        
        // Load button images
        
        let bundle = Bundle(for: type(of: self))
        let filledStar = UIImage(named: "starActive", in: bundle, compatibleWith: self.traitCollection)
        let halfFilledStar = UIImage(named: "starHalfActive", in: bundle, compatibleWith: self.traitCollection)
        let emptyStar = UIImage(named: "starInActive", in: bundle, compatibleWith: self.traitCollection)
        
        for _ in 0..<starCount{
            
            // Create the buttons
            
            let button  = UIButton()
           
            // Set the button images
            
            button.setImage(emptyStar, for: .normal)
            button.setImage(halfFilledStar, for: .selected)
            button.setImage(filledStar, for: .selected)
            button.setImage(filledStar, for: .highlighted)
            button.setImage(filledStar, for: [.highlighted, .selected])
            
            // Add constraints on button
            
            button.translatesAutoresizingMaskIntoConstraints = false
            button.widthAnchor.constraint(equalToConstant: starSize.width).isActive = true
            button.heightAnchor.constraint(equalToConstant: starSize.height).isActive = true
            
            // Setup the button action
            
            button.addTarget(self, action: #selector(RatingControl.ratingButtonTapped(button:)), for: .touchUpInside)
            
            // Add the button to stackview
            
            addArrangedSubview(button)
            
            // Add tne new button to the rating button array
            
            ratingButtons.append(button)
            }
        
        updateButtonSelectionStates()
        }
    
    private func updateButtonSelectionStates() {
        for (index, button) in ratingButtons.enumerated(){
            button.isSelected = Double(index) < rating
        }
        ratingControlDelegate?.starRating(rating)
        
    }
    
}
