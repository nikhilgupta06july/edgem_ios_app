//
//  ImageCache.swift
//  Edgem
//
//  Created by Namespace on 29/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

let imageCache = ImageCache()

class ImageCache: NSCache<AnyObject, AnyObject> {
    func add(_ image: UIImage, forKey key: String) {
        setObject(image, forKey: key as AnyObject)
    }
    func image(forKey key: String) -> UIImage? {
        guard let image = object(forKey: key as AnyObject) as? UIImage else {return nil}
        return image
    }
}

struct ImageProvider: RequestImages {
    fileprivate let downloadQueue = DispatchQueue(label: "Images cache", qos: DispatchQoS.background)
    //MARK: - Fetch image from URL and Images cache
    func loadImages(from url: URL, completion: @escaping (_ image: UIImage) -> Void) {
        downloadQueue.async(execute: { () -> Void in
            do{
                let data = try Data(contentsOf: url)
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async { completion(image) }
                } else { print("Could not decode image") }
            }catch { print("Could not load URL: \(url): \(error)") }
        })
    }
}

protocol RequestImages {}
extension RequestImages where Self == ImageProvider{
    func requestImage(from url: URL, completion: @escaping (_ image: UIImage) -> Void){
        //calling here another function and returning data completion
         //>>>>>
         //loadImages(from: url, completion: completion)
    }
}

class GetImageFromServer: NSObject{
    
    class func getImage(imageURL url: String, completion: @escaping (_ image: UIImage) -> Void){
        
        let imageProvider = ImageProvider()
        if let image = imageCache.image(forKey: url){
            completion(image)
        }else{
            if let i_url = URL(string: url) {
                imageProvider.requestImage(from: i_url) { (image) in
                    imageCache.add(image, forKey: url)
                    completion(image)
                }
            }
        }
    }
    
}
