//
//  AppVariables.swift
//  Edgem
//
//  Created by Namespace on 17/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import EasyTipView

struct AppVariables{
    
    static var chatListUserDetail: BookingDetails?
    static var makeOfferRequest: MakeOfferRequst?
    static var emailAndContact: EmailAndContact?
    static var update_Parent_EmailAndContact_Or_Child: String?
    static var emailAndContact_parent: EmailAndContact?
    static var bookMarkTutorObject: Tutor?
    static var banks: Banks?
    static var user_ID: String?
    
    static func isCreditCardBankDetail(_ vc: BaseViewController?) -> Bool {
        
        if AppDelegateConstant.user?.userType == tutorType {
            
            if AppDelegateConstant.user?.userBankDetail == nil{
                //vc?.showSimpleAlertWithMessage(CustomAlertMessages.add_bank_details_err.description())
                vc?.showSimpleAlertWithMessgeAndNavigateToAddBankDetailVC(CustomAlertMessages.add_bank_details_err.description())
                
                return false
            }
            
            if AppDelegateConstant.user?.paymentCard?.count == 0 || (AppDelegateConstant.user?.paymentCard == nil){
                //vc?.showSimpleAlertWithMessage(CustomAlertMessages.add_credit_card_err.description())
                vc?.showSimpleAlertWithMessgeAndNavigateToAddCardVC(CustomAlertMessages.add_credit_card_err.description())
                return false
            }
            
            
//            if AppDelegateConstant.user?.userBankDetail == nil && (AppDelegateConstant.user?.paymentCard?.count == 0 || AppDelegateConstant.user?.paymentCard == nil){
//                vc?.showSimpleAlertWithMessage(CustomAlertMessages.add_bank_details_credit_card_err.description())
//                return false
//            }else
//                if AppDelegateConstant.user?.userBankDetail == nil{
//                vc?.showSimpleAlertWithMessage(CustomAlertMessages.add_bank_details_err.description())
//                return false
//            }
//                else
//                    if AppDelegateConstant.user?.paymentCard?.count == 0 || (AppDelegateConstant.user?.paymentCard == nil){
//                 //vc?.showSimpleAlertWithMessage(CustomAlertMessages.add_credit_card_err.description())
//                vc?.showSimpleAlertWithMessgeAndNavigateToAddCardVC(CustomAlertMessages.add_credit_card_err.description())
//                return false
//            }
            
        }else{
            if AppDelegateConstant.user?.paymentCard?.count == 0 || AppDelegateConstant.user?.paymentCard == nil{
                vc?.showSimpleAlertWithMessgeAndNavigateToAddCardVC(CustomAlertMessages.add_credit_card_err.description())
                return false
            }
        }
        
        return true
    }

}


