//
//  QualificationsListTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 29/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class QualificationsListTableViewCell: UITableViewCell {
    
    var selectedQualificationProperty: ((_ section: Int,_ row:Int, _ status: Bool, _ qaulification: Qualification?, _ counter: String)->Void)?
    
    var counterValue: (( _ counter: String)->Void)?
    
    var index: Int? = 0
    var section: Int?
    var row: Int?
    
    var userQualification: Qualification?
    
    var isSelectQualificationBtnSelected: Bool = false{

        didSet{
            
              self.userQualification?.isSelected = isSelectQualificationBtnSelected
            
             if isSelectQualificationBtnSelected == false{
                
                yearsOfExperiencesContainerView.isHidden = true
                
                yearOfExpsLabel.isHidden = true
                
                incrementBtnContainerStackView.isHidden = true
                
                self.qualificationsContainerView.backgroundColor = UIColor.white
                
                self.selectQualificationBtn.setImage(UIImage(named: "deselected"), for: .normal)
                
                selectedQualificationProperty?(self.section!, self.row!,  isSelectQualificationBtnSelected, userQualification, self.counterLabel.text!)
                
            }else{
                
                yearsOfExperiencesContainerView.isHidden = false
                
                yearOfExpsLabel.isHidden = false
                
                incrementBtnContainerStackView.isHidden = false
                
                self.qualificationsContainerView.backgroundColor = cellBackgroundColor
                
                self.selectQualificationBtn.setImage(UIImage(named:"selected"), for: .normal)
                
                selectedQualificationProperty?(self.section!, self.row!,  isSelectQualificationBtnSelected, userQualification,self.counterLabel.text!)
                
            }
            
        }
        
    }
    
     // MARK: - @IBOutlets
    
//    @IBOutlet weak var titleLabel: UILabel!
//    @IBOutlet weak var titleContaineView: UIView!
    @IBOutlet weak var incrementBtnContainerStackView: UIStackView!
    @IBOutlet weak var qualificationsContainerView: UIView!
    @IBOutlet weak var qualificationNameLabel: UILabel!
    @IBOutlet weak var qualificationBorderView: UIView!
    @IBOutlet weak var yearOfExpsLabel: UILabel!
    @IBOutlet weak var yearsOfExperiencesContainerView: UIView!
    @IBOutlet weak var selectQualificationBtn: UIButton!
    @IBOutlet weak var yearOfExperienceLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var yearOFExperienceContinerViewHeight: NSLayoutConstraint!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetUP()
    }
    
    // MARK: - Helping Functions
    
    func configureCell(with index: Int, section: Int, row: Int, qualification:Qualification, title: String){
        
        
        self.section = section
        self.row = row
        self.index = index
        self.userQualification = qualification
        
            if qualification.isSelected == true {
                yearsOfExperiencesContainerView.isHidden = false
                yearOfExpsLabel.isHidden = false
                incrementBtnContainerStackView.isHidden = false
                
               // yearOFExperienceContinerViewHeight.constant = 124.5
                
                self.qualificationsContainerView.backgroundColor = cellBackgroundColor
                self.selectQualificationBtn.setImage(UIImage(named:"selected"), for: .normal)
            }else{
                yearsOfExperiencesContainerView.isHidden = true
                yearOfExpsLabel.isHidden = true
                incrementBtnContainerStackView.isHidden = true
                
                // yearOFExperienceContinerViewHeight.constant = 0
                
                self.qualificationsContainerView.backgroundColor = UIColor.white
                self.selectQualificationBtn.setImage(UIImage(named: "deselected"), for: .normal)
            }
        counterLabel.text = qualification.totalExperience
        qualificationNameLabel.text = qualification.name
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

    }
    
    // MARK: - @IBActions
    
    @IBAction func didTapSelectButton(_ sender: UIButton) {
                 isSelectQualificationBtnSelected = sender.hasImage(named: "selected", for: .normal) == true ? false : true
    }
    
    
    @IBAction func didTapIncrementBtn(_ sender: UIButton) {
        
        if let yearsOfExperience = Int( counterLabel.text!){

            if sender.tag == 1{
                // decrement
                if yearsOfExperience > 0{
                     counterLabel.text = "\(yearsOfExperience-1)"
                }
            }else{
                // increment
                if yearsOfExperience < 25{
                    counterLabel.text = "\(yearsOfExperience+1)"
                }
                
            }
          counterValue?(self.counterLabel.text!)
        }

    }
    
}

extension QualificationsListTableViewCell{
    
    class func cellIdentifire() -> String{
        return "QualificationsListTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 55
    }
    
    fileprivate func initialSetUP() {
        // Initialization code
        
        // title
        //        titleLabel.font = UIFont(name: QuicksandBold, size: 21)
        //        titleLabel.textColor = theamBlackColor
        
        // qualification name
        qualificationNameLabel.font = UIFont(name: QuicksandMedium, size: 18)
        qualificationNameLabel.textColor = theamBlackColor
        
        // qualification border
        qualificationBorderView.backgroundColor = theamBorderGrayColor
        
        // year of experience
        yearOfExperienceLabel.font = UIFont(name: QuicksandRegular, size: 15)
        yearOfExperienceLabel.textColor = theamBlackColor
        
        // counterLabel
        counterLabel.font = UIFont(name: QuicksandMedium, size: 30)
        counterLabel.textColor = theamBlackColor
    }
    
}




