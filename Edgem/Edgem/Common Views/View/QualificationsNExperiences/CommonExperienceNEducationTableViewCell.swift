//
//  CommonExperienceNEducationTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 30/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class CommonExperienceNEducationTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    var did_Tap_Add_Remove_Experience: ((Int, Int, Int, Int)->())?
    var section: Int?
    var row: Int?
    var from: Int?
    
    
     // MARK: - @IBOutlets
    
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellIndex: UILabel!
    
    @IBOutlet weak var firstTextFieldTitleLabel: UILabel!
    @IBOutlet weak var secondTextFieldTitleLabel: UILabel!
    @IBOutlet weak var thirdTextFieldTitleLabel: UILabel!
    @IBOutlet weak var fourthTextFieldTitleLabel: UILabel!
    
    @IBOutlet weak var addCellBtn: UIButton!
    @IBOutlet weak var removeCellBtn: UIButton!
    
    @IBOutlet weak var upperBorderView: UIView!
    @IBOutlet weak var borderView: UIView!
    
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var textField4: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func cellIdentifire() ->String{
        return "CommonExperienceNEducationTableViewCell"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        textField1.text = ""
        textField2.text = ""
    }
   
    func configureCellWithExperience(with experienceData: Experience, _ title: String, _ index: Int, lastIndex : Int, section : Int, from: Int ){
        
        row = index-1
        self.section = section
        self.from = from
        
        if index <= lastIndex && lastIndex == 1{
            removeCellBtn.isHidden = true
            addCellBtn.isHidden = false
        }else if index == lastIndex && lastIndex == 5{
            addCellBtn.isHidden = true
            removeCellBtn.isHidden = false
        }else if index < lastIndex && lastIndex < 5{
            addCellBtn.isHidden = true
            removeCellBtn.isHidden = true
        }else if index == lastIndex && lastIndex != 5 && lastIndex != 1{
            removeCellBtn.isHidden = false
            addCellBtn.isHidden = false
        }else{
            addCellBtn.isHidden = true
            removeCellBtn.isHidden = true
        }
        
//        if index == 0 {
//            upperBorderView.isHidden = false
//            borderView.isHidden = true
//        }else if index == lastIndex {
//            upperBorderView.isHidden = true
//            borderView.isHidden = true
//        }else{
//            upperBorderView.isHidden = true
//            borderView.isHidden = true
//        }
        
        if row == 0 {
            upperBorderView.isHidden = false
            borderView.isHidden = true
        }else if index == lastIndex{
            upperBorderView.isHidden = true
            borderView.isHidden = false
        }else{
            upperBorderView.isHidden = true
            borderView.isHidden = true
        }
//        if index == lastIndex{
//            borderView.isHidden = false
//        }else{
//            borderView.isHidden = true
//        }
        
        if index != 1{
            cellTitle.isHidden = true
        }else{
            cellTitle.text = title
            cellTitle.isHidden = false
        }
        
        cellIndex.text = "\(index)."
        
        firstTextFieldTitleLabel.text = "School*".localized
        textField1.attributedPlaceholder = NSAttributedString(string: "Enter your school name".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        textField1.tag = 1
        textField1.text = experienceData.instituteName
        
        secondTextFieldTitleLabel.text = "Subject*".localized
        textField2.attributedPlaceholder = NSAttributedString(string: "Enter your subject name".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        textField2.tag = 2
        textField2.text = experienceData.subjectName
        
        thirdTextFieldTitleLabel.text = "From*".localized
        textField3.attributedPlaceholder = NSAttributedString(string: "YYYY".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        textField3.tag = 3
        textField3.text = experienceData.fromYear
        
        fourthTextFieldTitleLabel.text = "To*".localized
        textField4.placeholder = "YYYY".localized
        textField4.attributedPlaceholder = NSAttributedString(string: "YYYY".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        textField4.tag = 4
        textField4.text = experienceData.toYear
        
    }
    
    func configureCellWithEducation(with educationData: Education, _ title: String, _ index: Int, lastIndex : Int, section : Int, from: Int ){
        
        row = index-1
        self.section = section
        self.from = from
        
        if index <= lastIndex && lastIndex == 1{
            removeCellBtn.isHidden = true
            addCellBtn.isHidden = false
        }else if index == lastIndex && lastIndex == 5{
            addCellBtn.isHidden = true
            removeCellBtn.isHidden = false
        }else if index < lastIndex && lastIndex < 5{
            addCellBtn.isHidden = true
            removeCellBtn.isHidden = true
        }else if index == lastIndex && lastIndex != 5 && lastIndex != 1{
            removeCellBtn.isHidden = false
            addCellBtn.isHidden = false
        }else{
            addCellBtn.isHidden = true
            removeCellBtn.isHidden = true
        }
        
        if row == 0 {
            upperBorderView.isHidden = true
            borderView.isHidden = false
        }
//        else if index == lastIndex{
//            upperBorderView.isHidden = true
//            borderView.isHidden = false
//        }
        else{
            upperBorderView.isHidden = true
            borderView.isHidden = true
        }
        
//        if index == lastIndex{
//            borderView.isHidden = false
//        }else{
//            borderView.isHidden = true
//        }
        
        if index != 1{
            cellTitle.isHidden = true
        }else{
            cellTitle.text = title
            cellTitle.isHidden = false
        }
        
       cellIndex.text = "\(index)."
        
        firstTextFieldTitleLabel.text = "School*".localized
        textField1.attributedPlaceholder = NSAttributedString(string: "Enter your school name".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        textField1.tag = 1
        textField1.text = educationData.instituteName
        
        secondTextFieldTitleLabel.text = "Certificate*".localized
        textField2.attributedPlaceholder = NSAttributedString(string: "Enter your certificate name".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        textField2.tag = 2
        textField2.text = educationData.certificateName
        
        thirdTextFieldTitleLabel.text = "From*".localized
        textField3.attributedPlaceholder = NSAttributedString(string: "YYYY".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        textField3.tag = 3
        textField3.text = educationData.fromYear
        
        fourthTextFieldTitleLabel.text = "To*".localized
        textField4.placeholder = "YYYY".localized
        textField4.attributedPlaceholder = NSAttributedString(string: "YYYY".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        textField4.tag = 4
        textField4.text = educationData.toYear
    }
        
    @IBAction func didTapAdd_Remove_Btn(_ sender: UIButton) {
         did_Tap_Add_Remove_Experience?(sender.tag, section!, row!, from!)
    }
    
}
