//
//  QualificationListWithoutExperienceTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 02/08/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class QualificationListWithoutExperienceTableViewCell: UITableViewCell {
    
    var userQualification: Qualification?
    
    var row: Int!
    var section: Int!
    
    var selectedQualificationProperty: (( _ status: Bool, _ qaulification: Qualification?, _ row: Int, _ sectin: Int )->Void)?
    
    var isSelectQualificationBtnSelected: Bool = false{
        
        didSet{
            
            self.userQualification?.isSelected = isSelectQualificationBtnSelected
            
            if isSelectQualificationBtnSelected == false{
                
                self.qualificationContainerView.backgroundColor = UIColor.white
                
                self.selectQualificationBtn.setImage(UIImage(named: "deselected"), for: .normal)
                
                 selectedQualificationProperty?( isSelectQualificationBtnSelected, userQualification, self.row, self.section)
                
            }else{
                
                self.qualificationContainerView.backgroundColor = cellBackgroundColor
                
                self.selectQualificationBtn.setImage(UIImage(named:"selected"), for: .normal)
                
                 selectedQualificationProperty?( isSelectQualificationBtnSelected, self.userQualification, self.row, self.section)
            }
            
        }
        
    }

    @IBOutlet weak var qualificationContainerView: UIView!
    @IBOutlet weak var qualificationName: UILabel!
    @IBOutlet weak var selectQualificationBtn: UIButton!
    @IBOutlet weak var qualificationBorderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // qualification name
        qualificationName.font = UIFont(name: QuicksandMedium, size: 18)
        qualificationName.textColor = theamBlackColor
        
        // qualification border
        qualificationBorderView.backgroundColor = theamBorderGrayColor
    }
    
    func configureCell(qualification: Qualification, row: Int, section: Int){
        
        self.row = row
        self.section = section
        
        userQualification = qualification
        
        qualificationName.text = qualification.name
        
        if qualification.isSelected == true{
            
            self.qualificationContainerView.backgroundColor = cellBackgroundColor
            
            self.selectQualificationBtn.setImage(UIImage(named:"selected"), for: .normal)
            
        }else{
            
            self.qualificationContainerView.backgroundColor = UIColor.white
            
            self.selectQualificationBtn.setImage(UIImage(named: "deselected"), for: .normal)
            
        }
    }
    
    @IBAction func didSelectQualificationBtn(_ sender: UIButton) {
         isSelectQualificationBtnSelected = sender.hasImage(named: "selected", for: .normal) == true ? false : true
    }

}

extension QualificationListWithoutExperienceTableViewCell{
    
    class func cellIdentifire() -> String{
        return "QualificationListWithoutExperienceTableViewCell"
    }
    
    class func cellHeight() ->CGFloat {
        return 55
    }
    
}
