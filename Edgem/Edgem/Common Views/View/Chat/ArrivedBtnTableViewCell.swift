//
//  ArrivedBtnTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 15/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class ArrivedBtnTableViewCell: UITableViewCell {
    
    var chatViewController: ChatViewController?
    var msgID: String = ""
    var message: ChatMessage?
    var enableButton: Bool = true
    var row: Int!
    var section: Int!
    var gradientLayer = CAGradientLayer()
    
    @IBOutlet weak var containeView: UIView!
    @IBOutlet weak var endBtn: UIButton!
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var seenStatusImage: UIImageView!
     @IBOutlet weak var timeLabel: UILabel!
    
    class func cellIdentifire() -> String {
        return "ArrivedBtnTableViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        gradientLayer.frame = containeView.bounds
        endBtn.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    fileprivate func gradientColor() {
        endLabel.backgroundColor = .clear
        endLabel.textColor = .white
        
        gradientLayer.colors = [UIColor(red: 152/255, green: 212/255, blue: 64/255, alpha: 1).cgColor, UIColor(red: 30/255, green: 170/255, blue: 45/255, alpha: 1).cgColor]
        
        
        containeView.backgroundColor = .clear
        containeView.layer.shadowColor = UIColor.black.cgColor
        containeView.layer.shadowOpacity = 0.7
        containeView.layer.shadowOffset = CGSize.zero
        containeView.layer.shadowRadius = 5
        //containeView.layer.shouldRasterize = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = cellBackgroundColor
        self.contentView.backgroundColor = cellBackgroundColor
        
        endBtn.layer.cornerRadius = 5
        endLabel.layer.cornerRadius = 5
        
        if message?.buttonFlag == 1 || enableButton == false{
        
            self.darkColor()

        }else{
            
            gradientColor()
            
        }
        
        endLabel.clipsToBounds = true
        endBtn.clipsToBounds = true
        
    }
    
    func configureCell(with message: ChatMessage){
        self.message = message
        self.msgID = "\(message.msgID)"
        self.seenStatusImage.image = message.isRead == 1 ? #imageLiteral(resourceName: "seen_image") : #imageLiteral(resourceName: "unseen_image")
        
        if message.buttonFlag == 1 || enableButton == false{
            endBtn.isUserInteractionEnabled = false
        }else{
            endBtn.isUserInteractionEnabled = true
        }
        
        if let time = message.sendedOn{
            self.timeLabel.text = Utilities.UTCToLocal(date: time, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "h:mm a")
        }
        
    }
    
    fileprivate func darkColor() {
        gradientLayer.colors = [UIColor(red: 244/255, green: 0/255, blue: 0/255, alpha: 1).cgColor]
        
        endLabel.backgroundColor = cellBackgroundColor
        endLabel.layer.borderWidth = 1
        endLabel.layer.borderColor = staticMessageCellBorderColor.cgColor
        endLabel.textColor = theamGrayColor
        endLabel.clipsToBounds = true
        
        containeView.backgroundColor = .clear
        containeView.layer.shadowColor = UIColor.clear.cgColor
        containeView.layer.shadowOpacity = 0.0
        containeView.layer.shadowOffset = CGSize.zero
        containeView.layer.shadowRadius = 0.0
    }
    
    @IBAction func didTapArrivedBtn(_ sender: UIButton) {
        let status: Bool!
        
        if sender.isSelected == false {
            sender.isSelected = true
            status = true
            
            darkColor()
            endBtn.isUserInteractionEnabled = false
            
        }else{
            sender.isSelected = false
             status = false
        }
        chatViewController?.didTappedArriveButton(status,self.msgID, self, row, section)
    }
    
}
