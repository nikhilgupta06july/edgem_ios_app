//
//  StudentAddressTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 10/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit

class StudentAddressTableViewCell: UITableViewCell {
    
    private let locationManager = CLLocationManager()
    let marker = GMSMarker()
    var camera = GMSCameraPosition()

    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mapContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
            mapView.delegate = self
        
//        locationManager.delegate = self
//        locationManager.requestWhenInUseAuthorization()
        
        //initialCameraPosition()
        //self.cameraPosition(1.310520, 103.855700)
    }

    class func cellIdentifire() -> String {
        return "StudentAddressTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = staticMessageCellBorderColor.cgColor
        containerView.layer.cornerRadius = 5
        containerView.clipsToBounds = true
        
        mapContainerView.layer.borderWidth = 1
        mapContainerView.layer.borderColor = staticMessageCellBorderColor.cgColor
        mapContainerView.layer.cornerRadius = 5
        mapContainerView.clipsToBounds = true
        
    }
    
    func configureCell(with message: ChatMessage) {
        
        // username
        if AppDelegateConstant.user?.userType == tutorType{
            if let name = AppDelegateConstant.user?.firstName{
                  self.userName.text = "Hi \(String(describing: name)),"
            }
          
        }
       
        print(message)
        let address = message.conversationMessage
        
        do{
            if let json = address.data(using: String.Encoding.utf8){
                if let jsonData = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String:AnyObject] {
                    print(jsonData)
                    if let addressArr = jsonData["address"] as? NSArray, let addrDict = addressArr[0] as? NSDictionary{
                        var addr1 = ""
                        var addr2 = ""
                        var postalCode = ""
                        var lat = "0"
                        var long = "0"
                        
                        if let address1 = (addrDict["addres_line1"] as? String){
                           addr1 = address1
                        }
                        if let address2 = (addrDict["addres_line2"] as? String){
                            addr2 = address2
                        }
                        if let postCode = (addrDict["postcode"] as? String){
                            postalCode = postCode
                        }
                        if let latt = (addrDict["lattitude"] as? String){
                            lat = latt
                        }
                        if let longg = (addrDict["longitude"] as? String){
                            long = longg
                        }
                        
                        let fulladdress  = addr1 + "\n" + addr2 + "\n" + postalCode
                        self.addressLabel.text = fulladdress
                        
                        self.cameraPosition((lat as NSString).doubleValue, (long as NSString).doubleValue)
                    }
                }
            }
        }catch{
            
        }
    }
    
    fileprivate func cameraPosition(_ lat: Double, _ long: Double){
        camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 17)//12.804883
        mapView.camera = camera
        showMarker(position: camera.target)
    }
    
    func showMarker(position: CLLocationCoordinate2D){
        marker.position = position
       // marker.isDraggable = true
        marker.icon=UIImage(named: "location.pdf")
        marker.map = mapView
    }
    
    fileprivate func openMapINMapKit(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        //[START This code is useful]
        
//        let latitute:CLLocationDegrees =  25.3176
//        let longitute:CLLocationDegrees =  82.9739
//
//        let coordinates = CLLocationCoordinate2DMake(latitute, longitute)
//        let options = [
//            MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving
//        ]
//        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
//        let mapItem = MKMapItem(placemark: placemark)
//        mapItem.name = placemark.title
//        mapItem.openInMaps(launchOptions: options)
        
         //[END This code is useful]
        let directionsURL = "http://maps.apple.com/?saddr=Current%20Location&daddr=daddr=\(Float(latitude)),\(Float(longitude))"
        guard let url = URL(string: directionsURL) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    fileprivate func openMapInGoogleMap(latitude: CLLocationDegrees, longitude: CLLocationDegrees){

            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
            {
                // Open Google map
                UIApplication.shared.open((NSURL(string:
                    "comgooglemaps://?saddr=&daddr=\(Float(latitude)),\(Float(longitude))&directionsmode=driving")! as URL), options: [:], completionHandler: nil)
                
            } else
            {
                // Open Apple map
                openMapINMapKit(latitude: latitude, longitude: longitude)
            }
            
        }
    
    
}

extension StudentAddressTableViewCell: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
      
        let strLat = marker.position.latitude
        let strLong = marker.position.longitude
        
         openMapInGoogleMap(latitude: strLat, longitude: strLong)
        
        return true
    }
    
}
