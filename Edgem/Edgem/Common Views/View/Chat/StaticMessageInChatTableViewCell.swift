//
//  StaticMessageInChatTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 25/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class StaticMessageInChatTableViewCell: UITableViewCell {
    
    // MARK: - Properties

    // MARK: - @IBOutlets
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var alertImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

     // MARK: - Helping Functions
    
    class func cellIdentifire() -> String {
        return "StaticMessageInChatTableViewCell"
    }
    
    func configureCell(withTitle title: String, withDescription description: String){
        cellTitle.text = title
        descriptionLabel.text = description
    }
    
    func configureCell(with message: ChatMessage){
        
        if AppDelegateConstant.user?.ID == message.senderID {
            cellTitle.text = "Woohoo you made an offer!".localized
            descriptionLabel.text = "Once a deal is made, remember to get your offer accepted. It’s like a handshake deal, Plus you’ll then be able to leave review for each other.".localized
        }else{
            cellTitle.text =  "Woohoo you got an offer!".localized
             descriptionLabel.text  = "Once a deal is made, remember to get your offer accepted. It’s like a handshake deal, Plus you’ll then be able to leave review for each other.".localized
            
        }

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = staticMessageCellBorderColor.cgColor
        containerView.layer.cornerRadius = 5
        containerView.clipsToBounds = true
        
    }
    
}
