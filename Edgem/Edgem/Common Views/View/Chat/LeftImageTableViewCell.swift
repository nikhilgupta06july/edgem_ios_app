//
//  LeftImageTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 23/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class LeftImageTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    var docImage: UIImage?
    var chatViewController: ChatViewController?

    // MARK: - @IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var msgImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var seenImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        activityIndicator.hidesWhenStopped = true
        
    }
    
    class func cellIdentifire() -> String{
        return "LeftImageTableViewCell"
    }
    
    func configureCell(_ message: ChatMessage){
        // profile image
        if let imageURL = message.realImage{
            GetImageFromServer.getImage(imageURL: imageURL) { (profileImage) in
                self.profileImageView.image = profileImage
            }
        }
        // time
        if let time = message.sendedOn{
            self.timeLabel.text = Utilities.UTCToLocal(date: time, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "h:mm a")
        }
        
        // message image
        if let localImage = message.localImage {
            self.msgImageView.image = localImage
        }else{
            self.loadImages(message)
        }
    }
    
    @objc func handleZoomTap(tapGesture: UITapGestureRecognizer){
        let imageView = tapGesture.view as? UIImageView
        self.chatViewController?.performZoomInForStartingImage(imageView!)
    }
    
    fileprivate func loadImages(_ message: ChatMessage)   {
     
        let imageProvider = ImageProvider()
        
        if let image = imageCache.image(forKey: "\(String(describing: message.conversationMessage))"){
            //docImage = image
             msgImageView.image = image           
        }else{
            
            let imageURL = message.conversationMessage
            
            if imageURL.isEmpty == false{
                activityIndicator.startAnimating()
                if let url = URL(string: imageURL) {
                    imageProvider.requestImage(from: url) { (image) in
                        self.activityIndicator.stopAnimating()
                        imageCache.add(image, forKey: "\(String(describing: message.conversationMessage))")
                        self.msgImageView.image = image
                        //self.docImage = image
                    }
                }
            }
            
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        msgImageView.image = nil
        profileImageView.image = nil
        timeLabel.text = ""
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundColor = cellBackgroundColor
        self.contentView.backgroundColor = cellBackgroundColor
        
        self.selectionStyle = .none
        
        profileImageView.layer.cornerRadius = profileImageView.bounds.width/2
        profileImageView.clipsToBounds = true
        
        imageContainerView.layer.cornerRadius = 5
        imageContainerView.clipsToBounds = true
        
        msgImageView.isUserInteractionEnabled = true
        msgImageView.contentMode = .scaleAspectFill
        msgImageView.layer.cornerRadius = 5
        msgImageView.layer.masksToBounds = true
        
        msgImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomTap)))
        
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 15
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.17
        containerView.layer.shadowRadius = 2.7
        containerView.layer.shadowOffset = CGSize(width: 6, height: 6)
        if #available(iOS 11.0, *) {
            containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
    }
    
}
