//
//  ChatCustomButtomTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 14/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class ChatCustomButtomTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    var didTapPaymentBtn: ((Bool, String?, Int, Int)->Void)?
    var title = ""
    var msgID: String?
    var message: ChatMessage?
    var enableButton: Bool = true
    
    var payBtnRow: Int?
    var payBtnSection: Int?
    // MARK: - @IBOutlets
    
    @IBOutlet weak var paymentBtn: UIButton!
    @IBOutlet weak var seenStatusImage: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var btnContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(with message: ChatMessage,_ row: Int, _ section: Int){
        
        self.payBtnRow = row
        self.payBtnSection = section
        
        self.message = message
//        if let payment = message.conversationMessage{
        self.msgID = "\(message.msgID)"
        
        if let time = message.sendedOn{
            self.timeLabel.text = Utilities.UTCToLocal(date: time, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "h:mm a")
        }
            title = message.conversationMessage
            paymentBtn.setTitle(title, for: .normal)
//        }
        if message.buttonFlag == 1 || enableButton == false{
            paymentBtn.isUserInteractionEnabled = false
            
            paymentBtn.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
            paymentBtn.titleLabel?.textColor = theamGrayColor
            paymentBtn.layer.borderWidth = 1
            paymentBtn.layer.borderColor = staticMessageCellBorderColor.cgColor
            paymentBtn.clipsToBounds = true
            
        }else{
            
            paymentBtn.isUserInteractionEnabled = true
            paymentBtn.backgroundColor = theamAppColor
            
            paymentBtn.backgroundColor = UIColor(red: 255/255, green: 210/255, blue: 113/255, alpha: 1)
            paymentBtn.titleLabel?.textColor = theamBlackColor
            btnContainerView.layer.cornerRadius = 5
            btnContainerView.layer.shadowColor = UIColor.black.cgColor
            btnContainerView.layer.shadowOpacity = 0.12
            btnContainerView.layer.shadowOffset = CGSize(width: 0, height: 4)
            btnContainerView.layer.shadowRadius = 6
            //btnContainerView.layer.shouldRasterize = true
        }
    }
    
    class func cellIdentifire() ->String {
        return "ChatCustomButtomTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 100
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if message?.buttonFlag == 1 || enableButton == false{
            
//            paymentBtn.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
            paymentBtn.titleLabel?.textColor = theamGrayColor
//            paymentBtn.layer.borderWidth = 1
//            paymentBtn.layer.borderColor = staticMessageCellBorderColor.cgColor
//            paymentBtn.clipsToBounds = true
            
        }else{
            
//            paymentBtn.backgroundColor = UIColor(red: 255/255, green: 210/255, blue: 113/255, alpha: 1)
            paymentBtn.titleLabel?.textColor = theamBlackColor
//            btnContainerView.layer.cornerRadius = 5
//            btnContainerView.layer.shadowColor = UIColor.black.cgColor
//            btnContainerView.layer.shadowOpacity = 0.12
//            btnContainerView.layer.shadowOffset = CGSize(width: 0, height: 4)
//            btnContainerView.layer.shadowRadius = 6
//            btnContainerView.layer.shouldRasterize = true
            
        }
        
        self.backgroundColor = cellBackgroundColor
        self.contentView.backgroundColor = cellBackgroundColor
        paymentBtn.layer.cornerRadius = 5
        paymentBtn.clipsToBounds = true
        
    }
    
     // MARK: - @IBActions
    
    @IBAction func didTappedPaymentBtn(_ sender: UIButton) {
        self.didTapPaymentBtn?(sender.isSelected, self.msgID, self.payBtnRow!, self.payBtnSection!)
    }
    
}

extension UIButton
{
    func applyGradient(colors: [CGColor])
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
