//
//  RightImageTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 24/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class RightImageTableViewCell: UITableViewCell {
    
    // MARK: - Properties
     var docImage: UIImage?
     var chatViewController: ChatViewController?
    
    // MARK: - @IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var seenImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var msgImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = theamAppColor
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        seenImageView.image = nil
        timeLabel.text = ""
        msgImageView.image = nil
    }

    class func cellIdentifire() -> String{
        return "RightImageTableViewCell"
    }
    
    func configureCell(_ message: ChatMessage, _ section:Int, _ row: Int){
        
        // seen image view
        self.seenImageView.image = #imageLiteral(resourceName: "seen_image")
        
        // time
        if let time = message.sendedOn{
            self.timeLabel.text = Utilities.UTCToLocal(date: time, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "h:mm a")
        }
        
        // message image
        
        if let localImage = message.localImage {
            self.msgImageView.image = localImage
            self.chatViewController?.currentRowAndSection(section, row)
            self.activityIndicator.startAnimating()
        }else{
            self.loadImages(message)
        }

    }
    
    @objc func handleZoomTap(tapGesture: UITapGestureRecognizer){
        let imageView = tapGesture.view as? UIImageView
        self.chatViewController?.performZoomInForStartingImage(imageView!)
    }
    
//    fileprivate func loadImages(_ message: ChatMessage)  {
//        let imageURL = message.conversationMessage
//         activityIndicator.startAnimating()
//        Alamofire.request(imageURL).responseImage { response in
//            debugPrint(response)
//            self.activityIndicator.startAnimating()
//            print(response.request)
//            print(response.response)
//            debugPrint(response.result)
//
//            if let image = response.result.value {
//                print("image downloaded: \(image)")
//                self.msgImageView.image = image
//            }
//        }
//    }
    
    fileprivate func loadImages(_ message: ChatMessage)  {
        
        let imageProvider = ImageProvider()

        if let image = imageCache.image(forKey: "\(String(describing: message.conversationMessage))"){
            msgImageView.image = image
        }else{

            let imageURL = message.conversationMessage

            if imageURL.isEmpty == false{
                activityIndicator.startAnimating()
                if let url = URL(string: imageURL) {
                    imageProvider.requestImage(from: url) { (image) in
                        self.activityIndicator.stopAnimating()
                        imageCache.add(image, forKey: "\(String(describing: message.conversationMessage))")
                        self.msgImageView.image = image
                    }
                }
            }

        }

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundColor = cellBackgroundColor
        self.contentView.backgroundColor = cellBackgroundColor
        
        self.selectionStyle = .none

        imageContainerView.layer.cornerRadius = 5
        imageContainerView.clipsToBounds = true
        
        msgImageView.isUserInteractionEnabled = true
        msgImageView.contentMode = .scaleAspectFill
        msgImageView.layer.cornerRadius = 5
        msgImageView.layer.masksToBounds = true
        msgImageView.backgroundColor = UIColor(red: 251/255, green: 134/255, blue: 42/255, alpha: 1)
            
        msgImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomTap)))

        containerView.backgroundColor = UIColor(red: 251/255, green: 134/255, blue: 42/255, alpha: 1)
        containerView.layer.cornerRadius = 15
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.17
        containerView.layer.shadowRadius = 2.7
        containerView.layer.shadowOffset = CGSize(width: 6, height: 6)
        containerView.layer.shadowOffset = CGSize(width: -6, height: 6)
        if #available(iOS 11.0, *) {
            containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        
    }
    
}
