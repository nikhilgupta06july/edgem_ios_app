//
//  EndTutionBtnTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 10/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class EndTutionBtnTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    var chatViewController: ChatViewController?
    var msgID: String = ""
    var message: ChatMessage?
    var row: Int!
    var section: Int!
    var enableButton: Bool = true
    var gradientLayer = CAGradientLayer()
    
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var endTutionBtn: UIButton!
    @IBOutlet weak var endTutionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var seenStatusImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        gradientLayer.frame = buttonContainerView.bounds
        endTutionBtn.layer.insertSublayer(gradientLayer, at: 0)
    }
    
     func gradientedBtn() {

        endTutionLabel.backgroundColor = .clear
        endTutionLabel.textColor = .white
        
        gradientLayer.colors = [UIColor.orange.cgColor, UIColor.red.cgColor]
        
        
        buttonContainerView.backgroundColor = .clear
        buttonContainerView.layer.shadowColor = UIColor.black.cgColor
        buttonContainerView.layer.shadowOpacity = 0.7
        buttonContainerView.layer.shadowOffset = CGSize.zero
        buttonContainerView.layer.shadowRadius = 5
        //buttonContainerView.layer.shouldRasterize = true
        
    }
    
     func darkButton() {
        
        gradientLayer.colors = [UIColor(red: 244/255, green: 0/255, blue: 0/255, alpha: 1).cgColor]
        
        endTutionLabel.backgroundColor = cellBackgroundColor
        endTutionLabel.layer.borderWidth = 1
        endTutionLabel.layer.borderColor = staticMessageCellBorderColor.cgColor
        endTutionLabel.textColor = theamGrayColor
        endTutionLabel.clipsToBounds = true
        
        buttonContainerView.backgroundColor = .clear
        buttonContainerView.layer.shadowColor = UIColor.clear.cgColor
        buttonContainerView.layer.shadowOpacity = 0.0
        buttonContainerView.layer.shadowOffset = CGSize.zero
        buttonContainerView.layer.shadowRadius = 0.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = cellBackgroundColor
        self.contentView.backgroundColor = cellBackgroundColor
        
        endTutionBtn.layer.cornerRadius = 5
        endTutionLabel.layer.cornerRadius = 5
       
        endTutionBtn.clipsToBounds = true
        endTutionLabel.clipsToBounds = true

    }
    
    class func cellIdentifire() ->String {
        return "EndTutionBtnTableViewCell"
    }
    
    class func cellHeight() -> CGFloat{
    return 100
    }
    
    func configureCell(with message: ChatMessage){
        self.message = message
        self.msgID = "\(message.msgID)"
        
        if message.buttonFlag == 1 || enableButton == false{
            darkButton()
            endTutionBtn.isUserInteractionEnabled = false
        }else{
            gradientedBtn()
            endTutionBtn.isUserInteractionEnabled = true
        }
        
        if let time = message.sendedOn{
            self.timeLabel.text = Utilities.UTCToLocal(date: time, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "h:mm a")
        }
        
    }
    
    
    @IBAction func didTappedEndTution(_ sender: UIButton) {
        let status: Bool!
        if sender.isSelected == false {
            sender.isSelected = true
            status = true
            darkButton()
            endTutionBtn.isUserInteractionEnabled = false
        }else{
            sender.isSelected = false
            status = false
        }
        chatViewController?.didTappedEndTutionButton(status, self.msgID, self, row, section)
    }
}

