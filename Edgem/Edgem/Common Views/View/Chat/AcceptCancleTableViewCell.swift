//
//  AcceptCancleTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 15/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class AcceptCancleTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    
    var chatViewController: ChatViewController?
    var msgID: String = ""
    var message: ChatMessage?
    var gradientLayer1 = CAGradientLayer()
    var gradientLayer2 = CAGradientLayer()
    var enableButton: Bool = true
    
    var section:Int?
    var row: Int?
    
    // MARK: - @IBOutlets
    @IBOutlet weak var containeView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var cancleBtn: UIButton!
    @IBOutlet weak var cancleLabel: UILabel!
    @IBOutlet weak var cancleBtnContainerView: UIView!
    
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var acceptBtnContainerView: UIView!
    @IBOutlet weak var acceptLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        gradientLayer1.frame = cancleBtnContainerView.bounds
        cancleBtn.layer.insertSublayer(gradientLayer2, at: 0)
        
        gradientLayer2.frame = acceptBtnContainerView.bounds
        acceptBtn.layer.insertSublayer(gradientLayer1, at: 0)
    }
    
    func configureCell(with message: ChatMessage, _ section: Int, _ row: Int){
        
        self.section = section
        self.row = row
        
        self.message = message
        
        messageLabel.text = message.conversationMessage
        
        self.msgID = "\(message.msgID)"
        
        if message.buttonFlag == 1 || enableButton == false {
            cancleBtn.isSelected = true
            acceptBtn.isSelected = true
            
            darkBtn()
            acceptBtn.isUserInteractionEnabled = false
            cancleBtn.isUserInteractionEnabled = false
            
        }else{
            
            cancleBtn.isSelected = false
            acceptBtn.isSelected = false
            gradientBtn()
            cancleBtn.isUserInteractionEnabled = true
            acceptBtn.isUserInteractionEnabled = true
            
        }
        
    }

    class func cellIdentifire() ->String {
        return "AcceptCancleTableViewCell"
    }
    
    private func darkBtn(){
        
        gradientLayer1.colors = [UIColor(red: 244/255, green: 0/255, blue: 0/255, alpha: 1).cgColor]
        gradientLayer2.colors = [UIColor(red: 244/255, green: 0/255, blue: 0/255, alpha: 1).cgColor]
        
        acceptLabel.backgroundColor = cellBackgroundColor
        acceptLabel.layer.borderWidth = 1
        acceptLabel.layer.borderColor = staticMessageCellBorderColor.cgColor
        acceptLabel.textColor = theamGrayColor
        acceptLabel.clipsToBounds = true
        
        cancleLabel.backgroundColor = cellBackgroundColor
        cancleLabel.layer.borderWidth = 1
        cancleLabel.layer.borderColor = staticMessageCellBorderColor.cgColor
        cancleLabel.textColor = theamGrayColor
        cancleLabel.clipsToBounds = true
        
        cancleBtnContainerView.backgroundColor = .clear
        cancleBtnContainerView.layer.shadowColor = UIColor.clear.cgColor
        cancleBtnContainerView.layer.shadowOpacity = 0.0
        cancleBtnContainerView.layer.shadowOffset = CGSize.zero
        cancleBtnContainerView.layer.shadowRadius = 0.0
        
        acceptBtnContainerView.backgroundColor = .clear
        acceptBtnContainerView.layer.shadowColor = UIColor.clear.cgColor
        acceptBtnContainerView.layer.shadowOpacity = 0.0
        acceptBtnContainerView.layer.shadowOffset = CGSize.zero
        acceptBtnContainerView.layer.shadowRadius = 0.0
        
    }
    
    func gradientBtn(){
        
        acceptLabel.backgroundColor = .clear
        acceptLabel.textColor = .white
        
        cancleLabel.backgroundColor = .clear
        cancleLabel.textColor = .white
        
        gradientLayer2.colors = [UIColor.orange.cgColor, UIColor.red.cgColor]
        gradientLayer1.colors = [UIColor(red: 152/255, green: 212/255, blue: 64/255, alpha: 1).cgColor, UIColor(red: 30/255, green: 170/255, blue: 45/255, alpha: 1).cgColor]
        
        acceptBtnContainerView.backgroundColor = .clear
        acceptBtnContainerView.layer.shadowColor = UIColor.black.cgColor
        acceptBtnContainerView.layer.shadowOpacity = 0.7
        acceptBtnContainerView.layer.shadowOffset = CGSize.zero
        acceptBtnContainerView.layer.shadowRadius = 5
        //acceptBtnContainerView.layer.shouldRasterize = true
        
        cancleBtnContainerView.backgroundColor = .clear
        cancleBtnContainerView.layer.shadowColor = UIColor.black.cgColor
        cancleBtnContainerView.layer.shadowOpacity = 0.7
        cancleBtnContainerView.layer.shadowOffset = CGSize.zero
        cancleBtnContainerView.layer.shadowRadius = 5
        //cancleBtnContainerView.layer.shouldRasterize = true
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = cellBackgroundColor
        self.contentView.backgroundColor = cellBackgroundColor
        
        cancleBtn.layer.cornerRadius = 5
        acceptBtn.layer.cornerRadius = 5
        cancleLabel.layer.cornerRadius = 5
        acceptLabel.layer.cornerRadius = 5
        
        containeView.layer.borderWidth = 1
        containeView.layer.borderColor = staticMessageCellBorderColor.cgColor
        containeView.layer.cornerRadius = 5
        containeView.clipsToBounds = true
        
        cancleBtn.clipsToBounds = true
        acceptBtn.clipsToBounds = true
    }
    
    @IBAction func didTapAcceptCancle(_ sender: UIButton) {
        
        let status: Bool!
        
        if sender.isSelected == false {
            sender.isSelected = true
            status = true
            
            darkBtn()
            acceptBtn.isUserInteractionEnabled = false
            cancleBtn.isUserInteractionEnabled = false
            
        }else{
            sender.isSelected = false
            status = false
        }
        chatViewController?.didTapAcceptCancleBtn(sender, self.msgID, self, self.section!, self.row!)
    }

}
