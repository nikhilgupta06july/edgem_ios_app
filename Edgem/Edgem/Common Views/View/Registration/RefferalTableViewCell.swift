//
//  RefferalTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 29/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class RefferalTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    var createUserViewController: CreateUserViewController?
    
    // MARK: - @IBOutlets
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var refferalCodeTextField: UITextField!
    @IBOutlet weak var errorImage: UIImageView!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        checkBoxBtn.setImage(#imageLiteral(resourceName: "checkboxInactive"), for: .normal)
        refferalCodeTextField.isHidden = true
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        refferalCodeTextField.layer.borderColor = UIColor.black.cgColor
        refferalCodeTextField.clipsToBounds = true
    }
    
    class func cellIdentifire() -> String {
        return "RefferalTableViewCell"
    }
    
    func configureCellForRowAtIndex(_ index: Int, withText text: String) {
        
        
        if refferalCodeTextField != nil{
            refferalCodeTextField.tag = index
            refferalCodeTextField.placeholder = text
        }
        
        if checkBoxBtn.isSelected == true {
            checkBoxBtn.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            refferalCodeTextField.isHidden = false
            errorLabel.text = ""
        }
    }
    
      // MARK: - @IBActions
    @IBAction func didTapCheckBoxBtn(_ sender: UIButton) {
    
        self.createUserViewController?.didTapCheckBoxBtn(sender)
    }

      // MARK: - Validations
    
     // MARK: Referral Code
    func validateReferralCode(text: String) {
        if text.isEmptyString() {
            self.errorImage.isHidden = true
            errorImage.backgroundColor = UIColor.clear
            errorImage.image = nil
            errorLabel.text = ValidationErrorMessage.referralCode.description()
        }else if !text.isValidReferralCode(){
            errorLabel.text = ValidationErrorMessage.referralCodeInValid.description()
        } else {
            //customTextField.showImage()
            errorImage.backgroundColor = UIColor.clear
            errorImage.image = nil
            errorLabel.text = ""
        }
    }
    
}








