//
//  OtherSubjectTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 18/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class OtherSubjectTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var checkBoxHolderView: UIView!
    @IBOutlet weak var otherSubjectTextFieldholderView: UIView!
    @IBOutlet weak var otherSubjectTextField: UITextField!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func cellIdentifire()->String{
        return "OtherSubjectTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 103
    }
    
    func configureCell(_ subject: Subject, _ isSelected: Bool, _ index: Int){
        
        subjectLabel.text = subject.name
        let imageName = isSelected ? "selected" : "deselected"
        otherSubjectTextFieldholderView.isHidden = imageName ==  "selected" ? false : true
        checkBoxHolderView.backgroundColor = isSelected ? cellBackgroundColor : UIColor.white
        holderView.backgroundColor = isSelected ? cellBackgroundColor : UIColor.white
        self.checkBoxButton.setImage(UIImage(named: imageName), for: .normal)
        
        if subject.name.contains("Music".localized){
            otherSubjectTextField.placeholder = "Enter your instruments".localized
        }else if subject.name.contains("Languages".localized){
            otherSubjectTextField.placeholder = "Enter a language*".localized
        }else if subject.name.contains("Others".localized){
             otherSubjectTextField.placeholder = "Enter your subject*".localized
        }

        bottomHeight.constant = isSelected ? 20 : 0
        otherSubjectTextField.tag = index
        
    }
    
    func configureCellWithSubject(_ subject: Subject, _ isSelected: Bool,_ index: Int){
        subjectLabel.text = subject.name
        let imageName = isSelected ? "selected" : "deselected"
        otherSubjectTextFieldholderView.isHidden = imageName ==  "selected" ? false : true
        checkBoxHolderView.backgroundColor = isSelected ? cellBackgroundColor : UIColor.white
        holderView.backgroundColor = isSelected ? cellBackgroundColor : UIColor.white
        self.checkBoxButton.setImage(UIImage(named: imageName), for: .normal)
        otherSubjectTextField.placeholder = "Enter your subject*".localized
        bottomHeight.constant = isSelected ? 20 : 0
        otherSubjectTextField.tag = index
    }
    
    func configureCellWithLanguage(_ subject: Subject, _ isSelected: Bool,_ index: Int){
        subjectLabel.text = subject.name
        let imageName = isSelected ? "selected" : "deselected"
        otherSubjectTextFieldholderView.isHidden = imageName ==  "selected" ? false : true
        checkBoxHolderView.backgroundColor = isSelected ? cellBackgroundColor : UIColor.white
        holderView.backgroundColor = isSelected ? cellBackgroundColor : UIColor.white
        self.checkBoxButton.setImage(UIImage(named: imageName), for: .normal)
        otherSubjectTextField.placeholder = "Enter a language*".localized
        bottomHeight.constant = isSelected ? 20 : 0
        otherSubjectTextField.tag = index
    }
    
}
