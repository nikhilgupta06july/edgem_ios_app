//
//  CommonCheckBoxTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 21/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class CommonCheckBoxTableViewCell: UITableViewCell {

    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var checkBoxButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetUP()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(with dataArr:String){
        self.subjectLabel.text = dataArr
    
    }
    
    class func cellIdentifire()->String{
        return "CommonCheckBoxTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 55
    }
    
    func initialSetUP(){
        subjectLabel.font = UIFont(name: QuicksandMedium, size: 18)
        subjectLabel.textColor = theamBlackColor
        
        // check box button
        checkBoxButton.setImage(UIImage(named: "checkInActive"), for: .normal)
        checkBoxButton.setImage(UIImage(named: "checkActive"), for: .selected)
    }
    
    
    @IBAction func checkBoxBtnTapped(_ sender: UIButton) {
        if sender.isSelected == true{
            sender.isSelected = false
        }else{
             sender.isSelected = true
        }
    }
    
}
