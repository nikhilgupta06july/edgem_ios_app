//
//  CommonExpandCollapsTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 19/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class CommonExpandCollapsTableViewCell: UITableViewCell {
    

     // MARK: --------- Properties

    var onDidTapAddRemoveBtn:((Int, Int, Int)->Void)?
    var cellIndex: Int!
    var cellSection: Int!
    
    
    // MARK: --------- @IBOutlets
    
    @IBOutlet weak var cellTitle: UILabel!
    
    @IBOutlet weak var serialNumber: UILabel!
    
    @IBOutlet weak var borderView: UIView!
    
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    
    @IBOutlet weak var firstTextFieldTitleLabel: UILabel!
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var seprator1: UIView!
    
    @IBOutlet weak var secondTextFieldTitleLabel: UILabel!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var seprator2: UIView!
    
    @IBOutlet weak var thirdTextFieldTitleLabel: UILabel!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var seprator3: UIView!
    @IBOutlet weak var image1Btn: UIButton!
    
    @IBOutlet weak var fourthTextFieldTitleLabel: UILabel!
    @IBOutlet weak var fourthTextField: UITextField!
    @IBOutlet weak var seprator4: UIView!
    @IBOutlet weak var image2Btn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        seprator1.backgroundColor = theamBorderGrayColor
        seprator2.backgroundColor = theamBorderGrayColor
        seprator3.backgroundColor = theamBorderGrayColor
        seprator4.backgroundColor = theamBorderGrayColor
        
    }
    
    // MARK: --------- Private Functions
    
    class func cellIdentifire() -> String{
        return "CommonExpandCollapsTableViewCell"
    }
    
    func configureCell(with educationData: Education, _ title: String, _ index: Int, lastIndex : Int, section : Int ) {
        
        cellIndex = index-1
        cellSection = section
        
        if index <= lastIndex && lastIndex == 1{
             removeBtn.isHidden = true
             addBtn.isHidden = false
        }else if index == lastIndex && lastIndex == 5{
             addBtn.isHidden = true
             removeBtn.isHidden = false
        }else if index < lastIndex && lastIndex < 5{
             addBtn.isHidden = true
             removeBtn.isHidden = true
        }else if index == lastIndex && lastIndex != 5 && lastIndex != 1{
              removeBtn.isHidden = false
              addBtn.isHidden = false
        }else{
            addBtn.isHidden = true
            removeBtn.isHidden = true
        }
        
        if index == lastIndex{
            borderView.isHidden = false
        }else{
            borderView.isHidden = true
        }
        
        if index != 1{
            cellTitle.isHidden = true
        }else{
              cellTitle.text = title
              cellTitle.isHidden = false
        }
    
        serialNumber.text = "\(index)."
        
        firstTextFieldTitleLabel.text = "School*".localized
        firstTextField.attributedPlaceholder = NSAttributedString(string: "Enter your school name".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        firstTextField.tag = 1
        firstTextField.text = educationData.instituteName
        
        secondTextFieldTitleLabel.text = "Certificate*".localized
        secondTextField.attributedPlaceholder = NSAttributedString(string: "Enter your certificate name".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        secondTextField.tag = 2
        secondTextField.text = educationData.certificateName
        
        thirdTextFieldTitleLabel.text = "From*".localized
        thirdTextField.attributedPlaceholder = NSAttributedString(string: "YYYY".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        thirdTextField.tag = 3
        thirdTextField.text = educationData.fromYear
        
        fourthTextFieldTitleLabel.text = "To*".localized
        fourthTextField.placeholder = "YYYY".localized
        fourthTextField.attributedPlaceholder = NSAttributedString(string: "YYYY".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        fourthTextField.tag = 4
        fourthTextField.text = educationData.toYear
        
    }
    
    func configureCellForExperience(with experienceData: Experience, _ title: String, _ index: Int, lastIndex : Int, section : Int ) {
        
        cellIndex = index-1
        cellSection = section
        
        if index <= lastIndex && lastIndex == 1{
            removeBtn.isHidden = true
            addBtn.isHidden = false
        }else if index == lastIndex && lastIndex == 5{
            addBtn.isHidden = true
            removeBtn.isHidden = false
        }else if index < lastIndex && lastIndex < 5{
            addBtn.isHidden = true
            removeBtn.isHidden = true
        }else if index == lastIndex && lastIndex != 5 && lastIndex != 1{
            removeBtn.isHidden = false
            addBtn.isHidden = false
        }else{
            addBtn.isHidden = true
            removeBtn.isHidden = true
        }
        
        if index == lastIndex{
            borderView.isHidden = false
        }else{
            borderView.isHidden = true
        }
        
        if index != 1{
            cellTitle.isHidden = true
        }else{
            cellTitle.text = title
            cellTitle.isHidden = false
        }
        
        serialNumber.text = "\(index)."
        
        firstTextFieldTitleLabel.text = "School*".localized
        firstTextField.attributedPlaceholder = NSAttributedString(string: "Enter your school name".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        firstTextField.tag = 1
        firstTextField.text = experienceData.instituteName
        
        secondTextFieldTitleLabel.text = "Subject*".localized
        secondTextField.attributedPlaceholder = NSAttributedString(string: "Enter your subject name".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        secondTextField.tag = 2
        secondTextField.text = experienceData.subjectName
        
        thirdTextFieldTitleLabel.text = "From*".localized
        thirdTextField.attributedPlaceholder = NSAttributedString(string: "YYYY".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        thirdTextField.tag = 3
        thirdTextField.text = experienceData.fromYear
        
        fourthTextFieldTitleLabel.text = "To*".localized
        fourthTextField.placeholder = "YYYY".localized
        fourthTextField.attributedPlaceholder = NSAttributedString(string: "YYYY".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        fourthTextField.tag = 4
        fourthTextField.text = experienceData.toYear
        
    }
    
    
    
    // MARK: --------- @IBActions
    
    @IBAction func didTapAddRemoveBtn(_ sender: UIButton) {
        onDidTapAddRemoveBtn?(sender.tag, cellIndex, cellSection)
    }

}
