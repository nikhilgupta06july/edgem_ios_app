//
//  YearOfExperienceTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 28/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class YearOfExperienceTableViewCell: UITableViewCell {
    
     // MARK: -------- Properties
    
    var count = 0{
        didSet{
            yearLabel.text = "\(count)"
            experienceCount?("\(count)")
        }
    }
    var experienceCount: ((String)->Void)?
    
    // MARK: -------- @IBOutlets
    
    @IBOutlet weak var cellTitleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        yearLabel.text = "\(count)"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
       // yearLabel.text = "\(count)"
    }
    
    // MARK: -------- Helping Functions
    
    class func cellIdentifire() -> String {
        return "YearOfExperienceTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 130.0
    }
    
    func configureCell(_ yearOfExperience: String){
        if let totalExp = Int(yearOfExperience){
            self.count = totalExp
        }
        
    }
    
     // MARK: -------- @IBActions
    
    @IBAction func didTapPlusOrMinusBtn(_ sender: UIButton) {
        
        if sender.tag == 1{
            if count <= 0{
                return
            }
             count -= 1
        }else{
            if count >= 25{
                return
            }
            count += 1
        }
    }
    
}
