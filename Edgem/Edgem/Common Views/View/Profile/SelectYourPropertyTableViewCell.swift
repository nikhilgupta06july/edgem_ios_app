//
//  SelectYourPropertyTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 20/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class SelectYourPropertyTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    var rowCount = 0
    var sectionCount = 0
    var userQualifications: [Qualification]!{
        didSet{
            sectionCount = userQualifications.count
            secondTableView.reloadData()
        }
    }
    
    @IBOutlet weak var secondTableViewHeight: NSLayoutConstraint!
    
    var selectedQualificatioNExperience: (([Qualification],String)->Void)?
     var updateSecondTableViewHeight:((Bool)->Void)?
    
    @IBOutlet weak var secondTableView: UITableView!{
        didSet{
            secondTableView.delegate = self
            secondTableView.dataSource = self
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        registerCell()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func cellIdentifire() -> String{
        return "SelectYourPropertyTableViewCell"
    }
    
    func configureCell(with qualifications: [Qualification], _ title: String){
        
        // title
        titleLabel.text = title
        self.userQualifications = qualifications
    }
    
    func experienceCount(count: String){
        userQualification()
    }
    
    fileprivate func userQualification() {
        var qualificationIDs = ""
        var yearOfExperience = ""
        let selectedQualifications = self.userQualifications.filter{$0.isSelected}
        let indexPath = IndexPath(row: 1, section: 0)
        if let cell = secondTableView.cellForRow(at: indexPath) as? YearOfExperienceTableViewCell{
            if let yearOfExp = cell.yearLabel.text {
                  yearOfExperience = yearOfExp
            }
        }else{
             print(selectedQualifications)
        }
        self.selectedQualificatioNExperience?(selectedQualifications, yearOfExperience)
    }
    
    
    func registerCell(){
        
        secondTableView.register(UINib(nibName: "CommonCheckBoxCell", bundle: nil), forCellReuseIdentifier: CommonCheckBoxCell.cellIdentifire())
        secondTableView.register(UINib(nibName: YearOfExperienceTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: YearOfExperienceTableViewCell.cellIdentifire())
         secondTableView.register(UINib(nibName: CommonExpandCollapsTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: CommonExpandCollapsTableViewCell.cellIdentifire())
    }
    


}

extension SelectYourPropertyTableViewCell: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            let qualification = userQualifications[section]
            return qualification.isSelected ? 2 : 1
        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let qualification = userQualifications[safe: indexPath.section] {
            guard indexPath.section != 0 else{
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire()) as! CommonCheckBoxCell
                    cell.configureCell(textString: qualification.name, isSelected: qualification.isSelected, index: indexPath.row)
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: YearOfExperienceTableViewCell.cellIdentifire()) as! YearOfExperienceTableViewCell
                    //cell.configureCell(textString: qualification.name, isSelected: qualification.isSelected, index: indexPath.row)
                    cell.configureCell(qualification.totalExperience ?? "\(0)")
                    cell.experienceCount = {[weak self] count in
                        self!.experienceCount(count: count)
                    }
                    
                    return cell
                }
            }
            
            guard indexPath.section != userQualifications.count else{
                return UITableViewCell()
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire()) as! CommonCheckBoxCell
            cell.configureCell(textString: qualification.name, isSelected: qualification.isSelected, index: indexPath.row)
            
            return cell
        }else{
            guard indexPath.section != userQualifications.count else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonExpandCollapsTableViewCell.cellIdentifire(), for: indexPath) as? CommonExpandCollapsTableViewCell else{
                    return UITableViewCell()
                }
                return cell
            }
          
        }
        return   UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let qualification = self.userQualifications[indexPath.section]
        
        qualification.isSelected = !qualification.isSelected
        if indexPath.section == 0 && indexPath.row == 0 {
            
            tableView.reloadSections([indexPath.section], with: .automatic)
            
               updateSecondTableViewHeight?(true)
        }else{
              tableView.reloadSections([indexPath.section], with: .automatic)
        }
        userQualification()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 1{
            return 130
        }else if indexPath.section == 5 {
            return UITableView.automaticDimension
        }
        
        return CommonCheckBoxCell.cellHeight()
    }
    
    
}
