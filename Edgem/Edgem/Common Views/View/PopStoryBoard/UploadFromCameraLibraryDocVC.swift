//
//  UploadFromCameraLibraryDocVC.swift
//  Edgem
//
//  Created by Namespace on 10/08/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class UploadFromCameraLibraryDocVC: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func uploadImage(_ sender: UIButton) {
        
       // self.dismiss(animated: true)
        
        if sender.tag == 1{
            self.camera()
        }else if sender.tag == 2{
            self.photoLibrary()
        }else if sender.tag == 3{
            self.document()
        }else{
             self.dismiss(animated: true)
        }
    }
}
