//
//  CommonPopUpVC.swift
//  Edgem
//
//  Created by Namespace on 12/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class CommonPopUpVC: UIViewController {
    
    // MARK: - Properties
    var chatViewController: ChatViewController?
    
     // MARK: - @IBOutlets
    
    @IBOutlet weak var popUPContentLabel: UILabel!
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // yesBtn
        yesBtn.titleLabel?.font = UIFont(name: QuicksandBold, size: 18)
        yesBtn.titleLabel?.textColor = theamBlackColor
        
        // yesBtn
        noBtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        noBtn.titleLabel?.textColor = theamBlackColor
        
        // popUPContentLabel
        popUPContentLabel.font = UIFont(name: QuicksandMedium, size: 15)
        popUPContentLabel.textColor = theamBlackColor
    }
    
    // MARK: - @IBActions
    
    @IBAction func didTapOkBtn(_ sender: UIButton) {
        
        if sender.tag == 1{
            // No btn tapped
            dismiss(animated: true)
        }else{
            // Yes btn tapped
            chatViewController?.yesBtnTappedFromCommonPopVC()
        }
        
    }
}
