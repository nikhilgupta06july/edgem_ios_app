//
//  CommonCheckBoxCell.swift
//  Edgem
//
//  Created by Hipster on 21/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol CommonCheckBoxCellDelegate {
    func checkboxTappedAtIndex(index: Int)
}

class CommonCheckBoxCell: UITableViewCell {
    
    // MARK: --------- Properties
    
    var delegate: CommonCheckBoxCellDelegate?
    var isFrom = ""
    
    // MARK: --------- @IBOutlets
    
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var sepratorView: UIView!
    
    
    
    class func cellIdentifire()->String{
        return "CommonCheckBoxCell"
    }
    
    class func cellHeight()->CGFloat{
        return 55
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetUP()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        subjectLabel.text = ""
        checkBoxButton.setImage(UIImage(named: "deselected"), for: .normal)
    }
    
    
    
    func configureCell(textString: String, isSelected: Bool, index: Int){
        self.subjectLabel.text = textString
        self.checkBoxButton.tag = index
        if isSelected {
            self.backgroundColor = cellBackgroundColor
            self.checkBoxButton.setImage(UIImage(named:"selected"), for: .normal)
        } else {
            self.backgroundColor = UIColor.white
            self.checkBoxButton.setImage(UIImage(named: "deselected"), for: .normal)
        }
       
    }
    
    func configureCellWithLevel(_ level: Level, _ isSelected: Bool){
        subjectLabel.text = level.name
        var imageName = ""
        if isFrom == "LevelSubjectViewController"{
              imageName = isSelected ? "ActiveRadio" : "InActiveRadio"
        }else{
             imageName = isSelected ? "selected" : "deselected"
        }
        
        self.backgroundColor = isSelected ? cellBackgroundColor : UIColor.white
        self.checkBoxButton.setImage(UIImage(named: imageName), for: .normal)
    }
    
    func configureCellWithSubject(_ subject: Subject, _ isSelected: Bool){
        subjectLabel.text = subject.name
        let imageName = isSelected ? "selected" : "deselected"
        self.backgroundColor = isSelected ? cellBackgroundColor : UIColor.white
        self.checkBoxButton.setImage(UIImage(named: imageName), for: .normal)
    }
    
    func configureCellWithQualification(_ qualification: Qualification, _ isSelected: Bool){
        subjectLabel.text = qualification.name
        let imageName = isSelected ? "selected" : "deselected"
        self.backgroundColor = isSelected ? cellBackgroundColor : UIColor.white
        self.checkBoxButton.setImage(UIImage(named: imageName), for: .normal)
    }
    
    func configureCellWithSortBy(_  sortByProperty: String, _ isSelected: Bool){
        subjectLabel.text = sortByProperty
        let imageName = isSelected ? "ActiveRadio" : "InActiveRadio"
        subjectLabel.font = UIFont(name: QuicksandRegular, size: 15)
        subjectLabel.textColor = theamBlackColor
        self.backgroundColor = isSelected ? cellBackgroundColor : UIColor.white
        self.checkBoxButton.setImage(UIImage(named: imageName), for: .normal)
    }
    
    func configureCellWithSortByTitles(_  sortByProperty: String, _ isSelected: Bool){
        subjectLabel.text = sortByProperty
        self.checkBoxButton.isHidden = true
        subjectLabel.font = UIFont(name: QuicksandMedium, size: 18)
        subjectLabel.textColor = theamBlackColor
    }
    
    func configureCellWithProperties(_  sortByProperty: String, _ isSelected: Bool){
        subjectLabel.text = sortByProperty
        let imageName = isSelected ? "ActiveRadio" : "InActiveRadio"
        self.backgroundColor = isSelected ? cellBackgroundColor : UIColor.white
        self.checkBoxButton.setImage(UIImage(named: imageName), for: .normal)
        
    }
    
    func configureCell(with sectionTitles: String, _ isSelected: Bool){
        subjectLabel.text = sectionTitles
        let imageName = isSelected ? "ActiveRadio" : "InActiveRadio"
        self.checkBoxButton.setImage(UIImage(named: imageName), for: .normal)
        
        subjectLabel.font = UIFont(name: QuicksandMedium, size: 18)
        subjectLabel.textColor = theamBlackColor
    }
    
    func configureCellWith(with rowTitles: String, _ isSelected: Bool){
        subjectLabel.text = rowTitles
        let imageName = isSelected ? "ActiveRadio" : "InActiveRadio"
        self.checkBoxButton.setImage(UIImage(named: imageName), for: .normal)
        
        subjectLabel.font = UIFont(name: QuicksandRegular, size: 15)
        subjectLabel.textColor = theamBlackColor
    }
    
    func initialSetUP(){
        //subjectLabel.font = UIFont(name: QuicksandMedium, size: 18)
        //subjectLabel.textColor = theamBlackColor
    }
    
    
    @IBAction func checkBoxBtnTapped(_ sender: UIButton) {
        delegate?.checkboxTappedAtIndex(index: sender.tag)
    }
    
}
