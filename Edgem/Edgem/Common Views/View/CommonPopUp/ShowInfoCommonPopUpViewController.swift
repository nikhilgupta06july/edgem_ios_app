//
//  ShowInfoCommonPopUpViewController.swift
//  Edgem
//
//  Created by Hipster on 04/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit

protocol ShowInfoCommonPopUpDelegate {
    func tableViewCellSelected(_ indexPath: IndexPath)
}
class ShowInfoCommonPopUpViewController: BaseViewController {
   
    //MARK: ---------- Properties
    var delegate: ShowInfoCommonPopUpDelegate!
    var fromController = UIViewController()//String = ""
    var backbtnTapped: String = "no"
    var customMsg = ""
    var showAlert = false
    var showDocumentCameraPhoneOptions = false
    var alertForCounter = false
    var isTutorArrived = false
    lazy var activeTextField =  UITextField()
     let toolBar = UIToolbar()
     let negotiableStatus = UIPickerView()
    
    //MARK: ---------- @IBOutlets
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.estimatedRowHeight = 44
            tableView.rowHeight = UITableView.automaticDimension
        }
    }
    @IBOutlet weak var tableViewHeighrConstraint: NSLayoutConstraint!
    
    //MARK: ---------- View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if alertForCounter {
           
            toolBar.barStyle = UIBarStyle.default
            toolBar.isTranslucent = true
            toolBar.tintColor = theamBlackColor
            toolBar.sizeToFit()
            
          //  let doneButton = UIBarButtonItem(title: "Done", target: self, style: UIBarButtonItem.Style.plain, action: #Selector(donePicker))
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
         
            toolBar.setItems([doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            
        }
        

        self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        tableView.layer.cornerRadius = 5.0
        tableView.clipsToBounds = true
        
        negotiableStatus.delegate = self
        negotiableStatus.dataSource = self
    }
    
    @objc func donePicker() {
        activeTextField.resignFirstResponder()
    }
    
    // Update table view according content
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tableView.layer.removeAllAnimations()
        tableViewHeighrConstraint.constant = tableView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
        
    }
    //MARK:  ---------- Private Functions
    

    //MARK:  ---------- @IBActions
   
    @IBAction func dismissBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
}


    // MARK:  ---------- TableView Delegate/Datasource

extension ShowInfoCommonPopUpViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch fromController {
        case is ProfileViewController, is SignUpViewController:
            return 3
        case is ContactUSViewController:
            return 3
        case is QualificationNExperienceViewController:
            return showAlert ? 3 : 4
        case is QualificationsNExperiencesVC:
            return showAlert ? 2 : 3
        case is ChatViewController:
            return showDocumentCameraPhoneOptions ? 2: 3
        case is DashboardViewController, is QRViewController:
            return 2
        default:
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch fromController {
            
        case is DashboardViewController:
            
            if indexPath.row == 1{
                return LoadCommonContentLabelForPopUpTableViewCell(indexPath, customMsg.localized)
            }else{
                return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.ok.description())
            }
            
        case is ContactUSViewController:
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CommonSingleImageTableViewCell", for: indexPath)
                return cell
            }else if indexPath.row == 1{
                return LoadCommonContentLabelForPopUpTableViewCell(indexPath, customMsg.localized)
            }else{
                return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.ok.description())
            }
        case is SignUpOptionViewController:
            if indexPath.row == 0{
                return LoadCommonContentLabelForPopUpTableViewCell(indexPath, CustomAlertMessages.ShowPopUPSelectUser.description())
            }else{
                return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.ok.description())
            }
        case  is CreateStudentAccountStepTwoViewController:
            if indexPath.row == 0{
                customMsg = backbtnTapped == "yes" ? AlertMessage.removeFieldData.description() : customMsg
                return LoadCommonContentLabelForPopUpTableViewCell(indexPath, customMsg)
            }else{
                //return LoadCommonAcceptDeclinePopBtnTableViewCell(indexPath, AlertButton.no.description(), AlertButton.yes.description())
                 return backbtnTapped == "yes" ? LoadCommonAcceptDeclinePopBtnTableViewCell(indexPath, AlertButton.no.description(), AlertButton.yes.description()) : LoadCommonOkBtnTableViewCell(indexPath, AlertButton.ok.description())
            }
        case is CreateStudentAccountViewController:
            if indexPath.row == 0{
                customMsg = backbtnTapped == "yes" ? AlertMessage.removeFieldData.description() : customMsg
                return LoadCommonContentLabelForPopUpTableViewCell(indexPath, customMsg)
            }else{
                return backbtnTapped == "yes" ? LoadCommonAcceptDeclinePopBtnTableViewCell(indexPath, AlertButton.no.description(), AlertButton.yes.description()) : LoadCommonOkBtnTableViewCell(indexPath, AlertButton.ok.description())
            }
        case is ProfileViewController, is SignUpViewController:
            if indexPath.row == 0{
                return LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.camera.description())
            }else if indexPath.row == 1{
                return LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.photoLibrary.description())
            }else{
                return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.close.description())
            }
        case is QRViewController:
            if indexPath.row == 0{
                return LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.shareQRCode.description())
            }else if indexPath.row == 1{
                //return LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.saveImage.description())
                 return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.close.description())
            }else{
                return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.close.description())
            }
            
        case is QualificationNExperienceViewController:
            if showAlert {
                if indexPath.row == 0{
                    return LoadCommonSingleImageTableViewCell(indexPath, "")
                }else if indexPath.row == 1 {
                    return LoadCommonContentLabelForPopUpTableViewCell(indexPath, CustomAlertMessages.certificatePending.description())
                }else{
                    return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.ok.description())
                }
            }else{
                if indexPath.row == 0{
                    return LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.camera.description())
                }else if indexPath.row == 1{
                    return LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.photoLibrary.description())
                }else if indexPath.row == 2{
                    return  LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.documents.description())
                }else{
                    return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.close.description())
                }
            }
            
        case is QualificationsNExperiencesVC:
            
            if showAlert {
                if indexPath.row == 0{
                    //return LoadCommonSingleImageTableViewCell(indexPath, "")
                    return LoadCommonContentLabelForPopUpTableViewCell(indexPath, CustomAlertMessages.certificatePending.description())
                }else if indexPath.row == 1 {
                    return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.ok.description())//LoadCommonContentLabelForPopUpTableViewCell(indexPath, CustomAlertMessages.certificatePending.description())
                }else{
                    return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.ok.description())
                }
            }else{
                if indexPath.row == 0{
                    return LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.camera.description())
                }else if indexPath.row == 1{
                    return LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.photoLibrary.description())
                }
//                else if indexPath.row == 2{
//                    return  LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.documents.description())
//                }
                else{
                    return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.close.description())
                }
            }
            
        case is ChatViewController :
            
            if alertForCounter {
                
                UserStore.shared.negotiableStatus = 0
                
                if indexPath.row == 0 {
                    return LoadCommonTitleLabelCell(indexPath, CustomAlertMessages.title.description())
                
                } else if indexPath.row == 1{
                    return loadCommonDropDownCell(indexPath, activeTextField.text ?? "")
                }else{
                    return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.ok.description())
                }
                
            }else if isTutorArrived == true{
                
                if indexPath.row == 0 {
                    return LoadCommonTitleLabelCell(indexPath, CustomAlertMessages.title.description())
                } else if indexPath.row == 1{
                    return  LoadCommonContentLabelForPopUpTableViewCell(indexPath, AlertMessage.tuttorArrived.description())
                }else{
                    return LoadCommonAcceptDeclinePopBtnTableViewCell(indexPath, AlertButton.no.description(),AlertButton.yes.description())                }
                
            }else if showDocumentCameraPhoneOptions == true{
                if indexPath.row == 0{
                    return LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.camera.description())
                }else if indexPath.row == 1{
                    return LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.photoLibrary.description())
                }else if indexPath.row == 2{
                    return  LoadCommonSingleTitleLabelCell(indexPath, CustomAlertMessages.documents.description())
                }else{
                    return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.close.description())
                }
            }else{
                if indexPath.row == 0 {
                    return LoadCommonTitleLabelCell(indexPath, CustomAlertMessages.cancleOffer.description())
                } else if indexPath.row == 1{
                     return LoadCommonContentLabelForPopUpTableViewCell(indexPath, self.customMsg)
                }else{
                    return LoadCommonAcceptDeclinePopBtnTableViewCell(indexPath, AlertButton.cancel.description(),AlertButton.confirm.description())
                }
            }
        
        default:
            return LoadCommonOkBtnTableViewCell(indexPath, AlertButton.ok.description())
        }
     
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch fromController {
        case is ProfileViewController, is SignUpViewController:
            self.dismiss(animated: true)
            if indexPath.row == 0{
                fromController.camera()
            }else{
                fromController.photoLibrary()
            }
            
        case is ChatViewController:
            if !alertForCounter{
                 self.dismiss(animated: true)
                if indexPath.row == 0{
                    fromController.camera()
                }else if indexPath.row == 1{
                    fromController.photoLibrary()
                }else{
                    fromController.document()
                }
            }
        case is QualificationNExperienceViewController:
            self.dismiss(animated: true)
            if indexPath.row == 0{
                fromController.camera()
            }else if indexPath.row == 1{
                fromController.photoLibrary()
            }else{
                fromController.document()
            }
            
        case is QualificationsNExperiencesVC:
            
            self.dismiss(animated: true)
            if indexPath.row == 0{
                fromController.camera()
            }else if indexPath.row == 1{
                fromController.photoLibrary()
            }else{
                fromController.document()
            }
            
            
        case is QRViewController:
             self.dismiss(animated: true)
             delegate.tableViewCellSelected(indexPath)
        break
        default:
        break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func LoadCommonSingleTitleLabelCell(_ indexPath: IndexPath, _ text: String) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonSingleTitleLabelCell.cellIdentifire(), for: indexPath) as? CommonSingleTitleLabelCell else{
            fatalError("could not load CommonSingleTitleLabelCell")
        }
        cell.deviderLine.isHidden = indexPath.row == 1
        cell.contentIcon.isHidden = (fromController is QRViewController)
        cell.contentIcon.image = indexPath.row == 0 ? UIImage(named: "camera2") : UIImage(named: "photoLibrary")
        cell.configureCell(text)
        return cell
    }
    
    func LoadCommonContentLabelForPopUpTableViewCell(_ indexPath: IndexPath, _ text: String) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonContentLabelForPopUpTableViewCell.cellIdentifire(), for: indexPath) as? CommonContentLabelForPopUpTableViewCell else{
                fatalError("could not load CommonContentLabelForPopUpTableViewCell")
            }
        cell.configureCell(text)
        return cell
    }
    
    func loadCommonDropDownCell(_ indexPath: IndexPath, _ text: String) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonDropDownTableViewCell.cellIdentifire(), for: indexPath) as? CommonDropDownTableViewCell else{
            fatalError("could not load CommonContentLabelForPopUpTableViewCell")
        }
       // cell.configureCell(text)
        cell.selectionStyle = .none
        cell.configureCell(with: text)
        cell.statusLabel.inputAccessoryView = self.toolBar
        cell.statusLabel.delegate = self
        cell.statusLabel.inputView = negotiableStatus
        return cell
    }
    
    func LoadCommonTitleLabelCell(_ indexPath: IndexPath, _ text: String) ->UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonTitleLabelTableViewCell.cellIdentifire(), for: indexPath) as? CommonTitleLabelTableViewCell else{
            fatalError("could not load CommonContentLabelForPopUpTableViewCell")
        }
         cell.selectionStyle = .none
        cell.isUserInteractionEnabled = (alertForCounter == true) ? false : true
        cell.configureCell(text)
        return cell
    }
    
    func LoadCommonSingleImageTableViewCell(_ indexPath: IndexPath, _ text: String) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonSingleImageTableViewCell.cellIdentifire(), for: indexPath) as? CommonSingleImageTableViewCell else{
            fatalError("could not load CommonContentLabelForPopUpTableViewCell")
        }
        cell.singleImage.image = #imageLiteral(resourceName: "warning")
        return cell
    }
    
    func LoadCommonOkBtnTableViewCell(_ indexPath: IndexPath, _ text: String) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonOkBtnTableViewCell.cellIdentifire(), for: indexPath) as? CommonOkBtnTableViewCell else{
            fatalError("could not load CommonOkBtnTableViewCell")
        }
        cell.delegate = self
        cell.okButton.setTitle(text, for: .normal)
        return cell
    }
    
    func LoadCommonAcceptDeclinePopBtnTableViewCell(_ indexPath: IndexPath, _ textOne: String, _ textTwo: String) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonAcceptDeclinePopBtnTableViewCell.cellIdentifire(), for: indexPath) as? CommonAcceptDeclinePopBtnTableViewCell else{
            fatalError("could not load CommonAcceptDeclinePopBtnTableViewCell")
        }
        cell.delegate = self
        cell.noBtn.setTitle(textOne, for: .normal)
        cell.yesbtn.setTitle(textTwo, for: .normal)
        return cell
    }
    
}

extension ShowInfoCommonPopUpViewController:CommonOkBtnTableViewDelegate, CommonAcceptDeclinePopBtnTableViewCellDelegate{
    
    func acceptBtnTapped() {
        self.dismiss(animated: true)
        NotificationCenter.default.post(name: .yesBtnTapped, object: nil)
    }
        
    func declineBtnTapped() {
        self.dismiss(animated: true)
       // NotificationCenter.default.post(name: .noBtnTapped, object: nil)
    }
    
    func okButtonTapped() {
        self.dismiss(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name("OKButtonTapped"), object: nil)
    }
}



extension ShowInfoCommonPopUpViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
        //managePickerView()
    }
    
}

extension ShowInfoCommonPopUpViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
//
//    func managePickerView() {
//        let textField = activeTextField as UITextField
//        negotiableStatus.reloadAllComponents()
//        negotiableStatus.reloadInputViews()
//        textField.inputView = negotiableStatus
//        let index = negotiableOptions.index(of: "Non negotiable")
//        if let _index = index {
//            self.negotiableStatus.selectRow(_index, inComponent: 0, animated: true)
//        } else {
//            negotiableStatus.selectRow(0, inComponent: 0, animated: true)
//            activeTextField.text = negotiableOptions[0]
//            UserStore.shared.negotiableStatus = "\(0)"
//        }
//                }
//    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //        if pickerView == titlePickerView {
        //            return titleOptions.count
        //        } else {
        return negotiableOptions.count
        //        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //        if pickerView == titlePickerView {
        //            return titleOptions[row]
        //        } else {
        return negotiableOptions[row]
        //        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //        if pickerView == titlePickerView {
        //            activeTextField.text = titleOptions[row]
        //        } else {
        activeTextField.text = negotiableOptions[row]
        UserStore.shared.negotiableStatus = row
        //        }
    }
    
}
