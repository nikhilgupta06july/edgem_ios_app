//
//  CommonSingleTitleLabelCell.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 05/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class CommonSingleTitleLabelCell: UITableViewCell {
    
    @IBOutlet weak var contentIcon: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var deviderLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSet()
    }
    
    func initialSet(){
        contentLabel.textColor = theamBlackColor
//        contentLabel.font  = UIFont(name: QuicksandMedium, size: 15)
    }
    
    func configureCell(_ content: String){
        contentLabel.text = content
    }
    
    class func cellIdentifire()->String{
        return "CommonSingleTitleLabelCell"
    }
    
    class func cellheight()->CGFloat{
        return 174
    }
    
    
}
