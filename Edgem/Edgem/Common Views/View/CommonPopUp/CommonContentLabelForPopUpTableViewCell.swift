//
//  CommonContentLabelForPopUpTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 02/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class CommonContentLabelForPopUpTableViewCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSet()
    }
    
    func initialSet(){
        contentLabel.textColor = theamBlackColor
        contentLabel.font  = UIFont(name: QuicksandMedium, size: 15)
    }
    
    func configureCell(_ content: String){
        contentLabel.text = content
    }
    
    class func cellIdentifire()->String{
        return "CommonContentLabelForPopUpTableViewCell"
    }
    
    class func cellheight()->CGFloat{
        return 174
    }


}
