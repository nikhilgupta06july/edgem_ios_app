//
//  CommonDropDownTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 13/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class CommonDropDownTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var statusLabel: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
    }
    
    class func cellIdentifire() -> String{
        return "CommonDropDownTableViewCell"
    }
    
    func configureCell(with text: String){
        if text == ""{
            statusLabel.text = "Non negotiable"
        }else{
            statusLabel.text = text
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        statusLabel.layer.borderWidth = 1
        statusLabel.layer.borderColor = theamBorderGrayColor.cgColor
        statusLabel.layer.cornerRadius = 5
        statusLabel.clipsToBounds = true
        
    }
    
    

}
