//
//  CommonSingleImageTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 09/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class CommonSingleImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var singleImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    class func cellIdentifire() -> String {
        return "CommonSingleImageTableViewCell"
    }

}
