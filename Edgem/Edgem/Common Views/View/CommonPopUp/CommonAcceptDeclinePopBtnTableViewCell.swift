//
//  CommonAcceptDeclinePopBtnTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 02/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol CommonAcceptDeclinePopBtnTableViewCellDelegate{
    func acceptBtnTapped()
    func declineBtnTapped()
}

class CommonAcceptDeclinePopBtnTableViewCell: UITableViewCell {
    
    // MARK: --------- Properties
    var delegate: CommonAcceptDeclinePopBtnTableViewCellDelegate!
    var commonAcceptDeclinePopBtnTableViewCell: CommonAcceptDeclinePopBtnTableViewCell?
    
    // MARK: --------- @IBOutlets
    
    @IBOutlet weak var yesbtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetUP()
    }
    
    // MARK: --------- Private Functions
    
    func configureCell( _ acceptBtnTitle: String, _ declinrBtnTitle: String){
        yesbtn.setTitle(acceptBtnTitle, for: .normal)
        noBtn.setTitle(declinrBtnTitle, for: .normal)
    }
    
    class func cellIdentifire()->String{
        return "CommonAcceptDeclinePopBtnTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 44.0
    }
    
    func initialSetUP(){
        yesbtn.backgroundColor = theamAppColor
        yesbtn.titleLabel?.textColor = theamBlackColor
        yesbtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 15)
        yesbtn.layer.cornerRadius = 2
        yesbtn.clipsToBounds = true
        
        noBtn.backgroundColor = theamAppColor
        noBtn.titleLabel?.textColor = theamBlackColor
        noBtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 15)
        noBtn.layer.cornerRadius = 2
        noBtn.clipsToBounds = true
    }
    
     // MARK: --------- @IBActions
    
    @IBAction func yesBtnTapped(_ sender: UIButton) {
        delegate.acceptBtnTapped()
    }
    
    
    @IBAction func noBtnTapped(_ sender: UIButton) {
        delegate.declineBtnTapped()
    }
    
}
