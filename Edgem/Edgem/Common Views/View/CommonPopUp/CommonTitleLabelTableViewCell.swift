//
//  CommonTitleLabelTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 19/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class CommonTitleLabelTableViewCell: UITableViewCell {
    
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentLabel.numberOfLines = 0
    }
    
    func configureCell(_ content: String){
        contentLabel.text = content
        
        contentLabel.font = UIFont(name: QuicksandMedium, size: 18)
          contentLabel.textColor = theamBlackColor
    }

    class func cellIdentifire() -> String {
        return "CommonTitleLabelTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 20
    }
    

}
