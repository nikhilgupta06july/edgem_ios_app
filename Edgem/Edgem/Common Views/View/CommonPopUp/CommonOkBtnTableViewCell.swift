//
//  CommonOkBtnTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 24/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

  protocol CommonOkBtnTableViewDelegate{
    func okButtonTapped()
}

class CommonOkBtnTableViewCell: UITableViewCell {
    
    var delegate: CommonOkBtnTableViewDelegate!

    @IBOutlet weak var okButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        okButton.setTitle("Ok", for: .normal)
        okButton.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        //okButton.titleLabel?.textColor = theamBlackColor
    }
    
    class func cellIdentifire()->String{
        return "CommonOkBtnTableViewCell"
    }

    class func cellheight()->CGFloat{
        return 45
    }
    
    
    @IBAction func okButtonTapped(_ sender: UIButton) {
        delegate.okButtonTapped()
    }
    
}
