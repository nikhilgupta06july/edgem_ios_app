//
//  OtherAddressTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 16/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol OtherAddressTableViewCellDelegate{
    func otherSelectionButtonTapped(_ status: Bool)
}

class OtherAddressTableViewCell: UITableViewCell {
    
    // MARK: -------- Properties
    let addressObject: [String:AnyObject] = [:]
    var otherAddressTableViewCellDelegate: OtherAddressTableViewCellDelegate?
    
    // MARK: -------- @IBOutlets
    
    @IBOutlet weak var addressIcon: UIImageView!
    @IBOutlet weak var selectionButton: UIButton!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        // Initialization code
        selectionButton.setImage(UIImage(named: "ActiveRadio"), for: .selected)
        selectionButton.setImage(UIImage(named: "InActiveRadio"), for: .normal)
        self.addressLabel.text = "Others".localized
    }
    
    func configureCell(userAddress: UserAddress, _ status: Bool){
        if userAddress == nil{
            return
        }
        //self.userAddressProperties = userAddress
        
        // Location
        self.addressTextField.text = userAddress.addressLocation
        
        //Label
        //self.addressLabel.text = userAddress.addressLabel
        
        // Icon
         addressIcon.image = UIImage(named: "Others_Address")
        
        //selection button
        selectionButton.isSelected = status
    }
    
    class func cellIdentifire()-> String{
        return "OtherAddressTableViewCell"
    }
    
    class func cellHeight()-> CGFloat{
        return  90//62.0//96
    }
    
    @IBAction func otherSelectionButtonTapped(_ sender: UIButton) {
        if sender.isSelected == false{
            sender.isSelected = true
            
            //addressObject["Label" : ]
        }else{
            sender.isSelected = false
        }
        otherAddressTableViewCellDelegate?.otherSelectionButtonTapped(sender.isSelected)
    }
}
