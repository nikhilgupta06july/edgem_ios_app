//
//  AddressTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 16/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol AddressTableViewCellDelegate{
    func selectedLocation(_ sender: UIButton)
}

class AddressTableViewCell: UITableViewCell {
    
    // MARK: ------- Properties
    var userAddressProperties: UserAddress!
    var userAddressPropertiesToSend: [String : AnyObject] = [:]
    var addressTableViewCellDelegate: AddressTableViewCellDelegate?
    
     // MARK: ------- @IBOutlets
    @IBOutlet weak var deviderLine: UIView!
    
    @IBOutlet weak var greyDeviderLine: UIView!
    @IBOutlet weak var selectionButton: UIButton!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressIcon: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionButton.setImage(UIImage(named: "ActiveRadio"), for: .selected)
        selectionButton.setImage(UIImage(named: "InActiveRadio"), for: .normal)
    }
    
    func configureCell(userAddress: UserAddress, index: Int, selected: Bool){
        if userAddress == nil{
            return
        }
    
        self.userAddressProperties = userAddress
        
        // selection button icon
        //selectionButton.isSelected = status
        selectionButton.tag = index
    
        self.selectionButton.isSelected = selected
        // Location
        self.address.text = userAddress.addressLocation
        
        //Label
        self.addressLabel.text = userAddress.addressLabel
        
        addressIcon.image = userAddress.addressLabel == "Home" ? UIImage(named: "home") : userAddress.addressLabel == "Work" ? UIImage(named: "Work") : UIImage(named: "Others_Address")
        deviderLine.isHidden = userAddress.addressLabel != "Others"
        greyDeviderLine.isHidden = userAddress.addressLabel != "Others"
        
        address.placeholder = "Add Location"
        
        address.isEnabled = userAddress.addressLabel == "Others"
        
    }

    class func cellIdentifire()-> String{
        return "AddressTableViewCell"
    }
    
    class func cellHeight()-> CGFloat{
        return 70//62.0
    }
    
    @IBAction func selectionButtonTapped(_ sender: UIButton) {
        addressTableViewCellDelegate?.selectedLocation(sender)
    }
    
}
