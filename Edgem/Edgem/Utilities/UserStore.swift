//
//  UserStore.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 02/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation

class UserStore: NSObject {
    
    static let shared = UserStore()
    let userDefaults = UserDefaults.standard
    
    fileprivate override init() {
        super.init()
}
    
    var selectedLanguageName: String {
        set {
            userDefaults.setValue(newValue, forKey: SelectedLanguage)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: SelectedLanguage) as? String ?? "English"
        }
    }
    
    var selectedLanguageCode: String {
        set {
            userDefaults.setValue(newValue, forKey: SelectedLanguageCode)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: SelectedLanguageCode) as? String ?? "en"
        }
    }
    
    var selectedUserType: String {
        set{
            userDefaults.set(newValue, forKey: UserType)
            userDefaults.synchronize()
        }
        get{
            return  userDefaults.value(forKey: UserType) as? String ?? ""
        }
    }
    
    var isLoggedIn: Bool {
        set {
            userDefaults.setValue(newValue, forKey: IsLoggedIn)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: IsLoggedIn) as? Bool ?? false
        }
    }
    
    var notificationPermissionsAsked: Bool {
        set {
            userDefaults.setValue(newValue, forKey: NotificationPermissionsAsked)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: NotificationPermissionsAsked) as? Bool ?? false
        }
    }
    
    var isParrent: Bool{
        set{
            userDefaults.set(newValue, forKey: IsParrent)
            userDefaults.synchronize()
        }get{
            return (userDefaults.value(forKey: IsParrent) as? Bool ?? false)
        }
    }
    
    var userEmail: String?{
        set{
            userDefaults.setValue(newValue, forKey: UserEmail)
            userDefaults.synchronize()
        }
        get{
            return userDefaults.value(forKey: UserEmail) as? String
        }
    }
    
    var logInType: Int?{
        set{
            userDefaults.setValue(newValue, forKey: loginType)
            userDefaults.synchronize()
        }
        get{
            return userDefaults.value(forKey: loginType) as? Int
        }
    }
    
    var userID: Int?{
        set{
            userDefaults.setValue(newValue, forKey: UserID)
            userDefaults.synchronize()
        }
        get{
            return userDefaults.value(forKey: UserID) as? Int
        }
    }
    
    var token: String {
        set {
            userDefaults.setValue(newValue, forKey: Token)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: Token) as? String ?? ""
        }
    }
    
    var isNotificationUnAuthorized: Bool {
        set {
            userDefaults.setValue(newValue, forKey: NotificationUnAuthorized)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: NotificationUnAuthorized) as? Bool ?? false
        }
        
    }
    
    var deviceToken: String {
        get {
            return userDefaults.value(forKey: DeviceToken) as? String ?? ""
        }
        set {
            userDefaults.setValue(newValue, forKey: DeviceToken)
            userDefaults.synchronize()
        }
    }
    
    var isNotificationsStatus: Bool {
        set {
            userDefaults.setValue(newValue, forKey: IsNotificationStatus)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: IsNotificationStatus) as? Bool ?? false
        }
    }
    
    var hasLoginKey: Bool {
        set {
            userDefaults.setValue(newValue, forKey: HasLoginKey)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: HasLoginKey) as? Bool ?? false
        }
        
    }
    
    var touchIDEmail: String? {
        set {
            userDefaults.setValue(newValue, forKey: TouchIDEmail)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: TouchIDEmail) as? String
        }
    }
    
    var bitlyURL: String? {
        set {
            userDefaults.setValue(newValue, forKey: BitlyURL )
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: BitlyURL ) as? String
        }
    }
    
//    var selectedSubjects: Subject{
//        set{
//            userDefaults.setValue(newValue, forKey: selectedSubject)
//            userDefaults.synchronize()
//        }
//        get{
//            return (userDefaults.value(forKey: selectedSubject) as? Subject ?? nil)!
//        }
//    }
    
    var isBioMetricEnabledByUser: Bool {
        set {
            userDefaults.setValue(newValue, forKey: BioMetricEnabledByUser)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: BioMetricEnabledByUser) as? Bool ?? false
        }
    }
    
    var isWalkthroughCompleted: Bool {
        set {
            userDefaults.setValue(newValue, forKey: WalkThrough)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: WalkThrough) as? Bool ?? false
        }
    }
    
    var offerIDNTotalAmmount: Dictionary<String, AnyObject>? {
        set {
            userDefaults.setValue(newValue, forKey: OfferIDNTotalAmmount)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: OfferIDNTotalAmmount) as? [String:AnyObject] ?? [:]
        }
    }
    
    var negotiableStatus: Int?{
        set {
            userDefaults.setValue(newValue, forKey: NegotiableStatus)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: NegotiableStatus) as? Int
        }
    }
    
    var stripeError: String?{
        set {
            userDefaults.setValue(newValue, forKey: StripeError )
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: StripeError ) as? String
        }
    }
}
