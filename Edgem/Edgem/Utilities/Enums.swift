//
//  Enums.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 02/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        
        let path = Bundle.main.path(forResource: UserStore.shared.selectedLanguageCode, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

enum PathURl: String {
    
    case Edgem_Socket                       = ":6001"
    case VerifySocialID                         = "verify-socialId"
    case VerifyReferralCode                 = "verify_rcode"
    case VerifyEmail                              = "verify-email/"
    case VerifyMobile                            = "verify-mobile"
    case AccountActivate                      = "account-activate"
    case VerifyRequestedOtp               = "verify_requested_otp"
    case UserLogin                                = "user-login"
    case ForgotPassword                      = "forgot-password"
    case VerifyForgotPassword            = "forgot-verify"
    case PasswordUpdate                     = "password-update"
    case verifyUserName                      = "verify-username/"
    case signup                                      = "add-user"
    case resendOTP                              = "resend-otp/"
    case AddAddress                             = "add-addresses"
    case UserTempAddresses               = "user-temp-addresses/"
    case AddressUpdate                       = "address-update"
    case Dashboard                               = "dashboard/"
    case userDetails                               = "user-info/"
    case Subjects                                   = "get-subjects"
    case Qualifications                           = "get-qualifications"
    case Levels                                       = "get-levels"
    case Filter_Tutor                                = "filter-tutor?"
    case ListOffer                                     = "list_offer?"
    case Filter_Student                           = "filter-student?"
    case UpdateUserInfo                        = "update-user-info"
    case TutorProfileUpd                         = "tutor_profile_upd"
    case AddBookMark                           = "add-bookmark"
    case RemoveBookmark                    = "remove-bookmark"
    case GetBookmarks                          = "get-bookmarks"
    case getStudentlevelNSubject         = "get-student-levelNsubject/"
    case postStudentlevelNSubject       = "post-student-levelNsubject"
    case ChangePassword                      = "change-password"
    case ContactRequest                        = "contact-request"
    case storeUserCard                          = "store-user-card"
    case userDetailsData                         = "user_details/"
    case Blank                                         = ""
    case UploadTutorPicture                   = "tutor_picture"
    case ManageBankDetails                  = "manage_bank_details"
    case ViewBankDetails                        =  "view_bank_details"
    case UserRatingData                          = "user_ratings/"
    case GetBanks                                    = "get-banks"
    case userDocumentUpload                = "user_document_upload"
    case PostTutorSubjectRate                 = "tutor_add_subject"
    case UsersCards                                 = "users-cards/"
    case DeleteUserCard                         = "delete-user-card/"
    case TutorAddQualification                = "tutor_add_qualification"
    case MakeOffer                                   = "make_offer"
    case CancelOffer                                 = "cancel_offer/"
    case CancelPaidOffer                        = "cancel_paid_offer/"
    case AcceptOffer                                =  "accept_offer/"
    case EditUpdateOffer                          = "edit_offer"
    case CounterOffer                               = "counter_offer"
    case notifications                                 = "register-device"
    case notificationStatus                        = "  "
    case TutorSubjectOffered                    = "tutor_subject_offered/"
    case UserLogout                                    = "user-logout"
    case WalletAddMoney                          = "wallet_add_money"
    case PayForOffer                                    = "pay_for_offer"
    case GetAllMessages                             = "get_all_messages/"
    case SendMessage                                = "send_message"
    case GetUserActivity                             = "get_user_activity"
    case WalletGetTransactions                   =  "wallet_get_transactions"
    case DeleteUserActivity                         = "delete_user_activity"
    case PostUserAvailability                         =   "post-user-availability"
    case GetUserAvailability                          = "get-user-availability/"
    case GetUserBooking                              = "get-user-booking/"
    case CheckUserWeeklyAvailability            = "check_user_weekly_availability"
    case CheckinForClass                              = "checkin_for_class"
    case FinishClass                                        = "finish_class"
    case ManageSchedule                             = "manage_schedule"
    case DisconnectUser                                 = "disconnect_user/"
    case ScheduleClass                                  = "schedule_class"
    case RequestOtp                                     =  "request_otp"
    case RateUser                                          = "rate_user"
    case RecommendedTutors                         = "recommended-tutors"
    case GetFilteredSchool                              = "get-filtered-school"
    case GetBookingDetails                            = "get-booking-details/"
}

enum AppStoryboard : String {
    
    case Main, Dashboard, Search, Chat, Activity, Profile, CommonPopUpVC
    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
    
}

// MARK: Custom layout overrides

enum CustomButtonType: Int {
    case clear = 0, white, dual
}

enum OTPTextFieldNum: Int{
    case textFieldOne = 1, textFieldTwo , textFieldThree , textFieldFour
}
enum ResponseStatusCode:Int {
    case notFound = 404
    case badRequest = 400
    case timeout = 408
    case internalServer = 500
    case gotResponse = 200
}


enum ResponseError: Error {
    case notFoundError
    case badRequestError
    case timeoutError
    case internalServerError
    case parseError
    case unboxParseError
    case apiFailureError(message:String)
    case unknownError
    
    func description() -> String {
        switch self {
        case .notFoundError:
            return "URL not found!!!".localized
        case .badRequestError:
            return "Edgem.API.BadRequest".localized
        case .timeoutError:
            return "Edgem.API.RequestTimeout".localized
        case .internalServerError:
            return "Edgem.API.InternalServerError".localized
        case .parseError:
            return "Something went wrong. Please try again.".localized//"Edgem.API.ParseError".localized
        default:
            return "Edgem.API.UnknownError".localized
        }
    }
}

enum ValidationErrorMessage: String {
    case email_username_Empty
    case emailEmpty
    case emailInvalid
    case emailMismatch
    case passwordEmpty
    case passwordInvalid
    case passwordMismatch
    case accountMissmatch
    case title
    case countryCode
    case gender
    case firstName
    case bankName
    case lastName
    case displayName
    case referralCode
    case birthday
    case address
    case postalCode
    case country
    case phoneEmpty
    case accountEmpty
    case phoneInvalid
    case accountInvalid
    case phoneMismatch
    case subject
    case level
    case qualification
    case teachingSince
    case sessionRate
    case monthRate
    case subjectAddress
    case distance
    case schedule
    case experience
    case studentName
    case postalCodeEmpty
    case address1
    case address2
    case mrtlrtstation
    case relationWithChild
    case referralCodeInValid
    
    func description() -> String {
        switch self {
        case .emailEmpty:
            return "Please enter an email address.".localized
        case .referralCode:
            return "Please enter a referral code.".localized
        case .referralCodeInValid:
            return "Please enter a valid referral code.".localized
        case .emailInvalid:
            return "Please enter a valid email address.".localized
        case .emailMismatch:
            return "Both emails did not match. Please try again.".localized
        case .passwordEmpty:
            return "Please enter password.".localized
        case .passwordInvalid:
            return "Password length should be \(PasswordMinLimit) characters or more.".localized
        case .passwordMismatch:
            return "Passwords do not match.".localized
        case .accountMissmatch:
            return "Both account numbers do not match.".localized
        case .title:
            return "Please select a title.".localized
        case .countryCode:
            return "Please select a country code.".localized
        case .gender:
            return "Please select a gender.".localized
        case .firstName:
            return "Please enter your first name.".localized
        case .bankName:
            return "Please enter your bank name.".localized
        case .lastName:
            return "Please enter your last name.".localized
        case .displayName:
            return "Username should not be empty.".localized
        case .birthday:
            return "Please select birthday.".localized
        case .address:
            return "Please enter address line 1.".localized
        case .postalCode:
            return "Must be \(PostalCodeMinLimit) characters".localized
            
        case .country:
            return "Please select a country.".localized
        case .phoneEmpty:
            return "Please enter telephone no.".localized
        case .accountEmpty:
            return "Please enter a bank account number.".localized
        case .phoneInvalid:
            return "Please enter valid telephone no.".localized
        case .accountInvalid:
            return "Please enter valid account no.".localized
        case .phoneMismatch:
            return "Both contact numbers did not match. Please try again.".localized
        case .subject:
            return "Please select subject.".localized
        case .level:
            return "Please select level.".localized
        case .qualification:
            return "Please select qualification.".localized
        case .teachingSince:
            return "Please select the year you first taught this subject.".localized
        case .sessionRate:
            return "Please enter your rate per session.".localized
        case .monthRate:
            return "Please enter your rate per month.".localized
        case .subjectAddress:
            return "Please provide an address.".localized
        case .distance:
            return "Please enter distance".localized
        case .schedule:
            return "Please provide your availability.".localized
        case .experience:
            return "Please select the preferred experience".localized
        case .studentName:
            return "Please enter student name.".localized
        case .postalCodeEmpty:
            return "Please enter postal code.".localized
        case .address1:
             return "Please enter address1.".localized
        case .address2:
             return "Please enter address2.".localized
        case .mrtlrtstation:
             return "Please enter MRT/LRT Station.".localized
        case .relationWithChild:
            return "Please enter relation with child".localized
        case .email_username_Empty:
            return "Mobile/Email should not be empty"
        }
    }
}

enum AlertMessage: String {
    case removeFieldData
    case resetPasswordSuccess
    case exitSignupProcess
    case invalidInput
    case noInternetConnection
    case changePasswordSuccess
    case changeEmailSuccess
    case changeMobileSuccess
    case fbAccountDoesntExist
    case gmailAccountDoesntExist
    case changePhoneNumber
    case docDesc
    case docDescRename
    case addSubjectSuccess
    case deleteSubject
    case editShortlistComment
    case delete
    case newShortlistComment
    case removeShortlist
    case tPakRenewal
    case suggestSubject
    case subjectLimit
    case wrongPostalCode
    case somethingWentWrong
    case cancleOffer
    case cancleOfferBefore24
    case cancleOfferAfter24
    case tuttorArrived
    case notificationPermissionTitle
    case notificationPermissionMessage
    
    func description() -> String {
        switch self {
        case .removeFieldData:
            return "Data entered will be lost once you leave this page. \nDo you want to proceed?".localized
        case .resetPasswordSuccess:
            return "Password has been successfully changed. Please login to continue.".localized
        case .exitSignupProcess:
            return "Data entered will be lost once you leave this page. \nDo you want to proceed?"
        case .invalidInput:
            return "Please enter valid inputs.".localized
        case .noInternetConnection:
            return "No active internet connection. Please check your wifi or mobile data settings.".localized
        case .changePasswordSuccess:
            return "Password has been successfully changed".localized
        case .changeEmailSuccess:
            return "Email has been successfully changed".localized
        case .changeMobileSuccess:
            return "Contact number has been successfully changed".localized
        case .fbAccountDoesntExist:
            return "There is no account associated with this Facebook account. Do you want to Sign Up using this account?".localized
        case .changePhoneNumber:
            return "Please enter the new phone number in the text field.".localized
        case .docDesc:
            return "Please enter name for the selected document.".localized
        case .docDescRename:
            return "Please enter new name for the document.".localized
        case .addSubjectSuccess:
            return "Subject added successfully. You will receive an instant notification from us shortly when there's a match. You may also widen your search in Find Now.".localized
        case .deleteSubject:
            return "Are you sure you want to delete the subject?".localized
        case .editShortlistComment:
            return "Edit your comments here".localized
        case .delete:
            return "Are you sure, you want to delete?".localized
        case .newShortlistComment:
            return "Save to My Shortlist".localized
        case .removeShortlist:
            return "Are you sure, you want to remove this user from shortlist?".localized
        case .tPakRenewal:
            return "Your T.Pak subscription has expired. An active subscription is required for subject, message and review management.".localized
        case .suggestSubject:
            return "Name of the New Subject".localized
        case .subjectLimit:
            return "Cannot select more than 4 subjects".localized
        case .wrongPostalCode:
            return "Please enter a valid Postal Code"
        case .somethingWentWrong:
             return "Something went wrong please try again"
        case .gmailAccountDoesntExist:
            return "There is no account associated with this Gmail account. Do you want to sign up using this account?".localized
        case .cancleOffer:
            return "Are you sure you want to cancel?".localized
        case .cancleOfferBefore24:
            return "Are you sure you wish to cancel tuition?\n\nYou will be charged a 10% cancellation fee of the total tuition fees. You may choose to reschedule tuition instead.\n\nPlease click Confirm to proceed with cancellation otherwise click Cancel.".localized
        case .cancleOfferAfter24:
            return "Please click Confirm to cancel. There will be no cancellation fees".localized
        case .notificationPermissionMessage:
            return "Please grant location permission to find resources nearby.".localized
        case .notificationPermissionTitle:
            return "Notification Permission".localized
        case .tuttorArrived:
            return "Please press this only when you have arrived at destination".localized
            
        }
    }
}

//MARK:- Alert Button Strings
enum AlertButton: String {
    case yes
    case no
    case ok
    case later
    case confirm
    case cancel
    case findNow
    case submit
    case renewNow
    case proceed
    case settings
    case close
    case enquire
    case sendMessage
    case next
    case skip
    case done
    
    func description() -> String {
        switch self {
        case .yes:
            return "Yes".localized
        case .no:
            return "No".localized
        case .ok:
            return "Ok".localized
        case .confirm:
            return "Confirm".localized
        case .later:
            return "Later".localized
        case .cancel:
            return "Cancel".localized
        case .findNow:
            return "Find Now".localized
        case .submit:
            return "Submit".localized
        case .renewNow:
            return "Renew Now".localized
        case .proceed:
            return "Proceed".localized
        case .settings:
            return "Settings".localized
        case .close:
            return "Close".localized
        case .enquire:
            return "Enquire".localized
        case .sendMessage :
            return "Send Message".localized
        case .next:
            return "NEXT".localized
        case .done:
            return "DONE".localized
        case .skip:
              return "SKIP".localized
        }
    }
}

enum CustomAlertMessages: String {
    case AuthenticationErrorMessage
    case ShowPopUPSelectUser
    case saveImage
    case shareQRCode
    case verificationNotify
    case OTPResend
    case ContactUsMsg
    case camera
    case photoLibrary
    case documents
    case certificatePending
    case cancleOffer
    case cancleOfferBefore24
    case cancleOfferAfter24
    case add_bank_details_credit_card_err
    case add_credit_card_err
    case add_bank_details_err
    case title
    case feeDeposited
    
    func description() -> String {
        switch self {
        case .AuthenticationErrorMessage:
            return "New Log-in on other device, your session expired and will be redirected to login screen.".localized
        case .ShowPopUPSelectUser:
            return "Please select whether you are signing up as a student or tutor.".localized
        case .saveImage:
            return "Save Image".localized
        case .shareQRCode:
            return "Share QR Code".localized
        case .verificationNotify:
            return "Your child will be notified that you have created an account. He/She will be sent a verification code to create an account which will be linked to your account.".localized
        case .OTPResend:
            return "OTP has been resent".localized
        case .ContactUsMsg:
            return "Thank you for your message. We will get back to you shortly via email.".localized
        case .camera:
            return "Camera".localized
        case .photoLibrary:
            return "Photo Library".localized
        case .documents:
             return "Documents".localized
        case .cancleOffer:
            return "CANCEL OFFER".localized
        case .certificatePending:
             return "Your_qualifications_have_been_updated_successfully".localized//"Certificates are pending \n approval".localized
        case .title:
            return "Please select an option \n from below".localized
        case .feeDeposited:
            return "Tuition fee deposited".localized
        case .cancleOfferBefore24:
            return "Please click Confirm to cancel. There will be no cancellation fees".localized
        case .cancleOfferAfter24:
            return "Are you sure you wish to cancel tuition?\n\nYou will be charged a 10% cancellation fee of the total tuition fees. You may choose to reschedule tuition instead.\n\nPlease click Confirm to proceed with cancellation otherwise click Cancel.".localized
        case .add_bank_details_credit_card_err:
             return "Please add your bank details and at least one credit card first.".localized
        case .add_credit_card_err:
            return "Please add at least one credit card.".localized
        case .add_bank_details_err:
            return "Please add your bank account details.".localized
        }
    }
}

enum ToolTipString: String {
    case levelInformation
    case subjectInformation
    case info
    case subjectInfo
    
    func description() -> String {
        switch self {
            
        case .levelInformation:
            return "State your current education level \n ".localized
            
        case .subjectInformation:
            return "State the subjects which you require a \n tutor for".localized
            
        case .info:
            return "You will have limited access until your certificates are verified. Your certificates will be kept confidential and are only for verification purposes.".localized
            
        case .subjectInfo:
            return "If you choose to lock your rate, students cannot offer a rate lower than this amount.".localized
            
        }
    }
}

enum GeneralString: String {
    case personalInformation
    case parent
    case child
    case address
    case student
    case tutor
    
    func description() -> String {
        switch self {
        case .personalInformation:
            return "Personal Information".localized
        case .parent:
            return "Parent".localized
        case .child:
            return "Student".localized
        case .address:
            return "Address".localized
        case .student:
            return "Student".localized
        case .tutor:
            return "Tutor".localized
        }
    }
}


// MARK: - TextFields

enum TutorProfileTextFieldType: Int {
    case firstName = 0, lastName, gender, birthday, emailAddress, dual,homeAddress, workAddress
}

enum CreateAccountTutorTextFieldType: Int {
    case firstName = 1, lastName,  gender, birthday, emailAddress
}

enum EdgemOfferTextField: Int {
    case perHour = 0, forHours, forSubjects, selectLevel, forLessons
}

enum BankDetailTextFieldType: Int {
    case fullName = 0, bankName, accountNumber, reAccountBankNumber
}

enum CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero: Int{
    case parentfirstName = 1, parentlastName,  relationshipwithchild, parentemailaddress
}

enum CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne: Int{
     case firstName = 1, lastName,  gender, birthday, emailAddress,contactNumber
}

enum CreatAccountStudentStepTwoTextFieldType: Int{
    case postalcode = 1, addressLine1, addressLine2, mrtlrtstation
}

enum CreatAccountStudentStepTwoDualTextFieldType: Int{
    case dual = 6
}

enum CreateUserTextFieldType: Int {
    case userName = 1, password,  confirmPassword, referralCode
}

enum AddOrSaveTextField: Int {
    case address = 1
}

enum VerifyPhoneNumberTextFieldType: Int {
    case dual = 1
}

enum VerifyEmailAndContactTextFieldType: Int {
    case dual = 0
}

enum VerifyEmailAndContactTextFieldTypeEmail: Int {
    case email = 0
}
enum ResetPasswordTextFieldType: Int{
    case password = 1, confirmPassword
}

enum ChangePasswordTextFieldType: Int{
    case currentPassword = 0 , newPassword , confirmPassword
}

enum responseMessagesForUserAvailable: String{
    case isexist
    case isNotExist
    
    func description()->String{
        switch self{
        case .isexist :
                                return "You may continue."
        case .isNotExist :
                                return "Sorry! You are already registered with Us. Please ACTIVATE your account!"
        }
    }
}

enum completionStatus: String {
    case Active
    case partiallyActive
    case InActive
    
    func status () ->String {
        return self.rawValue
    }
}

//enum FilterTableViewSectionType: Int {
//    case Qualification=0, Subjects, Level, Gender, TutionRate, Rating
//}
enum FilterScreenCategoryTypeStudent: Int {
    case Qualification = 0, Subjects, Level //Gender, Location, Availibility, Rating
}

enum FilterTableViewSectionType: Int {
    case Qualification = 0, Subjects, Level, Gender, Location, Availibility, Rating, Selection 
}

enum AddNewCardTextFieldType: Int{
    case CardNumber = 1, CardHolderName, Month, Year, CVV
}

enum LevelSubjectTableViewType: Int {
    case Level = 0, Subjects
}

enum FilterTableViewSectionTypeTutor: Int {
    case  Subjects = 0, Level, Location, Availibility, Selection
}

enum BottomPopUpVCSectionType: Int{
    case Preferred = 0, TutionRate, Ratings
}

enum QualificationNExperienceVCSectionType: Int {
    case SelectYourQualifications = 0, Experience,Education, Certificates
}

enum PaginationType: Int{
    case new, old,  reload
}

enum FilterUserType: Int{
    case student = 1, tutor
}

enum CardTypes: String{
     case Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay, Meastro
    
    func description()->String{
        switch self{
            
        case .Unknown :
            return "Unknown"
            
        case .Amex:
            return "Amex"
            
        case .Visa:
            return "Visa"
            
        case .MasterCard:
            return "MasterCard"
            
        case .Diners:
            return "Diners"
            
        case .Discover:
            return "Discover"
            
        case .JCB:
            return "JCB"
            
        case .Elo:
            return "Elo"
            
        case .Hipercard:
            return "Hipercard"
            
        case .UnionPay:
            return "UnionPay"
            
        case .Meastro:
            return "Meastro"
        }
    }
}
