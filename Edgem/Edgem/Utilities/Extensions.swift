//
//  Extensions.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 02/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation

extension UIViewController {
    
    class var storyboardID : String {
        return "\(self)"
    }
    
    static func instantiateFromAppStoryboard(appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
}


extension String {
    
    func widthWithConstrainedHeight(height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return boundingBox.width
    }
    
    
    func isValidAmmount() -> Bool {
        if self.isEmpty {
            return false
        }
        let ammountRegEx = "(\\d+(?:\\.\\d+)?)"
        
        let ammountTest = NSPredicate(format:"SELF MATCHES %@", ammountRegEx)
        return ammountTest.evaluate(with: self)
    }
    
    func isValidEmailID() ->Bool {
        if self.isEmpty {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidReferralCode() -> Bool {
        let userNameRegex =  "^(?=.{2,32}$)(?![_.0-9])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"
        
        let userNameTest = NSPredicate(format:"SELF MATCHES %@", userNameRegex)
        
        let dotArray = self.components(separatedBy: ".")
        let underScoreArray = self.components(separatedBy: "_")
        return userNameTest.evaluate(with: self) && dotArray.count - 1 <= 1 && underScoreArray.count - 1 <= 1
    }
    
    func isValidUserName() -> Bool {
        let userNameRegex =  "^(?=.{2,32}$)(?![_.0-9])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"
        
        let userNameTest = NSPredicate(format:"SELF MATCHES %@", userNameRegex)
        
        let dotArray = self.components(separatedBy: ".")
        let underScoreArray = self.components(separatedBy: "_")
        return userNameTest.evaluate(with: self) && dotArray.count - 1 <= 1 && underScoreArray.count - 1 <= 1
    }
    
    
    func isEmptyString() -> Bool {
        let trimmedString = self.components(separatedBy: .whitespacesAndNewlines).joined(separator: "")
        return trimmedString.isEmpty
    }
    
    
    
    
    func isValidPassword() -> Bool {
        if self.isEmptyString() || self.count < PasswordMinLimit {
            return false
        }
        return true
    }
    
    func isValidPostalCode() -> Bool {
        if self.count < PostalCodeMinLimit || self.count > PostalCodeMaxLimit {
            return false
        }
        return true
    }
    
    func isValidPhoneNumber() -> Bool {
        if self.isEmptyString() || self.count < PhoneNumberMinLimit {
            return false
        }
        return true
    }
    
    func isValidAccountNumber() -> Bool {
        if self.isEmptyString() || self.count < AccountNumberMinLimit {
            return false
        }
        return true
    }
    
    func isValidName() -> Bool {
        if self.isEmptyString() || self.count < NameMinLimit {
            return false
        }
        return true
    }
    
    func isValidAddress() -> Bool {
        if self.isEmptyString() || self.count < AddressMinLimit {
            return false
        }
        return true
    }
    
    func isValidCity() -> Bool {
        if self.isEmptyString() || self.count < CityMinLimit {
            return false
        }
        return true
    }
    
    func isFeedBackMessage() -> Bool {
        if self.isEmptyString() || self.count < MessageMinLimit {
            return false
        }
        return true
    }
    
    func isValidCardNumber() -> Bool {
        if self.isEmptyString() || self.count < CardNumberMinLimit {
            return false
        }
        return true
    }
    
    func isValidCardExpirationDate() -> Bool {
        if self.isEmptyString() || self.count < CardExpirationLimit {
            return false
        }
        return true
    }
    
    func isValidCardCVV() -> Bool {
        if self.isEmptyString()
            || self.count < CardCVVMinDigits
            || self.count > CardCVVMaxDigits {
            return false
        }
        
        return true
    }
    
    func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
        let disallowedCharacterSet = NSCharacterSet(charactersIn: matchCharacters).inverted
        return (self.rangeOfCharacter(from: disallowedCharacterSet) == nil)
    }
    
    mutating func convertHtml(fontFamilyName: String, fontSize: Int) -> NSMutableAttributedString{
        let aux = "<span style=\"font-family: \(fontFamilyName); font-size: \(fontSize)\">\(self)</span>"
        
        
        guard let data = aux.data(using: .utf8) else { return NSMutableAttributedString() }
        do{
            let htmlConvertedString = try NSMutableAttributedString(data: data,                                           options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            return htmlConvertedString
        }catch{
            return NSMutableAttributedString()
        }
    }
    
    var containsWhitespace : Bool {
        return(self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
    
    
    func formattedDate(_ date: String, format: String)-> String?{
    
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = format
        
        guard let date = dateFormatter.date(from: self) else {
            print(value: "Take a look to your format")
            
            return  nil
        }
        
        dateFormatter.dateFormat = "dd-MMM-yy"
        
        let dateSTR = dateFormatter.string(from: date)
        
        if !dateSTR.isEmpty {
            
            return dateSTR
            
        }else{
            
            print(value: "date not found")
            
            return nil
        }
    }
}

extension String {
    
    var NoWhiteSpace : String {
        
        var miaStringa = self as NSString
        
        if miaStringa.contains(" "){
            
            miaStringa =  miaStringa.replacingOccurrences(of: " ", with: "") as NSString
        }
        return miaStringa as String
        
    }
    
    
}

extension String {
    
    func separate(every: Int, with separator: String) -> String {
        return String(stride(from: 0, to: Array(self).count, by: every).map {
            Array(Array(self)[$0..<min($0 + every, Array(self).count)])
            }.joined(separator: separator))
    }
    
    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        guard let date = dateFormatter.date(from: self) else {
            preconditionFailure("Take a look to your format")
        }
        return date
    }
    
    func stringToDate(date:String, format: String) -> Date?{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        guard let date = dateFormatter.date(from: self) else {
           
            return nil
        }
       return date
    }
    
    
    func stringToDateWithOutUTC(date:String, format: String) -> Date?{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format

        guard let date = dateFormatter.date(from: self) else {
           // preconditionFailure("Take a look to your format")
            print(value: "Take a look to your format")
            return Date()
        }
        return date
    }

}

extension UIApplication {
    class func topViewController(viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(viewController: nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(viewController: selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(viewController: presented)
        }
        return viewController
    }
}


extension UILabel {
    
    func heightForText() -> CGFloat {
        let constraintRect = CGSize(width: self.frame.width, height: .greatestFiniteMagnitude)
        let boundingBox = self.text?.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : self.font], context: nil)
        
        return ceil(boundingBox!.height)
    }
    
}

extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    enum ScreenType: String {
        case iPhones_4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhones_X_XS = "iPhone X or iPhone XS"
        case iPhone_XR = "iPhone XR"
        case iPhone_XSMax = "iPhone XS Max"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhones_4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1792:
            return .iPhone_XR
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhones_X_XS
        case 2688:
            return .iPhone_XSMax
        default:
            return .unknown
        }
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

extension UIButton{
    func hasImage(named imageName: String, for state: UIControl.State) -> Bool {
        guard let buttonImage = image(for: state), let namedImage = UIImage(named: imageName) else {
            return false
        }
        return buttonImage.pngData() == namedImage.pngData()
    }
}

extension Notification.Name{
     static let filterAvailabilitySelected         =    Notification.Name("filterAvailabilitySelected")
     static let yesBtnTapped                         =    Notification.Name("yesBtnTapped")
     static let deviceTokenReceived             =    Notification.Name("FCMToken")
//     static let noBtnTapped                       =    Notification.Name("noBtnTapped")
     static let sortByBtnTapped                    =     Notification.Name("sortByBtnTapped")
     static let dismissPopUP                         =     Notification.Name("dismissPopUP")
     static let reconnectSocketSubscriber    =     Notification.Name("reconnectSocketSubscriber")
    static let userConnected                        =     Notification.Name("userConnected")
    static let tutorHasArrived                        =     Notification.Name("tutorHasArrived")
    static let silentNoti                                  =     Notification.Name("silentNoti")
}

extension NSMutableAttributedString {
    
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: QuicksandMedium, size: 15)!]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
         let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: QuicksandRegular, size: 13)!]
        let normal = NSMutableAttributedString(string:text, attributes: attrs)
        append(normal)
        
        return self
    }
    
}

extension UIApplication {
    class func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopMostViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopMostViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopMostViewController(base: presented)
        }
        return base
    }
}

extension UIImage {
    convenience init?(url: NSURL?) {
        guard let url = url else { return nil }
        
        do {
            let data = try Data(contentsOf: url as URL)
            
            self.init(data: data)
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
}

extension UIColor{
    
    convenience init( r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat ) {
        
        self.init(red: r/255, green: g/255, blue: b/255, alpha: a)
        
    }
    
}


extension Data {
    private static let mimeTypeSignatures: [UInt8 : String] = [
        0xFF : "image/jpeg",
        0x89 : "image/png",
        0x47 : "image/gif",
        0x49 : "image/tiff",
        0x4D : "image/tiff",
        0x25 : "application/pdf",
        0xD0 : "application/vnd",
        0x46 : "text/plain",
        ]
    
    var mimeType: String {
        var c: UInt8 = 0
        copyBytes(to: &c, count: 1)
        return Data.mimeTypeSignatures[c] ?? "application/octet-stream"
    }
    
    
}

extension Collection {
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension UINavigationController {
    
    func getPreviousViewController() -> UIViewController? {
        guard viewControllers.count > 1 else {
            return nil
        }
        return viewControllers[viewControllers.count - 2]
    }
    
    func getSecondPreviousViewController() -> UIViewController? {
        guard viewControllers.count > 1 else {
            return nil
        }
        return viewControllers[viewControllers.count - 3]
    }
    
    func getNhViewController(_ controller_number: Int)-> UIViewController?{
        guard viewControllers.count > 1 else {
            return nil
        }
        return viewControllers[viewControllers.count - controller_number]
    }
}

extension Date{
    
    static func dateFromCustomString(customString: String) -> Date {
        let dateFormmater = DateFormatter()
        dateFormmater.dateFormat = "MM/dd/yyyy"
        return dateFormmater.date(from: customString) ?? Date()
    }
    
    func reduceToMonthDayYear() -> Date {
        let calendar = Calendar.current
        let month = calendar.component(.month, from: self)
        let day = calendar.component(.day, from: self)
        let year = calendar.component(.year, from: self)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.date(from: "\(month)/\(day)/\(year)") ?? Date()
    }
    
    func dateToString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case secondLowest = 0.10
        case low     = 0.25
        case lowMedium = 0.30
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

extension Date {
    
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        return dateFormatter.string(from: self)
    }
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
        
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    
    
    
}

extension UIWindow {
    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController = self.rootViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: rootViewController)
        }
        return nil
    }
    
    static func getVisibleViewControllerFrom(vc:UIViewController) -> UIViewController {
        if let navigationController = vc as? UINavigationController,
            let visibleController = navigationController.visibleViewController  {
            return UIWindow.getVisibleViewControllerFrom( vc: visibleController )
        } else if let tabBarController = vc as? UITabBarController,
            let selectedTabController = tabBarController.selectedViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: selectedTabController )
        } else {
            if let presentedViewController = vc.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(vc: presentedViewController)
            } else {
                return vc
            }
        }
    }
}


extension Sequence where Iterator.Element : Hashable {
    
    func intersects<S : Sequence>(with sequence: S) -> Bool
        where S.Iterator.Element == Iterator.Element
    {
        let sequenceSet = Set(sequence)
        return self.contains(where: sequenceSet.contains)
    }
}

extension Date {
    
    func timeAgoDisplay() -> String {
        let secondsAgo = Int(Date().timeIntervalSince(self))
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        if secondsAgo < minute {
            return "just now"
        }
            
        else if secondsAgo < hour {
            
            if (secondsAgo / minute) == 1{
                return "\(secondsAgo / minute) minute ago"
            }else{
                return "\(secondsAgo / minute) minutes ago"
            }
            
        }
        else if secondsAgo < day {
            return "\(secondsAgo / hour) hours ago"
        }else if secondsAgo >= 86400 && secondsAgo < 172800 {
            return "\(secondsAgo / day) day ago"
        }  else if secondsAgo < week {
            return "\(secondsAgo / day) days ago"
        }
        return "\(secondsAgo / week) weeks ago"
    }
    
}

protocol Bluring {
    func addBlur(_ alpha: CGFloat)
}

extension Bluring where Self: UIView {
    func addBlur(_ alpha: CGFloat = 0.2) {
        // create effect
        let effect = UIBlurEffect(style: .light)
        let effectView = UIVisualEffectView(effect: effect)
        
        // set boundry and alpha
        effectView.frame = self.bounds
        effectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        effectView.alpha = alpha
        
        self.addSubview(effectView)
    }
}

// Conformance
extension UIView: Bluring {}
