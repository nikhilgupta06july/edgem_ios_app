//
//  ImagePickerView.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 15/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation


extension UIViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate {

   @objc func showImageOptions() {
    
        let commonPopUP = ShowInfoCommonPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        commonPopUP.fromController = self
        if self is ChatViewController{
        commonPopUP.showDocumentCameraPhoneOptions = true
        }
        let window = UIApplication.shared.keyWindow?.rootViewController
        window?.present(commonPopUP, animated: true, completion: nil)
    
    }

    func document() {
        
                let documentPicker = UIDocumentPickerViewController(documentTypes: ["com.apple.iwork.pages.pages", "com.apple.iwork.numbers.numbers", "com.apple.iwork.keynote.key","public.image", "com.apple.application", "public.item","public.data", "public.content", "public.audiovisual-content", "public.movie", "public.audiovisual-content", "public.video", "public.audio", "public.text", "public.data", "public.zip-archive", "com.pkware.zip-archive", "public.composite-content", "public.text"], in: .import)
                documentPicker.delegate = self
                self.present(documentPicker, animated: true, completion: nil)
        
    }
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.allowsEditing = !(self is QualificationsNExperiencesVC)
            self.present(myPickerController, animated: true, completion: nil)
        }

    }

    func photoLibrary() {

        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            myPickerController.allowsEditing = !(self is QualificationsNExperiencesVC)
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
}

