//
//  CommonFunction.swift
//  Edgem
//
//  Created by Hipster on 30/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation

class CustomImageView: UIImageView{
    var imageURLString: String?
    var img: UIImage? = nil
     func loadimagesUsingURLString(_urlString: String)->UIImage{
        imageURLString = _urlString
        guard let urlString = imageURLString else{return UIImage()}
        guard let url = URL(string: urlString) else{return UIImage()}
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil{
                print(error!)
                return
            }
            guard let image = UIImage(data: data!) else{return}
            self.img = image
        }.resume()
        if img != nil{
            return img!
        }else{
            return UIImage()
        }
    }
    
}
