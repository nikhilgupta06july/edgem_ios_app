//
//  Constants.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 02/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation
import UIKit

//let headers = ["Accept" : "application/json",
//               "Os-Type": "iOS",
//               "timezone": Utilities.getTimeZone(),
//               "locale": UserStore.shared.selectedLanguageCode]

let headers = [ "Accept"    : "application/json",
                        "Os-Type"   : "iOS",
                        "timezone"  : Utilities.getTimeZone(),
                        "locale"       : "en"]

let GoogleApiKey                         = "AIzaSyAEKnJtajYx71rlusv0WJakK2LpFyq3ELk"//"AIzaSyA_pOPvg9XJH83OdC5lM79gXIA8114GpwU"
let BaseUrlForGoogleMap            = "https://maps.googleapis.com/maps/api/geocode/json?"
let BaseURLForGooglePrediction = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
let AppDelegateConstant             = UIApplication.shared.delegate as! AppDelegate
let bitly_auth_token                       = "7606faa4d80584786d47053be19649461fc90cd9"

// [START get_device_ID]
 let uniqueIdentifier                       = UIDevice.current.identifierForVendor?.uuidString ?? ""
// [END get_device_ID]

// Braintree
//let toKinizationKey = "sandbox_mf8yj9ft_43kxyt9qxmtb7s3m" // ------> Sandbox
let toKinizationKey                         = "sandbox_x6h9nd65_m4qf5t9bfmtjgdct"




//MARK: - Dimension Constants
let ScreenWidth                     = UIScreen.main.bounds.width
let ScreenHeight                    = UIScreen.main.bounds.height

//MARK: - Typealias Constants
typealias ResponseObject      = (response: HTTPURLResponse, data: Data?)
var appDelegate                     = UIApplication.shared.delegate as? AppDelegate

let tutorType                           = "Tutor"
let studentType                      = "Student"
let parentType                        = "Parent"

let Tutor_Login_Type              = 3
let Child_Login_Type              = 2
let Parent_Login_Type            = 1

let MIN_DISTANCE: CGFloat   = 0
let MAX_DISTANCE: CGFloat  = 50

let subjectType                       = "Subject"
let certificateType                   = "Certificate"
let schoolType                        = "School"

let bullet                                 = "\u{2022}"
    
//MARK: - API related

let Live_URL                            = "http://app.edgem.co/api/"
let Development_URL             = "http://192.168.31.53:4040/api"//"http://192.168.31.71:4040/api/"//"http://192.168.0.19:4040/api/"

let Live_Socket_URL                = "http://app.edgem.co"
let Development_Socket_URL = "http://192.168.31.53"//"http://192.168.31.71"

// Stripe
//let PUBLISHABLE_API_KEY      = "pk_test_jMETcuy1lOv6oWlOnjabmzox"
//let SECRET_API_KEY                = "sk_test_TCIer3BddHA7xvufo3kkAOD700hAHFMWuv"

let PUBLISHABLE_API_KEY     = "pk_live_4ITweMT0JpwHt09P1yXEh8XH"
let SECRET_API_KEY               = "sk_live_EPXzd1NSxqtt8ME66qYs4WdH00j2TlyDyC"

let sharingBaseURL            = "http://edgem.co"//"http://edgem.co/api/"
let baseUrl                          = Live_URL
let socketUrl                       = Live_Socket_URL

let successCode                 = 200

// MARK: - URLs

let terms_conditions            = URL(string: "https://edgem.co/terms.html")!
let PrivacyPolicy                   = URL(string: "https://edgem.co/data-protection-policy.html")!
let ContactUs                       = URL(string: "https://edgem.co/contact-us")!//https://edgem.co/#contactUs

// MARK: - Default fonts
let textFieldDefaultFont       = UIFont(name: "Quicksand-Regular", size: 13.0)!
let textFieldLightFont           = UIFont(name: "Quicksand-Light", size: 15.0)!
let textFieldMediumFont      = UIFont(name: "Quicksand-Medium", size: 15.0)!
let textFieldRegularFont       = UIFont(name: "Quicksand-Regular", size: 15.0)!
let titleMediumFont              = UIFont(name: "Quicksand-Medium", size: 24.0)!
let titleBoldFont                    = UIFont(name: "Quicksand-Bold", size: 16)
let tabBarTitleFont                = UIFont(name: "SourceSansPro-Regular", size: 13)!

let QuicksandMedium           = "Quicksand-Medium"
let QuicksandRegular            = "Quicksand-Regular"
let SourceSansProBold          = "SourceSansPro-Bold"
let SourceSansProRegular     = "SourceSansPro-Regular"
let SourceSansProSemiBold  = "SourceSansPro-SemiBold"
let QuicksandBold                 = "Quicksand-Bold"

let genderOptions                 = [ "Male".localized, "Female".localized]
let relationOptions                 = ["Father".localized, "Mother".localized]
let negotiableOptions            = ["Non negotiable".localized, "Negotiable".localized]

//MARK: - UserDefault Keys -

let IsLoggedIn                        = "IsLoggedIn"
let DeviceToken                     = "DeviceToken"
let Token                                = "Token"
let WalkThrough                     = "WalkThrough"
let OfferIDNTotalAmmount    = "OfferIDNTotalAmmount"
let NegotiableStatus              = "NegotiableStatus"
let StripeError                         = "StripeError"
let LanguageSelected            = "LanguageSelected"
//let Skip = "Skip"
let NotificationUnAuthorized  = "NotificationUnAuthorized"
let IsNotificationStatus            = "IsNotificationStatus"
let HasLoginKey                      = "hasLoginKey"
let UserEmail                           = "UserEmail"
let loginType                           = "LogInType"
let UserID                                = "UserID"
let TouchIDEmail                     = "TouchIDEmail"
let BitlyURL                              = "BitlyURL"
let SelectedLanguage             = "SelectedLanguage"
let SelectedLanguageCode    = "SelectedLanguageCode"
let BioMetricEnabledByUser   = "BioMetricEnabledByUser"
//let IsNotificationOn = "IsNotificationOn"
//let IsNotificationSet = "IsNotificationSet"
let NotificationPermissionsAsked = "NotificationPermissionsAsked"
let UserType                             = "userType"
let IsParrent                              = "isParrent"
//let IsAgency = "isAgency"

let InvalidAuthTokenErrorCode = 403
let ForceAppUpdateErrorCode = 412

let AppName                             = "Edgem".localized
let selectedSubject                    = "SelectedSubject"

//MARK: - Validation Constants -

let PhoneNumberMaxLimit        = 15
let PhoneNumberMinLimit         = 8
let AccountNumberMinLimit      = 8
let PostalCodeMinLimit              = 6
let PostalCodeMaxLimit             = 10
let USAPostalCodeLimit             = 5
let UserNameMinLimit               = 2
let UserNameMaxLimit              = 32
let PasswordMinLimit                 = 8
let PasswordMaxLimit                 = 15
let EmailMaxLimit                       = 254
let NameMinLimit                       = 1
let NameMaxLimit                      = 50
let DisplayNameMinLimit           = 1
let DisplayNameMaxLimit           = 100
let ReferralCodeMinLimit            = 6
let AddressMinLimit                    = 12
let AddressMaxLimit                    = 360
let CityMinLimit                           = 0
let CityMaxLimit                          = 50
let CompanyNameMaxLimit       = 164
let MessageMinLimit                   = 16
let MessageMaxLimit                  = 360
let PhoneNumberCharacterSet   = "0123456789*#+()-"
let NumbersCharacterSet            = "0123456789"
let CardNumberMinLimit             = 13
let CardNumberMaxLimit            = 16
let CardCVVMinDigits                  = 3
let CardCVVMaxDigits                  = 4
let CardExpirationLimit                 = 4
let OTPMaxLimit                           = 4


//MARK: - App Colors
let themeRedColor                       =  UIColor(red: 222.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
let theamBlackColor                     = UIColor(red: 22.0/255.0, green: 22.0/255.0, blue: 22.0/255.0, alpha: 1.0)
let theamAppColor                       =  UIColor(red: 255.0/255.0, green: 226.0/255.0, blue: 115.0/255.0, alpha: 1.0)
let theamAppDarkColor               =  UIColor(red: 255.0/255.0, green: 210/255.0, blue: 113/255.0, alpha: 1.0)
let theamAppColorFaded             =  UIColor(red: 255.0/255.0, green: 226.0/255.0, blue: 115.0/255.0, alpha: 0.3)
let theamGrayColor                      = UIColor(red: 139/255, green: 139/255, blue: 139/255, alpha: 1)
let theamBorderGrayColor           = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
let cellBackgroundColor               = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1)
let buttonBorderColor                  = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
let buttonBackGroundColor         = UIColor(red: 255/255, green: 210/255, blue: 113/255, alpha: 1)
let theamAppGrayColor               = UIColor(red: 254/255, green: 241/255, blue: 187/255, alpha: 1)
let themeGreenColor                    = UIColor(red: 159/255, green: 209/255, blue: 38/255, alpha: 1)
let toolTipColor                             =  UIColor(red: 23/255, green: 23/255, blue: 23/255, alpha: 0.8)
let staticMessageCellBorderColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
let textViewPlaceHolderColor       = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1)

let kUserDefault                             = UserDefaults.standard
