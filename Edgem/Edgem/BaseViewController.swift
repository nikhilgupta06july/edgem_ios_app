//
//  BaseViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 12/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import Toaster
import SystemConfiguration
import AMPopTip
//import SafariServices

class BaseViewController: UIViewController, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var cupsLabel: UILabel!
    
    var popToolTip: PopTip!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.enableExclusiveTouchForView(self.view)
        
        ToastView.appearance().font = UIFont.systemFont(ofSize: 16.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //Subclasses should override this method if they want to allow the user to navigate back
    //while the network indicator is visible.
    func shouldBlockNavigation() -> Bool {
        return true
    }
    
    deinit {
        
    }
    
    @IBAction func navigateBack(sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func navigateToLandingScreen() {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        guard let rootViewController = window.rootViewController else {
            return
        }
        
        let landingViewController = LoginViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        landingViewController.view.frame = rootViewController.view.frame
        landingViewController.view.layoutIfNeeded()
        let navController = BaseNavigationViewController(rootViewController: landingViewController)
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = navController
        }, completion: nil)
        
    }
    
    //    func showLoginAlert() {
    //        let alert = UIAlertController(title: AppName, message: "You need to login in order to perform this action.".localized, preferredStyle: .alert)
    //        let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
    //            UserStore.shared.isSkipSelected = false
    //            self.navigateToLandingScreen()
    //        })
    //        let laterAction = UIAlertAction(title: AlertButton.later.description(), style: .default, handler: nil)
    //        alert.addAction(laterAction)
    //        alert.addAction(okAction)
    //        self.present(alert, animated: true, completion: nil)
    //    }
    
    //    func showLoginAlertForShoutOutForTutor(){
    //        let alertController = UIAlertController(title: AppName, message: "Only student can Shoutout!".localized, preferredStyle: .alert)
    //        let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
    //        alertController.addAction(okAction)
    //        self.present(alertController, animated: true, completion: nil)
    //    }
    
    //    func showLoginAlertForShoutOutForStudent(){
    //        let alertController = UIAlertController(title: AppName, message: "Only tutors can be Shoutout!".localized, preferredStyle: .alert)
    //        let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
    //        alertController.addAction(okAction)
    //        self.present(alertController, animated: true, completion: nil)
    //    }
    
    //    @objc func showForceUpdate(_ notification: NSNotification) {
    //        if let url = notification.userInfo?["app_url"] as? String {
    //            DispatchQueue.main.async(execute: {
    //                let alert = UIAlertController(title: AppName, message: "Please update the app to proceed futher. Click Ok to download.", preferredStyle: .alert)
    //                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
    //                    self.navigateToAppStore(url)
    //                })
    //                alert.addAction(okAction)
    //                self.present(alert, animated: true, completion: nil)
    //            })
    //        }
    //    }
    
    func showSimpleAlertWithMessage(_ message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showSimpleAlertWithMessgeAndNavigateToAddCardVC(_ message: String){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                                    let creditCardsListViewController = CreditCardsListViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                                    self.navigationController?.pushViewController(creditCardsListViewController, animated: true)
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showSimpleAlertWithMessgeAndNavigateBack(_ message: String){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
             self.navigationController?.popViewController(animated: true)
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showSimpleAlertWithMessgeAndNavigateToAddBankDetailVC(_ message: String){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                let bankDetailViewController = BankDetailViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
                self.navigationController?.pushViewController(bankDetailViewController, animated: true)
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func navigateToAppStore(_ url: String) {
        DispatchQueue.main.async(execute: {
            if let url = URL(string: url),
                UIApplication.shared.canOpenURL(url)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        })
    }
    
    //    func navigateToSettingsPage() {
    //        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
    //            return
    //        }
    
    //        if UIApplication.shared.canOpenURL(settingsUrl) {
    //            if #available(iOS 10.0, *) {
    //                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
    //                    print("Settings opened: \(success)") // Prints true
    //                })
    //            } else {
    //                UIApplication.shared.openURL(settingsUrl)
    //            }
    //        }
    //    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    //    override var preferredStatusBarStyle: UIStatusBarStyle {
    //        return .default
    //    }
    
    func enableExclusiveTouchForView(_ v: UIView) {
        
        for subView in v.subviews {
            subView.isExclusiveTouch = false
            if subView.subviews.count > 0 {
                self.enableExclusiveTouchForView(subView)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        imageCache.removeAllObjects()
    }
    
    func showNoInternetMessage() {
        self.showSimpleAlertWithMessage(AlertMessage.noInternetConnection.description())
    }
    
    func showInvalidInputsMessage() {
        self.showSimpleAlertWithMessage(AlertMessage.invalidInput.description())
    }
    
    func getAppVersion() -> String {
        
        if let text = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
            return text
        }
        return ""
    }
    
    func topViewController() -> UIViewController {
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let navController: UINavigationController = appDelegate.window?.rootViewController
            as! UINavigationController
        if navController.isKind(of: UINavigationController.self) {
            return navController.visibleViewController!
        }
        return navController
    }
    
    
    func pushToWebViewWithURL(_ url: URL, title: String?) {
        let webViewController = WebViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        webViewController.link = url
        webViewController.titleString = title
        self.navigationController?.pushViewController(webViewController, animated: true)
    }
    
    func navigateToDashboardViewController() {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        guard let rootViewController = window.rootViewController else {
            return
        }
        
        let tabBarController = BaseTabBarViewController()
        tabBarController.selectedIndex = 0
        tabBarController.view.frame = rootViewController.view.frame
        tabBarController.view.layoutIfNeeded()
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = tabBarController
        }, completion: nil)
        
    }
    
    func navigateToFindScreen() {
        DispatchQueue.main.async {
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func emptySchedulesArray() -> [String: [String]] {
        return ["MON": [String](), "TUE": [String](), "WED": [String](), "THU": [String](), "FRI": [String](), "SAT": [String](), "SUN": [String]()]
        
    }
    
    func logInBtnTapped(){
        let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
        
    }
    
    func dismissPopUp(){
        self.dismiss(animated: true)
    }
    
    func showPopToolTipForButton(_ button: UIButton, popMessage: String) {
        
        
        
        if let existingpopToolTip = self.popToolTip {
            existingpopToolTip.hide(forced: true)
            self.popToolTip = nil
            self.popToolTip = PopTip()
            self.popToolTip.tag = button.tag
            self.popToolTip.bubbleColor = toolTipColor
            self.popToolTip.shouldDismissOnTapOutside = true
            self.popToolTip.shouldDismissOnTap = true
            self.popToolTip.textColor = UIColor.white
            self.popToolTip.textAlignment = .left
            self.popToolTip.font = UIFont(name: QuicksandMedium, size: 15)!
            let fromRect = button.convert(button.bounds, to: self.view)
            self.popToolTip.show(text: popMessage, direction: .down, maxWidth: ScreenWidth*6/10, in: self.view, from: fromRect)
        } else {
            self.popToolTip = PopTip()
            self.popToolTip.bubbleColor = toolTipColor
            self.popToolTip.shouldDismissOnTapOutside = true
            self.popToolTip.shouldDismissOnTap = true
            self.popToolTip.textColor = UIColor.white
            self.popToolTip.textAlignment = .left
            self.popToolTip.font =  UIFont(name: QuicksandMedium, size: 15)!
            let fromRect = button.convert(button.bounds, to: self.view)
            self.popToolTip.show(text: popMessage, direction: .down, maxWidth: ScreenWidth*6/10, in: self.view, from: fromRect)
        }
        
    }
    
    func hidePopToolTipForButton() {
        if let existingPopTip = self.popToolTip {
            existingPopTip.hide()
        }
    }
}

