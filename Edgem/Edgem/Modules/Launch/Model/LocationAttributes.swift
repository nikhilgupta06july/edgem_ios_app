//
//  LocationAttributes.swift
//  Edgem
//
//  Created by Hipster on 20/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation

struct AddressAttribute{
    let address_label: String
    let address_location: String
    let lattitude: String
    let longitude: String
    
    
}

struct LocationAttributes{
    var email: String
    var addressAttribute: AddressAttribute
}

struct Addresses {
    var locationAttribute: [LocationAttributes]
}
