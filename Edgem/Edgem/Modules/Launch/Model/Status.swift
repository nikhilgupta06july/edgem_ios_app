//
//  Status.swift
//  Edgem
//
//  Created by Namespace on 29/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation

class Status {
    
    // MARK: - Properties
    
    var status: Int
    var message: String
    
    //MARK: Initialization
    
    init?(status: Int,  message: String){
        
        if status<0 || message.isEmpty{
            return nil
        }
        
        self.status = status
        self.message = message
    }
    
}
