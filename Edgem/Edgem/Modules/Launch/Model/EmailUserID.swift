//
//  Email.swift
//  Edgem
//
//  Created by Namespace on 09/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

struct EmailUserID: Unboxable{
    let email:String?
    let userId:Int
    
    init(unboxer: Unboxer) throws {
        self.email = try? unboxer.unbox(key: "email")
        self.userId = try unboxer.unbox(key: "user_id")
    }
}
