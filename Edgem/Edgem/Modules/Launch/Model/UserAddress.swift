//
//  UserAddress.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 11/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class UserAddress: Unboxable {
    
    var postalCode: String!
    var addressLine1: String!
    var addressLine2: String!
    var lattitude: String!
    var longitude: String!
    var addressID: Int!
    var addressLabel: String!
    var addressLocation: String!

    
    init(addressObject: [String:String]){

        self.lattitude = addressObject["lattitude"]
        self.longitude = addressObject["longitude"]
        self.addressLocation = addressObject["address_location"]
        
    }
    
    required init(unboxer: Unboxer) throws {
        
        self.addressLine1 = unboxer.unbox(key: "addres_line1") ?? ""
        self.addressLine2 = unboxer.unbox(key: "addres_line2") ?? ""
        self.postalCode = unboxer.unbox(key: "postcode") ?? ""
        self.lattitude = unboxer.unbox(key: "lattitude") ?? ""
        self.longitude = unboxer.unbox(key: "longitude") ?? ""
        self.addressLabel = unboxer.unbox(key: "address_label")
        self.addressLocation = unboxer.unbox(key: "address_location")
    }
    
    init(){
        
    }
    
//    init(_ address: UserAddress) {
//
//        self.addressLine1 = address.addressLine1
//        self.addressLine2 = address.addressLine2
//        self.postalCode = address.postalCode
//        self.lattitude = address.lattitude
//        self.longitude = address.longitude
//        self.addressLabel = address.addressLabel
//        self.addressLocation = address.addressLocation
//
//    }
    
}
