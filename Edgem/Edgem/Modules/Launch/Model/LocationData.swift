//
//  LocationData.swift
//  Edgem
//
//  Created by Hipster on 15/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation

struct LocationData{
    
    var profileImage: String
    var title: String
    var address: String
    var lattitude: String
    var longitude: String
    var address_id: String
    
    init(profilImage: String, title: String, address: String,lat: String, long: String, addressId: String) {
        profileImage = profilImage
        self.title = title
        self.address = address
        self.lattitude = lat
        self.longitude = long
        self.address_id = addressId
    }
    
//    init?(locationDict: [String:AnyObject]){
//        guard  let profileImage = locationDict["profileImage"] as? String,
//                    let title = locationDict["address_label"] as? String,
//                    let address = locationDict["address_location"] as? String,
//                    let lattitude = locationDict["lattitude"] as? String,
//                    let longitude = locationDict["longitude"] as? String else{return nil}
//
//        self.profileImage = profileImage
//        self.title = title
//        self.address = address
//        self.lattitude = lattitude
//        self.longitude = longitude
//
//    }
}
