//
//  UserSocialData.swift
//  Edgem
//
//  Created by Hipster on 11/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class UserSocialData: Unboxable{

    // MARK: - Properties
    
    var status: String! = nil
    var data: User?
    var message: String!
    var isExist : Int!
    
    init(){
    }
    
    required init(unboxer: Unboxer) throws {
        status                  = unboxer.unbox(key: "status")
        if let data  = unboxer.dictionary["data"] as? [String:AnyObject], let userData = data["user_data"] as? [String:AnyObject]{
            self.data           = try! unbox(dictionary: userData)
        }
        message              = unboxer.unbox(key: "message")
        isExist                  = unboxer.unbox(key: "is_exist")
    }
    
}
