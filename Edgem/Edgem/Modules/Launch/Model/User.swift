//
//  User.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 13/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation
import Unbox

class User: Unboxable {
    
    var ID: Int!
    var fbID: String!
    var gmailID: String!
    var userType: String!
    
    var userName: String!
    var firstName: String!
    var lastName: String!
    var gender: String!
    var birthday: String!
    var email: String!
    var countryContactCode: String!
    var contactNumber: String!
    
    var parentFirstName: String!
    var parentLastName: String!
    var relationwithchild: String!
    var parentEmail: String!
    var parentContactNumber: String!
    var parentContactCode: String!
    
    var userAddress: [UserAddress]?
    var level: [Level]?
    var subjects: [Subject]?
    var qualifications: [Qualification]?
    var educations: [Education]?
    var experiences: [Experience]?
    var totalExperience: String!
    var otherSubject: String = ""
    var language: String = ""
    var music: String = ""
    
    var imageURL: String!
    var status: String?
    var redirectToVerify: String!

    var createdOn: String!
    var userToken: String!
    
    var documents: [Documents]?
    
    var paymentCard:[StoreUserCard]?
    
    var ratings: String?
    
    var walletBalance: Double?
    var userBankDetail: BankDetails?
    
    var userTransactions: UserTransactions?
    
    var referralCode: String?
    var totalInvites: String?
    var totalCredits: String?
    
    var onlineStatus: Int?
    
    var btCustomerID: String?
    
    var creditsPerPoint: String?
    
    var availibility: DayAvailability?
    
    var distance: String?
    
    var loginType: Int?
    
    var otpVarificationStatus: Int?
    var otpVarificationContact: String?
    
//    var address
    
    init(){
        otherSubject = ""
        music = ""
        language = ""
    }
    
    required init(unboxer: Unboxer) throws {
        self.ID = unboxer.unbox(key: "user_id") ?? 0
        self.fbID = unboxer.unbox(key: "fb_id") ?? ""
        self.gmailID = unboxer.unbox(key: "gl_id") ?? "xxxx"
        self.userType = unboxer.unbox(key: "user_type") ?? ""
        
        self.userName = unboxer.unbox(key: "username") ?? ""
        self.firstName = unboxer.unbox(key: "first_name") ?? ""
        self.lastName = unboxer.unbox(key: "last_name") ?? ""
        self.gender = unboxer.unbox(key: "gender") ?? ""
        self.birthday = unboxer.unbox(key: "dob") ?? ""
        self.email = unboxer.unbox(key: "email") ?? ""
        self.countryContactCode = unboxer.unbox(key: "country_code") ?? ""
        self.contactNumber = unboxer.unbox(key: "contact_number") ?? ""
        
        
        self.parentFirstName = unboxer.unbox(key: "first_name_parent") ?? ""
        self.parentLastName = unboxer.unbox(key: "last_name_parent") ?? ""
        self.relationwithchild = unboxer.unbox(key: "relation") ?? ""
        self.parentEmail = unboxer.unbox(key: "email_parent") ?? ""
        self.parentContactCode = unboxer.unbox(key: "country_code_parent") ?? ""
        self.parentContactNumber = unboxer.unbox(key: "contact_number_parent") ?? ""
        
        self.imageURL = unboxer.unbox(key: "real_image") ?? ""
        self.status = unboxer.unbox(key: "status") ?? ""
        self.createdOn = unboxer.unbox(key: "addedon") ?? "2018-06-13 09:46:52"
        
        self.userAddress = unboxer.unbox(key: "address")
        self.level = unboxer.unbox(key: "level")
        self.subjects = unboxer.unbox(key: "subjects")
        self.qualifications = unboxer.unbox(key: "qualification") //qualifications
        self.educations = unboxer.unbox(key: "education")
        self.experiences = unboxer.unbox(key: "experience")
        
        self.userToken = unboxer.unbox(key: "token")
        
        self.documents = unboxer.unbox(key: "documents")
        
        self.referralCode = unboxer.unbox(key: "referral_code") ?? ""
        self.totalCredits = unboxer.unbox(key: "total_credits") ?? ""
        self.totalInvites = unboxer.unbox(key: "total_referred") ?? ""
        
        self.ratings = unboxer.unbox(key: "average_ratings")
        self.onlineStatus = unboxer.unbox(key: "online_status")
        self.btCustomerID = unboxer.unbox(key: "bt_customer_id")
        self.creditsPerPoint = unboxer.unbox(key: "credits_per_points")
        self.availibility = unboxer.unbox(key: "calendar")
        self.otpVarificationStatus = unboxer.unbox(key: "otp_verified")
        self.otpVarificationContact = unboxer.unbox(key: "mobile")
        self.loginType = unboxer.unbox(key: "login_type") 
        
        self.distance = unboxer.unbox(key: "distance")
        
        
       
    }
    
    init(_ user: User) {
        
        self.ID = user.ID
        self.fbID = user.fbID
        self.gmailID = user.gmailID
        self.userType = user.userType
        
        self.userName = user.userName
        self.firstName = user.firstName
        self.lastName = user.lastName
        self.gender = user.gender
        self.birthday = user.birthday
        self.email = user.email
        self.relationwithchild = user.relationwithchild
        self.countryContactCode = user.countryContactCode
        self.contactNumber = user.contactNumber
        
        self.parentFirstName = user.parentFirstName
        self.parentLastName = user.parentLastName
        self.parentEmail = user.parentEmail
        self.parentContactNumber = user.parentContactNumber
        self.parentContactCode = user.parentContactCode
        
        self.userAddress = user.userAddress
        self.level = user.level
        self.subjects = user.subjects
        self.qualifications = user.qualifications
        self.experiences = user.experiences
        self.educations = user.educations
        self.imageURL = user.imageURL
        self.status = user.status
        self.redirectToVerify = user.redirectToVerify
        self.createdOn = user.createdOn
        self.userToken = user.userToken
        self.documents = user.documents
        self.paymentCard = user.paymentCard
        self.totalExperience = user.totalExperience
        self.ratings = user.ratings
        self.walletBalance = user.walletBalance
        self.userBankDetail = user.userBankDetail
        self.onlineStatus = user.onlineStatus
        self.btCustomerID = user.btCustomerID
        self.creditsPerPoint = user.creditsPerPoint
        self.distance = user.distance
        self.loginType = user.loginType
        self.otpVarificationStatus = user.otpVarificationStatus
        self.otpVarificationContact = user.otpVarificationContact
    }
    
    func getQualificationsnEducations() -> [String:Any] {
        var dict = [String:Any]()
        
        dict = ["total_experience" : self.totalExperience] as [String : AnyObject]
        
        var qualificationIDs = ""
        if let qualification = qualifications {
            for i in 0..<qualification.count{
                if i != qualification.count-1{
                    if !qualificationIDs.contains(String(qualification[i].id)){
                        qualificationIDs += String(qualification[i].id)
                        qualificationIDs += ","
                    }
                }else{
                    if !qualificationIDs.contains(String(qualification[i].id)){
                        qualificationIDs += String(qualification[i].id)
                    }
                }
            }
        }
        if let last_char = qualificationIDs.last, last_char == ","{
            qualificationIDs.removeLast()
        }
        if !qualificationIDs.isEmpty {
            dict["qualifications"] = qualificationIDs as AnyObject
        }else{
            dict["qualifications"] = "" as AnyObject
        }
        if let _dict = (dict as? [String : AnyObject]) {
            dict = _dict
        }
        return dict
    }
    
    func getEditedTutorDetail() -> [String:AnyObject] {
          var dict = [String:Any]()
       
         dict = [
            "country_code" : self.countryContactCode ?? "",
            "contact_number" : self.contactNumber ?? "",
            "email" : self.email ?? "",
        ]
        
        if (userAddress?.count)! > 0{
            userAddress!.forEach({ (address) in
                var addressDict = [String:AnyObject]()
                addressDict = ["address_location" : address.addressLocation ?? "",
                               "lattitude" : address.lattitude ?? "",
                               "longitude" : address.longitude ?? ""] as [String : AnyObject]
                
                if address.addressLabel.contains("Home") == true{
                    dict["home_address"] = addressDict
                }else if address.addressLabel.contains("Work") == true {
                    dict["work_address"] = addressDict
                }
            })
        }
         return dict as [String:AnyObject]
    }
    
    func geteditedUserDetailStudent()-> [String:String]{
        
        var dict = [String:String]()
        dict = [
            
            "contact_number_parent"  : self.parentContactNumber,
            "parent_email"     : self.parentEmail,
            "country_code_parent" : self.parentContactCode,
            "first_name_parent" : self.parentFirstName,
            "last_name_parent" : self.parentLastName,
        
            "first_name" :  self.userName,
            "last_name" : self.lastName,
            "dob"  :   self.birthday,
            "email" :  self.email,
            "country_code" : self.countryContactCode,
            "relation" : self.relationwithchild
        
        ]
        
        if let userAddressCount = self.userAddress?.count {
            if userAddressCount > 0{
                dict["addres_line1"] = self.userAddress![0].addressLine1
                dict["addres_line2"] = self.userAddress![0].addressLine2
                dict["lattitude"]   = self.userAddress![0].lattitude
                dict["longitude"] = self.userAddress![0].longitude
                dict["postcode"] = self.userAddress![0].postalCode
                
            }
        }
        
        
        
        
    return dict
        
    }
    
    class func setAppdelegateUser(user: User) {
        DispatchQueue.main.async {
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.user = user
        }
    }
    
}
