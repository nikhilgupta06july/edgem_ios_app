//
//  UserAvailable.swift
//  Edgem
//
//  Created by Hipster on 05/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation
import Unbox

class UserAvailable: Unboxable{
    
    var res_data: String!
    var res_message: String!
    
    required init(unboxer: Unboxer) throws {
        self.res_data = unboxer.unbox(key: "res_data") ?? ""
        self.res_message = unboxer.unbox(key: "res_message") ?? ""
    }
    
    init(res_data: String, res_message: String) {
        self.res_data = res_data
        self.res_message = res_message
    }
}
