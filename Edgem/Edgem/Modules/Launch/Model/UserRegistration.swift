//
//  UserRegistration.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 14/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation

struct UserRegistration {
    
    var isStudent = true
    var userName = ""
    var password = ""
    var newPassword = ""
    var confirmPassword = ""
    
    var firstName = ""
    var lastName = ""
    var gender = ""
    var birthday = ""
    var email = ""
    var isParent = 0
    var relationwithchild = ""
    
    var childLastName = ""
    var childFirstName = ""
    var childGender = ""
    var childBirthday = ""
    var childEmail = ""
    var childContactNumber = ""
    var childContactCode = ""
    var childNotificationAllow = false
    
    var fbID = ""
    var gmailID = ""
    
    var postalCode = ""
    var addressLine1 = ""
    var addressLine2 = ""
    var lattitude = ""
    var longitude = ""
    var countryCode = ""
    
    var address_label: [String] = []
    var address_location: [String] = []
    var address_lattitude: [String] = []
    var address_longitude: [String] = []

    var countryContactCode = ""
    var contactNumber = ""
    var imageURL = ""
    var userID = ""
    var tutorImage: UIImage! = UIImage()
    var referralCode: String = ""
    
//    var confirmEmail = ""
//    var _parrent_email = ""
    
//    var parentfirstname = ""
//    var parentlastname = ""
    
//    var userType = ""
    var MRT_LRTStation = ""

//    var errorMsg = ""
    
//    title:Mr
//    address_line:Madehganj
//    city:Lucknow
//    zip_code:226020
//    country:India
//    contact_number:918127799111
    
    init() {
        
    }
    
    mutating func copyValuesFromFBDict(_ dict: [String: AnyObject]) {
        self.email = dict["email"] as? String ?? ""
//        self.confirmEmail = dict["email"] as? String ?? ""
        self.lastName = dict["last_name"] as? String ?? ""
        self.fbID = dict["fb_id"] as? String ?? ""
        self.firstName = dict["first_name"] as? String ?? ""
        self.imageURL = dict["url"] as? String ?? ""
    }
    
    mutating func copyValuesFromGmailDict(_ dict: [String: AnyObject]) {
        self.email = dict["email"] as? String ?? ""
//        self.confirmEmail = dict["email"] as? String ?? ""
        self.lastName = dict["last_name"] as? String ?? ""
        self.gmailID = dict["gl_id"] as? String ?? ""
        self.firstName = dict["first_name"] as? String ?? ""
        self.imageURL = dict["url"] as? String ?? ""
    }
    
}
