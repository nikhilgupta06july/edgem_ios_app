//
//  checkBoxCreateStudentTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 17/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit

class checkBoxCreateStudentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var notificationLabel: UILabel!
    
    let termText = "I wish for my child to receive notifications from Edgem. Please click here to find out more."
    let term = "here"
    
    var createStudentAccountViewController: CreateStudentAccountViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        notificationLabel.isUserInteractionEnabled = true
        //notificationLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:))))
        
        checkBoxButton.setImage(UIImage(named : "checkboxInactive"), for: UIControl.State.normal)
        checkBoxButton.setImage(UIImage(named : "checkboxActive"), for: UIControl.State.selected)
        
        
        let formattedText = String.format(strings: [term],
                                          boldFont: UIFont(name: "Quicksand-Bold", size: 15.0)!,
                                          boldColor: theamBlackColor,
                                          inString: termText,
                                          font: UIFont(name: QuicksandRegular, size: 15.0)!,
                                          color: theamBlackColor
                                        )
        notificationLabel.attributedText = formattedText
        notificationLabel.numberOfLines = 0
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTermTapped))
        notificationLabel.addGestureRecognizer(tap)
        notificationLabel.isUserInteractionEnabled = true
        notificationLabel.textAlignment = .left
    
        
    }
    
    class func cellIdentifier() -> String {
        return "checkBoxCreateStudentTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 104
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func checkBoxButtonTapped(_ sender: UIButton) {
        if checkBoxButton.isSelected == true {
            checkBoxButton.isSelected = false
        }else {
            checkBoxButton.isSelected = true
        }
    }
    
    @objc func handleTermTapped(gesture: UITapGestureRecognizer) {
        let termString = termText as NSString
        let termRange = termString.range(of: term)
       // let policyRange = termString.range(of: policy)
        
        let tapLocation = gesture.location(in: notificationLabel)
        let index = notificationLabel.indexOfAttributedTextCharacterAtPoint(point: tapLocation)
        
        if checkRange(termRange, contain: index) == true {
           // handleViewTermOfUse()
           // print("range gottted")
            createStudentAccountViewController?.didTaphereText()
            return
        }
        
    }
    
    func checkRange(_ range: NSRange, contain index: Int) -> Bool {
        return index > range.location && index < range.location + range.length
    }

}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        
        guard let attrString = label.attributedText else {
            return false
        }
        
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: .zero)
        let textStorage = NSTextStorage(attributedString: attrString)
        
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        textContainer.lineFragmentPadding = 0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
        
    }
}

extension String {
    static func format(strings: [String],
                       boldFont: UIFont = UIFont.boldSystemFont(ofSize: 14),
                       boldColor: UIColor = UIColor.blue,
                       inString string: String,
                       font: UIFont = UIFont.systemFont(ofSize: 14),
                       color: UIColor = UIColor.black) -> NSAttributedString {
        let attributedString =
            NSMutableAttributedString(string: string,
                                      attributes: [
                                        NSAttributedString.Key.font: font,
                                        NSAttributedString.Key.foregroundColor: color
                                        
                                        ])
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont, NSAttributedString.Key.foregroundColor: boldColor,NSAttributedString.Key.underlineStyle: NSNumber(value: 1)]
       
        for bold in strings {
            attributedString.addAttributes(boldFontAttribute, range: (string as NSString).range(of: bold))
          
        }
        return attributedString
    }
}

extension UILabel {
    func indexOfAttributedTextCharacterAtPoint(point: CGPoint) -> Int {
        assert(self.attributedText != nil, "This method is developed for attributed string")
        let textStorage = NSTextStorage(attributedString: self.attributedText!)
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: self.frame.size)
        textContainer.lineFragmentPadding = 0
        textContainer.maximumNumberOfLines = self.numberOfLines
        textContainer.lineBreakMode = self.lineBreakMode
        layoutManager.addTextContainer(textContainer)
        
        let index = layoutManager.characterIndex(for: point, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return index
    }
}
