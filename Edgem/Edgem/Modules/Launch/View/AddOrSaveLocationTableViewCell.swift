//
//  AddOrSaveLocationTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 15/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit


protocol AddOrSaveLocationTableViewCellDelegate {
    func sendDataOnImageTapped(_ profileImage: String, titleLabelText: String, addreeTextFieldText: String, _ index: Int, _ lattitude: String, _ longitude: String)
}
class AddOrSaveLocationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var editImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressTextField: UITextField!
    
    var addOrSaveLocationTableViewCellDelegate: AddOrSaveLocationTableViewCellDelegate?
    var lattitude: String!
    var longitude: String!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        editImageView.isUserInteractionEnabled = true
        editImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    class func cellIdentifire() -> String{
        return "AddOrSaveLocationTableViewCell"
    }
    
    class func cellHeight() -> CGFloat{
        return 92.0
    }
    
    func configureCellForRowAtIndex(locationData: LocationData, _ index: Int)  {
        profileImageView.image    = UIImage.init(named: locationData.profileImage)
        titleLabel.text                     = locationData.title
        addressTextField.text        = locationData.address
        addressTextField.tag         = index
        lattitude                              = locationData.lattitude
        longitude                            = locationData.longitude
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImageView.image = nil
        //editImageView.image = nil
        titleLabel.text = ""
        addressTextField.text = ""
        lattitude = ""
        longitude = ""
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        addOrSaveLocationTableViewCellDelegate?.sendDataOnImageTapped("home", titleLabelText: titleLabel.text!, addreeTextFieldText: addressTextField.text!, addressTextField.tag,self.lattitude,self.longitude)
        
        // Your action
    }
    
    
}
