//
//  TutorProfileImageTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 09/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol TutorProfileImageTableViewDelegate{
    func addImageButtonTapped()
}

class TutorProfileImageTableViewCell: UITableViewCell {
    
    //MARK: ------------ Variables ------------
    
    var tutorProfileImageTableViewDelegate: TutorProfileImageTableViewDelegate!
    
    //MARK: ------------ @IBOutlets ----------
    
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.text = "Create your account".localized
        titleLabel.font = UIFont(name: QuicksandMedium, size: 24)
        titleLabel.textColor = theamBlackColor
        
        addImageButton.setImage(UIImage(named : "addProfileImage"), for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        profileImage.layer.cornerRadius = profileImage.frame.width/2
        profileImage.clipsToBounds = true
        
        addImageButton.layer.cornerRadius = 20
    }
    
    class func cellIdentifire()->String{
        return "TutorProfileImageTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 175
    }
    
    @IBAction func addImageButtonTapped(_ sender: UIButton) {
        if tutorProfileImageTableViewDelegate != nil{
             tutorProfileImageTableViewDelegate.addImageButtonTapped()
        }
    }
    
}
