//
//  ProfileImageTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 08/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol ProfileImageTableViewDelegate{
    func profileImageTapped()
}

protocol ProfileImageTableViewDelegateCheckBox{
    func checkBoxButtonTapped(_ isSelected: Bool)
}

class ProfileImageTableViewCell: UITableViewCell {
    
    //MARK: ---------------- Properties ----------------
    
     var profileImageTapGesture: UITapGestureRecognizer!
    var profileImageTableViewDelegate: ProfileImageTableViewDelegate!
    var profileImageTableViewDelegateCheckBox: ProfileImageTableViewDelegateCheckBox!

    //MARK: --------------- @IBOutlets ----------------
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var creatYourAccountLabel: UILabel!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var declarationLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addTappGestureOnProfileImageView()
        profileImage.layer.cornerRadius = profileImage.frame.width/2
        //--- checkboxbutton
        checkBoxBtn.setImage(UIImage(named : "checkboxInactive"), for: UIControl.State.normal)
        checkBoxBtn.setImage(UIImage(named : "checkboxActive"), for: UIControl.State.selected)
    }

    //MARK: ---------------
    class func cellIdentifire()->String{
        return "ProfileImageTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 215
    }
    
    fileprivate func setProfileImage(_ url: String){
        DispatchQueue.global(qos: .background).async {
            do
            {
                let data = try Data.init(contentsOf: URL.init(string:url)!)
                DispatchQueue.main.async {
                    let image: UIImage = UIImage(data: data)!
                    self.profileImage.image = image
                }
            }
            catch {
                // error
            }
        }
    }
    
    private func addTappGestureOnProfileImageView(){
        profileImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(profileImageViewTapped))
        profileImage.addGestureRecognizer(profileImageTapGesture)
    }

    @objc private func profileImageViewTapped(){
        //showImageOptions()
        if profileImageTableViewDelegate != nil{
            profileImageTableViewDelegate.profileImageTapped()
        }
    }
}

//MARK: ------------------- @IBActions ------------------

extension ProfileImageTableViewCell{
    @IBAction func checkBoxBtnTapped(_ sender: UIButton) {

        sender.isSelected = !sender.isSelected
        profileImageTableViewDelegateCheckBox.checkBoxButtonTapped(sender.isSelected)
       
    }
}


