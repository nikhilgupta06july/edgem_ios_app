//
//  DropDownTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 14/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import DropDown

class DropDownTableViewCell: DropDownCell {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var locationArrow: UIImageView!
    @IBOutlet weak var locationImage: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        addressLabel.text = ""
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
