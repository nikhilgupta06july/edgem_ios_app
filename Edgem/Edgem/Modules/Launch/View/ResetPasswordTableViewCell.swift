//
//  ResetPasswordTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 26/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit

class ResetPasswordTableViewCell: UITableViewCell {
    
    @IBOutlet weak var errorStateImgaeView: UIImageView!
    @IBOutlet weak var customTextField: EdgemCustomTextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func cellIdentifire()->String{
        return "ResetPasswordTableViewCell"
    }
    
    func configureCellForRowAtIndex(_ index: Int, withText text: String){
        customTextField.tag = index
        customTextField.placeholder = text
        customTextField.title = text
        customTextField.textFont = textFieldMediumFont
        customTextField.placeholderFont = textFieldDefaultFont
        customTextField.titleFont = textFieldDefaultFont
    }
    
    // MARK: Password
    func validatePassword(text: String) {
        if text.isEmptyString() {
            errorStateImgaeView.image = nil
            errorLabel.text = ValidationErrorMessage.passwordEmpty.description()
        } else if !text.isValidPassword() {
            errorStateImgaeView.image = nil
            errorLabel.text = ValidationErrorMessage.passwordInvalid.description()
        } else {
            errorStateImgaeView.image = UIImage(named: "correctIcon")
            errorLabel.text = ""
        }
    }
    
    // MARK: Confirm Password
    
    func validateConfirmPassword(text: String, password: String) {
        if text.isEmptyString() {
            errorStateImgaeView.image = nil
            errorLabel.text = ValidationErrorMessage.passwordEmpty.description()
        } else if !text.isValidPassword() {
            errorStateImgaeView.image = nil
            errorLabel.text = ValidationErrorMessage.passwordInvalid.description()
        } else if text != password {
            errorLabel.text = ValidationErrorMessage.passwordMismatch.description()
            customTextField.textColor = themeRedColor
            errorStateImgaeView.image = UIImage(named: "incorrectIcon")
            customTextField.showImage()
        } else {
            customTextField.textColor = theamBlackColor
            errorStateImgaeView.image = UIImage(named: "correctIcon")
            errorLabel.text = ""
        }
    }
    

}
