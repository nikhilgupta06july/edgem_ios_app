//
//  HeaderCell.swift
//  Edgem
//
//  Created by Hipster on 28/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {
    
    @IBOutlet weak var sectionTitle: UILabel!
    
    class func cellIdentifire()->String{
        return "HeaderCell"
    }
    
    func configureCell(with title: String){
        sectionTitle.text = title
        sectionTitle.font = titleBoldFont
        sectionTitle.textColor = theamBlackColor
    }
}
