//
//  CreatStudentAccountStepTwoViewController.swift
//  Edgem
//
//  Created by Hipster on 01/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation


class CreateStudentAccountStepTwoViewController: BaseViewController, RefreshButtonTappedDelegate {
   
 
    //MARK: - Properties
    
    @IBOutlet weak var logInBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: EdgemCustomButton!
    @IBOutlet weak var creatYourAccount: UILabel!
    private var gradient: CAGradientLayer!
    
    @IBOutlet weak var viewtopConstraint: NSLayoutConstraint!
    //MARK: - Variables
    
    let labelFields = ["","Postal code*".localized, "Address line 1*".localized,"Address line 2".localized, "MRT/LRT Station*".localized]
    var userRegistration: UserRegistration!
    var activeTextField: UITextField!
    var _filledTextField: Bool = false
    
    var isPostalCodeWrong = false
    
    //MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        setGradient()
        viewtopConstraint.constant = UIDevice.current.screenType.rawValue.contains("X") ? 35 : 0
        
        // Add notification observer for common popup yes/no button
        
        NotificationCenter.default.addObserver(self, selector: #selector(yesButtonTapped), name: .yesBtnTapped, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         print(userRegistration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print(userRegistration)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //updategradientFrame()
        gradient.frame = tableView.superview?.bounds ?? .null
    }
    
    //MARK: - Private Methods
    
    // ---- Delegate
    func refreshBtnTapped() {
        print("refresh button tapped")
        self.userRegistration.postalCode = ""
        self.userRegistration.addressLine1 = ""
        self.userRegistration.addressLine2 = ""
        self.tableView.reloadData()
    }
    
    func setGradient(){
        gradient = CAGradientLayer()
        gradient.frame = (tableView.superview?.frame) ?? .null
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0.0, 0.05, 0.98, 1.0]
        tableView.backgroundColor = .clear
        tableView.superview?.layer.mask = gradient
    }
    
    func initialSetUp(){
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // --- update data into textfield
    func updateDataIntoTextField(_ customTextField: EdgemCustomTextFieldForPostal, fromCell: Bool){
        switch customTextField.tag{
        case CreatAccountStudentStepTwoTextFieldType.postalcode.rawValue:
                                                                                                                                fromCell ? (customTextField.text = userRegistration.postalCode) : (userRegistration.postalCode = customTextField.text!)
                                                                                                                                break
        case CreatAccountStudentStepTwoTextFieldType.addressLine1.rawValue:
                                                                                                                                fromCell ? (customTextField.text = userRegistration.addressLine1) : (userRegistration.addressLine1 = customTextField.text!)
                                                                                                                                if customTextField.text != ""{
                                                                                                                                    customTextField.showImage()
                                                                                                                                }else{
                                                                                                                                    customTextField.hideImage()
                                                                                                                                }
                                                                                                                                break
        case CreatAccountStudentStepTwoTextFieldType.addressLine2.rawValue:
                                                                                                                                fromCell ? (customTextField.text = userRegistration.addressLine2) : (userRegistration.addressLine2 = customTextField.text!)
                                                                                                                                if customTextField.text != ""{
                                                                                                                                    customTextField.showImage()
                                                                                                                                    nextBtn.isUserInteractionEnabled = true
                                                                                                                                    nextBtn.backgroundColor = UIColor.white
                                                                                                                                }
                                                                                                                                break
        case CreatAccountStudentStepTwoTextFieldType.mrtlrtstation.rawValue:
                                                                                                                                fromCell ? (customTextField.text = userRegistration.MRT_LRTStation) : (userRegistration.MRT_LRTStation = customTextField.text!)
                                                                                                                                break
        default:
            break
        }
        
        if userRegistration.postalCode == "" || userRegistration.addressLine1 == "" {
            showHideNextButton(false)
            if fromCell == false{
                tableView.beginUpdates()
                tableView.endUpdates()
            }
        }

        
    }
    
    // --- configure textfield
    func configureTextField(_ customTextField: EdgemCustomTextFieldForPostal) {
        customTextField.delegate = self
        customTextField.isSecureTextEntry = false
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        customTextField.inputView = nil
        switch customTextField.tag{
        case CreatAccountStudentStepTwoTextFieldType.postalcode.rawValue:
                                                                                                                                customTextField.keyboardType = .numberPad
                                                                                                                                customTextField.icon = "refreshIcon"
                                                                                                                                
                                                                                                                                customTextField.showImage()
        case CreatAccountStudentStepTwoTextFieldType.addressLine1.rawValue:
                                                                                                                                customTextField.keyboardType = .asciiCapable
                                                                                                                                //customTextField.isUserInteractionEnabled = false
                                                                                                                                 customTextField.icon = "edit"
            
                                                                                                                                if isPostalCodeWrong == false{
                                                                                                                                    customTextField.isUserInteractionEnabled = true
                                                                                                                                }else{
                                                                                                                                    customTextField.isUserInteractionEnabled = false
                                                                                                                                }
            
        case CreatAccountStudentStepTwoTextFieldType.addressLine2.rawValue:
                                                                                                                                customTextField.keyboardType = .asciiCapable
                                                                                                                                customTextField.icon = "edit"
            
                                                                                                                                if isPostalCodeWrong == false{
                                                                                                                                    customTextField.isUserInteractionEnabled = true
                                                                                                                                }else{
                                                                                                                                    customTextField.isUserInteractionEnabled = false
                                                                                                                                }
        case CreatAccountStudentStepTwoTextFieldType.addressLine2.rawValue:
                                                                                                                                customTextField.keyboardType = .asciiCapable
                                                                                                                                customTextField.hideImage()
            
        default:
            customTextField.keyboardType = .asciiCapable
        }
    }
    
    // --- check for textfield blank or not
//    func isTextFieldFilled() -> (Bool){
//        for index in 1...labelFields.count{
//            let currentIndexPath = IndexPath.init(row: index, section: 0)
//            switch index{
//            case CreatAccountStudentStepTwoTextFieldType.postalcode.rawValue:
//                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
//                    if cell.customTextField.text != "" || userRegistration.postalCode != ""{
//                       // _filledTextField = false
//                        _filledTextField = true
////                        return _filledTextField
//                    }else{
//                         _filledTextField = false
//                         return _filledTextField
//                    }
//                }
//            case CreatAccountStudentStepTwoTextFieldType.addressLine1.rawValue:
//                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
//                    if cell.customTextField.text != "" || userRegistration.addressLine1 != ""{
////                        _filledTextField = false
////                        return _filledTextField
//                        _filledTextField = true
//                    }else{
//                        _filledTextField = false
//                        return _filledTextField
//                    }
//                }
//            default:
//                break
//            }
//        }
//
//        return _filledTextField
//    }
    
    // --- check for textfield data validation
   func isDataValid() -> (isValid: Bool, message: String){
        var errorMessages: [String] = []
        for index in 1...labelFields.count{
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index{
            case CreatAccountStudentStepTwoTextFieldType.postalcode.rawValue:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validatePostalCode(text: userRegistration.postalCode)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case CreatAccountStudentStepTwoTextFieldType.addressLine1.rawValue:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateAddress1(text: userRegistration.addressLine1)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
//            case CreatAccountStudentStepTwoTextFieldType.addressLine2.rawValue:
//                break
//                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
//                    cell.validateAddress2(text: userRegistration.addressLine2)
//                    errorMessages.append(cell.errorLabel.text ?? "")
//                }
//            case CreatAccountStudentStepTwoTextFieldType.mrtlrtstation.rawValue:
//                if let cell = tableView.cellForRow(at: currentIndexPath) as? AddressPlacePickerTableViewCell {
//                    //  cell.validateMRT(text: userRegistration.MRT_LRTStation)
//                    errorMessages.append(cell.errorLabel.text ?? "")
//                }
            default:
                break
            }
        }
    let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
        message.count > 0
    }
    tableView.beginUpdates()
    tableView.endUpdates()
//    tableView.reloadData()
    let isValidData = filteredErrorMessages.count == 0
    return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }
    
    func showHideNextButton(_ status: Bool) {
        if status == false{
            nextBtn.isUserInteractionEnabled = false
            nextBtn.backgroundColor = UIColor(white: 1, alpha: 0.60)
        }else{
            nextBtn.isUserInteractionEnabled = true
            nextBtn.backgroundColor = UIColor.white
        }
    }
    
   
    func getLatLngForZip(zipCode: String) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        var request: NSMutableURLRequest? = nil
        let baseUrl = BaseUrlForGoogleMap
        let apikey = GoogleApiKey
        //"\(baseUrl)address=\(zipCode)&key=\(apikey)&components=country:SG"
        //"https://maps.googleapis.com/maps/api/geocode/json?address="+zipCode+"&country=SG&sensor=false&key=\(apikey)"
        let url = "\(baseUrl)address=\(zipCode)&key=\(apikey)&components=country:SG"
        if let aText = URL(string: url) {
            request = NSMutableURLRequest(url: aText)
        }
        request?.httpMethod = "POST"
        var response: URLResponse?
        var responseData: Data? = nil
        Utilities.showHUD(forView: self.view, excludeViews: [])
        if let aRequest = request {
            responseData = try? NSURLConnection.sendSynchronousRequest(aRequest as URLRequest, returning: &response)
        }
        var resSrt: String? = nil
        if let aData = responseData {
            resSrt = String(data: aData, encoding: .ascii)
        }
        
        print("got response==\(resSrt ?? "")")
        
        var dict: [String : AnyObject]? = nil
        if let aData = responseData {
            dict = try! JSONSerialization.jsonObject(with: aData, options: .mutableContainers) as? [String : AnyObject]
        }
//        let addr = ((((dict as! [String:AnyObject])["results"] as! NSArray)[0]) as! [String:AnyObject])["formatted_address"] as! String
////        print(addr)
         let arr = (dict as! [String:AnyObject])["results"] as! NSArray
        if arr.count > 0{
              self.isPostalCodeWrong = false
            let lataddre = ((((((dict as! [String:AnyObject])["results"] as! NSArray)[0]) as! [String:AnyObject])["geometry"] as! [String:AnyObject])["location"] as! [String:AnyObject])["lat"] as! Double
            let longaddr = ((((((dict as! [String:AnyObject])["results"] as! NSArray)[0]) as! [String:AnyObject])["geometry"] as! [String:AnyObject])["location"] as! [String:AnyObject])["lng"] as! Double
            //        getAddressFromLatLon(pdblLatitude: lataddre, withLongitude: longaddr, zipCode)
            getAddressForLatLng(latitude: "\(lataddre)", longitude: "\(longaddr)", zipCode: zipCode)
        }else{
            print("No lat long found")
            Utilities.hideHUD(forView: self.view)
             self.isPostalCodeWrong = true
             self.showSimpleAlertWithMessage("You have entered an invalid postal code.".localized)
             showHideNextButton(false)
             self.userRegistration.addressLine1 = ""
             self.userRegistration.addressLine2 = ""
             self.tableView.reloadRows(at: [IndexPath(row: 2, section: 0), IndexPath(row: 3, section: 0)], with: .automatic)
            
        }

        
    }
    
    func getAddressForLatLng(latitude: String, longitude: String, zipCode: String) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }

        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?address=\(latitude),\(longitude)&key=\(GoogleApiKey)&country=SG&sensor=false")
        let data = NSData(contentsOf: url! as URL)
        if let data = data{
            let string1 = String(data: data as Data, encoding: String.Encoding.utf8) ?? "Data could not be printed"
            print(string1)
        }
        if data != nil {
            let json = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            print(json)
            Utilities.hideHUD(forView: self.view)
            var address = ""
            if let result = json["results"] as? NSArray   {
                if result.count > 0 {
                    for index in 0..<result.count{
                        if let result = result[index] as? [String:AnyObject]{
                            if let addressComponents = result["address_components"] as? NSArray{
                                for addressComponentsIndex in 0..<addressComponents.count{
                                    if let components = addressComponents[addressComponentsIndex] as? [String:AnyObject] {
                                        if let componentTypes = components["types"] as? NSArray{
                                            for componentTypeIndex in 0..<componentTypes.count{
                                                if let type = componentTypes[componentTypeIndex] as? String{
                                                    if type == "postal_code" && components["long_name"] as! String == zipCode{
                                                        print("postal_code key existed and address : \(String(describing:  result["formatted_address"]))")
                                                        if let addr =  result["formatted_address"] as? String {
                                                            address = addr
                                                        }
                                                    }else{
                                                        print("postal_code key not existed")
                                                    }
                                                }
                                            
                                            }
                                        
                                    }
                                }
                            }
                        }
                }
            }
         }
        }
            //----
            if address != ""{
                let addressComponent = address.components(separatedBy: ",")
                if addressComponent.count > 1{
                    print (address)
                    self.userRegistration.addressLine1 = addressComponent[0]
                    var addr2: [String] = Array()
                    for index in 1..<addressComponent.count{
                        addr2.append(addressComponent[index])
                    }
                    self.userRegistration.addressLine2 = addr2.joined(separator: ",")
                }else if  addressComponent.count == 1 && addressComponent[0] != ""{
                    self.userRegistration.addressLine1 = addressComponent[0]
                    self.userRegistration.addressLine2 = ""
                }
                self.userRegistration.lattitude = latitude
                self.userRegistration.longitude = longitude
                self.tableView.reloadData()
            }else{
                 self.showSimpleAlertWithMessage("You have entered an invalid postal code".localized)
                self.userRegistration.addressLine1 = ""
                self.userRegistration.addressLine2 = ""
                self.tableView.reloadData()
            }
            
        }
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double, _ zipCode: String) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude//Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = pdblLongitude//Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                 Utilities.hideHUD(forView: self.view)
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    if zipCode == ""{
                    }
                     if pm.postalCode != nil && pm.postalCode == zipCode{
                        var addressString1 : String = ""
                        var addressString2 : String = ""
                        if pm.subThoroughfare != nil {
                            addressString1 = addressString1 + pm.subThoroughfare! + ", "
                        }
                        if pm.subLocality != nil {
                            addressString1 = addressString1 + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString1 = addressString1 + pm.thoroughfare! + " "
                        }
                        if pm.locality != nil {
                            addressString2 = addressString2 + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString2 = addressString2 + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString2 = addressString2 + pm.postalCode! + " "
                        }
                        
                        self.userRegistration.addressLine1 = addressString1
                        self.userRegistration.addressLine2 = addressString2
                        self.userRegistration.lattitude = String(lat)
                        self.userRegistration.longitude = String(lon)
                        self.tableView.reloadData()
                        self.validateTextFields(zipCode, addressString1, addressString2)
                        print(addressString1+addressString2)
                     }else if(pm.postalCode == nil){
                        if pm.country != ""{
                            var addressString1 : String = ""
                            var addressString2 : String = ""
                            if pm.subThoroughfare != nil {
                                addressString1 = addressString1 + pm.subThoroughfare! + ", "
                            }
                            if pm.subLocality != nil {
                                addressString1 = addressString1 + pm.subLocality! + ", "
                            }
                            if pm.thoroughfare != nil {
                                addressString1 = addressString1 + pm.thoroughfare! + " "
                            }
                            if pm.locality != nil {
                                addressString2 = addressString2 + pm.locality! + ", "
                            }
                            if pm.country != nil {
                                addressString2 = addressString2 + pm.country! + " "
                            }
                            if pm.postalCode != nil {
                                addressString2 = addressString2 + pm.postalCode! + " "
                            }
                            
                            self.userRegistration.addressLine1 = addressString1
                            self.userRegistration.addressLine2 = addressString2
                            self.userRegistration.lattitude = String(lat)
                            self.userRegistration.longitude = String(lon)
                            self.tableView.reloadData()
                            self.validateTextFields(zipCode, addressString1, addressString2)
                            print(addressString1+addressString2)
                            
                        }
                     }else{
                        if zipCode == ""{
                            //ValidationErrorMessage.postalCodeEmpty.description()
                             self.showAlertWithTitle(title: AppName, message:  ValidationErrorMessage.postalCodeEmpty.description())
                        }else{
                             self.showAlertWithTitle(title: AppName, message: AlertMessage.wrongPostalCode.description())
                        }
                       
                    }
                }else{
                    self.userRegistration.addressLine1 = ""
                    self.userRegistration.addressLine2 = ""
                    self.tableView.reloadData()
                }
        })
        
    }
    
    func validateTextFields(_ zipCode: String, _ addr1: String, _ addr2: String){
        for index in 1...labelFields.count{
            let indexPath = IndexPath(row: index, section: 0)

            switch index{
            case CreatAccountStudentStepTwoTextFieldType.postalcode.rawValue:
                let cell = tableView.cellForRow(at: indexPath) as! EdgemTextFieldTableViewCell
                cell.validatePostalCode(text: zipCode )
            case CreatAccountStudentStepTwoTextFieldType.addressLine1.rawValue:
                let cell = tableView.cellForRow(at: indexPath) as! EdgemTextFieldTableViewCell
                cell.validateAddress1(text: addr1 )
            case CreatAccountStudentStepTwoTextFieldType.addressLine2.rawValue:
                let cell = tableView.cellForRow(at: indexPath) as! EdgemTextFieldTableViewCell
                cell.validateAddress2(text: addr2 )
            case CreatAccountStudentStepTwoTextFieldType.mrtlrtstation.rawValue:
                if (tableView.cellForRow(at: indexPath) as? AddressPlacePickerTableViewCell) != nil {
                }
            default:
                break
            }
        }
    }
    
    func showAlertWithTitle( title:String, message:String ) {
        
        let commonPopUP = ShowInfoCommonPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        commonPopUP.fromController = self
        commonPopUP.customMsg =  message
        self.userRegistration.addressLine1 = ""
        self.userRegistration.addressLine2 = ""
        self.tableView.reloadData()
        self.present(commonPopUP, animated: true, completion: nil)
        
    }
    
    @objc func yesButtonTapped(){
        print("yes button tapped")
        self.navigationController?.popViewController(animated: true)
    }
  
    //MARK: - Actions
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if userRegistration.postalCode != "" ||
            userRegistration.addressLine1 != "" /*||
            userRegistration.addressLine2 != "" ||
            userRegistration.MRT_LRTStation != "" */{
          
                 let alert = UIAlertController(title: AppName, message: AlertMessage.removeFieldData.description(), preferredStyle: .alert)
                 let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                 self.navigateBack(sender: sender)
                 }
                 
                 let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
                 alert.addAction(yesAction)
                 alert.addAction(noAction)
                 self.present(alert, animated: true, completion: nil)
                

        } else {
            navigateBack(sender: sender)
        }
    }
    
    @IBAction func logInBtnTapped(_ sender: UIButton) {
      self.logInBtnTapped()
    }
    
    @IBAction func nextBtnTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        let validationInformation = isDataValid()
        guard validationInformation.isValid else {
            self.showSimpleAlertWithMessage(validationInformation.message)
            return
        }
        let verifyPhoneNumberVC = VerifyPhoneNumberViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        verifyPhoneNumberVC.userRegistration = self.userRegistration
        self.navigationController?.pushViewController(verifyPhoneNumberVC, animated: true)
    }
    
    
    @IBAction func edgemTextField(_ sender: EdgemCustomTextFieldForPostal) {
   
    }
    
    
}


    //MARK: TableView Delegates/Datasource

extension CreateStudentAccountStepTwoViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelFields.count-1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else{
             let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
            let label = cell.viewWithTag(1) as! UILabel
            label.text = "Create your account".localized
             return cell
        }
        
        guard indexPath.row != 4 else{
            let cell = tableView.dequeueReusableCell(withIdentifier: AddressPlacePickerTableViewCell.cellIdentifier(), for: indexPath) as! AddressPlacePickerTableViewCell
            cell.backgroundColor = cell.contentView.backgroundColor
            cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
            updateDataIntoTextField(cell.placeTextField, fromCell: true)
            cell.delegate = self
            configureTextField(cell.placeTextField)
            return cell

        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EdgemTextFieldTableViewCell.cellIdentifier(), for: indexPath) as? EdgemTextFieldTableViewCell  else{return UITableViewCell()}
        cell.configurePostalCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
        if indexPath.row != 1{
            cell.refreshBtn.isUserInteractionEnabled = false
        }
        updateDataIntoTextField(cell.customTextFieldForPostal, fromCell: true)
        configureTextField(cell.customTextFieldForPostal)
        cell.refreshDelegate = self
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return 175
        }
        return UITableView.automaticDimension
    }
    
    
}

    //MARK: - TextField Delegate

extension CreateStudentAccountStepTwoViewController: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        updateDataIntoTextField(textField as! EdgemCustomTextFieldForPostal, fromCell: false)
        
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        
        switch textField.tag {
            
        case CreatAccountStudentStepTwoTextFieldType.postalcode.rawValue:
            
            let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
            cell.validatePostalCode(text: textField.text ?? "")
            
            if textField.text?.isValidPostalCode() ?? false{
                
                getLatLngForZip(zipCode: textField.text ?? "")
                
            }else{
                
                self.userRegistration.addressLine1 = ""
                self.userRegistration.addressLine2 = ""
                self.tableView.reloadRows(at: [IndexPath(row: 2, section: 0), IndexPath(row: 3, section: 0)], with: .automatic)
            }
            
        case CreatAccountStudentStepTwoTextFieldType.addressLine1.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
            //cell.customTextField.isUserInteractionEnabled = false
            cell.validateAddress1(text: textField.text ?? "")
//        case CreatAccountStudentStepTwoTextFieldType.addressLine2.rawValue:
//            let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
//            cell.validateAddress2(text: textField.text ?? "")
//        case CreatAccountStudentStepTwoTextFieldType.mrtlrtstation.rawValue:
//            _ = tableView.cellForRow(at: currentIndexPath) as! AddressPlacePickerTableViewCell
            //cell.validateMRT(text: textField.text ?? "")
        default:
            break
        }
        if userRegistration.postalCode != "" && userRegistration.addressLine1 != "" {
         showHideNextButton(isDataValid().isValid)
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        
        switch textField.tag {
            
             case CreatAccountStudentStepTwoTextFieldType.postalcode.rawValue:
                                                                                                                                guard let stringRange = Range(range, in: currentText) else { return false }
                
                                                                                                                                let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
                
                                                                                                                                return updatedText.count <= 6
            
        default:
            return true
        }
       
    }
    
}

//MARK: - Place Picker Delegate

extension CreateStudentAccountStepTwoViewController: AddressPlacePickerTableViewCellDelegate{
    func showPlacePicker() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
}

//MARK: GMSAutocompleteViewControllerDelegate
extension CreateStudentAccountStepTwoViewController: GMSAutocompleteViewControllerDelegate{
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        userRegistration.lattitude = String(place.coordinate.latitude)
        userRegistration.longitude = String(place.coordinate.longitude)
        userRegistration.MRT_LRTStation = place.formattedAddress ?? place.name
//        // Get the address components.
//        if let addressLines = place.addressComponents {
//            // Populate all of the address fields we can find.
//            for field in addressLines {
//                switch field.type {
//                case kGMSPlaceTypeCountry:
//                    let index = countries.index { (country) -> Bool in
//                        country.name == field.name
//                    }
//                    if let _index = index {
//                        userRegistration.country = countries[_index].name
//                        userRegistration.countryID = countries[_index].id
//                    }
//                case kGMSPlaceTypePostalCode:
//                    userRegistration.zipCode = field.name
//                case kGMSPlaceTypeLocality:
//                    userRegistration.city = field.name
//                default:
//                    print("ignore")
//                }
//            }
//        }
         tableView.reloadData()
        let cell = tableView.cellForRow(at: IndexPath.init(row: 4, section: 0)) as! AddressPlacePickerTableViewCell
        cell.placeTextField.text = place.formattedAddress ?? place.name
        cell.validateAddress(text: cell.placeTextField.text ?? "")
         dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
