//
//  CreatStudentAccountViewController.swift
//  Edgem
//
//  Created by Hipster on 30/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import RxSwift


class CreateStudentAccountViewController: BaseViewController,ProfileImageTableViewDelegateCheckBox  {
   
    
    //MARK: - @IBOutlets
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var nextBtn: EdgemCustomButton!
    @IBOutlet weak var popContainerView: UIView!
    @IBOutlet weak var popView: UIView!
    @IBOutlet weak var okButton: UIButton!
    //@IBOutlet weak var notificationCheckBoxBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewTopConstraint: NSLayoutConstraint!
    //@IBOutlet weak var notificationLabel: UILabel!
    
    
    //MARK: - Properties
    
    let labelFields = ["","First Name*".localized, "Last Name*".localized, "Gender*".localized, "Birthday*".localized, "Email Address*".localized ]
    let labelFields2 = ["","First Name*".localized, "Last Name*".localized,"Relationship with child*".localized, "Email Address*".localized]
    let labelFields3 = ["","First Name*".localized, "Last Name*".localized, "Gender*".localized, "Birthday*".localized, "Email Address*".localized, "Contact Number*".localized ]
    
    var errorDictForChild = ["FirstName": "", "LastName": "", "Gender": "", "Birthday": "", "EmailAddress": ""]
    var errorDictForParent = ["FirstName": "", "LastName": "", "Relationship": "", "EmailAddress": ""]
    
    
    private var gradient: CAGradientLayer!
    var sectionHeaderTitle: [String]  = ["","Parent’s Details".localized, "Child’s Details".localized]
    var countryCodeOptions = ["+65"]
    var userRegistration: UserRegistration!
    let _relationwithchild_pickerview = UIPickerView()
    let genderPickerView = UIPickerView()
    let datePicker = UIDatePicker()
    let countryCodePicker = UIPickerView()
    var activeTextField: UITextField!
    var disposableBag = DisposeBag()
    var profileImageTapGesture: UITapGestureRecognizer!
    var checkBoxBtn: Bool = false
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUP()
         viewTopConstraint.constant = UIDevice.current.screenType.rawValue.contains("X") ? 35 : 0
        popUpContainerViewShadow()
        if userRegistration.imageURL != ""{
             setProfileImage(userRegistration.imageURL)
        }
        
        userRegistration.childContactCode = "+65"
        showHideNextButton(false)
        setGradient()
        
        // Add notification observer for common popup yes/no button
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateStudentAccountViewController.okBtnTapped(_:)), name: NSNotification.Name(rawValue: "OKButtonTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(yesButtonTapped), name: .yesBtnTapped, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         popContainerView.isHidden = true
         popView.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print(userRegistration)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //updategradientFrame()
        gradient.frame = tableView.superview?.bounds ?? .null
    }
    
    //MARK: - Private Functions
    
    func didTaphereText(){
                    let commonPopUP = ShowInfoCommonPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                    commonPopUP.fromController = self
                    commonPopUP.customMsg = CustomAlertMessages.verificationNotify.description()
                    self.present(commonPopUP, animated: true, completion: nil)
    }
    
    @objc func okBtnTapped(_ notification: NSNotification) {
        print ("Ok Btn tapped")
        self.dismissPopUp()
//         if checkBoxBtn == true &&  nextBtn.isUserInteractionEnabled{
//
//                let creatStudentAccountStepTwoVC = CreateStudentAccountStepTwoViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
//                creatStudentAccountStepTwoVC.userRegistration = self.userRegistration
//                self.navigationController?.pushViewController(creatStudentAccountStepTwoVC, animated: true)
//        }else{
//
//            self.dismissPopUp()
//
//        }
        
    }
    
    func setGradient(){
        gradient = CAGradientLayer()
        gradient.frame = (tableView.superview?.frame) ?? .null
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0.0, 0.05, 0.98, 1.0]
        tableView.backgroundColor = .clear
        tableView.superview?.layer.mask = gradient
    }
    
    fileprivate func setProfileImage(_ url: String){
        
        let indexPath = IndexPath(row: 0, section: 0)
        guard let cell = self.tableView.cellForRow(at: indexPath) as? ProfileImageTableViewCell else{return}
        if url == "Female"{
            cell.profileImage.image = UIImage(named: "femaleIcon")
        }else if url == "Male"{
            cell.profileImage.image = UIImage(named: "maleIcon")
        }else{
             cell.profileImage.image = UIImage(named: "defaultIcon")
        }
 
    }
    
    func popUpContainerViewShadow(){
        popView.layer.shadowColor = UIColor.black.cgColor
        popView.layer.shadowOpacity = 0.83
        popView.layer.shadowOffset = CGSize.zero
        popView.layer.shadowRadius = 10
        popView.layer.shadowPath = UIBezierPath(rect: popView.bounds).cgPath
        //popView.layer.shouldRasterize = true
    }
    
    func initialSetUP(){
        
        _relationwithchild_pickerview.delegate = self
        _relationwithchild_pickerview.dataSource = self
        _relationwithchild_pickerview.tag = 1
        
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        genderPickerView.tag = 2
        
        countryCodePicker.delegate = self
        countryCodePicker.dataSource = self
        countryCodePicker.tag = 3
        
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
        
    }
    
    @objc func yesButtonTapped(){
        print("yes button tapped")
        self.navigationController?.popViewController(animated: true)
    }
   
    //MARK: - Methods
    
    @IBAction func okButtonTapped(_ sender: UIButton){
//        popView.isHidden = true
//        let creatStudentAccountStepTwoVC = CreateStudentAccountStepTwoViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
//        creatStudentAccountStepTwoVC.userRegistration = self.userRegistration
//        self.navigationController?.pushViewController(creatStudentAccountStepTwoVC, animated: true)
    }
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if userRegistration.firstName != "" ||
            userRegistration.lastName != "" ||
            userRegistration.email != "" ||
            userRegistration.gender != "" ||
            userRegistration.childBirthday != "" {
            let alert = UIAlertController(title: AppName, message: AlertMessage.removeFieldData.description(), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                self.navigateBack(sender: sender)
            }
            
            let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
            alert.addAction(yesAction)
            alert.addAction(noAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            navigateBack(sender: sender)
        }
    }
    
    @IBAction func logInBtnTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func checkBoxButtonTapped(_ isSelected: Bool){
        checkBoxBtn = isSelected
        
        DispatchQueue.main.async {
          self.tableView.reloadData()
            
            let status = self.checkBoxBtn ? self.isDataValid(1).isValid && self.isDataValid(2).isValid : self.isDataValid(0).isValid
            self.showHideNextButton(status)
        
        
        }
      
    }
    
    @IBAction func nextBtnTapped(_ sender: EdgemCustomButton) {
        self.view.endEditing(true)
        
        //self.tableView.reloadData()
        if checkBoxBtn == false {
            let validationInformation = isDataValid(0)
          UserStore.shared.isParrent = false
            guard validationInformation.isValid else {
                self.showSimpleAlertWithMessage(validationInformation.message)
                return
            }
//            popContainerView.isHidden = true
//            popView.isHidden = true
            let creatStudentAccountStepTwoVC = CreateStudentAccountStepTwoViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            creatStudentAccountStepTwoVC.userRegistration = self.userRegistration
            self.navigationController?.pushViewController(creatStudentAccountStepTwoVC, animated: true)
        }else{
            
            UserStore.shared.isParrent = true
            userRegistration.isParent = 1
            let validationInformation = isDataValid(1)
        
            //UserStore.shared.isParrent = false
            guard validationInformation.isValid else {
                self.showSimpleAlertWithMessage(validationInformation.message)
                return
            }
            let _validationInformation = isDataValid(2)
            //UserStore.shared.isParrent = false
            guard _validationInformation.isValid else {
                self.showSimpleAlertWithMessage(_validationInformation.message)
                return
            }
            
            if let parent_email = userRegistration.email as? String, !parent_email.isEmptyString(), let child_email = userRegistration.childEmail as? String, !child_email.isEmptyString(){
                if parent_email == child_email{
                    self.showSimpleAlertWithMessage("Parent and child email id should be different.")
                    return
                }
            }
            
            //popContainerView.isHidden = false
            //popView.isHidden = false
            if checkBoxBtn == true &&  nextBtn.isUserInteractionEnabled{
                
                let creatStudentAccountStepTwoVC = CreateStudentAccountStepTwoViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                creatStudentAccountStepTwoVC.userRegistration = self.userRegistration
                self.navigationController?.pushViewController(creatStudentAccountStepTwoVC, animated: true)
                
            }
        }
    }
    
     //MARK:- Helping Function
    
    func configureTextField(_ cell: EdgemTextFieldTableViewCell?, _ customTextField: EdgemCustomTextField, _ section: Int) {
        customTextField.delegate = self
        customTextField.isSecureTextEntry = false
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        customTextField.inputView = nil

        if checkBoxBtn == false{
            switch customTextField.tag {
            case CreateAccountTutorTextFieldType.firstName.rawValue:
                customTextField.keyboardType = .asciiCapable
                customTextField.autocapitalizationType = .words
                customTextField.hideImage()
                cell?.errorLabel.text = errorDictForChild["FirstName"]
                customTextField.tintColor =  UIColor.blue
            case CreateAccountTutorTextFieldType.lastName.rawValue:
                customTextField.keyboardType = .asciiCapable
                customTextField.autocapitalizationType = .words
                customTextField.hideImage()
                cell?.errorLabel.text = errorDictForChild["LastName"]
            case CreateAccountTutorTextFieldType.gender.rawValue:
                customTextField.icon = "drop-down-arrow"
                customTextField.showImage()
                customTextField.inputView = genderPickerView
                customTextField.tintColor = UIColor.clear
                cell?.errorLabel.text = errorDictForChild["Gender"]

            case CreateAccountTutorTextFieldType.birthday.rawValue:
                //customTextField.icon = "drop-down-arrow"
                //customTextField.showImage()
                customTextField.inputView = datePicker
                customTextField.tintColor = UIColor.clear
                cell?.errorLabel.text = errorDictForChild["Birthday"]

            case CreateAccountTutorTextFieldType.emailAddress.rawValue:
                customTextField.keyboardType = .emailAddress
                customTextField.hideImage()
                customTextField.tintColor = UIColor.blue
                customTextField.autocapitalizationType = .none
                cell?.errorLabel.text = errorDictForChild["EmailAddress"]
            default:
                customTextField.keyboardType = .asciiCapable
            }
        } else{
            if section == 0{

            } else if section == 1{
                switch customTextField.tag {
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.parentfirstName.rawValue:
                    customTextField.keyboardType = .asciiCapable
                     customTextField.autocapitalizationType = .words
                    customTextField.hideImage()
                     customTextField.tintColor = UIColor.blue
                    cell?.errorLabel.text = errorDictForParent["FirstName"]

                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.parentlastName.rawValue:
                    customTextField.keyboardType = .asciiCapable
                     customTextField.autocapitalizationType = .words
                    customTextField.hideImage()
                    customTextField.tintColor = UIColor.blue
                    cell?.errorLabel.text = errorDictForParent["LastName"]
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.relationshipwithchild.rawValue:
                    customTextField.icon = "drop-down-arrow"
                    customTextField.showImage()
                    customTextField.inputView = _relationwithchild_pickerview
                    customTextField.tintColor = UIColor.clear
                    cell?.errorLabel.text = errorDictForParent["Relationship"]

                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.parentemailaddress.rawValue:
//                    customTextField.icon = "drop-down-arrow"
                    customTextField.keyboardType = .emailAddress
                    customTextField.hideImage()
                    customTextField.tintColor = UIColor.blue
                    customTextField.autocapitalizationType = .none
//                    customTextField.inputView = datePicker
                    cell?.errorLabel.text = errorDictForParent["EmailAddress"]

                default:
                    customTextField.keyboardType = .asciiCapable
                }
            }else{

                switch customTextField.tag {
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.firstName.rawValue:
                    customTextField.keyboardType = .asciiCapable
                     customTextField.autocapitalizationType = .words
                    customTextField.hideImage()
                    customTextField.tintColor = UIColor.blue
                    cell?.errorLabel.text = errorDictForChild["FirstName"]

                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.lastName.rawValue:
                    customTextField.keyboardType = .asciiCapable
                     customTextField.autocapitalizationType = .words
                    customTextField.hideImage()
                    customTextField.tintColor = UIColor.blue
                    cell?.errorLabel.text = errorDictForChild["LastName"]

                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.gender.rawValue:
                    customTextField.icon = "drop-down-arrow"
                    customTextField.showImage()
                    customTextField.inputView = genderPickerView
                    customTextField.tintColor = UIColor.clear
                    cell?.errorLabel.text = errorDictForChild["Gender"]

                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.birthday.rawValue:
                    //customTextField.icon = "drop-down-arrow"
                    //customTextField.showImage()
                    customTextField.inputView = datePicker
                    customTextField.tintColor = UIColor.clear
                    cell?.errorLabel.text = errorDictForChild["Birthday"]

                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.emailAddress.rawValue:
                    customTextField.keyboardType = .emailAddress
                    customTextField.hideImage()
                    customTextField.tintColor = UIColor.blue
                    customTextField.autocapitalizationType = .none
                    cell?.errorLabel.text = errorDictForChild["EmailAddress"]
                    
                case CreatAccountStudentStepTwoDualTextFieldType.dual.rawValue:
                    if customTextField.title == "Contact Number*".localized {
                        customTextField.keyboardType = .phonePad
                        customTextField.tintColor = UIColor.blue

                    } else{
                        customTextField.inputView = countryCodePicker
                        customTextField.icon = "drop-down-arrow"
                        customTextField.showImage()
                        customTextField.keyboardType = .phonePad
                        customTextField.tintColor = UIColor.clear
                    }
                    
                default:
                    customTextField.keyboardType = .asciiCapable
                }
            }
        }
    }

    
    func updateDataIntoTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool, _ section: Int) {
        
        if checkBoxBtn == false{
            switch customTextField.tag {
            case CreateAccountTutorTextFieldType.firstName.rawValue:
                fromCell ? (customTextField.text = userRegistration.firstName) : (userRegistration.firstName = customTextField.text!)
                break
            case CreateAccountTutorTextFieldType.lastName.rawValue:
                fromCell ? (customTextField.text = userRegistration.lastName) : (userRegistration.lastName = customTextField.text!)
                break
            case CreateAccountTutorTextFieldType.gender.rawValue:
                fromCell ? (customTextField.text = userRegistration.gender) : (userRegistration.gender = customTextField.text!)
                break
            case CreateAccountTutorTextFieldType.birthday.rawValue:
                fromCell ? (customTextField.text = userRegistration.childBirthday) : (userRegistration.childBirthday = customTextField.text!)
                break
            case CreateAccountTutorTextFieldType.emailAddress.rawValue:
                fromCell ? (customTextField.text = userRegistration.email) : (userRegistration.email = customTextField.text!)
                break
            default:
                break
            }
            
            if userRegistration.firstName == "" || userRegistration.lastName == "" || userRegistration.gender == "" || userRegistration.childBirthday == "" || userRegistration.email == "" {
                showHideNextButton(false)
                if fromCell == false{
                    tableView.beginUpdates()
                    tableView.endUpdates()
                }
            }
        }else{
            if section == 0{

            }else if(section == 1){
                switch customTextField.tag {
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.parentfirstName.rawValue:
                    fromCell ? (customTextField.text = userRegistration.firstName) : (userRegistration.firstName = customTextField.text!)
                    break
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.parentlastName.rawValue:
                    fromCell ? (customTextField.text = userRegistration.lastName) : (userRegistration.lastName = customTextField.text!)
                    break
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.relationshipwithchild.rawValue:
                    fromCell ? (customTextField.text = userRegistration.relationwithchild) : (userRegistration.relationwithchild = customTextField.text!)
                    break
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.parentemailaddress.rawValue:
                    fromCell ? (customTextField.text = userRegistration.email) : (userRegistration.email = customTextField.text!)
                    break
                default:
                    break
                }
                
                if userRegistration.firstName == "" || userRegistration.lastName == "" || userRegistration.relationwithchild == "" || userRegistration.email == ""  {
                    showHideNextButton(false)
                    if fromCell == false{
                        tableView.beginUpdates()
                        tableView.endUpdates()
                    }
                }
            }else{
                switch customTextField.tag {
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.firstName.rawValue:
                    fromCell ? (customTextField.text = userRegistration.childFirstName) : (userRegistration.childFirstName = customTextField.text!)
                    break
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.lastName.rawValue:
                    fromCell ? (customTextField.text = userRegistration.childLastName) : (userRegistration.childLastName = customTextField.text!)
                    break
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.gender.rawValue:
                    fromCell ? (customTextField.text = userRegistration.childGender) : (userRegistration.childGender = customTextField.text!)
                    break
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.birthday.rawValue:
                    fromCell ? (customTextField.text = userRegistration.childBirthday) : (userRegistration.childBirthday = customTextField.text!)
                    break
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.emailAddress.rawValue:
                    fromCell ? (customTextField.text = userRegistration.childEmail) : (userRegistration.childEmail = customTextField.text!)
                    break
                case CreatAccountStudentStepTwoDualTextFieldType.dual.rawValue:
                    if customTextField.title == "Contact Number*".localized {
                        fromCell ? (customTextField.text = userRegistration.childContactNumber) : (userRegistration.childContactNumber = customTextField.text!)
                    } else {
                        fromCell ? (customTextField.text = userRegistration.childContactCode) : (userRegistration.childContactCode = customTextField.text!)
                    }
                    break
                default:
                    break
                }
                if userRegistration.childFirstName == "" || userRegistration.childLastName == "" || userRegistration.childGender == "" || userRegistration.childBirthday == "" || userRegistration.childEmail == "" {
                    showHideNextButton(false)
                    if fromCell == false{
                        tableView.beginUpdates()
                        tableView.endUpdates()
                    }
                }
            }
        }
        
        
        
    }
    
    func isDataValid(_ section: Int) -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        if checkBoxBtn == false{
            if userRegistration.firstName.isEmpty && userRegistration.lastName.isEmpty && userRegistration.gender.isEmpty && userRegistration.childBirthday.isEmpty && userRegistration.email.isEmpty{
                return (isValid: false, message: "")
            } else {
                
            for index in 1...labelFields.count {
                let currentIndexPath = IndexPath.init(row: index, section: 0)
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                
                switch index {
                case CreateAccountTutorTextFieldType.firstName.rawValue:
                        cell.validateFirstName(text: userRegistration.firstName)
                        errorDictForChild["FirstName"] = cell.errorLabel.text ?? ""

                case CreateAccountTutorTextFieldType.lastName.rawValue:
                        cell.validateLastName(text: userRegistration.lastName)
                        errorDictForChild["LastName"] = cell.errorLabel.text ?? ""

                case CreateAccountTutorTextFieldType.gender.rawValue:
                        cell.validateGenderName(text: userRegistration.gender)
                        errorDictForChild["Gender"] = cell.errorLabel.text ?? ""

                case CreateAccountTutorTextFieldType.birthday.rawValue:
                        cell.validateBirthday(text: userRegistration.childBirthday)
                        errorDictForChild["Birthday"] = cell.errorLabel.text ?? ""

                case CreateAccountTutorTextFieldType.emailAddress.rawValue:
                        cell.validateEmail(text: userRegistration.email)
                        errorDictForChild["EmailAddress"] = cell.errorLabel.text ?? ""
                        
                        if cell.errorLabel.text?.count == 0, !userRegistration.email.isEmpty {
                            verifyEmail(userRegistration.email, 0)
                    }

                default:
                    break
                }
                    let message = cell.errorLabel.text ?? ""
                    if !message.isEmptyString() {
                        errorMessages.append(message)
                    }
             }
            }
                tableView.beginUpdates()
                tableView.endUpdates()
                
                let isValidData = errorMessages.count == 0
                return (isValid: isValidData, message: isValidData ? "" : errorMessages[0])
            }

        }else{
            if section == 0{
                
            }else if section == 1{
                if userRegistration.firstName.isEmpty && userRegistration.lastName.isEmpty && userRegistration.relationwithchild.isEmpty && userRegistration.email.isEmpty{
                    return (isValid: false, message: "")
                } else {
                    
                for index in 1...labelFields2.count{
                    let currentIndexPath = IndexPath.init(row: index, section: 1)
                    if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    switch index {
                    case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.parentfirstName.rawValue:
                            cell.validateParentFirstName(text: userRegistration.firstName)

                    case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.parentlastName.rawValue:
                            cell.validateLastName(text: userRegistration.lastName)
                        errorDictForParent["LastName"] = cell.errorLabel.text ?? ""
                    case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.relationshipwithchild.rawValue:
                            cell.validateRelationWithChild(text: userRegistration.relationwithchild)
                            errorDictForParent["Relationship"] = cell.errorLabel.text ?? ""

                    case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.parentemailaddress.rawValue:
                            cell.validateEmail(text: userRegistration.email)
                            errorDictForParent["EmailAddress"] = cell.errorLabel.text ?? ""

                    default:
                        break
                    }
                        let message = cell.errorLabel.text ?? ""
                        if !message.isEmptyString() {
                            errorMessages.append(message)
                        }
                    }
                }
                    tableView.beginUpdates()
                    tableView.endUpdates()
                    let isValidData = errorMessages.count == 0
                    return (isValid: isValidData, message: isValidData ? "" : errorMessages[0])

                }
            }else{
                if userRegistration.childFirstName.isEmpty && userRegistration.childLastName.isEmpty && userRegistration.childGender.isEmpty && userRegistration.childBirthday.isEmpty && userRegistration.childEmail.isEmpty {
                    return (isValid: false, message: "")
                } else {
                    
                for index in 1...labelFields3.count{
                    let currentIndexPath = IndexPath.init(row: index, section: 2)
                    if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    switch index {
                    case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.firstName.rawValue:
                        
                            cell.validateChildFirstName(text: userRegistration.childFirstName)
                            errorDictForChild["FirstName"] = cell.errorLabel.text ?? ""

                        
                    case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.lastName.rawValue:
                            cell.validateLastName(text: userRegistration.childLastName)
                            errorDictForChild["LastName"] = cell.errorLabel.text ?? ""

                    case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.gender.rawValue:
                            cell.validateGenderName(text: userRegistration.childGender)
                            errorDictForChild["Gender"] = cell.errorLabel.text ?? ""

                    case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.birthday.rawValue:
                            cell.validateBirthday(text: userRegistration.childBirthday)
                            errorDictForChild["Birthday"] = cell.errorLabel.text ?? ""

                    case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.emailAddress.rawValue:
                            cell.validateEmail(text: userRegistration.childEmail)
                            errorDictForChild["EmailAddress"] = cell.errorLabel.text ?? ""
                        
                            if cell.errorLabel.text?.count == 0, !userRegistration.email.isEmpty {
                                verifyEmail(userRegistration.childEmail, 2)
                            }
            
                    default:
                        break
                    }
                        let message = cell.errorLabel.text ?? ""
                        if !message.isEmptyString() {
                            errorMessages.append(message)
                        }
                    }
                }
                    
                tableView.beginUpdates()
                tableView.endUpdates()
                let isValidData = errorMessages.count == 0
                return (isValid: isValidData, message: isValidData ? "" : errorMessages[0])
                
            }

            }
        }
        return (isValid: false, message: "")
    }
    
    func showHideNextButton(_ status: Bool) {
       
        if status == false{
            nextBtn.isUserInteractionEnabled = false
            nextBtn.backgroundColor = UIColor(white: 1, alpha: 0.60)
        }else{
            nextBtn.isUserInteractionEnabled = true
            nextBtn.backgroundColor = UIColor.white
        }
    }
    
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        activeTextField.text = Utilities.getFormattedDate(sender.date, dateFormat: "dd MMMM yyyy")
    }
    
}

extension CreateStudentAccountViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        let textField = activeTextField as! EdgemCustomTextField
        _relationwithchild_pickerview.reloadAllComponents()
        _relationwithchild_pickerview.reloadInputViews()
        genderPickerView.reloadAllComponents()
        genderPickerView.reloadInputViews()
        countryCodePicker.reloadAllComponents()
        countryCodePicker.reloadInputViews()
        if textField.title == "Relationship with child*".localized {
                textField.inputView = _relationwithchild_pickerview
                let index = relationOptions.index(of: userRegistration.relationwithchild)
                if let _index = index {
                    self._relationwithchild_pickerview.selectRow(_index, inComponent: 0, animated: true)
                } else {
                    _relationwithchild_pickerview.selectRow(0, inComponent: 0, animated: true)
                    activeTextField.text = relationOptions[0]
                }
        } else if(textField.text?.contains("+") == true){
            textField.inputView = countryCodePicker
            let index = countryCodeOptions.index(of: userRegistration.childContactCode)
            if let _index = index {
                self.countryCodePicker.selectRow(_index, inComponent: 0, animated: true)
            } else {
                countryCodePicker.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = countryCodeOptions[0]
            }
        } else {
                textField.inputView = genderPickerView
                let index = genderOptions.index(of: userRegistration.gender)
                if let _index = index {
                self.genderPickerView.selectRow(_index, inComponent: 0, animated: true)
        } else {
                genderPickerView.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = genderOptions[0]
        }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == _relationwithchild_pickerview {
                return relationOptions.count
        }else if(pickerView == countryCodePicker ){
            return countryCodeOptions.count
        }
        else {
                return genderOptions.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == _relationwithchild_pickerview {
                return relationOptions[row]
        }else if(pickerView == countryCodePicker ){
                return countryCodeOptions[row]
        } else {
                return genderOptions[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == _relationwithchild_pickerview {
                activeTextField.text = relationOptions[row]
        }else if(pickerView == countryCodePicker ){
                activeTextField.text = countryCodeOptions[row]
        }else {
                activeTextField.text = genderOptions[row]
                }
    }
    
}

//MARK: -------------- Delegates -------------
extension CreateStudentAccountViewController: ProfileImageTableViewDelegate{
   func profileImageTapped() {
    self.showImageOptions()
    }

}

    //MARK: - TableView Delegate/Datasource

extension CreateStudentAccountViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if checkBoxBtn{
            return sectionHeaderTitle.count
        }
       return 1
      
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if checkBoxBtn == false{
            return labelFields.count
        }else{
            if section == 0{
                return 1
            }else if section == 1{
                return  labelFields2.count
            }else{
                return labelFields3.count+1
            }
        }
       
    }
    
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if checkBoxBtn == false{
        return UIView()
    }else{
        if section == 0{
            return UIView()
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: HeaderCell.cellIdentifire()) as? HeaderCell else {return UIView()}
            cell.backgroundColor = theamAppColor
            cell.configureCell(with: sectionHeaderTitle[section])
            return cell.contentView
        }
    }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if checkBoxBtn == false{
            guard indexPath.row != 0 else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier:ProfileImageTableViewCell.cellIdentifire() , for: indexPath) as? ProfileImageTableViewCell else{return UITableViewCell()}
                cell.profileImageTableViewDelegate = self
                cell.profileImageTableViewDelegateCheckBox = self
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: EdgemTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemTextFieldTableViewCell
            cell.backgroundColor = cell.contentView.backgroundColor
            cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
            updateDataIntoTextField(cell.customTextField, fromCell: true, 0)
            configureTextField(cell, cell.customTextField,0)
            cell.selectionStyle = .none
            return cell
        }else{
            if indexPath.section == 0{
                guard indexPath.row != 0 else {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier:ProfileImageTableViewCell.cellIdentifire() , for: indexPath) as? ProfileImageTableViewCell else{return UITableViewCell()}
                    cell.profileImageTableViewDelegate = self
                    cell.profileImageTableViewDelegateCheckBox = self
                    return cell
                }
                return UITableViewCell()
            }else if indexPath.section == 1{
                guard indexPath.row != 0 else{
                    return UITableViewCell()
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: EdgemTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemTextFieldTableViewCell
                cell.backgroundColor = cell.contentView.backgroundColor
                cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields2[indexPath.row])
                updateDataIntoTextField(cell.customTextField, fromCell: true, 1)
                configureTextField(cell, cell.customTextField,1)
                cell.selectionStyle = .none
                return cell
            }
            else{
                guard indexPath.row != 0 else {
                    return UITableViewCell()
                }
                guard indexPath.row != 6 else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: EdgemDualTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemDualTextFieldTableViewCell
                    cell.configureCellWithTitles(title: ["+65",labelFields3[indexPath.row]], index: indexPath.row)
                    configureTextField(nil, cell.textFieldOne, 2)
                    configureTextField(nil, cell.textFieldTwo, 2)
                    updateDataIntoTextField(cell.textFieldOne, fromCell: true, 2)
                    updateDataIntoTextField(cell.textFieldTwo, fromCell: true, 2)

                    cell.selectionStyle = .none
                    return cell
                }
                guard indexPath.row != 7 else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: checkBoxCreateStudentTableViewCell.cellIdentifier(), for: indexPath) as! checkBoxCreateStudentTableViewCell
                    cell.selectionStyle = .none
                    cell.createStudentAccountViewController = self
                    return cell
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: EdgemTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemTextFieldTableViewCell
                cell.backgroundColor = cell.contentView.backgroundColor
                cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields3[indexPath.row])
                updateDataIntoTextField(cell.customTextField, fromCell: true, 2)
                configureTextField(cell, cell.customTextField,2)
                cell.selectionStyle = .none
                return cell
            }
        }

        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if checkBoxBtn == false {
            guard indexPath.row != 0 else {
                return ProfileImageTableViewCell.cellHeight()
            }
             return UITableView.automaticDimension
        }else{
            if checkBoxBtn && indexPath.section == 0{
                if indexPath.row == 0{
                    return ProfileImageTableViewCell.cellHeight()
                }
                return UITableView.automaticDimension
            }else if checkBoxBtn && indexPath.section == 2{
                if indexPath.row == 0{
                    return 0
                }
                if indexPath.row == 7{
                    return checkBoxCreateStudentTableViewCell.cellHeight()
                }
                 return UITableView.automaticDimension
            }else{
                if indexPath.row == 0{
                    return 0
                }
                 return UITableView.automaticDimension
            }
    }
    
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if checkBoxBtn == false {
            guard indexPath.row != 0 else {
                return ProfileImageTableViewCell.cellHeight()
            }
            return 92
        }else{
            if checkBoxBtn && indexPath.section == 0{
                if indexPath.row == 0{
                    return ProfileImageTableViewCell.cellHeight()
                }
                return 92
            }else if checkBoxBtn && indexPath.section == 2{
                if indexPath.row == 0{
                    return 0
                }
                if indexPath.row == 7{
                    return checkBoxCreateStudentTableViewCell.cellHeight()
                }
                return 92
            }else{
                if indexPath.row == 0{
                    return 0
                }
                return UITableView.automaticDimension
            }
        }
    }
    
}

    //MARK: - TextField Delegate
extension CreateStudentAccountViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! EdgemCustomTextField
        if activeTextField.tag == CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.relationshipwithchild.rawValue ||
            activeTextField.tag == CreateAccountTutorTextFieldType.gender.rawValue ||
            activeTextField.text?.contains("+") == true {
            managePickerView()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if checkBoxBtn == false {
            updateDataIntoTextField(textField as! EdgemCustomTextField, fromCell: false, 0)
            let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
            guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else{return}
            
            switch textField.tag {
            case CreateAccountTutorTextFieldType.firstName.rawValue:
                cell.validateFirstName(text: textField.text ?? "")
                errorDictForChild["FirstName"] = cell.errorLabel.text ?? ""

            case CreateAccountTutorTextFieldType.lastName.rawValue:
                cell.validateLastName(text: textField.text ?? "")
                errorDictForChild["LastName"] = cell.errorLabel.text ?? ""

            case CreateAccountTutorTextFieldType.gender.rawValue:
                cell.validateGenderName(text: textField.text ?? "")
                errorDictForChild["Gender"] = cell.errorLabel.text ?? ""
                setProfileImage(textField.text ?? "")
                
            case CreateAccountTutorTextFieldType.birthday.rawValue:
                cell.validateBirthday(text: textField.text ?? "")
                errorDictForChild["Birthday"] = cell.errorLabel.text ?? ""

            case CreateAccountTutorTextFieldType.emailAddress.rawValue:
                cell.validateEmail(text: textField.text ?? "")
                errorDictForChild["EmailAddress"] = cell.errorLabel.text ?? ""

                if cell.errorLabel.text?.count == 0, !userRegistration.email.isEmpty {
                    verifyEmail(userRegistration.email, 0)
                }
            default:
                break
            }
            
            
            if userRegistration.firstName != "" && userRegistration.lastName != "" && userRegistration.gender != "" && userRegistration.childBirthday != "" && userRegistration.email != ""{
                showHideNextButton(isDataValid(0).isValid)
            }
        
        }else{
            
            let pointInTable: CGPoint = textField.convert(textField.bounds.origin, to: self.tableView)
            let cellIndexPath = self.tableView.indexPathForRow(at: pointInTable)
            let currentSection = cellIndexPath?.section
            print("current section : \(String(describing: cellIndexPath?.section))")
            if currentSection == 0{
                
            }else if currentSection == 1{
                updateDataIntoTextField(textField as! EdgemCustomTextField, fromCell: false, 1)
                let currentIndexPath = IndexPath.init(row: textField.tag, section: 1)
                guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else {return}

                switch textField.tag {
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.parentfirstName.rawValue:
                    cell.validateParentFirstName(text: textField.text ?? "")
                    errorDictForParent["FirstName"] = cell.errorLabel.text ?? ""

                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.parentlastName.rawValue:
                    cell.validateLastName(text: textField.text ?? "")
                    errorDictForParent["LastName"] = cell.errorLabel.text ?? ""

                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.relationshipwithchild.rawValue:
                    cell.validateRelationWithChild(text: textField.text ?? "")
                    errorDictForParent["Relationship"] = cell.errorLabel.text ?? ""

                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectZero.parentemailaddress.rawValue:
                    cell.validateEmail(text: textField.text ?? "")
                    errorDictForParent["EmailAddress"] = cell.errorLabel.text ?? ""

                    if cell.errorLabel.text?.count == 0, !userRegistration.email.isEmpty {
                        verifyEmail(userRegistration.email, 1)
                    }
                default:
                    break
                }
                if userRegistration.firstName != "" && userRegistration.lastName != "" && userRegistration.relationwithchild != "" && userRegistration.email != "" &&   userRegistration.childFirstName != "" && userRegistration.childLastName != "" && userRegistration.childGender != "" && userRegistration.childBirthday != "" && userRegistration.childEmail != ""{
                    showHideNextButton(isDataValid(1).isValid)
                }
            }else{

                updateDataIntoTextField(textField as! EdgemCustomTextField, fromCell: false, 2)
                let currentIndexPath = IndexPath.init(row: textField.tag, section: 2)
                switch textField.tag {
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.firstName.rawValue:
                    guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else{return}
                    cell.validateChildFirstName(text: textField.text ?? "")
                    errorDictForChild["FirstName"] = cell.errorLabel.text ?? ""

                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.lastName.rawValue:
                    guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else{return}
                    cell.validateLastName(text: textField.text ?? "")
                    errorDictForChild["LastName"] = cell.errorLabel.text ?? ""

                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.gender.rawValue:
                    guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else {return}
                    cell.validateGenderName(text: textField.text ?? "")
                    errorDictForChild["Gender"] = cell.errorLabel.text ?? ""
                    setProfileImage(textField.text ?? "")
                    
                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.birthday.rawValue:
                    guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else{return}
                    cell.validateBirthday(text: textField.text ?? "")
                    errorDictForChild["Birthday"] = cell.errorLabel.text ?? ""

                case CreateAccountTutorTextFieldTypeCheckBoxSelectedSectOne.emailAddress.rawValue:
                    guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else {return}
                    cell.validateEmail(text: textField.text ?? "")
                    errorDictForChild["EmailAddress"] = cell.errorLabel.text ?? ""

                    if cell.errorLabel.text?.count == 0, !userRegistration.email.isEmpty {
                        verifyEmail(userRegistration.childEmail, 2)
                    }
                case CreatAccountStudentStepTwoDualTextFieldType.dual.rawValue:
                    guard let dualCell = tableView.cellForRow(at: currentIndexPath) as? EdgemDualTextFieldTableViewCell else{return}
                    let customField = textField as! EdgemCustomTextField
                    if customField.title == "Contact Number*".localized {
                        dualCell.validatePhone(text: customField.text ?? "")
                    } else {
                        dualCell.validateCountryCode(text: customField.text ?? "")
                    }
                default:
                    break
                }
            }
            if userRegistration.firstName != "" && userRegistration.lastName != "" && userRegistration.relationwithchild != "" && userRegistration.email != "" &&   userRegistration.childFirstName != "" && userRegistration.childLastName != "" && userRegistration.childGender != "" && userRegistration.childBirthday != "" && userRegistration.childEmail != ""{
                showHideNextButton(isDataValid(2).isValid)
            }
        }
        tableView.reloadData()

        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if checkBoxBtn == true && textField.tag == CreatAccountStudentStepTwoDualTextFieldType.dual.rawValue{
    
            // get the current text, or use an empty string if that failed
            let currentText = textField.text ?? ""
            
            // attempt to read the range they are trying to change, or exit if we can't
            guard let stringRange = Range(range, in: currentText) else { return false }
            
            // add their new text to the existing text
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            // make sure the result is under 16 characters
            return updatedText.count <= 10
        }
        return true
    }
}

//MARK: - Image Picker Delegate

extension CreateStudentAccountViewController{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        let indexPath = IndexPath(row: 0, section: 0)
        guard let cell = tableView.cellForRow(at: indexPath) as? ProfileImageTableViewCell else{return}
        cell.profileImage.layer.cornerRadius = (cell.profileImage.frame.width/2)
        cell.profileImage.layer.borderWidth = 2.0
        cell.profileImage.layer.borderColor = theamBlackColor.cgColor
        cell.profileImage.image = image
        tableView.reloadData()
        dismiss(animated:true, completion: nil)
    }
}

//MARK: - API Implementation

extension CreateStudentAccountViewController{
    
    func verifyEmail(_ email: String, _ section: Int){

        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let emailObserver = ApiManager.shared.apiService.verifyEmail(email)
        let emailDisposable = emailObserver.subscribe(onNext: {(isAvailable) in
            DispatchQueue.main.async {
                 Utilities.hideHUD(forView: self.view)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
    
                        if section == 0 {
                            
                            guard  let cell = self.tableView.cellForRow(at: IndexPath.init(row: CreateAccountTutorTextFieldType.emailAddress.rawValue, section: section) ) as? EdgemTextFieldTableViewCell else{return}
                            cell.errorLabel.text = error.localizedDescription
                            self.showHideNextButton(false)
                            cell.customTextField.hideImage()
                            self.tableView.beginUpdates()
                            self.tableView.endUpdates()
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                            
                        }else if section == 1{
                            let indexPath = IndexPath(row: 4, section: 1)
                            guard  let cell = self.tableView.cellForRow(at: indexPath) as? EdgemTextFieldTableViewCell else{return}
                            //cell.customTextField.becomeFirstResponder()
                            self.showHideNextButton(false)
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                            
                        }else if section == 2{
                            
                            guard  let cell = self.tableView.cellForRow(at: IndexPath.init(row: CreateAccountTutorTextFieldType.emailAddress.rawValue, section: section) ) as? EdgemTextFieldTableViewCell else{return}
                            cell.errorLabel.text = error.localizedDescription
                            self.showHideNextButton(false)
                           // cell.customTextField.becomeFirstResponder()
                            cell.customTextField.hideImage()
                            self.tableView.beginUpdates()
                            self.tableView.endUpdates()
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                            
                        }
                    }
                }
            })
        })
        emailDisposable.disposed(by: disposableBag)
    }
}

