//
//  VerifyPhoneNumberViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 17/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

    import UIKit
    import RxSwift
    import Alamofire
    import  Unbox


protocol VerifyPhoneNumberViewDelegate{
    func fromVerifyPhoneNumberVC(_ backBtnTapped: Bool)
}
    
    class VerifyPhoneNumberViewController: BaseViewController {
        
        var disposableBag = DisposeBag()
        let labelFields = ["","Contact Number*".localized, "Generate OTP".localized]
        var _filledTextField: Bool = false
        var countryCodeOptions = ["+65"]
        var userRegistration: UserRegistration!
        var activeTextField: UITextField!
        let countryCodePicker = UIPickerView()
        var _fromLogInViewController: Bool = false
        var _backButtonTappedDelegate: VerifyPhoneNumberViewDelegate?
        
        var isFrom: UIViewController?
        
        
        
        @IBOutlet weak var loginBtn: UIButton!
        @IBOutlet weak var backBtn: UIButton!
        @IBOutlet weak var viewtopConstraint: NSLayoutConstraint!
        @IBOutlet weak var tableView: UITableView!
        @IBOutlet weak var titleLabel: UILabel!{
            didSet{
                titleLabel.text = "Verify your number"
            }
        }
        
        override func viewDidLoad() {
            
            if let vc = isFrom, vc is AddOrSaveLocationViewController{
                backBtn.isHidden = true
                loginBtn.isHidden = false
            }else{
                 backBtn.isHidden = false
                 loginBtn.isHidden = true
            }
            
            super.viewDidLoad()
            initialSetUpForCountryCodePicker()
             viewtopConstraint.constant = UIDevice.current.screenType.rawValue.contains("X") ? 35 : 0
            userRegistration.countryContactCode = "+65"
        }
        
        deinit {
            NotificationCenter.default.removeObserver(self)
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)

        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            //print("USER DATA: \(userRegistration)")
        }
        
        //MARK:- IBActions
        
        @IBAction func generateOTPButtonTapped(_ sender: EdgemCustomButton) {
            self.view.endEditing(true)
            let validationInformation = isDataValid()
            guard validationInformation.isValid else {
                self.showSimpleAlertWithMessage(validationInformation.message)
                return
            }
            if _fromLogInViewController{
                sentOTPToMobileNumber(userRegistration.contactNumber, "", "")
            }else{
                if UserStore.shared.selectedUserType == "T"{
                     //createAccount()
                    if userRegistration != nil{
                        if !userRegistration.imageURL.isEmpty{
                            if let image = userRegistration.tutorImage{
                                if let imageData = image.jpeg(.secondLowest){
                                    uploadProfilePicture(imageData: imageData)
                                }
                               
                            }else{
                                
                            }

                        }
                    }

                }else{
                     createAccount()
                }
                
            }
        }
        
        
        
        @IBAction func DidTaplogInBtn(_ sender: UIButton) {
            
            let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                self.navigationController?.popToRootViewController(animated: true)
            }
            
            let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
            alert.addAction(yesAction)
            alert.addAction(noAction)
            self.present(alert, animated: true, completion: nil)
            
        }
        
        @IBAction func backButtonTapped(_ sender: UIButton) {
            self.view.endEditing(true)
            _backButtonTappedDelegate?.fromVerifyPhoneNumberVC(true)
            if userRegistration.contactNumber != ""{
                let alert = UIAlertController(title: AppName, message: AlertMessage.removeFieldData.description(), preferredStyle: .alert)
                let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                    self.navigateBack(sender: sender)
                }
                
                let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
                alert.addAction(yesAction)
                alert.addAction(noAction)
                self.present(alert, animated: true, completion: nil)
            } else {
                navigateBack(sender: sender)
            }
        }
        
        //MARK:- Helping Function
    
        func initialSetUpForCountryCodePicker(){
            countryCodePicker.delegate = self
            countryCodePicker.dataSource = self
            countryCodePicker.tag = 1
        }
        
        func configureTextField(_ customTextField: EdgemCustomTextField) {
            customTextField.delegate = self
            customTextField.inputView = nil
            customTextField.autocorrectionType = UITextAutocorrectionType.no
            switch customTextField.tag {
           
            case VerifyPhoneNumberTextFieldType.dual.rawValue:
                if customTextField.title == "Contact Number*".localized {
                    customTextField.keyboardType = .phonePad
                } else{
                    customTextField.inputView = countryCodePicker
                    customTextField.icon = "drop-down-arrow"
                    
                    customTextField.showImage()
                    customTextField.keyboardType = .phonePad
                    customTextField.tintColor = UIColor.clear
                }
                
            default:
                customTextField.keyboardType = .asciiCapable
            }
            
        }
        
        func showHideNextButton(_ status: Bool) {
            
            let indexPath = IndexPath(row: 2, section: 0)
            guard let cell = tableView.cellForRow(at: indexPath) else{return}
            guard let generaeOTPBtn = cell.viewWithTag(indexPath.row) as? UIButton else{return}
            if status == false{
                generaeOTPBtn.isUserInteractionEnabled = false
                generaeOTPBtn.backgroundColor = UIColor(white: 1, alpha: 0.60)
            }else{
                generaeOTPBtn.isUserInteractionEnabled = true
                generaeOTPBtn.backgroundColor = UIColor.white
            }
            
        }
        
        
        
        func updateDataIntoTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
            switch customTextField.tag {
            case VerifyPhoneNumberTextFieldType.dual.rawValue:
                if customTextField.title == "Contact Number*".localized {
                    fromCell ? (customTextField.text = userRegistration.contactNumber) : (userRegistration.contactNumber = customTextField.text!)
                } else {
                     fromCell ? (customTextField.text = userRegistration.countryContactCode) : (userRegistration.countryContactCode = customTextField.text!)
                }

                break
            default:
                break
            }
            
            if userRegistration.contactNumber == ""  {
                showHideNextButton(false)
                if fromCell == false{
                    tableView.beginUpdates()
                    tableView.endUpdates()
                }
            }
            
        }
    
        func isDataValid() -> (isValid: Bool, message: String) {
            var errorMessages: [String] = []
            for index in 1...labelFields.count {
                let currentIndexPath = IndexPath.init(row: index, section: 0)
                switch index {
                case VerifyPhoneNumberTextFieldType.dual.rawValue:
                    let dualCell = tableView.cellForRow(at: currentIndexPath) as! EdgemDualTextFieldTableViewCell
                    dualCell.validateCountryCode(text: dualCell.textFieldOne.text ?? "+65")
                    dualCell.validatePhone(text: userRegistration.contactNumber)
                    errorMessages.append(dualCell.errorMessageLabelOne.text ?? "")
                    errorMessages.append(dualCell.errorMessageLabelTwo.text ?? "")
               
                default:
                    break
                }
            }
            let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
                message.count > 0
            }
            tableView.reloadData()
            let isValidData = filteredErrorMessages.count == 0
            return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
        }
        
    }
    
    extension VerifyPhoneNumberViewController: UITableViewDelegate, UITableViewDataSource{
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return labelFields.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard indexPath.row != 0 else {
                return UITableViewCell()
            }
            guard indexPath.row != 2 else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath)
                let generaeOTPBtn = cell.viewWithTag(indexPath.row) as? UIButton
                generaeOTPBtn?.setTitle(labelFields[indexPath.row], for: .normal)
                if userRegistration.contactNumber == ""{
                    generaeOTPBtn?.isUserInteractionEnabled = false
                    generaeOTPBtn?.backgroundColor = UIColor(white: 1, alpha: 0.60)
                }else{
                    generaeOTPBtn?.isUserInteractionEnabled = true
                    generaeOTPBtn?.backgroundColor = UIColor.white
                }
                cell.selectionStyle = .none
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: EdgemDualTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemDualTextFieldTableViewCell
            cell.configureCellWithTitles(title: ["+65",labelFields[indexPath.row]], index: indexPath.row)
                configureTextField(cell.textFieldOne)
                updateDataIntoTextField(cell.textFieldOne, fromCell: true)
                configureTextField(cell.textFieldTwo)
                 updateDataIntoTextField(cell.textFieldTwo, fromCell: true)
         
            cell.selectionStyle = .none
            return cell
            
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            guard indexPath.row != 0 else {
                return 0
            }
            return UITableView.automaticDimension
        }
    }
    
    extension VerifyPhoneNumberViewController: UITextFieldDelegate {
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
            activeTextField = textField as! EdgemCustomTextField
            if  activeTextField.text?.contains("+") == true{
                managePickerView()
            }
        }
     
        func textFieldDidEndEditing(_ textField: UITextField) {
            updateDataIntoTextField(textField as! EdgemCustomTextField, fromCell: false)
            
            let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
            switch textField.tag {
            case VerifyPhoneNumberTextFieldType.dual.rawValue:
                let dualCell = tableView.cellForRow(at: currentIndexPath) as! EdgemDualTextFieldTableViewCell
                let customField = textField as! EdgemCustomTextField
                if customField.title == "Contact Number*".localized {
                    dualCell.validatePhone(text: customField.text ?? "")
                } else {
                   dualCell.validateCountryCode(text: customField.text ?? "")
                }
            default:
                break
            }
            if userRegistration.contactNumber != ""  {
                showHideNextButton(isDataValid().isValid)
            }
            tableView.beginUpdates()
            tableView.endUpdates()
            
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            
            switch textField.tag {
            case VerifyPhoneNumberTextFieldType.dual.rawValue:
                
                // get the current text, or use an empty string if that failed
                let currentText = textField.text ?? ""
                
                // attempt to read the range they are trying to change, or exit if we can't
                guard let stringRange = Range(range, in: currentText) else { return false }
                
                // add their new text to the existing text
                let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
                
                // make sure the result is under 16 characters
                return updatedText.count <= 10
                
            default:
                return true
            }
        
                return true
        }
}

// MARK: UIPickerView Delegate/Datasource

extension VerifyPhoneNumberViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        let textField = activeTextField as! EdgemCustomTextField
        countryCodePicker.reloadAllComponents()
        countryCodePicker.reloadInputViews()
        if textField.tag == 1 {
            textField.inputView = countryCodePicker
            let index = countryCodeOptions.index(of: userRegistration.countryContactCode)
            if let _index = index {
                self.countryCodePicker.selectRow(_index, inComponent: 0, animated: true)
                activeTextField.text = countryCodeOptions[_index]
            } else {
                countryCodePicker.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = countryCodeOptions[0]
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryCodeOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return countryCodeOptions[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

            activeTextField.text = countryCodeOptions[row]
    }
    
    
}

extension VerifyPhoneNumberViewController {
    
    func sentOTPToMobileNumber(_ mobile: String,_ userID: String, _ otp: String)  {
            if !isConnectedToNetwork(){
                self.showNoInternetMessage()
                return
            }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let forgotObserver = ApiManager.shared.apiService.sentOTPToMobileNumber(mobile, userID, otp)
        let forgotDisposable = forgotObserver.subscribe(onNext: {(userIDEmail) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                let forgotPasswordOTPViewController = OTPViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
               // forgotPasswordOTPViewController.phoneNumber = self.mobileNumberTextField.text!
                forgotPasswordOTPViewController.userID = userIDEmail.userId
                forgotPasswordOTPViewController.email = userIDEmail.email
                forgotPasswordOTPViewController._userRegistration = self.userRegistration
                forgotPasswordOTPViewController.contactNumber = self.userRegistration.contactNumber
                forgotPasswordOTPViewController.countryCode = self.userRegistration.countryContactCode
                forgotPasswordOTPViewController._forgotPassword = true
                forgotPasswordOTPViewController._userRegistration = self.userRegistration
                self.navigationController?.pushViewController(forgotPasswordOTPViewController, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        forgotDisposable.disposed(by: disposableBag)
    }
    
    
    func uploadProfilePicture(imageData: Data) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        var dob = ""
        if UserStore.shared.isParrent{
            dob =  userRegistration.childBirthday
        }else{
            dob = userRegistration.isStudent ? userRegistration.childBirthday : userRegistration.birthday
        }
        
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = "dd-MMM-yyyy"
        guard let date = inputDateFormatter.date(from: dob) else{return}
        let formattedDateString = Utilities.getFormatedDate(date, dateFormat: "yyyy-MM-dd")
        
        var countryCode = ""
//        if userRegistration.countryContactCode.first == "+"{
//            countryCode = String(userRegistration.countryContactCode.dropFirst())
//        }else{
            countryCode = userRegistration.countryContactCode.replacingOccurrences(of: "+", with: "")
//        }
       
        var signUPDict : [String : AnyObject] = [String : AnyObject]()
        
        signUPDict = [
            "user_id"                  : userRegistration.userID,
            "username"             : userRegistration.userName,
            "password"              : userRegistration.password,
            "first_name"            : userRegistration.firstName,
            "last_name"            : userRegistration.lastName,
            "gender"                 : userRegistration.gender,
            "dob"                       : formattedDateString,
            "email"                     : userRegistration.email,
            "address_line1"       : userRegistration.addressLine1,
            "address_line2"       : userRegistration.addressLine2,
            "postal_code"          : userRegistration.postalCode,
            "nearest_station"     : userRegistration.MRT_LRTStation,
            "lattitude"                 : userRegistration.lattitude,
            "longitude"               : userRegistration.longitude,
            "user_type"               : userRegistration.isStudent ? "Student" : "Tutor",
            "country_code"         : "65",
            "contact_number"    : userRegistration.contactNumber,
            "address_label"        : userRegistration.address_label,
            "address_location"    : userRegistration.address_location,
            "fb_id"                        : userRegistration.fbID,
            "gl_id"                        : userRegistration.gmailID,
            "referral_code"           : userRegistration.referralCode
            ] as [String : AnyObject]
        
       Utilities.showHUD(forView: self.view, excludeViews: [])
        let url = "\(baseUrl)\(PathURl.signup.rawValue)"
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Accept": "application/json",
            "Authorization": "Bearer \(UserStore.shared.token)"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
                        for (key, value) in signUPDict {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                        }
            let fileName = "profile_picture.png"
            let mimeType = imageData.mimeType
            multipartFormData.append(imageData, withName: "profile_image", fileName: fileName, mimeType: mimeType)
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    //                    print("Succesfully uploaded")
                    DispatchQueue.main.async(execute: {
                        Utilities.hideHUD(forView: self.view)
                    })
                    if let _ = response.error{
                        self.showSimpleAlertWithMessage("There seems to be an error. Please try again later.".localized)
                        return
                    }
                    let json = try? JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                    print(json as Any)
                    if let responseDict = json as? [String: AnyObject], let message = responseDict["message"] as? String {
                        
                        if let data = responseDict["data"] as? [String: AnyObject]{
                            AppDelegateConstant.user?.imageURL = data["real_image"] as? String
                            
                           var userID = 0
                            var otp = ""
                           userID = (data["user_id"] as? Int)!
                            otp = (data["otp"] as? String)!

                            self.userRegistration.userID = "\(userID)"
                            DispatchQueue.main.async {
                                let OTPVC = OTPViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                                // signupConfirmMobileViewController.mobileNumber = self.userRegistration.phone
                                self.userRegistration.userID = "\(userID)"
                                OTPVC._userRegistration = self.userRegistration
                                OTPVC.userID = userID
                                OTPVC.countryCode = self.userRegistration.countryContactCode
                                OTPVC.contactNumber = self.userRegistration.contactNumber
                                self.navigationController?.pushViewController(OTPVC, animated: true)
                            }
                        }
                      //  self.showSimpleAlertWithMessage(message)
                        
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    Utilities.hideHUD(forView: self.view)
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            }
        }
    }
    
    func createAccount(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
       
        var dob = ""
        if UserStore.shared.isParrent{
            dob =  userRegistration.childBirthday
        }else{
            dob = userRegistration.isStudent ? userRegistration.childBirthday : userRegistration.birthday
        }
        let inputDateFormatter = DateFormatter()
        inputDateFormatter.dateFormat = "dd-MMM-yyyy"
        guard let date = inputDateFormatter.date(from: dob) else{return}
        let formattedDateString = Utilities.getFormatedDate(date, dateFormat: "yyyy-MM-dd")
        var signUPDict : [String : AnyObject] = [String : AnyObject]()
        
        var usr_id = ""
        if let user_ID = AppVariables.user_ID{
            usr_id = user_ID
        }else{
            usr_id = userRegistration.userID
        }

        signUPDict = [
            "user_id"                  : usr_id,
            "username"             : userRegistration.userName,
            "password"              : userRegistration.password,
            "first_name"            : userRegistration.firstName,
            "last_name"            : userRegistration.lastName,
            "gender"                 : userRegistration.gender,
            "dob"                       : formattedDateString,
            "email"                     : userRegistration.email,
            "address_line1"       : userRegistration.addressLine1,
            "address_line2"       : userRegistration.addressLine2,
            "postal_code"          : userRegistration.postalCode,
            "nearest_station"     : userRegistration.MRT_LRTStation,
            "lattitude"                 : userRegistration.lattitude,
            "longitude"               : userRegistration.longitude,
            "user_type"               : userRegistration.isStudent ? "Student" : "Tutor",
            "country_code"         : "65", //(userRegistration.countryContactCode).replacingOccurrences(of: "+", with: "")
            "contact_number"    : userRegistration.contactNumber,
            "address_label"        : userRegistration.address_label,
            "address_location"    : userRegistration.address_location,
            "fb_id"                        : userRegistration.fbID,
            "gl_id"                        : userRegistration.gmailID,
             "referral_code"           : userRegistration.referralCode
            ] as [String : AnyObject]
        
        if UserStore.shared.isParrent{
            signUPDict = [
                          "user_id"                                             :                usr_id,
                          "fb_id"                                                 :               userRegistration.fbID,
                          "gl_id"                                                  :              userRegistration.gmailID,
                          "username"                                         :               userRegistration.userName,
                          "password"                                         :               userRegistration.password,
                          "user_type"                                         :               userRegistration.isStudent ? "Student" : "Tutor",
                          "is_parent"                                          :                userRegistration.isParent,
                          
                          // child detail
                          "first_name"                                        :                userRegistration.childFirstName,
                          "last_name"                                         :                userRegistration.childLastName,
                          "gender"                                              :                userRegistration.childGender,
                          "dob"                                                   :                 formattedDateString,
                          "email"                                                 :                 userRegistration.childEmail,
                          "country_code"                                   :                 "65", //(userRegistration.childContactCode).replacingOccurrences(of: "+", with: "")
                          "contact_number"                              :                 userRegistration.childContactNumber,
                          
                          // parent detail
                          "first_name_parent"                           :                   userRegistration.firstName,
                          "last_name_parent"                            :                   userRegistration.lastName,
                          "relation"                                             :                   userRegistration.relationwithchild,
                          "email_parent"                                    :                   userRegistration.email,
                          "country_code_parent"                       :                   "65", //(userRegistration.countryContactCode).replacingOccurrences(of: "+", with: "")
                          "contact_number_parent"                  :                    userRegistration.contactNumber,
                         
                         // address detail
                          "address_line1"                                   :                   userRegistration.addressLine1,
                          "address_line2"                                   :                   userRegistration.addressLine2,
                          "postal_code"                                     :                   userRegistration.postalCode,
                          "lattitude"                                           :                    userRegistration.lattitude,
                          "longitude"                                         :                    userRegistration.longitude,
                          "nearest_station"                                :                   userRegistration.MRT_LRTStation,
                          "notify_child"                                       :                   userRegistration.childNotificationAllow,
                
                         "referral_code"                                     :                      userRegistration.referralCode
                
                ] as [String : AnyObject]
        }else{
            signUPDict = [
                "user_id"                  : usr_id,
                "username"             : userRegistration.userName,
                "password"              : userRegistration.password,
                "first_name"            : userRegistration.firstName,
                "last_name"            : userRegistration.lastName,
                "gender"                 : userRegistration.gender,
                "dob"                       : formattedDateString,
                "email"                     : userRegistration.email,
                "address_line1"       : userRegistration.addressLine1,
                "address_line2"       : userRegistration.addressLine2,
                "postal_code"          : userRegistration.postalCode,
                "nearest_station"     : userRegistration.MRT_LRTStation,
                "lattitude"                 : userRegistration.lattitude,
                "longitude"               : userRegistration.longitude,
                "user_type"               : userRegistration.isStudent ? "Student" : "Tutor",
                "country_code"         : "65", //(userRegistration.countryContactCode).replacingOccurrences(of: "+", with: "")
                "contact_number"    : userRegistration.contactNumber,
                "address_label"        : userRegistration.address_label,
                "address_location"    : userRegistration.address_location,
                "fb_id"                        : userRegistration.fbID,
                "gl_id"                        : userRegistration.gmailID,
                 "referral_code"           : userRegistration.referralCode
                ] as [String : AnyObject]
        }
        
        PrintJSONOnConsole.printJSON(dict: signUPDict)
       // return
         Utilities.showHUD(forView: self.view, excludeViews: [])
        
        
        
        let regObserver = ApiManager.shared.apiService.creatAccountWithDetail(signUPDict)
        let regDisposable = regObserver.subscribe(onNext: {(userID) in
            Utilities.hideHUD(forView: self.view)
            self.userRegistration.userID = "\(userID)"
            AppVariables.user_ID = "\(userID)"
            DispatchQueue.main.async {
                let OTPVC = OTPViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
               // signupConfirmMobileViewController.mobileNumber = self.userRegistration.phone
                self.userRegistration.userID = "\(userID)"
                OTPVC.userID = userID
                OTPVC._userRegistration = self.userRegistration
                OTPVC.countryCode = self.userRegistration.countryContactCode
                OTPVC.contactNumber = self.userRegistration.contactNumber
                self.navigationController?.pushViewController(OTPVC, animated: true)
            }
        },onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        }
        )
         regDisposable.disposed(by: disposableBag)
    }
}






