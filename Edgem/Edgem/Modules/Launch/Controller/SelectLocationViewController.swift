//
//  SelectLocationViewController.swift
//  Edgem
//
//  Created by Hipster on 30/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import EasyTipView

class SelectLocationViewController: BaseViewController, EasyTipViewDelegate {
    
    //MARK:- -------------- PROPERTIES --------------
    
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var logInBtn: UIButton!
    @IBOutlet weak var selectLocationBtn: UIButton!
    @IBOutlet weak var noLocationSelectedLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint!
    
    //MARK: ---------------- VARIABLES ----------------
    
    var userRegistration: UserRegistration!
    weak var tipView: EasyTipView?
     var preferences: EasyTipView.Preferences!
    
    //MARK: ---------------- VIEW LIFECYCLE ----------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        preferences = EasyTipView.Preferences()
        preferences.drawing.arrowPosition = .bottom
        preferences.drawing.textAlignment = .center
        preferences.drawing.font = UIFont(name: QuicksandMedium, size: 15)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.positioning.textHInset = 40
        preferences.positioning.textVInset = 10
        preferences.drawing.backgroundColor = UIColor(red: 23/255, green: 23/255, blue: 23/255, alpha: 0.80)
        preferences.animating.dismissTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1.5
        preferences.animating.dismissDuration = 1.5
        
        EasyTipView.globalPreferences = preferences
        
        initialSetUP()
         topViewConstraint.constant = UIDevice.current.screenType.rawValue.contains("X") ? 35 : 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("USER DATA: \(userRegistration)")
        
        if let tipView = self.tipView{
            tipView.dismiss()
            infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let tipView = self.tipView{
            tipView.dismiss()
            infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }
        
    }
    
    
    //MARK: -----------------PRIVATE FUNCTION --------
    func initialSetUP(){
        
        // ------setup select location button
        selectLocationBtn.layer.cornerRadius = 5
        selectLocationBtn.clipsToBounds = true
    }
    
    //MARK: ----------------- @IBActions-----------------
    
    
    @IBAction func didTapInfoBtn(_ sender: UIButton) {
        
        
        let text = "Your location will be kept confidential and is only for the purposes of searching for students near you.".localized
        
        if let tipView = self.tipView{
            
            tipView.dismiss()
            
            infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }else{
            
            let tip = EasyTipView(text: text, preferences: self.preferences, delegate: self)
            
            infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
            
            tip.show(forView: self.infoBtn)
            
            tipView = tip
            
        }
        
    }
    
    // Easy tipview delegate
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
    }
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
       navigateBack(sender: sender)
    }
    
    @IBAction func logInBtnTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func selectLocationBtnTapped(_ sender: UIButton) {
        let confirmLocationVC = ConfirmLocationViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        confirmLocationVC.userRegistration = self.userRegistration
        self.navigationController?.pushViewController(confirmLocationVC, animated: true)
    }
    
    
}
