//
//  WalkThroughPageViewController.swift
//  Edgem
//
//  Created by Namespace on 29/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class WalkThroughPageViewController: UIPageViewController {
    
    // MARK: Properties
    
    var pageControl = UIPageControl()
    var nextButton: UIButton!
    var skipButton: UIButton!
    
    fileprivate lazy var pages: [UIViewController] = {
        return [
            WalkThroughScreenOneViewController.instantiateFromAppStoryboard(appStoryboard: .Main),
            WalkThroughScreenTwoViewController.instantiateFromAppStoryboard(appStoryboard: .Main),
            WalkThroughScreenThreeViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        ]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.dataSource = self
        self.delegate = self
        
        configurePageControl()
        configureNextBtn()
        configureSkipBtn()
       
        let bgView = UIView.init(frame: UIScreen.main.bounds)
        bgView.backgroundColor = UIColor.white
        view.insertSubview(bgView, at: 0)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        if let firstVC = pages.first {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return  .default
    }
    
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 100,width: UIScreen.main.bounds.width,height: 50))
        pageControl.numberOfPages = pages.count
        pageControl.currentPage = 0
        pageControl.tintColor = .gray
        pageControl.pageIndicatorTintColor = UIColor.init(red: 226/255, green: 226/255, blue: 226/255, alpha: 1)
        pageControl.currentPageIndicatorTintColor = UIColor.init(red: 255/255, green: 210/255, blue: 113/255, alpha: 1)
        pageControl.layer.position.y = ScreenHeight - 50
        view.addSubview(pageControl)
    }
    
    func changeValues() {
        
        let pageContentViewController = self.viewControllers![0]
        self.pageControl.currentPage = pages.index(of: pageContentViewController)!
        
        if self.pageControl.currentPage == pages.count - 1 {
            nextButton.setTitle(AlertButton.done.description(), for: .normal)
            nextButton.addTarget(self, action: #selector(doneButtonTapped), for: .touchUpInside)
        } else {
            nextButton.setTitle(AlertButton.next.description(), for: .normal)
            nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        }
        
    }
    
    func navigateToLandingPage(){
        
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        guard let rootViewController = window.rootViewController else {
            return
        }
        
        let landingViewController = LoginViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        landingViewController.view.frame = rootViewController.view.frame
        landingViewController.view.layoutIfNeeded()
        UserStore.shared.isWalkthroughCompleted = true
        let navController = BaseNavigationViewController(rootViewController: landingViewController)
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = navController
        }, completion: nil)
        
    }
    
    @objc func doneButtonTapped() {
        navigateToLandingPage()
    }
    
    @objc func nextButtonTapped() {
        
        let currentViewController = pages[pageControl.currentPage]
        guard let nextViewController = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) else { return }
        setViewControllers([nextViewController], direction: .forward, animated: true) { (completed) in
            DispatchQueue.main.async {
                self.changeValues()
            }
        }
        
    }
    
    @objc func skipButtonTapped() {
        navigateToLandingPage()
    }
    
    func configureSkipBtn(){
        
        skipButton = UIButton(frame: CGRect(x: 20, y: ScreenHeight - 70, width: 100, height: 44))
        skipButton.contentHorizontalAlignment = .center
        skipButton.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
        skipButton.setTitle(AlertButton.skip.description(), for: .normal)
        skipButton.titleLabel?.font = UIFont(name: SourceSansProRegular, size: 15.0)
        skipButton.setTitleColor(theamGrayColor, for: .normal)
        skipButton.titleLabel?.textColor = theamGrayColor
        skipButton.addTarget(self, action: #selector(skipButtonTapped), for: .touchUpInside)
        view.addSubview(skipButton)
        view.bringSubviewToFront(skipButton)
    }
    
    func configureNextBtn(){
        
         nextButton = UIButton(frame: CGRect(x: ScreenWidth - 115, y: ScreenHeight - 70, width: 100, height: 44))
         nextButton.contentHorizontalAlignment = .center
         nextButton.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
         nextButton.setTitle(AlertButton.next.description(), for: .normal)
         nextButton.titleLabel?.font = UIFont(name: SourceSansProRegular, size: 15.0)
         nextButton.setTitleColor(theamGrayColor, for: .normal)
         nextButton.titleLabel?.textColor = theamGrayColor
         nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        view.addSubview(nextButton)
        view.bringSubviewToFront(nextButton)
    }
    
    
}

extension WalkThroughPageViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
         guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        
         let previousIndex = viewControllerIndex - 1
        
         guard previousIndex >= 0          else { return nil }
        
         guard pages.count > previousIndex else { return nil }
        
         return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < pages.count else { return nil }
        
        guard pages.count > nextIndex else { return nil }
        
        return pages[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        changeValues()
    }

}
