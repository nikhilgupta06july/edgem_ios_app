//
//  ConfirmLocationViewController.swift
//  Edgem
//
//  Created by Hipster on 30/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import DropDown
import CoreLocation



class Circle{
    class func drawCircle(_ position: CLLocationCoordinate2D, _ radius: CLLocationDistance)-> GMSCircle{
        return GMSCircle(position: position, radius: radius)
    }
}

class ConfirmLocationViewController: BaseViewController, AddOrSaveLocationViewControllerDelegate,BackButtonTappedDelegate,AddMoreButtonTappedDelegate {

    
    
    //MARK: - Properties
    
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var logInBtn: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var confirmLocationButton: UIButton!
    @IBOutlet weak var selectyourpreferredlocationLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var addressView: UIView!
    
    
    
    
    //MARK: - ------------------------- VARIABLES --------------------------
    
    var _city_addresses:[String] = []{
        didSet{
            if _city_addresses.count > 0{
                configureDropDown()
                self.dropDown.show()
            }
            dropDown.reloadAllComponents()
        }
    }
    
    let addressLabelPicker = UIPickerView()
    var addressLabelOptions = ["Home", "Work"]
    var activeTextField: UITextField!
    
    var userRegistration: UserRegistration!
    var locationAddress: Address! = Address()
    var tempLocationAddress = Address()
    var locationData: LocationData = LocationData(profilImage: "home", title: "Home", address: "", lat: "", long: "", addressId: "")
    var addressDict = [String:AnyObject]()
    var latlongAdr = [String:AnyObject]()
    var _insertValueAtIndex: Int!
    let dropDown = DropDown()
     let marker = GMSMarker()
    var circleCenter: CLLocationCoordinate2D!
    var circle =  GMSCircle()
    var camera = GMSCameraPosition()
    var fromAddOrSaveLocationViewController: Bool = false{
        didSet{
            if fromAddOrSaveLocationViewController{
                confirmLocationButton.setTitle("Save".localized, for: .normal)
                locationLabel.text = "Edit location".localized
                addressView.isHidden = false
            }else{
                confirmLocationButton.setTitle("Confirm location".localized, for: .normal)
                locationLabel.text = "Location".localized
                addressView.isHidden = true
            }
        }
    }
    
    var _fromBackBtnTapped: Bool = false{
        didSet{
            if _fromBackBtnTapped{
                 addressView.isHidden = true
                confirmLocationButton.setTitle("Confirm location".localized, for: .normal)
                locationLabel.text = "Location".localized
            }
        }
    }
    
    var _fromAddMoreBtnTapped: Bool = false{
        didSet{
            if _fromAddMoreBtnTapped{
                confirmLocationButton.setTitle("Confirm location".localized, for: .normal)
                addressView.isHidden = true
                searchTextField.text = ""
                addressTextField.text = "Home"
            }else{
                
            }
        }
    }
    
    var _fromSaveBtnTapped: Bool = false

    var referredVC: UIViewController?
    
    
    //
    //MARK: - -------------------- View LifeCycle ------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationAddress = Address()
        mapView.delegate = self
       // mapView.settings.zoomGestures   = false
        addressView.isHidden = true
       initialCameraPosition()
        configureTextField()
        configureDropDown()
        configureCircle()
       // self.initialSetUpForAddressLabelPicker()
        
        addressLabelPicker.delegate = self
        addressLabelPicker.dataSource = self
        addressLabelPicker.tag = 1
        
        addressTextField.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         //locationAddress = Address()
        
        if let vc = referredVC, vc is SearchViewController || vc is TutorProfileViewController{
            self.logInBtn.isHidden = true
        }else{
            self.logInBtn.isHidden = false
        }
        if fromAddOrSaveLocationViewController == false{
            searchTextField.text = ""
        }
        showHideConfirmButton()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("USER DATA: \(String(describing: userRegistration))")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        showHideConfirmButton()
    }
    
    // MARK: - ----------------------- PRIVATE FUNCTIONS -----------------
    
    
    // ---- Delegate
    
    func sendLocationData(_ profileImage: String, titleLabel: String, address: String, _ fromAddOrSaveLocationViewController: Bool, _ index: Int, _ lattitude: String, _ longitude: String, _ addressObject: Address) {
        locationData = LocationData(profilImage: profileImage, title: titleLabel, address: address, lat: lattitude, long: longitude, addressId: "")
        self.fromAddOrSaveLocationViewController = fromAddOrSaveLocationViewController
        searchTextField.text = address
        addressTextField.text = titleLabel
        _insertValueAtIndex = index
        self.locationAddress = addressObject
        guard let lat = Double(lattitude) else{return}
        guard let long = Double(longitude) else{return}
        //self.locationAddress = self.tempLocationAddress
        self.cameraPosition(lat, long)
    }
    
    func backBtnTapped(_ fromBackBtnTapped: Bool) {
        if fromBackBtnTapped{
            locationAddress = Address()
            _fromBackBtnTapped = true
            searchTextField.text = ""
            addressTextField.text = "Home"
        }
    }
    
    func addMoreBtnTapped(_ fromAddMoreBtnTapped: Bool) {
        _fromAddMoreBtnTapped = true
    }
    
    
    // ---- textfield delegate initialization
    
    fileprivate func configureTextField(){
        searchTextField.delegate = self
        searchTextField.attributedPlaceholder = NSAttributedString(string: "Search Location",
                                                               attributes: [NSAttributedString.Key.foregroundColor: theamGrayColor])
    }
    

    
    // ---- camera position
    
    fileprivate func cameraPosition(_ lat: Double, _ long: Double){
        camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 13)//12.804883
        mapView.camera = camera
         showMarker(position: camera.target)
         circle(position: camera.target)
    }

    
    // ---- initial camera position
    fileprivate func initialCameraPosition(){
        let camera = GMSCameraPosition.camera(withLatitude: 1.3521, longitude: 103.8198, zoom: 11.237988)
        mapView.camera = camera
    }
    
    // ---- show marker on map
    func showMarker(position: CLLocationCoordinate2D){
        marker.position = position
        marker.isDraggable = true
        marker.icon=UIImage(named: "location.pdf")
        marker.map = mapView
    }
    
//    func initialSetUpForaddressLabelPicker(){
//        addressLabelPicker.delegate = self
//        addressLabelPicker.dataSource = self
//        addressLabelPicker.tag = 1
//    }
    
    func configureCircle(){
        circle = GMSCircle(position: camera.target, radius: 1500)
        circle.fillColor = UIColor(red: 252/255, green: 151/255, blue: 34/255, alpha: 0.2)
        circle.strokeColor = .white
        circle.map = mapView
    }
    
    // ---- draw circle
    func circle(position: CLLocationCoordinate2D){
        circle.position = position
    }
    
    
    func showHideConfirmButton(){
        if searchTextField.text == "" {
            if selectyourpreferredlocationLabel != nil{
                selectyourpreferredlocationLabel.isHidden = false
                confirmLocationButton.isHidden = true
            }
        }else{
            selectyourpreferredlocationLabel.isHidden = true
            confirmLocationButton.isHidden = false
            
        }
    }
    
    fileprivate func configureDropDown(){
        let appearance = DropDown.appearance()
        appearance.cellHeight = 72
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        
        dropDown.anchorView = containerView
        dropDown.width = containerView.frame.width
        dropDown.bottomOffset = CGPoint(x: 0, y: containerView.bounds.height+4)
        dropDown.dataSource = _city_addresses
        dropDown.direction = .bottom
        
        
        dropDown.cellNib = UINib(nibName: "DropDownTableViewCell", bundle: nil)
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? DropDownTableViewCell else { return }
            // Setup your custom UI components
            if self._city_addresses.count>0{
                cell.addressLabel.text = self._city_addresses[index]
            }
            
        }
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.locationData.address = item
            self.updateDataInTextField(true)
            self.view.endEditing(true)
//            self.getLatLngForAddress(address: item)//self.searchTextField.text ?? "")
            self.getAddressFromString(item)
        }
        
    }
    
    func updateDataInTextField(_ fromDropDown: Bool){
        
        fromDropDown ? (searchTextField.text = locationData.address) : (locationData.address = searchTextField.text!)
        showHideConfirmButton()
    }
    
    //MARK: ----------------- @IBActions -----------------
    
    
    @IBAction func searchTextFieldUpdated(_ sender: UITextField) {
        
        if sender.tag != 1{
//            if ((sender.text?.isEmpty)!){
//                    showHideConfirmButton()
//            }
            if selectyourpreferredlocationLabel != nil{
                selectyourpreferredlocationLabel.isHidden = false
                confirmLocationButton.isHidden = true
            }
         }
        
     }
    
    
    @IBAction func searchLocation(_ sender: UITextField) {
        if sender.text?.count != 0{
            let searchStr = sender.text?.NoWhiteSpace
            getPlaces(searchString: searchStr!)
        }
    }
    
    
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
   
    @IBAction func confirmLocationButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if let vc = self.referredVC, vc is SearchViewController{
            
             (vc as! SearchViewController).addressFromConfirmLocationVC(self.locationData)
             self.navigationController?.popViewController(animated: true)
            
        }else if let vc = self.referredVC, vc is TutorProfileViewController{
            
            (vc as! TutorProfileViewController).addressFromConfirmLocationVC(self.locationData)
            self.navigationController?.popViewController(animated: true)
            
        }
        else{
            
            let addOrSaveVC = AddOrSaveLocationViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            addOrSaveVC.userRegistration = self.userRegistration
            addOrSaveVC._addOrSaveLocationViewControllerDelegate = self
            addOrSaveVC._backButtonTappedDelegate = self
            addOrSaveVC._addMoreButtonTappedDelegate = self
            addOrSaveVC._saveButtonTappedDelegate = self
            
            if sender.titleLabel?.text == "Save".localized{
                if searchTextField.text != ""{
                    addOrSaveVC.fromSaveorConfirm = true
                    if self.locationData.title != addressTextField.text{
                        self.locationData.title = addressTextField.text!
                        if self.locationData.title == "Work"{
                            self.locationData.profileImage = "Work"
                        }else if self.locationData.title == "Home"{
                            self.locationData.profileImage = "home"
                        }
                    }
                    locationAddress.addr[_insertValueAtIndex] = locationData
                    addOrSaveVC._locationData = self.locationData
                    addOrSaveVC.index = _insertValueAtIndex
                    addOrSaveVC.addressObjects = locationAddress
                }
            }else{
                addOrSaveVC.fromSaveorConfirm = false
                let locationData = self.locationData
                
                locationAddress.addr.append(locationData)
                
                if locationAddress.addr.count > 1{
                    let label = locationAddress.addr[1].title
                    if label == "Home"{
                        locationAddress.addr[1].title = "Work"
                        locationAddress.addr[1].profileImage = "Work"
                    }
                }
                addOrSaveVC.savedAddressObject = locationAddress
                
                addOrSaveVC._locationData = locationData
            }
            self.navigationController?.pushViewController(addOrSaveVC, animated: true)
        }

    }
    
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        
        navigateBack(sender: sender)
       
//        if self.referredVC != nil {
//             navigateBack(sender: sender)
//        }else{
//            
//            let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
//            let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
//                if let pre_vc = self.navigationController?.getSecondPreviousViewController(){
//                    self.navigationController?.popToViewController(pre_vc, animated: true)
//                }
//            }
//            
//            let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
//            alert.addAction(yesAction)
//            alert.addAction(noAction)
//            self.present(alert, animated: true, completion: nil)
//            
//        }
    }
 
}

extension ConfirmLocationViewController: SaveButtonTappedDelegate{
    func saveBtnTapped(_ fromSaveBtnTapped: Bool) {
        if fromSaveBtnTapped{
            self.locationAddress = Address()
        }
    }
    
    
}

// MARK: - TextField Delegates Methods

extension ConfirmLocationViewController: UITextFieldDelegate{
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1{
            activeTextField = textField
            managePickerView()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag != 1{
            showHideConfirmButton()
            updateDataInTextField(false)
        }

    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField.tag != 1{
////            if (textField.text?.isEmpty)! || string == " " {
////                showHideConfirmButton()
////                return false
////            }
//            let currentText = textField.text ?? ""
//            guard let stringRange = Range(range, in: currentText) else { return false }
//
//            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
//
//            if  updatedText.count <= 1{
//                showHideConfirmButton()
//                //return false
//            }else{
//                //return true
//            }
//        }
//        return true
//    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if textField.tag != 1{
            if (textField.text?.count)! > 0{
                textField.text = ""
                textField.resignFirstResponder()
                return true
            }
        }
        return false
    }
}



extension ConfirmLocationViewController: GMSMapViewDelegate{
    
    //MARK - GMSMarker Dragging
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {

    }
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        let postion = CLLocationCoordinate2D(latitude: marker.position.latitude, longitude: marker.position.longitude)
        self.circle(position: postion)
    }
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        let lat = marker.position.latitude
        let lng = marker.position.longitude
        
        getAddressForLatLong(lat, lng)
        showHideConfirmButton()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let zoom = mapView.camera.zoom              // -------- > current zoom level
        print("map zoom is ",String(zoom))
    }
    
    
    // ---- handles Info Window tap
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
       // print("didTapInfoWindowOf")
    }
    
    /* handles Info Window long press */
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
       // print("didLongPressInfoWindowOf")
    }
    
    /* set a custom Info Window */
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let view = UIView(frame: CGRect.init(x: 0, y: 0, width: 240, height: 35))
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 4
        
        let lbl2 = UILabel(frame: CGRect.init(x: 8, y: 8, width: view.frame.size.width - 16, height: 15))
        lbl2.text = "Move pin to change location"
        lbl2.font = textFieldDefaultFont
        view.addSubview(lbl2)
        
        return view
    }
}

extension ConfirmLocationViewController: UISearchDisplayDelegate {
    
    private func getPlaces(searchString: String) {
        var request: NSMutableURLRequest? = nil
        let baseUrl = BaseURLForGooglePrediction
        let apikey = GoogleApiKey
        let latlng = "1.3521,103.8198"
        let component = "country:SG"
        //http://maps.googleapis.com/maps/api/geocode/json?address=Singapore%20505468&sensor=false&components=country:SG
        //\(baseUrl)location=\(latlng)&key=\(apikey)&components=\(component)&input=\(searchString))&type=establishment&radius=10000
        if let aText = URL(string: "\(baseUrl)key=\(apikey)&components=\(component)&input=\(searchString))") {
            request = NSMutableURLRequest(url: aText)
        }
   
        print("URL : \(String(describing: request)) \n")
        if request == nil{
            return
        }
        request?.httpMethod = "POST"
        
        var _: Error?
        var response: URLResponse?
        var responseData: Data? = nil
        _city_addresses.removeAll()
        if let aRequest = request {
            responseData = try? NSURLConnection.sendSynchronousRequest(aRequest as URLRequest, returning: &response)
        }
        var resSrt: String? = nil
        if let aData = responseData {
            resSrt = String(data: aData, encoding: .ascii)
        }
        var dict: [String : AnyObject]? = nil
        if let aData = responseData {
            dict = try! JSONSerialization.jsonObject(with: aData, options: .mutableContainers) as? [String : AnyObject]
        }
        if let dict = dict{
            if  let status = (dict )["status"] as? String {
                if status == "OK"{
                    if let addresses = (dict )["predictions"] as? NSArray{
                        if addresses.count > 0{
                            for index in 0..<addresses.count{
//                                if let data = addresses[index] as? [String:AnyObject], let structured_formatting = data["structured_formatting"] as? [String:AnyObject], let address = structured_formatting["main_text"] as? String{
//                                    _city_addresses.append(address)
//                                }
                                if let data = addresses[index] as? [String:AnyObject], let description = data["description"] as? String{
                                    _city_addresses.append(description)
                                }

                                
                            }
                        }
                        print("CITIES : \(_city_addresses) \n")
                    }
                }else if status == "ZERO_RESULTS"{
                    self.dropDown.hide()
                }
            }
        }
    }
    
    func getLatLngForAddress(address: String) {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            guard let placemarks = placemarks else {return}
            let location = placemarks.first?.location
            let lat = (location?.coordinate.latitude) ?? 1.3521
            let lng = (location?.coordinate.longitude) ?? 103.8198
            self.latlongAdr = ["address":address, "lattitude":"\(lat)", "longitude":"\(lng)"] as [String:AnyObject]
            self.userRegistration.address_location.append(address)
            self.userRegistration.address_lattitude.append("\(lat)")
            self.userRegistration.address_longitude.append("\(lng)")

            self.userRegistration.address_label.append("Home")
            self.locationData = LocationData(profilImage: "home", title: self.addressTextField.text!, address: address, lat: "\(lat)", long: "\(lng)", addressId: "")
            self.cameraPosition(lat, lng)

        }
        
    }
    func getAddressFromString(_ address: String) {
        var request: NSMutableURLRequest? = nil
        let apikey = GoogleApiKey
        
        
        if let aText = URL(string: "\(BaseUrlForGoogleMap)address=\(address.replacingOccurrences(of: " ", with: "+"))&type=establishment&key=\(apikey)") {
            request = NSMutableURLRequest(url: aText)
        }
        
        request?.httpMethod = "POST"
        
        var _: Error?
        var response: URLResponse?
        var responseData: Data? = nil
        //Utilities.showHUD(forView: self.view, excludeViews: [])
        if let aRequest = request {
            responseData = try? NSURLConnection.sendSynchronousRequest(aRequest as URLRequest, returning: &response)
        }
        var resSrt: String? = nil
        if let aData = responseData {
            resSrt = String(data: aData, encoding: .ascii)
        }
        
        print("got response==\(resSrt ?? "")")
        
        var dict: [String : AnyObject]? = nil
        if let aData = responseData {
            dict = try! JSONSerialization.jsonObject(with: aData, options: .mutableContainers) as? [String : AnyObject]
        }
        guard let addrDict = dict, let result = addrDict["results"] as? NSArray  else {return}
        if result.count>0{
            print(result)
            guard let addrStr = result[0] as? [String:AnyObject], let geometry = addrStr["geometry"] as? [String:AnyObject], let location = geometry["location"] as? [String:AnyObject] else{return}
            
//            let location = placemarks.first?.location
            let lat = location["lat"] ?? 1.3521 as AnyObject//(location?.coordinate.latitude) ?? 1.3521
            let lng = location["lng"] ?? 1.3521 as AnyObject//(location?.coordinate.longitude) ?? 103.8198
            self.latlongAdr = ["address":address, "lattitude":"\(lat)", "longitude":"\(lng)"] as [String:AnyObject]
            
            if let vc = referredVC, vc is SearchViewController || vc is TutorProfileViewController{
                
            }else{
                
                self.userRegistration.address_location.append(address)
                self.userRegistration.address_lattitude.append("\(lat)")
                self.userRegistration.address_longitude.append("\(lng)")
                
                self.userRegistration.address_label.append("Home")
               // self.locationData = LocationData(profilImage: "home", title: self.addressTextField.text!, address: address, lat: "\(lat)", long: "\(lng)", addressId: "")
                
            }
     
            self.locationData = LocationData(profilImage: "home", title: self.addressTextField.text!, address: address, lat: "\(lat)", long: "\(lng)", addressId: "")
            self.cameraPosition(lat as! Double, lng as! Double)
            
//            address = addr
        }
   }

    func getAddressForLatLong(_ lat: Double, _ long: Double) {
        var request: NSMutableURLRequest? = nil
        var address = ""
        //let baseUrl = BaseUrlForGoogleMap
        let apikey = GoogleApiKey
        if let aText = URL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lat),\(long)&key=\(apikey)") {
            request = NSMutableURLRequest(url: aText)
        }
        
        request?.httpMethod = "POST"
        
        var _: Error?
        var response: URLResponse?
        var responseData: Data? = nil
        //Utilities.showHUD(forView: self.view, excludeViews: [])
        if let aRequest = request {
            responseData = try? NSURLConnection.sendSynchronousRequest(aRequest as URLRequest, returning: &response)
        }
        var resSrt: String? = nil
        if let aData = responseData {
            resSrt = String(data: aData, encoding: .ascii)
        }
        
        print("got response==\(resSrt ?? "")")
        
        var dict: [String : AnyObject]? = nil
        if let aData = responseData {
            dict = try! JSONSerialization.jsonObject(with: aData, options: .mutableContainers) as? [String : AnyObject]
        }
        guard let addrDict = dict, let result = addrDict["results"] as? NSArray  else {return}
        if result.count>0{
            guard let addrStr = result[0] as? [String:AnyObject], let addr = addrStr["formatted_address"] as? String else{return}
            address = addr
        }
        print("addr : \(address)")
        let addrs = address.components(separatedBy: ",")
        self.searchTextField.text = addrs[0]
        
        if let vc = referredVC, vc is SearchViewController || vc is TutorProfileViewController {
            self.locationData = LocationData(profilImage: "home", title: self.addressTextField.text!, address: address, lat: "\(lat)", long: "\(long)", addressId: "")
        }else{
            self.userRegistration.address_location.append(addrs[0])
            self.userRegistration.address_lattitude.append("\(lat)")
            self.userRegistration.address_longitude.append("\(long)")
            self.userRegistration.address_label.append("Home")
            self.locationData = LocationData(profilImage: "home", title: self.addressTextField.text!, address: address, lat: "\(lat)", long: "\(long)", addressId: "")
        }
    }
}


extension ConfirmLocationViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        let textField = activeTextField
        addressLabelPicker.reloadAllComponents()
        addressLabelPicker.reloadInputViews()
        if textField?.tag == 1 {
            
            textField?.inputView = addressLabelPicker
            
            addressLabelPicker.selectRow(0, inComponent: 0, animated: true)
            activeTextField.text = addressLabelOptions[0]
            profileImage.image = addressTextField.text == "Home" ? #imageLiteral(resourceName: "home") : #imageLiteral(resourceName: "Work")
//            let index = addressLabelOptions.index(of: userRegistration.countryContactCode)
//            if let _index = index {
//                self.addressLabelPicker.selectRow(_index, inComponent: 0, animated: true)
//                activeTextField.text = countryCodeOptions[_index]
//            } else {
//                addressLabelPicker.selectRow(0, inComponent: 0, animated: true)
//                activeTextField.text = countryCodeOptions[0]
//            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return addressLabelOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return addressLabelOptions[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        activeTextField.text = addressLabelOptions[row]
        profileImage.image = addressTextField.text == "Home" ? #imageLiteral(resourceName: "home") : #imageLiteral(resourceName: "Work")
    }
    
    
}
