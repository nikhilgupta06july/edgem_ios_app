//
//  SplashViewController.swift
//  Edgem
//
//  Created by Hipster on 28/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class SplashViewController: BaseViewController {
    
    // MARK: --------------- VARIABLES ---------------
    
    var isNetAvailable = true
    var disposableBag = DisposeBag()
    var subjectDisposable: Disposable!
    
    @IBOutlet weak var noInternetMessageLabel: UILabel!
    @IBOutlet weak var retryButton: UIButton!

    
    // MARK: --------------- VIEW LIFECYCLE ----------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppDelegateConstant.shouldShowPopUPForSubjec = true

        // Do any additional setup after loading the view.
        isNetAvailable = isConnectedToNetwork()
        if UserStore.shared.isLoggedIn, isNetAvailable {
            
            fetchUserDetails()
            
        }else{
             navigateAccordinglyToScreen()
        }
       fetchLevels()
       fetchSubjects()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func retryButtonTapped(_ sender: UIButton) {
        isNetAvailable = Utilities.shared.isNetworkReachable()
        if isNetAvailable {
            noInternetMessageLabel.isHidden = true
            retryButton.isHidden = true
            fetchUserDetails()
            Utilities.showHUD(forView: self.view, excludeViews: [])
        }
    }
    
    // MARK: ----------------- PRIVATE FUNCTIONS ------
    
    func notLoggedInFlow(){
        
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        guard let rootViewController = window.rootViewController else {
            return
        }
        
        if UserStore.shared.isWalkthroughCompleted == true {
             self.navigateToLandingScreen()
        }else{
            let walkThroughPageViewController = WalkThroughPageViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            walkThroughPageViewController.view.frame = rootViewController.view.frame
            walkThroughPageViewController.view.layoutIfNeeded()
            let navController = BaseNavigationViewController(rootViewController: walkThroughPageViewController)
            UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                window.rootViewController = navController
            }, completion: nil)
        }
    }
        
    func navigateAccordinglyToScreen(){
        if UserStore.shared.isLoggedIn{
            if self.isNetAvailable {
             self.navigateToDashboardViewController()
            }else {
                self.noInternetMessageLabel.isHidden = false
                self.retryButton.isHidden = false
            }
        }else{
             notLoggedInFlow()//self.navigateToLandingScreen()
        }
    }
    

}

extension SplashViewController{
    
    func fetchUserDetails() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let userObserver = ApiManager.shared.apiService.fetchUserDetails()
        let userDisposable = userObserver.subscribe(onNext: {(user) in
            DispatchQueue.main.async {
               
                    AppDelegateConstant.user = user
                    self.navigateToDashboardViewController()
                
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                self.navigateToLandingScreen()
                //self.logout()
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    
    // logout
    fileprivate func logout(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let deviceToken = UserStore.shared.deviceToken
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let logoutUserObserver = ApiManager.shared.apiService.UserLogout(deviceToken)
        let logoutUserDisposable = logoutUserObserver.subscribe(onNext: { (msg) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                
                Utilities.logoutUser()
                self.navigateToLandingScreen()
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
            }
        })
        return logoutUserDisposable.disposed(by: disposableBag)
        
    }
    
    // Fetch levels
    
    func fetchLevels(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        //Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
            let levelObserver = ApiManager.shared.apiService.fetchLevels(1)
            let levelDisposable = levelObserver.subscribe(onNext: { (paginationObject) in
                AppDelegateConstant.levelPagination = LevelPagination()
                AppDelegateConstant.levelPagination?.appendDataFromObject(paginationObject)
            }, onError: { (error) in
                DispatchQueue.main.async {
                    //Utilities.hideHUD(forView: self.view)
                }
            })
            return levelDisposable.disposed(by: self.disposableBag)
        }
    }
    
    // Fetch subjects
    fileprivate  func fetchSubjects(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        //Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
            let subjectObserver = ApiManager.shared.apiService.fetchSubjects(1)
            self.subjectDisposable = subjectObserver.subscribe(onNext: { (paginationObject) in
                DispatchQueue.main.async {
                   AppDelegateConstant.subjectListing = paginationObject.subjects
                }
            }, onError: { (error) in
                DispatchQueue.main.async {
                    //Utilities.hideHUD(forView: self.view)
                }
            })
            self.subjectDisposable.disposed(by: self.disposableBag)
            
        }

    }
   
}
