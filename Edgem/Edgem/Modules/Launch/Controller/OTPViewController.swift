//
//  OTPViewController.swift
//  Edgem
//
//  Created by Hipster on 01/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import RxSwift
import Unbox

protocol OTPViewControllerProtocol{
    func returnEmailOrContact(_ emailOrContact: String)
}

class OTPViewController: BaseViewController {
    
    //MARK: - Properties
    
    @IBOutlet weak var logInBtn: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var verificationCode: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var fourthLabel: UILabel!
    @IBOutlet weak var resendCodeBtn: UIButton!
    @IBOutlet weak var confirmationTextField: UITextField!
    @IBOutlet weak var countDownLabel: UILabel!
    
    @IBOutlet weak var viewtopConstraint: NSLayoutConstraint!
    //MARK: - Variables
    var userID: Int!
    var countryCode: String!
    var emailFromVerifyEmailAndContact = ""
    var contactNumber: String!
    var disposableBag = DisposeBag()
    var otpString: String!
    var _forgotPassword: Bool = false
    var _userRegistration: UserRegistration!
    var email:String?
    
    var seconds = 60*3
    var timer = Timer()
    var isTimerRunning = false
    
    var vc: UIViewController?
    var fromVerifyEmailAndContactVCToVerifyEmail = false
    
   // var delegate: OTPViewControllerProtocol?
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
         viewtopConstraint.constant = UIDevice.current.screenType.rawValue.contains("X") ? 35 : 0
        resendCodeBtn.isUserInteractionEnabled = false
        resendCodeBtn.backgroundColor =  UIColor(white: 1, alpha: 0.60)
        
        //countDownLabel.isHidden = false
        
        descriptionLabelSetUp()
        confirmationTextField.delegate = self
        confirmationTextField.isUserInteractionEnabled = true
        confirmationTextField.addTarget(self, action: #selector(codeChanged(textField:)), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        if isTimerRunning == false {
            countDownLabel.isHidden = false
            runTimer()
        }
    }
    
    //MARK: - Private Methods
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(OTPViewController.updateTimer)), userInfo: nil, repeats: true)
        isTimerRunning = true
    }
    
    @objc func updateTimer() {
        if seconds < 1 {
            timer.invalidate()
            //Send alert to indicate "time's up!"
            timerStoped()
        } else {
            seconds -= 1
            countDownLabel.text = timeString(time: TimeInterval(seconds))
        }
    }
    
    func resetTimer() {
        timer.invalidate()
        seconds = 60*3    //Here we manually enter the restarting point for the seconds, but it would be wiser to make this a variable or constant.
        countDownLabel.text = timeString(time: TimeInterval(seconds))
        isTimerRunning = false
    }
    
    func timeString(time:TimeInterval) -> String {
        //let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    func timerStoped(){
        print("timer stopped")
        resendCodeBtn.isUserInteractionEnabled = true
        resendCodeBtn.backgroundColor =  UIColor.white
        
        otpLabelOpaqueBacground()
    }
    
    func otpLabelWhiteBacground(){
        confirmationTextField.isUserInteractionEnabled = true
        firstLabel.backgroundColor = UIColor(white: 1, alpha: 1)
        secondLabel.backgroundColor = UIColor(white: 1, alpha: 1)
        thirdLabel.backgroundColor = UIColor(white: 1, alpha: 1)
        fourthLabel.backgroundColor = UIColor(white: 1, alpha: 1)
    }
    
    func otpLabelOpaqueBacground(){
        confirmationTextField.isUserInteractionEnabled = false
        firstLabel.backgroundColor = UIColor(white: 1, alpha: 0.5)
        secondLabel.backgroundColor = UIColor(white: 1, alpha: 0.5)
        thirdLabel.backgroundColor = UIColor(white: 1, alpha: 0.5)
        fourthLabel.backgroundColor = UIColor(white: 1, alpha: 0.5)
    }
    
    func descriptionLabelSetUp(){
        
        if  self.fromVerifyEmailAndContactVCToVerifyEmail == true {
            descriptionLabel.text = "A code has been sent to \(String(describing: emailFromVerifyEmailAndContact))"
        }else{
//            descriptionLabel.text = "A code has been sent to \(String(describing: countryCode!)) \(String(describing: contactNumber!))"
            if let countryCode = countryCode, let contactNumber = self.contactNumber{
                 descriptionLabel.text = "A code has been sent to \(String(describing: countryCode)) \(String(describing: contactNumber))"
            }
        }
        countDownLabel.font = UIFont(name: "Quicksand-Bold", size: 16)
    }
    
    @objc func codeChanged(textField: UITextField) {
        resetFields()
        let code = textField.text ?? ""
        for (index, character) in code.enumerated() {
            switch index + 1 {
            case 1:
                //firstLabel.backgroundColor = UIColor.white
                firstLabel.text = "\(character)"
            case 2:
               // secondLabel.backgroundColor = UIColor.white
                secondLabel.text = "\(character)"
            case 3:
                //thirdLabel.backgroundColor = UIColor.white
                thirdLabel.text = "\(character)"
            case 4:
                //fourthLabel.backgroundColor = UIColor.white
                fourthLabel.text = "\(character)"
            default:
                break
            }
        }
    }
    
    //---- reset OTP labels
    func resetFields() {
        firstLabel.text = ""
        secondLabel.text = ""
        thirdLabel.text = ""
        fourthLabel.text = ""
    }
    
    func resendButtonInActiveStatus(){
        resendCodeBtn.isUserInteractionEnabled = false
        resendCodeBtn.backgroundColor =  UIColor(white: 1, alpha: 0.60)
    }
    
    func resendButtonActiveStatus(){
        resendCodeBtn.isUserInteractionEnabled = true
        resendCodeBtn.backgroundColor =  UIColor.white
    }
    //MARK: - Actions
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        navigateBack(sender: sender)
    }
    
    @IBAction func ligInBtnTapped(_ sender: UIButton) {
         self.logInBtnTapped()
    }
    
    @IBAction func resendBtnTapped(_ sender: UIButton) {
        confirmationTextField.text = ""
//   resendButtonInActiveStatus()
//        resetTimer()
//        runTimer()
        resetFields()
        resendOTP()
    }
    
    @IBAction func activateTextField(_ sender: UIButton) {
        confirmationTextField.becomeFirstResponder()
    }
    
}

    //MARK: - TextField Delegate/DataSource

extension OTPViewController: UITextFieldDelegate{
   
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == OTPMaxLimit{
            if _forgotPassword{
                 forgotPassword(textField.text ?? "")
            }else{
                if vc is VerifyEmailAndContactViewController{
                    verifyRequestOTP(textField.text ?? "")
                }else{
                     verifyOTP(textField.text ?? "")
                }

            }
          
           
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        
        return (textField.text?.count)! < OTPMaxLimit
    }
}

extension OTPViewController{
    //MARK: - API Implementation
    
    // ---- FORGET PASSWORD
    
    func forgotPassword(_ otp: String) {
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
//        if isTimerRunning == false {
//            countDownLabel.isHidden = false
//            //runTimer()
//        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let userId = String(userID)
        let forgotObserver = ApiManager.shared.apiService.sentOTPToMobileNumber("", userId, otp)
        let forgotDisposable = forgotObserver.subscribe(onNext: {(userIDEmail) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                let resetPasswordOTPViewController = ResetPasswordViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                // forgotPasswordOTPViewController.phoneNumber = self.mobileNumberTextField.text!
                resetPasswordOTPViewController._userID = String(userIDEmail.userId)
                if let email = self.email{
                     resetPasswordOTPViewController.userEmail = email
                }
                resetPasswordOTPViewController._userRegistration = self._userRegistration
                self.navigationController?.pushViewController(resetPasswordOTPViewController, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        forgotDisposable.disposed(by: disposableBag)
    }
    
    
    // --- verify request otp
    /// This function will only be called when user is already logged - in and navigationg From VerifyEmailAndContactViewController.
    ///
    /// - Parameter otp: otp will be get from server
    func verifyRequestOTP(_ otp: String){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let param:[String:String] = ["otp" : otp]
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let verifyObserver = ApiManager.shared.apiService.verifyRequestedOtp(param as [String : AnyObject])
        let verifyDisposable = verifyObserver.subscribe(onNext:{(msg) in
            DispatchQueue.main.async{
                Utilities.hideHUD(forView: self.view)
                
                // ----> When user wants to verify email then 'if' condition should be execute and if user wants to update contact then 'else' condition should be execute
                if self.fromVerifyEmailAndContactVCToVerifyEmail == true{
                    // update email
                    let emailAndContactRefrence = EmailAndContact(self.emailFromVerifyEmailAndContact, nil, nil)
                    if AppVariables.update_Parent_EmailAndContact_Or_Child == "Parent"{
                         AppVariables.emailAndContact_parent = emailAndContactRefrence
                    }else{
                         AppVariables.emailAndContact = emailAndContactRefrence
                    }
                   
                }else{
                     // update contact
                    var countryCode = ""
                    if self.countryCode.contains("+") == true{
                        countryCode = String(self.countryCode.dropFirst())
                    }else{
                        countryCode = self.countryCode
                    }
                    
                    let emailAndContactRefrence = EmailAndContact(nil, self.contactNumber, countryCode)
                    //AppVariables.emailAndContact = emailAndContactRefrence
                    if AppVariables.update_Parent_EmailAndContact_Or_Child == "Parent"{
                        AppVariables.emailAndContact_parent = emailAndContactRefrence
                    }else{
                         AppVariables.emailAndContact = emailAndContactRefrence
                    }
                }

                if let pre_vc = self.navigationController?.getSecondPreviousViewController(){
                    self.navigationController?.popToViewController(pre_vc, animated: true)
                }
            }
            
        }, onError:{(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        verifyDisposable.disposed(by: disposableBag)
    }
    //
    
    //---- verify OTP
    func verifyOTP(_ otp: String){

        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }

        Utilities.showHUD(forView: self.view, excludeViews: [])
        
       let verifyObserver = ApiManager.shared.apiService.verifyOTP(otp, forUserID: userID)
       let verifyDisposable = verifyObserver.subscribe(onNext:{(user) in
        
            Utilities.hideHUD(forView: self.view)
        
        DispatchQueue.main.async{
            
            UserStore.shared.isLoggedIn = true
            UserStore.shared.userEmail = user.email
            UserStore.shared.selectedUserType = user.userType
            UserStore.shared.userID = user.ID
            UserStore.shared.token = user.userToken
            
            if let subject = user.subjects, subject.count == 0{
                AppDelegateConstant.shouldShowPopUPForSubjec = true
            }else{
                AppDelegateConstant.shouldShowPopUPForSubjec = false
            }
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.user = user
            AppVariables.user_ID = nil
            if user.userType == "Student" || user.userType == "Parent"{
                let congratulationVC = CongratulationViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                congratulationVC.user = user
//                if self._userRegistration.password.isEmptyString(){
//                    congratulationVC.password = self._userRegistration.password
//                }
                if let reg = self._userRegistration, reg.password.isEmptyString(){
                    congratulationVC.password = self._userRegistration.password
                }
               
                self.navigationController?.pushViewController(congratulationVC, animated: true)
            }else{
                let congratulationVC = CongratulationViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                congratulationVC.user = user
//                if self._userRegistration.password.isEmptyString(){
//                    congratulationVC.password = self._userRegistration.password
//                }
                
                if let reg = self._userRegistration, reg.password.isEmptyString(){
                     congratulationVC.password = self._userRegistration.password
                }
               
                self.navigationController?.pushViewController(congratulationVC, animated: true)
            }
        }
            
        }, onError:{(error) in
                    DispatchQueue.main.async(execute: {
                       Utilities.hideHUD(forView: self.view)
                         if let error_ = error as? ResponseError {
                         self.showSimpleAlertWithMessage(error_.description())
                         } else {
                          if !error.localizedDescription.isEmpty {
                                self.showSimpleAlertWithMessage(error.localizedDescription)
                          }
                        }
                    })
        })
         verifyDisposable.disposed(by: disposableBag)
    }
    
    //---- resend OTP
    func resendOTP() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])

        let resendObserver = ApiManager.shared.apiService.resendOTPForID(userID)
        let resendDisposable = resendObserver.subscribe(onNext: {(successDictionary) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                self.resendButtonInActiveStatus()
                let alert = UIAlertController(title: AppName, message: "OTP has been resent".localized, preferredStyle: .alert)
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                    self.resetTimer()
                    self.runTimer()
                    self.countDownLabel.isHidden = false
                 self.otpLabelWhiteBacground()
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        resendDisposable.disposed(by: disposableBag)
    }
}

