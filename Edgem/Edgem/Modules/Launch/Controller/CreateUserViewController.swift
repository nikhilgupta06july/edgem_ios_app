//
//  CreateUserViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 15/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

    import UIKit
    import RxSwift
    
    class CreateUserViewController: BaseViewController {
    
        var disposableBag = DisposeBag()
        let labelFields = ["Create Username &\n Password".localized,"Username*".localized, "Password*".localized, "Confirm password*".localized, "Referral Code".localized]//, "Register to authenticate with your \nface or finger".localized]
        var isStudent: Bool!
        var facebookDict: [String: AnyObject]!
        var gmailDict: [String:AnyObject]!
        var userRegistration = UserRegistration()
        var isValid: Bool!
        var isCheckBoxBtnSelected: Bool = false
        
        private var gradient: CAGradientLayer!
        
        @IBOutlet weak var viewTopConstraint: NSLayoutConstraint!
        @IBOutlet weak var tableView: UITableView!
        @IBOutlet weak var nextButton: UIButton!
      
            override func viewDidLoad() {
                super.viewDidLoad()
                
                tableView.register(UINib(nibName: RefferalTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: RefferalTableViewCell.cellIdentifire())
                self.isCheckBoxBtnSelected = (userRegistration.referralCode != "")
                
                viewTopConstraint.constant = UIDevice.current.screenType.rawValue.contains("X") ? 35 : 0
                
                gradient = CAGradientLayer()
                gradient.frame = (tableView.superview?.frame) ?? .null
                gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
                gradient.locations = [0.0, 0.05, 0.98, 1.0]
                tableView.backgroundColor = .clear
                tableView.superview?.layer.mask = gradient
                
                userRegistration.isStudent = isStudent
                
                if let fbDict = facebookDict {
                    self.userRegistration.copyValuesFromFBDict(fbDict)
                    self.tableView.reloadData()
                }else if let gmailDict = gmailDict{
                    self.userRegistration.copyValuesFromGmailDict(gmailDict)
                    self.tableView.reloadData()
                }
                
                showHideNextButton(false)
                
                // Add notification observer for common popup yes/no button
                
                NotificationCenter.default.addObserver(self, selector: #selector(yesButtonTapped), name: .yesBtnTapped, object: nil)
            }
        
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            gradient.frame = tableView.superview?.bounds ?? .null
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
              print(userRegistration)
        }
            //MARK:- IBActions
            @IBAction func loginButtonTapped(_ sender: UIButton) {
                let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
                let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                    self.navigationController?.popToRootViewController(animated: true)
                }
                
                let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
                alert.addAction(yesAction)
                alert.addAction(noAction)
                self.present(alert, animated: true, completion: nil)
            }
            
            @IBAction func nextButtonTapped(_ sender: EdgemCustomButton) {
                self.view.endEditing(true)
                let validationInformation = isDataValid()
                guard validationInformation.isValid else {
                    self.showSimpleAlertWithMessage(validationInformation.message)
                    return
                }
//                if !userRegistration.referralCode.isEmptyString() && isCheckBoxBtnSelected == true{
//                    checkReferralCodeValidity(userRegistration.referralCode)
//                }
                if UserStore.shared.selectedUserType == "S"{
                    let creatStudentAccountViewController = CreateStudentAccountViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                    creatStudentAccountViewController.userRegistration = self.userRegistration
                    self.navigationController?.pushViewController(creatStudentAccountViewController, animated: true)
                }else{
                     let signupViewController = SignUpViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                    signupViewController.userRegistration = self.userRegistration
                    self.navigationController?.pushViewController(signupViewController, animated: true)
                }
            }
            
            @IBAction func backButtonTapped(_ sender: UIButton) {
                
                self.view.endEditing(true)
                
                if userRegistration.userName != "" ||
                    userRegistration.password != "" ||
                    userRegistration.confirmPassword != ""  {
                    let alert = UIAlertController(title: AppName, message: AlertMessage.removeFieldData.description(), preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                        self.navigateBack(sender: sender)
                    }
                    
                    let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
                    alert.addAction(yesAction)
                    alert.addAction(noAction)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    navigateBack(sender: sender)
                }
            }
            
            //MARK:- Helping Function
        
        private func ReferralTextFieldConditions(_ status: Bool) {
            let validationInformation = isDataValid()
            if status == true {

                if validationInformation.isValid == false || self.userRegistration.referralCode == ""{
                    showHideNextButton(false)
                }else{
                    showHideNextButton(true)
                }
                
            }else{
                
                if validationInformation.isValid == true {
                    showHideNextButton(true)
                }else{
                    showHideNextButton(false)
                }
                
            }
            
        }
        
        func didTapCheckBoxBtn(_ sender: UIButton){
            //self.tableView.beginUpdates()
            // isCheckBoxBtnSelected = sender.isSelected
            if let cell = tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as? RefferalTableViewCell{
            
                if sender.isSelected{
                    sender.isSelected = false
                    sender.setImage(#imageLiteral(resourceName: "checkboxInactive"), for: .normal)
                    cell.refferalCodeTextField.isHidden = true
                    cell.refferalCodeTextField.text = ""
                    userRegistration.referralCode = ""
                    cell.errorLabel.text = ""
                    isCheckBoxBtnSelected = false
                    ReferralTextFieldConditions(isCheckBoxBtnSelected)
                 
                }else{
                    sender.isSelected = true
                    sender.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
                    cell.refferalCodeTextField.isHidden = false
                    isCheckBoxBtnSelected = true
                    ReferralTextFieldConditions(isCheckBoxBtnSelected)
                }
                
                
                tableView.reloadData()
                tableView.scrollToRow(at: IndexPath(item: 4, section: 0), at: .bottom, animated: true)
            }
        }
        @objc func yesButtonTapped(){
            print("yes button tapped")
            self.navigationController?.popViewController(animated: true)
         }
        
            func configureTextField(_ customTextField: EdgemCustomTextField) {
                customTextField.delegate = self
                customTextField.inputView = nil
                customTextField.keyboardType = .asciiCapable
                customTextField.autocorrectionType = UITextAutocorrectionType.no
                switch customTextField.tag {
                case CreateUserTextFieldType.userName.rawValue:
                    customTextField.isSecureTextEntry = false
                case CreateUserTextFieldType.password.rawValue:
                    customTextField.isSecureTextEntry = true
                case CreateUserTextFieldType.confirmPassword.rawValue:
                    customTextField.isSecureTextEntry = true
                default:
                    customTextField.keyboardType = .asciiCapable
                }
                
            }
        
        func updateDataIntoRefferalTextField(_ customTextField: UITextField, fromCell: Bool) {
             fromCell ? (customTextField.text = userRegistration.referralCode) : (userRegistration.referralCode = customTextField.text!)
            if userRegistration.referralCode == "" && isCheckBoxBtnSelected == true{
                showHideNextButton(false)
            }else{
                ReferralTextFieldConditions(isCheckBoxBtnSelected)
            }
        }
            
            func updateDataIntoTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
                
                switch customTextField.tag {
                    
                case CreateUserTextFieldType.userName.rawValue:
                    fromCell ? (customTextField.text = userRegistration.userName) : (userRegistration.userName = customTextField.text!)
                    break
                case CreateUserTextFieldType.password.rawValue:
                    fromCell ? (customTextField.text = userRegistration.password) : (userRegistration.password = customTextField.text!)
                    break
                case CreateUserTextFieldType.confirmPassword.rawValue:
                    fromCell ? (customTextField.text = userRegistration.confirmPassword) : (userRegistration.confirmPassword = customTextField.text!)
                    break
                default:
                    break
                }
                
             if userRegistration.userName == "" || userRegistration.password == "" || userRegistration.confirmPassword == ""{
                        showHideNextButton(false)
                        if fromCell == false{
                            tableView.beginUpdates()
                            tableView.endUpdates()
                        }
                    }
            }
            
        func isDataValid() -> (isValid: Bool, message: String){
            var errorMessages: [String] = []
            if userRegistration.userName.isEmpty, userRegistration.password.isEmpty, userRegistration.confirmPassword.isEmpty {
                return (isValid: false, message: "")
            } else {
                for index in 1..<labelFields.count {
                    
                    let currentIndexPath = IndexPath.init(row: index, section: 0)
                    if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                        switch index {
                        case CreateUserTextFieldType.userName.rawValue:
                            cell.validateUserName(text: userRegistration.userName)
                        case CreateUserTextFieldType.password.rawValue:
                            cell.validatePassword(text: userRegistration.password)
                        case CreateUserTextFieldType.confirmPassword.rawValue:
                            cell.validateConfirmPassword(text: userRegistration.confirmPassword, password: userRegistration.password)
                        
                        default:
                            break
                        }
                        let message = cell.errorLabel.text ?? ""
                        if !message.isEmptyString() {
                            errorMessages.append(message)
                        }
                    }
                    
                }
                tableView.beginUpdates()
                tableView.endUpdates()
                let isValidData = errorMessages.count == 0
                return (isValid: isValidData, message: isValidData ? "" : errorMessages[0])
            }
        }

    func showHideNextButton(_ status: Bool) {
        if status == false{
            nextButton.isUserInteractionEnabled = false
            nextButton.backgroundColor = UIColor(white: 1, alpha: 0.60)
        }else{
            nextButton.isUserInteractionEnabled = true
            nextButton.backgroundColor = UIColor.white
        }
    }
    
    
        }

        // MARK: - TableView Delegate/DataSource
        
        extension CreateUserViewController: UITableViewDelegate, UITableViewDataSource{
            
            func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return labelFields.count
            }
            
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                guard indexPath.row != 0 else {
                   let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
                    cell.selectionStyle = .none
                    if let label = cell.viewWithTag(1) as? UILabel{
                        label.text = labelFields[indexPath.row]//"Create username &\n passsword".localized
                    }
                    return cell
                }
                
                guard indexPath.row != labelFields.count-1 else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: RefferalTableViewCell.cellIdentifire(), for: indexPath) as! RefferalTableViewCell
                    cell.selectionStyle = .none
                    cell.refferalCodeTextField.delegate = self
                    cell.checkBoxBtn.isSelected = isCheckBoxBtnSelected
//                    ReferralTextFieldConditions(isCheckBoxBtnSelected)
                    cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
                    updateDataIntoRefferalTextField(cell.refferalCodeTextField, fromCell: true)
                    cell.createUserViewController = self
                    return cell
                }
                
                let cell = tableView.dequeueReusableCell(withIdentifier: EdgemTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemTextFieldTableViewCell
                cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
                
                configureTextField(cell.customTextField)
                updateDataIntoTextField(cell.customTextField, fromCell: true)
                
                cell.selectionStyle = .none
                return cell
                
            }
            
            func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                guard indexPath.row != 0 else {
                    return 175
                }
                
                return UITableView.automaticDimension
            }
        }

    //MARK: - TextField Delegate
        
        extension CreateUserViewController: UITextFieldDelegate {
            
            func textFieldDidBeginEditing(_ textField: UITextField) {
               textField.textColor = theamBlackColor
            }
            
            func textFieldDidEndEditing(_ textField: UITextField) {
                
                if textField.tag == 4{
                    print("refereal text field")
                    updateDataIntoRefferalTextField(textField, fromCell: false)
                    let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
                    let cell = tableView.cellForRow(at: currentIndexPath) as! RefferalTableViewCell
                    cell.validateReferralCode(text: textField.text!)
                    
                    if cell.errorLabel.text?.count == 0, !userRegistration.referralCode.isEmpty{
                        checkReferralCodeValidity(textField.text ?? "" )
                    }
                    if userRegistration.referralCode == "" {
                        showHideNextButton(false)
                    }else{
                        if userRegistration.userName != "" && userRegistration.password != "" && userRegistration.confirmPassword != "" {
                            if isCheckBoxBtnSelected == true {
                              if userRegistration.referralCode == ""{
                                    showHideNextButton(false)
                              }else{
                                    checkReferralCodeValidity(userRegistration.referralCode)
                                }
                            }else{
                                showHideNextButton(isDataValid().isValid)
                            }
                            
                        }
                    }
                    tableView.beginUpdates()
                    tableView.endUpdates()
                    return
                }
                
                updateDataIntoTextField(textField as! EdgemCustomTextField, fromCell: false)
                
                let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
                 let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
                
                switch textField.tag {
                case CreateUserTextFieldType.userName.rawValue:
                    cell.validateUserName(text: textField.text ?? "")
                    if cell.errorLabel.text?.count == 0, !userRegistration.userName.isEmpty{
                        checkUserName(textField.text ?? "" )
                    }
                case CreateUserTextFieldType.password.rawValue:
                    cell.validatePassword(text: textField.text ?? "")
                case CreateUserTextFieldType.confirmPassword.rawValue:
                    cell.validateConfirmPassword(text: textField.text ?? "", password: userRegistration.password)

                default:
                    break
                }
                
                if userRegistration.userName != "" && userRegistration.password != "" && userRegistration.confirmPassword != "" {
                    if isCheckBoxBtnSelected == true && userRegistration.referralCode == ""{
                        showHideNextButton(false)
                    }else if isCheckBoxBtnSelected == true && userRegistration.referralCode != ""{
                        checkReferralCodeValidity(userRegistration.referralCode)
                    }else{
                        showHideNextButton(isDataValid().isValid)
                    }
                 
                }
                
                tableView.beginUpdates()
                tableView.endUpdates()
                
            }

                func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                    if string == " " {
                        return false
                    }

                    switch textField.tag {
                    case CreateUserTextFieldType.userName.rawValue:
                        
                        return (textField.text?.count)! < DisplayNameMaxLimit
                        
                    case CreateUserTextFieldType.referralCode.rawValue:
                        
                        let currentText = textField.text ?? ""
                        
                        guard let stringRange = Range(range, in: currentText) else { return false }
                        
                        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
                        
                        return updatedText.count <= ReferralCodeMinLimit
                        
//                        return (textField.text?.count)! < ReferralCodeMinLimit
                        
                    default:
                        return true
                    }
            }
}


// MARK: -  API Implementation

extension CreateUserViewController{
    
    func checkReferralCodeValidity(_ referralCode: String) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userNameObserver = ApiManager.shared.apiService.verifyReferralCode(referralCode)
        let userNameDisposable = userNameObserver.subscribe(onNext: {(message) in
            
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                if let cell = self.tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as? RefferalTableViewCell{
                    //cell.errorLabel.text = ""
                      self.showHideNextButton(self.isDataValid().isValid)
                }
//                self.tableView.reloadData()
//                self.tableView.scrollToRow(at: IndexPath(item: 4, section: 0), at: .bottom, animated: true)
                if message != "You may continue"{
                    self.showSimpleAlertWithMessage(message)
                }
            }
            
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                if let cell = self.tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as? RefferalTableViewCell{
                   //
                   // cell.errorLabel.text = (error.localizedDescription)
                    self.showHideNextButton(false)
//                    self.tableView.reloadData()
//                    self.tableView.scrollToRow(at: IndexPath(item: 4, section: 0), at: .bottom, animated: true)
                }
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userNameDisposable.disposed(by: disposableBag)
    }
    
    
    
    func checkUserName(_ userName: String){
        if !isConnectedToNetwork(){
             self.showNoInternetMessage()
             return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let userNameObserver = ApiManager.shared.apiService.checkUserNameAvailibility(userName)
        let userNameDisposable = userNameObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            print(message)
            if message == responseMessagesForUserAvailable.isNotExist.description(){
                DispatchQueue.main.async {
                    
                }

            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.showHideNextButton(false)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        //self.showSimpleAlertWithMessage(error.localizedDescription)
                        let alert = UIAlertController(title: AppName,
                                                      message: error.localizedDescription.localized,
                                                      preferredStyle: .alert)
                        let yesAction = UIAlertAction(title: AlertButton.ok.description(),
                                                      style: UIAlertAction.Style.default,
                                                      handler: { (action) in
                                                        let indexPath = IndexPath(row: 1, section: 0)
                                                        guard  let cell = self.tableView.cellForRow(at: indexPath) as? EdgemTextFieldTableViewCell else{return}
                                                        cell.customTextField.becomeFirstResponder()
                                                        //                                                        self.nextButton.isUserInteractionEnabled = false
                                                        //                                                        self.nextButton.backgroundColor = UIColor(white: 1, alpha: 0.60)
                        })
                        alert.addAction(yesAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        })
        userNameDisposable.disposed(by: disposableBag)
    }
    
}








