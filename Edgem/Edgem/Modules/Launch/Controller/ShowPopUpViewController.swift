//
//  ShowPopUpViewController.swift
//  Edgem
//
//  Created by Hipster on 20/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit

class ShowPopUpViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func okBtnTapped(_ sender: UIButton) {
        self.dismissPopUp()
        let selectLocationVC = CreateStudentAccountStepTwoViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        UIApplication.topViewController()?.present(selectLocationVC, animated: true, completion: nil)
    }
}
