//
//  AddOrSaveLocationViewController.swift
//  Edgem
//
//  Created by Hipster on 14/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import RxSwift

protocol BackButtonTappedDelegate{
    func backBtnTapped(_ fromBackBtnTapped: Bool)
}


protocol AddMoreButtonTappedDelegate{
    func addMoreBtnTapped(_ fromAddMoreBtnTapped: Bool)
}

protocol SaveButtonTappedDelegate{
    func saveBtnTapped(_ fromSaveBtnTapped: Bool)
}

protocol AddOrSaveLocationViewControllerDelegate{
    func sendLocationData(_ profileImage: String, titleLabel: String, address: String, _ fromAddOrSaveLocationViewController:  Bool, _ index: Int, _ lattitude: String,_ longitude: String, _ addressObject: Address)
}

class AddOrSaveLocationViewController: BaseViewController, AddOrSaveLocationTableViewCellDelegate,VerifyPhoneNumberViewDelegate {
   

    // MARK: --------------------- Properties ----------------------
    
   @IBOutlet weak var tableView: UITableView!
    
    // MARK: --------------------- Variables ------------------------
    
    var userRegistration: UserRegistration!
    var address = [String]()
    var profileImage = [String]()
    var titleHeader = [String]()
    //var locationAddress: Address!
    var _addOrSaveLocationViewControllerDelegate: AddOrSaveLocationViewControllerDelegate?
    var _backButtonTappedDelegate: BackButtonTappedDelegate?
    var _saveButtonTappedDelegate: SaveButtonTappedDelegate?
    var _addMoreButtonTappedDelegate: AddMoreButtonTappedDelegate?
    var _locationData: LocationData!
    var disposableBag = DisposeBag()
    var fromSaveorConfirm: Bool!
    //var addressObjects: Address!
     var addressObjects = Address()
    var savedAddressObject = Address()
    var addressObjectsAfterSave: Address = Address()
    var index: Int!
    var gradient: CAGradientLayer!
    
    // MARK: ----------------------- @IBOutlets --------------------
    
    @IBOutlet weak var addMoreButton: UIButton!
    
    // MARK: ----------------------- View LifeCycle--------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewSetUP()
        setGradient()
    }
    
    func setGradient(){
        gradient = CAGradientLayer()
        gradient.frame = (tableView.superview?.frame) ?? .null
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0.0, 0.05, 0.98, 1.0]
        tableView.backgroundColor = .clear
        tableView.superview?.layer.mask = gradient
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //updategradientFrame()
        gradient.frame = tableView.superview?.bounds ?? .null
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         print("USER DATA: \(String(describing: userRegistration))")
        //>>>>>
        if !fromSaveorConfirm{
            if userRegistration.email != ""{
                getSavedLocationAttributes(userRegistration.email)
            }
        }else{
            if addressObjects.addr.count>0{

            }
        }
       
         tableView.reloadData()
        if self.addressObjects.addr.count >= 2{
            self.enableAddMoreButton(false)
        }else{
            self.enableAddMoreButton(true)
        }
        
    }
    
    
    // MARK: ----------------- @IBActions -------------------------
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        if _backButtonTappedDelegate != nil{

            
            let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                
                self._backButtonTappedDelegate?.backBtnTapped(true)
                
                self.userRegistration.address_label.removeAll()
                self.userRegistration.address_location.removeAll()
                self.userRegistration.address_lattitude.removeAll()
                self.userRegistration.address_longitude.removeAll()
                
                if let pre_vc = self.navigationController?.getNhViewController(4){
                    self.navigationController?.popToViewController(pre_vc, animated: true)
                }
            }
            
            let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
            alert.addAction(yesAction)
            alert.addAction(noAction)
            self.present(alert, animated: true, completion: nil)
        }else{
            print("_backButtonTappedDelegate is nil")
        }
    }
    
    @IBAction func addMoreBtnTapped(_ sender: UIButton) {
        if _addMoreButtonTappedDelegate != nil{
            _addMoreButtonTappedDelegate?.addMoreBtnTapped(true)
            self.addressObjects = Address()
            self.savedAddressObject = Address()
             self.navigationController?.popViewController(animated: true)
        }else{
            print("_addMoreButtonTappedDelegate is nil")
        }
    }
    
    @IBAction func saveBtnTapped(_ sender: UIButton) {
        if _saveButtonTappedDelegate != nil{
            _saveButtonTappedDelegate?.saveBtnTapped(true)
        }
        saveLocationAttributes()
    }
    
    
}

// MARK: ---------------------Private Functions---------------------------


extension AddOrSaveLocationViewController{
    
    // ---- initial setup
    
  
    
    // ---- tableview setup
    
    private func tableViewSetUP(){
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // ---- enable/disable AddMore Button
    func enableAddMoreButton(_ status: Bool){
        if status == false{
            addMoreButton.isUserInteractionEnabled = false
            addMoreButton.backgroundColor = UIColor(white: 1, alpha: 0.60)
        }else{
            addMoreButton.isUserInteractionEnabled = true
            addMoreButton.backgroundColor = UIColor.white
        }
    }
    
    // ---- update data into textfield
    
    func updateDataIntoTextField(_ textField: UITextField, _ fromCell: Bool){
        switch textField.tag{
        case AddOrSaveTextField.address.rawValue:
            fromCell ? (textField.text = userRegistration.addressLine1) : (userRegistration.addressLine1 = textField.text!)
        default:
            break
        }
    }
    
    func fromVerifyPhoneNumberVC(_ backBtnTapped: Bool) {
        if backBtnTapped{
            savedAddressObject = Address()
        }else{
            
        }
    }
    
    // ---- configure textfield
    
    func configureTextField(_ textField: UITextField){
        //textField.delegate = self
        textField.inputView = nil
        textField.keyboardType = .asciiCapable
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.isUserInteractionEnabled = true
    }
    
    // ---- data from cell
    
    func sendDataOnImageTapped(_ profileImage: String, titleLabelText: String, addreeTextFieldText: String, _ index: Int, _ lattitude: String, _ longitude: String) {
        editImageTapped(profileImage, title: titleLabelText, address: addreeTextFieldText, index, lattitude, longitude: longitude)
       // self.address.remove(at: index)
    }
    
    // ---- edit image tapped in cell
    
    func editImageTapped(_ profileImage: String, title: String, address: String, _ index: Int, _ lattitude: String, longitude: String){
        
        if _addOrSaveLocationViewControllerDelegate != nil{
            _addOrSaveLocationViewControllerDelegate?.sendLocationData(profileImage, titleLabel: title, address: address, true, index, lattitude, longitude, self.addressObjects)
            
            self.navigationController?.popViewController(animated: true)
        }else{
            print("_addOrSaveLocationViewControllerDelegate is nil")
        }
    }
}

//MARK: ---------------------- TableView Delegate/Datasource --------------------------

extension AddOrSaveLocationViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (addressObjects.addr.count) != 0 ?  (addressObjects.addr.count) : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: AddOrSaveLocationTableViewCell.cellIdentifire(), for: indexPath) as? AddOrSaveLocationTableViewCell else{return UITableViewCell()}
        let locationData = addressObjects.addr[indexPath.row]
        cell.configureCellForRowAtIndex(locationData: locationData, indexPath.row)
        cell.addressTextField.delegate = self
        cell.addOrSaveLocationTableViewCellDelegate = self
         cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  AddOrSaveLocationTableViewCell.cellHeight()
    }
    
    
}

extension AddOrSaveLocationViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.tintColor = UIColor.clear
        textField.resignFirstResponder()
    }
}


    // MARK: -------------------------API SERVICES ------------------------------------------

extension AddOrSaveLocationViewController{
    
    
    func editSavedLocation(){
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        
    }
    
    func getSavedLocationAttributes(_ email: String){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        self.addressObjects = Address()
        
        let regObserver = ApiManager.shared.apiService.userTempAddresses(email)
        let regDisposable = regObserver.subscribe(onNext: {(address) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                print("Saved Location Data: \(address)")
                self.addressObjects = Address()
//                if address.addr.count > 0{
//                    for index in 0..<address.addr.count{
//                         self.addressObjects.addr.append(address.addr[index])
//                    }
//                }
                
                if self.savedAddressObject.addr.count>0{
                    for index in 0..<self.savedAddressObject.addr.count{
                        self.addressObjects.addr.append(self.savedAddressObject.addr[index])
                    }
                }
               if self.addressObjects.addr.count >= 2{
                    self.enableAddMoreButton(false)
                }else{
                    self.enableAddMoreButton(true)
                }
                self.tableView.reloadData()
            }
        },onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        }
        )
        regDisposable.disposed(by: disposableBag)
    }
    
    func saveLocationAttributes(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        if addressObjects.addr.count <= 0{
            return
        }
         var address_label: [String] = [String]()
         var address_location: [String] = [String]()
         var address_lattitude: [String] = [String]()
         var address_longitude: [String] = [String]()
        for index in 0..<addressObjects.addr.count{
             address_label.append(addressObjects.addr[index].title)
             address_location.append(addressObjects.addr[index].address)
             address_lattitude.append(addressObjects.addr[index].lattitude)
             address_longitude.append(addressObjects.addr[index].longitude)
        }
        let locationAttributeParams = ["email" : userRegistration.email,
                                                         "address_label" : address_label,
                                                         "address_location" : address_location,
                                                         "lattitude" : address_lattitude,
                                                         "longitude" : address_longitude] as [String:AnyObject]
        
        
        let regObserver = ApiManager.shared.apiService.saveLocationAttributes(locationAttributeParams)
        let regDisposable = regObserver.subscribe(onNext: {(address) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                print(address)
                self.addressObjects = address
                self.tableView.reloadData()
                let verifyPhoneNumberVC = VerifyPhoneNumberViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                verifyPhoneNumberVC.userRegistration = self.userRegistration
                verifyPhoneNumberVC.isFrom = self
                verifyPhoneNumberVC._backButtonTappedDelegate = self
                self.navigationController?.pushViewController(verifyPhoneNumberVC, animated: true)
                
            }
        },onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        }
        )
        regDisposable.disposed(by: disposableBag)
    }
    }



