//
//  VerifyEmailAndContactViewController.swift
//  Edgem
//
//  Created by Namespace on 29/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class VerifyEmailAndContactViewController : BaseViewController {
    
    // MARK: - Properties
    var rowCount = 0
    let labelFields = ["Contact Number*".localized, "Generate OTP".localized]
    var countryCodeOptions = ["+65"]
    let countryCodePicker = UIPickerView()
    var activeTextField: UITextField!
    var userRegistration = UserRegistration()
    var disposableBag = DisposeBag()
    
    var VC_TITLE = ""
    var fromVerifyEmailAndContactVCToVerifyEmail = false
    var email_or_contact: Int = -1                      // ---> 0 represent email type vc and 1 represent contact type vc
    //var someModel = SomeModel()
    
    // MARK: - @IBOutlets
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - ViewLifeCycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUpForCountryCodePicker()
        initialSetUp()
        
        userRegistration.countryContactCode = "+65"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.someFunction()
    }
    
    // MARK: - @IBActions
    
    @objc func didTapGenerateOtpBtn(_ sender: UIButton) {
        if email_or_contact == 0 {
            // email api
            fromVerifyEmailAndContactVCToVerifyEmail = true
            let param: [String:String] = ["new_email":userRegistration.email]
            self.requestOTP(param as [String : AnyObject])
        }else{
            // contact api
            fromVerifyEmailAndContactVCToVerifyEmail = false
            var countryCode = ""
            if (userRegistration.countryContactCode).contains("+") == true{
                countryCode = String((userRegistration.countryContactCode).dropFirst())
            }else{
                countryCode = (userRegistration.countryContactCode)
            }
            let param: [String:String] = ["country_code":"65",
                                                        "new_contact_number": userRegistration.contactNumber]
            self.requestOTP(param as [String : AnyObject])
        }
    }
    
    @IBAction func didTapBackBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Helping Functions
    
    fileprivate  func isDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        for index in 0...labelFields.count {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            
            if email_or_contact == 0{
                
                switch index {
                case VerifyEmailAndContactTextFieldTypeEmail.email.rawValue:
                    if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                        cell.validateEmail(text: userRegistration.email)
                        errorMessages.append(cell.errorLabel.text ?? "")
                    }
                    
                default:
                    break
                }
                
            }else{
                
                switch index {
                case VerifyEmailAndContactTextFieldType.dual.rawValue:
                    let dualCell = tableView.cellForRow(at: currentIndexPath) as! EdgemDualTextFieldTableViewCell
                    dualCell.validateCountryCode(text: dualCell.textFieldOne.text ?? "+65")
                    dualCell.validatePhone(text: userRegistration.contactNumber)
                    errorMessages.append(dualCell.errorMessageLabelOne.text ?? "")
                    errorMessages.append(dualCell.errorMessageLabelTwo.text ?? "")
                    
                default:
                    break
                }
                
            }

        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.reloadData()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }
    
    func showHideNextButton(_ status: Bool) {
        
        let indexPath = IndexPath(row: 1, section: 0)
        guard let cell = tableView.cellForRow(at: indexPath) else{return}
        guard let generaeOTPBtn = cell.viewWithTag(indexPath.row) as? UIButton else{return}
        if status == false{
            generaeOTPBtn.isUserInteractionEnabled = false
            generaeOTPBtn.backgroundColor = UIColor(white: 1, alpha: 0.60)
        }else{
            generaeOTPBtn.isUserInteractionEnabled = true
            generaeOTPBtn.backgroundColor = UIColor.white
        }
        
    }
    
    private func initialSetUpForCountryCodePicker(){
        countryCodePicker.delegate = self
        countryCodePicker.dataSource = self
        countryCodePicker.tag = 1
    }
    
    private func initialSetUp(){
        
        // View Controller tilte
        self.titleLabel.text = VC_TITLE
        
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func updateDataIntoTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
        switch customTextField.tag {
        case VerifyEmailAndContactTextFieldType.dual.rawValue:
            if customTextField.title == "Contact Number*".localized {
                fromCell ? (customTextField.text = userRegistration.contactNumber) : (userRegistration.contactNumber = customTextField.text!)
            } else {
                fromCell ? (customTextField.text = userRegistration.countryContactCode) : (userRegistration.countryContactCode = customTextField.text!)
            }
            
            break
        default:
            break
        }
        
        if userRegistration.contactNumber == ""  {
            showHideNextButton(false)
            if fromCell == false{
                tableView.beginUpdates()
                tableView.endUpdates()
            }
        }
        
    }
    
    func updateDataIntoTextFieldForEmail(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
        switch customTextField.tag {
        case VerifyEmailAndContactTextFieldTypeEmail.email.rawValue:
           fromCell ? (customTextField.text = userRegistration.email) : (userRegistration.email = customTextField.text!)
            break
        default:
            break
        }
        
        if userRegistration.email == ""  {
            showHideNextButton(false)
            if fromCell == false{
                tableView.beginUpdates()
                tableView.endUpdates()
            }
        }
        
    }
    
    private func configureTextField(_ customTextField: EdgemCustomTextField) {
        customTextField.delegate = self
        customTextField.inputView = nil
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        switch customTextField.tag {
            
        case VerifyEmailAndContactTextFieldType.dual.rawValue:
            if customTextField.title == "Contact Number*".localized {
                customTextField.keyboardType = .phonePad
            } else{
                customTextField.inputView = countryCodePicker
                customTextField.icon = "drop-down-arrow"
                
                customTextField.showImage()
                customTextField.keyboardType = .phonePad
                customTextField.tintColor = UIColor.clear
            }
            
        default:
            customTextField.keyboardType = .asciiCapable
        }
        
    }
    
    private func configureTextFieldForEmail(_ customTextField: EdgemCustomTextField) {
        customTextField.delegate = self
        customTextField.inputView = nil
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        switch customTextField.tag {
            
        case VerifyEmailAndContactTextFieldTypeEmail.email.rawValue:
        customTextField.keyboardType = .emailAddress
        customTextField.tintColor = UIColor.blue
            
        default:
            customTextField.keyboardType = .asciiCapable
        }
        
    }
    
}

    // MARK: - TableView Delegates/Datasources

extension VerifyEmailAndContactViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2//rowCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row != 1 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath)
            let generaeOTPBtn = cell.viewWithTag(indexPath.row) as? UIButton
            generaeOTPBtn?.setTitle(labelFields[indexPath.row], for: .normal)
            generaeOTPBtn?.addTarget(self, action: #selector(didTapGenerateOtpBtn), for: .touchUpInside)
            if email_or_contact == 0 {
                
                if userRegistration.email == ""{
                    generaeOTPBtn?.isUserInteractionEnabled = false
                    generaeOTPBtn?.backgroundColor = UIColor(white: 1, alpha: 0.60)
                }else{
                    generaeOTPBtn?.isUserInteractionEnabled = true
                    generaeOTPBtn?.backgroundColor = UIColor.white
                }
                
            }else{
                if userRegistration.contactNumber == ""{
                    generaeOTPBtn?.isUserInteractionEnabled = false
                    generaeOTPBtn?.backgroundColor = UIColor(white: 1, alpha: 0.60)
                }else{
                    generaeOTPBtn?.isUserInteractionEnabled = true
                    generaeOTPBtn?.backgroundColor = UIColor.white
                }
            }
            cell.selectionStyle = .none
            return cell
        }
        
        if email_or_contact == 0 {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: EdgemTextFieldTableViewCell.cellIdentifier(), for: indexPath) as? EdgemTextFieldTableViewCell else{return UITableViewCell()}
                     cell.configureCellForRowAtIndex(indexPath.row, withText: "Email Address*")
                     configureTextFieldForEmail(cell.customTextField)
                     updateDataIntoTextFieldForEmail(cell.customTextField, fromCell: true)
            
                    cell.selectionStyle = .none
                    return cell
        }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: EdgemDualTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemDualTextFieldTableViewCell
                    cell.configureCellWithTitles(title: ["+65",labelFields[indexPath.row]], index: indexPath.row)
                    configureTextField(cell.textFieldOne)
                    updateDataIntoTextField(cell.textFieldOne, fromCell: true)
                    configureTextField(cell.textFieldTwo)
                    updateDataIntoTextField(cell.textFieldTwo, fromCell: true)
            
                    cell.selectionStyle = .none
                    return cell
        }

    }

}

// MARK: - TextField Delegates

extension VerifyEmailAndContactViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! EdgemCustomTextField
        if  activeTextField.text?.contains("+") == true{
            managePickerView()
        }
    }

    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if email_or_contact == 0{
               updateDataIntoTextFieldForEmail(textField as! EdgemCustomTextField, fromCell: false)
            
        }else{
               updateDataIntoTextField(textField as! EdgemCustomTextField, fromCell: false)
        }

        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        
        if email_or_contact == 0 {
            //updateDataIntoTextFieldForEmail(textField as! EdgemCustomTextField, fromCell: false)
            
            switch textField.tag {
             
             case VerifyEmailAndContactTextFieldTypeEmail.email.rawValue:
             guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else{return}
             cell.validateEmail(text: textField.text ?? "")
             if cell.errorLabel.text?.count == 0, !userRegistration.email.isEmpty {
             verifyEmail(userRegistration.email)
             }
             
             default:
             break
             }
            
             if  userRegistration.email != "" {
             showHideNextButton(isDataValid().isValid)
             }
            
        }else{
            // updateDataIntoTextField(textField as! EdgemCustomTextField, fromCell: false)
            
            switch textField.tag {
            case VerifyEmailAndContactTextFieldType.dual.rawValue:
                let dualCell = tableView.cellForRow(at: currentIndexPath) as! EdgemDualTextFieldTableViewCell
                let customField = textField as! EdgemCustomTextField
                if customField.title == "Contact Number*".localized {
                    dualCell.validatePhone(text: customField.text ?? "")
                    if dualCell.errorMessageLabelTwo.text?.count == 0, !userRegistration.contactNumber.isEmpty {
                        verifyContact(userRegistration.contactNumber)
                    }
                } else {
                    dualCell.validateCountryCode(text: customField.text ?? "")
                }
            default:
                break
            }
            if userRegistration.contactNumber != ""  {
                showHideNextButton(isDataValid().isValid)
            }
            
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if email_or_contact != 0{
            // get the current text, or use an empty string if that failed
            let currentText = textField.text ?? ""
            
            // attempt to read the range they are trying to change, or exit if we can't
            guard let stringRange = Range(range, in: currentText) else { return false }
            
            // add their new text to the existing text
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            // make sure the result is under 16 characters
            return updatedText.count <= 10
        }
        return true
    }
    
}

 // MARK: - UI PickerView

extension VerifyEmailAndContactViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        let textField = activeTextField as! EdgemCustomTextField
        countryCodePicker.reloadAllComponents()
        countryCodePicker.reloadInputViews()
        if textField.tag == 1 {
            textField.inputView = countryCodePicker
            countryCodePicker.selectRow(0, inComponent: 0, animated: true)
            activeTextField.text = countryCodeOptions[0]
//            let index = countryCodeOptions.index(of: userRegistration.countryContactCode)
//            if let _index = index {
//                self.countryCodePicker.selectRow(_index, inComponent: 0, animated: true)
//                activeTextField.text = countryCodeOptions[_index]
//            } else {
//                countryCodePicker.selectRow(0, inComponent: 0, animated: true)
//                activeTextField.text = countryCodeOptions[0]
//            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryCodeOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryCodeOptions[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        activeTextField.text = countryCodeOptions[row]
    }
    
    
}

    // MARK: - API Implementations

extension VerifyEmailAndContactViewController {
    
    // [START verify email api]
    func verifyEmail(_ email: String){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let emailObserver = ApiManager.shared.apiService.verifyEmail(email)
        let emailDisposable = emailObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        emailDisposable.disposed(by: disposableBag)
    }
     // [START verify email api]
  
    // [START verify contact api]
    fileprivate func verifyContact(_ contact: String){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.verifyContact(contact)
        let userDisposable = userObserver.subscribe(onNext: {(message) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                if message != "You may continue"{
                     self.showSimpleAlertWithMessage(message)
                }
               
                self.showHideNextButton(true)
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                 self.showHideNextButton(false)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    // [END verify contact api]

     // [START request otp api]
    fileprivate func requestOTP(_ param: [String:AnyObject]){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.requestOTP(param)
        let userDisposable = userObserver.subscribe(onNext: {(message) in
            DispatchQueue.main.async {
               Utilities.hideHUD(forView: self.view)
                
               let otpViewController = OTPViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                
                if self.fromVerifyEmailAndContactVCToVerifyEmail == true{
                    otpViewController.fromVerifyEmailAndContactVCToVerifyEmail = true
                    otpViewController.emailFromVerifyEmailAndContact = self.userRegistration.email
                }else{
                    otpViewController.fromVerifyEmailAndContactVCToVerifyEmail = false
                    otpViewController.countryCode = self.userRegistration.countryContactCode
                    otpViewController.contactNumber = self.userRegistration.contactNumber
                }
   
                otpViewController.userID = AppDelegateConstant.user?.ID
                otpViewController.vc = self
               self.navigationController?.pushViewController(otpViewController, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
     // [END verify contact api]
 
}
