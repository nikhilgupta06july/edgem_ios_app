//
//  SignUpOptionViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 12/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import  FBSDKLoginKit
import RxSwift

import UIKit
import  FBSDKLoginKit
import RxSwift
import GoogleSignIn
import FirebaseAuth

class SignUpOptionViewController: BaseViewController, GIDSignInUIDelegate,GIDSignInDelegate  {
    
    var selectedOption = 0
    var userRegistration = UserRegistration()
    var disposableBag = DisposeBag()
    private var gradient: CAGradientLayer!
    var socialPlateForm: String = ""
    
    @IBOutlet weak var tutorLabel: UILabel!
    @IBOutlet weak var studentLabel: UILabel!
    @IBOutlet weak var gradViewtopConstraint: NSLayoutConstraint!
    @IBOutlet weak var gradView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var signUpButton: EdgemCustomButton!
    @IBOutlet weak var fbSignUpButton: EdgemCustomButton!
    @IBOutlet weak var googleSignUpButton: EdgemCustomButton!
    @IBOutlet weak var studentButton: UIButton!
    @IBOutlet weak var tutorButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        gradViewtopConstraint.constant = UIDevice.current.screenType.rawValue.contains("X") ? 115 : 70
        gradient = CAGradientLayer()
        // configureGradView()
        // Add notification observer for common popup yes/no button
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gradient.frame = gradView.bounds
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scrollView.setContentOffset(.zero, animated: true)
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance()?.delegate = self                                                                                  // ------------> GIDSignInDelegate  Methods
        GIDSignIn.sharedInstance()?.uiDelegate = self
    }
    
    // MARK: - Private Functions
    
    private func configureGradView(){
        gradient = CAGradientLayer()
        gradient.frame = gradView.bounds
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0, 0.5, 0.98, 1]
        gradView.layer.mask = gradient
    }
    
    func showPopUPToSelectUser(){
        DispatchQueue.main.async {
            let commonPopUP = ShowInfoCommonPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            commonPopUP.fromController = self
            self.present(commonPopUP, animated: true, completion: nil)
        }
    
    }
    
    // MARK: -------------------- GIDSignInDelegate  Methods -------------------------
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {         //-----------> this method will be called when user sign in with google
        
        if let err = error {
            print("Failed to log into google: ", err)
            return
        }
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in                                // -----------> Use this user as Firebase user
            if let error = error {
                print("Failed to create a Firebase user with google account:", error)
                return
            }
            guard let email = user.profile.email else{return}
            guard let uid = user.userID else{return}
            guard let firstname = user.profile.givenName else{return}
            guard let lastname = user.profile.familyName else{return}
            self.socialPlateForm = ""
            var loginObject = [
                "email": email,
                "first_name": firstname,
                "last_name": lastname,
                "gl_id":uid
                
                ] as [String : AnyObject]
//            if let url = (user!.profile!.imageURL(withDimension: 200)) {
//                loginObject["url"] = url.absoluteString as AnyObject
//            }
            DispatchQueue.main.async {
                //self.verifyGmail(user.profile.email, gmailDict: loginObject)
                 self.socialPlateForm = "gmail"
                self.verifySocialID(socialID: user!.userID, socialDict: loginObject, email: email)
            }
            
        }
        
        
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result as! [String: Any])
                    if let fbObject = result as? [String: Any] {
                            self.socialPlateForm = ""
                            var loginObject = [
                                "email": fbObject["email"] as? String,
                                "first_name": fbObject["first_name"] as! String,
                                "last_name": fbObject["last_name"] as! String,
                                "fb_id": fbObject["id"] as! String
                                ] as [String : AnyObject]
//                            if let picutureDict = fbObject["picture"] as? [String: AnyObject],
//                                let dataDict = picutureDict["data"] as? [String: AnyObject],
//                                let isSilhouetter = dataDict["is_silhouette"] as? Int, isSilhouetter == 0,
//                                let url = dataDict["url"] as? String {
//                                loginObject["url"] = url as AnyObject
//                            }
                            print(loginObject)
                            DispatchQueue.main.async {
                                //self.checkEmailID(email, fbDict: loginObject)
                                //self.verifyEmail(email, fbDict: loginObject)
                                 self.socialPlateForm = "fb"
                                self.verifySocialID(socialID: fbObject["id"] as! String, socialDict: loginObject, email: fbObject["email"] as? String ?? "" )
                               
                            }
                        
                    } else {
                        self.showSimpleAlertWithMessage("Failed while trying to login with Facebook".localized)
                    }
                }
            })
        }
    }
    
    //MARK:- IBActions
    @IBAction func optionButtonTapped(_ sender: UIButton) {
        studentButton.isSelected = sender.tag == 1
        tutorButton.isSelected = sender.tag == 2
        if studentButton.isSelected{
            UserStore.shared.selectedUserType = "S"
            studentLabel.font = UIFont(name: QuicksandBold, size: 16)
            tutorLabel.font = UIFont(name: QuicksandMedium, size: 16)
        }else{
            UserStore.shared.selectedUserType = "T"
            studentLabel.font = UIFont(name: QuicksandMedium, size: 16)
            tutorLabel.font = UIFont(name: QuicksandBold, size: 16)
        }
    }
    
    @IBAction func createAccountButtonTapped(_ sender: UIButton) {
        if !studentButton.isSelected && !tutorButton.isSelected{
            //showPopUPToSelectUser()
             showSimpleAlertWithMessage(CustomAlertMessages.ShowPopUPSelectUser.description())
        }else{
            let creatUserVC = CreateUserViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            creatUserVC.isStudent = studentButton.isSelected ? true : false
            creatUserVC.userRegistration = self.userRegistration
            self.navigationController?.pushViewController(creatUserVC, animated: true)
        }
       
    }
    
    @IBAction func fbLogInButtonTapped(_ sender: UIButton){
        if !studentButton.isSelected && !tutorButton.isSelected{
            //showPopUPToSelectUser()
            showSimpleAlertWithMessage(CustomAlertMessages.ShowPopUPSelectUser.description())
        }else{
            let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
            fbLoginManager.logOut()
            fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) -> Void in
                if (error == nil){
                    let fbloginresult : FBSDKLoginManagerLoginResult = result!
                    if (result?.isCancelled)!{
                        return
                    }
                    if(fbloginresult.grantedPermissions.contains("email")) {
                        self.getFBUserData()
                    }
                }
            }
        }
    }
    
    @IBAction func gmailLogInButtonTapped(_ sender: UIButton){
        if !studentButton.isSelected && !tutorButton.isSelected{
            //showPopUPToSelectUser()
             showSimpleAlertWithMessage(CustomAlertMessages.ShowPopUPSelectUser.description())
        }else{
            if GIDSignIn.sharedInstance()?.uiDelegate != nil{
                GIDSignIn.sharedInstance()?.signOut()
                GIDSignIn.sharedInstance()?.signIn()
            }else{
                print("GIDSignIn.sharedInstance()?.uiDelegate is NIL")
            }
        }
    }
}

extension SignUpOptionViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        gradient.frame = gradView.bounds
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0, 0.1, 0.98, 1]
        gradView.layer.mask = gradient
    }
}

extension SignUpOptionViewController{
    
    // Verify social id
    
    func verifySocialID(socialID: String, socialDict: [String:AnyObject], email: String){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let socialDataObserver = ApiManager.shared.apiService.verifySocialID(socialID, self.socialPlateForm, email: email)
        let socialDataDisposable = socialDataObserver.subscribe(onNext: { (socialData) in
            print(socialData)
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                if let isExist = socialData.isExist{
                    if isExist == 0{
                        let createUserViewController = CreateUserViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                        createUserViewController.isStudent = self.studentButton.isSelected ? true : false
                        if self.socialPlateForm == "gmail"{
                            if let email = socialDict["email"] as? String{
                                print(email)
                                self.verifyEmail(email, fbDict: socialDict)
                            }else{
                                createUserViewController.gmailDict = socialDict
                                self.navigationController?.pushViewController(createUserViewController, animated: true)
                            }

                        }else if self.socialPlateForm == "fb"{
                            if let email = socialDict["email"] as? String{
                                print(email)
                                self.verifyEmail(email, fbDict: socialDict)
                            }else{
                                 createUserViewController.facebookDict = socialDict
                                 self.navigationController?.pushViewController(createUserViewController, animated: true)
                            }
                        }
                    
                    }else{
                           self.showSimpleAlertWithMessage(socialData.message)
                    }
                }
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                         self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        socialDataDisposable.disposed(by: disposableBag)
        
    }
    
    //  Previous working code, remove after testing
     
    func verifyGmail(_ email: String, gmailDict: [String:AnyObject]){
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let emailObserver = ApiManager.shared.apiService.verifyEmail(email)
        let emailDisposable = emailObserver.subscribe(onNext: {(isAvailable) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                let createUserViewController = CreateUserViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                createUserViewController.isStudent = self.studentButton.isSelected ? true : false
                createUserViewController.gmailDict = gmailDict
                self.navigationController?.pushViewController(createUserViewController, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                       self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        emailDisposable.disposed(by: disposableBag)
        
    }
 
    
    func verifyEmail(_ email: String, fbDict: [String: AnyObject]){
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let emailObserver = ApiManager.shared.apiService.verifyEmail(email)
        let emailDisposable = emailObserver.subscribe(onNext: {(isAvailable) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                let createUserViewController = CreateUserViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                createUserViewController.isStudent = self.studentButton.isSelected ? true : false
                createUserViewController.facebookDict = fbDict
                self.navigationController?.pushViewController(createUserViewController, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        //self.socialLoginWithDict(fbDict)
                         self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        emailDisposable.disposed(by: disposableBag)
    }
    
    
    func socialLoginWithDict(_ dict: [String: AnyObject]) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let loginObserver = ApiManager.shared.apiService.facebookLogInWithDict(dict)
        let loginDisposable = loginObserver.subscribe(onNext: {(user) in
            Utilities.hideHUD(forView: self.view)
            
            // UserStore.shared.userEmail = user.email
            UserStore.shared.isLoggedIn = true
            
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                
                UserStore.shared.isLoggedIn = true
                UserStore.shared.userEmail = user.email
                UserStore.shared.selectedUserType = user.userType
                UserStore.shared.userID = user.ID
                UserStore.shared.token = user.userToken
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.user = user
                
                guard let window = UIApplication.shared.keyWindow else{return}
                guard let rootViewController = window.rootViewController else{return}
                
                if user.userType == "Student"{
                    //let dashboardVC = DashboardViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
                    //self.navigationController?.pushViewController(dashboardVC, animated: true)
                    let tabBarController = BaseTabBarViewController()
                    tabBarController.selectedIndex = 0
                    tabBarController.view.frame = rootViewController.view.frame
                    tabBarController.view.layoutIfNeeded()
                    UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                        window.rootViewController = tabBarController
                    }, completion: nil)
                }else{
                    //                    let dashboardVC = DashboardViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
                    //                    self.navigationController?.pushViewController(dashboardVC, animated: true)
                    let tabBarController = BaseTabBarViewController()
                    tabBarController.selectedIndex = 0
                    tabBarController.view.frame = rootViewController.view.frame
                    tabBarController.view.layoutIfNeeded()
                    UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                        window.rootViewController = tabBarController
                    }, completion: nil)
                }
            }
//            {
//                print(user)
//                self.navigateToDashboardViewController()
//            }
            print(user)
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        loginDisposable.disposed(by: disposableBag)
    }
    

}
