//
//  ResetPasswordViewController.swift
//  Edgem
//
//  Created by Hipster on 26/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class ResetPasswordViewController: BaseViewController {
    
    // MARK: --------------- Properties ------------------
    
    var _labelFields: [String] = ["", "Password*".localized, "Confirm password*".localized]
    var _userRegistration: UserRegistration!
    var _userID: String?
    var disposableBag = DisposeBag()
    var _filledTextField: Bool = false
    var user: User!
    var userEmail:String?
    var password: String!
    
    // MARK: --------------- @IBOutlets ----------------
    
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var updatePassword: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
     @IBOutlet weak var viewTopConstraint: NSLayoutConstraint!
    
    
    // MARK: ----------------- View LifeCycle -----------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTopConstraint.constant = UIDevice.current.screenType.rawValue.contains("X") ? 35 : 0
        // Do any additional setup after loading the view.
        tableViewSetUp()
    }
    
   
    
    // MARK: ------------------ Private Functions ------------------
    
    fileprivate func tableViewSetUp(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    fileprivate func configureTextField(_ customTextField: EdgemCustomTextField){
        
        customTextField.delegate = self
        customTextField.inputView = nil
        customTextField.keyboardType = .asciiCapable
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        customTextField.isSecureTextEntry = true
        
    }
    
    fileprivate func updateDataIntoTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool){
        
        switch customTextField.tag{
        case ResetPasswordTextFieldType.password.rawValue :
            fromCell ? (customTextField.text = _userRegistration.password) : (_userRegistration.password = customTextField.text!)
        case ResetPasswordTextFieldType.confirmPassword.rawValue :
            fromCell ? (customTextField.text = _userRegistration.confirmPassword) : (_userRegistration.confirmPassword = customTextField.text!)
            if customTextField.text != ""{
                nextButton.isUserInteractionEnabled = true
                nextButton.backgroundColor = UIColor.white
            }else{
                nextButton.isUserInteractionEnabled = false
                nextButton.backgroundColor = UIColor(white: 1, alpha: 0.60)
            }
        default:
            break
            
        }
       
//        let check = isTextFieldFilled()
//        if check{
//            nextButton.isUserInteractionEnabled = true
//            nextButton.backgroundColor = UIColor.white
//        }else{
//            nextButton.isUserInteractionEnabled = false
//            nextButton.backgroundColor = UIColor(white: 1, alpha: 0.60)
//        }
        
    }
    
    fileprivate func isDataValid()->(isValid: Bool, message: String){
        var errorMessages: [String] = []
        for index in 1..._labelFields.count{
            let currentIndexPath = IndexPath(row: index, section: 0)
            
            switch index{
            case ResetPasswordTextFieldType.password.rawValue:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? ResetPasswordTableViewCell{
                    cell.validatePassword(text: _userRegistration.password)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
                
            case ResetPasswordTextFieldType.confirmPassword.rawValue:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? ResetPasswordTableViewCell{
                    cell.validateConfirmPassword(text: _userRegistration.password, password: _userRegistration.confirmPassword)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            default:
                break
            }
            
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.reloadData()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }
    
    fileprivate func isTextFieldFilled() -> (Bool){
        for index in 1..._labelFields.count{
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index{
            case ResetPasswordTextFieldType.password.rawValue:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? ResetPasswordTableViewCell{
                    if cell.customTextField.text == "" || _userRegistration.userName == ""{
                        _filledTextField = false
                        return _filledTextField
                    }else{
                        _filledTextField = true
                    }
                }
            case ResetPasswordTextFieldType.confirmPassword.rawValue:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? ResetPasswordTableViewCell{
                    if cell.customTextField.text == "" || _userRegistration.password == ""{
                        _filledTextField = false
                        return _filledTextField
                    }else{
                        _filledTextField = true
                    }
                }
            default:
                break
            }
        }
        
        return _filledTextField
    }
    
    
    
    
}

//MARK: --------------- @IBActions ----------------------------------------

extension ResetPasswordViewController{
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if _userRegistration.password != "" ||
            _userRegistration.confirmPassword != "" {
            let alert = UIAlertController(title: AppName, message: AlertMessage.removeFieldData.description(), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                self.navigateBack(sender: sender)
            }
            
            let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
            alert.addAction(yesAction)
            alert.addAction(noAction)
            self.present(alert, animated: true, completion: nil)
        }else{
            navigateBack(sender: sender)
        }
    }
    
    
    @IBAction func logInButtonTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        let validationInformation = isDataValid()
        guard validationInformation.isValid else {
            self.showSimpleAlertWithMessage(validationInformation.message)
            return
        }
        updatePasswordFunction()
    }
}

// MARK: ----------------TABLE VIEW DELEGATES/DATASOURCES ----------

extension ResetPasswordViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _labelFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else{return
            UITableViewCell()
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ResetPasswordTableViewCell.cellIdentifire(), for: indexPath) as? ResetPasswordTableViewCell else{return UITableViewCell()}
        cell.configureCellForRowAtIndex(indexPath.row, withText: _labelFields[indexPath.row])
        configureTextField(cell.customTextField)
        updateDataIntoTextField(cell.customTextField, fromCell: true)
        configureTextField(cell.customTextField)
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return 0
        }
        return UITableView.automaticDimension
    }
    
    
}

//MARK: -------------- TEXT FIELD DELEGATES -----------------

extension ResetPasswordViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.textColor = theamBlackColor
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField as! EdgemCustomTextField, fromCell: false)
        let currentIndexPath = IndexPath(row: textField.tag, section: 0)
        switch textField.tag {
            
        case ResetPasswordTextFieldType.password.rawValue:
            if let cell = tableView.cellForRow(at: currentIndexPath) as? ResetPasswordTableViewCell{
                cell.validatePassword(text: textField.text!)
            }
            
        case ResetPasswordTextFieldType.confirmPassword.rawValue:
            if let cell = tableView.cellForRow(at: currentIndexPath) as? ResetPasswordTableViewCell{
                cell.validateConfirmPassword(text: textField.text!, password: _userRegistration.password)
            }
        default:
            break
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}

// MARK: ---------------- API Implementation ------------

extension ResetPasswordViewController{
    
    func updatePasswordFunction(){
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
         let userId = _userID
        let password = _userRegistration.password
        let params = ["user_id" : userId, "password" : password] as [String:AnyObject]
        
        let updatePasswordObserver = ApiManager.shared.apiService.updatePassword(params)
        let updatePasswordDisposable = updatePasswordObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            // Saving password in keychain
            
            do {
                // This is a new account, create a new keychain item with the account name.
                let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                        account: self.userEmail ?? "",
                                                        accessGroup: KeychainConfiguration.accessGroup)
                
                // Save the password for the new item.
                try passwordItem.savePassword(self._userRegistration.password)
                UserStore.shared.hasLoginKey = true
            } catch {
                UserStore.shared.hasLoginKey = false
                fatalError("Error updating keychain - \(error)")
            }
            DispatchQueue.main.async {
                let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (alertAction) in
                    self.navigateToLandingScreen()
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
                
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
          updatePasswordDisposable.disposed(by: disposableBag)
    }
}


