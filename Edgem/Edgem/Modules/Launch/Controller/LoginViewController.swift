//
//  LoginViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 01/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//


import UIKit
import RxSwift
import SkyFloatingLabelTextField
import LocalAuthentication
import BiometricAuthentication
import FBSDKLoginKit
import GoogleSignIn
import FirebaseAuth

struct KeychainConfiguration {
    static let serviceName = "TouchMeIn"
    static let accessGroup: String? = nil
}

class LoginViewController: BaseViewController, GIDSignInUIDelegate,GIDSignInDelegate {
   
    var disposableBag = DisposeBag()
    var _errorMessagesArray: [String] = []
    var userRegistration = UserRegistration()
    private var gradient: CAGradientLayer!
    var socialPlateForm: String = ""
    var passwordItems: [KeychainPasswordItem] = []
    
    @IBOutlet weak var gradViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var userNameTextField: EdgemCustomTextField!
    @IBOutlet weak var passCodeTextField: EdgemCustomTextField!
    @IBOutlet weak var userNameErrorLabel: UILabel!
    @IBOutlet weak var userNameErrorLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var loginButton: EdgemCustomButton!
    @IBOutlet weak var fbLoginButton: EdgemCustomButton!
    @IBOutlet weak var googleLoginButton: EdgemCustomButton!
    @IBOutlet weak var gradView: UIView!
    
   // Biometric properties
    @IBOutlet weak var biometricDeviceViewHeight: NSLayoutConstraint!
    @IBOutlet weak var biometricDeviceViewTop: NSLayoutConstraint!
    @IBOutlet weak var biometricDeviceContainerView: UIView!
     @IBOutlet weak var biometricStackView: UIStackView!
    @IBOutlet weak var faceIDBtn: UIButton!
    @IBOutlet weak var touchIDBtn: UIButton!
    @IBOutlet weak var sepratorBwFaceAndTouch: UIImageView!
    @IBOutlet weak var orLabel: UILabel!
    
    @IBOutlet weak var forgotPwdBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        showHideBioMetric()
        scrollView.delegate = self
        gradient = CAGradientLayer()
        gradViewTopConstraint.constant = UIDevice.current.screenType.rawValue.contains("X") ? 115 : 70
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gradient.frame = gradView.bounds
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scrollView.setContentOffset(.zero, animated: true)
        GIDSignIn.sharedInstance()?.delegate = self                                                                                  // ------------> GIDSignInDelegate  Methods
        GIDSignIn.sharedInstance()?.uiDelegate = self
    }

    // MARK:-  Helper Methods
    
     // MARK: -------------------- GIDSignInDelegate  Methods -------------------------
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {         //-----------> this method will be called when user sign in with google
        
        if let err = error {
            print(value: "Failed to log into google: \(err)")
            return
        }
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in                                // -----------> Use this user as Firebase user
            if let error = error {
                print(value: "Failed to create a Firebase user with google account:,\(error)")
                return
            }
            guard let email = user.profile.email else{return}
            guard let uid = user.userID else{return}
            guard let firstname = user.profile.givenName else{return}
            guard let lastname = user.profile.familyName else{return}
           self.socialPlateForm = ""
            var loginObject = [
                "email": email,
                "first_name": firstname,
                "last_name": lastname,
                "gl_id":uid
                
                ] as [String : AnyObject]
            if let url = (user!.profile!.imageURL(withDimension: 200)) {
                loginObject["url"] = url.absoluteString as AnyObject
            }
            self.socialPlateForm = "gmail"
            self.verifySocialID(socialID: user!.userID, socialDict: loginObject, email: email)

        }
        
        
    }
    
    func configureUI() {
        
        forgotPwdBtn.setTitle("FORGOT_PASSWORD".localized, for: .normal)
        
        userNameTextField.placeholder = "Email/Mobile"
        userNameTextField.title = "Email/Mobile"
        userNameTextField.textFont = textFieldMediumFont
        userNameTextField.placeholderFont = textFieldDefaultFont
        userNameTextField.titleFont = textFieldDefaultFont
        userNameTextField.delegate = self
        
        passCodeTextField.placeholder = "Password"
        passCodeTextField.title = "Password"
        passCodeTextField.delegate = self
        
        passCodeTextField.textFont = textFieldDefaultFont
        passCodeTextField.isSecureTextEntry = true
        
        userNameErrorLabelHeight.constant = 0
        passwordErrorLabelHeight.constant = 0
    }
    
    func isDataValid() -> Bool {
        let _username = userNameTextField.text ?? ""
        let _password = passCodeTextField.text ?? ""
        _errorMessagesArray.removeAll()
        if _username.isEmptyString(){
            userNameErrorLabel.text = ValidationErrorMessage.email_username_Empty.description()
            _errorMessagesArray.append(userNameErrorLabel.text ?? "")
        } else {
            userNameErrorLabel.text = ""
        }
        animateEmailError()
        
        if _password.isEmptyString() {
            passwordErrorLabel.text = ValidationErrorMessage.passwordEmpty.description()
            _errorMessagesArray.append(passwordErrorLabel.text ?? "")
        } else {
            passwordErrorLabel.text = ""
        }
        animatePasswordError()
        
        return _errorMessagesArray.isEmpty
    }
    
    func animateEmailError() {
        let text = userNameErrorLabel.text ?? ""
        UIView.animate(withDuration: 0.2) {
            self.userNameErrorLabelHeight.constant = text.isEmpty ? 0 : self.userNameErrorLabel.heightForText()
            self.view.layoutIfNeeded()
        }
    }
    
    func animatePasswordError() {
        let text = passwordErrorLabel.text ?? ""
        UIView.animate(withDuration: 0.2) {
            self.passwordErrorLabelHeight.constant = text.isEmpty ? 0 : self.passwordErrorLabel.heightForText()
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: - Touch ID authentication
    func authenticateUsingTouchID(_ num: Int){
        let authContext = LAContext()
        var authReason = "For Log in"
        var authError: NSError?
        
            if authContext.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &authError){
                
                if #available(iOS 11.0, *) {
                    if num == 1{
                        if authContext.biometryType == LABiometryType.faceID{
                            authReason = "Unlock using Face ID"
                            print("FaceId support")
                        }else{
                            showAlertViewIfNoBiometricSensorHasBeenDetected("This device does not have a Face ID sensor.")
                            return
                        }
                    }else{
                        if(authContext.biometryType == LABiometryType.touchID){
                            authReason = "Unlock using Touch ID"
                            print("TouchId support")
                        }else{
                            showAlertViewIfNoBiometricSensorHasBeenDetected("This device does not have a Touch ID sensor.")
                            return
                        }
                    }
                } else {
                    // Fallback on earlier versions
                }
                authContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: authReason) { (success, error) in
                    if success{
                        DispatchQueue.main.async {
                            // Go to view controller
                            self.navigateToAuthenticatedViewController()
                        }
                    }else{
                        if let error = error{
                            DispatchQueue.main.async {
                                //let message = self.errorMessageForLAErrorCode(error.code)
                                // print(message)
                                //self.showAlertViewAfterEvaluatingPolicyWithMessage(message)
                                let err = self.showError(error: error as! LAError)
                                self.showAlertWithTitle(title: "Edgem", message: err)
                            }
                        }
                    }
                }
            }else{
            // We have error
            print(authError?.localizedDescription)
                showAlertViewIfNoBiometricSensorHasBeenDetected(authError?.localizedDescription ?? "")
            //show the normal login
        }
    }
    
    func navigateToAuthenticatedViewController(){
        self.navigateToDashboardViewController()
//        let dashboardVC = DashboardViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
//        self.navigationController?.pushViewController(dashboardVC, animated: true)
    }
    
    func showAlertViewIfNoBiometricSensorHasBeenDetected(_ message: String){
        
        showAlertWithTitle(title: "Edgem", message: message)
        
    }
    
    func showAlertWithTitle( title:String, message:String ) {
        
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertVC.addAction(okAction)
        DispatchQueue.main.async {
            self.present(alertVC, animated: true, completion: nil)
        }
        
    }
    
    // ---- List of LAError
    func showError(error: LAError)->String {
        var message: String = ""
        switch error.code {
        case LAError.authenticationFailed:
            message = "Authentication was not successful because the user failed to provide valid credentials. Please enter password to login."
            break
        case LAError.userCancel:
            message = "Authentication was canceled by the user"
            break
        case LAError.userFallback:
            message = "Authentication was canceled because the user tapped the fallback button"
            break
        case LAError.touchIDNotEnrolled:
            message = "Authentication could not start because Touch ID has no enrolled fingers."
            break
        case LAError.passcodeNotSet:
            message = "Passcode is not set on the device."
            break
        case LAError.systemCancel:
            message = "Authentication was canceled by system"
            break
        default:
            message = error.localizedDescription
            break
        }
        print(message)
        return message
    }
    
    // ---- get fb user data
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result as! [String: Any])
                    if let fbObject = result as? [String: Any] {
                        if let email = fbObject["email"] as? String {
                             self.socialPlateForm = ""
                            var loginObject = [
                                "email": email,
                                "first_name": fbObject["first_name"] as! String,
                                "last_name": fbObject["last_name"] as! String,
                                "fb_id": fbObject["id"] as! String
                                ] as [String : AnyObject]
                            if let picutureDict = fbObject["picture"] as? [String: AnyObject],
                                let dataDict = picutureDict["data"] as? [String: AnyObject],
                                let isSilhouetter = dataDict["is_silhouette"] as? Int, isSilhouetter == 0,
                                let url = dataDict["url"] as? String {
                                loginObject["url"] = url as AnyObject
                            }
                            print(value: loginObject)
                            DispatchQueue.main.async {
                                self.socialPlateForm = "fb"
                                self.verifySocialID(socialID: fbObject["id"] as! String, socialDict: loginObject, email: email )
                            }
                        } else {
                            self.showSimpleAlertWithMessage("An email ID is not associated with this account.".localized)
                        }
                    } else {
                        self.showSimpleAlertWithMessage("Failed while trying to login with Facebook".localized)
                    }
                }
            })
        }
    }
    
    fileprivate func showHideBioMetric() {
        // TouchID Button
        biometricDeviceContainerView.isHidden = !UserStore.shared.isBioMetricEnabledByUser
        biometricStackView.isHidden = !UserStore.shared.isBioMetricEnabledByUser
        biometricDeviceViewHeight.constant = (UserStore.shared.isBioMetricEnabledByUser == true) ? 125.5 : 0
        biometricDeviceViewTop.constant = (UserStore.shared.isBioMetricEnabledByUser == true) ? 15 : -15
        
        if UserStore.shared.isBioMetricEnabledByUser && BioMetricAuthenticator.canAuthenticate() {
            biometricDeviceContainerView.isHidden = false
            biometricStackView.isHidden = false
            biometricDeviceViewHeight.constant = 125.5
            biometricDeviceViewTop.constant = 15
            if BioMetricAuthenticator.shared.faceIDAvailable() {
              //  touchIDButton.setImage(UIImage(named: "FaceIcon"), for: .normal)
                print("face id  available")
                faceIDBtn.isHidden = false
                touchIDBtn.isHidden = true
                orLabel.isHidden = true
                sepratorBwFaceAndTouch.isHidden = true
            } else {
              //  touchIDButton.setImage(UIImage(named: "Touch-icon-lg"), for: .normal)
                 print("touch id  available")
                faceIDBtn.isHidden = true
                touchIDBtn.isHidden = false
                orLabel.isHidden = true
                sepratorBwFaceAndTouch.isHidden = true
            }
        } else {
            biometricDeviceContainerView.isHidden = true
             biometricStackView.isHidden = true
             biometricDeviceViewHeight.constant =  0
            biometricDeviceViewTop.constant = -15
            
            faceIDBtn.isHidden = true
            touchIDBtn.isHidden = true
            orLabel.isHidden = true
            sepratorBwFaceAndTouch.isHidden = true
        }
    }
    
    //MARK:- IBActions
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton) {
        let verifyPhoneViewController = VerifyPhoneNumberViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        verifyPhoneViewController.userRegistration = userRegistration
        verifyPhoneViewController._fromLogInViewController = true
        self.navigationController?.pushViewController(verifyPhoneViewController, animated: true)
    }
    
    @IBAction func signupButtonTapped(_ sender: UIButton) {
        let signupViewController = SignUpOptionViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        self.navigationController?.pushViewController(signupViewController, animated: true)
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
    
        self.view.endEditing(true)
        guard isDataValid() else {
            self.showSimpleAlertWithMessage(_errorMessagesArray[0])
            return
        }
        self.loginUserWithEmail(userNameTextField.text!, password: passCodeTextField.text!)

    }
    
    @IBAction func touchIDButtonTapped(_ sender: UIButton){
        //authenticateUsingTouchID(sender.tag)
        
        BioMetricAuthenticator.authenticateWithBioMetrics(reason: "", success: {
            guard let email = UserStore.shared.touchIDEmail, !email.isEmpty else {
                return
            }
            
            do {
                let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                        account: email,
                                                        accessGroup: KeychainConfiguration.accessGroup)
                let keychainPassword = try passwordItem.readPassword()
                DispatchQueue.main.async {
                    self.loginUserWithEmail(email, password: keychainPassword)
                }
            } catch {
                //Error
                fatalError("Error reading password from keychain - \(error)")
            }
        }, failure: { [weak self] (error) in
            print("Fail")
            
            // do nothing on canceled
            if error == .canceledByUser || error == .canceledBySystem {
                return
            } else if error == .biometryNotAvailable {
                // device does not support biometric (face id or touch id) authentication
                self?.showSimpleAlertWithMessage(error.message())
            } else if error == .fallback {
                // show alternatives on fallback button clicked
                // here we're entering username and password
                
            } else if error == .biometryNotEnrolled {
                // No biometry enrolled in this device, ask user to register fingerprint or face
                //Settings page
            } else if error == .biometryLockedout {
                // Biometry is locked out now, because there were too many failed attempts.
                // Need to enter device passcode to unlock.
                
                // show passcode authentication
            } else {
                // show error on authentication failed
                self?.showSimpleAlertWithMessage(error.message())
            }
        })
    }
    
    @IBAction func fbLogInButtonTapped(_ sender: UIButton){
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email")) {
                    
                    self.getFBUserData()
                }
            }else{
                print(error)
            }
        }
    }
    
    @IBAction func gmailLogInButtonTapped(_ sender: UIButton){
        //GIDSignIn.sharedInstance()?.signOut()
        if GIDSignIn.sharedInstance()?.uiDelegate != nil{
             GIDSignIn.sharedInstance()?.signOut()
             GIDSignIn.sharedInstance()?.signIn()
        }else{
            print(value: "GIDSignIn.sharedInstance()?.uiDelegate is NIL")
        }
    }
    
}

extension LoginViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
        gradient.frame = gradView.bounds
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0, 0.1, 0.98, 1]
        gradView.layer.mask = gradient
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        gradient.frame = gradView.bounds
//        gradient.colors = [theamAppColor.cgColor, theamAppColor.cgColor, theamAppColor.cgColor, theamAppColor.cgColor]
//        gradient.locations = [0, 0.1, 0.9, 1]
//        gradView.layer.mask = gradient
    }
}
//MARK: - TextField Delegate/Datasource

extension LoginViewController: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            if textField.text!.isEmptyString() {
                userNameErrorLabel.text = ValidationErrorMessage.email_username_Empty.description()
            } else {
                userNameErrorLabel.text = ""
            }
            animateEmailError()
        } else {
            if textField.text!.isEmptyString() {
                passwordErrorLabel.text = ValidationErrorMessage.passwordEmpty.description()
            } else {
                passwordErrorLabel.text = ""
            }
            animatePasswordError()
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        
        switch textField.tag {
        case 1:
            return (textField.text?.count)! < EmailMaxLimit
        case 2:
            return (textField.text?.count)! < PasswordMaxLimit
        default:
            return true
        }
    }
}


//MARK: - API Implementation
extension LoginViewController{
    
    // Verify social id
    func verifySocialID(socialID: String, socialDict: [String:AnyObject], email: String){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let socialDataObserver = ApiManager.shared.apiService.verifySocialID(socialID, self.socialPlateForm, email: email)
        let socialDataDisposable = socialDataObserver.subscribe(onNext: { (socialData) in
            print(socialData)
            
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                
                if UserStore.shared.deviceToken.isEmpty == false {
                    Utilities.sendDeviceToken(UserStore.shared.deviceToken)
                }
                
                if let isExist = socialData.isExist{
                    if isExist == 1{
                        if let user = socialData.data{
                            
                            if let otpVarifiedStatus = user.otpVarificationStatus, otpVarifiedStatus == 0{
                                self.resendOTP(user.ID, user)
                            }else{
                                
                                UserStore.shared.logInType = user.loginType
                                UserStore.shared.isLoggedIn = true
                                UserStore.shared.userEmail = user.email
                                UserStore.shared.selectedUserType = user.userType
                                UserStore.shared.userID = user.ID
                                UserStore.shared.token = user.userToken
                                
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.user = user
                                
                                guard let window = UIApplication.shared.keyWindow else{return}
                                guard let rootViewController = window.rootViewController else{return}
                                if user.userType == "Student"{
                                    let tabBarController = BaseTabBarViewController()
                                    tabBarController.selectedIndex = 0
                                    tabBarController.view.frame = rootViewController.view.frame
                                    tabBarController.view.layoutIfNeeded()
                                    UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                                        window.rootViewController = tabBarController
                                    }, completion: nil)
                                }else{
                                    let tabBarController = BaseTabBarViewController()
                                    tabBarController.selectedIndex = 0
                                    tabBarController.view.frame = rootViewController.view.frame
                                    tabBarController.view.layoutIfNeeded()
                                    UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                                        window.rootViewController = tabBarController
                                    }, completion: nil)
                                }
                            }
                            

                        }
        
                    }else{
                        var msg: String = ""
                        if self.socialPlateForm == "gmail"{
                            msg = AlertMessage.gmailAccountDoesntExist.description()
                        }else if self.socialPlateForm == "fb"{
                            msg = AlertMessage.fbAccountDoesntExist.description()
                        }
                        let alert = UIAlertController(title: AppName,
                                                      message: msg,
                                                      preferredStyle: .alert)
                        let yesAction = UIAlertAction(title: AlertButton.yes.description(),
                                                      style: UIAlertAction.Style.default,
                                                      handler: { (action) in
                                                        DispatchQueue.main.async {
                                                            let signupOptionViewController = SignUpOptionViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                                                            self.navigationController?.pushViewController(signupOptionViewController, animated: true)
                                                        }
                        })
                        let noAction = UIAlertAction(title: AlertButton.no.description(),
                                                     style: .cancel,
                                                     handler: nil)
                        
                        alert.addAction(noAction)
                        alert.addAction(yesAction)
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        socialDataDisposable.disposed(by: disposableBag)
        
    }
    
    func fetchDashboard() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            let dashboardObserver = ApiManager.shared.apiService.fetchDashboardDetails()
            let dashboardDisposable = dashboardObserver.subscribe(onNext: {(dashboardResponseDataObject) in
                DispatchQueue.main.async {
                    let dashBoardVC = DashboardViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                    dashBoardVC.dashboardDetails = dashboardResponseDataObject
                    dashBoardVC.isFromLogin = true
                }
            }, onError: {(error) in
                DispatchQueue.main.async(execute: {
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            })
            dashboardDisposable.disposed(by: self.disposableBag)
        }
        
    }
    
    func fetchLevels(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        //Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
            let levelObserver = ApiManager.shared.apiService.fetchLevels(1)
            let levelDisposable = levelObserver.subscribe(onNext: { (paginationObject) in
                AppDelegateConstant.levelPagination = LevelPagination()
                AppDelegateConstant.levelPagination?.appendDataFromObject(paginationObject)
            }, onError: { (error) in
                DispatchQueue.main.async {
                    //Utilities.hideHUD(forView: self.view)
                }
            })
            return levelDisposable.disposed(by: self.disposableBag)
        }
    }
    
    //---- resend OTP
    func resendOTP(_ userID: Int, _ user: User) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
             let resendObserver = ApiManager.shared.apiService.resendOTPForID(userID)
            
            let resendDisposable = resendObserver.subscribe(onNext: {(successDictionary) in
                DispatchQueue.main.async {
                    Utilities.hideHUD(forView: self.view)
                    let OTPVC = OTPViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                    OTPVC.countryCode = "+65"
                    OTPVC.userID = user.ID
                    OTPVC.contactNumber = user.otpVarificationContact
                    self.navigationController?.pushViewController(OTPVC, animated: true)
                }
            }, onError: {(error) in
                DispatchQueue.main.async(execute: {
                    Utilities.hideHUD(forView: self.view)
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            })
            resendDisposable.disposed(by: self.disposableBag)
            
        }
    }
    
    func loginUserWithEmail(_ email: String, password: String) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let loginObserver = ApiManager.shared.apiService.loginUserWithEmail(email, password: password)
        let loginDisposable = loginObserver.subscribe(onNext: {(user) in
            print(user)
            DispatchQueue.main.async{
                
                 //self.fetchDashboard()
                if let otpVarifiedStatus = user.otpVarificationStatus, otpVarifiedStatus == 0{
                    self.resendOTP(user.ID, user)
                }else{
                    Utilities.hideHUD(forView: self.view)
                    self.fetchLevels()
                    UserStore.shared.logInType = user.loginType
                    UserStore.shared.isLoggedIn = true
                    UserStore.shared.userEmail = email
                    UserStore.shared.selectedUserType = user.userType
                    UserStore.shared.userID = user.ID
                    UserStore.shared.token = user.userToken
                
                    if let subject = user.subjects, subject.count == 0{
                        AppDelegateConstant.shouldShowPopUPForSubjec = true
                    }else{
                        AppDelegateConstant.shouldShowPopUPForSubjec = false
                    }
                
                    if UserStore.shared.deviceToken.isEmpty == false {
                        Utilities.sendDeviceToken(UserStore.shared.deviceToken)
                    }
                
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.user = user
                
                    do {
                    // This is a new account, create a new keychain item with the account name.
                        let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                            account: email,
                                                            accessGroup: KeychainConfiguration.accessGroup)
                    
                    // Save the password for the new item.
                        try passwordItem.savePassword(password)
                        UserStore.shared.hasLoginKey = true
                    
                    } catch {
                        UserStore.shared.hasLoginKey = false
                        fatalError("Error updating keychain - \(error)")
                    }
                
                        guard let window = UIApplication.shared.keyWindow else{return}
                        guard let rootViewController = window.rootViewController else{return}
                
                    if user.userType == "Student"{
                    //let dashboardVC = DashboardViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
                    //self.navigationController?.pushViewController(dashboardVC, animated: true)
                        let tabBarController = BaseTabBarViewController()
                        tabBarController.selectedIndex = 0
                        tabBarController.view.frame = rootViewController.view.frame
                        tabBarController.view.layoutIfNeeded()
                        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                            window.rootViewController = tabBarController
                        }, completion: nil)
                    }else{
//                    let dashboardVC = DashboardViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
//                    self.navigationController?.pushViewController(dashboardVC, animated: true)
                        let tabBarController = BaseTabBarViewController()
                        tabBarController.selectedIndex = 0
                        tabBarController.view.frame = rootViewController.view.frame
                        tabBarController.view.layoutIfNeeded()
                        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                        window.rootViewController = tabBarController
                        }, completion: nil)
                    }
                }
            }
        }, onError: {(error) in
                DispatchQueue.main.async(execute: {
                    Utilities.hideHUD(forView: self.view)
                    if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        loginDisposable.disposed(by: disposableBag)
    }
    
    func verifyGmail(_ email: String, gmailDict: [String:AnyObject]){
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let emailObserver = ApiManager.shared.apiService.verifyEmail(email)
        let emailDisposable = emailObserver.subscribe(onNext: {(isAvailable) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                let alert = UIAlertController(title: AppName,
                                              message: AlertMessage.gmailAccountDoesntExist.description(),
                                              preferredStyle: .alert)
                let yesAction = UIAlertAction(title: AlertButton.yes.description(),
                                              style: UIAlertAction.Style.default,
                                              handler: { (action) in
                                                DispatchQueue.main.async {
                                                    let signupOptionViewController = SignUpOptionViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
//                                                    createUserViewController.isStudent = true
//                                                    createUserViewController.gmailDict = gmailDict
                                                    self.navigationController?.pushViewController(signupOptionViewController, animated: true)
                                                }
                })
                let noAction = UIAlertAction(title: AlertButton.no.description(),
                                             style: .cancel,
                                             handler: nil)
                
                alert.addAction(noAction)
                alert.addAction(yesAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.socialLoginWithDict(gmailDict)
                    }
                }
            })
        })
         emailDisposable.disposed(by: disposableBag)
    }
    
    func verifyEmail(_ email: String, fbDict: [String: AnyObject]){

        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let emailObserver = ApiManager.shared.apiService.verifyEmail(email)
        let emailDisposable = emailObserver.subscribe(onNext: {(isAvailable) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                let alert = UIAlertController(title: AppName,
                                              message: AlertMessage.fbAccountDoesntExist.description(),
                                              preferredStyle: .alert)
                let yesAction = UIAlertAction(title: AlertButton.yes.description(),
                                              style: UIAlertAction.Style.default,
                                              handler: { (action) in
                                                DispatchQueue.main.async {
                                                    let createUserViewController = SignUpOptionViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
//                                                    createUserViewController.isStudent = true
//                                                    createUserViewController.facebookDict = fbDict
                                                    self.navigationController?.pushViewController(createUserViewController, animated: true)
                                                }
                                                
                })
                let noAction = UIAlertAction(title: AlertButton.no.description(),
                                             style: .cancel,
                                             handler: nil)
                
                alert.addAction(noAction)
                alert.addAction(yesAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.socialLoginWithDict(fbDict)
                    }
                }
            })
        })
        emailDisposable.disposed(by: disposableBag)
    }
    
    func socialLoginWithDict(_ dict: [String: AnyObject]) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let loginObserver = ApiManager.shared.apiService.facebookLogInWithDict(dict)
        let loginDisposable = loginObserver.subscribe(onNext: {(user) in
            Utilities.hideHUD(forView: self.view)
            
//            UserStore.shared.userEmail = user.email
//            UserStore.shared.isLoggedIn = true
            
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                
                if let otpVarifiedStatus = user.otpVarificationStatus, otpVarifiedStatus == 0{
                                self.resendOTP(user.ID, user)
                }else{
                    UserStore.shared.logInType = user.loginType
                    UserStore.shared.isLoggedIn = true
                    UserStore.shared.userEmail = user.email
                    UserStore.shared.selectedUserType = user.userType
                    UserStore.shared.userID = user.ID
                    UserStore.shared.token = user.userToken
                    UserStore.shared.hasLoginKey = false
                    
                    if let subject = user.subjects, subject.count == 0{
                        AppDelegateConstant.shouldShowPopUPForSubjec = true
                    }else{
                        AppDelegateConstant.shouldShowPopUPForSubjec = false
                    }
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.user = user
                    
                    guard let window = UIApplication.shared.keyWindow else{return}
                    guard let rootViewController = window.rootViewController else{return}
                    //Utilities.sendDeviceToken(nil)
                    if user.userType == "Student"{
                        //let dashboardVC = DashboardViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
                        //self.navigationController?.pushViewController(dashboardVC, animated: true)
                        let tabBarController = BaseTabBarViewController()
                        tabBarController.selectedIndex = 0
                        tabBarController.view.frame = rootViewController.view.frame
                        tabBarController.view.layoutIfNeeded()
                        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                            window.rootViewController = tabBarController
                        }, completion: nil)
                    }else{
                        //                    let dashboardVC = DashboardViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
                        //                    self.navigationController?.pushViewController(dashboardVC, animated: true)
                        let tabBarController = BaseTabBarViewController()
                        tabBarController.selectedIndex = 0
                        tabBarController.view.frame = rootViewController.view.frame
                        tabBarController.view.layoutIfNeeded()
                        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
                            window.rootViewController = tabBarController
                        }, completion: nil)
                    }
                }
                

            }
//                {
//               // Utilities.sendDeviceToken(nil)
//
//                //let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                //appDelegate.user = user
//                self.navigateToDashboardViewController()
////                let dashboardVC = DashboardViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
//           //     self.navigationController?.pushViewController(dashboardVC, animated: true)
//
//            }
            print(user)
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        loginDisposable.disposed(by: disposableBag)
    }
}


