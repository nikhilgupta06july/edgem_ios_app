//
//  ShowPopUpForChildMessageViewController.swift
//  Edgem
//
//  Created by Hipster on 04/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit

class ShowPopUpForChildMessageViewController: BaseViewController {
    
    
    //MARK: - Properties
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var BGViewConfiguration: UIView!
    
    // MARK: - Variables
    var userRegistration: UserRegistration!
    
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        okBtnConfiguration()
        bgViewConfiguration()
        
    }
    
    //MARK: - Private Functions
    private func okBtnConfiguration(){
        okBtn.backgroundColor = theamAppColor
        
    }
    
    private func bgViewConfiguration(){
        // --- implement shadow for BackGround
    }
    
    //MARK: - Actions
    @IBAction func okBtnTapped(_ sender: UIButton) {
        self.dismissPopUp()
        let selectLocationVC = CreateStudentAccountStepTwoViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        selectLocationVC.userRegistration = self.userRegistration
        UIApplication.topViewController()?.present(selectLocationVC, animated: true, completion: nil)
       // self.present(selectLocationVC, animated: true, completion: nil)
        // self.navigationController?.pushViewController(selectLocationVC, animated: true)
    }
}
