//
//  CongratulationViewController.swift
//  Edgem
//
//  Created by Hipster on 01/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class CongratulationViewController: BaseViewController {
    
    
    //MARK: - @IBOutlets
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!

    
    //MARK: - Variables
    
    var user: User!
    var password: String!
    var disposableBag = DisposeBag()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if user.userType == "Student"  {
            //"Congratulations \n approval. Please upload \n registration!"
            descriptionLabel.text = "Congratulations \n on your successful \n registration!"
            okBtn.isHidden = true
            Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(navigateToLogInScreen), userInfo: nil, repeats: false)
        }else if(user.userType == "Parent"){
            descriptionLabel.text = "Congratulations \n on your successful \n registration!"
            okBtn.isHidden = true
            Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(navigateToLogInScreen), userInfo: nil, repeats: false)
        } else{
            //okBtn.isHidden = true
            descriptionLabel.text = "Your account is pending approval. \n Please upload your certificates  \n for verification."
            okBtn.isHidden = false
            fetchLevels()
            //Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(navigateToLogInScreen), userInfo: nil, repeats: false)
        }
        
        // Saving password in keychain
        if password != nil && !password.isEmptyString(){
            let email = user.email!
            UserStore.shared.userEmail = email
            do {
                // This is a new account, create a new keychain item with the account name.
                let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                        account: email,
                                                        accessGroup: KeychainConfiguration.accessGroup)
                
                // Save the password for the new item.
                try passwordItem.savePassword(password)
                UserStore.shared.hasLoginKey = true
            } catch {
                UserStore.shared.hasLoginKey = false
                fatalError("Error updating keychain - \(error)")
            }
        }
    }
    
    //MARK: Private Methods
    
    @objc func navigateToLogInScreen(){
        self.navigateToDashboardViewController()
        
    }
    
    
    //MARK: - Actions
    
    @IBAction func okBtnTapped(_ sender: UIButton) {
//        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(navigateToLogInScreen), userInfo: nil, repeats: false)
        self.navigateToDashboardViewController()
    }
    
}

extension CongratulationViewController{
    
    func fetchLevels(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        //Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
            let levelObserver = ApiManager.shared.apiService.fetchLevels(1)
            let levelDisposable = levelObserver.subscribe(onNext: { (paginationObject) in
                AppDelegateConstant.levelPagination = LevelPagination()
                AppDelegateConstant.levelPagination?.appendDataFromObject(paginationObject)
            }, onError: { (error) in
                DispatchQueue.main.async {
                    //Utilities.hideHUD(forView: self.view)
                }
            })
            return levelDisposable.disposed(by: self.disposableBag)
        }
    }
}
