//
//  SignUpViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 13/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class SignUpViewController: BaseViewController {

    let labelFields = ["","First Name*".localized, "Last Name*".localized, "Gender*".localized, "Birthday*".localized, "Email Address*".localized ]
     var _filledTextField: Bool = false
    var userRegistration: UserRegistration!
     let genderPickerView = UIPickerView()
    let datePicker = UIDatePicker()
     var activeTextField: UITextField!
    var disposableBag = DisposeBag()
    private var gradient: CAGradientLayer!
    
    @IBOutlet weak var viewTopConstraints: NSLayoutConstraint!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    //@IBOutlet weak var titleLabel: UILabel!
    //@IBOutlet weak var profileImageView: UIImageView!
//    @IBOutlet weak var uploadImageButton: UIButton!{
//        didSet{
//        uploadImageButton.isHidden = userRegistration.userType == "student"
//        //uploadImageButton.backgroundColor = UIColor.white
////            uploadImageButton.setImage(UIImage(named : "unselectedImage"), forState: UIControl.State.Normal)
////            uploadImageButton.setImage(UIImage(named : "selectedImage"), forState: UIControl.State.Selected)
//            uploadImageButton.layer.cornerRadius = 20
//            uploadImageButton.addTarget(self, action: #selector(showImageOptions), for: .touchUpInside)
//
//        }
//    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradient()
        
         viewTopConstraints.constant = UIDevice.current.screenType.rawValue.contains("X") ? 35 : 0
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        genderPickerView.tag = 2
        
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
//        if userRegistration.fbID != "" {
//            self.userRegistration.displayName = self.userRegistration.firstName.lowercased().trimmingCharacters(in: .whitespaces) + userRegistration.lastName.lowercased().trimmingCharacters(in: .whitespaces)
//            getProfileLinkURLForName(self.userRegistration.displayName)
//        }
        
        // Add notification observer for common popup yes/no button
        
        NotificationCenter.default.addObserver(self, selector: #selector(yesButtonTapped), name: .yesBtnTapped, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //updategradientFrame()
        gradient.frame = tableView.superview?.bounds ?? .null
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       print("USER DATA: \(userRegistration)")
    }

    //MARK:- IBActions
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: AppName, message: AlertMessage.exitSignupProcess.description(), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func nextButtonTapped(_ sender: EdgemCustomButton) {
        self.view.endEditing(true)
        // check for image
  
        if userRegistration.imageURL == ""{
            print("upload image")
            self.showSimpleAlertWithMessage("Please upload image")
            return
        }
        let validationInformation = isDataValid()
        guard validationInformation.isValid else {
            self.showSimpleAlertWithMessage(validationInformation.message)
            return
        }
//        guard isImageSelected else {
//            self.showSimpleAlertWithMessage("Please select profile picture to continue.".localized)
//            return
//        }
        UserStore.shared.isParrent = false
        let selectLocationVC = SelectLocationViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        selectLocationVC.userRegistration = self.userRegistration
        self.navigationController?.pushViewController(selectLocationVC, animated: true)
    }

    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if userRegistration.firstName != "" ||
            userRegistration.lastName != "" ||
            userRegistration.email != "" ||
            userRegistration.gender != "" ||
            userRegistration.birthday != "" {
            let alert = UIAlertController(title: AppName, message: AlertMessage.removeFieldData.description(), preferredStyle: .alert)
            let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default) { (action) in
                self.navigateBack(sender: sender)
            }
            
            let noAction =  UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
            alert.addAction(yesAction)
            alert.addAction(noAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            navigateBack(sender: sender)
        }
    }

    //MARK:- Helping Function
    
    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
        return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
    }
    
    @objc func yesButtonTapped(){
        print("yes button tapped")
        self.navigationController?.popViewController(animated: true)
    }
    
    func nextButtonVisible(){
        self.nextButton.isUserInteractionEnabled = true
        self.nextButton.backgroundColor = UIColor.white
    }
    
    func nextButtonInvisible(){
        self.nextButton.isUserInteractionEnabled = false
        self.nextButton.backgroundColor = UIColor(white: 1, alpha: 0.60)
    }
    
    func setGradient(){
        gradient = CAGradientLayer()
        gradient.frame = (tableView.superview?.frame) ?? .null
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0.0, 0.05, 0.98, 1.0]
        tableView.backgroundColor = .clear
        tableView.superview?.layer.mask = gradient
    }
    
    func configureTextField(_ customTextField: EdgemCustomTextField) {
        
        customTextField.delegate = self
        customTextField.isSecureTextEntry = false
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        customTextField.inputView = nil
        switch customTextField.tag {
        case CreateAccountTutorTextFieldType.firstName.rawValue:
            customTextField.keyboardType = .asciiCapable
             customTextField.autocapitalizationType = .words
             customTextField.hideImage()
            customTextField.tintColor = UIColor.blue
            //            if firstNameTick == true {
            //                customTextField.showImage()
            //                firstNameTick = false
        //            }
        case CreateAccountTutorTextFieldType.lastName.rawValue:
            customTextField.keyboardType = .asciiCapable
             customTextField.autocapitalizationType = .words
            customTextField.hideImage()
             customTextField.tintColor = UIColor.blue
            //            if lastNameTick == true {
            //                customTextField.showImage()
            //                lastNameTick = false
        //            }
        case CreateAccountTutorTextFieldType.gender.rawValue:
            customTextField.icon = "drop-down-arrow"
            customTextField.showImage()
              customTextField.inputView = genderPickerView
            customTextField.tintColor = UIColor.clear
            
        case CreateAccountTutorTextFieldType.birthday.rawValue:
            //customTextField.icon = "drop-down-arrow"
            //customTextField.showImage()
            customTextField.inputView = datePicker
            customTextField.tintColor = UIColor.clear
            
        case CreateAccountTutorTextFieldType.emailAddress.rawValue:
            customTextField.keyboardType = .emailAddress
             customTextField.hideImage()
             customTextField.tintColor = UIColor.blue
        default:
            customTextField.keyboardType = .asciiCapable
        }

    }
    

    func showHideNextButton(_ status: Bool) {
        if status == false{
            nextButton.isUserInteractionEnabled = false
            nextButton.backgroundColor = UIColor(white: 1, alpha: 0.60)
        }else{
            nextButton.isUserInteractionEnabled = true
            nextButton.backgroundColor = UIColor.white
        }
    }

    func updateDataIntoTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
        switch customTextField.tag {
        case CreateAccountTutorTextFieldType.firstName.rawValue:
            fromCell ? (customTextField.text = userRegistration.firstName) : (userRegistration.firstName = customTextField.text!)
            break
        case CreateAccountTutorTextFieldType.lastName.rawValue:
            fromCell ? (customTextField.text = userRegistration.lastName) : (userRegistration.lastName = customTextField.text!)
            break
        case CreateAccountTutorTextFieldType.gender.rawValue:
            fromCell ? (customTextField.text = userRegistration.gender) : (userRegistration.gender = customTextField.text!)
            break
        case CreateAccountTutorTextFieldType.birthday.rawValue:
            fromCell ? (customTextField.text = userRegistration.birthday) : (userRegistration.birthday = customTextField.text!)
            break
        case CreateAccountTutorTextFieldType.emailAddress.rawValue:
            fromCell ? (customTextField.text = userRegistration.email) : (userRegistration.email = customTextField.text!)
            break
        default:
            break
        }
        
        if  userRegistration.firstName == "" || userRegistration.lastName == "" || userRegistration.gender == "" || userRegistration.birthday == "" || userRegistration.email == ""{
            showHideNextButton(false)
            if fromCell == false{
                tableView.beginUpdates()
                tableView.endUpdates()
            }
        }

    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        //activeTextField.text = Utilities.shared.getFormattedDate(sender.date, dateFormat: "dd-MMM-yyyy")
        activeTextField.text = Utilities.getFormattedDate(sender.date, dateFormat: "dd MMMM yyyy")
    }

    func isDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        for index in 1...labelFields.count {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case CreateAccountTutorTextFieldType.firstName.rawValue:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateFirstName(text: userRegistration.firstName)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case CreateAccountTutorTextFieldType.lastName.rawValue:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateLastName(text: userRegistration.lastName)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case CreateAccountTutorTextFieldType.gender.rawValue:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateGenderName(text: userRegistration.gender)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case CreateAccountTutorTextFieldType.birthday.rawValue:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateBirthday(text: userRegistration.birthday)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case CreateAccountTutorTextFieldType.emailAddress.rawValue:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateEmail(text: userRegistration.email)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            default:
                break
            }
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.reloadData()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }
    
    
   // MARK: --------------- Add Image -------------
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        let indexPath = IndexPath(row: 0, section: 0)
        if let cell = tableView.cellForRow(at: indexPath) as? TutorProfileImageTableViewCell{
            cell.profileImage.image = image
            userRegistration.tutorImage = image
            if #available(iOS 11.0, *) {
                userRegistration.imageURL = "\(String(describing: info[UIImagePickerController.InfoKey.imageURL]))"
            } else {
                 userRegistration.imageURL = "\(String(describing: info[UIImagePickerController.InfoKey.referenceURL]))"
            }
            cell.addImageButton.setImage(UIImage(named : "editProfileImage"), for: .normal)
            tableView.reloadData()
        }
        dismiss(animated:true, completion: nil)
        
    }

}


extension SignUpViewController: TutorProfileImageTableViewDelegate{
    func addImageButtonTapped() {
        showImageOptions()
    }
}

extension SignUpViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        let textField = activeTextField as! EdgemCustomTextField
        genderPickerView.reloadAllComponents()
        genderPickerView.reloadInputViews()
            textField.inputView = genderPickerView
            let index = genderOptions.index(of: userRegistration.gender)
            if let _index = index {
                self.genderPickerView.selectRow(_index, inComponent: 0, animated: true)
            } else {
                genderPickerView.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = genderOptions[0]
            }
//        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        if pickerView == titlePickerView {
//            return titleOptions.count
//        } else {
            return genderOptions.count
//        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        if pickerView == titlePickerView {
//            return titleOptions[row]
//        } else {
            return genderOptions[row]
//        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        if pickerView == titlePickerView {
//            activeTextField.text = titleOptions[row]
//        } else {
            activeTextField.text = genderOptions[row]
//        }
    }
    
}

extension SignUpViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TutorProfileImageTableViewCell.cellIdentifire(), for: indexPath) as? TutorProfileImageTableViewCell else{return UITableViewCell()}
            cell.tutorProfileImageTableViewDelegate = self
              cell.selectionStyle = .none
            return cell
            
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: EdgemTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemTextFieldTableViewCell
                cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
                updateDataIntoTextField(cell.customTextField, fromCell: true)
                configureTextField(cell.customTextField)
        cell.selectionStyle = .none
        return cell
        
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return TutorProfileImageTableViewCell.cellHeight()
        }
        return UITableView.automaticDimension
    }
}

//extension SignUpViewController: EdgemTextFieldTableViewCellDelegate {

//    func valueChangedForTextField(_ textField: TueetorTextField) {
//    }
    
//}

extension SignUpViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! EdgemCustomTextField
        if activeTextField.tag == CreateAccountTutorTextFieldType.gender.rawValue{
             managePickerView()
        }
    }
    

    func textFieldDidEndEditing(_ textField: UITextField) {
        
        updateDataIntoTextField(textField as! EdgemCustomTextField, fromCell: false)

        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        switch textField.tag {
        case CreateAccountTutorTextFieldType.firstName.rawValue:
            guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else{return}
            cell.validateFirstName(text: textField.text ?? "")
        case CreateAccountTutorTextFieldType.lastName.rawValue:
            guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else{return}
            cell.validateLastName(text: textField.text ?? "")
        case CreateAccountTutorTextFieldType.gender.rawValue:
            guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else{return}
            cell.validateGenderName(text: textField.text ?? "")
        case CreateAccountTutorTextFieldType.birthday.rawValue:
            guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else{return}
            cell.validateBirthday(text: textField.text ?? "")
        case CreateAccountTutorTextFieldType.emailAddress.rawValue:
            guard let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell else{return}
            cell.validateEmail(text: textField.text ?? "")
            if cell.errorLabel.text?.count == 0, !userRegistration.email.isEmpty {
                verifyEmail(userRegistration.email)
            }
            
        default:
            break
        }
        
        if userRegistration.firstName != "" && userRegistration.lastName != "" && userRegistration.gender != "" && userRegistration.birthday != "" && userRegistration.email != "" {
            showHideNextButton(isDataValid().isValid)
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()

    }
    }


//MARK: - API Implementation

extension SignUpViewController{
    
    func verifyEmail(_ email: String){

        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let emailObserver = ApiManager.shared.apiService.verifyEmail(email)
        let emailDisposable = emailObserver.subscribe(onNext: {(message) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                
            }

        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        //let cell = self.tableView.cellForRow(at: IndexPath.init(row: CreateAccountTutorTextFieldType.emailAddress.rawValue, section: 0) ) as! EdgemTextFieldTableViewCell
                        if let cell = self.tableView.cellForRow(at: IndexPath.init(row: CreateAccountTutorTextFieldType.emailAddress.rawValue, section: 0) ) as? EdgemTextFieldTableViewCell{
                            cell.errorLabel.text = error.localizedDescription
                            cell.customTextField.hideImage()
                            cell.customTextField.becomeFirstResponder()
                            self.tableView.beginUpdates()
                            self.tableView.endUpdates()
                        }
                       // self.showSimpleAlertWithMessage(error.localizedDescription)
                        let alert = UIAlertController(title: AppName,
                                                      message: (error.localizedDescription).localized,
                                                      preferredStyle: .alert)
                        let yesAction = UIAlertAction(title: AlertButton.yes.description(),
                                                      style: UIAlertAction.Style.default,
                                                      handler: { (action) in
                                                        let indexPath = IndexPath(row: 5, section: 0)
                                                        guard  let cell = self.tableView.cellForRow(at: indexPath) as? EdgemTextFieldTableViewCell else{return}
                                                        self.nextButton.isUserInteractionEnabled = false
                                                        self.nextButton.backgroundColor = UIColor(white: 1, alpha: 0.60)
                                                        cell.customTextField.becomeFirstResponder()
                        })
                        alert.addAction(yesAction)
                        self.present(alert, animated: true, completion: nil)
                        
//                        let commonPopUP = ShowInfoCommonPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
//                        commonPopUP.fromController = self
//                        commonPopUP.customMsg =  error.localizedDescription.localized
//                       
//                        self.nextButton.isUserInteractionEnabled = false
//                        self.nextButton.backgroundColor = UIColor(white: 1, alpha: 0.60)
//                        self.present(commonPopUP, animated: true, completion: nil)
                    }
                }
            })
        })
        emailDisposable.disposed(by: disposableBag)
    }
}


