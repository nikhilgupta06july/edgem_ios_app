//
//  NoStudentTutorViewController.swift
//  Edgem
//
//  Created by Hipster on 11/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class NoStudentTutorViewController: BaseViewController {
    
    // MARK: - Properties
    
    var userType: String?
    var tutorStatusLabelText = ""
    var studentStatusLabelText = ""
    var isFrom = ""

    // MARK: - @IBOutlets
    
    @IBOutlet weak var tryAgainBtnHolderView: UIView!
    @IBOutlet weak var cancleBtnHolderView: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var tryAgain: UIButton!
    @IBOutlet weak var cancleButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    
    // MARK: - View LifeCycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUP()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // cancle button properties
        
        cancleButton.layer.cornerRadius = 5
        cancleButton.layer.borderWidth = 1
        cancleButton.layer.borderColor = theamGrayColor.cgColor
        cancleButton.clipsToBounds = true
        
        cancleButton.setTitle("Cancel".localized, for: .normal)
        cancleButton.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        cancleButton.titleLabel?.textColor = theamBlackColor
        
        
        
        // cancle button shadow properties
        cancleBtnHolderView.layer.shadowColor = UIColor.black.cgColor
        cancleBtnHolderView.layer.shadowOpacity = 0.12
        cancleBtnHolderView.layer.shadowRadius = 4
        cancleBtnHolderView.layer.shadowOffset = .zero
        cancleBtnHolderView.layer.shadowPath = UIBezierPath(rect: cancleBtnHolderView.bounds).cgPath
    
        // apply button attributes
        tryAgain.layer.cornerRadius = 5
        //tryAgain.layer.borderWidth = 1
        //tryAgain.layer.borderColor = theamGrayColor.cgColor
        tryAgain.backgroundColor = buttonBackGroundColor
        tryAgain.clipsToBounds = true
        
        tryAgain.setTitle("Try Again".localized, for: .normal)
        tryAgain.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        tryAgain.titleLabel?.textColor = theamBlackColor
        
        
        // apply button shadow
        tryAgainBtnHolderView.layer.shadowColor = UIColor.black.cgColor
        tryAgainBtnHolderView.layer.shadowOpacity = 0.12
        tryAgainBtnHolderView.layer.shadowRadius = 4
        tryAgainBtnHolderView.layer.shadowOffset = .zero
        tryAgainBtnHolderView.layer.shadowPath = UIBezierPath(rect: tryAgainBtnHolderView.bounds).cgPath
        
    }
    
    // MARK: - Private Functions
    private func initialSetUP(){
        icon.image = UIImage(named: userType == "Tutor" ? "tutorIcon" : "studentIcon")
        //statusLabel.text = userType == "Tutor" ? "No tutors found" : "No students found"
        statusLabel.text = userType == "Tutor" ? tutorStatusLabelText : studentStatusLabelText
    }
    
    
     // MARK: - @IBActions
    
    @IBAction func cancleBtunTapped(_ sender: UIButton) {
        self.navigateToDashboardViewController()
    }
    
    @IBAction func tryAgainBtnTapped(_ sender: UIButton) {
        if isFrom == "UsersBookMarksListingViewController"{
            let count = self.navigationController?.viewControllers.count
            if let count = count{
                self.navigationController?.popToViewController((self.navigationController?.viewControllers[count-3])!, animated: true)
            }
        }else{
              self.navigationController?.popToRootViewController(animated: true)
        }
      
    }
    
}
