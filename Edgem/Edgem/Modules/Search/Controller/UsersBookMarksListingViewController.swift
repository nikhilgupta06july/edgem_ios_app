//
//  UsersBookMarksListingViewController.swift
//  Edgem
//
//  Created by Hipster on 21/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

//protocol UsersBookMarksListingViewControllerDelegate{
//    func backButtonTapped()
//}

class UsersBookMarksListingViewController: BaseViewController {
    
    
    // MARK: --------- Properties
    
    var rowCount = 0
    var titleVC = ""
    var userType: String!
    var disposableBag = DisposeBag()
    
    var bookMarkedTutorListing = [Tutor]()
    var bookMarkedStudentListing = [Student]()
    var pagination = Pagination.init()
    
     // MARK: --------- @IBOutlets
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    
    // MARK: --------- View LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        userType = UserStore.shared.selectedUserType
        titleLabel.text = titleVC
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegateConstant.idList?.removeAll()
        if userType == tutorType{
            getBookMarkedStudentListing()
        }else{
            getBookMarkedTutorListing()
        }
    }
    
    // MARK: --------- @IBActions
    
    @IBAction func boookMarksButtonTapped(_ sender: UIButton) {
        
    }
    
     // MARK: --------- Private Functions
}

 // MARK: --------- TableView Delegates/Datasource

extension UsersBookMarksListingViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if userType == tutorType{
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: StudentListTableViewCell.cellIdentifire(), for: indexPath) as? StudentListTableViewCell else{
                fatalError("could not load TutorlistTableViewCell")}
            cell.configureCell(with: bookMarkedStudentListing[indexPath.row], indexPath: indexPath)
            cell.studentListTableViewCellDelegate = self
            return cell
            
        }else{
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TutorlistTableViewCell.cellIdentifire(), for: indexPath) as? TutorlistTableViewCell else{
                fatalError("could not load TutorlistTableViewCell")
            }
            cell.tutorlistTableViewCellDelegate = self
            cell.configureCell(with: bookMarkedTutorListing[indexPath.row], indexPath: indexPath)
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if userType == tutorType{
            if indexPath.row == bookMarkedStudentListing.count-1 && pagination.currentPage != pagination.lastPage{
                getBookMarkedStudentListing()
            }
            
        }else{
            if indexPath.row == bookMarkedTutorListing.count-1 && pagination.currentPage != pagination.lastPage{
                getBookMarkedTutorListing()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if userType == tutorType{
            let studentCompleteProfileViewController = StudentCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            studentCompleteProfileViewController.studentID = self.bookMarkedStudentListing[indexPath.row].studentID
            self.navigationController?.pushViewController(studentCompleteProfileViewController, animated: true)
        }else{
            let tutorCompleteProfileViewController = TutorCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            tutorCompleteProfileViewController.tutorID = self.bookMarkedTutorListing[indexPath.row].tutorID
            tutorCompleteProfileViewController.tutor =  self.bookMarkedTutorListing[indexPath.row]
            self.navigationController?.pushViewController(tutorCompleteProfileViewController, animated: true)
        }
        
    }

}


// MARK:  ------------ TutorListTableViewCell Delegate, StudentListTableViewDelegate

extension UsersBookMarksListingViewController: TutorlistTableViewCellDelegate, StudentListTableViewCellDelegate{
    
    func studentBookmarksBtnTapped(_ status: Bool, index: Int) {
        
        guard status else {
            removeBookMarks(bookMarkedStudentListing[index].studentID, index: index)
            return
        }
        return
    }
    
    func bookmarksBtnTapped(_ status: Bool, index: Int) {
        guard status else {
            removeBookMarks(bookMarkedTutorListing[index].tutorID, index: index)
            return
        }
        return
        
    }
    
}

// MARK:  ------------ API Implementation

extension UsersBookMarksListingViewController{
    
    // Remove bookmarks
    func removeBookMarks(_ ID: Int, index: Int){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else{return}
        let param = [  "user_id"                   :    userID,
                       "bookmarked_user" :    ID ] as [String : AnyObject]
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let addBookMarksObserver = ApiManager.shared.apiService.removeBookmarks(param)
        let addBookMarksDisposable = addBookMarksObserver.subscribe(onNext: { (message) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                AppDelegateConstant.idList?.append(ID)
                if self.userType == tutorType{
                    self.bookMarkedStudentListing.remove(at: index)
                    self.rowCount = self.bookMarkedStudentListing.count
                    if self.rowCount > 0 {
                        self.tableView.reloadData()
                    }else{
                        let noTutorStudentVC = NoStudentTutorViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
                        noTutorStudentVC.userType = "Student"
                        noTutorStudentVC.studentStatusLabelText = "No bookmarks found"
                        noTutorStudentVC.isFrom = "UsersBookMarksListingViewController"
                        self.navigationController?.pushViewController(noTutorStudentVC, animated: true)
                        
                    }
                }else{
                    self.bookMarkedTutorListing.remove(at: index)
                    self.rowCount = self.bookMarkedTutorListing.count
                    if self.rowCount > 0 {
                        self.tableView.reloadData()
                    }else{
                        
                        let noTutorStudentVC = NoStudentTutorViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
                        noTutorStudentVC.userType = "Tutor"
                        noTutorStudentVC.tutorStatusLabelText = "No bookmarks found"
                        noTutorStudentVC.isFrom = "UsersBookMarksListingViewController"
                        self.navigationController?.pushViewController(noTutorStudentVC, animated: true)
                    
                    }
                }

                
                
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
            }
        })
        return addBookMarksDisposable.disposed(by: disposableBag)
        
    }
    
    // Bookmarked Tutor
    func getBookMarkedTutorListing() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
       
        let getBookMarksObserver = ApiManager.shared.apiService.getBookmarkedTutor(String(pagination.currentPage+1))
        let getBookMarksDisposable = getBookMarksObserver.subscribe(onNext: { (tutorPagination) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                
                self.pagination = tutorPagination.pagination
                self.bookMarkedTutorListing += tutorPagination.tutor
                
                if self.bookMarkedTutorListing.count > 0{
                    self.rowCount = self.bookMarkedTutorListing.count
                    self.tableView.reloadData()
                }else{
                    let noTutorStudentVC = NoStudentTutorViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
                    noTutorStudentVC.userType = "Tutor"
                    noTutorStudentVC.tutorStatusLabelText = "No bookmarks found"
                    noTutorStudentVC.isFrom = "UsersBookMarksListingViewController"
                    self.navigationController?.pushViewController(noTutorStudentVC, animated: true)

                }
            }
        }, onError: { (error) in
            Utilities.hideHUD(forView: self.view)
        })
        return getBookMarksDisposable.disposed(by: disposableBag)
    }
    
    
    // Bookmarked Student
    func getBookMarkedStudentListing(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let getBookMarksObserver = ApiManager.shared.apiService.getBookmarkedStudent(String(pagination.currentPage+1))
        let getBookMarksDisposable = getBookMarksObserver.subscribe(onNext: { (studentPagination) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                self.pagination = studentPagination.pagination
                self.bookMarkedStudentListing += studentPagination.student
                
                if self.bookMarkedStudentListing.count > 0{
                    self.rowCount = self.bookMarkedStudentListing.count
                    self.tableView.reloadData()
                }else{

                    let noTutorStudentVC = NoStudentTutorViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
                    noTutorStudentVC.userType = "Student"
                    noTutorStudentVC.studentStatusLabelText = "No bookmarks found"
                    noTutorStudentVC.isFrom = "UsersBookMarksListingViewController"
                self.navigationController?.pushViewController(noTutorStudentVC, animated: true)

                }
                
            }
        }, onError: { (error) in
            Utilities.hideHUD(forView: self.view)
        })
        return getBookMarksDisposable.disposed(by: disposableBag)
    }
    
}
