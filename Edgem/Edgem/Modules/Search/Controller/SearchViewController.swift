//
//  SearchViewController.swift
//  Edgem
//
//  Created by Hipster on 27/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import RxSwift
import GooglePlaces

class SearchViewController: BaseViewController {
        
    //MARK: -------------- Properties --------------
    var disposableBag = DisposeBag()
    var rowCount = 0
    var numberOfRows = 2
    var sortBy = ""
    var filterParameters = FilterParams()
    var user: User!
    var userAddresses = [UserAddress]()
    var status = false
    var selectedIndex = 1
    
    //MARK: -------------- @IBOutlet ---------------
    
    @IBOutlet weak var controllerTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    //MARK: -------------- View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        controllerTitleLabel.font = UIFont(name: QuicksandMedium, size: 18)
        controllerTitleLabel.textColor = theamBlackColor
        
        // register cell for tutor address
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let user = appDelegate.user{
                if let userAddress = user.userAddress{
                if userAddress.count > 0{
                    for index in 0...userAddress.count{
                        if userAddress.indices.contains(index){
                        self.userAddresses.append(userAddress[index])
                        }else{
                            let tempUserAddress = UserAddress.init()
                            tempUserAddress.addressLabel = "Others"
                            self.userAddresses.append(tempUserAddress)
                        }
                    }
                    numberOfRows = 1 + userAddresses.count
                }
            }
        }
        
        registerCell()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self,
                                                                           selector: #selector(filterAvailabilityTapped), name: .filterAvailabilitySelected,
                                                                           object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserStore.shared.selectedUserType == "Tutor"{
            rowCount = 5
        }else{
            rowCount = 8
        }
        
    }
    
    
    
    //MARK: --------------- Private Functions
    
    
    func registerCell(){
        tableView.register(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: AddressTableViewCell.cellIdentifire())
    }
    
    @objc func filterAvailabilityTapped() {
        if UserStore.shared.selectedUserType == "Student"{
                tableView.reloadData()
        }else{
                tableView.reloadData()
        }
        
       //tableView.reloadData()
        DispatchQueue.main.async {
            //self.getCountForFilterApplied()
        }
    }
    
    
    // Reset all values
    func resetAllValues(){
        self.filterParameters.clearFilter()
        self.selectedIndex = 0
        tableView.reloadData()
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    // Tutor Cell
    func ConfigureTutorCell(_ tableView: UITableView, _ indexPath: IndexPath)->UITableViewCell{
        switch indexPath.section{
            
        case FilterTableViewSectionTypeTutor.Subjects.rawValue :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchSubjectTableViewCell.cellIdentifire(), for: indexPath) as? SearchSubjectTableViewCell  else{
                fatalError("Could not load SearchSubjectTableViewCell")
            }
            cell.configureCellWithSubjects("Subjects".localized, self.filterParameters.selectedSubjects)
            return cell
            
        case FilterTableViewSectionTypeTutor.Level.rawValue :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchSubjectTableViewCell.cellIdentifire(), for: indexPath) as? SearchSubjectTableViewCell  else{
                fatalError("Could not load SearchSubjectTableViewCell")
            }
           // cell.configureCellWithLevels("Level".localized, self.filterParameters.selectedLevels)
            cell.configureCellWithLevelsFromSearchController("Level".localized, self.filterParameters.selectedLevels)
            return cell
            
        case FilterTableViewSectionTypeTutor.Location.rawValue :
            guard indexPath.row != 0  else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: LocationFilterTutorTableViewCell.cellIdentifire(), for: indexPath) as? LocationFilterTutorTableViewCell  else{
                    fatalError("Failed to load a LocationFilterTutorTableViewCell from the table.")
                }
                 var minDistance = "\(MIN_DISTANCE)"
                 var maxDistance = "\(MAX_DISTANCE)"
                 if let min = filterParameters.minDistance{
                 minDistance = min
                 }
                 if let max = filterParameters.maxDistance{
                 maxDistance = max
                 }
                 cell.ConfigureCellForTutor(minDistance, maxDistance)
                 cell.locationSlider.refresh()
                 cell.locationFilterTutorTableViewCellDelegate = self
                 return cell
                
            }
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AddressTableViewCell.cellIdentifire(), for: indexPath) as?  AddressTableViewCell else{
                return UITableViewCell()
            }
            cell.addressTableViewCellDelegate = self
            if selectedIndex > 0{
                   cell.configureCell(userAddress: userAddresses[indexPath.row-1],index: indexPath.row, selected: indexPath.row==selectedIndex)
            }else{
                if let address = userAddresses[safe:indexPath.row-1]{
                      cell.configureCell(userAddress: address,index: indexPath.row, selected:false)
                }
                
            }
            
            return cell

        case FilterTableViewSectionTypeTutor.Availibility.rawValue :
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterAvailabilitySelectionTableViewCell.cellIdentifire(), for: indexPath) as! FilterAvailabilitySelectionTableViewCell
            cell.filterParams = self.filterParameters
            cell.reloadCollectionViewData()
            return cell

        case FilterTableViewSectionTypeTutor.Selection.rawValue :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: FilterSelectionButtonTableViewCell.cellIdentifire(), for: indexPath) as? FilterSelectionButtonTableViewCell else{
                fatalError("Failed to load a RatingTableViewCell from the table.")
            }
            cell.filterSelectionButtonTableViewCellDelegate = self
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    // Configure tutor cell height
    func configureTutorCellHeight(_ tableView: UITableView, _ indexPath: IndexPath)->CGFloat{
        
        switch indexPath.section{
            
        case FilterTableViewSectionTypeTutor.Subjects.rawValue           :       return SearchSubjectTableViewCell.cellHeight()
            
        case FilterTableViewSectionTypeTutor.Level.rawValue                 :       return SearchSubjectTableViewCell.cellHeight()
            
        case FilterTableViewSectionTypeTutor.Location.rawValue           :
                                                                                                                            guard indexPath.row != 0 else{ return  LocationFilterTutorTableViewCell.cellHeight()}
                                                                                                                            
//                                                                                                                            guard indexPath.row != 3 else{ return  OtherAddressTableViewCell.cellHeight()}
                                                                                                                            
                                                                                                                            return AddressTableViewCell.cellHeight()
            
        case FilterTableViewSectionTypeTutor.Availibility.rawValue         :        return FilterAvailabilitySelectionTableViewCell.cellHeight()
            
        case FilterTableViewSectionTypeTutor.Selection.rawValue          :        return FilterSelectionButtonTableViewCell.cellHeight()
            
        default: return UITableView.automaticDimension
            
        }
    }
    
    // configure student cell
    func ConfigureStudentCell(_ tableView: UITableView, _ indexPath: IndexPath)->UITableViewCell{
        
        switch indexPath.section{
        case FilterTableViewSectionType.Qualification.rawValue :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchSubjectTableViewCell.cellIdentifire(), for: indexPath) as? SearchSubjectTableViewCell  else{
                fatalError("Could not load SearchSubjectTableViewCell")
            }
            cell.configureCellWithQualifications("Qualification".localized, self.filterParameters.selectedQualifications)
            return cell
            
        case FilterTableViewSectionType.Subjects.rawValue :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchSubjectTableViewCell.cellIdentifire(), for: indexPath) as? SearchSubjectTableViewCell  else{
                fatalError("Could not load SearchSubjectTableViewCell")
            }
            cell.configureCellWithSubjects("Subjects".localized, self.filterParameters.selectedSubjects)
            return cell
            
        case FilterTableViewSectionType.Level.rawValue :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchSubjectTableViewCell.cellIdentifire(), for: indexPath) as? SearchSubjectTableViewCell  else{
                fatalError("Could not load SearchSubjectTableViewCell")
            }
            //cell.configureCellWithLevels("Level".localized, self.filterParameters.selectedLevels)
            cell.configureCellWithLevelsFromSearchController("Level".localized, self.filterParameters.selectedLevels)
            return cell
            
        case FilterTableViewSectionType.Gender.rawValue :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: GenderTableViewCell.cellIdentifire(), for: indexPath) as? GenderTableViewCell  else{
                fatalError("Failed to load a RatingTableViewCell from the table.")
            }
            cell.genderTableViewCellDelegate = self
            cell.configureCell(filterParameters.gender)
            return cell
            
        case FilterTableViewSectionType.Location.rawValue :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LocationFilterTutorTableViewCell.cellIdentifire(), for: indexPath) as? LocationFilterTutorTableViewCell  else{
                fatalError("Failed to load a LocationFilterTutorTableViewCell from the table.")
            }
            var minPrice = "0"//"20"
            var maxPrice = "150"
            if let min = filterParameters.minPrice{
                minPrice = min
            }
            if let max = filterParameters.maxPrice{
                maxPrice = max
            }
            cell.configureCell(minPrice, maxPrice)
            cell.locationSlider.refresh()
            cell.locationTitleLabel.text = "Tuition Rate (Per Hour)".localized
            cell.locationFilterTutorTableViewCellDelegate = self
            return cell
        case FilterTableViewSectionType.Availibility.rawValue :
            let cell = tableView.dequeueReusableCell(withIdentifier: FilterAvailabilitySelectionTableViewCell.cellIdentifire(), for: indexPath) as! FilterAvailabilitySelectionTableViewCell
            cell.filterParams = self.filterParameters
            cell.reloadCollectionViewData()
            return cell
        case FilterTableViewSectionType.Rating.rawValue :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SwiftyStarRatingTableViewCell.cellIdentifire(), for: indexPath) as? SwiftyStarRatingTableViewCell else{
                fatalError("Failed to load a RatingTableViewCell from the table.")
            }
            cell.configureCellWithRating(filterParameters.rating)
            cell.swiftyStarRatingTableViewCellDelegate = self
            return cell
        case FilterTableViewSectionType.Selection.rawValue :
            guard let cell = tableView.dequeueReusableCell(withIdentifier: FilterSelectionButtonTableViewCell.cellIdentifire(), for: indexPath) as? FilterSelectionButtonTableViewCell else{
                fatalError("Failed to load a RatingTableViewCell from the table.")
            }
            cell.filterSelectionButtonTableViewCellDelegate = self
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    // Configure student cell height
    func configureStudentCellHeight(_ tableView: UITableView, _ indexPath: IndexPath)->CGFloat{
        
        switch indexPath.section{
            
        case FilterTableViewSectionType.Qualification.rawValue     :       return SearchSubjectTableViewCell.cellHeight()
            
        case FilterTableViewSectionType.Subjects.rawValue           :       return SearchSubjectTableViewCell.cellHeight()
            
        case FilterTableViewSectionType.Level.rawValue                 :       return SearchSubjectTableViewCell.cellHeight()
            
        case FilterTableViewSectionType.Gender.rawValue              :       return GenderTableViewCell.cellHeight()
            
        case FilterTableViewSectionType.Location.rawValue           :        return LocationFilterTutorTableViewCell.cellHeight()
            
        case FilterTableViewSectionType.Availibility.rawValue         :        return FilterAvailabilitySelectionTableViewCell.cellHeight()
            
//        case FilterTableViewSectionType.Rating.rawValue             :        return RatingTableViewCell.cellHeight()
        case FilterTableViewSectionType.Rating.rawValue               :        return SwiftyStarRatingTableViewCell.cellHeight()
            
        case FilterTableViewSectionType.Selection.rawValue          :      return FilterSelectionButtonTableViewCell.cellHeight()
            
        default: return UITableView.automaticDimension
        }
    }
    
    // Configure student cell selection
    func configureStudentCellSelection(_ tableView: UITableView, _ indexPath: IndexPath){
        switch indexPath.section{
        case FilterTableViewSectionType.Qualification.rawValue :
            let subjectlListingVC = SubjectListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            subjectlListingVC.subjectListingViewControllerDelegate = self
            subjectlListingVC.screenType = .Qualification
            subjectlListingVC.filterParameters = self.filterParameters
            self.navigationController?.pushViewController(subjectlListingVC, animated: true)
            
        case FilterTableViewSectionType.Subjects.rawValue :
            let subjectlListingVC = SubjectListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            subjectlListingVC.subjectListingViewControllerDelegate = self
            subjectlListingVC.screenType = .Subjects
            subjectlListingVC.filterParameters = self.filterParameters
            self.navigationController?.pushViewController(subjectlListingVC, animated: true)
            
        case FilterTableViewSectionType.Level.rawValue :
            let subjectlListingVC = SubjectListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            subjectlListingVC.subjectListingViewControllerDelegate = self
            subjectlListingVC.screenType = .Level
            subjectlListingVC.filterParameters = self.filterParameters
            self.navigationController?.pushViewController(subjectlListingVC, animated: true)
        default:
            break
        }
    }
    
    // Configure tutor cell selection
    func configureTutorCellSelection(_ tableView: UITableView, _ indexPath: IndexPath){
        switch indexPath.section{
            
        case FilterTableViewSectionTypeTutor.Subjects.rawValue :
            let subjectlListingVC = SubjectListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            subjectlListingVC.subjectListingViewControllerDelegate = self
            subjectlListingVC.screenType = .Subjects
            subjectlListingVC.filterParameters = self.filterParameters
            self.navigationController?.pushViewController(subjectlListingVC, animated: true)
            
        case FilterTableViewSectionTypeTutor.Level.rawValue :
            let subjectlListingVC = SubjectListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            subjectlListingVC.subjectListingViewControllerDelegate = self
            subjectlListingVC.screenType = .Level
            subjectlListingVC.filterParameters = self.filterParameters
            self.navigationController?.pushViewController(subjectlListingVC, animated: true)
        default:
            break
        }
    }
    
    // autocomplete functions
    
    // Present the Autocomplete view controller when the button is pressed.
      func autocompleteClicked() {
        
        let vc = ConfirmLocationViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        vc.referredVC = self
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func addressFromConfirmLocationVC(_ address: LocationData?){
        print(address)
        if let addr = address{
            
            let otherUserAddress = userAddresses[selectedIndex-1]

            otherUserAddress.addressLocation = "\(addr.address)"
            otherUserAddress.addressLabel = "Others".localized
            otherUserAddress.lattitude = "\(addr.lattitude)"
            otherUserAddress.longitude = "\(addr.longitude)"
            
            self.tableView.reloadSections([FilterTableViewSectionTypeTutor.Location.rawValue], with: .none)
            
        }
    }
    
  
}

    //MARK:  ----------------- GMSAutocompleteViewControllerDelegate Delegates Methods

extension SearchViewController: GMSAutocompleteViewControllerDelegate{
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        let otherUserAddress = userAddresses[selectedIndex-1]
        otherUserAddress.addressLocation = "\(place.name)"
        otherUserAddress.addressLabel = "Others".localized
        otherUserAddress.lattitude = "\(place.coordinate.latitude)"
        otherUserAddress.longitude = "\(place.coordinate.longitude)"
//        userAddresses.insert(otherUserAddress, at: selectedIndex-1)
    self.tableView.reloadSections([FilterTableViewSectionTypeTutor.Location.rawValue], with: .none)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

    //MARK: ----------------- SearchViewController's Cells Delegates Methods

extension SearchViewController: SearchSubjectTableViewDelegate, GenderTableViewCellDelegate, FilterSelectionButtonTableViewCellDelegate, SwiftyStarRatingTableViewCellDelegate, LocationFilterTutorTableViewCellDelegate, AddressTableViewCellDelegate{
    
    func selectedLocation(_ sender: UIButton){
            selectedIndex = sender.tag
        tableView.reloadSections([FilterTableViewSectionTypeTutor.Location.rawValue], with: .none)
        if sender.tag == userAddresses.count{
            autocompleteClicked()
        }
    }
    
    
    func sliderValue(_ minValue: String, _ maxVlaue: String) {
        print(minValue, maxVlaue)
        if UserStore.shared.selectedUserType == "Tutor"{
            filterParameters.minDistance = minValue
            filterParameters.maxDistance = maxVlaue
        }else{
            filterParameters.minPrice = minValue
            filterParameters.maxPrice = maxVlaue
        }
       // tableView.reloadData()
    }
    
   
    func resetButtonTapped() {
        resetAllValues()
    }
    
    func applyButtonTapped() {
        
        if UserStore.shared.selectedUserType == "Tutor"{
            if selectedIndex > 0{
                let selectedAddress = userAddresses[selectedIndex-1]
                if selectedAddress.lattitude != nil, selectedAddress.longitude != nil, selectedAddress.lattitude != "", selectedAddress.longitude != ""{
                    filterParameters.addresslabel = (selectedAddress.addressLabel)
                    filterParameters.addressLattitude = (selectedAddress.lattitude)
                    filterParameters.addressLongitude = selectedAddress.longitude

                }
//                else{
//                    self.showSimpleAlertWithMessage("Please select location.")
//                }
            }
            
            let userListingVC = UsersListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            userListingVC.filterParameters = self.filterParameters
            userListingVC.vc = self
            if self.userAddresses.count > 0{
                 userListingVC.userAddresses = self.userAddresses[0]
            }
            userListingVC.titleVC = "Students".localized
            self.navigationController?.pushViewController(userListingVC, animated: true)
            
        }else{

            let userListingVC = UsersListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            userListingVC.filterParameters = self.filterParameters
            userListingVC.vc = self
            userListingVC.titleVC = "Tutors".localized
            self.navigationController?.pushViewController(userListingVC, animated: true)
            
        }
        
    }
    
    func ratingValueChanged(_ ratingValue: Double) {
        self.filterParameters.rating = "\(ratingValue)"
    }
    
    func getStarRating(_ rating: Double) {
        self.filterParameters.rating = "\(rating)"
        print( "----\(String(describing: self.filterParameters.rating))")
    }
    
    
    func genderStatus(_ status: String) {
        self.filterParameters.gender = status
        print("Gender: \(String(describing: self.filterParameters.gender))")
    }
    
    func searchSubjectBtnTapped() {
        let subjectlListingVC = SubjectListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
        self.navigationController?.pushViewController(subjectlListingVC, animated: true)
    }
    
}

    //MARK: -----------------SubjectListingViewController's Delegates Methods

extension SearchViewController: SubjectListingViewControllerDelegate{
    
    func selectedSubject(_ selectedSubs: [Subject]) {
      
        if selectedSubs.count>0{
            for index in 0..<selectedSubs.count{
                if selectedSubs[index].name == "Others".localized{
                    //selectedSubs[index].id = filterParameters.otherSubject
                    selectedSubs[index].enteredName = filterParameters.otherSubject
                }else if selectedSubs[index].name == "Languages".localized{
                    selectedSubs[index].enteredName = filterParameters.language
                }else if selectedSubs[index].name == "Music".localized{
                    selectedSubs[index].enteredName = filterParameters.music
                }
            }
        }
            filterParameters.selectedSubjects = selectedSubs
        
            tableView.reloadData()
    }
    
    func selectedLevel(_ selectedLevles: [Level]) {
        print(selectedLevles)
        //if selectedLevles.count != 0{
            filterParameters.selectedLevels = selectedLevles
            tableView.reloadData()
        //}
    }
    
    func selectedQualification(_ selectedQuals: [Qualification]) {
         print(selectedQuals)
       // if selectedQuals.count != 0{
            filterParameters.selectedQualifications = selectedQuals
            tableView.reloadData()
        //}
    }
    
    
}

    //MARK: ------------------ TableView Delegates/DataSources

extension SearchViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if UserStore.shared.selectedUserType == "Tutor" && section == 2{
           return numberOfRows
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if UserStore.shared.selectedUserType == "Tutor"{
            return ConfigureTutorCell(tableView, indexPath)
        }else{
             return ConfigureStudentCell(tableView, indexPath)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if UserStore.shared.selectedUserType == "Tutor"{
            configureTutorCellSelection(tableView, indexPath)
        }else{
           configureStudentCellSelection(tableView, indexPath)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if UserStore.shared.selectedUserType == "Tutor"{
            return configureTutorCellHeight(tableView, indexPath)
        }else{
            return configureStudentCellHeight(tableView, indexPath)
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section{
        case 0 :
            return SearchSubjectTableViewCell.cellHeight()
        default:
            return UITableView.automaticDimension
        }
    }
    
}


    //MARK: ------------------ TextField Delegate

extension SearchViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return  true
    }
}

