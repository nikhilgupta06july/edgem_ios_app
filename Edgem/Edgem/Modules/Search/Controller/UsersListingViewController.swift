//
//  TutorListViewController.swift
//  Edgem
//
//  Created by Hipster on 19/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class UsersListingViewController: BaseViewController {
    
    //MARK: ------ Properties -------------
    
    var userType: String!
    
    var filterParameters = FilterParams()
    var subjectID: String = ""
    var getOnlySubject: Bool = false
    var bookMarkedStudentData: BookMarkedStudent!
    var rowCount = 0
    var titleVC: String = ""
    var disposableBag = DisposeBag()
    var addBookMarksDisposable: Disposable!
    var sortBy: String = ""
    
    var tutorListing = [Tutor]()
    var studentListing = [Student]()
    var pagination = Pagination()
    
    var tutorImage: UIImage?
    var studentImage: UIImage?
    
    var vc: UIViewController?
    var userAddresses: UserAddress?
    
    /// This variable will be used to get recommended tutors list
    var isFromViewMore: Bool? = false
    
    // MARK: ------------ @IBOutlets -------------
    
    
    @IBOutlet weak var bookMarkBtn: UIButton!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    // MARK: ------------ View LifeCycle -------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userType = UserStore.shared.selectedUserType
        titleLabel.text = titleVC
        pagination = Pagination.init()
        
        if let fromViewMore = self.isFromViewMore ,  fromViewMore == true{
            
            bookMarkBtn.isHidden = true
            filterBtn.isHidden = true
            
            tutorListing = []
            getRecommendedTutors()
            
        }else{
            
            bookMarkBtn.isHidden = false
            filterBtn.isHidden = false
            
            if userType == tutorType{
                studentListing = []
                getResponseForFilterStudent()
            }else{
                tutorListing = []
                getResponseForFilterTutor()
            }
            
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let fromViewMore = self.isFromViewMore ,  fromViewMore == true{
            
            if let idList = AppDelegateConstant.idList{
                if idList.count > 0{
                    let tutors = self.tutorListing
                    for index in 0..<tutors.count{
                        for index_id in 0..<idList.count{
                            if tutors[index].tutorID == idList[index_id]{
                                tutors[index].isbookmark = false
                            }
                        }
                    }
                    self.tutorListing = tutors
                    tableView.reloadData()
                }
            }
            
        }else{
            
            if userType == tutorType{
                if let idList = AppDelegateConstant.idList{
                    if idList.count > 0{
                        let students = self.studentListing
                        for index in 0..<students.count{
                            for index_id in 0..<idList.count{
                                if students[index].studentID == idList[index_id]{
                                    students[index].isbookmark = false
                                }
                            }
                        }
                        self.studentListing = students
                        tableView.reloadData()
                    }
                }
            }else{
                if let idList = AppDelegateConstant.idList{
                    if idList.count > 0{
                        let tutors = self.tutorListing
                        for index in 0..<tutors.count{
                            for index_id in 0..<idList.count{
                                if tutors[index].tutorID == idList[index_id]{
                                    tutors[index].isbookmark = false
                                }
                            }
                        }
                        self.tutorListing = tutors
                        tableView.reloadData()
                    }
                }
            }
            
        }

    }
    
    // MARK: ------------ @IBActions -------------
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigateBack(sender: sender)
    }
    
    @IBAction func sortByBtnTapped(_ sender: UIButton) {
        
        if let fromViewMore = self.isFromViewMore ,  fromViewMore == true{
            let bottomPopUpVC = BottomPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            bottomPopUpVC.bottomPopUpViewControllerDelegate = self
            bottomPopUpVC.userType = "Tutor"
            bottomPopUpVC.sortByPropertiesArr = ["","Furthest to Nearest", "Nearest to Furthest"]
            self.navigationController?.present(bottomPopUpVC, animated: true, completion: nil)
        }else{
            if userType == "Student" || userType == "Parent"{
                
                let bottomPopUpVCStudent = BottomPopUpVCStudent.instantiateFromAppStoryboard(appStoryboard: .Profile)
                bottomPopUpVCStudent.bottomPopUpVCStudentDelegate = self
                //bottomPopUpVCStudent.bottomPopUpViewControllerDelegate = self
                //bottomPopUpVCStudent.userType = "Tutor"
                bottomPopUpVCStudent.sectionTitlesArr = ["Preferred", "Tuition Rate", "Ratings"]
                self.navigationController?.present(bottomPopUpVCStudent, animated: true, completion: nil)
            }else if userType == "Tutor"{
                
                let bottomPopUpVC = BottomPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                bottomPopUpVC.bottomPopUpViewControllerDelegate = self
                bottomPopUpVC.userType = "Tutor"
                bottomPopUpVC.sortByPropertiesArr = ["","Furthest to Nearest", "Nearest to Furthest"]
                self.navigationController?.present(bottomPopUpVC, animated: true, completion: nil)
                
            }
        }
    }
    
    
    @IBAction func getBookMarks(_ sender: UIButton) {
        
        if let fromViewMore = self.isFromViewMore ,  fromViewMore == true{

            let usersBookMarksListingViewController = UsersBookMarksListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            
            usersBookMarksListingViewController.titleVC = "Bookmarks".localized
            
            self.navigationController?.pushViewController(usersBookMarksListingViewController, animated: true)
            
        }else{
            if userType == tutorType{
                
                let usersBookMarksListingViewController = UsersBookMarksListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
                
                usersBookMarksListingViewController.titleVC = "Bookmarks".localized
                
                self.navigationController?.pushViewController(usersBookMarksListingViewController, animated: true)
                
            }else {
                
                let usersBookMarksListingViewController = UsersBookMarksListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
                
                usersBookMarksListingViewController.titleVC = "Bookmarks".localized
                
                self.navigationController?.pushViewController(usersBookMarksListingViewController, animated: true)
                
            }
        }

    }

}

// MARK: ------------ BottomPopUpViewController Delegates
extension UsersListingViewController: BottomPopUpViewControllerDelegate, BottomPopUpVCStudentDelegate{
    
    func studentApplyBtnTapped(_ sortBy: String) {
        print(sortBy)
        //Preffered - prfrd
        //rate - rate_a/rate_d
        //ratings - ratng_a/ratng_d
        if sortBy.localized == "Preferred".localized{
             self.sortBy = "prfrd"
        }else if sortBy.localized == "Highest to Lowest ".localized{
            self.sortBy = "rate_d"
        }else if sortBy.localized == "Lowest to Highest ".localized{
            self.sortBy = "rate_a"
        }else if sortBy.localized == "Highest to Lowest".localized{
            self.sortBy = "ratng_d"
        }else if sortBy.localized == "Lowest to Highest".localized{
            self.sortBy = "ratng_a"
        }
        pagination.currentPage = 0
        self.tutorListing.removeAll()
        getResponseForFilterTutor()
        
    }
    
    
    func sortByApplyButtonTapped(_ sortBy: String) {
        print(sortBy)
                        if sortBy.localized == "Furthest to Nearest".localized{
                            self.sortBy = "ftn"
                        }else if sortBy.localized == "Nearest to Furthest".localized{
                            self.sortBy = "ntf"
                        }
//                        if pagination.currentPage-1 >= 0{
//                            pagination.currentPage = 0
//                        }
                        pagination.currentPage = 0
                        // hit service
                        if userType == tutorType{
                            self.studentListing.removeAll()
                            getResponseForFilterStudent()
                        }else{
                            self.tutorListing.removeAll()
                            getResponseForFilterTutor()
                            
                        }
        
                    }
    
    func haveTutorImage(_ tutorImage: UIImage) {
        self.tutorImage = tutorImage
    }
    
    }

// MARK:  ------------ TutorListTableViewCell Delegate, StudentListTableViewDelegate ---------------

extension UsersListingViewController: TutorlistTableViewCellDelegate, StudentListTableViewCellDelegate{
    
    func studentBookmarksBtnTapped(_ status: Bool, index: Int) {
        guard status else {
            removeBookMarks(studentListing[index].studentID, index: index)
            return
        }
        addBookMarks(studentListing[index].studentID, index: index)
        return
    }
    
    func bookmarksBtnTapped(_ status: Bool, index: Int) {
        guard status else {
            removeBookMarks(tutorListing[index].tutorID, index: index)
            return
        }
        addBookMarks(tutorListing[index].tutorID, index: index)
        return
    }
    
}

// MARK:  ------------ TableView Delegates/DataSources -------------

extension UsersListingViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if userType == tutorType{
            
            if isFromViewMore == true{
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: TutorlistTableViewCell.cellIdentifire(), for: indexPath) as? TutorlistTableViewCell else{
                    fatalError("could not load TutorlistTableViewCell")
                }
                
                cell.tutorlistTableViewCellDelegate = self
                cell.bookmarksBtn.isHidden = true
                cell.configureCell(with: tutorListing[indexPath.row], indexPath: indexPath)
                cell.tutorsImage = { [weak self] (image) in
                    self?.haveTutorImage(image)
                }
                return cell
                
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: StudentListTableViewCell.cellIdentifire(), for: indexPath) as? StudentListTableViewCell else{
                    fatalError("could not load TutorlistTableViewCell")}
                 cell.bookmarksBtn.isHidden = false
                cell.configureCell(with: studentListing[indexPath.row], indexPath: indexPath)
                cell.studentListTableViewCellDelegate = self
                return cell
            }

        }else{
          
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TutorlistTableViewCell.cellIdentifire(), for: indexPath) as? TutorlistTableViewCell else{
                fatalError("could not load TutorlistTableViewCell")
            }
       
            cell.tutorlistTableViewCellDelegate = self
            cell.configureCell(with: tutorListing[indexPath.row], indexPath: indexPath)
            cell.tutorsImage = { [weak self] (image) in
                self?.haveTutorImage(image)
            }
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
             return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if userType == tutorType{
            if indexPath.row == studentListing.count-1 && pagination.currentPage != pagination.lastPage{
                getResponseForFilterStudent()
            }
            
        }else{
            if indexPath.row == tutorListing.count-1 && pagination.currentPage != pagination.lastPage{
                    getResponseForFilterTutor()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let fromViewMore = self.isFromViewMore ,  fromViewMore == true{
            let tutorCompleteProfileViewController = TutorCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            
            tutorCompleteProfileViewController.tutorID = self.tutorListing[indexPath.row].tutorID
            
            self.navigationController?.pushViewController(tutorCompleteProfileViewController, animated: true)
        }else{
            if userType == tutorType{
                let studentCompleteProfileViewController = StudentCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                
                studentCompleteProfileViewController.studentID = self.studentListing[indexPath.row].studentID
                
                self.navigationController?.pushViewController(studentCompleteProfileViewController, animated: true)
            }else{
                let tutorCompleteProfileViewController = TutorCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                
                tutorCompleteProfileViewController.tutorID = self.tutorListing[indexPath.row].tutorID
                
                self.navigationController?.pushViewController(tutorCompleteProfileViewController, animated: true)
            }
        }

    }
    
}

    // MARK: ------------ API Implementations -------------

extension UsersListingViewController{
    
    // Add bookmarks
    func addBookMarks(_ ID: Int, index:Int){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else{return}
        let param = [  "user_id"                   :    userID,
                                "bookmarked_user" :    ID ] as [String : AnyObject]
        
         Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let addBookMarksObserver = ApiManager.shared.apiService.addBookmarks(param)
        let addBookMarksDisposable = addBookMarksObserver.subscribe(onNext: { (message) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                
                if let fromViewMore = self.isFromViewMore ,  fromViewMore == true{
                    
                     self.tutorListing[index].isbookmark = !self.tutorListing[index].isbookmark
                    
                }else{
                    
                    if self.userType == tutorType{
                        
                        self.studentListing[index].isbookmark = !self.studentListing[index].isbookmark
                        
                    }else{
                        
                        self.tutorListing[index].isbookmark = !self.tutorListing[index].isbookmark
                        
                    }
                }
                
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
            }
        })
        return addBookMarksDisposable.disposed(by: disposableBag)
        
    }
    
     // Remove bookmarks
    func removeBookMarks(_ ID: Int, index:Int){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else{return}
        let param = [  "user_id"                   :    userID,
                               "bookmarked_user" :    ID ] as [String : AnyObject]
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let addBookMarksObserver = ApiManager.shared.apiService.removeBookmarks(param)
        let addBookMarksDisposable = addBookMarksObserver.subscribe(onNext: { (message) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                if self.userType == tutorType{
                    if index < self.studentListing.count{
                         self.studentListing[index].isbookmark = !self.studentListing[index].isbookmark
                    }
                }else{
                    if index < self.tutorListing.count{
                         self.tutorListing[index].isbookmark = !self.tutorListing[index].isbookmark
                    }
                }
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
            }
        })
        return addBookMarksDisposable.disposed(by: disposableBag)
        
    }
    
    // Filter Tutor
    func getResponseForFilterTutor() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
 
        var filterParams = filterParameters.getFilterParamsDict()
       
        if let viewController = self.vc , viewController is DashboardViewController{
              filterParams["subject"] = subjectID
        }
        filterParams["page"] = String(pagination.currentPage+1)
                if self.sortBy != ""{
                    if sortBy == "ftn" {
                        filterParams["sort_by"] = "distance_d"
                    }else if sortBy == "ntf"{
                        filterParams["sort_by"] = "distance_a"
                    }else if sortBy == "prfrd"{
                        filterParams["sort_by"] = "prfrd"
                    }else if sortBy == "ratng_d"{
                        filterParams["sort_by"] = "ratng_d"
                    }else if sortBy == "ratng_a"{
                        filterParams["sort_by"] = "ratng_a"
                    }else if sortBy == "rate_a"{
                        filterParams["sort_by"] = "rate_a"
                    }else if sortBy == "rate_d"{
                        filterParams["sort_by"] = "rate_d"
                    }
                }
        
        
        let filterObserver = ApiManager.shared.apiService.fetchTutorFilterResult(filterParams as [String : AnyObject])
        let fileterDisposable = filterObserver.subscribe(onNext: { (tutorPagination) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                
                self.pagination = tutorPagination.pagination
                self.tutorListing += tutorPagination.tutor

                if self.tutorListing.count > 0{
                    self.rowCount = self.tutorListing.count
                    self.tableView.reloadData()
                }else{
                    let noTutorStudentVC = NoStudentTutorViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
                    noTutorStudentVC.userType = "Tutor"
                    noTutorStudentVC.tutorStatusLabelText = "No Tutors found".localized
                    self.navigationController?.pushViewController(noTutorStudentVC, animated: true)
                    
                }
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                let noTutorStudentVC = NoStudentTutorViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
                noTutorStudentVC.userType = "Tutor"
                noTutorStudentVC.tutorStatusLabelText = "No Tutors found".localized
                self.navigationController?.pushViewController(noTutorStudentVC, animated: true)
            }

        })
        return fileterDisposable.disposed(by: disposableBag)
    }
    
    // Filter Student
    func getResponseForFilterStudent(){

        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])

        var filterParams = [String : String]()
        
        if getOnlySubject == true{
            if let viewController = self.vc , viewController is DashboardViewController{
                if AppDelegateConstant.user?.userType == tutorType{
                     filterParams["subject"]            = subjectID
                     filterParams["longitude"]        = AppDelegateConstant.user?.userAddress?[0].longitude
                     filterParams["lattitude"]          = AppDelegateConstant.user?.userAddress?[0].lattitude
                     filterParams["address_label"]  = AppDelegateConstant.user?.userAddress?[0].addressLabel
                     filterParams["distance_max"]  = "\(MAX_DISTANCE)"
                     filterParams["distance_min"]  = "\(MIN_DISTANCE)"
                     filterParams["distance_min"]  = "\(MIN_DISTANCE)"
                    
                }else{
                     filterParams["subject"] = subjectID
                }
                    filterParams["page"] = String(pagination.currentPage+1)
                   if self.sortBy != ""{
                        if sortBy == "ftn" {
                        filterParams["sort_by"] = "distance_d"
                    }else if sortBy == "ntf"{
                        filterParams["sort_by"] = "distance_a"
                    }
                }
            }
        }else{
            filterParams = filterParameters.getFilterParamsDict()
            
            if let long = filterParams["longitude"]{
                if long.isEmpty{
                    if let userAddress = self.userAddresses{
                        filterParams["longitude"] = userAddress.longitude
                        filterParams["lattitude"] = userAddress.lattitude
                    }
                }
            }
            if let viewController = self.vc , viewController is DashboardViewController{
                filterParams["subject"] = subjectID
            }
            filterParams["page"] = String(pagination.currentPage+1)
            
            if self.sortBy != ""{
                if sortBy == "ftn" {
                    filterParams["sort_by"] = "distance_d"
                }else if sortBy == "ntf"{
                    filterParams["sort_by"] = "distance_a"
                }
            }
        }
        
        let filterObserver = ApiManager.shared.apiService.fetchStudentFilterResult(filterParams as [String : AnyObject])
        let fileterDisposable = filterObserver.subscribe(onNext: { (studentPagination) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                self.pagination = studentPagination.pagination
                self.studentListing += studentPagination.student
                
                if self.studentListing.count > 0{
                    self.rowCount = self.studentListing.count
                    self.tableView.reloadData()
                }else{
                    
                    let noTutorStudentVC = NoStudentTutorViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
                    noTutorStudentVC.userType = "Student"
                    noTutorStudentVC.studentStatusLabelText = "No students found"
                    self.navigationController?.pushViewController(noTutorStudentVC, animated: true)
                    
                }
                
            }
        }, onError: { (error) in
            
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                // this implementation is not correct please check and reslolve
                let noTutorStudentVC = NoStudentTutorViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
                noTutorStudentVC.userType = "Student"
                noTutorStudentVC.studentStatusLabelText = "No students found"
                self.navigationController?.pushViewController(noTutorStudentVC, animated: true)
            }
        })
        return fileterDisposable.disposed(by: disposableBag)
    }
    
    // Get Recommended Tutor
    fileprivate func getRecommendedTutors(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
    
        let filterObserver = ApiManager.shared.apiService.getRecommendedTutors()
        let fileterDisposable = filterObserver.subscribe(onNext: { (tutorPagination) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                
                self.pagination = tutorPagination.pagination
                self.tutorListing += tutorPagination.tutor
                
                if self.tutorListing.count > 0{
                    self.rowCount = self.tutorListing.count
                    self.tableView.reloadData()
                }else{
                    let noTutorStudentVC = NoStudentTutorViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
                    noTutorStudentVC.userType = "Tutor"
                    noTutorStudentVC.tutorStatusLabelText = "No Tutors found".localized
                    self.navigationController?.pushViewController(noTutorStudentVC, animated: true)
                    
                }
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                let noTutorStudentVC = NoStudentTutorViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
                noTutorStudentVC.userType = "Tutor"
                noTutorStudentVC.tutorStatusLabelText = "No Tutors found".localized
                self.navigationController?.pushViewController(noTutorStudentVC, animated: true)
            }
            
        })
        return fileterDisposable.disposed(by: disposableBag)
    }
}
