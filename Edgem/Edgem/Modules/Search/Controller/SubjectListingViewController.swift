//
//  SubjectListingViewController.swift
//  Edgem
//
//  Created by Hipster on 21/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

protocol SubjectListingViewControllerDelegate{
    func selectedSubject(_ selectedSubs: [Subject])
    func selectedLevel(_ selectedLevls: [Level])
    func selectedQualification(_ selectedQuals: [Qualification])
}

class SubjectListingViewController: BaseViewController {
    
    //MARK: -------------- Properties
    
    var screenType: FilterScreenCategoryTypeStudent = .Qualification
    var totalRows: Int = 0
    var isMultiSelect = false
    
    var searchFilterCatagory = ""
    var qualifications = [Qualification]()
    var selectedQualification: [Qualification]! = []
    
    var levelPagination = LevelPagination.init()
    var selectedLevel: [Level]! = []
    
    var subjectListing: [Subject]! = []
    var pagination = Pagination()
    
    var selectedSubjects: [Subject]! = []
    var searchedSubject: [Subject] = Array()
    var originalSubjects: [Subject] = Array()
    
    var disposableBag = DisposeBag()
    
    var filterParameters: FilterParams!
    var addLevelSubject: AddLevelSubject!
    
    var userData: User!
    var subjectListingViewControllerDelegate: SubjectListingViewControllerDelegate?
    
    var isFrom: UIViewController?
    
    // MARK: -------------- Outlets
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    @IBOutlet weak var searchHolderView: UIView!
    @IBOutlet weak var searchholderviewHeight: NSLayoutConstraint!
    @IBOutlet weak var applyBtnHolderView: UIView!
    @IBOutlet weak var backBtnTapped: UIButton!
    @IBOutlet weak var controllerTitleLabel: UILabel!
    @IBOutlet weak var searchSubjectTextField: UITextField!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var searchSubjectTextfield: UITextField!
    @IBOutlet weak var searchSubjectcontaineView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        initialSetUP()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(value: "Controller Name: SubjectListingViewController")
    }
    
    //MARK: -------------- Private Functions
    
    func numberOfRows(){
        switch screenType{
        case .Subjects:
            totalRows = subjectListing.count//subjectListing.count + 1
        case .Qualification:
            totalRows = qualifications.count
        case .Level:
            totalRows = levelPagination.levels.count
        }
    }
    
    fileprivate func setSelectedFields(){
        
        guard let filter = filterParameters else{return}
        switch screenType{
            
        case .Qualification:
            self.selectedQualification = filter.selectedQualifications
            tableView.reloadData()
        case .Subjects:
            self.selectedSubjects = filter.selectedSubjects
            tableView.reloadData()
        case .Level:
            self.selectedLevel = filter.selectedLevels
            tableView.reloadData()
            
        }
    }
    
    fileprivate func setSelectedFieldsForLevelSubjectVC(){
        
        guard let filter = userData else{return}
        switch screenType{
            
        case .Subjects:
           
            if filter.subjects?.count == 0 && self.selectedSubjects.count != 0{
                 //self.selectedSubjects = filter.subjects
                self.selectedSubjects.forEach({ (subject) in
                    if subject.name == "Music"{
                        self.userData.music = subject.enteredName
                    }else if subject.name == "Languages"{
                        self.userData.language = subject.enteredName
                    }else if subject.name == "Others"{
                        self.userData.otherSubject = subject.enteredName
                    }
                })
            }else{
                 self.selectedSubjects = filter.subjects
                if ((filter.subjects?.count) != nil){
                    filter.subjects?.forEach({ (subject) in
                        if subject.name == "Music"{
                            self.userData.music = subject.enteredName
                        }else if subject.name == "Languages"{
                            self.userData.language = subject.enteredName
                        }else if subject.name == "Others"{
                            self.userData.otherSubject = subject.enteredName
                        }
                    })
                }
            }
            tableView.reloadData()
            
        case .Level:
            self.selectedLevel = filter.level
            tableView.reloadData()
            
        case .Qualification:
            break
        }
    }
    
    func registerCell(){
        tableView.register(UINib(nibName: "CommonCheckBoxCell", bundle: nil), forCellReuseIdentifier: CommonCheckBoxCell.cellIdentifire())
        tableView.register(UINib(nibName: "OtherSubjectTableViewCell", bundle: nil), forCellReuseIdentifier: OtherSubjectTableViewCell.cellIdentifire())
    }
    
    
    func initialSetUP(){
        // status bar backgroung color
        // UIApplication.shared.statusBarView?.backgroundColor = theamAppColor
       
        // Placeholder setup
        let placeholderText = "Enter your subject"
        let placeHolder = NSMutableAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.font:UIFont(name: QuicksandRegular, size: 15.0)!])
        placeHolder.addAttributes([NSAttributedString.Key.foregroundColor: theamGrayColor], range: NSRange(location: 0, length: placeholderText.count))
        searchSubjectTextField.attributedPlaceholder = placeHolder
        
        // apply button shadow properties
        applyBtnHolderView.layer.shadowColor = UIColor.black.cgColor
        applyBtnHolderView.layer.shadowRadius = 4
        applyBtnHolderView.layer.shadowOpacity = 0.12
        applyBtnHolderView.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        applyBtnHolderView.layer.shadowPath = UIBezierPath(rect: applyButton.frame).cgPath
        
        // apply button properties
        applyButton.layer.cornerRadius = 5
        applyButton.clipsToBounds = true
        
        // search subject holder view properties
        searchSubjectcontaineView.layer.borderWidth = 1
        searchSubjectcontaineView.layer.borderColor = theamBorderGrayColor.cgColor
        searchSubjectcontaineView.layer.cornerRadius = 4
        searchSubjectcontaineView.clipsToBounds = true
        
        // search holder view status
        switch screenType {
        case .Subjects:
            searchHolderView.isHidden = false
            searchholderviewHeight.constant = 64
            controllerTitleLabel.text = "Subjects".localized
            if let subjects = AppDelegateConstant.subjectListing {
                self.subjectListing = subjects
                self.originalSubjects = self.subjectListing
                
                if self.isFrom is LevelSubjectViewController{
                    self.setSelectedFieldsForLevelSubjectVC()
                }else{
                    self.setSelectedFields()
                }
                self.numberOfRows()
                self.tableView.reloadData()
            }else{
                fetchSubjects()
            }
            
        case .Qualification:
            searchHolderView.isHidden = true
            searchholderviewHeight.constant = 0
            controllerTitleLabel.text = "Qualification".localized
            
            if let qualifications = AppDelegateConstant.qualification{
                self.qualifications = qualifications
                self.setSelectedFields()
                self.numberOfRows()
                tableView.reloadData()
            }else{
                fetchQualification()
            }
            
        case .Level:
            searchHolderView.isHidden = true
            searchholderviewHeight.constant = 0
            controllerTitleLabel.text = "Level".localized
            
            if let level = AppDelegateConstant.levelPagination{
                self.levelPagination = level
                
                if self.isFrom is LevelSubjectViewController{
                    self.setSelectedFieldsForLevelSubjectVC()
                }else{
                    self.setSelectedFields()
                }
                
                self.numberOfRows()
                self.tableView.reloadData()
            }else{
                fetchLevels()
            }
            
        }
        
    }
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigateBack(sender: sender)
    }
    
    @IBAction func searchSubjects(_ sender: UITextField) {
        
        self.subjectListing.removeAll()
        if sender.text?.count != 0{
            for subject in originalSubjects{
                if let subjectToSearch = sender.text{
                    let range = subject.name.lowercased().range(of: subjectToSearch, options: .caseInsensitive, range: nil, locale: nil)
                    if range != nil{
                        self.subjectListing.append(subject)
                    }
                }
            }
        }else{
            self.subjectListing = self.originalSubjects
        }
        numberOfRows()
        tableView.reloadData()
    }
    
    func updateDataIntoTextField(_ customTextField: UITextField, fromCell: Bool, _ index: Int) {
        
        if customTextField.tag == totalRows-1{
            if isFrom is LevelSubjectViewController{
                fromCell ? (customTextField.text = self.userData.otherSubject) : ( self.userData.otherSubject = customTextField.text!)
            }else{
                fromCell ? (customTextField.text = filterParameters.otherSubject) : ( filterParameters.otherSubject = customTextField.text!)
            }
            
        }
        
        if customTextField.tag == totalRows-2{
            if isFrom is LevelSubjectViewController{
                fromCell ? (customTextField.text = self.userData.language) : ( self.userData.language = customTextField.text!)
            }else{
                fromCell ? (customTextField.text = filterParameters.language) : ( filterParameters.language = customTextField.text!)
            }
            
        }
        
        if customTextField.tag == totalRows-3{
            if isFrom is LevelSubjectViewController{
                fromCell ? (customTextField.text = self.userData.music) : ( self.userData.music = customTextField.text!)
            }else{
                fromCell ? (customTextField.text = filterParameters.music) : ( filterParameters.music = customTextField.text!)
            }
            
        }
        
    }
    
//    func enableApplyButton(_ selectedSubjects: [Subject])->String{
////        var sub: String = ""
//        let tempStr = selectedSubjects.filter{ $0.name == "Others".localized ||
//            $0.name == "Languages".localized  ||
//            $0.name == "Music".localized
//        }
////        for index in 0..<selectedSubjects.count{
////            if selectedSubjects[index].name == "Others".localized || selectedSubjects[index].name == "Languages".localized || selectedSubjects[index].name == "Music".localized{
////                sub = selectedSubjects[index].name
////            }
////        }
//        return tempStr[0].name
//    }
}

//MARK: -------------- @IBActions

extension SubjectListingViewController{
    
    @IBAction func applyBtnTapped(_ sender: UIButton) {
        
        switch screenType{
            
        case .Qualification:
            
            subjectListingViewControllerDelegate?.selectedQualification(selectedQualification)
            
        case .Subjects:
      
            if isFrom is LevelSubjectViewController{
                
                if selectedSubjects.count <= 0 {
                self.showSimpleAlertWithMessage("Please select at least one subject".localized)
                return
                
                }
                
                let otherSub = selectedSubjects.filter{ ($0.name == "Others".localized && userData.otherSubject.isEmpty) ||
                    ($0.name == "Languages".localized && userData.language.isEmpty)  ||
                    ($0.name == "Music".localized && userData.music.isEmpty) }
                
                guard otherSub.count>0, let errorSub = otherSub[0].name else{
                    subjectListingViewControllerDelegate?.selectedSubject(selectedSubjects)
                    self.navigateBack(sender: sender)
                    return
                }
                
                if errorSub == "Others".localized {
                    self.showSimpleAlertWithMessage("Please enter other subject".localized)
                    return
                }else if errorSub == "Music".localized{
                    self.showSimpleAlertWithMessage("Please specify your instrument".localized)
                    return
                }else if errorSub == "Languages".localized{
                    self.showSimpleAlertWithMessage("Please enter Language".localized)
                    return
                }
                
            }else{
                
                let otherSub = selectedSubjects.filter{ ($0.name == "Others".localized && filterParameters.otherSubject.isEmpty) ||
                    ($0.name == "Languages".localized && filterParameters.language.isEmpty)  ||
                    ($0.name == "Music".localized && filterParameters.music.isEmpty) }
                
                guard otherSub.count>0, let errorSub = otherSub[0].name else{
                    subjectListingViewControllerDelegate?.selectedSubject(selectedSubjects)
                    self.navigateBack(sender: sender)
                    return
                }
                
                if errorSub == "Others".localized {
                    self.showSimpleAlertWithMessage("Please enter other subject".localized)
                    return
                }else if errorSub == "Music".localized{
                    self.showSimpleAlertWithMessage("Please specify your instrument".localized)
                    return
                }else if errorSub == "Languages".localized{
                    self.showSimpleAlertWithMessage("Please enter Language".localized)
                    return
                }
            }
            
            
        case .Level:
            
            if isFrom is LevelSubjectViewController{
                if selectedLevel.count > 0 {
                    subjectListingViewControllerDelegate?.selectedLevel(selectedLevel)
                }else{
                    self.showSimpleAlertWithMessage("Please select at least one level".localized)
                    return
                }
            }else{
                subjectListingViewControllerDelegate?.selectedLevel(selectedLevel)
            }
        }
        
        self.navigateBack(sender: sender)
        
    }
    
    
}

//MARK: -------------- TableView Delegates/DataSources

extension SubjectListingViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch screenType{
            
        case .Qualification:
            let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire(), for: indexPath) as! CommonCheckBoxCell
            if qualifications.count > 0{
                
                let qualification = qualifications[indexPath.row]
                var isSelected = false
                let index = selectedQualification.index{ qual in
                    return qual.id == qualification.id
                }
                if let subIndex = index , subIndex > -1{
                    isSelected = true
                }
                cell.configureCellWithQualification(qualification, isSelected)
                
            }
            return cell
            
            
        case .Subjects:
            
            guard let subject = subjectListing[safe: indexPath.row], subject.isOption == "1" else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire(), for: indexPath) as! CommonCheckBoxCell
                let subject = subjectListing[indexPath.row]
                var isSelected = false
                let index = selectedSubjects.index{ (sub)->Bool in
                    return sub.id == subject.id
                }
                if let subindex = index, subindex > -1{
                    isSelected = true
                }
                cell.configureCellWithSubject(subject, isSelected)
                if indexPath.row == subjectListing.count ||  indexPath.row == subjectListing.count-1{
                    cell.sepratorView.isHidden = true
                }else{
                    cell.sepratorView.isHidden = false
                }
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: OtherSubjectTableViewCell.cellIdentifire(), for: indexPath) as! OtherSubjectTableViewCell
            cell.otherSubjectTextField.delegate = self
            var isSelected = false
            let index = selectedSubjects.index{ (sub)->Bool in
                return sub.id == subject.id
            }
            if let subindex = index, subindex > -1{
                isSelected = true
            }
            cell.configureCell(subject, isSelected, indexPath.row)
            updateDataIntoTextField(cell.otherSubjectTextField, fromCell: true, indexPath.row)
//            let subjectData = subjectListing[indexPath.row]
            
            return cell
            
            
        case .Level:
            let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire(), for: indexPath) as! CommonCheckBoxCell
            let level = levelPagination.levels[indexPath.row]
            var isSelected = false
            let index = selectedLevel.index{ (lev)->Bool in
                return lev.id == level.id
            }
            if let subindex = index, subindex > -1{
                isSelected = true
            }
            if isFrom is LevelSubjectViewController{
                cell.isFrom = "LevelSubjectViewController"
            }else{
                
            }
            cell.configureCellWithLevel(level, isSelected)
            return cell
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if screenType == .Subjects{
            //            if indexPath.row == totalRows-1 || indexPath.row == totalRows-2 {
            //                return UITableView.automaticDimension
            //            }
            
            if let subject = subjectListing[safe: indexPath.row], subject.isOption == "1"{
                return UITableView.automaticDimension
            }
        }
        return CommonCheckBoxCell.cellHeight()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        switch screenType{
            
        case .Qualification:
            let qualification = qualifications[indexPath.row]
            let index = selectedQualification.index { (sub) -> Bool in
                return sub.id == qualification.id
            }
            if let subIndex = index, subIndex > -1{
                selectedQualification.remove(at: subIndex)
            }else{
                selectedQualification.append(qualification)
            }
            
        case .Subjects:
            let subject = subjectListing[indexPath.row]
            let index = selectedSubjects.index { (sub) -> Bool in
                return sub.id == subject.id
            }
            if let subIndex = index, subIndex > -1{
                selectedSubjects.remove(at: subIndex)
            }else{
                selectedSubjects.append(subject)
            }
            // find selected subjects
            var subs: [String] = Array()
            if let cell = tableView.cellForRow(at: indexPath) as? OtherSubjectTableViewCell{
                for index in 0..<selectedSubjects.count{
                    subs.append(selectedSubjects[index].name)
                }
                if subs.contains("Others".localized) || subs.contains("Languages".localized) || subs.contains("Music".localized){
                    cell.otherSubjectTextFieldholderView.isHidden = false
                }else{
                    cell.otherSubjectTextFieldholderView.isHidden = true
                }
            }
            
            
        case .Level:
            if isFrom is LevelSubjectViewController{
                let level = levelPagination.levels[indexPath.row]
                let index = selectedLevel.index { (level_) -> Bool in
                    return level_.id == level.id
                }
                if let subindex = index, subindex > -1{
                    selectedLevel.remove(at: subindex)
                }else{
                    selectedLevel.removeAll()
                    selectedLevel.append(level)
                }
            }else{
                let level = levelPagination.levels[indexPath.row]
                let index = selectedLevel.index { (level_) -> Bool in
                    return level_.id == level.id
                }
                if let subindex = index, subindex > -1{
                    selectedLevel.remove(at: subindex)
                }else{
                    selectedLevel.append(level)
                }
            }
            
            
        }
        tableView.reloadData()
        
    }
    
    
}

// MARK: -------------- API Implementation

extension SubjectListingViewController: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        updateDataIntoTextField(textField , fromCell: false, textField.tag)
        
//        if selectedSubjects.count > 0{
//            for index in 0..<selectedSubjects.count{
//                if selectedSubjects[index].name == "Others".localized{
//                    //selectedSubjects[index].name = textField.text ?? ""
//                    //selectedSubjects[index].enteredName = textField.text
//                    if isFrom is LevelSubjectViewController{
//                        self.userData.otherSubject = textField.text
//                    }else{
//                        filterParameters.otherSubject = textField.text
//                    }
//
//                    //otherSubject.otherSubject = (textField.text)
//                }
//            }
//        }
        
    }
}

extension SubjectListingViewController: CommonCheckBoxCellDelegate {
    
    func checkboxTappedAtIndex(index: Int) {
    }
    
}

// MARK: -------------- API Implementation

extension SubjectListingViewController{
    
    // Fetch subjects
    
    fileprivate  func fetchSubjects(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let subjectObserver = ApiManager.shared.apiService.fetchSubjects(1)
        let subjectDisposable = subjectObserver.subscribe(onNext: { (paginationObject) in
            
            self.pagination = Pagination.init()
            self.subjectListing = paginationObject.subjects
            
            //  var subjects = paginationObject.subjects
            
            //            var music_Index: Int?
            //            var art_index: Int?
            //
            //            for (index, subject) in subjects.enumerated() {
            //                if subject.id == "17"{
            //                    music_Index = index
            //                }else if subject.id == "18"{
            //                    art_index = index
            //                }
            //            }
            //
            //            if let m_index = music_Index, let a_index = art_index{
            //                subjects.swapAt(m_index, a_index)
            //            }
            //
            //            self.subjectListing = subjects
            
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                self.originalSubjects = self.subjectListing
                if self.isFrom is LevelSubjectViewController{
                    self.setSelectedFieldsForLevelSubjectVC()
                }else{
                    self.setSelectedFields()
                }
                self.numberOfRows()
                self.tableView.reloadData()
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
            }
        })
        subjectDisposable.disposed(by: disposableBag)
    }
    
    // Fetch qualifications
    
    fileprivate func fetchQualification(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let qualificationObserver = ApiManager.shared.apiService.fetchQualification()
        let qualificationDisposable = qualificationObserver.subscribe(onNext: { (qualifications) in
            self.qualifications = qualifications
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                self.setSelectedFields()
                self.numberOfRows()
                self.tableView.reloadData()
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
            }
        })
        return qualificationDisposable.disposed(by: disposableBag)
    }
    
    // Fetch levels
    
    func fetchLevels(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let levelObserver = ApiManager.shared.apiService.fetchLevels(1)
        let levelDisposable = levelObserver.subscribe(onNext: { (paginationObject) in
            self.levelPagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                
                if self.isFrom is LevelSubjectViewController{
                    self.setSelectedFieldsForLevelSubjectVC()
                }else{
                    self.setSelectedFields()
                }
                
                self.numberOfRows()
                self.tableView.reloadData()
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
            }
        })
        return levelDisposable.disposed(by: disposableBag)
    }
    
    
}
