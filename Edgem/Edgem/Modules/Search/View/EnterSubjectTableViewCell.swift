//
//  EnterSubjectTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 21/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class EnterSubjectTableViewCell: UITableViewCell {
    
    var placeHolder = NSMutableAttributedString()

    @IBOutlet weak var enterSubjectTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetUP()
    }
    
    func initialSetUP(){
        // placeholder setup
        let placeholderText = "Enter your subject"
        placeHolder = NSMutableAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.font:UIFont(name: QuicksandRegular, size: 15.0)!])
        placeHolder.addAttributes([NSAttributedString.Key.foregroundColor: theamGrayColor], range: NSRange(location: 0, length: placeholderText.count))
        enterSubjectTextField.attributedPlaceholder = placeHolder
    }
    
    class func cellIdentifire()->String{
        return "EnterSubjectTableViewCell"
    }

    class func cellHeight()->CGFloat{
        return 46.0
    }
}
