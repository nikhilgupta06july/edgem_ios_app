//
//  SwiftyStarRatingTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 14/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import Cosmos

protocol SwiftyStarRatingTableViewCellDelegate {
    func ratingValueChanged(_ ratingValue: Double)
}

class SwiftyStarRatingTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    var swiftyStarRatingTableViewCellDelegate: SwiftyStarRatingTableViewCellDelegate?
    var imageName = ""
    // MARK: - @IBOutlets
    
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var anyLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var starRatingView: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetUP()
        starRatingView.settings.starMargin = Double(((self.frame.width-60)-40*5)/5)
        starRatingView.didTouchCosmos = didTouchCosmos
        starRatingView.didFinishTouchingCosmos = didFinishTouchingCosmos
    }


    // MARK: Private Functions
    

    private func didTouchCosmos(_ rating: Double) {
        //print("---\(Float(rating))")
    }
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        print(rating)
        imageName = rating < 5 ? "deselected" : "selected"
        checkBoxButton.setImage(UIImage(named: imageName), for: .normal)
        swiftyStarRatingTableViewCellDelegate?.ratingValueChanged(rating)
    }
    
    func configureCellWithRating(_ rating: String) {
        
          starRatingView.rating = rating == "any" ? 5 : (rating as NSString).doubleValue
          starRatingView.settings.starMargin = Double(((self.frame.width-60)-40*5)/4)
    }
    
    func initialSetUP(){
        starRatingView.rating = 5
    }
    
    class func cellIdentifire()->String{
        return "SwiftyStarRatingTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 150.0
    }
    
    
    
    // MARK: - @IBActions
    
    @IBAction func checkBoxButtonTapped(_ sender: UIButton) {
        
        if sender.hasImage(named: "selected", for: .normal) {
            starRatingView.rating = 0
            sender.setImage(UIImage(named: "deselected"), for: .normal)
        }else{
            starRatingView.rating = 5
             sender.setImage(UIImage(named: "selected"), for: .normal)
        }
        swiftyStarRatingTableViewCellDelegate?.ratingValueChanged(starRatingView.rating)
    }

}
