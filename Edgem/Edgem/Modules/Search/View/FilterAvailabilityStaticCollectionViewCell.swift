//
//  FilterAvailabilityStaticCollectionViewCell.swift
//  Edgem
//
//  Created by Hipster on 25/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class FilterAvailabilityStaticCollectionViewCell: UICollectionViewCell {
    
    class func cellIdentifier() -> String {
        return "FilterAvailabilityStaticCollectionViewCell"
    }
    
}
