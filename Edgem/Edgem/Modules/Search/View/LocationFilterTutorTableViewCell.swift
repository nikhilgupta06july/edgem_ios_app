//
//  LocationFilterTutorTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 23/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RangeSeekSlider

protocol LocationFilterTutorTableViewCellDelegate {
    func sliderValue(_ minValue: String, _ maxVlaue: String)
}

class LocationFilterTutorTableViewCell: UITableViewCell, RangeSeekSliderDelegate {
    
    // MARK: ----------Properties
    var locationFilterTutorTableViewCellDelegate: LocationFilterTutorTableViewCellDelegate?
    var userType = ""
    
    // MARK: ----------@IBOutlets

    @IBOutlet weak var locationSlider: RangeSeekSlider!{
        didSet{
               locationSlider.delegate = self
        }
    }
    @IBOutlet weak var locationTitleLabel: UILabel!
    @IBOutlet weak var sliderTitleLabel: UILabel!
    @IBOutlet weak var sepratorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetUp()
        locationSlider.handleImage = UIImage(named: "sliderHandle")
         //locationSlider.handleImage = UIImage.init(named: "sliderHandle")
    }
    
    // MARK: ----------Private Functions

    class func cellIdentifire()->String{
        return "LocationFilterTutorTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 160
    }
    
    func initialSetUp(){
        userType = UserStore.shared.selectedUserType
        if userType == "Tutor"{
            sliderTitleLabel.isHidden = false
            locationSlider.minValue = MIN_DISTANCE
            locationSlider.maxValue = MAX_DISTANCE
            //locationSlider.numberFormatter.numberStyle = .
        }else{
             sliderTitleLabel.isHidden = true
             locationSlider.minValue = 0//20
             locationSlider.maxValue = 150
             locationSlider.numberFormatter.numberStyle = .currency
        }
    }
    
    func configureCell(_ minValue: String, _ maxValue: String){
        
        locationSlider.numberFormatter.numberStyle = .currency
        if let minValue = NumberFormatter().number(from: minValue) {
            locationSlider.selectedMinValue = CGFloat(truncating: minValue)
        }

        if let maxValue = NumberFormatter().number(from: maxValue) {
            locationSlider.selectedMaxValue = CGFloat(truncating: maxValue)
        }
        
    }
    
    func ConfigureCellForTutor(_ minValue: String, _ maxValue: String){
        
        locationSlider.numberFormatter.positiveSuffix = " Km"
        sepratorView.backgroundColor = UIColor.white
        
        
        if let minValue = NumberFormatter().number(from: minValue) {
            locationSlider.selectedMinValue = CGFloat(truncating: minValue)
             //ocationSlider.minValue = CGFloat(truncating: minValue)
        }
        
        if let maxValue = NumberFormatter().number(from: maxValue) {
            locationSlider.selectedMaxValue = CGFloat(truncating: maxValue)
            //locationSlider.maxValue = locationSlider.selectedMaxValue
        }
        //locationSlider.minValue = locationSlider.selectedMinValue
        //locationSlider.maxValue = locationSlider.selectedMaxValue
        
        configureSliderLabel(Int(locationSlider.selectedMinValue), Int( locationSlider.selectedMaxValue))
    }
    
    func configureSliderLabel(_ minValue: Int, _ maxValue: Int){
        //  sliderTitleLabel.text = "Within \(maxValue) Km from \(minValue) Km"
          let formattedString = NSMutableAttributedString()
          formattedString
            .normal("Within ")
            .bold("\(maxValue) Km")
//            .normal(" from")
//            .bold(" \(minValue) Km")
          sliderTitleLabel.attributedText = formattedString
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        locationSlider.hideLabels = false
        //locationSlider.handleImage = UIImage.init(named: "sliderHandle")
        locationSlider.lineHeight = 12
        locationSlider.tintColor = theamBorderGrayColor
        locationSlider.minLabelColor = theamBlackColor
        locationSlider.minLabelFont = UIFont(name: SourceSansProRegular, size: 15)!
        locationSlider.maxLabelColor = theamBlackColor
        locationSlider.maxLabelFont = UIFont(name: SourceSansProRegular, size: 15)!
        locationSlider.tag = 0
        locationSlider.enableStep = true
        locationSlider.step = 1
        locationSlider.colorBetweenHandles = theamAppColor
    }
    

    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        print(minValue,maxValue)
        locationFilterTutorTableViewCellDelegate?.sliderValue("\(minValue)", "\(maxValue)")
        //sliderTitleLabel.text = "Within \(maxValue) Km from \(minValue) Km"
        configureSliderLabel(Int(minValue), Int(maxValue))
    }

}
