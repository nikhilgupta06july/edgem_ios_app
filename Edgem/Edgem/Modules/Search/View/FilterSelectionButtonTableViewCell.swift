//
//  FilterSelectionButtonTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 27/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol FilterSelectionButtonTableViewCellDelegate{
    func resetButtonTapped()
    func applyButtonTapped()
}

class FilterSelectionButtonTableViewCell: UITableViewCell {
    
    // MARK: ------ Properties
    
    var filterSelectionButtonTableViewCellDelegate: FilterSelectionButtonTableViewCellDelegate?
    
    // MARK: ------ @IBOutlets
    
    @IBOutlet weak var resetButtonHolder: UIView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var applyButtonHolder: UIView!
    @IBOutlet weak var applyButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: ------- @IBActions
    @IBAction func resetButtonTapped(_ sender: UIButton) {
       filterSelectionButtonTableViewCellDelegate?.resetButtonTapped()
    }
    
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        filterSelectionButtonTableViewCellDelegate?.applyButtonTapped()
    }
    
    
    // MARK: ------- Private Functions
    
    class func cellIdentifire()-> String{
    return "FilterSelectionButtonTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 90
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Reset button attributes
        resetButton.layer.cornerRadius = 5
        resetButton.layer.borderWidth = 1
        resetButton.layer.borderColor = theamGrayColor.cgColor
        resetButton.clipsToBounds = true
        
        // Reset button shadow
        resetButtonHolder.layer.shadowColor = UIColor.black.cgColor
        resetButtonHolder.layer.shadowOpacity = 0.12
        resetButtonHolder.layer.shadowRadius = 4
        resetButtonHolder.layer.shadowOffset = .zero
        resetButtonHolder.layer.shadowPath = UIBezierPath(rect: resetButtonHolder.frame).cgPath
        
        // apply button attributes
        applyButton.layer.cornerRadius = 5
       // applyButton.layer.borderWidth = 1
        //applyButton.layer.borderColor = theamGrayColor.cgColor
        applyButton.backgroundColor = buttonBackGroundColor
        applyButton.clipsToBounds = true
        
        // apply button shadow
        applyButtonHolder.layer.shadowColor = UIColor.black.cgColor
        applyButtonHolder.layer.shadowOpacity = 0.12
        applyButtonHolder.layer.shadowRadius = 4
        applyButtonHolder.layer.shadowOffset = .zero
        applyButtonHolder.layer.shadowPath = UIBezierPath(rect: resetButtonHolder.frame).cgPath
    }
    
    
}
