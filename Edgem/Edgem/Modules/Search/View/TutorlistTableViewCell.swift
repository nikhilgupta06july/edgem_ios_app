//
//  TutorlistTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 19/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import SwiftyStarRatingView

protocol TutorlistTableViewCellDelegate{
    func bookmarksBtnTapped(_ status: Bool, index: Int)
}

class TutorlistTableViewCell: UITableViewCell {
    
    //MARK: -------- Properties
    var tutorlistTableViewCellDelegate: TutorlistTableViewCellDelegate?
    var isBookMarksBtnTapped = false
    var imageURLString: String?
    var tutorsImage: ((UIImage)->Void)?
    
    //MARK: -------- @IBOutlets
    
    @IBOutlet weak var bookmarksBtn: UIButton!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var subjectsLabel: UILabel!
    @IBOutlet weak var subjectHeaderLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var lavelHeaderLabel: UILabel!
    @IBOutlet weak var perSessionAmmountLabel: UILabel!
    @IBOutlet weak var perSessionHeaderLabel: UILabel!
    @IBOutlet weak var educationLabel: UILabel!
    @IBOutlet weak var ratingNumbersLabel: UILabel!
    @IBOutlet weak var tutorRatingView: SwiftyStarRatingView!
    @IBOutlet weak var tutorNameLabel: UILabel!
    @IBOutlet weak var tutorProfileImageView: UIImageView!
    @IBOutlet weak var preferredLabel: UILabel!
    @IBOutlet weak var preferredContainerView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetUP()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        subjectsLabel.text = ""
        perSessionAmmountLabel.text = ""
        tutorNameLabel.text = ""
        educationLabel.text = ""
        tutorProfileImageView.image = nil
    }
    
    
    //MARK: ------------ Private Functions --------

    class func cellIdentifire()->String{
        return "TutorlistTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 226
    }
    
    func getHighestRate(levels: [Level]) -> Double{
        
        var upperRate = 0.0
        
        let rates_in_double = levels.map{ _level -> Double  in
            
            if let _price = _level.price{
                return (_price as NSString).doubleValue
            }
            return 0.0
            }.filter{ $0 != 0.0}
        
        
        let sorted_rates = rates_in_double.sorted { (v1, v2) -> Bool in
            v1 > v2
        }
        
        if sorted_rates.count > 1{
            upperRate = sorted_rates[0]
            return upperRate
        }else if sorted_rates.count == 1{
            upperRate = sorted_rates[0]
            return upperRate
        }
        return 0.0
    }
    
    func getIndexAndHighestRateOfSubject(subjects: [Subject])->Int?{
        var data:[(Int,Double)] = [(Int,Double)]()
        
        for (index, subject) in subjects.enumerated(){
              data.append((index, getHighestRate(levels: subject.levels)))
        }
        
        
        
        if data.count > 0{
            let sorted_data = data.sorted{(v1,v2) -> Bool  in
                v1.1 > v2.1
            }
            return (sorted_data[0].0)
        }
        return nil
    }
    
    func configureCell(with tutor: Tutor, indexPath: IndexPath){
        
         // Tutor Name
        tutorNameLabel.text = tutor.firstName //+ " " +  tutor.lastName
        
        // RatePerSession
        if let subject = tutor.subjects, let index = getIndexAndHighestRateOfSubject(subjects: subject), let subject1 = subject[safe: index]{
            perSessionAmmountLabel.text = GetSubjectsRange.get(from: subject1.levels)
        }
        
        // Subject on cell
        if let subjects = tutor.subjects, (subjects.count) > 0{
            let subjectTitle = subjects.map { $0.name ?? "" }
            self.subjectsLabel.text = subjectTitle.joined(separator: ", ")
        }
        
        // Qualification on cell
        if let qualifications = tutor.qualification, (qualifications.count) > 0{
            let qualificationTitle = qualifications.map { $0.name! }
            self.educationLabel.text = qualificationTitle.joined(separator: ", ")
        }

        // Level on cell
        if let levels = tutor.levels, (levels.count) > 0{
            let levelTitle = levels.map { $0.name! }
            self.levelLabel.text = levelTitle.joined(separator: ", ")
        }
        
        // ProfileImage

        loadImages(tutor)
        
        //Rating label
        ratingNumbersLabel.text = "(\(tutor.rating))"
        
         //Rating
        tutorRatingView.value = CGFloat((tutor.averageRating as NSString).floatValue)
        
        // bookmarks button
        bookmarksBtn.setImage(tutor.isbookmark ? UIImage(named: "activeBookmarks") : UIImage(named: "inactiveBookmark") , for: .normal)
        bookmarksBtn.tag = indexPath.row
    }
    
    fileprivate func loadImages(_ tutor: Tutor) {
        let imageProvider = ImageProvider()
        if let image = imageCache.image(forKey: "\(String(describing: tutor.tutorID))"){
            self.tutorProfileImageView.image = image
            tutorsImage?( image)
        }else{
            tutorProfileImageView.image = UIImage(named: "femaleIcon")
            let imageURL = tutor.realImage
            if let url = URL(string: imageURL) {
                imageProvider.requestImage(from: url) { (image) in
                    imageCache.add(image, forKey: "\(String(describing:  tutor.tutorID))")
                    self.tutorProfileImageView.image = image
                    self.tutorsImage?( image)
                }
            }
        }
      
    }
    
    func initialSetUP(){
        
        tutorRatingView.filledStarImage = UIImage(named: "filledStarImage")
        tutorRatingView.halfStarImage = UIImage(named: "halfStarImage")
        tutorRatingView.emptyStarImage = UIImage(named: "emptyStarImage")
        tutorRatingView.isEnabled = false
        
        tutorNameLabel.font = UIFont(name: SourceSansProBold, size: 16)
        tutorNameLabel.textColor = theamBlackColor
        
        ratingNumbersLabel.font = UIFont(name: SourceSansProRegular, size: 15)
        ratingNumbersLabel.textColor = theamBlackColor
        
        educationLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        educationLabel.textColor = theamBlackColor
        
        perSessionHeaderLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        perSessionHeaderLabel.textColor = theamGrayColor
        
        perSessionAmmountLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        perSessionAmmountLabel.textColor = theamBlackColor
        
        lavelHeaderLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        lavelHeaderLabel.textColor = theamGrayColor
        
        levelLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        levelLabel.textColor = theamBlackColor
        
        subjectHeaderLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        subjectHeaderLabel.textColor = theamGrayColor
        
        subjectsLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        subjectsLabel.textColor = theamBlackColor
        
        tutorProfileImageView.layer.cornerRadius = tutorProfileImageView.frame.width/2
        tutorProfileImageView.clipsToBounds = true
        
        preferredContainerView.layer.cornerRadius = 4
        preferredContainerView.clipsToBounds = true
        
//        bookmarksBtn.setImage(UIImage(named: "activeBookmarks"), for: .normal)
        bookmarksBtn.setImage(UIImage(named: "inactiveBookmark"), for: .normal)
        
        //        Holder View
         self.holderView.layer.cornerRadius = 4
         self.holderView.layer.borderWidth = 1
         self.holderView.layer.borderColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1).cgColor
         
         holderView.layer.shadowColor = UIColor.black.cgColor
         holderView.layer.shadowOpacity = 0.15
         holderView.layer.shadowRadius = 4
         holderView.layer.shadowOffset = CGSize(width: 0, height: 2)
        
    }
    
    func updateShadow(_ update: Bool){
        if update{
            holderView.layer.shadowColor = theamAppColor.cgColor
            holderView.layer.shadowOpacity = 0.15
            holderView.layer.shadowRadius = 4
            holderView.layer.shadowOffset = .zero//CGSize(width: 0, height: 2)
        }else{
            holderView.layer.shadowColor = UIColor.black.cgColor
            holderView.layer.shadowOpacity = 0.15
            holderView.layer.shadowRadius = 4
            holderView.layer.shadowOffset = CGSize(width: 0, height: 2)
        }
    }

}

  //MARK: -------- @IBAction -----------------

extension TutorlistTableViewCell{
    
    @IBAction func bookmarksBtnTapped(_ sender: UIButton) {
        
        updateShadow(sender.isSelected)
        isBookMarksBtnTapped = sender.hasImage(named: "activeBookmarks", for: .normal) == true ? false : true

    tutorlistTableViewCellDelegate?.bookmarksBtnTapped(isBookMarksBtnTapped, index: sender.tag)
        bookmarksBtn.setImage(UIImage(named: (isBookMarksBtnTapped ? "activeBookmarks" : "inactiveBookmark")), for: .normal)
        
    }
    
}




