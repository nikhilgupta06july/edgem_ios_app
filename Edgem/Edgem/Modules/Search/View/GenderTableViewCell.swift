//
//  GenderTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 23/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol GenderTableViewCellDelegate{
    func genderStatus(_ status: String)
}

class GenderTableViewCell: UITableViewCell {
    
    //MARK: ---------- Properties
    var gender: String!
    var genderTableViewCellDelegate: GenderTableViewCellDelegate?
    
    var isFemaleButtunSelected = false{
        didSet{
            if isFemaleButtunSelected{
                 femaleButton.setImage(UIImage(named: "genderSelected"), for: .normal)
                 isAnyButtunSelected = false
                 isMaleButtunSelected = false
                 genderTableViewCellDelegate?.genderStatus("Female")
            }else{
                 femaleButton.setImage(UIImage(named: "genderDeselcetd"), for: .normal)
            }
        }
    }
    
    var isMaleButtunSelected = false{
        didSet{
            if isMaleButtunSelected{
                 maleButton.setImage(UIImage(named: "genderSelected"), for: .normal)
                 isAnyButtunSelected = false
                 isFemaleButtunSelected = false
                 genderTableViewCellDelegate?.genderStatus("Male")
            }else{
                  maleButton.setImage(UIImage(named: "genderDeselcetd"), for: .normal)
            }
        }
    }
    
    var isAnyButtunSelected = true{
        didSet{
            if isAnyButtunSelected{
                selectionButton.setImage(UIImage(named: "selected"), for: .normal)
                isMaleButtunSelected = false
                isFemaleButtunSelected = false
                genderTableViewCellDelegate?.genderStatus("any")
            }else{
                  selectionButton.setImage(UIImage(named: "deselected"), for: .normal)
            }
        }
    }
    
    // MARK: ---------- @IBOutlets
    
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var maleButton: ButtonWithImage!
    @IBOutlet weak var selectionButton: UIButton!
    @IBOutlet weak var femaleButton: ButtonWithImage!
    @IBOutlet weak var anyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetUP()
    }
    
    // MARK: ---------- Private Functions
    
    func initialSetUP(){
        cellTitle.text = "Gender".localized
        cellTitle.textColor = theamBlackColor
        cellTitle.font = UIFont(name: QuicksandMedium, size: 15)
        
        anyLabel.text = "Any".localized
        anyLabel.textColor = theamBlackColor
        anyLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        
        maleButton.setTitle("Male".localized, for: .normal)
        maleButton.titleLabel?.font = UIFont(name: QuicksandRegular, size: 15)
        
        femaleButton.setTitle("Female".localized, for: .normal)
        femaleButton.titleLabel?.font = UIFont(name: QuicksandRegular, size: 15)
        
    }
    
    
    func configureCell(_ gender: String){
        if gender == "any".localized || gender == "Any".localized{
             isAnyButtunSelected = true
        }else if gender == "Male".localized{
             isMaleButtunSelected = true
        }else{
             isFemaleButtunSelected = true
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //male button properties
        maleButton.layer.borderWidth = 1
        maleButton.layer.cornerRadius = 4
        maleButton.layer.borderColor = buttonBorderColor.cgColor
        maleButton.backgroundColor = UIColor.white
        
        //female button properties
        femaleButton.layer.borderWidth = 1
        femaleButton.layer.cornerRadius = 4
        femaleButton.layer.borderColor = buttonBorderColor.cgColor
        femaleButton.backgroundColor = UIColor.white
    }
    
    class func cellIdentifire()->String{
        return "GenderTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 128.0
    }
}

    // MARK: ------------ @IBActions
extension GenderTableViewCell{
    
    @IBAction func selectionButtonTapped(_ sender: UIButton) {
        isAnyButtunSelected = sender.hasImage(named: "selected", for: .normal) == true ? false : true
    }
    
    @IBAction func maleButtonTapped(_ sender: ButtonWithImage) {
        isMaleButtunSelected = sender.hasImage(named: "genderSelected", for: .normal) == true ? false : true
    }
    
    
    @IBAction func femaleButtonTapped(_ sender: ButtonWithImage) {
        isFemaleButtunSelected = sender.hasImage(named: "genderSelected", for: .normal) == true ? false : true
    }
}
