//
//  SearchSubjectTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 07/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import AMPopTip

protocol SearchSubjectTableViewDelegate{
    func searchSubjectBtnTapped()
}

class SearchSubjectTableViewCell: UITableViewCell {
    
    var searchSubjectTableViewDelegate: SearchSubjectTableViewDelegate!
    
    let popTip = PopTip()
    let setUpToolTip = SetUpToolTip()
    var direction = PopTipDirection.up
    var topRightDirection = PopTipDirection.down
 
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var deviderView: UIView!
    @IBOutlet weak var subject: LabelWithPadding!
    @IBOutlet weak var listLabel: UILabel!
    @IBOutlet weak var toolTipBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetUP()
        setUpToolTip.initialSetUp(popTip)
        
        popTip.appearHandler = { popTip in
            self.toolTipBtn.setImage(UIImage(named: "tooltip_selected"), for: .normal)
        }
        popTip.dismissHandler = { popTip in
            self.toolTipBtn.setImage(UIImage(named: "tooltip_deselected"), for: .normal)
        }

    }
    
    
    // MARK: ----------- Private Functions
    
    private func initialSetUP(){
        // Title attributes
        subjectLabel.font = UIFont(name: QuicksandMedium, size: 15)
        subjectLabel.textColor = theamBlackColor
        
        // Label attributes
        subject.font = UIFont(name: QuicksandRegular, size: 15)
        subject.textColor = theamGrayColor
    }
    
    func configureCell(_ title: String){
        
        subjectLabel.font = UIFont(name: QuicksandMedium, size: 15)
        subjectLabel.textColor = theamBlackColor
        subjectLabel.text = title
        
        subject.text = "Select".localized
        subject.font = UIFont(name: QuicksandRegular, size: 15)
        subject.textColor = theamGrayColor
        
    }
    
    func configureCellWithSubjects(_ title: String, _ subjects: [Subject]){
        
         subjectLabel.text = title  // ------> cell title
        
        if subjects.count <= 0{
            subject.text = "Select".localized
            subject.textColor = theamGrayColor
        }else if subjects.count == 1{
            subject.text = subjects[0].enteredName != "" ?  subjects[0].enteredName : subjects[0].name
            subject.font = UIFont(name: QuicksandRegular, size: 15)
            subject.textColor = theamBlackColor
        }else if subjects.count > 1{
            subject.text = (subjects[0].enteredName != "" ?  subjects[0].enteredName : subjects[0].name) + (" ") + ("(+\(subjects.count - 1))")
            subject.font = UIFont(name: QuicksandRegular, size: 15)
            subject.textColor = theamBlackColor
        }
    }
    
    func configureCellWithQualifications(_ title: String, _ qualification: [Qualification]){
        
        subjectLabel.text = title  // ------> cell title
        
        if qualification.count <= 0{
            subject.text = "Select".localized
            subject.textColor = theamGrayColor
        }else if qualification.count == 1{
            subject.text = qualification[0].name
            subject.font = UIFont(name: QuicksandRegular, size: 15)
            subject.textColor = theamBlackColor
        }else if qualification.count > 1{
            subject.text = qualification[0].name + (" ") + ("(+\(qualification.count - 1))")
            subject.font = UIFont(name: QuicksandRegular, size: 15)
            subject.textColor = theamBlackColor
        }
    }
    
    func configureCellWithLevelsFromSearchController(_ title: String, _ levels: [Level]){
        
        subjectLabel.text = title  // ------> cell title
        
        if levels.count <= 0{
            subject.text = "Select".localized
            subject.textColor = theamGrayColor
        }else if levels.count == 1{
            subject.text = levels[0].name
            subject.font = UIFont(name: QuicksandRegular, size: 15)
            subject.textColor = theamBlackColor
        }else if levels.count > 1{
            subject.text = levels[0].name + (" ") + ("(+\(levels.count - 1))")
            subject.font = UIFont(name: QuicksandRegular, size: 15)
            subject.textColor = theamBlackColor
        }
    }
    
    func configureCellWithLevels(_ title: String, _ levels: [Level]){
        
        toolTipBtn.tag = 2
        
        subjectLabel.text = title  // ------> cell title
        
        if levels.count <= 0{
            subject.text = "Select".localized
            subject.textColor = theamGrayColor
        }else if levels.count == 1{
            subject.text = levels[0].name
            subject.font = UIFont(name: QuicksandRegular, size: 15)
            subject.textColor = theamBlackColor
        }else if levels.count > 1{
            subject.text = levels[0].name + (" ") + ("(+\(levels.count - 1))")
            subject.font = UIFont(name: QuicksandRegular, size: 15)
            subject.textColor = theamBlackColor
        }
    }
    
    func configureCellWithInfo(_ title: String, _ subjects: String){
         subjectLabel.text = title
        deviderView.isHidden = true
        subject.text = subjects
        subject.font = UIFont(name: QuicksandRegular, size: 15)
        subject.textColor = theamBlackColor
    }
    
    func configureCellWithSubjectsList(_ title: String, _ subjects: [Subject]){
        toolTipBtn.tag = 1
        subjectLabel.text = title  // ------> cell title
        deviderView.isHidden = true
        
        var subs: String = ""
        if subjects.count > 0{
            for index in 0..<subjects.count{
                 subs += "\u{2022} \(subjects[index].name!)\n"
            }
           
        }
        
        listLabel.text = subs//"\u{2022} English\n\u{2022} Maths\n\u{2022} sgdhd\n\u{2022}Sid"
        
        if subjects.count <= 0{
            subject.text = "Select".localized
            subject.textColor = theamGrayColor
        }else if subjects.count == 1{
            subject.text = subjects[0].name
            subject.font = UIFont(name: QuicksandRegular, size: 15)
            subject.textColor = theamBlackColor
        }else if subjects.count > 1{
            subject.text = subjects[0].name + (" ") + ("(+\(subjects.count - 1))")
            subject.font = UIFont(name: QuicksandRegular, size: 15)
            subject.textColor = theamBlackColor
        }
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        subject.layer.borderColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1).cgColor
        subject.layer.cornerRadius = 4
        subject.layer.borderWidth = 1
    }
    
    class func cellIdentifire()->String{
        return "SearchSubjectTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 115
    }
   
    @IBAction func tooTipButtonTapped(_ sender: UIButton) {

        var msg = ""
        if sender.tag == 1 {
            msg = ToolTipString.subjectInformation.description()
        }else{
            msg = ToolTipString.levelInformation.description()
        }
        popTip.show(text: msg, direction: .down, maxWidth: self.contentView.frame.width-40, in: self.contentView, from: sender.frame)
    }
    
}

extension SearchSubjectTableViewCell{
    @IBAction func searchSubjectButtonTapped(_ sender: UIButton) {
        if searchSubjectTableViewDelegate != nil{
            searchSubjectTableViewDelegate.searchSubjectBtnTapped()
        }
    }
}
