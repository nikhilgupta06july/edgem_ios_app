//
//  FilterAvailabilitySelectionTableViewCell.swift
//  Demo
//
//  Created by Hipster on 27/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class FilterAvailabilitySelectionTableViewCell: UITableViewCell {
    
    // MARK: ------- Properties
    
    var filterParams: FilterParams!
    
    // MARK: ------- @IBOutlets
    
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var sepratorViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if UserStore.shared.selectedUserType == "Tutor"{
            sepratorViewHeight.constant = 1
            sepratorView.backgroundColor = cellBackgroundColor
        }else{
            sepratorViewHeight.constant = 5
            sepratorView.backgroundColor = cellBackgroundColor
        }
        
    }
    
    // MARK: ------- Private Functions
    
    class func cellIdentifire()->String{
        return "FilterAvailabilitySelectionTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 265.0
    }
    
    func reloadCollectionViewData(){
        collectionView.reloadData()
    }

}

extension FilterAvailabilitySelectionTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard indexPath.item != 0 else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterAvailabilityStaticCollectionViewCell.cellIdentifier(), for: indexPath) as! FilterAvailabilityStaticCollectionViewCell
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterAvailabilityTimingCollectionViewCell.cellIdentifier(), for: indexPath) as! FilterAvailabilityTimingCollectionViewCell
        cell.configureCellForDay(indexPath.item, filterParams: filterParams)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == 0 {
            return CGSize(width: 50, height: 241)
        } else {
            return CGSize(width: (( UIScreen.main.bounds.width) - 95)/7, height: 241)
        }
    }
    
    
}
