//
//  StudentListTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 21/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol StudentListTableViewCellDelegate {
     func studentBookmarksBtnTapped(_ status: Bool, index: Int)
}

class StudentListTableViewCell: UITableViewCell {

    //MARK: -------- Properties ---------------
    
    var imageURLString: String?
    var isBookMarksBtnTapped = false
    var studentListTableViewCellDelegate: StudentListTableViewCellDelegate?
    
    //MARK: -------- @IBOutlets ---------------
    
    @IBOutlet weak var bookmarksBtn: UIButton!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var subjectsLabel: UILabel!
    @IBOutlet weak var subjectHeaderLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var lavelHeaderLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var distanceHeaderLabel: UILabel!
    @IBOutlet weak var studentNameLabel: UILabel!
    @IBOutlet weak var studentProfileImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetUP()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        studentProfileImageView.image = nil
        studentNameLabel.text = ""
        self.subjectsLabel.text = ""
    }
    
    
    //MARK: ------------ Private Functions --------
    
    class func cellIdentifire()->String{
        return "StudentListTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 170
    }
    
    func configureCell(with student: Student, indexPath: IndexPath){
        
         // student name
        studentNameLabel.text = student.firstName //+ " " + student.lastName
        
        // level
        if let levels = student.levels, (levels.count) > 0{
            let levelTitle = levels.map { $0.name! }
            self.levelLabel.text = levelTitle.joined(separator: ", ")
        }
        
        // Subjects
        if let subjects = student.subjects, (subjects.count) > 0{
            let subjectTitle = subjects.map { $0.name! }
            self.subjectsLabel.text = subjectTitle.joined(separator: ", ")
            
        // distance
            if let distance = student.distance{
                self.distanceLabel.text = distance
            }
        
        }
        
        // ProfileImage

        loadImages(student)
        
        // bookmarks button
        bookmarksBtn.setImage(student.isbookmark ? UIImage(named: "activeBookmarks") : UIImage(named: "inactiveBookmark"), for: .normal)
        bookmarksBtn.tag = indexPath.row
        
    }
    
    fileprivate func loadImages(_ student: Student) {
        
        let imageProvider = ImageProvider()
        if let image = imageCache.image(forKey: "\(String(describing: student.studentID))"){
            self.studentProfileImageView.image = image
        }else{
            studentProfileImageView.image = UIImage(named: "femaleIcon")
            let imageURL = student.realImage
            if let url = URL(string: imageURL) {
                imageProvider.requestImage(from: url) { (image) in
                    imageCache.add(image, forKey: "\(String(describing: student.studentID))")
                    self.studentProfileImageView.image = image
                }
            }
        }
        
    }
    
    func initialSetUP(){
        
        studentNameLabel.font = UIFont(name: SourceSansProBold, size: 16)
        studentNameLabel.textColor = theamBlackColor
        
        distanceHeaderLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        distanceHeaderLabel.textColor = theamGrayColor
        
        distanceLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        distanceLabel.textColor = theamBlackColor
        
        lavelHeaderLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        lavelHeaderLabel.textColor = theamGrayColor
        
        levelLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        levelLabel.textColor = theamBlackColor
        
        subjectHeaderLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        subjectHeaderLabel.textColor = theamGrayColor
        
        subjectsLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        subjectsLabel.textColor = theamBlackColor
        
        studentProfileImageView.layer.cornerRadius = studentProfileImageView.frame.width/2
        studentProfileImageView.clipsToBounds = true
        
        // Holder View
        self.holderView.layer.cornerRadius = 4
        self.holderView.layer.borderWidth = 1
        self.holderView.layer.borderColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1).cgColor
        
        holderView.layer.shadowColor = UIColor.black.cgColor
        holderView.layer.shadowOpacity = 0.15
        holderView.layer.shadowRadius = 4
        holderView.layer.shadowOffset = CGSize(width: 0, height: 2)
        
//        bookmarksBtn.setImage(UIImage(named: "activeBookmarks"), for: .normal)
        bookmarksBtn.setImage(UIImage(named: "inactiveBookmark"), for: .normal)
       
        
    }
    
    func updateShadow(_ update: Bool){
        if update{
            holderView.layer.shadowColor = theamAppColor.cgColor
            holderView.layer.shadowOpacity = 0.15
            holderView.layer.shadowRadius = 4
            holderView.layer.shadowOffset = CGSize(width: 0, height: 2)
        }else{
            holderView.layer.shadowColor = UIColor.black.cgColor
            holderView.layer.shadowOpacity = 0.15
            holderView.layer.shadowRadius = 4
            holderView.layer.shadowOffset = CGSize(width: 0, height: 2)
        }
    }
    
    // MARK: -------- @IBActions ---------------
    
    @IBAction func bookmarksBtnTApped(_ sender: UIButton) {
        
        updateShadow(sender.isSelected)
        isBookMarksBtnTapped = sender.hasImage(named: "activeBookmarks", for: .normal) == true ? false : true
        bookmarksBtn.setImage(UIImage(named: (isBookMarksBtnTapped ? "activeBookmarks" : "inactiveBookmark")), for: .normal)
        studentListTableViewCellDelegate?.studentBookmarksBtnTapped(isBookMarksBtnTapped, index: sender.tag)
        
        
    }
}

