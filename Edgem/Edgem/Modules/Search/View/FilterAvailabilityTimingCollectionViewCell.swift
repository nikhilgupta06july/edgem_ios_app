//
//  FilterAvailabilityTimingCollectionViewCell.swift
//  Demo
//
//  Created by Hipster on 27/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class FilterAvailabilityTimingCollectionViewCell: UICollectionViewCell {
    
    var days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    var day = "Mon"
    var filterParams: FilterParams!
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var slotOneSelectionButton: UIButton!
    @IBOutlet weak var slotTwoSelectionButton: UIButton!
    @IBOutlet weak var slotThreeSelectionButton: UIButton!
    
    class func cellIdentifier() -> String {
        return "FilterAvailabilityTimingCollectionViewCell"
    }
    
    func configureCellForDay(_ day: Int, filterParams: FilterParams) {
        let dayString = days[day-1]
        self.day = dayString
        self.filterParams = filterParams
        slotOneSelectionButton.tag = 0
        slotTwoSelectionButton.tag = 1
        slotThreeSelectionButton.tag = 2
        dayLabel.text = dayString.localized
        let schedule = filterParams.selectedTimeSlots[dayString]!
        for i in 0..<schedule.count{
            let isSelected = schedule[i] == "0" ? false : true
            switch i{
            case 0 :
                
                        let imageName = isSelected ? "active_avalibility" : "inactive_avalibility"
                        slotOneSelectionButton.setImage(UIImage.init(named: imageName), for: .normal)
                
            case 1 :
                
                        let imageName = isSelected ? "active_avalibility" : "inactive_avalibility"
                        slotTwoSelectionButton.setImage(UIImage.init(named: imageName), for: .normal)
                
            default :
                
                        let imageName = isSelected ? "active_avalibility" : "inactive_avalibility"
                        slotThreeSelectionButton.setImage(UIImage.init(named: imageName), for: .normal)
            }
        }
    }
    
    func handleTapForButton(_ checkboxButton: UIButton){
        let isSelected = checkboxButton.hasImage(named: "active_avalibility", for: .normal)
        var selectionArray = self.filterParams.selectedTimeSlots[self.day]!
        selectionArray[checkboxButton.tag] = isSelected ? "0" : "1"
        self.filterParams.selectedTimeSlots[self.day] = selectionArray
        let imageName = isSelected ? "checkbox_empty" : "checkbox"
        checkboxButton.setImage(UIImage.init(named: imageName), for: .normal)
        NotificationCenter.default.post(name: .filterAvailabilitySelected, object: nil, userInfo:nil)
    }
    
    @IBAction func slotOneButtonTapped(_ sender: UIButton) {
        handleTapForButton(sender)
    }
    
    @IBAction func slotTwoButtonTapped(_ sender: UIButton) {
        handleTapForButton(sender)
    }
    
    @IBAction func slowThreeButtonTapped(_ sender: UIButton) {
        handleTapForButton(sender)
    }
    
}
