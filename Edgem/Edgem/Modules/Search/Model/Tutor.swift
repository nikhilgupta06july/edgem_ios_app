//
//  Tutor.swift
//  Edgem
//
//  Created by Hipster on 04/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class TutorPagination: Unboxable {
    var tutor: [Tutor]
    var pagination: Pagination
    
    required init(unboxer: Unboxer) throws {
        self.tutor = try unboxer.unbox(key: "filetr_tutors")
        self.pagination = try unboxer.unbox(key: "pagination")
    }
    
}

class Tutor: Unboxable{
    
    let tutorID: Int
    let firstName: String
    let lastName: String
    let ratePerHour: String
    let rating: Int
    let averageRating: String
    let subjects: [Subject]?
    let qualification: [Qualification]?
    let levels: [Level]?
    let realImage: String
    var isbookmark: Bool
    
    
    init(tutor:Tutor){
        self.tutorID = tutor.tutorID
        self.firstName = tutor.firstName
        self.lastName = tutor.lastName
        self.ratePerHour = tutor.ratePerHour
        self.rating = tutor.rating
        self.averageRating = tutor.averageRating
        self.subjects = tutor.subjects
        self.qualification = tutor.qualification
        self.levels = tutor.levels
        self.realImage = tutor.realImage
        self.isbookmark = tutor.isbookmark
    }
    
    required init(unboxer: Unboxer) throws {
        self.tutorID = unboxer.unbox(key: "tutor_id") ?? 0
        self.firstName = unboxer.unbox(key: "first_name") ?? ""
        self.lastName = unboxer.unbox(key: "last_name") ?? ""
        self.ratePerHour = unboxer.unbox(key: "rate_per_hour") ?? ""
        self.rating = unboxer.unbox(key: "ratings") ?? 0
        self.averageRating = unboxer.unbox(key: "average_ratings") ?? ""
        self.subjects = unboxer.unbox(key: "subjects")
        self.qualification = unboxer.unbox(key: "qualification")
        self.levels = unboxer.unbox(key: "level")
        self.realImage = unboxer.unbox(key: "real_image") ?? ""
        self.isbookmark = unboxer.unbox(key: "is_bookmark") ?? false
        
    }
    
}
