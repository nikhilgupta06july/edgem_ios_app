//
//  Student.swift
//  Edgem
//
//  Created by Hipster on 05/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox


class StudentPagination: Unboxable {
    var student: [Student]
    var pagination: Pagination
    
    required init(unboxer: Unboxer) throws {
        self.student = try unboxer.unbox(key: "filetr_students")
        self.pagination = try unboxer.unbox(key: "pagination")
    }
    
}

class Student: Unboxable{
    
    let studentID: Int
    let firstName: String
    let lastName: String
    let subjects: [Subject]?
    let address: [LocationData]?
    let levels: [Level]?
    let realImage: String
    var isbookmark: Bool
    var distance: String?
    
    init(student:Student){
        self.studentID = student.studentID
        self.firstName = student.firstName
        self.lastName = student.lastName
        self.subjects = student.subjects
        self.address = student.address
        self.levels = student.levels
        self.realImage = student.realImage
        self.isbookmark = student.isbookmark
        self.distance = student.distance
    }
    
    required init(unboxer: Unboxer) throws {
        self.studentID = unboxer.unbox(key: "student_id") ?? 0
        self.firstName = unboxer.unbox(key: "first_name") ?? ""
        self.lastName = unboxer.unbox(key: "last_name") ?? ""
        self.subjects = unboxer.unbox(key: "subjects")
        self.address = unboxer.unbox(key: "address")
        self.levels = unboxer.unbox(key: "levels")
        self.realImage = unboxer.unbox(key: "real_image") ?? ""
        self.isbookmark = unboxer.unbox(key: "is_bookmark") ?? false
        self.distance = unboxer.unbox(key: "distance") ?? ""
    }
    
}

