//
//  Subject.swift
//  Edgem
//
//  Created by Hipster on 28/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class SubjectPagination: Unboxable {
    var subjects: [Subject]
    var pagination: Pagination?
    
    required init(unboxer: Unboxer) throws {
        self.subjects = try unboxer.unbox(key: "data")
        self.pagination = unboxer.unbox(key: "pagination")
    }
    
}

class Subject: Unboxable{
   
    var id: String!
    var name: String!
    var otherSubjectName: String?
    var isOption: String!
    var isSelected = false
    var subjectRate: String?
    var isOtherSelected = false
    var enteredName : String!
    var errorOne: String!
    var errorTwo: String!
    var lockRate: String!
    var levels: [Level]
    
    required init(unboxer: Unboxer) throws {
        self.id = try? unboxer.unbox(key: "subject_id")  as String
        self.name = unboxer.unbox(key: "title")
        self.otherSubjectName = unboxer.unbox(key: "other_subject")
        let rate = unboxer.unbox(key: "subject_rate") ?? ""
        self.subjectRate = rate != "" ? "$" + rate : rate 
        self.lockRate =  unboxer.unbox(key: "lock_rate")
        self.isOption = unboxer.unbox(key: "is_option")
        self.enteredName = unboxer.unbox(key: "other_subject") ?? ""
        self.levels = unboxer.unbox(key: "level") ?? [Level]()
        self.errorOne = ""
        self.errorTwo = ""
      
    }
    
    init(_ subject: Subject) {
        self.id = subject.id
        self.name = subject.name
        self.subjectRate = subject.subjectRate
        self.isSelected = false
        self.isOtherSelected = false
        self.enteredName = ""
        self.errorOne = ""
        self.errorTwo = ""
        self.lockRate = subject.lockRate
        self.isOption = subject.isOption
        self.otherSubjectName = subject.otherSubjectName
        self.levels = subject.levels
    }
}

class OtherSubject{
    var otherSubject: String!
    
    init(_ subject: String){
         self.otherSubject = subject
    }
}
