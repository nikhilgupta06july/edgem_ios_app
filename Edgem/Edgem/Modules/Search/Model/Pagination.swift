//
//  Pagination.swift
//  Edgem
//
//  Created by Hipster on 04/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import Unbox

class Pagination: Unboxable{
    
    // MARK:-  Properties
    
    let  count : Int
    var  currentPage: Int
    let  lastPage: Int
    let  total: Int
    
    // MARK:- Initializer
    
    init(){
        self.count = 0
        self.currentPage = 0
        self.lastPage = 0
        self.total = 0
    }
    
    required init(unboxer: Unboxer) throws {
        self.count = unboxer.unbox(key: "count") ?? 0
        self.currentPage = unboxer.unbox(key: "currentPage") ?? 0
        self.lastPage = unboxer.unbox(key: "lastPage") ?? 0
        self.total = unboxer.unbox(key: "total") ?? 0
    }
}
