//
//  Qualification.swift
//  Edgem
//
//  Created by Hipster on 29/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class Qualification: Unboxable,Equatable{
   
    var id: String!
    var name: String!
    var totalExperience: String?
    var isSelected = false
    
    init(_ id: String, _ name: String){
        self.id = id
        self.name = name
    }
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "qualification_id")
        self.name = unboxer.unbox(key: "title")
        self.totalExperience = unboxer.unbox(key: "total_experience") ?? "0"
    }
    
    static func == (lhs: Qualification, rhs: Qualification) -> Bool {
        return lhs.isSelected == rhs.isSelected
    }
    
    func getAttributedQualification() -> NSMutableAttributedString{
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: bullet + " " + self.name!,
                                                   attributes: [.font: UIFont(name: SourceSansProRegular, size: 18.0)!]))
        
        attributedString.append(NSAttributedString(string: self.totalExperience != "0" ? "\n   " + self.totalExperience! + " Years of Experience\n\n" : "\n",
                                                   attributes: [.font: UIFont(name: SourceSansProRegular, size: 13.0)!, .foregroundColor: UIColor.gray]))
        
        
        return attributedString
    }
}
