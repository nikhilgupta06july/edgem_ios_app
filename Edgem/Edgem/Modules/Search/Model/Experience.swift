//
//  Experience.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 15/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class Experience: Unboxable{
    
    var id: String!
    var instituteName: String?
    var subjectName: String!
    var fromYear: String?
    var toYear: String?
    var isUpdated = false
    
    init(){
        
    }
//    init(_ experience: Experience){
//        self.id = experience.id
//        self.instituteName = experience.instituteName
//        self.subjectName = experience.subjectName
//        self.fromYear = experience.fromYear
//        self.toYear = experience.toYear
//    }
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "experience_id")
        self.instituteName = unboxer.unbox(key: "institute_name")
        self.subjectName = unboxer.unbox(key: "subject_name")
        self.fromYear = unboxer.unbox(key: "year_from") ?? "0"
        self.toYear = unboxer.unbox(key: "year_to") ?? "0"
    }
    
    func getAttributedExperience() -> NSMutableAttributedString{
        let attributedString = NSMutableAttributedString()
        
        if let instituteName = self.instituteName{
            
            attributedString.append(NSAttributedString(string: bullet + " " + instituteName ,
                                                       attributes: [.font: UIFont(name: SourceSansProRegular, size: 18.0)!]))

        }else{
            attributedString.append(NSAttributedString(string: bullet + " " + "" ,
                                                       attributes: [.font: UIFont(name: SourceSansProRegular, size: 18.0)!]))
        }
        
        if let subjectName = self.subjectName {
            attributedString.append(NSAttributedString(string: "\n   " + subjectName,
                                                       attributes: [.font: UIFont(name: SourceSansProRegular, size: 13.0)!, .foregroundColor: UIColor.gray]))
            attributedString.append(NSAttributedString(string: "\n   " + self.fromYear! + " - " + self.toYear! + "\n\n",
                                                       attributes: [.font: UIFont(name: SourceSansProRegular, size: 13.0)!, .foregroundColor: UIColor.gray]))
        }
        
       

        return attributedString
    }
    
}
