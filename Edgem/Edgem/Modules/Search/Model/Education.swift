//
//  Education.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 15/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class Education: Unboxable{
    
    var id: String = "\(0)"
    var instituteName: String = ""
    var certificateName: String = ""
    var fromYear: String = ""
    var toYear: String = ""
    var isUpdated = false
    
    init(){
        
    }
    
//    init(_ education: Education){
//        self.id = education.id
//        self.instituteName = education.instituteName
//        self.certificateName = education.certificateName
//        self.fromYear = education.fromYear
//        self.toYear = education.toYear
//    }
    required init(unboxer: Unboxer) throws {
        self.id = try unboxer.unbox(key: "education_id")
        self.instituteName = try unboxer.unbox(key: "institute_name")
        self.certificateName = try unboxer.unbox(key: "certificate_name")
        self.fromYear = try unboxer.unbox(key: "year_from")
        self.toYear = try unboxer.unbox(key: "year_to")
    }
    
    
    func getAttributedEducation() -> NSMutableAttributedString{
        let attributedString = NSMutableAttributedString()
        attributedString.append(NSAttributedString(string: bullet + " " + self.instituteName,
                                                   attributes: [.font: UIFont(name: SourceSansProRegular, size: 18.0)!]))
        attributedString.append(NSAttributedString(string: "\n   " + self.certificateName,
                                                   attributes: [.font: UIFont(name: SourceSansProRegular, size: 13.0)!, .foregroundColor: UIColor.gray]))
        attributedString.append(NSAttributedString(string: "\n   " + self.fromYear + " - " + self.toYear + "\n\n",
                                                   attributes: [.font: UIFont(name: SourceSansProRegular, size: 13.0)!, .foregroundColor: UIColor.gray]))
        return attributedString
    }
}
