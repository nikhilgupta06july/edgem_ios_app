//
//  RecommendedTutors.swift
//  Edgem
//
//  Created by Namespace on 31/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class RecommendedTutors: Unboxable {
    var tutor: [Tutor]
    var pagination: Pagination
    
    required init(unboxer: Unboxer) throws {
        self.tutor = try unboxer.unbox(key: "recommended_tutors")
        self.pagination = try unboxer.unbox(key: "pagination")
    }
    
}
