//
//  BookMarkedTutor.swift
//  Edgem
//
//  Created by Hipster on 21/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class BookMarkedTutor: Unboxable {
        var tutor: [Tutor]
        var pagination: Pagination
        
        required init(unboxer: Unboxer) throws {
            self.tutor = try unboxer.unbox(key: "bookmarked_user")
            self.pagination = try unboxer.unbox(key: "pagination")
        }
        
}
