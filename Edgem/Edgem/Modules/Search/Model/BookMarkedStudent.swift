//
//  BookMarkedStudent.swift
//  Edgem
//
//  Created by Hipster on 19/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox


class BookMarkedStudent: Unboxable {
    var student: [Student]
    var pagination: Pagination
    
    required init(unboxer: Unboxer) throws {
        self.student = try unboxer.unbox(key: "bookmarked_user")
        self.pagination = try unboxer.unbox(key: "pagination")
    }
    
}
