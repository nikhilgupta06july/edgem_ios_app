//
//  Documents.swift
//  Edgem
//
//  Created by Namespace on 18/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class Documents: Unboxable{
    
    var id: String!
    var name: String!
    
    init(_ id: String, _ name: String){
        
        self.id = id
        self.name = name

    }
    
    required init(unboxer: Unboxer) throws {
        self.id = unboxer.unbox(key: "document_id")
        self.name = unboxer.unbox(key: "file_name")
    }
    
    
}
