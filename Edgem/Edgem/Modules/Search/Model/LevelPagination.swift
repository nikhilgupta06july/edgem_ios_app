//
//  LevelPagination.swift
//  Edgem
//
//  Created by Hipster on 29/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class LevelPagination: Unboxable{
    
    var levels:[Level] = []
    private let  limit = 10
    var currentPageNumber = 1
    var hasMoreToLoad = false
    var paginationType: PaginationType = .old
    
    init(){
        
        levels = []
        currentPageNumber = 1
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        
        if let levelArray = unboxer.dictionary["data"] as? [[String:AnyObject]]{
            for levelDict in levelArray{
                let level: Level = try unbox(dictionary: levelDict)
                self.levels.append(level)
            }
        }
        hasMoreToLoad = self.levels.count == limit
    }
    
    func appendDataFromObject(_ newPaginationobject: LevelPagination){
        
        switch self.paginationType{
            case .new, .reload:
                                            self.levels = []
                                            self.levels = newPaginationobject.levels
                                            self.paginationType = .old
            case .old               :
                                            self.levels +=  newPaginationobject.levels
                                            self.paginationType = .old
            }
        self.hasMoreToLoad = newPaginationobject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
