//
//  Lavel.swift
//  Edgem
//
//  Created by Hipster on 29/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class Level: Unboxable,Codable{
    
    var id: String!
    var name: String!
    var isSelected = false
    var price: String?
    
    init(_ id: String, _ name: String){
        self.id = id
        self.name = name
        self.isSelected = false
    }
    
    required init(unboxer: Unboxer) throws {
        self.id       = unboxer.unbox(key: "level_id")
        self.name = unboxer.unbox(key: "title")
        self.price  = unboxer.unbox(key: "subject_rate") ?? ""
    }
    
    enum CodingKeys: String, CodingKey {
        
        case id                      = "level_id"
        case name                = "title"
        case isSelected         = "other_subject"
        case price                 = "subject_rate"
    }
    
    required init(from decoder: Decoder) throws {
        
        let values                   = try decoder.container(keyedBy: CodingKeys.self)
        id                               = String(try values.decode(Int.self, forKey: .id))
        name                          = try values.decodeIfPresent(String.self, forKey: .name)
        price                           = try values.decodeIfPresent(String.self, forKey: .price) ?? ""
        isSelected                  = try values.decodeIfPresent(Bool.self, forKey: .isSelected) ?? false

    }
    
}

