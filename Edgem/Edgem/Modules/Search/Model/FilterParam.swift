//
//  FilterParam.swift
//  Edgem
//
//  Created by Hipster on 30/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation

class FilterParams{
    var selectedSubjects = [Subject]()
    var selectedLevels = [Level]()
    var address = [LocationData]()
    var otherSubject: String!
    var language: String!
    var music: String!
    // address
    var addresslabel: String!
    var addressLocation: String!
    var addressLattitude: String!
    var addressLongitude: String!
    
    var selectedQualifications = [Qualification]()
    var gender: String!
    //var rating: Double!
    var rating: String!
    var minPrice: String!
    var maxPrice: String!
    var minDistance: String!
    var maxDistance: String!
    var selectedTimeSlots = [
                                                "Mon"    : ["0", "0", "0"],
                                                "Tue"    : ["0", "0", "0"],
                                                "Wed"    : ["0", "0", "0"],
                                                "Thu"    : ["0", "0", "0"],
                                                "Fri"    : ["0", "0", "0"],
                                                "Sat"    : ["0", "0", "0"],
                                                "Sun"    : ["0", "0", "0"],
                                              ]
    
    init(){
          gender          = "any"
          rating            = "any"
          minPrice        = "0"
          maxPrice       = "150"
          minDistance  = "\(MIN_DISTANCE)"
          maxDistance  = "\(MAX_DISTANCE)"
          otherSubject  = ""
          language        = ""
          music              = ""
    }
    
    func copyValuesFromObject(_ filterParams: FilterParams){
        
        self.selectedSubjects              = filterParams.selectedSubjects
        self.selectedLevels                 = filterParams.selectedLevels
        self.selectedQualifications     = filterParams.selectedQualifications
        self.otherSubject                    = filterParams.otherSubject
        self.language                          = filterParams.language
        self.music                                = filterParams.music
        self.gender                             = filterParams.gender
        self.rating                                = filterParams.rating
        self.selectedTimeSlots            = filterParams.selectedTimeSlots
        self.maxPrice                           = filterParams.minPrice
        self.maxPrice                          = filterParams.maxPrice
        self.minDistance                     = filterParams.minDistance
        self.maxDistance                    = filterParams.maxDistance
        self.address                            = filterParams.address
        self.addresslabel                    = filterParams.addresslabel
        self.addressLocation              = filterParams.addressLocation
        self.addressLattitude             = filterParams.addressLattitude
        self.addressLongitude            = filterParams.addressLongitude
        
    }
    
    func getFilterParamsDict()->[String:String]{
        var filterDict = [String: String]()
        
        filterDict = [
            
                      "gender"             :      ("\(gender ?? "any")"),
                      "avg_rating"         :      ("\(rating ?? "any")"),
                    //"featured"           :      ("\(1)"),
//                      "min_price"          :      ("\(minPrice ?? "")"),
//                      "max_price"          :      ("\(maxPrice ?? "")")
//                      "longitude"          :      ("\(addressLongitude ?? "")"),
//                      "lattitude"          :      ("\(addressLattitude ?? "")"),
//                      "address_label"      :      ("\(addresslabel ?? "Home")"),
//                      "distance_min"       :      ("\(minDistance ?? "")"),
//                      "distance_max"       :      ("\(maxDistance ?? "")")
            
            
        ]
        
        if AppDelegateConstant.user?.userType == tutorType{
             filterDict["distance_min"]  =  ("\(minDistance ?? "")")
             filterDict["distance_max"] =  ("\(maxDistance ?? "")")
            filterDict["longitude"]         =  ("\(addressLongitude ?? "")")
            filterDict["lattitude"]           =  ("\(addressLattitude ?? "")")
            filterDict["address_label"]   =  ("\(addresslabel ?? "Home")")
        }else{
             filterDict["min_price"] =  ("\(minPrice ?? "")")
             filterDict["max_price"] =  ("\(maxPrice ?? "")")
        }
        var subIDs = ""
        
        if selectedSubjects.count > 0{
      
            for i in 0..<selectedSubjects.count{
                
                if i != selectedSubjects.count-1{
                    if !self.otherSubject.isEmpty && selectedSubjects[i].enteredName == self.otherSubject{
                        subIDs += String(selectedSubjects[i].enteredName)
                        subIDs += ","
                    }
                    if !self.music.isEmpty && selectedSubjects[i].enteredName == self.music{
                        subIDs += String(selectedSubjects[i].enteredName)
                        subIDs += ","
                    }
                    if !self.language.isEmpty && selectedSubjects[i].enteredName == self.language{
                        subIDs += String(selectedSubjects[i].enteredName)
                        subIDs += ","
                    }
                    subIDs += String(selectedSubjects[i].id)
                    subIDs += ","
                }else{
                    if !self.otherSubject.isEmpty && selectedSubjects[i].enteredName == self.otherSubject{
                        subIDs += String(selectedSubjects[i].enteredName)
                        subIDs += ","
                    }
                    if !self.music.isEmpty && selectedSubjects[i].enteredName == self.music{
                        subIDs += String(selectedSubjects[i].enteredName)
                        subIDs += ","
                        
                    }
                    if !self.language.isEmpty && selectedSubjects[i].enteredName == self.language{
                        subIDs += String(selectedSubjects[i].enteredName)
                        subIDs += ","
                
                    }
                    subIDs += String(selectedSubjects[i].id)
                }
            }
        }
        if !subIDs.isEmpty {
            filterDict["subject"] = subIDs
        }else{
             filterDict["subject"] = ""
        }
        
        var qualificationIDs = ""
        for i in 0..<selectedQualifications.count{
            if i != selectedQualifications.count-1{
                qualificationIDs += String(selectedQualifications[i].id)
                qualificationIDs += ","
            }else{
                qualificationIDs += String(selectedQualifications[i].id)
            }
        }
        if !qualificationIDs.isEmpty {
            filterDict["qualification"] = qualificationIDs
        }else{
             filterDict["qualification"] = ""
        }
        
        var levelIDs = ""
        for i in 0..<selectedLevels.count{
            if i != selectedLevels.count-1{
                levelIDs += String(selectedLevels[i].id)
                levelIDs += ","
            }else{
                levelIDs += String(selectedLevels[i].id)
            }
        }
        if !levelIDs.isEmpty {
            filterDict["level"] = levelIDs
        }else{
            filterDict["level"] = ""
        }
        
        if let monTimeSlot = selectedTimeSlots["Mon"], monTimeSlot.contains("1") {
            filterDict["Mon"] = monTimeSlot.joined(separator: ",")
        }
        
        if let tueTimeSlot = selectedTimeSlots["Tue"], tueTimeSlot.contains("1") {
            filterDict["Tue"] = tueTimeSlot.joined(separator: ",")
        }
        
        if let wedTimeSlot = selectedTimeSlots["Wed"], wedTimeSlot.contains("1") {
            filterDict["Wed"] = wedTimeSlot.joined(separator: ",")
        }
        
        if let thuTimeSlot = selectedTimeSlots["Thu"], thuTimeSlot.contains("1") {
            filterDict["Thu"] = thuTimeSlot.joined(separator: ",")
        }
        
        if let friTimeSlot = selectedTimeSlots["Fri"], friTimeSlot.contains("1") {
            filterDict["Fri"] = friTimeSlot.joined(separator: ",")
        }
        
        if let satTimeSlot = selectedTimeSlots["Sat"], satTimeSlot.contains("1") {
            filterDict["Sat"] = satTimeSlot.joined(separator: ",")
        }
        
        if let sunTimeSlot = selectedTimeSlots["Sun"], sunTimeSlot.contains("1") {
            filterDict["Sun"] = sunTimeSlot.joined(separator: ",")
        }
        return filterDict
    }
    
    func clearFilter(){
        self.selectedSubjects.removeAll()
        self.otherSubject = ""
        self.language = ""
        self.music = ""
        self.selectedLevels.removeAll()
        self.selectedQualifications.removeAll()
        self.rating = "any"
        self.minPrice = "0"
        self.maxPrice = "150"
        self.minDistance = "\(MIN_DISTANCE)"
        self.maxDistance = "\(MAX_DISTANCE)"
        self.addressLattitude = ""
        self.addressLongitude = ""
        selectedTimeSlots = [
                                            "Mon"   : ["0", "0", "0"],
                                            "Tue"   : ["0", "0", "0"],
                                            "Wed"   : ["0", "0", "0"],
                                            "Thu"   : ["0", "0", "0"],
                                            "Fri"   : ["0", "0", "0"],
                                            "Sat"   : ["0", "0", "0"],
                                            "Sun"   : ["0", "0", "0"],
                            ]
        self.gender = "any"
    }
    
}
