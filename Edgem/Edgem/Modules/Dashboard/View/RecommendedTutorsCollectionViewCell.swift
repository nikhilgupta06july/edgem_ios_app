//
//  RecommendedTutorsCollectionViewCell.swift
//  Edgem
//
//  Created by Hipster on 05/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import SwiftyStarRatingView

class RecommendedTutorsCollectionViewCell: UICollectionViewCell {
    
    //MARK: -------------- Properties -------------
    
    var firstName = ""
    var lastName = ""
    
    //MARK: -------------- @IBOutlets -------------
    
    @IBOutlet weak var holdersView: UIView!
    @IBOutlet weak var tutorImageView: UIImageView!
    @IBOutlet weak var ratersLabels: UILabel!
    @IBOutlet weak var tutorFeesLabel: UILabel!
    @IBOutlet weak var tutorRatingView: SwiftyStarRatingView!
    @IBOutlet weak var tutorSubjectLabel: UILabel!
    @IBOutlet weak var tutorNameLabel: UILabel!
    @IBOutlet weak var preferedLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.holdersView.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.holdersView.layer.shadowColor = UIColor.black.cgColor
        self.holdersView.layer.shadowRadius = 2
        self.holdersView.layer.cornerRadius = 5
        self.holdersView.layer.borderColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1).cgColor
        self.holdersView.layer.borderWidth = 1
        self.holdersView.layer.shadowOpacity = 0.1
        self.holdersView.layer.masksToBounds = false
        self.holdersView.layer.rasterizationScale = UIScreen.main.scale
        self.holdersView.clipsToBounds = false
        
        self.tutorImageView.layer.cornerRadius = (tutorImageView.bounds.width)/2
        self.tutorImageView.clipsToBounds = true
        
        self.preferedLabel.layer.cornerRadius = 4
        self.preferedLabel.clipsToBounds = true
    }
    
    
    
    //MARK: --------------- Private Functions -------
    
    func getIndexAndHighestRateOfSubject(subjects: [RecommendedTutorSubjects])->Int?{
        var data:[(Int,Double)] = [(Int,Double)]()
        
        for (index, subject) in subjects.enumerated(){
            if let levels = subject.levelData{
                  data.append((index, getHighestRate(levels: levels)))
            }
        }

        if data.count > 0{
            let sorted_data = data.sorted{(v1,v2) -> Bool  in
                v1.1 > v2.1
            }
            return (sorted_data[0].0)
        }
        return nil
    }
    
    func getHighestRate(levels: [Level]) -> Double{
        
        var upperRate = 0.0
       
        let rates_in_double = levels.map{ _level -> Double  in
            
            if let _price = _level.price{
                return (_price as NSString).doubleValue
            }
            return 0.0
            }.filter{ $0 != 0.0}
        
        
        let sorted_rates = rates_in_double.sorted { (v1, v2) -> Bool in
            v1 > v2
        }
        
        if sorted_rates.count > 1{
            upperRate = sorted_rates[0]
            return upperRate
        }else if sorted_rates.count == 1{
            upperRate = sorted_rates[0]
            return upperRate
        }
        return 0.0
    }
    
    class func cellIdentifier() -> String {
        return "RecommendedTutorsCollectionViewCell"
    }
    
    func configureCell(With recommendedTutors: Recommended_tutors){
        tutorImageView.image = UIImage(named: "defaultIcon")
        
        if let n = recommendedTutors.rating{
            tutorRatingView.value = CGFloat(n)
        }
        
        tutorRatingView.filledStarImage = UIImage(named: "filledStarImage")
        tutorRatingView.halfStarImage = UIImage(named: "halfStarImage")
        tutorRatingView.emptyStarImage = UIImage(named: "emptyStarImage")
        
        // rating
        ratersLabels.font =  UIFont(name: "SourceSansPro-Regular", size: 15)!
        ratersLabels.textColor = theamBlackColor
        if let rating  = recommendedTutors.rating{
            ratersLabels.text = "(\(String(describing: rating)))"
        }
        
        
        // average rating
        if let rating = recommendedTutors.averageRating{
            tutorRatingView.value  = rating
        }
        
        // rate per hour
        tutorFeesLabel.font =  UIFont(name: "SourceSansPro-Regular", size: 15)!
        tutorFeesLabel.textColor = theamBlackColor
        
        if let subject = recommendedTutors.subjects, let index = getIndexAndHighestRateOfSubject(subjects: subject), let subject1 = subject[safe: index]{
            tutorFeesLabel.text = GetSubjectsRange.get(from: subject1.levelData!)
        }
        
        // subject
        tutorSubjectLabel.font =  UIFont(name: "SourceSansPro-Bold", size: 16)!
        tutorSubjectLabel.textColor = theamBlackColor
        if let subjects = recommendedTutors.subjects{
            if subjects.count > 0{
                if let index = getIndexAndHighestRateOfSubject(subjects: recommendedTutors.subjects!){
                    tutorSubjectLabel.text = subjects[index].title
                }
                
            }
        }
        
        // ProfileImage
        loadImages(recommendedTutors)

        
        tutorNameLabel.font =  UIFont(name: "SourceSansPro-Regular", size: 15)!
        tutorNameLabel.textColor = theamBlackColor
        if let _firstName = recommendedTutors.first_name{
            self.firstName = _firstName
        }
        if let _lastName = recommendedTutors.last_name{
            self.lastName = _lastName
        }
        tutorNameLabel.text = firstName+" "+lastName
        
        preferedLabel.font = UIFont(name: "SourceSansPro-Regular", size: 15)!
        preferedLabel.textColor = theamBlackColor
        preferedLabel.text = "Preferred".localized
    }
    
    fileprivate func loadImages(_ tutor: Recommended_tutors) {
        //var docImage: UIImage?
        let imageProvider = ImageProvider()
        if let image = imageCache.image(forKey: "\(String(describing: tutor.tutor_id))"){
           // docImage = image
             self.tutorImageView.image = image
        }else{
             tutorImageView.image = UIImage(named: "femaleIcon")
            guard let imageURL = tutor.real_image else{return}
            if let url = URL(string: imageURL) {
                imageProvider.requestImage(from: url) { (image) in
                    imageCache.add(image, forKey: "\(String(describing: tutor.tutor_id))")
                    self.tutorImageView.image = image
                    // docImage = image
                }
            }
        }
        //return docImage
    }
}
