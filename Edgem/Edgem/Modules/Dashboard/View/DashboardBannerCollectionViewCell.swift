//
//  DashboardBannerCollectionViewCell.swift
//  Edgem
//
//  Created by Hipster on 04/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit


class DashboardBannerCollectionViewCell: UICollectionViewCell {
    
    //MARK: ----------- Properties -------------------
    
    var imageURLString: String?
    var img: UIImage? = nil
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    //MARK: ----------- @IBOutlets ------------------
    
    @IBOutlet weak var bannerImageView: UIImageView!
    
    class func cellIdentifier() -> String {
        return "DashboardBannerCollectionViewCell"
    }
    
    func configureCellWithBanner(_ banner: Advertisements) {
        loadimagesUsingURLString(_urlString: banner.banner_file!)
    }
    
    func startActivityIndicator(){
        activityIndicator.center = self.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .gray
        self.addSubview(activityIndicator)
    }
    
    func stopActivityIndicator(){
        activityIndicator.stopAnimating()
    }
    
    func loadimagesUsingURLString(_urlString: String){
        imageURLString = _urlString
        guard let urlString = imageURLString else{return }
        guard let url = URL(string: urlString) else{return}
        startActivityIndicator()
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil{
                print(error!)
                return
            }
            guard let image = UIImage(data: data!) else{return}
            DispatchQueue.main.async {
                self.stopActivityIndicator()
                 self.bannerImageView.image = image
            }
            }.resume()
    }
    
}
