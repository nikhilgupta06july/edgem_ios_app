//
//  GroupTutionTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 05/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class GroupTutionTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var commingSoonLabel: UILabel!
    
    func configureCell(){
        TitleLabel.font = UIFont(name: "Quicksand-Bold", size: 18)
        TitleLabel.textColor = theamBlackColor
        TitleLabel.text = "Group Tuition".localized
        
        commingSoonLabel.font = UIFont(name: "SourceSansPro-Regular", size: 15)
        commingSoonLabel.textColor = theamGrayColor
        commingSoonLabel.text = "Coming soon…".localized
    }
    
    class func cellIdentifire()->String{
        return "GroupTutionTableViewCell"
    }
    
    class func cellHeight()-> CGFloat{
        return 60
    }
    
}
