//
//  DashboardBannerTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 04/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class DashboardBannerTableViewCell: UITableViewCell {
    
    //MARK: ----------------- Properties ------------------
    
    var banners: [Advertisements]!
    var dummyCount: Int{
        get{
            return banners.count>1 ? 3 : 1
        }
    }
    var timer: Timer!
    
    var cellWidth: CGFloat {
        return ScreenWidth
    }
    
    var totalContentWidth: CGFloat {
        return CGFloat(banners.count * dummyCount) * ScreenWidth
    }

    //MARK: ----------------- @IBOutlets ------------------
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: ----------------- Private Functions ------------
    
    class func cellIdentifier() -> String {
        return "DashboardBannerTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return UITableView.automaticDimension//88.0
    }
    
    func reloadCollectionViewData() {
        self.collectionView.reloadData()
    }
    
    func configureCell() {
        if timer == nil {
            startTimer()
        }
    }
    
    func startTimer() {
        if banners != nil{
            if banners.count > 1 && timer == nil {
                timer = Timer.scheduledTimer(timeInterval: TimeInterval(5), target: self, selector: #selector(rotate), userInfo: nil, repeats: true)
            }
        }
    }
    
    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc func rotate() {
        let offset = CGPoint(x: collectionView.contentOffset.x + cellWidth, y: collectionView.contentOffset.y)
        collectionView.setContentOffset(offset, animated: true)
    }

}

extension DashboardBannerTableViewCell: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
          self.centerIfNeeded()
    }
    
    
    
    func centerIfNeeded() {
        let currentOffset = collectionView.contentOffset
        let contentWidth = self.totalContentWidth
        let width = contentWidth / CGFloat(dummyCount)
        
        if 0 > currentOffset.x {
            //left scrolling
            collectionView.contentOffset = CGPoint(x: width - currentOffset.x, y: currentOffset.y)
        } else if (currentOffset.x + cellWidth) > contentWidth {
            //right scrolling
            let difference = (currentOffset.x + cellWidth) - contentWidth
            collectionView.contentOffset = CGPoint(x: width - (cellWidth - difference), y: currentOffset.y)
        }
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.stopTimer()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.startTimer()
    }
}

extension DashboardBannerTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return banners != nil ? dummyCount * banners.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DashboardBannerCollectionViewCell.cellIdentifier(), for: indexPath) as! DashboardBannerCollectionViewCell
        if banners != nil{
            let itemIndex = indexPath.item % self.banners.count
            cell.configureCellWithBanner((banners?[itemIndex])!)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = ScreenWidth*12/55
        return CGSize(width: ScreenWidth, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
}
