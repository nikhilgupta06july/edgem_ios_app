//
//  DashboardSectionViewTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 04/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class DashboardSectionViewTableViewCell: UITableViewCell {
    
    
    //MARK: ------------------ Properties -----------------
    
    //MARK: ------------------ @IBOutlets ----------------
    
    @IBOutlet weak var sectionTitle: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK: ------------------ Private Functions -----------
    
    class func cellIdentifire()->String{
        return "DashboardSectionViewTableViewCell"
    }
    
    class func cellHeight()-> CGFloat{
        return 60
    }
    
    fileprivate func configureCell(with data: DashboardSectionViewInfo){
        sectionTitle.font =  UIFont(name: "Quicksand-Bold", size: 18)
        sectionTitle.textColor = theamBlackColor
        sectionTitle.text = data.sectionTitle
        
        currencyLabel.font = UIFont(name: "SourceSansPro-Regular", size: 14)
        currencyLabel.textColor = theamGrayColor
        currencyLabel.text = data.currency
        
        costLabel.font = UIFont(name: "Quicksand-Bold", size: 18)
        costLabel.textColor = theamBlackColor
        costLabel.text = data.cost
    }
}

//MARK: -------------------- @IBActions -------------------

extension DashboardSectionViewTableViewCell{
    @IBAction func costLabelTapped(_ sender: UIButton) {
    }
}
