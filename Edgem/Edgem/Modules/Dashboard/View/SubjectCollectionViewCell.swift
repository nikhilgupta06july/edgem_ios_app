//
//  SubjectCollectionViewCell.swift
//  Edgem
//
//  Created by Hipster on 07/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class SubjectCollectionViewCell: UICollectionViewCell {
    
      var imageURLString: String?
    
    @IBOutlet weak var subjectImage: UIImageView!
    @IBOutlet weak var subjectName: UILabel!
    @IBOutlet weak var bottomBorderView: UIView!
    
    class func cellIdentifier() -> String {
        return "SubjectCollectionViewCell"
    }
    
    func configureCellWithSubject(_ image: String, _ label: String, _ index: Int) {
        self.subjectName.font = UIFont(name: "Quicksand-Medium", size: 13)
        self.subjectName.textColor = theamBlackColor
        self.subjectName.text = image
        
        self.subjectImage.image = UIImage(named: label)
        
        if index == 12 || index == 13{
            bottomBorderView.isHidden = true
        }else{
            bottomBorderView.isHidden = false
        }
        
    }
    
    func configureCell(with subjects: Subjects, _ index: Int){
        self.subjectName.font = UIFont(name: "Quicksand-Medium", size: 13)
        self.subjectName.textColor = theamBlackColor
        
        self.subjectName.text = subjects.title
        
        // subject image
        if let imageURL = subjects.subject_icon{
            loadimagesUsingURLString(_urlString: imageURL)
        }
        if index == 12 || index == 13{
            bottomBorderView.isHidden = true
        }else{
            bottomBorderView.isHidden = false
        }
    
    }
    
    // load images from server
    func loadimagesUsingURLString(_urlString: String){
        imageURLString = _urlString
        guard let urlString = imageURLString else{return }
        guard let url = URL(string: urlString) else{return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil{
                print(error!)
                return
            }
            guard let image = UIImage(data: data!) else{return}
            DispatchQueue.main.async {
                 self.subjectImage.image = image
            }
            }.resume()
    }

}
