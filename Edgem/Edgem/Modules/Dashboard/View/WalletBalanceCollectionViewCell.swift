//
//  WalletBalanceCollectionViewCell.swift
//  Edgem
//
//  Created by Hipster on 04/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class WalletBalanceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    class func cellIdentifier() -> String {
        return "WalletBalanceCollectionViewCell"
    }
    
    
    
    func configureCellWithPartner(_ image: String, _ label: String, _ index: Int) {
        
        self.label.font = UIFont(name: "SourceSansPro-SemiBold", size: 13)
        self.label.textColor = index != 3 ? theamGrayColor : theamBlackColor
        
        let origImage = UIImage(named: label)
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        self.image.tintColor = buttonBorderColor
        
        self.image.image = index != 3 ? tintedImage : origImage
        self.label.text = image
        
    }
    
}
