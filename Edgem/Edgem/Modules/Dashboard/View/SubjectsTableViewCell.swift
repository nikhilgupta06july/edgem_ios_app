//
//  SubjectsTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 07/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol SubjectsTableViewCellDelegate{
    func selectedSubjectID(_ ID: Int)
}

class SubjectsTableViewCell: UITableViewCell {
    
    //MARK: ------------ Properties ---------------
     let label:[(String,String)] = [("English","english"),("Chinese","chemistry"),("Mathematics","maths"),("Chemistry","chemistry"),("Physics","physics"),("Biology","biology"),("Economics","economics"),("Literature","literature"),("History","history"),("Geography","geography"),("Accounting","accounting"),("Music","music"),("Art","art"),("Others","others")]
    
     var subjects:[Subjects]!
     var subjectsTableViewCellDelegate: SubjectsTableViewCellDelegate?
    
    //MARK: ------------ @IBOutlets --------------
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func configureCell(){
        titleLabel.font =  UIFont(name: "Quicksand-Bold", size: 18)
        titleLabel.textColor = theamBlackColor
        titleLabel.text = "Subjects".localized
    }
    
    class func cellIdentifire()->String{
        return "SubjectsTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 121.0
    }

    func reloadCollectionViewData() {
        self.collectionView.reloadData()
    }
}

//MARK: UICollectionView Delegate/Datasource

extension SubjectsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subjects != nil ? subjects.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SubjectCollectionViewCell.cellIdentifier(), for: indexPath) as! SubjectCollectionViewCell
       // cell.configureCellWithSubject(label[indexPath.row].0, label[indexPath.row].1, indexPath.row)
        cell.configureCell(with: self.subjects[indexPath.row], indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.subjects.count > 0{
            if let subjectID = (self.subjects[indexPath.row].subject_id){
                    subjectsTableViewCellDelegate?.selectedSubjectID(subjectID)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = (collectionView.frame.size.width)/CGFloat(3)
        return CGSize(width: yourWidth, height: 64.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return  0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return  0
    }
    
    
}
