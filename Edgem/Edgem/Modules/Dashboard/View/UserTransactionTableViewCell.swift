//
//  UserTransactionTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 20/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class UserTransactionTableViewCell: UITableViewCell {
    
    // MART: - @IBOutlets
    
    @IBOutlet weak var bankImage: UIImageView!
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var ammountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(with transaction: Transactions){
        titleLabel.text = transaction.description
        accountNumberLabel.text = "************"+transaction.cardEnd
        dateLabel.text = Utilities.UTCToLocal(date: transaction.addedOn, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd MMMM yyyy")
        ammountLabel.text = "\(transaction.amount)"
    }

    class func cellIdentifire() -> String{
        return "UserTransactionTableViewCell"
    }

}
