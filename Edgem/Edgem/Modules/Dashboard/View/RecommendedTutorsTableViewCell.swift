//
//  RecommendedTutorsTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 05/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol RecommendedTutorsTableViewDelegate{
    func viewMoreBtnTapped()
}

class RecommendedTutorsTableViewCell: UITableViewCell {
    
   
    //MARK: ------------- Properties --------------
    
    var didTapRecommendedTutor:((Recommended_tutors)->Void)?
    
    var recommendedTutorsTableViewDelegate: RecommendedTutorsTableViewDelegate!
    var recommended_tutors : [Recommended_tutors]!{
        didSet{
            collectionView.reloadData()
        }
    }
    
    //MARK: ------------- @IBOutlets --------------
     @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var recommendedTutorLabel: UILabel!
    @IBOutlet weak var viewMore: UILabel!
    @IBOutlet weak var viewMoreHolder: UIView!
    @IBOutlet weak var recommendedTutorsTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    //var announcements: [Announcement]!
    
    func configureCell(){
        
        viewMoreHolder.backgroundColor = UIColor.clear
        
        viewMore.font = UIFont(name: "SourceSansPro-Regular", size: 15)
        viewMore.textColor = theamGrayColor
        viewMore.text = "View more".localized
        
        recommendedTutorsTitle.font = UIFont(name: "Quicksand-Bold", size: 18)
        recommendedTutorsTitle.textColor = theamBlackColor
        recommendedTutorsTitle.text = "Recommended Tutors".localized
        
    }
    
    class func cellIdentifier() -> String {
        return "RecommendedTutorsTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 195
    }
    
    func reloadCollectionViewData() {
        self.collectionView.reloadData()
    }
    
    
    

}

    //MARK: ------------- @IBAction ----------
extension RecommendedTutorsTableViewCell{
    
    @IBAction func viewMoreBtnTapped(_ sender: UIButton) {
        if recommendedTutorsTableViewDelegate != nil{
              recommendedTutorsTableViewDelegate.viewMoreBtnTapped()
             viewMoreHolder.backgroundColor = cellBackgroundColor
        }else{
            print("recommendedTutorsTableViewDelegate is nil")
        }
      
    }
}

    //MARK: - UICollectionView Delegates/DataSources

extension RecommendedTutorsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recommended_tutors != nil ? recommended_tutors.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RecommendedTutorsCollectionViewCell.cellIdentifier(), for: indexPath) as? RecommendedTutorsCollectionViewCell else{return UICollectionViewCell()}
        //cell.configureCellWithRecommendedTutors()
        if recommended_tutors != nil{
            cell.configureCell(With: recommended_tutors[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 213, height: 108)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return  19
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return  19
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if recommended_tutors.count > 0{
            didTapRecommendedTutor?(recommended_tutors[indexPath.row])
        }
        
    }
    
}
