//
//  WalletBalanceTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 04/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class WalletBalanceTableViewCell: UITableViewCell {
    
    //MARK: -------------------- Properties -------------------
    
    let label:[(String,String)] = [("Pay","pay"),("Request","request"),("Top Up","topUp"),("Rewards","rewards_filled")]
    let filledLabel = ["pay_filled", "request_filled", "topUp_filled", "rewards_filled"]
    
    var didTapWalletOptions: ((Int)->Void)?
    //MARK: -------------------- @IBOutlets ------------------
    
    @IBOutlet weak var walletBallanceBtn: UIButton!
    @IBOutlet weak var walletBalance: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    
        func configureCell(){
            
            
        walletBalance.font = UIFont(name: "Quicksand-Bold", size: 18)
        walletBalance.textColor = theamBlackColor
        walletBalance.text = "Wallet Balance".localized
        
        currencyLabel.font =  UIFont(name: "SourceSansPro-Regular", size: 14)!
        currencyLabel.textColor = theamGrayColor
        currencyLabel.text = "SGD".localized
        
        costLabel.font = UIFont(name: "Quicksand-Bold", size: 18)
        costLabel.textColor = theamBlackColor
//        costLabel.text = "256.0".localized
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func cellIdentifier() -> String {
        return "WalletBalanceTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        //return 350.0
        return 163
    }
    
    func reloadCollectionViewData() {
        self.collectionView.reloadData()
    }
    

}

// MARK: ---------------------- CollecionView Delegates/DataSource -----

extension WalletBalanceTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return label.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WalletBalanceCollectionViewCell.cellIdentifier(), for: indexPath) as! WalletBalanceCollectionViewCell
        cell.configureCellWithPartner(label[indexPath.row].0, label[indexPath.row].1, indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = (collectionView.frame.size.width)/CGFloat(label.count)
        return CGSize(width: yourWidth, height: 64.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return  0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return  0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if let cell = collectionView.cellForItem(at: indexPath) as? WalletBalanceCollectionViewCell{
//               let imgeName = filledLabel[indexPath.item]
//               cell.image.image = UIImage(named: imgeName)
//        }
     self.didTapWalletOptions?(indexPath.item)
    }
    
    
}
