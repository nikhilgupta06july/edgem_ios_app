//
//  AllTransactionViewController.swift
//  Edgem
//
//  Created by Namespace on 20/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class AllTransactionViewController : BaseViewController {
    
    // MARK: - Properties
    var rowCount = 0
    var userTransactions = UserTransactions()
    var disposableBag = DisposeBag()
    
    // MARK: - @IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var Notransactions: UIStackView!
    
    // MARK: - ViewLifeCycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Notransactions.isHidden = true
        initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let userTransactions =  AppDelegateConstant.user?.userTransactions, let transactions = userTransactions.transactions {
            if transactions.count > 0{
                self.rowCount = (userTransactions.transactions?.count)!
                self.userTransactions = userTransactions
                self.tableView.reloadData()
            }else{
                 self.Notransactions.isHidden = false
            }
        }else{
            self.getTransactions()
        }
    }
    
    // MARK: - @IBActions
    
    
    // MARK: - Helping Functions
    
    private func initialSetUp(){
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
    }
    
}

// MARK: - TableView Delegates/Datasources

extension AllTransactionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UserTransactionTableViewCell.cellIdentifire(), for: indexPath) as? UserTransactionTableViewCell else{return UITableViewCell()}
        cell.configureCell(with: (self.userTransactions.transactions?[indexPath.row])!)
        return cell
    }
    
}

// MARK: - API Implementations

extension AllTransactionViewController {
    
    // [START some api]
    
    fileprivate func getTransactions(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.getUserTransactions("\(1)")
        let userDisposable = userObserver.subscribe(onNext: {(usertransactions) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                self.userTransactions = usertransactions
                
                if let transactions = usertransactions.transactions{
                    if transactions.count > 0{
                        self.rowCount = (usertransactions.transactions?.count)!
                        self.tableView.reloadData()
                    }else{
                        self.Notransactions.isHidden = false
                    }
                }else{
                    self.Notransactions.isHidden = false
                }
//                if (usertransactions.transactions?.count)! > 0{
//                    self.rowCount = (usertransactions.transactions?.count)!
//                     self.tableView.reloadData()
//                }else{
//                    self.Notransactions.isHidden = false
//                }
               
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    
    // [END send message api]
    
    
}
