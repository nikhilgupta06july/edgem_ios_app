//
//  WalletBalanceVC.swift
//  Edgem
//
//  Created by Hipster on 01/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class WalletBalanceVC: BaseViewController {
    
    var s_titles = [ "Add Credit Card".localized, "All Transactions".localized]
    var t_titles = ["Add Bank Details".localized, "Add Credit Card".localized, "All Transactions".localized]
    var images = ["bank", "card", "rate"]
    var s_images = ["card", "rate"]
    var disposableBag = DisposeBag()
    
    @IBOutlet weak var ammountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if AppVariables.banks == nil || (AppVariables.banks != nil && AppVariables.banks?.bankNames.count == 0){
            getBankName()
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let balance = AppDelegateConstant.user?.walletBalance{
            ammountLabel.text = "\(balance)".changeStringNumberToWholeNumber()//String(format: "%.2f", balance)
        }else{
            ammountLabel.text = "0"
        }
        
        if let _ = AppDelegateConstant.user?.userBankDetail{
            
        }else{
            self.viewBankDetails()
        }
        
        if let _ =  AppDelegateConstant.user?.userTransactions {
            
        }else{
            self.getTransactions()
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigateBack(sender: sender)
        
    }
}

extension WalletBalanceVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  (AppDelegateConstant.user?.userType == tutorType) ? 3 : 2//2//titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if UserStore.shared.selectedUserType == "Tutor"{
            let cell = tableView.dequeueReusableCell(withIdentifier: WalletBalanceVCCell.cellIdentifier(), for: indexPath) as! WalletBalanceVCCell
            var title = ""
            if indexPath.row == 0{
                
                if let bankDetail = AppDelegateConstant.user?.userBankDetail, let bank_name = bankDetail.bankName, let acc_no = bankDetail.accountNumber{
                    title = "\(bank_name)/....\(String(acc_no.suffix(4)))"
                }else{
                    title = t_titles[indexPath.row]
                }
                
            }else if indexPath.row == 1{
                if let userCards = AppDelegateConstant.user?.paymentCard, userCards.count > 0{
                    title = ".... .... .... "+userCards[0].cardNumber
                }else{
                    title = t_titles[indexPath.row]
                }
            }else{
                    title = t_titles[indexPath.row]
            }
            cell.configureCellWithTitle(title, andImage: images[indexPath.row])
            cell.disclosureIconImageView.image = UIImage(named:"right-arrow-dark")
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: WalletBalanceVCCell.cellIdentifier(), for: indexPath) as! WalletBalanceVCCell
            var title = ""
            if indexPath.row == 0{
                if let userCards = AppDelegateConstant.user?.paymentCard, userCards.count > 0{
                  title = ".... .... .... "+userCards[0].cardNumber
                }else{
                 title = s_titles[indexPath.row]
                }
            }else{
                 title = s_titles[indexPath.row]
            }
            cell.configureCellWithTitle(title, andImage: s_images[indexPath.row])
            cell.disclosureIconImageView.image = UIImage(named:"right-arrow-dark")
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if UserStore.shared.selectedUserType == "Tutor"{

            switch indexPath.row {
                
            case 0:
                
                let bankDetailViewController = BankDetailViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
                self.navigationController?.pushViewController(bankDetailViewController, animated: true)
                
            case 1:
                
                let creditCardsListViewController = CreditCardsListViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                self.navigationController?.pushViewController(creditCardsListViewController, animated: true)
                
                break
            
            case 2:
                
                let allTransactionViewController = AllTransactionViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
                self.navigationController?.pushViewController(allTransactionViewController, animated: true)
                
                break
                
            default:
                break
            }
            
        }else{
            
            switch indexPath.row {
                
            case 0:
                
                let creditCardsListViewController = CreditCardsListViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                self.navigationController?.pushViewController(creditCardsListViewController, animated: true)
                
            case 1:
                
                let allTransactionViewController = AllTransactionViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
                self.navigationController?.pushViewController(allTransactionViewController, animated: true)

            default:
                break
            }

        }
        
    }
    
}

extension WalletBalanceVC{
    
    // [START view bank details api]  -------> this api will be used at BankDetailViewController
    fileprivate func viewBankDetails(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
            // Utilities.showHUD(forView: self.view, excludeViews: [])
            
            let forgotObserver = ApiManager.shared.apiService.getBankDetails()
            let forgotDisposable = forgotObserver.subscribe(onNext: {(bankDetail) in
                // Utilities.hideHUD(forView: self.view)
                DispatchQueue.main.async {
                    AppDelegateConstant.user?.userBankDetail = bankDetail
                }
            }, onError: {(error) in
                DispatchQueue.main.async(execute: {
                    // Utilities.hideHUD(forView: self.view)
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            })
            forgotDisposable.disposed(by: self.disposableBag)
            
        }
        
    }
    // [END view bank details api]
    
    
    // [START All transaction api]   -------> this api will be consume in AllTransactionViewControlle
    fileprivate func getTransactions(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        //Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
            let userObserver = ApiManager.shared.apiService.getUserTransactions("\(1)")
            let userDisposable = userObserver.subscribe(onNext: {(usertransactions) in
                DispatchQueue.main.async {
                    //Utilities.hideHUD(forView: self.view)
                    if AppDelegateConstant.user?.userTransactions == nil{
                         AppDelegateConstant.user?.userTransactions = usertransactions
                    }
                   
                }
            }, onError: {(error) in
                DispatchQueue.main.async(execute: {
                    //Utilities.hideHUD(forView: self.view)
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            })
            userDisposable.disposed(by: self.disposableBag)
        }

    }
    // [END All transaction api]
    
    // [START get all banks]
    func getBankName(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let bankObserver = ApiManager.shared.apiService.getBank()
        
        let bankDisposable = bankObserver.subscribe(onNext: {(banks) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
               AppVariables.banks = banks
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        bankDisposable.disposed(by: self.disposableBag)
    }
     // [END get all banks]
}


