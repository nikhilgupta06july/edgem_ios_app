//
//  EarnCreaditViewController.swift
//  Edgem
//
//  Created by Namespace on 18/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import EasyTipView

class EarnCreaditViewController: BaseViewController,EasyTipViewDelegate {

    var borderLayer =  CAShapeLayer()
    weak var tipView: EasyTipView?
    
    var referralCode: String?
    
    // MARK: - @IBOutlets
    
    @IBOutlet weak var refferalCodeContainerView: UIView!
    @IBOutlet weak var inviteNowBtnContainerView: UIView!
    @IBOutlet weak var refferalCodeLabel: UILabel!
    @IBOutlet weak var yourCreditsContainerView: UIView!
    @IBOutlet weak var yourCreditsView: UIView!
    @IBOutlet weak var totalCredits: UILabel!
    @IBOutlet weak var totalInvitesView: UIView!
    @IBOutlet weak var totalInvitesContainerView: UIView!
    @IBOutlet weak var totalInvites: UILabel!
    @IBOutlet weak var toolTipBtn: UIButton!
    @IBOutlet weak var toolTipView: UIView!
    @IBOutlet weak var thirdImageLabelContent: UILabel!
    
    var preferences: EasyTipView.Preferences!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        preferences = EasyTipView.Preferences()
        preferences.drawing.arrowPosition = .bottom
        preferences.drawing.textAlignment = .center
        preferences.drawing.font = UIFont(name: QuicksandMedium, size: 15)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.positioning.textHInset = 40
        preferences.positioning.textVInset = 10
        preferences.drawing.backgroundColor = UIColor(red: 23/255, green: 23/255, blue: 23/255, alpha: 0.80)
        preferences.animating.dismissTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1.5
        preferences.animating.dismissDuration = 1.5

        EasyTipView.globalPreferences = preferences
        
        if let referralCode = AppDelegateConstant.user?.referralCode, let totalCredit = AppDelegateConstant.user?.totalCredits, let totalInvite = AppDelegateConstant.user?.totalInvites{
            refferalCodeLabel.text = referralCode
            self.referralCode = referralCode
            totalCredits.text = totalCredit
            totalInvites.text = totalInvite
        }
       // configureDashedBorder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let user = AppDelegateConstant.user, let points = user.creditsPerPoint{
            thirdImageLabelContent.text = "Both of you will get \(points) credits"
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        refferalCodeContainerView.layoutIfNeeded()
        configureDashedBorder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let tipView = self.tipView{
            tipView.dismiss()
            toolTipBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        inviteNowBtnContainerView.layer.cornerRadius = 5
        inviteNowBtnContainerView.layer.shadowColor = UIColor.black.cgColor
        inviteNowBtnContainerView.layer.shadowOpacity = 0.12
        inviteNowBtnContainerView.layer.shadowRadius = 6
        inviteNowBtnContainerView.layer.shadowOffset = CGSize(width: 0, height: 3)//.zero
        inviteNowBtnContainerView.layer.shadowPath = UIBezierPath(rect: inviteNowBtnContainerView.bounds).cgPath
        
        yourCreditsView.layer.cornerRadius = 5
        yourCreditsView.clipsToBounds = true
        
        totalInvitesView.layer.cornerRadius = 5
        totalInvitesView.clipsToBounds = true
        
        
        let width: CGFloat = yourCreditsContainerView.frame.width
        let height: CGFloat = 123

        let contactRect = CGRect(x: yourCreditsContainerView.frame.minX+4, y: height + 8, width: width-4 , height: 3)
        
        yourCreditsContainerView.layer.shadowPath = UIBezierPath(roundedRect: contactRect, cornerRadius: 5).cgPath
        yourCreditsContainerView.layer.shadowRadius = 4
        yourCreditsContainerView.layer.shadowOpacity = 1
        yourCreditsContainerView.backgroundColor = .clear
        yourCreditsContainerView.layer.shadowColor = UIColor.black.cgColor
        
        totalInvitesContainerView.layer.shadowPath = UIBezierPath(roundedRect: contactRect, cornerRadius: 5).cgPath
        totalInvitesContainerView.layer.shadowRadius = 4
        totalInvitesContainerView.layer.shadowOpacity = 1
        totalInvitesContainerView.backgroundColor = .clear
        totalInvitesContainerView.layer.shadowColor = UIColor.black.cgColor

    }
    
    // MARK: - @IBActions
    
    
    @IBAction func didTapInviteButtons(_ sender: UIButton) {
        
        if let bitlyURL = UserStore.shared.bitlyURL, let referralCode = self.referralCode, let user = AppDelegateConstant.user, let points = user.creditsPerPoint{
            
            let msg = "Edgem is a great way to find top tutors & students. Register an account with Edgem via this link \(bitlyURL) and enter the unique code '\(referralCode)' and both of us will receive \(points) free credits to be used for future lessons."
            
            let activityController = UIActivityViewController(activityItems: [msg], applicationActivities: nil)
            
            self.present(activityController, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func didTapInfoBtn(_ sender: UIButton) {
        
        if let user = AppDelegateConstant.user, let points = user.creditsPerPoint{
            
                 let text = "Every \(String(describing: points)) credits earned = $1 \n\n Credits can be used to offset future tuition or service fees.".localized
            
                if let tipView = self.tipView{
                    
                    tipView.dismiss()
                    
                    toolTipBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
                }else{
                
                let tip = EasyTipView(text: text, preferences: self.preferences, delegate: self)
                    
                toolTipBtn.setImage(#imageLiteral(resourceName: "infoImageSelected"), for: .normal)
                    
                tip.show(forView: self.toolTipView)
                    
                tipView = tip
                    
            }
            
        }


    }
    
    // Easy tipview delegate
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        toolTipBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
    }
        
    fileprivate func configureDashedBorder() {
        borderLayer  = dashedBorderLayerWithColor(color: theamGrayColor.cgColor)
        refferalCodeContainerView.layer.addSublayer(borderLayer)
    }
    
    func dashedBorderLayerWithColor(color:CGColor) -> CAShapeLayer {
        
        let  borderLayer = CAShapeLayer()
        borderLayer.name  = "borderLayer"
        let frameSize = refferalCodeContainerView.bounds.size
        let shapeRect =   CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
      
        borderLayer.bounds=shapeRect
        borderLayer.position =  CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = color
        borderLayer.lineWidth=1
        borderLayer.lineJoin=CAShapeLayerLineJoin.round
        borderLayer.lineDashPattern = NSArray(array: [NSNumber(value: 8),NSNumber(value:4)]) as? [NSNumber]
        
        let path = UIBezierPath.init(roundedRect: shapeRect, cornerRadius: 5)
        
        borderLayer.path = path.cgPath
        
        return borderLayer
        
    }
    
}
