//
//  DashboardViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 13/11/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import FirebaseAuth
import GoogleSignIn
import FBSDKLoginKit
import RxSwift
import BitlySDK

class DashboardViewController: BaseViewController {
    
    //MARK: ----------------Properties ------------
    var firebaseAuth: Auth!
    var disposableBag = DisposeBag()
    var userRegistration: UserRegistration!
    var user: User?
    var dashboardDetails: DashboardResponseData!
    var sectionViewTitles: [String] = ["","Wallet Balance".localized, "Recommended Tutors".localized]
    var isFromLogin: Bool = false
    
    // MARK: --------------- @IBOutlets -----------
    
    @IBOutlet weak var logIntypeContainerView: UIView!
    @IBOutlet weak var logInTypeLabel: UILabel!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }

    //MARK: ----------------- View LifeCycle -------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserStore.shared.bitlyURL  == nil{
              getShortReferralURL()
        }
        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//         if let _user = appDelegate.user{
//               user = _user
//        }
        
        if let _user = AppDelegateConstant.user{
            user = _user
        }
        
       let userType = UserStore.shared.selectedUserType
       if userType == "Tutor"{
            fetchLevels()
        }

        fetchCardsDetail()
        
        loadImagesInDocumentCell()
        initialSetUp()
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        imageCache.removeAllObjects()
    }
    
    fileprivate func getShortReferralURL() {
        
        if let referralCode = (AppDelegateConstant.user?.referralCode){
            
            let urlToShort = "\(sharingBaseURL)?type=invite&token=\(referralCode)"//sharingBaseURL+"?type=invite&token="+referralCode
    
            Bitly.shorten(urlToShort) { (response, error) in
                if error == nil{
                    UserStore.shared.bitlyURL = response?.bitlink
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let user = AppDelegateConstant.user{
            
            UserStore.shared.selectedUserType = user.userType
            
            let userType = UserStore.shared.selectedUserType
            
//            if let firstName = user.firstName{
//                self.headerTitle.text = "Welcome \(firstName)!"
//            }
            
            if userType == "Parent"{
        
                if UserStore.shared.logInType == 1{
                    if let firstName = user.parentFirstName{
                                     self.headerTitle.text = "Welcome \(firstName)!"
                    }
                }else if UserStore.shared.logInType == 2{
                    if let firstName = user.firstName{
                                       self.headerTitle.text = "Welcome \(firstName)!"
                    }
                }
             
            }else{
                if let firstName = user.firstName{
                    self.headerTitle.text = "Welcome \(firstName)!"
                }
            }

        }
        

        if AppDelegateConstant.qualification == nil{
            fetchQualification()
        }
        
        if isFromLogin == true{
            if self.dashboardDetails == nil {
                fetchDashboard()
            }
        }else{
             fetchDashboard()
        }

//        if let bookingDetail = AppDelegateConstant.bookingDetails, bookingDetail.count > 0{
//
//        }else{
//            getBookingDetails()
//        }
        
        if let _ = AppDelegateConstant.user?.userBankDetail{
            
        }else{
            self.viewBankDetails()
        }
        
       AppDelegateConstant.connectToSocketChannel()
        //connectToSocketChannel()
    }
    
    func showPopUPForSubject(){
        if let _user = AppDelegateConstant.user{
            if  let subjects = _user.subjects, subjects.count == 0 && AppDelegateConstant.shouldShowPopUPForSubjec == true{
                
                AppDelegateConstant.shouldShowPopUPForSubjec = false
                var msg = ""
                if AppDelegateConstant.user?.userType == tutorType{
                    msg = "Please update your rates for the respective subjects and levels."
                }else{
                    msg = "Please add subjects in your profile."
                }
                
                let alertController = UIAlertController(title: AppName, message: msg.localized, preferredStyle: .alert)
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                    if AppDelegateConstant.user?.userType == tutorType{
                        let vc = SubjectRateViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        let vc = LevelSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                })
                
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
      // MARK: ------------ @IBActions --------------------
    
    @IBAction func navigateToLogInScreen(_ sender: UIButton) {
        firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        GIDSignIn.sharedInstance()?.signOut()
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        Utilities.logoutUser()
        self.navigateToLandingScreen()
        
    }
    
    @IBAction func walletButtonTapped(_ sender: UIButton) {
        let walletBalanceVC = WalletBalanceVC.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        navigationController?.pushViewController(walletBalanceVC, animated: true)
    }

    @IBAction func bookmarksBtnTapped(_ sender: UIButton){
        
        if AppDelegateConstant.user?.userType == tutorType{
            
            let usersBookMarksListingViewController = UsersBookMarksListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            
            usersBookMarksListingViewController.titleVC = "Bookmarks".localized
            
            self.navigationController?.pushViewController(usersBookMarksListingViewController, animated: true)
            
        }else {
            
            let usersBookMarksListingViewController = UsersBookMarksListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            
            usersBookMarksListingViewController.titleVC = "Bookmarks".localized
            
            self.navigationController?.pushViewController(usersBookMarksListingViewController, animated: true)
            
        }
        
    }
  
    
    
    
    // MARK: ------------ PRIVATE FUNCTIONS --------------------
    
//    @objc func connectToSocketChannel() {
//
//
//        if let _ = AppDelegateConstant.socket{
//
//            guard AppDelegateConstant.socket.status == .connected else{
//                perform(#selector(connectToSocketChannel), with: nil, afterDelay: 1)
//                return
//            }
//        }else{
//            AppDelegateConstant.conectSocket()
//            guard AppDelegateConstant.socket.status == .connected else{
//                perform(#selector(connectToSocketChannel), with: nil, afterDelay: 1)
//                return
//            }
//        }
//        var token = ""
//        if UserStore.shared.isLoggedIn{
//            token = UserStore.shared.token
//        }
//        let headers = ["Authorization":  "Bearer " + token,
//                       "User": String(UserStore.shared.userID ?? 0)]
//
//        let tempHeader = ["headers":headers]
//        let subscribeData = ["channel": "private-edgem-chat",
//                             "name": "subscribe",
//                             "auth": tempHeader] as [String : Any]
//
//        #if DEBUG
//        do {
//            let json = try JSONSerialization.data(withJSONObject: subscribeData, options: [])
//            let string1 = String(data: json, encoding: String.Encoding.utf8) ?? "Data could not be printed"
//            print(string1)
//        }catch{
//            print(error.localizedDescription)
//        }
//        #endif
//
//        AppDelegateConstant.socket.emitWithAck("subscribe", subscribeData).timingOut(after: 2, callback: { (data) in
//            print("subscribed")
//
//            AppDelegateConstant.socket.on("userConnected") {data, ack in
//
//                print("==userConnected=====")
//                                if let tempData = (data[1] as? [String:AnyObject]), let id = tempData["user_online"] as? Int{
//
//                                    if Global.idArrays.contains(id) == false{
//                                        Global.idArrays.append(id)
//                                    }
//
//                                }
//
//                print(Global.idArrays)
//                 NotificationCenter.default.post(name: .userConnected, object: nil)
//            }
//
//            AppDelegateConstant.socket.on("userDisconnected") {data, ack in
//
//                print("==userDisconnected=====")
//                if let tempData = (data[1] as? [String:AnyObject]), let id = tempData["user_disconnected"] as? Int{
//
//                    if Global.idArrays.contains(id) == true{
//                        let filteredID = Global.idArrays.filter{$0 != id}
//                        Global.idArrays = filteredID
//                    }
//
//                }
//                print(Global.idArrays)
//                NotificationCenter.default.post(name: .userConnected, object: nil)
//            }
//
//        })
//        AppDelegateConstant.socket.connect()
//    }

    fileprivate func initialSetUp(){
        
        headerTitle.font = UIFont(name: "Quicksand-Medium", size: 20)
        headerTitle.textColor = theamBlackColor
        
        if UserStore.shared.selectedUserType == parentType{
            
            if UserStore.shared.logInType != 1 && UserStore.shared.logInType  != 2{
                
                logIntypeContainerView.isHidden = true
                logInTypeLabel.isHidden = true
                
            }else{
                
                logInTypeLabel.text =  UserStore.shared.logInType == 1 ? "PARENT" : "CHILD"
                
                logIntypeContainerView.isHidden = false
                logInTypeLabel.isHidden = false
                
                logIntypeContainerView.backgroundColor = theamBlackColor
                logIntypeContainerView.layer.cornerRadius = 3
                logIntypeContainerView.clipsToBounds = true
                
                logInTypeLabel.font = UIFont(name: "Quicksand-Medium", size: 15)
                logInTypeLabel.textColor = theamAppColor
                
            }
            
        }

    }
    
    func didTapRecommendedTutor(_ recommendetTutor: Recommended_tutors){
        let tutorCompleteProfileViewController = TutorCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        tutorCompleteProfileViewController.isViewController = self
        tutorCompleteProfileViewController.tutorID = recommendetTutor.tutor_id
        self.navigationController?.pushViewController(tutorCompleteProfileViewController, animated: true)
        
    }
    
    fileprivate func didTapWalletOptions(_ index: Int){
        switch index{
        case 0:
            break
        case 1:
            break
        case 2:
            break
        case 3:
            let earnCreaditViewController = EarnCreaditViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
            self.navigationController?.pushViewController(earnCreaditViewController, animated: true)
        default:
            break
        }
    }
    
    /// This function will get stored credit card from stripe and store it to Globally. The response of function will not be used in this controller.
    /// It will be used in CreditCardListViewController.
    fileprivate func fetchCardsDetail(){
        
        guard let cus_id = AppDelegateConstant.user?.btCustomerID else{return}
        
        // initialige customer id in StripeUtils which is needed to get card list
        StripeUtils.shared.customerID = cus_id
        
        StripeUtils.shared.getCardsList { (result) in
            // check card array available or not
            if let result = result{
                // check user have any card or not
                if result.count > 0{
                    // temp StoreUserCard variable
                    var storeUserCardObject =  [StoreUserCard]()
                    
                    result.forEach({ (cardData) in
                        storeUserCardObject.append(StoreUserCard(data: cardData))
                    })
                    AppDelegateConstant.user?.paymentCard = storeUserCardObject
                }else{
                    print(value: "user does not have any card")
                }
                
            }else{
                print(value: "Card arr not available")
            }
        }
    }
    
}

//MARK: -----------------Cells Delegates Methods -----------------

extension DashboardViewController: RecommendedTutorsTableViewDelegate, SubjectsTableViewCellDelegate{
    
    func selectedSubjectID(_ ID: Int) {
        if UserStore.shared.selectedUserType == "Tutor"{
             // getResponseForFilterStudent("\(ID)")
            let userListingVC = UsersListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            userListingVC.titleVC = "Students".localized
            userListingVC.vc = self
            userListingVC.getOnlySubject = true
            userListingVC.subjectID = "\(ID)"
            self.navigationController?.pushViewController(userListingVC, animated: true)
        }else{
            let userListingVC = UsersListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            userListingVC.subjectID = "\(ID)"
            userListingVC.vc = self
            userListingVC.getOnlySubject = true
            userListingVC.titleVC = "Tutors".localized
            self.navigationController?.pushViewController(userListingVC, animated: true)
        }
      
    }
    
    func viewMoreBtnTapped() {
        let userListingVC = UsersListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
       // userListingVC.subjectID = "\(ID)"
        userListingVC.titleVC = "Recommended Tutors".localized
        //userListingVC.bookMarkBtn.isHidden = true
        userListingVC.isFromViewMore = true
        self.navigationController?.pushViewController(userListingVC, animated: true)
    }
    
}

//MARK: ----------------- TableViewDelegate/DataSource -----------

extension DashboardViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row{
        case 1 :
            
                    let cell = tableView.dequeueReusableCell(withIdentifier: WalletBalanceTableViewCell.cellIdentifier(), for: indexPath) as! WalletBalanceTableViewCell
                    cell.configureCell()
                    cell.didTapWalletOptions = {[weak self] index in
                        self?.didTapWalletOptions(index)
                    }
                    if dashboardDetails != nil{
                        if let balance = self.dashboardDetails.wallet_balance{
                            //let balanceInDouble = Double(balance)
                            cell.costLabel.text = "\(balance)".changeStringNumberToWholeNumber()//String(format: "%.2f", balanceInDouble)
                        }
                        
                    }
                    cell.reloadCollectionViewData()
                    cell.selectionStyle = .none
                    return cell
            
        case 2 :
            
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: RecommendedTutorsTableViewCell.cellIdentifier(), for: indexPath) as? RecommendedTutorsTableViewCell else{
                        fatalError("Could not load RecommendedTutorsTableViewCell from table view")
                    }
                    cell.recommendedTutorsTableViewDelegate = self
                    cell.didTapRecommendedTutor = {  [weak self] recommendedTutor in
                        self!.didTapRecommendedTutor(recommendedTutor)
                    }
                    cell.configureCell()
                    if dashboardDetails != nil{
                        if let recommended_tutors = self.dashboardDetails.recommended_tutors{
                            if recommended_tutors.count > 0{
                                cell.recommendedTutorLabel.isHidden = true
                                cell.stackView.isHidden = false
                                cell.recommended_tutors = recommended_tutors
                          }else{
                                cell.stackView.isHidden = true
                                cell.recommendedTutorLabel.isHidden = false
                          }
                       }
                    }
                    cell.selectionStyle = .none
                    cell.reloadCollectionViewData()
                    return cell
            
        case 3 :
            
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: GroupTutionTableViewCell.cellIdentifire(), for: indexPath) as? GroupTutionTableViewCell else{
                        fatalError("Could not load GroupTutionTableViewCell from table view")
                    }
                    cell.configureCell()
                    cell.selectionStyle = .none
                    return cell
            
        case 4 :
            
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: SubjectsTableViewCell.cellIdentifire(), for: indexPath) as? SubjectsTableViewCell else{return UITableViewCell()}
                    cell.configureCell()
                    if dashboardDetails != nil{
                        if let subjects = self.dashboardDetails.subjects{
                            if subjects.count > 0{
                                cell.subjects = subjects
                            }
                        }
                    }
                    cell.subjectsTableViewCellDelegate = self
                    cell.reloadCollectionViewData()
                    cell.selectionStyle = .none
                    return cell
            
        default:
            
                    let cell = tableView.dequeueReusableCell(withIdentifier: DashboardBannerTableViewCell.cellIdentifier(), for: indexPath) as! DashboardBannerTableViewCell
                    // cell.delegate = self
                    if dashboardDetails != nil{
                        cell.banners = self.dashboardDetails.advertisements
                    }
                    cell.configureCell()
                    cell.selectionStyle = .none
                    cell.reloadCollectionViewData()
                    return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if let bannerCell = tableView.cellForRow(at: indexPath) as? DashboardBannerTableViewCell {
                bannerCell.timer?.invalidate()
                bannerCell.timer = nil
            }
            print("removed")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row{
        case 1 :
                     return WalletBalanceTableViewCell.cellHeight()
        case 2 :
                    return RecommendedTutorsTableViewCell.cellHeight()
        case 3 :
                    return GroupTutionTableViewCell.cellHeight()
        case 4 :
                     return 400//return UITableView.automaticDimension//return //SubjectsTableViewCell.cellHeight()
        default:
                    return DashboardBannerTableViewCell.cellHeight()
        }
       
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row{
        case 1 :
                    return WalletBalanceTableViewCell.cellHeight()
        case 2 :
                    return RecommendedTutorsTableViewCell.cellHeight()
        case 3 :
                    return GroupTutionTableViewCell.cellHeight()
        case 4 :
                    return 400//UITableView.automaticDimension//return //SubjectsTableViewCell.cellHeight()
        default:
            return DashboardBannerTableViewCell.cellHeight()
        }
    }
    
}


    //MARK: -------------------- API Services -------------

extension DashboardViewController{
    
    fileprivate func loadImagesInDocumentCell(){
        
//        DispatchQueue.global(qos: .background).async {
//            do
//            {
//                var docsImage = [UIImage]()
//                var docImage = UIImage()
//                if let user = AppDelegateConstant.user, let userDocuments = user.documents{
//                    if userDocuments.count > 0{
//                        
//                        // var docsImage: UIImage?
//                        let docs = userDocuments.map{return $0}
//                        let documents = docs.map{ imageObject -> UIImage in
//                            let imageProvider = ImageProvider()
//                            if let image = imageCache.image(forKey: imageObject.id){
//                                docsImage.append(image)
//                                docImage = image
//                            }else{
//                                if let url = URL(string: imageObject.name){
//                                    imageProvider.requestImage(from: url, completion: { (image) in
//                                        imageCache.add(image, forKey: imageObject.id)
//                                        docsImage.append(image)
//                                        docImage = image
//                                    })
//                                }
//                            }
//                            return docImage
//                        }
//                        print(documents)
//                    }
//                }
//            }
//            catch {
//                // error
//            }
//        }
    }
    
//    func fetchCardsDetail(){
//
//        if !isConnectedToNetwork(){
//            self.showNoInternetMessage()
//            return
//        }
//
//       // Utilities.showHUD(forView: self.view, excludeViews: [])
//
//        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
//        dispatchQueue.async {
//
//            let userObserver = ApiManager.shared.apiService.fetchUserPaymentCardDetails()
//            let userDisposable = userObserver.subscribe(onNext: {(cards) in
//                DispatchQueue.main.async {
//
//                  //  Utilities.hideHUD(forView: self.view)
//                    self.user?.paymentCard = cards
//                    AppDelegateConstant.user = self.user
//
//                }
//            }, onError: {(error) in
//                DispatchQueue.main.async(execute: {
//                    //Utilities.hideHUD(forView: self.view)
//                    if let error_ = error as? ResponseError {
//                        self.showSimpleAlertWithMessage(error_.description())
//                    } else {
//                        if !error.localizedDescription.isEmpty {
//                            self.showSimpleAlertWithMessage(error.localizedDescription)
//                        }
//                    }
//                    self.fetchDashboard()
//                })
//            })
//            userDisposable.disposed(by: self.disposableBag)
//
//        }
//
//
//    }

    func fetchDashboard() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            let dashboardObserver = ApiManager.shared.apiService.fetchDashboardDetails()
            let dashboardDisposable = dashboardObserver.subscribe(onNext: {(dashboardResponseDataObject) in
                DispatchQueue.main.async {
                    if self.dashboardDetails != dashboardResponseDataObject{
                          self.dashboardDetails = dashboardResponseDataObject
                        if let balance =  dashboardResponseDataObject.wallet_balance{
                             AppDelegateConstant.user?.walletBalance = Double(balance)
                        }
             
                          self.tableView.reloadData()
                    }
                    
                    self.showPopUPForSubject()
                }
            }, onError: {(error) in
                DispatchQueue.main.async(execute: {
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            })
            dashboardDisposable.disposed(by: self.disposableBag)
        }

    }
    
    // [START view bank details api]  -------> this api will be used at BankDetailViewController
    fileprivate func viewBankDetails(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
            // Utilities.showHUD(forView: self.view, excludeViews: [])
            
            let forgotObserver = ApiManager.shared.apiService.getBankDetails()
            let forgotDisposable = forgotObserver.subscribe(onNext: {(bankDetail) in
                // Utilities.hideHUD(forView: self.view)
                DispatchQueue.main.async {
                    AppDelegateConstant.user?.userBankDetail = bankDetail
                }
            }, onError: {(error) in
                DispatchQueue.main.async(execute: {
                    // Utilities.hideHUD(forView: self.view)
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            })
            forgotDisposable.disposed(by: self.disposableBag)
            
        }

    }
    // [END view bank details api]
    
    fileprivate func fetchQualification(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        // Utilities.showHUD(forView: self.view, excludeViews: [])
        AppDelegateConstant.qualification?.removeAll()
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            let qualificationObserver = ApiManager.shared.apiService.fetchQualification()
            let qualificationDisposable = qualificationObserver.subscribe(onNext: { (qualifications) in
                
                DispatchQueue.main.async {
                    AppDelegateConstant.qualification = qualifications
                }
            }, onError: { (error) in
                DispatchQueue.main.async {
                    //Utilities.hideHUD(forView: self.view)
                }
            })
            return qualificationDisposable.disposed(by: self.disposableBag)
        }
    }
    
    // Fetch Levels
    func fetchLevels(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        //Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
            let levelObserver = ApiManager.shared.apiService.fetchLevels(1)
            let levelDisposable = levelObserver.subscribe(onNext: { (paginationObject) in
                AppDelegateConstant.levelPagination = LevelPagination()
                AppDelegateConstant.levelPagination?.appendDataFromObject(paginationObject)
            }, onError: { (error) in
                DispatchQueue.main.async {
                    //Utilities.hideHUD(forView: self.view)
                }
            })
            return levelDisposable.disposed(by: self.disposableBag)
        }
    }
    
    // This API result will be use in ChatMemberlistingViewController
    func getBookingDetails() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
         let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
         dispatchQueue.async {
            
            let getBookingDetalsObserver = ApiManager.shared.apiService.fetchBookingDetails(String(0))
            let getBookingDetalsDisposable = getBookingDetalsObserver.subscribe(onNext: { (bookingPagination) in
                DispatchQueue.main.async {
                    AppDelegateConstant.bookingDetails = bookingPagination.bookingDetails
                }
            }, onError: { (error) in
                Utilities.hideHUD(forView: self.view)
            })
            return getBookingDetalsDisposable.disposed(by: self.disposableBag)
            
        }

    }
    
}



