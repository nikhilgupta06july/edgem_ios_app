//
//  BankDetailViewController.swift
//  Edgem
//
//  Created by Namespace on 12/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class BankDetailViewController: BaseViewController {
    
     // MARK: ------ Properties
    
    /// bankNames variable will be used to store all bank names
    var bankNames: [String] = Array()
    
    var bankDetail = BankDetails()
    var disposableBag = DisposeBag()
    var activeTextField: UITextField!
    var bankPicker = UIPickerView()
    
    let labelFields = ["Full Name*", "Bank Name*", "Account Number* (omit dashes)", "Re-Enter Account Number* (omit dashes)"]
    var rowCount = 0
    
    // MARK: ------ @IBOutlets
    
    @IBOutlet weak var vcTitleLable: UILabel!
    @IBOutlet weak var BottomHolderView: UIView!
    @IBOutlet weak var addAccountbtn: UIButton!
    @IBOutlet weak var addAccountBtncontainerView: UIView!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let bank_names  = AppVariables.banks?.bankNames{
            self.bankNames = bank_names
        }else{
            getBankName()
        }
        
        bankPicker.delegate = self
        bankPicker.dataSource = self

        rowCount = labelFields.count
         showHideNextButton(false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let bankDetails = AppDelegateConstant.user?.userBankDetail {
            addAccountbtn.setTitle("Update Account".localized, for: .normal)
            self.bankDetail = bankDetails
            self.tableView.reloadData()
            
        }else{
            addAccountbtn.setTitle("Add Account".localized, for: .normal)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // BottomHolderView
        BottomHolderView.layer.borderWidth = 0.8
        BottomHolderView.layer.borderColor = UIColor(hue: 23/255, saturation: 23/255, brightness: 23/255, alpha: 0.3).cgColor
        
        //Add Account
        addAccountbtn.layer.cornerRadius = 5
        addAccountbtn.clipsToBounds = true
//        addAccountbtn.setTitle("Add Account".localized, for: .normal)
        addAccountbtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        addAccountbtn.titleLabel?.textColor = theamBlackColor
    
        // addAccountBtncontainerView
        addAccountBtncontainerView.layer.shadowColor = UIColor.black.cgColor
        addAccountBtncontainerView.layer.shadowOpacity = 0.12
        addAccountBtncontainerView.layer.shadowRadius = 4
        addAccountBtncontainerView.layer.shadowOffset = .zero
        addAccountBtncontainerView.layer.shadowPath = UIBezierPath(rect: addAccountBtncontainerView.bounds).cgPath
    }
    
    
     // MARK: ------ @IBActions
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigateBack(sender: sender)
    }
    
    @IBAction func didTappedAddAccountBtn(_ sender: UIButton) {
        let validationInformation = isDataValid()
        guard validationInformation.isValid else {
            self.showSimpleAlertWithMessage(validationInformation.message)
            return
        }
        
        getBankDetails()
        
    }
    
    
     // MARK: ------ Private Functions
    
    fileprivate func configureTextField(_ customTextField: EdgemCustomTextField) {
        
        customTextField.delegate = self
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        
        switch customTextField.tag {
        case BankDetailTextFieldType.fullName.rawValue:
            
            customTextField.keyboardType = .asciiCapable
            customTextField.autocapitalizationType = .words
    
        case BankDetailTextFieldType.bankName.rawValue:
            
            customTextField.keyboardType = .asciiCapable
            customTextField.autocapitalizationType = .words
            customTextField.inputView = bankPicker
            
        case BankDetailTextFieldType.accountNumber.rawValue:
             customTextField.keyboardType = .numberPad
             customTextField.isSecureTextEntry = false
            break
        case BankDetailTextFieldType.reAccountBankNumber.rawValue:
            customTextField.keyboardType = .numberPad
            break

        default:
            customTextField.keyboardType = .asciiCapable
        }
        
    }
    
    func showHideNextButton(_ status: Bool) {
        
        if status == false{
            addAccountbtn.isUserInteractionEnabled = false
            addAccountbtn.backgroundColor = UIColor(red: 255.0/255.0, green: 226.0/255.0, blue: 115.0/255.0, alpha: 0.5)//theamAppColor//UIColor(white: 1, alpha: 0.60)
        }else{
            addAccountbtn.isUserInteractionEnabled = true
            addAccountbtn.backgroundColor = theamAppColor
        }
        
    }
    
    func updateDataIntoTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
        
        switch customTextField.tag {
            
        case  BankDetailTextFieldType.fullName.rawValue:
            fromCell ? (customTextField.text = bankDetail.accountHolderName) : (bankDetail.accountHolderName = customTextField.text!)
            break
        case BankDetailTextFieldType.bankName.rawValue:
            fromCell ? (customTextField.text = bankDetail.bankName) : (bankDetail.bankName = customTextField.text!)
            break
        case BankDetailTextFieldType.accountNumber.rawValue:
            fromCell ? (customTextField.text = bankDetail.accountNumber) : (bankDetail.accountNumber = customTextField.text!)
            break
        case BankDetailTextFieldType.reAccountBankNumber.rawValue:
           
            fromCell ? (customTextField.text = bankDetail.reAccountNumber) : (bankDetail.reAccountNumber = customTextField.text!)
            break
            
        default:
            break
        }
        
        if  bankDetail.accountHolderName == "" || bankDetail.bankName == "" || bankDetail.accountNumber == "" || bankDetail.reAccountNumber == ""{
            showHideNextButton(false)
            if fromCell == false{
                tableView.beginUpdates()
                tableView.endUpdates()
            }
        }
        
    }
    

    func isDataValid() -> (isValid: Bool, message: String){
        
        var errorMessages: [String] = []
        
        if  bankDetail.accountHolderName == "" ,  bankDetail.bankName == "",  bankDetail.accountNumber == "",  bankDetail.reAccountNumber == ""{
             return (isValid: false, message: "")
        }else{
            for index in 0..<labelFields.count {
                
                let currentIndexPath = IndexPath.init(row: index, section: 0)
                
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    
                    switch index {
                        
                    case BankDetailTextFieldType.fullName.rawValue: cell.validateFirstName(text: bankDetail.accountHolderName ?? "")
                        
                    case BankDetailTextFieldType.bankName.rawValue: cell.validateBankName(text: bankDetail.bankName ?? "")
                        
                    case BankDetailTextFieldType.accountNumber.rawValue: cell.validateAccountNumber(text: bankDetail.accountNumber ?? "")
                        
                    case BankDetailTextFieldType.reAccountBankNumber.rawValue: cell.validateConfirmAccount(text: bankDetail.reAccountNumber ?? "" , account: bankDetail.accountNumber ?? "")
                    
                    default:
                        break
                        
                    }
                    let message = cell.errorLabel.text ?? ""
                    if !message.isEmptyString() {
                        errorMessages.append(message)
                    }
                }
                
            }
            tableView.beginUpdates()
            tableView.endUpdates()
            let isValidData = errorMessages.count == 0
            return (isValid: isValidData, message: isValidData ? "" : errorMessages[0])
        }

    }

}

extension BankDetailViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
//    func managePickerView() {
//        let textField = activeTextField as! EdgemCustomTextField
//        genderPickerView.reloadAllComponents()
//        genderPickerView.reloadInputViews()
//        textField.inputView = genderPickerView
//        let index = genderOptions.index(of: userRegistration.gender)
//        if let _index = index {
//            self.genderPickerView.selectRow(_index, inComponent: 0, animated: true)
//        } else {
//            genderPickerView.selectRow(0, inComponent: 0, animated: true)
//            activeTextField.text = genderOptions[0]
//        }
//        //        }
//    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.bankNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.bankNames[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        activeTextField.text = self.bankNames[row]
    }
    
}


// MARK: --------- Text Field methods
extension BankDetailViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        updateDataIntoTextField(textField as! EdgemCustomTextField, fromCell: false)
        
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        
        let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
        
        switch textField.tag {
            
        case BankDetailTextFieldType.fullName.rawValue:
            cell.validateFirstName(text: textField.text ?? "")
            
        case BankDetailTextFieldType.bankName.rawValue:
            cell.validateBankName(text: textField.text ?? "")
            
        case BankDetailTextFieldType.accountNumber.rawValue:
            cell.validateAccountNumber(text: textField.text ?? "")
            
        case BankDetailTextFieldType.reAccountBankNumber.rawValue:
           cell.validateConfirmAccount(text: textField.text!, account: bankDetail.accountNumber ?? "")
            
        default:
            break
        }
        
        if bankDetail.accountHolderName != "" && bankDetail.bankName != "" ||  bankDetail.accountNumber != "" || bankDetail.reAccountNumber != ""{
            showHideNextButton(isDataValid().isValid)
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        
        switch textField.tag {
        case 2:
            return (textField.text?.count)! < 19
        case 3:
            return (textField.text?.count)! < 19
        default:
            return true
        }
    }
    
}

// MARK: ------TableView Methods

extension BankDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EdgemTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemTextFieldTableViewCell
        cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
        configureTextField(cell.customTextField)
        updateDataIntoTextField(cell.customTextField, fromCell: true)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}


// MARK - API Implementation

extension BankDetailViewController{
    
    func getBankDetails(){

        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }

        Utilities.showHUD(forView: self.view, excludeViews: [])

        let bankDetailData = self.bankDetail.getBankDetailParamsDict()

        let forgotObserver = ApiManager.shared.apiService.manageBankDetails(bankDetailData)
        let forgotDisposable = forgotObserver.subscribe(onNext: {(bankDetail) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
                self.bankDetail = bankDetail
                AppDelegateConstant.user?.userBankDetail = bankDetail
                self.tableView.reloadData()
                self.showSimpleAlertWithMessage("Your bank account has been updated successfully".localized)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        forgotDisposable.disposed(by: disposableBag)
    }
    
    // [START get all banks]
    func getBankName(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let bankObserver = ApiManager.shared.apiService.getBank()
        
        let bankDisposable = bankObserver.subscribe(onNext: {(banks) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                AppVariables.banks = banks
                self.bankNames = banks.bankNames
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        bankDisposable.disposed(by: self.disposableBag)
    }
    // [END get all banks]
    
}


