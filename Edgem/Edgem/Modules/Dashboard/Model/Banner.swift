//
//  Banner.swift
//  Edgem
//
//  Created by Hipster on 28/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import Foundation
import Unbox

struct Banner: Unboxable{
    
    var countryID: String!
    var title: String!
    var agencyID: String!
    var url: String!
    var imageURL: String!
    var slideTime: String
    var bannerOrder: String!
    var status: String!
    var assetID: String!
    
    init(unboxer: Unboxer) throws {
        self.countryID = unboxer.unbox(key: "country_id") ?? ""
        self.title = unboxer.unbox(key: "title") ?? ""
        self.url = unboxer.unbox(key: "url") ?? ""
        self.imageURL = unboxer.unbox(key: "image") ?? ""
        self.agencyID = unboxer.unbox(key: "agency_id") ?? ""
        self.slideTime = unboxer.unbox(key: "slide_time") ?? ""
        self.bannerOrder = unboxer.unbox(key: "banner_order") ?? ""
        self.status = unboxer.unbox(key: "status") ?? ""
        self.assetID = unboxer.unbox(key: "asset_id") ?? ""
    }
    
}
