
import Foundation

struct Subjects : Codable, Equatable {
	let title : String?
	let subject_id : Int?
	let subject_icon : String?

	enum CodingKeys: String, CodingKey {

		case title = "title"
		case subject_id = "subject_id"
		case subject_icon = "subject_icon"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		subject_id = try values.decodeIfPresent(Int.self, forKey: .subject_id)
		subject_icon = try values.decodeIfPresent(String.self, forKey: .subject_icon)
	}

}
