
import Foundation

struct Recommended_tutors : Codable,Equatable {
    
	let first_name : String?
	let last_name : String?
	let tutor_id : Int?
	let price_perHour : String?
	let real_image : String?
    let rating: Int?
    let rate_per_hour: String?
    let averageRating: CGFloat?
    let subjects : [RecommendedTutorSubjects]?
    
    static func == (lhs: Recommended_tutors, rhs: Recommended_tutors) -> Bool {
        return true
    }


	enum CodingKeys: String, CodingKey {

		case first_name = "first_name"
		case last_name = "last_name"
		case tutor_id = "tutor_id"
		case price_perHour = "price_perHour"
		case real_image = "real_image"
        case rating = "ratings"
        case rate_per_hour = "rate_per_hour"
        case averageRating = "average_ratings"
        case subjects = "subjects"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
		last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
		tutor_id = try values.decodeIfPresent(Int.self, forKey: .tutor_id)
		price_perHour = try values.decodeIfPresent(String.self, forKey: .price_perHour)
		real_image = try values.decodeIfPresent(String.self, forKey: .real_image)
        rating = try (values.decode(Int.self, forKey: .rating))
        rate_per_hour = String(try values.decode(Int.self, forKey: .rate_per_hour))
        averageRating = try values.decode(CGFloat.self, forKey: .averageRating)
        subjects = try values.decodeIfPresent([RecommendedTutorSubjects].self, forKey: .subjects)
	}
    
}
