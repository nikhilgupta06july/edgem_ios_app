
import Foundation

struct RecommendedTutorSubjects : Codable {
    
    let title : String?
    let subject_id : Int?
    let otherSubjectTitle: String?
    let lockRate: String?
    var levelData: [Level]?
    
    enum CodingKeys: String, CodingKey {
        
        case title                      = "title"
        case subject_id            = "subject_id"
        case otherSubjectTitle = "other_subject"
        case lockRate               = "lock_rate"
        case levelData              = "level"
    }
    
    init(from decoder: Decoder) throws {
        
        let values                      = try decoder.container(keyedBy: CodingKeys.self)
        title                               = try values.decodeIfPresent(String.self, forKey: .title)
        subject_id                     = try values.decodeIfPresent(Int.self, forKey: .subject_id)
        levelData                       = try values.decodeIfPresent([Level].self, forKey: .levelData)
        otherSubjectTitle          = try values.decodeIfPresent(String.self, forKey: .otherSubjectTitle)
        lockRate                        = String(try values.decode(Int.self, forKey: .lockRate))
       
        
    }
    
}
