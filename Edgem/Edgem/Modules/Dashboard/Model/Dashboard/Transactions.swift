//
//  Transactions.swift
//  Edgem
//
//  Created by Namespace on 20/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class Transactions: Unboxable{
    
    
    var transactionID : Int
    var transactionType : String
    var amount : String
    var brainTreeTransactionID : String
    var cardBrand : String
    //var cardStart: String
    var cardEnd: String
    var description: String
    var addedOn: String
    
    required init(unboxer: Unboxer) throws {
        self.transactionID = try unboxer.unbox(key: "transaction_id")
        self.transactionType = try unboxer.unbox(key: "type")
        self.amount = try unboxer.unbox(key: "amount")
        self.brainTreeTransactionID = try unboxer.unbox(key: "bt_trans_id")
        self.cardBrand = try unboxer.unbox(key: "card_brand")
        //self.cardStart = try unboxer.unbox(key: "card_start")
        self.cardEnd = try unboxer.unbox(key: "card_end")
        self.description = try unboxer.unbox(key: "description")
        self.addedOn = try unboxer.unbox(key: "added_on")
    }
    
}

