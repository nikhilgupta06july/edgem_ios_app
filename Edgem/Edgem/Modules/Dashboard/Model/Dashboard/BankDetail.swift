//
//  BankDetail.swift
//  Edgem
//
//  Created by Namespace on 13/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox


class BankDetail: Unboxable {
    
    // Properties
    var id: Int?
    var userId: Int?
    var accountHolderName: String?
    var bankName: String?
    var accountNumber: Int?
    var createdAt: String?
    var updatedAt: String?
    var braintreeNounce: String?
    
    init(){
        
    }

    required init(unboxer: Unboxer) throws {
        self.id = try? unboxer.unbox(key: "id")
        self.userId = try? unboxer.unbox(key: "user_id")
        self.accountHolderName = try? unboxer.unbox(key: "account_holder_name")
        self.bankName = try? unboxer.unbox(key: "bank_name")
        self.accountNumber = try? unboxer.unbox(key: "account_number")
        self.createdAt = try? unboxer.unbox(key: "created_at")
        self.updatedAt = try? unboxer.unbox(key: "updated_at")
        self.braintreeNounce = try? unboxer.unbox(key: "braintree_nonce")
    }
    
    func getBankDetailParamsDict() -> [String : String] {
        
        var bankDetailDict = [String : String]()
        
        bankDetailDict = [
            "account_holder_name" : self.accountHolderName,
            "bank_name" : self.bankName,
            "account_number" : self.accountNumber
            ] as! [String : String]
        
        return bankDetailDict
    }
    
    
}

