//
//  UserTransactions.swift
//  Edgem
//
//  Created by Namespace on 20/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class UserTransactions:Unboxable{
    let transactions : [Transactions]?
    let pagination : Pagination?
    
    init(){
        self.transactions = nil
        self.pagination = nil
    }
    
    required init(unboxer: Unboxer) throws {
        self.transactions =  unboxer.unbox(key: "transactions")
        self.pagination =  unboxer.unbox(key: "pagination")
    }
    
}
