/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct ResponseData : Codable {
	let wallet_balance : Int?
	let subjects : [Subjects]?
	let recommended_tutors : [Recommended_tutors]?
	let advertisements : [Advertisements]?

	enum CodingKeys: String, CodingKey {

		case wallet_balance = "wallet_balance"
		case subjects = "subjects"
		case recommended_tutors = "recommended_tutors"
		case advertisements = "advertisements"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		wallet_balance = try values.decodeIfPresent(Int.self, forKey: .wallet_balance)
		subjects = try values.decodeIfPresent([Subjects].self, forKey: .subjects)
		recommended_tutors = try values.decodeIfPresent([Recommended_tutors].self, forKey: .recommended_tutors)
		advertisements = try values.decodeIfPresent([Advertisements].self, forKey: .advertisements)
	}

}
