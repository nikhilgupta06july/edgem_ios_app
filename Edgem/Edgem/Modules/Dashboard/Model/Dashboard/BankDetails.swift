//
//  BankDetails.swift
//  Edgem
//
//  Created by Namespace on 18/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class BankDetails:Unboxable {
    
    var id: Int!
    var userID: Int!
    var accountHolderName: String!
    var bankName: String!
    var accountNumber: String!
    var reAccountNumber: String! 
    var braintreeNounce: String!
    var createdAt: String!
    var updatedAt: String!
    
    init(){
        
    }
    
    required init(unboxer: Unboxer) throws {
        self.id = try? unboxer.unbox(key: "id")
        self.userID = try? unboxer.unbox(key: "user_id")
        self.accountHolderName = try? unboxer.unbox(key: "account_holder_name")
        self.bankName = try? unboxer.unbox(key: "bank_name")
        self.accountNumber = try? unboxer.unbox(key: "account_number")
        self.reAccountNumber =  try? unboxer.unbox(key: "account_number")
        self.braintreeNounce = try? unboxer.unbox(key: "braintree_nonce")
        self.createdAt = try? unboxer.unbox(key: "created_at")
        self.updatedAt = try? unboxer.unbox(key: "updated_at")
        
    }
    
    func getBankDetailParamsDict() -> [String : String] {
        
        var bankDetailDict = [String : String]()
        
        bankDetailDict = [
            "account_holder_name" : self.accountHolderName,
            "bank_name" : self.bankName,
            "account_number" : self.accountNumber
            ] as! [String : String]
        
        return bankDetailDict
    }

}

