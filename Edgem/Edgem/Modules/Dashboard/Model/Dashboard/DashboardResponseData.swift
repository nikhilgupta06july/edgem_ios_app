

import Foundation

struct DashboardResponseData : Codable, Equatable {
	let wallet_balance : Int?
	let subjects : [Subjects]?
	let recommended_tutors : [Recommended_tutors]?
	let advertisements : [Advertisements]?

	enum CodingKeys: String, CodingKey {

		case wallet_balance = "wallet_balance"
		case subjects = "subjects"
		case recommended_tutors = "recommended_tutors"
		case advertisements = "advertisements"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		wallet_balance = try values.decodeIfPresent(Int.self, forKey: .wallet_balance)
		subjects = try values.decodeIfPresent([Subjects].self, forKey: .subjects)
		recommended_tutors = try values.decodeIfPresent([Recommended_tutors].self, forKey: .recommended_tutors)
		advertisements = try values.decodeIfPresent([Advertisements].self, forKey: .advertisements)
	}

}
