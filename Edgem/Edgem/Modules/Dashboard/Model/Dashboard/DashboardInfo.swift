
import Foundation

struct DashboardInfo : Codable {
	let status : String?
	let data : Data?
	let message : String?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case data = "data"
		case message = "message"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		data = try values.decodeIfPresent(Data.self, forKey: .data)
		message = try values.decodeIfPresent(String.self, forKey: .message)
	}

}
