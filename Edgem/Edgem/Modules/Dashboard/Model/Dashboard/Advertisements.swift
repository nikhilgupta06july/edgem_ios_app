
import Foundation

struct Advertisements : Codable, Equatable {
	let title : String?
	let banner_file : String?

	enum CodingKeys: String, CodingKey {

		case title = "title"
		case banner_file = "banner_file"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		banner_file = try values.decodeIfPresent(String.self, forKey: .banner_file)
	}

}
