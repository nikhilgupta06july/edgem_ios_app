//
//  Banks.swift
//  Edgem
//
//  Created by Namespace on 31/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class Banks: Unboxable {
    var bankNames: [String]!
    
    required init(unboxer: Unboxer) throws {
         self.bankNames = try? unboxer.unbox(key: "bank_list")
    }
    
}
