//
//  DashboardSectionViewInfo.swift
//  Edgem
//
//  Created by Hipster on 04/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation

struct DashboardSectionViewInfo{
    var sectionTitle: String = ""
    var currency: String = ""
    var cost: String = ""
    
    init(){
        
    }
    
    mutating func setValues(data: Dictionary<String, AnyObject>){
        self.sectionTitle = data["sectionTitle"] as! String
        self.currency = data["currency"] as! String
        self.cost = data["cost"] as! String
    }
        
}
