//
//  DashboardInfo.swift
//  Edgem
//
//  Created by Hipster on 04/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class DashboardInfo: Unboxable {
    
//    var trainersOnline = 0
//    var leadsGenerated = 0
//    var activeSubjects = 0
//    var subjectsAvailable = 0
//    var locations = 0
//    var topSubjectsCount = 0
//    var featuredTutorsCount = 0
//    var featuredPartnersCount = 0
//    var topSubjects = [SubjectCategory]()
//    var partners = [Partner]()
    var banners = [Banner]()
//    var topTutors = [Tutor]()
//    var announcements = [Announcement]()
//
    required init(unboxer: Unboxer) throws {
//        self.trainersOnline = unboxer.unbox(key: "trainers_online") ?? 0
//        self.leadsGenerated = unboxer.unbox(key: "leads_generated") ?? 0
//        self.activeSubjects = unboxer.unbox(key: "active_subjects") ?? 0
//        self.subjectsAvailable = unboxer.unbox(key: "subjects_available") ?? 0
//        self.locations = unboxer.unbox(key: "locations") ?? 0
//        self.topSubjectsCount = unboxer.unbox(key: "num_of_top_subjects") ?? 0
//        self.featuredTutorsCount = unboxer.unbox(key: "num_of_top_tutors") ?? 0
//        self.featuredPartnersCount = unboxer.unbox(key: "num_of_top_partners") ?? 0
//        var categoryArray = [SubjectCategory]()
//        if let categoryObjArray = unboxer.dictionary["top_subjects"] as? [[String: AnyObject]],
//            categoryObjArray.count > 0 {
//            for categoryDict in categoryObjArray {
//                do {
//                    let subjectCategory: SubjectCategory = try unbox(dictionary: categoryDict)
//                    categoryArray.append(subjectCategory)
//                } catch {
//                    print("Couldn't parse Subject")
//                }
//            }
//        }
//        self.topSubjects = categoryArray
//
//        var partnersArray = [Partner]()
//        if let partnerObjArray = unboxer.dictionary["featured_partner"] as? [[String: AnyObject]],
//            partnerObjArray.count > 0 {
//            for partnerDict in partnerObjArray {
//                do {
//                    let partner: Partner = try unbox(dictionary: partnerDict)
//                    print(partnerDict)
//                    let Obj = (partnerDict["assets"] as? [String:AnyObject])
//                    if let idobj = Obj{
//                        if let finObj = idobj["id_asset"]{
//                            partner.assetId = "\(finObj)"
//                        }
//                    }
//                    partnersArray.append(partner)
//                } catch {
//                    print("Couldn't parse Partner")
//                }
//            }
//        }
//        self.partners = partnersArray
        
        var bannersArray = [Banner]()
        if let bannerObjArray = unboxer.dictionary["banners"] as? [[String: AnyObject]],
            bannerObjArray.count > 0 {
            for bannerDict in bannerObjArray {
                do {
                    let banner: Banner = try unbox(dictionary: bannerDict)
                    bannersArray.append(banner)
                } catch {
                    print("Couldn't parse Banner")
                }
            }
        }
        self.banners = bannersArray
        
//        var tutorsArray = [Tutor]()
//        if let tutorObjArray = unboxer.dictionary["top_tutors"] as? [[String: AnyObject]],
//            tutorObjArray.count > 0 {
//            for tutorDict in tutorObjArray {
//                if let tutorUserDict = tutorDict["user"] as? [String: AnyObject] {
//                    do {
//                        let tutor: Tutor = try unbox(dictionary: tutorUserDict)
//                        tutorsArray.append(tutor)
//                    } catch {
//                        print("Couldn't parse Tutor")
//                    }
//                }
//            }
//        }
//        self.topTutors = tutorsArray
        
//        var announcementsArray = [Announcement]()
//        if let newsObjArray = unboxer.dictionary["news"] as? [[String: AnyObject]],
//            newsObjArray.count > 0 {
//            for newsDict in newsObjArray {
//                do {
//                    let announcement: Announcement = try unbox(dictionary: newsDict)
//                    announcementsArray.append(announcement)
//                } catch {
//                    print("Couldn't parse Announcement")
//                }
//            }
//        }
//        self.announcements = announcementsArray
        
        
    }
    
}

