//
//  UserActivityTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 17/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class UserActivityTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var activity_image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func cellIdentifire() -> String {
        return "UserActivityTableViewCell"
    }
    
    func configureCell(with activity: Activities){
        
        if let activityType = activity.activityType{
            
    
            if activityType == "0" || activityType == "1"{
                
                 userName.text = activity.activitySubject
                
                 messageLabel.text = activity.activityData
                
                 if let date = activity.activityRecorded{
                    
                   dateLabel.text = activity.activityRecorded?.formattedDate(date, format: "yyyy-MM-dd")
                    
                 }
                
                 activity_image.image = UIImage(named: "notification")
                
            }else if activityType == "2"{
                
                 userName.text = activity.activitySubject
                
                if let date = activity.activityRecorded{
                    
                    dateLabel.text = activity.activityRecorded?.formattedDate(date, format: "yyyy-MM-dd")
                    
                }
                
                if let data = activity.activityData{
                    
                    if let dict = convertToDict(data), let lastMsg = dict["last_message"] as? [String:AnyObject]{
                        
                        // Image
                        if let imageURL = lastMsg["real_image"] as? String, !imageURL.isEmpty{
                            
                            GetImageFromServer.getImage(imageURL: imageURL) { (image) in
                                
                                self.activity_image.image = image
                                
                            }
                        }
                        
                        //Description
                        if let msgType = lastMsg["message_type"] as? Int{
                            
                            if msgType == 1{
                                
                                 messageLabel.text = "Image"
                                
                            }else{
                                
                                if let msg = lastMsg["conversation_message"] as? String{
                                    
                                  messageLabel.text = msg
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.activity_image.image = nil
        
    }
    
    // Helping Functions
    
    private func convertToDict(_ text: String) -> [String:Any]? {
        
        if let data = text.data(using: .utf8){
            
            do {
                
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                
            }catch{
                
                print(value: error.localizedDescription)
            }
            
        }
        
        return nil
        
    }

}

extension String{
    func toDictionary() -> NSDictionary {
        let blankDict : NSDictionary = [:]
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            } catch {
                print(error.localizedDescription)
            }
        }
        return blankDict
    }
}
