//
//  Activities.swift
//  Edgem
//
//  Created by Namespace on 17/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class Activities : Unboxable {


    let activityID : Int?
    let activityType : String?
    let activitySubject : String?
    let activityData : String?
    let activityRecorded : String?
    
    init() {
        
         self.activityID = nil
         self.activityType = nil
         self.activitySubject = nil
         self.activityData = nil
         self.activityRecorded = nil
        
    }

    required init(unboxer: Unboxer) throws {
        self.activityID = unboxer.unbox(key: "activity_id")
        self.activityType = unboxer.unbox(key: "activity_type")
        self.activitySubject = unboxer.unbox(key: "activity_subject")
        self.activityData = unboxer.unbox(key: "activity_data")
        self.activityRecorded = unboxer.unbox(key: "activity_recorded")
    }
    
}

