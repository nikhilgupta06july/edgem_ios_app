//
//  UserActivities.swift
//  Edgem
//
//  Created by Namespace on 17/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class UserActivities : Unboxable {
    var activities : [Activities]?
    let pagination : Pagination?
    
    init(){
        self.activities     = nil
        self.pagination  = nil
    }
    
    required init(unboxer: Unboxer) throws {
        self.activities    =  unboxer.unbox(key: "activities")
        self.pagination =  unboxer.unbox(key: "pagination")
    }
    
}
