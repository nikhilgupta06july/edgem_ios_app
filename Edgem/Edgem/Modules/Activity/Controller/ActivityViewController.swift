//
//  ActivityViewController.swift
//  Edgem
//
//  Created by Namespace on 17/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class ActivityViewController : BaseViewController {
    
    // MARK: - Properties
    var rowCount = 0
    var userActivities = UserActivities()
    var activities = [Activities]()
    var userDummyActivities:[(Int, Activities)] = [(Int, Activities)]()
    var disposableBag = DisposeBag()
    var pagination = Pagination()
    
    // MARK: - @IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noActivityMsg: UILabel!
    // MARK: - ViewLifeCycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.tabBarItem.badgeValue = nil
        activities.removeAll()
        pagination = Pagination.init()
        
        self.getActivities()
    }
    
    // MARK: - @IBActions
    
    
    // MARK: - Helping Functions
    
    private func initialSetUp(){
        
         self.noActivityMsg.isHidden = true
        
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
    }
//
    fileprivate func removalStatus(_ status: Bool){

        if status == true{
            print(userDummyActivities)
            self.userDummyActivities.removeAll()
            print(userDummyActivities)
        }else{
            if userDummyActivities.count > 0 {
                for index in 0..<self.userDummyActivities.count{
                    self.activities.insert(self.userDummyActivities[index].1, at: self.userDummyActivities[index].0)
                }
                self.rowCount = self.activities.count
                self.tableView.reloadData()
            }
        }
    }
    
}

    // MARK: - TableView Delegates/Datasources

extension ActivityViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UserActivityTableViewCell.cellIdentifire(), for: indexPath) as? UserActivityTableViewCell else{return UITableViewCell()}
        
//        if let activity = self.userActivities.activities?[safe:indexPath.row]{
//             cell.configureCell(with: activity)
//        }
    
        if let activity = self.activities[safe:indexPath.row]{
            cell.configureCell(with: activity)
        }
        
        return cell
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let action =  UIContextualAction(style: .normal, title: "Remove", handler: { (action,view,completionHandler ) in
            //do stuff
            if let currentActivity = self.activities[safe:indexPath.row], let activityID = currentActivity.activityID{
               // self.activities.remove(at: indexPath.row)
                self.activities =  self.activities.filter{$0.activityID != activityID}
                let touple = (indexPath.row,currentActivity)
                self.userDummyActivities.append(touple)
                self.rowCount = self.activities.count
                self.tableView.reloadData()
                 self.removeActivity(activityID)
            }
            
            completionHandler(true)
        })
        action.image = #imageLiteral(resourceName: "settings")//UIImage(named: "deleteCards.png")
        action.backgroundColor = .red
        let confrigation = UISwipeActionsConfiguration(actions: [action])
        
        return confrigation
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == activities.count-1 && pagination.currentPage != pagination.lastPage{
            getActivities()
        }
    }
    
}

    // MARK: - API Implementations

extension ActivityViewController {
    
    // [START remove user activity api]
    fileprivate func removeActivity(_ activityID: Int) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
       // Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let params: [String : Any] = ["activity_id" : "\(activityID)"]
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
            let userObserver = ApiManager.shared.apiService.removeUserActivity(params as [String : AnyObject])
            let userDisposable = userObserver.subscribe(onNext: {(message) in
                DispatchQueue.main.async {
                   // Utilities.hideHUD(forView: self.view)
                     self.removalStatus(true)
                    //self.tableView.reloadData()
                }
            }, onError: {(error) in
                DispatchQueue.main.async(execute: {
                    //Utilities.hideHUD(forView: self.view)
                     self.removalStatus(false)
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            })
            userDisposable.disposed(by: self.disposableBag)
        }
        
    }
    // [END remove user activity api]
    
    
    
    // [START get user activity api]
    fileprivate func getActivities(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.getUserActivities(String(pagination.currentPage+1))
        let userDisposable = userObserver.subscribe(onNext: {(userActivity) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                
                if let pagination = userActivity.pagination, let activities = userActivity.activities{
                    
                    self.pagination = pagination
                    
                    self.activities += activities
                    
                }
                
                if self.activities.count > 0{
                    self.noActivityMsg.isHidden = true
                     self.rowCount = self.activities.count
                     self.tableView.reloadData()
                }else{
                    self.noActivityMsg.isHidden = false
                }
                
//                if (userActivity.activities?.count)! > 0{
//                    self.rowCount = (userActivity.activities?.count)!
//                    self.activities = userActivity.activities!
//                }
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    // [END get user activity api]

    
}
