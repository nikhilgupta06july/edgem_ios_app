//
//  SelectAvailibilityViewController.swift
//  
//
//  Created by Namespace on 23/05/19.
//

import UIKit
import SpreadsheetView
import RxSwift

class SelectAvailibilityViewController: BaseViewController {
    
    var headers = ["".localized.uppercased(), "MON".localized, "TUE".localized, "WED".localized, "THU".localized, "FRI".localized, "SAT".localized, "SUN".localized]
    
    var data = [ [String] ]()
    
    var schedules: DayAvailability?
    var disposableBag = DisposeBag()
    
    var dateRange: [String : Any]?
    
    var startDateForServer: String = ""
    var endDateForServer: String = ""
    
    var userID: Int?
    var offerID: Int?
    var currentLesson: String = ""
    
    lazy var scheduleClassParam: [String:String] = [  "hour" : "",
                                                                                "day_name" : "",
                                                                                "scheduled_date" : "",
                                                                                "offer_id" : "",
                                                                                "lesson_number" : "" ]

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var calendarSpreadSheet: SpreadsheetView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var btnContainerView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let startDate = self.dateRange?["startDate"], let endDate = self.dateRange?["endDate"] {
        print(startDate, endDate)

            let  _startDate = getFormatedDate(startDate as! Date)
            let  _endDate = getFormatedDate(endDate as! Date)
            
            self.dateLabel.text = _startDate + " - " + _endDate
            
            self.startDateForServer = datesForServer(DateForServer: _startDate)
            self.endDateForServer = datesForServer(DateForServer: _endDate)
        }
        checkUserWeeklyAvailibility()
    }
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        bottomContainerView.layer.borderWidth = 1
        bottomContainerView.layer.borderColor = staticMessageCellBorderColor.cgColor
        bottomContainerView.clipsToBounds = true
        disableBtn()
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        scheduleClass()
    }
    
    
    @IBAction func backwordDateBtnTapped(_ sender: UIButton) {
        
        if let startDate = self.dateRange?["startDate"] {
            let _startDate = startDate as! Date
            
            let startOfWeek = _startDate.startOfWeekForPrev
            let endOfWeek = _startDate.endOfWeekForPrev
            
            guard let startDate = startOfWeek else{return}
            guard let endDate = endOfWeek else{return}
            
            if endDate < Date() {
                return
            }
            let st = getFormatedDate(startDate )
            let et = getFormatedDate(endDate )
            
            self.dateLabel.text = st + " - " +  et
            
            self.dateRange?["startDate"] = startOfWeek
            self.dateRange?["endDate"] = endOfWeek
            
            self.startDateForServer = datesForServer(DateForServer: st)
            self.endDateForServer = datesForServer(DateForServer: et)
            
            checkUserWeeklyAvailibility()

        }

    }
    
    @IBAction func forwordDateBtnTapped(_ sender: UIButton) {

        if let _ = self.dateRange?["startDate"], let endDate = self.dateRange?["endDate"] {
            
            let _endDate = endDate as! Date
            
            guard let startOfWeek = _endDate.getThisWeekStart() else {return}
            guard let endOfWeek = _endDate.getThisWeekEnd() else{return}
    
            self.dateLabel.text =  getFormatedDate(startOfWeek ) + " - " +  getFormatedDate(endOfWeek )
            
            self.dateRange?["startDate"] = startOfWeek
            self.dateRange?["endDate"] = endOfWeek
            
            self.startDateForServer = datesForServer(DateForServer: getFormatedDate(startOfWeek ))
            self.endDateForServer = datesForServer(DateForServer: getFormatedDate(endOfWeek ))

            checkUserWeeklyAvailibility()
            
        }
    }
    
    fileprivate func getScheduleDay(dayIndex index: Int) -> String{
        
        switch index {
            
        case 1 : return "MON"
        
        case 2 : return "TUE"
            
        case 3 : return "WED"
            
        case 4 : return "THU"
    
        case 5 : return "FRI"
            
        case 6 : return "SAT"
            
        case 7 : return "SUN"
            
        default : return ""
        }
        
    }
    
    fileprivate func checkScheduleData(){
        if currentLesson == "" || scheduleClassParam["hour"] == "" || scheduleClassParam["scheduled_date"] == "" || scheduleClassParam["day_name"] == ""{
            enableDisableBookButton(false)
        }else{
            enableDisableBookButton(true)
        }
    }
    
    fileprivate func enableBtn() {
        btnContainerView.layer.cornerRadius = 5
        btnContainerView.layer.shadowColor = UIColor.black.cgColor
        btnContainerView.layer.shadowOpacity = 0.12
        btnContainerView.layer.shadowOffset = CGSize.zero
        btnContainerView.layer.shadowRadius = 6
        //btnContainerView.layer.shouldRasterize = true
        
        saveBtn.backgroundColor = theamAppDarkColor
        saveBtn.titleLabel?.textColor = theamBlackColor
        saveBtn.layer.cornerRadius = 5
        saveBtn.clipsToBounds = true
        saveBtn.isUserInteractionEnabled = true
    }
    
    fileprivate func disableBtn() {
        btnContainerView.layer.cornerRadius = 5
       
        saveBtn.layer.cornerRadius = 5
        saveBtn.clipsToBounds = true
        
        saveBtn.isUserInteractionEnabled = false
        saveBtn.backgroundColor = UIColor(red: 255/255, green: 210/255, blue: 113/255, alpha: 0.4)//UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        saveBtn.titleLabel?.textColor = theamGrayColor
        //saveBtn.layer.borderWidth = 1
        //saveBtn.layer.borderColor = staticMessageCellBorderColor.cgColor
        saveBtn.clipsToBounds = true
    }
    
    fileprivate func enableDisableBookButton(_ status: Bool) {
        
        if status == true{
            enableBtn()
        }else{
            disableBtn()
        }
        
    }
    
    fileprivate func refreshData(){
        
        data = getDataArrayFromSchedules()
//        checkScheduleData()
        calendarSpreadSheet.delegate = self
        calendarSpreadSheet.dataSource = self
        calendarSpreadSheet.isDirectionalLockEnabled = true
        calendarSpreadSheet.register(HeaderSpreadsheetCell.self, forCellWithReuseIdentifier: String(describing: HeaderSpreadsheetCell.self))
        calendarSpreadSheet.register(TextCell.self, forCellWithReuseIdentifier: String(describing: TextCell.self))
        calendarSpreadSheet.register(CheckBoxCell.self, forCellWithReuseIdentifier: String(describing: CheckBoxCell.self))
        
    }
    
    func getFormatedDate(_ _startDate: Date) -> String{
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let dateStr = dateFormatter.string(from: _startDate)
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: dateStr)
        
        dateFormatter.dateFormat = "EEE d MMM yy"
        dateFormatter.timeZone = TimeZone.current
        let timeStamp = dateFormatter.string(from: date!)
        
        return timeStamp
    }
    
    fileprivate func datesForServer(DateForServer startDate: String)->String {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "EEE d MMM yy"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: startDate)
    
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone.current
        let timeStamp = dateFormatter.string(from: date!)
        
        return timeStamp
        
    }
    
    func getDataArrayFromSchedules() -> [[String]] {
        var completeData = [[String]]()
        let weeksArray = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
        for i in 9...20 {
            var hourSchedule = [String]()
            if i > 9 {
                hourSchedule = ["\(i):00"]
            } else {
                hourSchedule = ["0\(i):00"]
            }
            for day in weeksArray {

                let daysSchedule = day == weeksArray[0] ? schedules?.mon :
                day == weeksArray[1] ? schedules?.tue :
                day == weeksArray[2] ? schedules?.wed :
                day == weeksArray[3] ? schedules?.thus :
                day == weeksArray[4] ? schedules?.fri :
                day == weeksArray[5] ? schedules?.sat :
                schedules?.sun
                
                let filteredSchedules = daysSchedule?.filter({ (schedule) -> Bool in
                    return "\(schedule.time)" == String(i)
                })

                if let schedulesArray = filteredSchedules, schedulesArray.count > 0 {
                    
                    for scheduleArr in schedulesArray{
                        if let startDate = self.dateRange?["startDate"] as? Date, Date() >= startDate{
                            let currentDay = Calendar.current.component(.weekday, from: Date())
                            
                            if currentDay-2 > weeksArray.firstIndex(of: day)!{
                    
                                hourSchedule.append("")
                                
                            }else if currentDay-2 == weeksArray.firstIndex(of: day)!{
                                let formatter = DateFormatter()
                                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let myString = formatter.string(from: Date())
                                
                                let currentTime = Calendar.current.component(.hour, from: formatter.date(from: myString)!)
                                    //Utilities.UTCToLocal(date: myString, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "HH:mm:ss")
                                
                                hourSchedule.append(Int(currentTime) < scheduleArr.time ? "\(scheduleArr.type)" : "")
                            }else{
                                hourSchedule.append("\(scheduleArr.type)")
                            }
                        }else{
                        hourSchedule.append("\(scheduleArr.type)")
                        }
                    }
                }else {
                    hourSchedule.append("")
                }
            }
            completeData.append(hourSchedule)
        }
        return completeData
    }
    
}

extension SelectAvailibilityViewController: SpreadsheetViewDataSource, SpreadsheetViewDelegate {
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return self.headers.count
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        if data.count < 1{
            return 0
        }
        return data.count+1 //+ data.count
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return 50
        } else {
            return (ScreenWidth - 60)/7
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if case 0 = row {
            return 45
        } else {
            return 35
        }
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        if case 0 = indexPath.row {
            let cell = calendarSpreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: HeaderSpreadsheetCell.self), for: indexPath) as! HeaderSpreadsheetCell
            cell.label.text = headers[indexPath.column]
            cell.gridlines.left = .none
            cell.gridlines.right = .none
            cell.gridlines.top = .none
            cell.gridlines.left = .none
            cell.gridlines.bottom = .none
            return cell
        } else {
            let dataString = data[indexPath.row - 1][indexPath.column]
            
            if indexPath.column == 0 {
                let cell = calendarSpreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: TextCell.self), for: indexPath) as! TextCell
                cell.label.text = dataString
                cell.label.backgroundColor = UIColor.clear
                cell.gridlines.left = .none
                cell.gridlines.top = .none
                cell.gridlines.right = .none
                cell.gridlines.bottom = .none
                
                return cell
            } else {
                let cell = calendarSpreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: CheckBoxCell.self), for: indexPath) as! CheckBoxCell
                
                cell.imageView.image = UIImage(named: dataString == "0" ? "active_avalibility" : (dataString == "1" || dataString == "2" ? "largeBooking" : "inactive_avalibility"))
                
                cell.gridlines.left = .none
                cell.gridlines.top = .none
                cell.gridlines.right = .none
                cell.gridlines.bottom = .none
                
                return cell
            }
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
            
            if indexPath.row != 0 || indexPath.column != 0 {
                guard let cellValue = data[safe: indexPath.row - 1]?[safe:indexPath.column] else{
                    preconditionFailure("cellValue is not found")
                }
                let cell = spreadsheetView.cellForItem(at: indexPath) as! CheckBoxCell
                
                for i in 0..<data.count{
                    for j in 1..<data[i].count{
                        data[i][j] = data[i][j] == "2" ? "0" : data[i][j]
                    }
                }
                
                if cellValue == "0" {
                    data[indexPath.row - 1][indexPath.column] = "2"
                    cell.imageView.image = #imageLiteral(resourceName: "largeBooking")
                    
                        let scheduleDay = getScheduleDay(dayIndex: indexPath.column)
    
                        let sheduleHour =  ((data[indexPath.row-1][0]).split(separator: ":")[0]).hasPrefix("0") ? (((data[indexPath.row-1][0]).split(separator: ":")[0]).dropFirst()) : ((data[indexPath.row-1][0]).split(separator: ":")[0])
    
                        let date = ((self.dateRange!["startDate"]!) as! Date).addingTimeInterval(TimeInterval(3600*24*(indexPath.column-1)))
                        let sheduleDate = datesForServer(DateForServer: getFormatedDate(date))
    
                        scheduleClassParam["hour"] = String(sheduleHour)
                        scheduleClassParam["day_name"] = scheduleDay
                        scheduleClassParam["scheduled_date"] = sheduleDate
    
                }else{
                    return
                }
                spreadsheetView.reloadData()
                checkScheduleData()
                
            }
        
    }
    
}

//extension SelectAvailibilityViewController{//: CalendarDetailViewControllerDelegate {

//        func calendarDidUpdate(schedules: [String : [Schedule]], userCalendarEvents: [String : [UserEvent]]) {
//            self.schedules = schedules
//            self.userEvents = userCalendarEvents
//            data = getDataArrayFromSchedules(self.schedules, events: self.userEvents)
//            shouldReloadDataOnViewDidAppear = true
//        }
    
//}

extension SelectAvailibilityViewController {
    
    fileprivate func scheduleClass() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        var availibilityDict = [String : AnyObject]()
        
        guard let offerID = self.offerID else {
            print("offer ID not found")
            return
        }
        
        availibilityDict = [
            "offer_id"      : "\(offerID)",
            "lesson_number"   : self.currentLesson,
            "hour"       : scheduleClassParam["hour"],
            "day_name" : scheduleClassParam["day_name"],
            "scheduled_date" : scheduleClassParam["scheduled_date"]
            ] as [String : AnyObject]
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let calendarObserver = ApiManager.shared.apiService.scheduleClass(availibilityDict)
        let calendarDisposable = calendarObserver.subscribe(onNext: {(message) in
            
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                self.navigationController?.popViewController(animated: true)
            }
            
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        calendarDisposable.disposed(by: disposableBag)
    }
    
    func checkUserWeeklyAvailibility() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        var availibilityDict = [String : AnyObject]()
        
        guard let id = self.userID else{return}
        
        availibilityDict = [
            "tutor_id"      : "\(id)",
            "from_date"   : self.startDateForServer,
            "to_date"       : self.endDateForServer
            ] as [String : AnyObject]
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let calendarObserver = ApiManager.shared.apiService.checkUserWeeklyAvailibility(availibilityDict)
        let calendarDisposable = calendarObserver.subscribe(onNext: {(weeklyAvailibility) in
            
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                print(weeklyAvailibility)
                self.schedules = weeklyAvailibility.selectedTimeSlots 
                self.refreshData()
            }
            
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        calendarDisposable.disposed(by: disposableBag)
    }
}
