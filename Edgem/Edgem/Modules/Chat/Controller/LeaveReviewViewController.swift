//
//  LeaveReviewViewController.swift
//  Edgem
//
//  Created by Namespace on 03/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import Cosmos
import RxSwift
import IQKeyboardManagerSwift

class LeaveReviewViewController: BaseViewController {
    
    // MARK: - Properties
    var userStatusArr = ["Extremely Dissatisfied", "Dissatisfied", "Neutral", "Satisfied", "Extremely Satisfied"]
    var sentences: [String] = []{
        didSet{
            userStatus.text = userStatusArr[(Int(self.starRatingView.rating)-1)]
            self.collectionView.reloadData()
        }
    }
    let notificationCenter = NotificationCenter.default
    var size: CGSize!
    var placeholderText = ""
    var oppositeUserID: Int?
    var offerID: Int?
    var disposableBag = DisposeBag()
    
    // MARK: - @IBOutlets
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var reviewCharacterCountLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userStatus: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var starContaineView: UIView!
    @IBOutlet weak var descriptionContainerView: UIView!
    @IBOutlet weak var starRatingView: CosmosView!
    @IBOutlet weak var reviewContainerView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.reviewContainerView.isHidden = true
        registerNib()
        initialSetUP()
        addNotifications()
        
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        submitBtn.layer.cornerRadius = 5
        submitBtn.clipsToBounds = true
        
        starContaineView.layer.borderWidth = 1
        starContaineView.layer.borderColor = staticMessageCellBorderColor.cgColor
        starContaineView.layer.cornerRadius = 5
        starContaineView.clipsToBounds = true
        
        descriptionContainerView.layer.borderWidth = 1
        descriptionContainerView.layer.borderColor = staticMessageCellBorderColor.cgColor
        
        reviewContainerView.layer.borderWidth = 1
        reviewContainerView.layer.borderColor = staticMessageCellBorderColor.cgColor
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeNotifications()
    }
    
    // MARK: - @IBActions
    
    @IBAction func didTapSubmitBtn(_ sender: UIButton) {
        if descriptionTextView.textColor == textViewPlaceHolderColor{
            self.showSimpleAlertWithMessage("Please leave a rating and feedback for the user.".localized)
            return
        }
        self.submitReview()
    }
    
    // MARK: - Helping Functions
    
    fileprivate func addNotifications() {
        
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
    }
    
    fileprivate func removeNotifications() {
        
        // [START this will enable IQKeyboardManager for other controllers]
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        // [END this will enable IQKeyboardManager for other controllers]
        
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        notificationCenter.removeObserver(self)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            //bottomConstraint.constant = 0
            self.reviewContainerView.isHidden = true
            self.bottomConstraint.constant = 0
        } else {
           // bottomConstraint.constant = keyboardViewEndFrame.height
              self.reviewContainerView.isHidden = false
              self.bottomConstraint.constant = 20
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (completed) in
            
//            if notification.name == UIResponder.keyboardWillChangeFrameNotification {
//                if let indexPath = self.getScrollPosition() {
//                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
//                }
//
//            }
        }
    }
    
    func getDataSourceForUserWithStarCount(userType user: String, starCount count: Int ) -> [String] {
        
        switch (user,count){
            
        case ("Tutor", 1),  ("Tutor", 2):
            sentences = ["Not punctual".localized, "Lack content knowledge".localized, "Poor communication".localized, "Unprepared for lessons".localized]
            return sentences
            
        case ("Tutor", 3):
            sentences = ["Can improve on punctuality".localized, "Can improve on content knowledge".localized, "Can improve on communication".localized, "Can improve on teaching technique".localized]
            return sentences
            
        case ("Tutor", 4),  ("Tutor", 5):
            sentences = ["Punctual".localized, "Knowledgeable".localized, "Effective in teaching".localized, "Patient".localized, "Great communication".localized, "Dedicated".localized]
            return sentences
            
        case ("Student", 1),  ("Tutor", 2):
            sentences = ["Unprepared for lessons".localized]
            return sentences
            
        case ("Student", 3),  ("Tutor", 4), ("Tutor", 5):
            sentences = ["Prepared for lessons".localized, "Positive attitude".localized, "Pays attention".localized, "Prepared for lessons".localized, "Fast learner".localized]
            return sentences
            
        case (_, _):
            return [""]
        }
    }
    
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        print(rating)
        print(getDataSourceForUserWithStarCount(userType: "Tutor", starCount: Int(rating)))
    }
    
    private func initialSetUP(){
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        descriptionTextView.delegate = self
        descriptionTextView.textContainerInset = UIEdgeInsets(top: 16, left: 13, bottom: 60, right: 13)
        
        if AppDelegateConstant.user?.userType == "Tutor"{
            placeholderText = "What went well/wrong? Your feedback will be helpful towards the student's development.".localized
        }else{
            placeholderText = "What went well/wrong? Your feedback will be much appreciated by your tutor".localized
        }
        descriptionTextView.text = placeholderText
        descriptionTextView.textColor = textViewPlaceHolderColor
        
        // This allow us to use collection view methos in this class
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // star rating view setup
        starRatingView.settings.fillMode = .full
        starRatingView.rating = 5.0
        let frameWidth = ((self.view.frame.width-60)-40*5)
        starRatingView.settings.starMargin = Double(frameWidth/5)
        starRatingView.didFinishTouchingCosmos = didFinishTouchingCosmos
        
        // review datasource according to user type
        if AppDelegateConstant.user?.userType == "Tutor"{
            sentences = ["Punctual".localized, "Knowledgeable".localized, "Effective in teaching".localized, "Patient".localized, "Great communication".localized, "Dedicated".localized]
        }else{
            sentences = ["Prepared for lessons".localized, "Positive attitude".localized, "Pays attention".localized, "Prepared for lessons".localized, "Fast learner".localized]
        }
        self.userStatus.text = "Extremely Satisfied"
    }
    
    func registerNib() {
        let nib = UINib(nibName: AppropriateReviewSentencesCollectionViewCell.nibName, bundle: nil)
        
        collectionView?.register(nib, forCellWithReuseIdentifier: AppropriateReviewSentencesCollectionViewCell.reuseIdentifier)
        
        //        if let flowLayout = self.collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
        ////            flowLayout.estimatedItemSize = CGSize(width: 100, height: 100)
        //            flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        //
        //        }
    }
}

// MARK: - Collection view data source
extension LeaveReviewViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sentences.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppropriateReviewSentencesCollectionViewCell.reuseIdentifier, for: indexPath) as? AppropriateReviewSentencesCollectionViewCell else{
            return UICollectionViewCell()
        }
        cell.configureCell(with: sentences[indexPath.row])
        return cell
    }
    
    
}

// MARK: - Collection view delegate
extension LeaveReviewViewController: UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (descriptionTextView.text.count) + (sentences[indexPath.row].count) >= 250{
            return
        }
        if descriptionTextView.textColor == textViewPlaceHolderColor{
            descriptionTextView.textColor = theamBlackColor
            descriptionTextView.text = sentences[indexPath.row]
        }else{
            descriptionTextView.textColor = theamBlackColor
            descriptionTextView.text = descriptionTextView.text  + " " + sentences[indexPath.row]
        }
        if descriptionTextView.text.count-1 >= 0{
            self.reviewCharacterCountLabel.text  = ("\(250-(descriptionTextView.text.count-1)) Words")
        }
        
    }
    
    /*
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     guard let cell: AppropriateReviewSentencesCollectionViewCell = Bundle.main.loadNibNamed(AppropriateReviewSentencesCollectionViewCell.nibName,
     owner: self,
     options: nil)?.first as? AppropriateReviewSentencesCollectionViewCell else {
     return CGSize.zero
     }
     cell.configureCell(with: sentences[indexPath.row])
     cell.prepareForReuse()
     cell.layoutIfNeeded()
     let size: CGSize = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
     return CGSize(width: sentences[indexPath.row].count * 10, height: 40)
     }
     
     */
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        guard let cell = collectionView.cellForItem(at: indexPath) as? AppropriateReviewSentencesCollectionViewCell else{return CGSize.zero}
        //        cell.configureCell(with: sentences[indexPath.row])
        //        cell.reviewLabel.sizeToFit()
        //        let size: CGSize = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingExpandedSize)
        let label = UILabel(frame: CGRect.zero)
        label.text = sentences[indexPath.item]
        label.font = UIFont(name: QuicksandMedium, size: 15)
        label.sizeToFit()
        return CGSize(width: label.frame.width + 50, height: 40)
        
    }
}

// MARK: - Text view delegates
extension LeaveReviewViewController: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text.count > 250{
            return
        }
        
        if textView.textColor == textViewPlaceHolderColor {
            textView.text = nil
            textView.textColor = theamBlackColor
        }
        if !textView.text.isEmpty {
            descriptionTextView.text = descriptionTextView.text + " "
        }
        self.reviewContainerView.isHidden = false
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            if AppDelegateConstant.user?.userType == "Tutor"{
                placeholderText = "What went well/wrong? Your feedback will be helpful towards the student's development.".localized
            }else{
                placeholderText = "What went well/wrong? Your feedback will be much appreciated by your tutor".localized
            }
            descriptionTextView.text = placeholderText
            textView.textColor = textViewPlaceHolderColor
        }
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        if (changedText.count-1) > 0{
            self.reviewCharacterCountLabel.text  = ("\(250-(changedText.count-1)) Words")
        }
        return changedText.count <= 250
        
    }
}

// MARK: --------- API Implementation

extension LeaveReviewViewController{
    
    // SUBMIT CONTACTUS DATA
    func submitReview(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
       
        
        guard let userID = UserStore.shared.userID else{return}
        
        //guard let oppositeUserID = self.oppositeUserID else{return}
        var oppositeUserID: Int?
        if let opp_usr_id = self.oppositeUserID{
            oppositeUserID = opp_usr_id
        }else if let opp_usr_id = AppVariables.chatListUserDetail?.offeredToID{
            oppositeUserID = opp_usr_id
        }else if let opp_usr_id = AppVariables.makeOfferRequest?.offeredToID{
             oppositeUserID = opp_usr_id
        }else{
            return
        }
        
       // guard let offerID = self.offerID else{return}
        
        var offerID: Int?
           if let offr_id = self.offerID{
               offerID = offr_id
           }else if let offr_id = AppVariables.chatListUserDetail?.offerID{
               offerID = offr_id
           }else if let offr_id = AppVariables.makeOfferRequest?.offerID{
                offerID = offr_id
           }else{
               return
           }
        
        let rating = String(self.starRatingView.rating)
        let review = self.descriptionTextView.text
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let params = ["user_id" : "\(userID)", "to_rate" : "\(oppositeUserID ?? 0)", "offer_id" : "\(offerID ?? 0)", "rating":rating, "comment" : review]
        
        let submitContactUsObserver = ApiManager.shared.apiService.submitReview(params as [String : AnyObject])
        let submitContactUsDisposable = submitContactUsObserver.subscribe(onNext: {(message) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                
                let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                    
                    self.navigationController?.popViewController(animated: true)
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        submitContactUsDisposable.disposed(by: disposableBag)
    }
    
}

