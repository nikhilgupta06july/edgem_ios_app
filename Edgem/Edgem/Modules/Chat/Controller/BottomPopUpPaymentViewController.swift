//
//  BottomPopUpPaymentViewController.swift
//  Edgem
//
//  Created by Namespace on 15/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift
import Stripe
import IQKeyboardManagerSwift

class BottomPopUpPaymentViewController : BaseViewController {
    
    // MARK: - Properties
     private var indexOfCellBeforeDragging = 0
     var disposableBag = DisposeBag()
     var makeOfferRequest: MakeOfferRequst?
    var chatViewController: ChatViewController?
    var msgID: String?
    var payBtnRow: Int?
    var payBtnSection: Int?
    var payBtnCell: ChatCustomButtomTableViewCell?
    var paymentCard: [StoreUserCard] = Array(){
        didSet{
            collectionView.reloadData()
        }
    }
    
    var activeTextField = UITextField()
    var totalCredit:Double = 0
    var ammoutToPay = ""
    var is_wallet_balance_used: Double = 0
    var is_Wallet_Credit_Greater_than_Amt = false
    var titlle  = ""{
        didSet{
            //self.bottomBtn.setTitle("Pay $" + titlle, for: .normal)
            self.bottomBtn.setTitle((is_Wallet_Credit_Greater_than_Amt == false ? "Pay $" + titlle : "Pay From Wallet"), for: .normal)
        }
    }
  
    // MARK: - @IBOutlets
    
    
    @IBOutlet weak var selectCreditBtn: UIButton!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var totalCreditLabel: UILabel!
    @IBOutlet weak var cretidTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lockImageView: UIImageView!
    @IBOutlet weak var selectCardLabelContainerView: UIView!
    @IBOutlet weak var saveCardLabelContainerView: UIView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottomBtnContainerView: UIView!
    @IBOutlet weak var bottomBtn: UIButton!
    @IBOutlet weak var dismissBtn: UIButton!
    
    // MARK: - ViewLifeCycles
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetUp()
        collectionViewFlowLayout.minimumLineSpacing = 0
        
        if let totalCredits = AppDelegateConstant.user?.totalCredits, let totalCredits_int = Double(totalCredits), totalCredits_int > 0{
            selectCreditBtn.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            totalCreditLabel.text = "Total Credits: \(totalCredits.changeStringNumberToWholeNumber())"
            selectCreditBtn.isUserInteractionEnabled = true
            totalCredit = totalCredits_int
        }else{
            totalCreditLabel.text = "Total Credits: 0"
            selectCreditBtn.setImage(#imageLiteral(resourceName: "checkboxInactive"), for: .normal)
            selectCreditBtn.isUserInteractionEnabled = false
            totalCredit = 0
        }
        
        if let paymentCards = AppDelegateConstant.user?.paymentCard{
            self.paymentCard = paymentCards
        }
        
        if let makeOfferRequstObject = self.makeOfferRequest,let ammount = makeOfferRequstObject.totalAmmount {
            
            if totalCredit > 0 {
//                if let amm = Double(ammount){
//                    ammoutToPay = "\(amm - (totalCredit/100))"
//                }
                
                guard let amm = Double(ammount) else{ return }
                
                if amm > (totalCredit/100){
                    
                     is_wallet_balance_used = (totalCredit)
                     ammoutToPay = "\(amm - (totalCredit/100))"
                    
                }else if amm <= (totalCredit/100){
                    self.is_Wallet_Credit_Greater_than_Amt = true
                    ammoutToPay = "0"
                    is_wallet_balance_used = (amm*100)
                    
                }
               
            }else{
                ammoutToPay = ammount
                is_wallet_balance_used = 0
            }
             titlle =  ammoutToPay.changeStringNumberToWholeNumber()
        }else{
            guard let offerNAmmount = UserStore.shared.offerIDNTotalAmmount else{
                print("offer ID or ammount not found")
                return
            }
            
            let totalAmmount = offerNAmmount["total_ammount"] as! String
            
            if totalCredit > 0 {
                //                if let amm = Double(ammount){
                //                    ammoutToPay = "\(amm - (totalCredit/100))"
                //                }
                
                guard let amm = Double(totalAmmount) else{ return }
                
                if amm > (totalCredit/100){
                    
                    is_wallet_balance_used = (totalCredit)
                    ammoutToPay = "\(amm - (totalCredit/100))"
                    
                }else if amm <= (totalCredit/100){
                    self.is_Wallet_Credit_Greater_than_Amt = true
                    ammoutToPay = "0"
                    is_wallet_balance_used = (amm*100)
                    
                }
                
            }else{
                    ammoutToPay = totalAmmount
                    is_wallet_balance_used = 0
            }
             titlle =  ammoutToPay.changeStringNumberToWholeNumber()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        sepratorView.backgroundColor = theamBorderGrayColor
        
        bottomBtnContainerView.layer.cornerRadius = 4
        bottomBtnContainerView.layer.shadowColor = UIColor.black.cgColor
        bottomBtnContainerView.layer.shadowOpacity = 0.12
        bottomBtnContainerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        bottomBtnContainerView.layer.shadowRadius = 4
        
        bottomBtn.layer.cornerRadius = 4
        bottomBtn.clipsToBounds = true
        
        configureCollectionViewLayoutItemSize()
        
        if paymentCard.count == 0 {
            collectionViewHeight.constant = 0
            descriptionLabelTopConstraint.constant = 8
            lockImageView.isHidden = true
            selectCardLabelContainerView.isHidden = true
            saveCardLabelContainerView.isHidden = true
            descriptionLabel.text = "No card added. Please add card(s).".localized
            descriptionLabel.textAlignment = .center
            dismissBtn.isHidden = true
            //descriptionLabel.font = UIFont(name: QuicksandMedium, size: 18)
            descriptionLabel.adjustsFontSizeToFitWidth = true
           // descriptionLabel.fon
            bottomBtn.setTitle("Add Card", for: .normal)
        }else{
            descriptionLabelTopConstraint.constant = -4
            dismissBtn.isHidden = false
            descriptionLabel.textAlignment = .left
            descriptionLabel.text = "Your payment details are secured via 128 Bit encryption by Verisign"
            descriptionLabel.font = UIFont(name: QuicksandRegular, size: 13)
            collectionViewHeight.constant = 240
            lockImageView.isHidden = false
            selectCardLabelContainerView.isHidden = false
            saveCardLabelContainerView.isHidden = false
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       fetchCardsDetailFromStripe()// self.fetchCardsDetail() // ---> this api will be called just for updation
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    // MARK: - @IBActions
    
    
    @IBAction func didSelectCreditBtn(_ sender: UIButton) {
        
        if sender.imageView?.image == #imageLiteral(resourceName: "selected") {
            
            sender.setImage(#imageLiteral(resourceName: "checkboxInactive"), for: .normal)
            is_Wallet_Credit_Greater_than_Amt = false
            is_wallet_balance_used = 0
            
            if let makeOfferRequstObject = self.makeOfferRequest,let ammount = makeOfferRequstObject.totalAmmount {
                
                titlle =  ammount.changeStringNumberToWholeNumber()
               
            }else{
                guard let offerNAmmount = UserStore.shared.offerIDNTotalAmmount else{
                    print("offer ID or ammount not found")
                    return
                }
                
                let totalAmmount = offerNAmmount["total_ammount"] as! String
                titlle =  totalAmmount.changeStringNumberToWholeNumber()
              
            }
           
        }else{
            sender.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
             is_wallet_balance_used = 1
            
            if let makeOfferRequstObject = self.makeOfferRequest,let ammount = makeOfferRequstObject.totalAmmount {
                
                if totalCredit > 0 {

                    guard let amm = Double(ammount) else{ return }
                    
                    if amm > (totalCredit/100){
                        
                        is_wallet_balance_used                           = (totalCredit)
                        ammoutToPay                                          = "\(amm - (totalCredit/100))"
                        
                    }else if amm <= (totalCredit/100){
                        self.is_Wallet_Credit_Greater_than_Amt = true
                        ammoutToPay                                          = "0"
                        is_wallet_balance_used                           = (amm*100)
                    }
                    
                }else{
                    ammoutToPay = ammount
                }
                titlle =  ammoutToPay.changeStringNumberToWholeNumber()
            
            }else{
                guard let offerNAmmount = UserStore.shared.offerIDNTotalAmmount else{
                    print("offer ID or ammount not found")
                    return
                }
                
                let totalAmmount = offerNAmmount["total_ammount"] as! String
                if totalCredit > 0 {
//                    if let amm = Double(totalAmmount){
//                        ammoutToPay = "\(amm - (totalCredit/100))"
//                    }
                    
                    guard let amm = Double(totalAmmount) else{ return }
                    
                    if amm > (totalCredit/100){
                        
                        is_wallet_balance_used = (totalCredit)
                        ammoutToPay = "\(amm - (totalCredit/100))"
                        
                    }else if amm <= (totalCredit/100){
                        self.is_Wallet_Credit_Greater_than_Amt = true
                        ammoutToPay = "0"
                        is_wallet_balance_used = (amm*100)
                        
                    }
                }else{
                    ammoutToPay = totalAmmount
                }
                titlle = ammoutToPay.changeStringNumberToWholeNumber()
              
            }
        }
    }
    
    @IBAction func didTapBackgroundBorder(_ sender: UIButton) {
        self.dismissPopUp()
    }
    
    @IBAction func didTapSelectCardBtn(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.collectionView)
        let indexPath = self.collectionView.indexPathForItem(at: buttonPosition)//indexPathForRow(at:buttonPosition)
        let cell = self.collectionView.cellForItem(at: indexPath!) as! BottomPaymentCardListCollectionViewCell
        
        if sender.isSelected == true{
            sender.isSelected = false
            sender.setImage(#imageLiteral(resourceName: "genderDeselcetd"), for: .normal)
            
            if let indexPath = collectionView.indexPath(for: cell), let cardObject = paymentCard[safe: indexPath.item] {
                cardObject.isPrimary = 0
                cell.cvvtextField.text = ""
            }
            
        }else{
            sender.isSelected = true
            sender.setImage(#imageLiteral(resourceName: "genderSelected"), for: .normal)

            if let indexPath = collectionView.indexPath(for: cell), let cardObject = paymentCard[safe: indexPath.item] {
                cardObject.isPrimary = 1
            }
            
        }
     
    }
    
    @IBAction func didTapPayButton(_ sender: UIButton) {
        self.view.endEditing(true)
        if sender.titleLabel?.text == "Add Card".localized{
            self.dismiss(animated: true) {
                self.chatViewController?.sendToAddCards()
            }
        }else{
            if activeTextField.text?.isEmpty == true && is_Wallet_Credit_Greater_than_Amt == false{
                self.showSimpleAlertWithMessage("Please Enter CVV".localized)
                return
            }else{
                 doPayment()
            }
           
        }
        
    }
    
    
    // MARK: - Helping Functions
    
    /// This function will get card details from stripe
    fileprivate func fetchCardsDetailFromStripe(){
        
        guard let cus_id = AppDelegateConstant.user?.btCustomerID else{return}
        
        // initialige customer id in StripeUtils which is needed to get card list
        //self.stripeUtils.customerID = cus_id
        StripeUtils.shared.customerID = cus_id
        
        StripeUtils.shared.getCardsList { (result) in
            // check card array available or not
            if let result = result{
                // check user have any card or not
                if result.count > 0{
                    
                    #if DEBUG
                        var data = [String:Any]()
                        data = ["data" : result]
                        PrintJSONOnConsole.printJSON(dict: (data as AnyObject) as! [String : AnyObject])
                    #endif
                    
                    // temp StoreUserCard variable
                    var storeUserCardObject =  [StoreUserCard]()
                    
                    result.forEach({ (cardData) in
                        storeUserCardObject.append(StoreUserCard(data: cardData))
                    })
                    
                    DispatchQueue.main.async {
                        if (self.paymentCard != storeUserCardObject) {
                            self.paymentCard = storeUserCardObject
                            AppDelegateConstant.user?.paymentCard = storeUserCardObject
                            self.collectionView.reloadData()
                            
                        }
                    }
                }else{
                    print(value: "user does not have any card")
                }
                
            }else{
                print(value: "Card arr not available")
            }
        }
    }
    
    private func initialSetUp(){
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    private func calculateSectionInset() -> CGFloat {
        return 30
    }
    
    private func configureCollectionViewLayoutItemSize() {
        let inset: CGFloat = calculateSectionInset() // This inset calculation is some magic so the next and the previous cells will peek from the sides. Don't worry about it
        collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        
        collectionViewFlowLayout.itemSize = CGSize(width: collectionViewFlowLayout.collectionView!.frame.size.width - inset * 2, height: collectionViewFlowLayout.collectionView!.frame.size.height-24)
        collectionViewFlowLayout.collectionView!.reloadData()
    }
    
    fileprivate func indexOfMajorCell() -> Int {
        let itemWidth = collectionViewFlowLayout.itemSize.width
        let proportionalOffset = collectionViewFlowLayout.collectionView!.contentOffset.x / itemWidth
        let index = Int(round(proportionalOffset))
        let safeIndex = max(0, min(paymentCard.count - 1, index))
        return safeIndex
    }
    
}


// MARK: --------- Delegates

extension BottomPopUpPaymentViewController: AddNewCardViewControllerDelegate {
    
    func getCardDetail(_ cardDetail: StoreUserCard) {
        
        AppDelegateConstant.user?.paymentCard?.append(cardDetail)
        if let userCards = AppDelegateConstant.user?.paymentCard{
            paymentCard = userCards
        }
        
    }
    
}

    // MARK: - collection Delegates/Datasources

extension BottomPopUpPaymentViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paymentCard.count > 0 ? paymentCard.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BottomPaymentCardListCollectionViewCell.cellIdentifire(), for: indexPath) as!  BottomPaymentCardListCollectionViewCell
        cell.configureCell(with: paymentCard[indexPath.item], indexPath.item)
        cell.cvvtextField.delegate = self
     
        return cell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        indexOfCellBeforeDragging = indexOfMajorCell()
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        // Stop scrollView sliding:
        targetContentOffset.pointee = scrollView.contentOffset
        
        // calculate where scrollView should snap to:
        let indexOfMajorCell = self.indexOfMajorCell()
        
        // calculate conditions:
        let swipeVelocityThreshold: CGFloat = 0.5 // after some trail and error
        let hasEnoughVelocityToSlideToTheNextCell = indexOfCellBeforeDragging + 1 < paymentCard.count && velocity.x > swipeVelocityThreshold
        let hasEnoughVelocityToSlideToThePreviousCell = indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
        let majorCellIsTheCellBeforeDragging = indexOfMajorCell == indexOfCellBeforeDragging
        let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)
        
        if didUseSwipeToSkipCell {
            
            let snapToIndex = indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
            let toValue = collectionViewFlowLayout.itemSize.width * CGFloat(snapToIndex)
            
            // Damping equal 1 => no oscillations => decay animation:
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: velocity.x, options: .allowUserInteraction, animations: {
                scrollView.contentOffset = CGPoint(x: toValue, y: 0)
                scrollView.layoutIfNeeded()
            }, completion: nil)
            
        } else {
            // This is a much better way to scroll to a cell:
            let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
            collectionViewFlowLayout.collectionView!.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    
}


    // MARK: - TextField Methods
extension BottomPopUpPaymentViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let cardObject = paymentCard[safe: textField.tag] {
            if cardObject.isPrimary == 0 {
                 self.showSimpleAlertWithMessage("Please select card.")
                 textField.resignFirstResponder()
            }else{
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField.text?.isEmpty == false{
             activeTextField = textField
        }
       
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        
        guard let stringRange = Range(range, in: currentText) else{return false}
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updatedText.count <= 4

    }
    
    
}

    // MARK: - API Implementations

extension BottomPopUpPaymentViewController {
    
    func getCards(_ storeUserCardObject: StoreUserCard) {
    
        guard let cus_id = AppDelegateConstant.user?.btCustomerID else{return}
        guard let card_id = storeUserCardObject.cardID else{return}
        
        var totalAmmount = ""
        var offerID = ""
        
        if let offer = self.makeOfferRequest, let ammount = offer.totalAmmount, let offer_id = offer.offerID{
            totalAmmount = self.titlle//ammount
            offerID = "\(offer_id)"
        }else{
            
            guard let offerNAmmount = UserStore.shared.offerIDNTotalAmmount else{
                print("offer ID or ammount not found")
                return
            }
            
            totalAmmount = self.titlle//offerNAmmount["total_ammount"] as! String//"\(String(describing: offerNAmmount["total_ammount"]))"
            guard let offer_id = offerNAmmount["offer_id"] as? Int else{
                print("Offer id not found")
                return}
            offerID = "\(offer_id)"
        }
        
        guard let msg_ID = self.msgID else{return}
        
        let params: [String : Any] = ["amount" : totalAmmount,
                                                    "bt_customer_id" : cus_id,
                                                    "offer_id" : "\(offerID)",
                                                    "card_token": card_id,
                                                    "msg_id": msg_ID,
                                                    "is_wallet_balance_used": "\(is_wallet_balance_used)"]
        
        self.proceedPayment(params)
        
       //PrintJSONOnConsole.printJSON(dict: params as [String : AnyObject])
        
    }
    
    func doPayment() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let index = indexOfMajorCell()
        if let cardDetailObject = paymentCard[safe: index]{
             getCards(cardDetailObject)
        }
        
    }

    
    func proceedPayment(_ params: [String : Any]){

        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let addPaymentCardsObserver = ApiManager.shared.apiService.proseedPayment(params as [String : AnyObject])
        let addPaymentCardsDisposable = addPaymentCardsObserver.subscribe(onNext: {(makeOfferRequest) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                if self.is_wallet_balance_used > 0{
                    //AppDelegateConstant.user?.totalCredits = "\(0)"
                    if let totalCredits = AppDelegateConstant.user?.totalCredits, let totalCredits_int = Double(totalCredits){
                        AppDelegateConstant.user?.totalCredits = "\(totalCredits_int-self.is_wallet_balance_used)".changeStringNumberToWholeNumber()
                    }
                }
                self.chatViewController?.makeOfferDataFromBottomPopUP(makeOfferRequest: makeOfferRequest, payBtnRow: self.payBtnRow!, payBtnSection: self.payBtnSection!)
                self.dismissPopUp()
                if let payBtn = self.payBtnCell{
                    payBtn.enableButton = false
                }
            }
        },onError: {(error) in
            DispatchQueue.main.async(execute: {
                if let payBtn = self.payBtnCell{
                   payBtn.enableButton = true
                }
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        }
        )
        addPaymentCardsDisposable.disposed(by: disposableBag)
    }
    
    func fetchCardsDetail(){

        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async{
            //Time consuming task here
            let userObserver = ApiManager.shared.apiService.fetchUserPaymentCardDetails()
            let userDisposable = userObserver.subscribe(onNext: {(cards) in
                DispatchQueue.main.async {
                    
                    if (self.paymentCard != cards) {
                        self.paymentCard = cards
                        AppDelegateConstant.user?.paymentCard = cards
                        self.collectionView.reloadData()
                        
                    }
                }
            }, onError: {(error) in
                DispatchQueue.main.async(execute: {
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                    // self.fetchDashboard()
                })
            })
            userDisposable.disposed(by: self.disposableBag)
        }

    }
    
}
