//
//  EdgemOfferViewController.swift
//  Edgem
//
//  Created by Namespace on 16/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift


class EdgemOfferViewController: BaseViewController {
    
    // MARK: ----------- Properties
    
     var disposableBag = DisposeBag()
    
    var makeOfferRequst =  MakeOfferRequst()
    var userData: User?
    
    var isFromViewController: UIViewController?
    
    var gradient: CAGradientLayer!
    
    var activeTextField: UITextField!
    
    let hoursPickerView = UIPickerView()
    let hoursOption = ["1 hour".localized, "1.5 hours".localized, "2 hours".localized]
    
    let lessonsPickerView = UIPickerView()
    let lessonsOptions = [ "4 lessons".localized, "6 lessons".localized, "8 lessons".localized]
    
    let subjectsPicker = UIPickerView()
    var subjectOptions: [(String,String,String,String,String,String)]? = Array()
    
    let levelPicker = UIPickerView()
    var levelOptions: [Level] = Array()
    
    var Subjects:[(String,String)] = Array()
    
     var commonSubjectOfStudentAndTutor: [Subject]?
    
    var lessons: Double = 0.0
    
    var lockRateStatus = ""
    var suggestedOffer = ""
    
    var tutorId: String?
    
    var isCounterOrEditOffer = ""
    
    var shouldUpdateOffer: Bool = false
    
    var totalAmmount: Double {
        get {
                        var ammount_offer: Double = 0.0
                        var ammount_hour: Double = 0.0
                        var ammount_lesson: Double = 0.0
            
                        if let offerValueInString = makeOfferRequst.ammountPerHour, let offerValueInDouble = Double((offerValueInString).filter{ !" \n\t\r".contains($0) }){
                            ammount_offer = offerValueInDouble
                        }
            
                        if let hours = makeOfferRequst.hours{
                            ammount_hour = Double(hoursInDouble(hours))!
                        }
            
                        if let lessonsValueInString = (makeOfferRequst.lessons)?.replacingOccurrences(of: "lessons".localized, with: ""), let lessonsValueInDouble = Double((lessonsValueInString).filter{ !" \n\t\r".contains($0) }){
                            ammount_lesson = lessonsValueInDouble
                            lessons =  lessonsValueInDouble
                        }
            
                        return ammount_offer * ammount_hour * ammount_lesson
        }
        set {
            
                        let totalAmtIndexPath = IndexPath(row: 5, section: 0)
            
                        guard let totalAmtCell = tableView.cellForRow(at: totalAmtIndexPath) as? OfferTotalAmmountTableViewCell else{
                            print(value: "cell did not initialiged")
                            return
                        }
            
                        if makeOfferRequst.lockRate == "1" {
                            
                            let ammountWithOutDiscount = newValue
                            var ammountWithDiscount:Double  = 0
                            var discountedAmt:Double = 0
                            
                            if lessonsInDouble(makeOfferRequst.lessons!) == "4"{
                                
                                totalAmtCell.approvedAmtLabel.text = "$\(newValue.isWholeNumber())"
                                makeOfferRequst.discountedPrice = "\(0)"
                                makeOfferRequst.totalAmmount = "\(newValue)"
                                totalAmtCell.declinedAmtLabel.isHidden = true
                                totalAmtCell.statusLabel.isHidden = true
                                totalAmtCell.ammountCut_View.isHidden = true
                            }else if lessonsInDouble(makeOfferRequst.lessons!) == "6"{
                                ammountWithDiscount = ammountWithOutDiscount - ((ammountWithOutDiscount*3)/100)
                                discountedAmt = ((ammountWithOutDiscount*3)/100)
                                
                                let  amt_with_disc = ammountWithDiscount.isWholeNumber()
                                let amt_without_disc = ammountWithOutDiscount.isWholeNumber()
                                
                                totalAmtCell.approvedAmtLabel.text = "$\(amt_with_disc)"
                                makeOfferRequst.totalAmmount = "\(ammountWithDiscount)"
                                
                                totalAmtCell.declinedAmtLabel.text = "$\(amt_without_disc)"
                                
                                totalAmtCell.statusLabel.isHidden = false
                                totalAmtCell.ammountCut_View.isHidden = false
                                
                                let discount_amt_str = (AppDelegateConstant.user?.userType == tutorType) ? "" : "Nicely done, you save $\(discountedAmt.isWholeNumber())".localized
                                totalAmtCell.statusLabel.text = discount_amt_str
                                makeOfferRequst.discountedPrice = discountedAmt.isWholeNumber()
                                totalAmtCell.declinedAmtLabel.isHidden = false
                                
                            }else if lessonsInDouble(makeOfferRequst.lessons!) == "8"{
                                ammountWithDiscount = ammountWithOutDiscount - ((ammountWithOutDiscount*7)/100)
                                discountedAmt = ((ammountWithOutDiscount*7)/100)
                                totalAmtCell.ammountCut_View.isHidden = false
                                totalAmtCell.statusLabel.isHidden = false
                                
                                let discount_amt_str = (AppDelegateConstant.user?.userType == tutorType) ? "" : "Nicely done, you save $\(discountedAmt.isWholeNumber())".localized
                                totalAmtCell.statusLabel.text = discount_amt_str
                                makeOfferRequst.discountedPrice = discountedAmt.isWholeNumber()
                                totalAmtCell.approvedAmtLabel.text = "$\(ammountWithDiscount.isWholeNumber())"
                                makeOfferRequst.totalAmmount = "\(ammountWithDiscount)"
                                totalAmtCell.declinedAmtLabel.text = "$\(ammountWithOutDiscount.isWholeNumber())"
                                totalAmtCell.declinedAmtLabel.isHidden = false
                                
                            }
                        }else{

                            totalAmtCell.approvedAmtLabel.text = "$\(newValue.isWholeNumber())"
                            makeOfferRequst.discountedPrice = "\(0)"
                            makeOfferRequst.totalAmmount = "\(newValue)"
                            totalAmtCell.declinedAmtLabel.isHidden = true
                            totalAmtCell.ammountCut_View.isHidden = true
                            totalAmtCell.statusLabel.isHidden = true

                        }
        }
    }
    
    var vcNavigationStatus: Int?
    
    // MARK: ----------- @IBOutlets
    
    @IBOutlet weak var vcTtitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var backBtnContainerView: UIView!
    @IBOutlet weak var confirmBtnContainerView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton!
    
    // MARK: ----------- View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if let commonSubject = commonSubjectOfStudentAndTutor{
            
            for subject in commonSubject{
                
                var levels = [Level]()
                
                for level in subject.levels{
                    
                    if level.price! != "0.0"{
                        
                        levels.append(level)
                        
                    }
                    
                }
                subject.levels = levels
            }
            self.subjectOptions = commonSubject.map{return ($0.name,$0.levels[safe: 0]?.price ?? "", $0.lockRate ?? "", $0.id, $0.levels[0].name, $0.levels[0].id)}
            
        }
        
        if let _userData = self.userData{
            if let tID = _userData.ID{
                self.tutorId = "\(String(describing: tID))"
            }
        }
        
        initialSetUP()
        
        if isFromViewController is ChatViewController{
           
        }else{
            makeOfferRequst.hours                     = "1 hour".localized
            makeOfferRequst.subjects                 = "Select Subject".localized
            makeOfferRequst.lessons                  = "4 lessons".localized
            makeOfferRequst.level                      = "Select Level".localized
            makeOfferRequst.totalAmmount      = "0"
        }
        
        //setGradient()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //vcTtitleLabel.text = isCounterOrEditOffer == "counter" ? "Counter Offer".localized : "Make Offer".localized
        viewControllerTitle()
        if isFromViewController is ChatViewController{
             getTutorSubjects()
        }
    }
    
    override func viewDidLayoutSubviews() {
         //gradient.frame = tableView.superview?.bounds ?? .null
        
        // BottomHolderView
        bottomContainerView.layer.borderWidth = 0.8
        bottomContainerView.layer.borderColor = UIColor(hue: 23/255, saturation: 23/255, brightness: 23/255, alpha: 0.3).cgColor
        
        //Add Account
        backBtn.layer.cornerRadius = 5
        backBtn.layer.borderWidth = 1
        backBtn.layer.borderColor = theamGrayColor.cgColor
        backBtn.clipsToBounds = true
        backBtn.setTitle("Back".localized, for: .normal)
        backBtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        backBtn.titleLabel?.textColor = theamBlackColor
        
        // addAccountBtncontainerView
        backBtnContainerView.layer.shadowColor = UIColor.black.cgColor
        backBtnContainerView.layer.shadowOpacity = 0.12
        backBtnContainerView.layer.shadowRadius = 4
        backBtnContainerView.layer.shadowOffset = .zero
   
        // confirm button attributes
        confirmBtn.layer.cornerRadius = 5
        confirmBtn.backgroundColor = buttonBackGroundColor
        confirmBtn.clipsToBounds = true
        
        confirmBtn.setTitle("Confirm".localized, for: .normal)
        confirmBtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        confirmBtn.titleLabel?.textColor = theamBlackColor
        
//        confirmBtn.layoutIfNeeded()
        
        
        // apply button shadow
        confirmBtnContainerView.layer.shadowColor = UIColor.black.cgColor
        confirmBtnContainerView.layer.shadowOpacity = 0.12
        confirmBtnContainerView.layer.shadowRadius = 4
        confirmBtnContainerView.layer.shadowOffset = .zero
        confirmBtnContainerView.layoutIfNeeded()
        confirmBtnContainerView.layer.shadowPath = UIBezierPath(rect: confirmBtnContainerView.bounds).cgPath
        //confirmBtnContainerView.layer.shouldRasterize = true
    }
    
    
    // MARK: ----------- @IBActions
    
    @IBAction func didTappedBackOrConfirmBtn(_ sender: UIButton) {
        if sender.tag == 1 {
            self.navigationController?.popViewController(animated: true)
        }else{
           
            let validationInformation = isDataValid()
            
            guard validationInformation.isValid else {
                self.showSimpleAlertWithMessage(validationInformation.message)
                return
            }
            
            let bank_card_status = AppVariables.isCreditCardBankDetail(self)
            if bank_card_status == false{
                return
            }
//            if self.shouldUpdateOffer == true{
//
//            }else{
//                makeOffer()
//            }
            
            if let _ = self.makeOfferRequst.offerID{
               
                if vcTtitleLabel.text == "Counter Offer".localized{
                    counterOffer()
                }else{
                      updateOffer()
                }
            }else{
                 makeOffer()
            }
           

        }
    }
    
    // MARK: ----------- Helping Functions
    
    fileprivate func viewControllerTitle() {
        if isCounterOrEditOffer == "counter"{
            vcTtitleLabel.text = "Counter Offer"
        }else if isCounterOrEditOffer == "edit"{
            vcTtitleLabel.text = "Edit Offer"
        }else{
            vcTtitleLabel.text = "Make Offer"
        }
    }
    
    fileprivate func initialSetUP(){
        
        tableView.delegate = self
        tableView.dataSource = self
        
        hoursPickerView.delegate = self
        hoursPickerView.dataSource = self
        hoursPickerView.tag = 1
        
        lessonsPickerView.delegate = self
        lessonsPickerView.dataSource = self
        lessonsPickerView.tag = 3
        
        subjectsPicker.delegate = self
        subjectsPicker.dataSource = self
        subjectsPicker.tag = 2
        
        levelPicker.delegate = self
        levelPicker.dataSource = self
        levelPicker.tag = 4
    }
    
    func setGradient(){
        gradient = CAGradientLayer()
        gradient.frame = (tableView.superview?.frame) ?? .null
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0.0, 0.05, 0.98, 1.0]
        tableView.backgroundColor = .clear
        tableView.superview?.layer.mask = gradient
    }
    

    fileprivate func getPreviouslyMadeOfferRequst(_ offer: MakeOfferRequst, _ updateStatus: Bool) {
        
        var _offer = offer
       
        if let hours = _offer.hours, let lessons = _offer.lessons{
           
            let hrs = hours == "1" ? (hours + " " + "hour".localized) : (hours + " " + "hours".localized)
            let less = lessons + " " + "lessons"
            _offer.hours = hrs
            _offer.lessons = less
        }
        
        self.makeOfferRequst = _offer
        self.shouldUpdateOffer = updateStatus
        self.tableView.reloadData()
        
    }
    
    private func hoursInDouble(_ time: String?) -> String{
        if time == "1 hour".localized{
            return "1"
        }else if time == "1.5 hours".localized{
            return "1.5"
        }else if time == "2 hours".localized {
             return "2"
        }else if time == "1".localized{
             return "1"
        }else if time == "1.5"{
             return "1.5"
        }else if time == "2"{
             return "2"
        }
        return "1"
    }
    
    private func lessonsInDouble(_ lesson: String?) -> String{
        
        switch lesson {
        case "4 lessons".localized : return "4"
            
        case "6 lessons".localized : return "6"
            
        case "8 lessons".localized : return "8"
            
        case "4" : return "4"
            
        case "6" : return "6"
            
        case "8" : return "8"
            
        default :  return ""
            
        }
        
//        if lesson == "4 lessons".localized{
//            return "4"
//        }else if lesson == "6 lessons".localized{
//            return "6"
//        }else if lesson == "8 lessons".localized {
//            return "8"
//        }
//        return ""
    }
    
    func configureTextField(_ textField: UITextField) {
        textField.delegate = self
        textField.inputView = nil
        
        switch textField.tag {
        case EdgemOfferTextField.perHour.rawValue :
                                                                                    textField.keyboardType = UIKeyboardType.decimalPad
                                                                                    break
        case EdgemOfferTextField.forHours.rawValue:
            
                                                                                    textField.inputView = hoursPickerView
                                                                                    break
            
        case EdgemOfferTextField.forSubjects.rawValue :
                                                                                    if isFromViewController is ChatViewController{
                                                                                    /// do nothing. We are not giving any option here to pick anather subject because if user changed the subject then entire offer will be changed. better to cancle offer than counter.
                                                                                        textField.isUserInteractionEnabled = false
                                                                                        //textField.textColor = theamBorderGrayColor
                                                                                        return
                                                                                    }
                                                                                    textField.inputView = subjectsPicker
                                                                                    break
            
        case EdgemOfferTextField.selectLevel.rawValue :
                                                                                    if isFromViewController is ChatViewController{
                                                                                        /// do nothing. We are not giving any option here to pick anather subject because if user changed the subject then entire offer will be changed. better to cancle offer than counter.
                                                                                        textField.isUserInteractionEnabled = false
                                                                                        //textField.textColor = theamBorderGrayColor
                                                                                        return
                                                                                    }
                                                                                    if textField.text == "Select Level".localized || textField.text == "".localized{
                                                                                        textField.isUserInteractionEnabled = false
                                                                                    }else{
                                                                                        textField.isUserInteractionEnabled = true
                                                                                        textField.inputView = levelPicker
                                                                                        break
                                                                                    }
            
        case EdgemOfferTextField.forLessons.rawValue :
            
                                                                                    textField.inputView = lessonsPickerView
                                                                                    break

        default:
            break
        }
        
    }
    
    func updateDataInTextField(_ textField: UITextField, fromCell: Bool){
        
        switch textField.tag{
            
        case EdgemOfferTextField.perHour.rawValue :
            fromCell ? (textField.text = makeOfferRequst.ammountPerHour?.changeStringNumberToWholeNumber()) : (makeOfferRequst.ammountPerHour = textField.text!)
            
        case EdgemOfferTextField.forHours.rawValue :
           //fromCell ? (textField.text = makeOfferRequst.hours) : (makeOfferRequst.hours = textField.text!)
           if fromCell{
            if let hours = makeOfferRequst.hours{
//                if !hours.contains("hours".localized) && !hours.contains("hour".localized){
//                    textField.text = hours + " hours"
//                }else{
//                    textField.text = hours
//                }
                if  hours == "1.0"{
                     textField.text =  "1 hour"
                }else if hours == "1.5"{
                    textField.text = "1.5 hours"
                }else if hours == "2.0"{
                    textField.text = "2 hours"
                }else if hours.contains("1".localized) && !hours.contains("hour".localized){
                     textField.text = hours + " hour"
                }else if hours.contains("1".localized) && hours.contains("hour".localized){
                    textField.text = hours
                }else if !hours.contains("1".localized) && !hours.contains("hour".localized){
                     textField.text = hours + " hours"
                }else{
                     textField.text = hours
                }
            }
           }else{
            (makeOfferRequst.hours = textField.text!)
            }
            
        case EdgemOfferTextField.forSubjects.rawValue :
            fromCell ? (textField.text = makeOfferRequst.subjects) : (makeOfferRequst.subjects = textField.text!)
            
        case EdgemOfferTextField.selectLevel.rawValue :
            fromCell ? (textField.text = makeOfferRequst.level) : (makeOfferRequst.level = textField.text!)
            
        case EdgemOfferTextField.forLessons.rawValue :
            //fromCell ? (textField.text = makeOfferRequst.lessons) : (makeOfferRequst.lessons = textField.text!)
            if fromCell{
                if let lessons = makeOfferRequst.lessons{
                    if !lessons.contains("lessons"){
                        textField.text = lessons + " lessons"
                    }else{
                        textField.text = lessons
                    }
                }
            }else{
                (makeOfferRequst.lessons = textField.text!)
            }
        default:
            break
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func isDataValid() -> (isValid: Bool, message: String) {
        
        var errorMessages: [String] = []
        
        for index in 0...4{
            let currentIndexPath = IndexPath(row: index, section: 0)
            switch index {
            case EdgemOfferTextField.perHour.rawValue :
                if let cell = tableView.cellForRow(at: currentIndexPath) as? OfferTableViewCell{
                    cell.validateOffer(makeOfferRequst.ammountPerHour?.replacingOccurrences(of: " ", with: "") ?? "")
                    errorMessages.append(cell.errorLabel.text!)
                }
            
            case EdgemOfferTextField.forHours.rawValue :
//                if let cell = tableView.cellForRow(at: currentIndexPath) as? OfferForOptionsTableViewCell{
//                    cell.va(makeOfferRequst.ammountPerHour!)
//                    errorMessages.append(cell.error!)
//                }
                break
                
            case EdgemOfferTextField.forSubjects.rawValue :
                if let cell = tableView.cellForRow(at: currentIndexPath) as? OfferForOptionsTableViewCell{
                    cell.validateSubjects(makeOfferRequst.subjects!)
                    errorMessages.append(cell.errorLabel.text!)
                }
                
            case EdgemOfferTextField.selectLevel.rawValue :
                if let cell = tableView.cellForRow(at: currentIndexPath) as? OfferForOptionsTableViewCell{
                    cell.validateLevel(makeOfferRequst.level!)
                    errorMessages.append(cell.errorLabel.text!)
                }
                
            case EdgemOfferTextField.forLessons.rawValue :
//                if let cell = tableView.cellForRow(at: currentIndexPath) as? OfferForOptionsTableViewCell{
//                    cell.validateOffer(makeOfferRequst.ammountPerHour!)
//                    errorMessages.append(cell.error!)
//                }
                  break
            default:
                break
            }
        }
        
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.reloadData()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }
    
    func countToatalValue(){
        totalAmmount = totalAmmount * 1
    }
    
}

    // MARK: ----------- TableView Methods

extension EdgemOfferViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row{
        case EdgemOfferTextField.perHour.rawValue :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: OfferTableViewCell.cellIdentifire(), for: indexPath) as! OfferTableViewCell
            cell.configureCellForOffer(indexPath.row, lock_rate_status: lockRateStatus, suggestedOffer: suggestedOffer)
            configureTextField(cell.amountTextField)
            updateDataInTextField(cell.amountTextField, fromCell: true)
            return cell
            
        case EdgemOfferTextField.forHours.rawValue :
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: OfferForOptionsTableViewCell.cellIdentifire(), for: indexPath) as? OfferForOptionsTableViewCell else{return UITableViewCell()}
            configureTextField(cell.optionTextField)
            cell.configureCellForHours(indexPath.row)
            updateDataInTextField(cell.optionTextField, fromCell: true)
            return cell
            
        case EdgemOfferTextField.forSubjects.rawValue :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: OfferForOptionsTableViewCell.cellIdentifire(), for: indexPath) as! OfferForOptionsTableViewCell
            cell.configureCell(indexPath.row)
            configureTextField(cell.optionTextField)
            updateDataInTextField(cell.optionTextField, fromCell: true)
            return cell
            
        case EdgemOfferTextField.selectLevel.rawValue :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: OfferForOptionsTableViewCell.cellIdentifire(), for: indexPath) as! OfferForOptionsTableViewCell
            cell.configureCell(indexPath.row)
            //configureTextField(cell.optionTextField)
            updateDataInTextField(cell.optionTextField, fromCell: true)
            configureTextField(cell.optionTextField)
            return cell
            
        case EdgemOfferTextField.forLessons.rawValue :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: OfferForOptionsTableViewCell.cellIdentifire(), for: indexPath) as! OfferForOptionsTableViewCell
            cell.configureCell(indexPath.row)
            configureTextField(cell.optionTextField)
            updateDataInTextField(cell.optionTextField, fromCell: true)
            return cell
            
        case 5 :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: OfferTotalAmmountTableViewCell.cellIdentifire(), for: indexPath) as! OfferTotalAmmountTableViewCell
            if let amt = makeOfferRequst.totalAmmount{
                cell.configureCell(indexPath.row,amt.changeStringNumberToWholeNumber(),amt.changeStringNumberToWholeNumber())
            }
            return cell
            
        default:
            return UITableViewCell()
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension EdgemOfferViewController: ChatViewControllerDelegate{
    func changeVCTitle(_ title: String?, _ vcNavigationStatus: Int) {
        self.isCounterOrEditOffer = "counter"
        self.isFromViewController = ChatViewController()
        self.vcNavigationStatus = vcNavigationStatus
    }
}

    // MARK: ----------- TextFields Methods

extension EdgemOfferViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    
        if textField.tag == EdgemOfferTextField.forHours.rawValue || textField.tag == EdgemOfferTextField.forLessons.rawValue || textField.tag == EdgemOfferTextField.forSubjects.rawValue || textField.tag == EdgemOfferTextField.selectLevel.rawValue {
             managePickerView()
        }
       
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataInTextField(textField, fromCell: false)
        
        let indexPath = IndexPath(row: textField.tag, section: 0)
        
            switch textField.tag{
                
            case EdgemOfferTextField.perHour.rawValue :
                guard let cell = tableView.cellForRow(at: indexPath) as? OfferTableViewCell else{return}
                cell.validateOffer(textField.text!.replacingOccurrences(of: " ", with: ""))
                
            case EdgemOfferTextField.forSubjects.rawValue :
                guard let cell = tableView.cellForRow(at: indexPath) as? OfferForOptionsTableViewCell else{return}
                cell.validateSubjects(textField.text!)
                
            default:
                break
        }
        if makeOfferRequst.subjects == "Select Subject".localized{
            print("Please select subject")
        }else{
             self.countToatalValue()
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
        }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty { return true }
        if textField.tag == 0{
            let currentText = textField.text ?? ""
            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
            return replacementText.isValidDouble(maxDecimalPlaces: 2)
        }else if textField.tag == 1{
            return false
        }
        return true
    }
    
    }

extension String {
    func isValidDouble(maxDecimalPlaces: Int) -> Bool {

        let formatter = NumberFormatter()
        formatter.allowsFloats = true
        let decimalSeparator = formatter.decimalSeparator ?? "."
        if formatter.number(from: self) != nil {
            let split = self.components(separatedBy: decimalSeparator)
            let digits = split.count == 2 ? split.last ?? "" : ""
            return digits.characters.count <= maxDecimalPlaces    // TODO: Swift 4.0 replace with digits.count, YAY!
        }
        return false
    }
}

    // MARK: ----------- PickerView Methods

extension EdgemOfferViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    fileprivate func managePickerView() {
        
        let textField = activeTextField
        
        hoursPickerView.reloadAllComponents()
        hoursPickerView.reloadInputViews()
        
        lessonsPickerView.reloadAllComponents()
        lessonsPickerView.reloadInputViews()
        
        subjectsPicker.reloadAllComponents()
        subjectsPicker.reloadInputViews()
        
        levelPicker.reloadAllComponents()
        levelPicker.reloadInputViews()
        
        if textField?.tag == 1{
            
             textField?.inputView = hoursPickerView
            
            let index = hoursOption.index(of: makeOfferRequst.hours ?? "")
            if let _index = index {
                self.hoursPickerView.selectRow(_index, inComponent: 0, animated: true)
            }else{
                self.hoursPickerView.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = hoursOption[0]
            }
            
        }else if textField?.tag == 2 {
            
            if let subjects = subjectOptions?[safe:0]?.0{
                activeTextField.text = subjects
                
                if let common_subjects = self.commonSubjectOfStudentAndTutor{
                    let selectedSubject = common_subjects.filter{$0.name! == subjects}
                    self.levelOptions = selectedSubject[0].levels.filter{ $0.price! != "0.0"}
                }
            }
            if let _lockRateStatus = subjectOptions?[safe: 0]?.2{
                lockRateStatus = _lockRateStatus
            }
            
            if let offer = subjectOptions?[safe: 0]?.1 {
                suggestedOffer = offer
            }
            
            let indexPath1 = IndexPath(row: 0, section: 0)
            let indexPath2 = IndexPath(row: 3, section: 0)
            // tableView.beginUpdates()
            if let lockRate = subjectOptions?[safe: 0]?.2{
                makeOfferRequst.lockRate = lockRate
            }
            if let subjectID = subjectOptions?[safe: 0]?.3{
                makeOfferRequst.subjectID = subjectID
            }
            
            if let levelName = subjectOptions?[safe: 0]?.4{
                makeOfferRequst.level = levelName
            }
            
            if let levelID = subjectOptions?[safe: 0]?.5{
                makeOfferRequst.levelID = levelID
            }
            
            if let ammountPerHour = subjectOptions?[safe: 0]?.1.replacingOccurrences(of: "$", with: ""){
                makeOfferRequst.ammountPerHour = ammountPerHour.changeStringNumberToWholeNumber()
            }
            
            tableView.reloadRows(at: [indexPath1,indexPath2], with: .automatic)
            countToatalValue()
        }else if textField?.tag == 3{
            
            textField?.inputView = levelPicker
            
            let index = lessonsOptions.index(of: makeOfferRequst.lessons ?? "")
            
            if let _index = index {
                self.levelPicker.selectRow(_index, inComponent: 0, animated: true)
            }else{
                self.levelPicker.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = levelOptions[safe: 0]?.name
            }
            
        }else if textField?.tag == 4 {
            textField?.inputView = lessonsPickerView
            
            let index = lessonsOptions.index(of: makeOfferRequst.lessons ?? "")
            if let _index = index {
                self.lessonsPickerView.selectRow(_index, inComponent: 0, animated: true)
            }else{
                self.lessonsPickerView.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = lessonsOptions[0]
            }
            
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == hoursPickerView {
             return hoursOption.count
        }else if pickerView == lessonsPickerView{
             return lessonsOptions.count
        }else if pickerView == levelPicker{
            return levelOptions.count
        }else{
            return subjectOptions?.count ?? 0
        }
       
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      
        if pickerView == hoursPickerView {
            return hoursOption[row]
        }else if pickerView == lessonsPickerView{
            return lessonsOptions[row]
        }else if pickerView == levelPicker{
            return levelOptions[row].name
        }else{
            return subjectOptions?[row].0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == hoursPickerView {
               activeTextField.text = hoursOption[row]
               makeOfferRequst.hours = hoursOption[row]
               countToatalValue()
          
        }else if pickerView == lessonsPickerView{
                activeTextField.text = lessonsOptions[row]
                makeOfferRequst.lessons = lessonsOptions[row]
               countToatalValue()
        }else if pickerView == levelPicker{
            let indexPath1 = IndexPath(row: 0, section: 0)
            activeTextField.text = levelOptions[row].name
            makeOfferRequst.level = levelOptions[row].name
            
            if let offer = levelOptions[safe: row]?.price {
                suggestedOffer = offer
            }
            
            if let ammountPerHour = levelOptions[safe: row]?.price{
                makeOfferRequst.ammountPerHour = ammountPerHour.changeStringNumberToWholeNumber()
            }
            
            if let levelID = levelOptions[safe: row]?.id{
                makeOfferRequst.levelID = levelID
            }
            
            tableView.reloadRows(at: [indexPath1], with: .automatic)
            countToatalValue()
        }else{
            if let subjects = subjectOptions?[safe:row]?.0{
                 activeTextField.text = subjects
                
                if let common_subjects = self.commonSubjectOfStudentAndTutor{
                    let selectedSubject = common_subjects.filter{$0.name! == subjects}
                    self.levelOptions = selectedSubject[0].levels.filter{ $0.price! != "0.0"}
                }
            }
            if let _lockRateStatus = subjectOptions?[safe: row]?.2{
                  lockRateStatus = _lockRateStatus
            }
          
            if let offer = subjectOptions?[safe: row]?.1 {
                suggestedOffer = offer
            }
           
            let indexPath1 = IndexPath(row: 0, section: 0)
            let indexPath2 = IndexPath(row: 3, section: 0)
           // tableView.beginUpdates()
            if let lockRate = subjectOptions?[safe: row]?.2{
                makeOfferRequst.lockRate = lockRate
            }
            if let subjectID = subjectOptions?[safe: row]?.3{
                 makeOfferRequst.subjectID = subjectID
            }
            
            if let levelName = subjectOptions?[safe: row]?.4{
                makeOfferRequst.level = levelName
            }
            
            if let levelID = subjectOptions?[safe: row]?.5{
                makeOfferRequst.levelID = levelID
            }
           
            if let ammountPerHour = subjectOptions?[safe: row]?.1.replacingOccurrences(of: "$", with: ""){
                makeOfferRequst.ammountPerHour = ammountPerHour.changeStringNumberToWholeNumber()
            }
            
            tableView.reloadRows(at: [indexPath1,indexPath2], with: .automatic)
            countToatalValue()
        }
        
    }
}

// MARK: - API Implementation
extension EdgemOfferViewController {
    
    func makeOffer(){

        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        var params = [String:String]()
        params = makeOfferRequst.getOfferRequset()
        
        if let totalAmt = params["total_amount"], totalAmt.isEmptyString() == false{
             let amt_in_float = (totalAmt as NSString).doubleValue
            if amt_in_float < 0.5{
                 self.showSimpleAlertWithMessage("Amount must be at least $0.50 sgd")
                 return
            }
        }
   
        if let tID = self.tutorId{
            params["offered_to"] = tID
        }else{
              params["offered_to"] = ""
        }
        
//        PrintJSONOnConsole.printJSON(dict: params as [String : AnyObject])
//        return
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.makeOffer(params as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(offer) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                self.makeOfferRequst = offer
                AppVariables.makeOfferRequest = offer
                 let chatViewController = ChatViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
                 chatViewController.makeOfferRequst = offer
                 chatViewController.firstTimeOffer = true
                 chatViewController.getMakeOfferData = {  [weak self] offer, status in
                    self?.getPreviouslyMadeOfferRequst(offer, status)
                }
                chatViewController.setTitle = { [weak self] title,vcStatus in
                    self?.isCounterOrEditOffer = "edit"
                    self?.isFromViewController = ChatViewController()
                    self?.vcNavigationStatus = vcStatus
                }
                
                chatViewController.delegate = self
                  self.navigationController?.pushViewController(chatViewController, animated: true)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    
    func counterOffer(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        var params = [String:String]()
        
        params = makeOfferRequst.getOfferRequset()
        
        if let totalAmt = params["total_amount"], totalAmt.isEmptyString() == false{
            let amt_in_float = (totalAmt as NSString).doubleValue
            if amt_in_float < 0.5{
                self.showSimpleAlertWithMessage("Amount must be at least $0.50 sgd")
                return
            }
        }
        
        guard let offerID = makeOfferRequst.offerID else{return}
        params["offer_id"] = "\(offerID)"
        
        print(params)
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.counterOffer(params as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(offer) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                let chatViewController = ChatViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
                chatViewController.makeOfferRequst = offer
                chatViewController.getMakeOfferData = {  [weak self] offer, status in
                    self?.getPreviouslyMadeOfferRequst(offer, status)
                }
                chatViewController.setTitle = { [weak self] title,vcStatus in
                    self?.isCounterOrEditOffer = "edit"
                    self?.isFromViewController = ChatViewController()
                    self?.vcNavigationStatus = vcStatus
                 }
                 chatViewController.delegate = self
                //self.navigationController?.pushViewController(chatViewController, animated: true)
                if let status = self.vcNavigationStatus{
                    if status == VCNavigationStatus.push.description(){
                        self.navigationController?.popViewController(animated: true)
                 }else if status == VCNavigationStatus.pop.description(){
                         self.navigationController?.pushViewController(chatViewController, animated: true)
                    }
                }
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    
    func updateOffer(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        var params = [String:String]()
        
        params = makeOfferRequst.getOfferRequset()
        
        if let totalAmt = params["total_amount"], totalAmt.isEmptyString() == false{
            let amt_in_float = (totalAmt as NSString).doubleValue
            if amt_in_float < 0.5{
                self.showSimpleAlertWithMessage("Amount must be at least $0.50 sgd")
                return
            }
        }
        
        guard let offerID = makeOfferRequst.offerID else{return}
        params["offer_id"] = "\(offerID)"

        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.editUpdateOffer(params as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(offer) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                let chatViewController = ChatViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
                chatViewController.makeOfferRequst = offer
                chatViewController.getMakeOfferData = {  [weak self] offer, status in
                    self?.getPreviouslyMadeOfferRequst(offer, status)
                }
                chatViewController.delegate = self
                if let status = self.vcNavigationStatus{
                    if status == VCNavigationStatus.push.description(){
                        self.navigationController?.popViewController(animated: true)
                    }else if status == VCNavigationStatus.pop.description(){
                        self.navigationController?.pushViewController(chatViewController, animated: true)
                    }
                }
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    
    //getTutorSubjects
    func getTutorSubjects(){

        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }

        //Utilities.showHUD(forView: self.view, excludeViews: [])
        var tutorID: String = ""

            if AppDelegateConstant.user?.userType == tutorType{
                guard let id =  AppDelegateConstant.user?.ID else{ return}
                tutorID = "\(id)"
            }else{
                if AppVariables.chatListUserDetail != nil{

                    if AppDelegateConstant.user?.ID != AppVariables.chatListUserDetail?.offeredByID{
                        guard let offeredByID =  AppVariables.chatListUserDetail?.offeredByID else{ return}
                        tutorID = "\(offeredByID)"
                    }else{
                        guard let offeredToID =  AppVariables.chatListUserDetail?.offeredToID else{ return}
                        tutorID = "\(offeredToID)"
                    }

                }else if AppVariables.makeOfferRequest != nil{

                    if AppDelegateConstant.user?.ID != AppVariables.makeOfferRequest?.offeredByID{
                        guard let offeredByID =  AppVariables.makeOfferRequest?.offeredByID else{ return}
                        tutorID = "\(offeredByID)"
                    }else{
                        guard let offeredToID =  AppVariables.makeOfferRequest?.offeredToID else{ return}
                        tutorID = "\(offeredToID)"
                    }

                }
            }

        if tutorID.isEmpty{
            print(value: "Tutor id not found")
            return
        }

        let userObserver = ApiManager.shared.apiService.getTutorSubjects(userID: tutorID)

        let userDisposable = userObserver.subscribe(onNext: {(tutorSubjects) in
            DispatchQueue.main.async {
        
                for subject in tutorSubjects{
                    
                    /// Below are two conditions 1 and 2 . I will be executed when we are comming on this screen from ChatMemberListingViewController. In this case makeofferRequst will be nill. 2 will be executed when we are comming directlly from chat list in this case BookingDetail will be nill. only one at a time will be executed

                    // 1
                    if let chatListUserDetail = AppVariables.chatListUserDetail{

                        if subject.subjectID == "\(chatListUserDetail.subjectID)"{

                             self.lockRateStatus                    = subject.lock_rate
                            
                             self.makeOfferRequst.lockRate =  subject.lock_rate

                             subject.levels.forEach({ (level) in
                                if let sub_lev_id = level.id, let chatList_user_lev_id = AppVariables.chatListUserDetail?.levelID{
                                    if sub_lev_id == "\(chatList_user_lev_id)"{
                                        self.suggestedOffer = level.price!.changeStringNumberToWholeNumber()
                                    }
                                    
                                }
                                
                            })
                            
                        }

                    }
                    
                    // 2
                    if let makeOfferDetail = AppVariables.makeOfferRequest{
                        
                        if subject.subjectID == makeOfferDetail.subjectID{
                            
                            self.lockRateStatus                    = subject.lock_rate
                            
                            self.makeOfferRequst.lockRate =  subject.lock_rate
                            
                            subject.levels.forEach({ (level) in
                                if let sub_lev_id = level.id, let chatList_user_lev_id = AppVariables.makeOfferRequest?.levelID{
                                    if sub_lev_id == "\(chatList_user_lev_id)"{
                                        self.suggestedOffer = level.price!.changeStringNumberToWholeNumber()
                                    }
                                    
                                }
                                
                            })
                            
                        }
                        
                    }
    
                }
                
                self.tableView.reloadData()
                self.countToatalValue()

            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    
    
}


extension Double{
    func isWholeNumber() -> String{
        if self.truncatingRemainder(dividingBy: 1) == 0{
           return "\(Int(self))"
        }else{
            let multiplier = pow(10, Double(2))
            return "\((Darwin.round(self * multiplier) / multiplier))"
        }
    }
}

extension String{
    func changeStringNumberToWholeNumber() -> String{
        return ((self as NSString).doubleValue).isWholeNumber()
    }
}
