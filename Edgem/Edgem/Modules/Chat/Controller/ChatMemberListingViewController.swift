//
//  ChatMemberListingViewController.swift
//  Edgem
//
//  Created by Namespace on 02/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class ChatMemberListingViewController : BaseViewController {
    
    // MARK: - Properties
    
    var disposableBag = DisposeBag()
    
    var pagination = Pagination.init()
    var bookingDetails = [BookingDetails]()
    
    var isRefreshed = false
    
     var bookingPagination = BookingPagination()
     var isFetchingData = false
    var rowCount = 0
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = theamGrayColor
        
        return refreshControl
    }()
  
    // MARK: - @IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var NoChatListFoundLabel: UILabel!
    // MARK: - ViewLifeCycles
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //pagination = Pagination.init()
        
//        self.navigationController?.tabBarItem.badgeValue = "5"
        
        self.tableView.addSubview(self.refreshControl)
        initialSetUp()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.tabBarItem.badgeValue = nil
        connectToSocketChannel()
         pagination = Pagination.init()
         self.bookingDetails.removeAll()
         isRefreshed = true
        NoChatListFoundLabel.isHidden = true

        getBookingDetails()

    }
    
    // MARK: - @IBActions
    
    
    // MARK: - Helping Functions
    
    @objc func connectToSocketChannel() {
        
        
        if let _ = AppDelegateConstant.socket{
            
            guard AppDelegateConstant.socket.status == .connected else{
                AppDelegateConstant.conectSocket()
                perform(#selector(connectToSocketChannel), with: nil, afterDelay: 1)
                return
            }
        }else{
            AppDelegateConstant.conectSocket()
            guard AppDelegateConstant.socket.status == .connected else{
                perform(#selector(connectToSocketChannel), with: nil, afterDelay: 1)
                return
            }
        }
        

        let token = UserStore.shared.token
        let headers = ["Authorization":  "Bearer " + token,
                       "User": String(UserStore.shared.userID ?? 0)]
        
        let tempHeader = ["headers":headers]
        
        let subscribeData = ["channel": "private-edgem-offers.\(String(UserStore.shared.userID ?? 0))",
            "name": "subscribe",
            "auth": tempHeader] as [String : Any]
        print(subscribeData)
        AppDelegateConstant.socket.emitWithAck("subscribe", subscribeData).timingOut(after: 2, callback: { (data) in
            print("subscribed")
            
            AppDelegateConstant.socket.on("lastOffer") {data, ack in
                
                  print(value: data)
                
                  if let tempData = (data[1] as? [String:AnyObject]), let indexData = tempData["0"] as? [String:AnyObject]{
                   let bookingDetails = BookingDetails(booking: indexData)
                    for bookingDetailObject in self.bookingDetails{
                        if  bookingDetailObject.offerID == bookingDetails.offerID{
                            return
                        }
                    }
                    
                    self.bookingDetails.append(bookingDetails)
                    if self.bookingDetails.count > 0{
                        self.rowCount = self.bookingDetails.count
                        self.NoChatListFoundLabel.isHidden = true
                    }else{
                        self.rowCount = self.bookingDetails.count
                        self.NoChatListFoundLabel.isHidden = false
                    }
                    self.tableView.reloadData()
                }
            }
            
            AppDelegateConstant.socket.on("lastInList") {data, ack in
                print(data)
                if let tempData = (data[1] as? [String:AnyObject]), let indexData = tempData["0"] as? [String:AnyObject]{
                    let bookingDetails = BookingDetails(booking: indexData)
                    for (index, offerObject) in self.bookingDetails.enumerated(){
                        print(index,offerObject )
                        if offerObject.offerID == bookingDetails.offerID{
                            self.bookingDetails[index] = bookingDetails
                            let indexPath = IndexPath(row: index, section: 0)
                            self.tableView.reloadRows(at: [indexPath], with: .none)
                        }
                    }
                }
            }
            
            AppDelegateConstant.socket.on("offerDeleted") {data, ack in
                print(data)
                if let tempData = data[1] as? [String:AnyObject], let offerID = tempData["offer_deleted"] as? String {
                    if let local_offer_id = Int(offerID) {
                        self.bookingDetails = self.bookingDetails.filter{$0.offerID != local_offer_id}
                        if self.bookingDetails.count > 0{
                            self.rowCount = self.bookingDetails.count
                            self.NoChatListFoundLabel.isHidden = true
                        }else{
                            self.rowCount = self.bookingDetails.count
                            self.NoChatListFoundLabel.isHidden = false
                        }
                        self.tableView.reloadData()
                    }
                }
            }

        })
        AppDelegateConstant.socket.connect()
       // }
        
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
       // self.bookingPagination.paginationType = .new
      //  if pagination.currentPage < pagination.lastPage{
              pagination = Pagination.init()
              self.bookingDetails.removeAll()
              isRefreshed = true
              getBookingDetails()
     //  }
        
        refreshControl.endRefreshing()
    }
    
    private func initialSetUp(){
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
    }
    
}

    // MARK: - TableView Delegates/Datasources

extension ChatMemberListingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ChatMemberTableViewCell.cellIdentifire(), for: indexPath) as? ChatMemberTableViewCell else{
            return UITableViewCell()
        }
        self.bookingDetails = self.bookingDetails.sorted(by: {$1.lastmessage.timeStamp < $0.lastmessage.timeStamp})
        
        if self.bookingDetails.count > 0 {
             cell.isUserInteractionEnabled = true
             cell.configureCell(with: self.bookingDetails[indexPath.row])
        }else{
             cell.isUserInteractionEnabled = false
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ChatMemberTableViewCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//                    if indexPath.row == self.bookingDetails.count-3 && bookingPagination.hasMoreToLoad  && !isFetchingData{
//                        getBookingDetails()
//                    }
        if indexPath.row == self.bookingDetails.count-3 && pagination.currentPage != pagination.lastPage{
            getBookingDetails()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
         if let bookingDetails  = self.bookingDetails[safe: indexPath.row]{
            let chatViewController = ChatViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
            
            AppVariables.chatListUserDetail = bookingDetails
            chatViewController.bookingDetails = bookingDetails
             let data = [  "offer_id" : bookingDetails.offerID,
                                "total_ammount" : bookingDetails.totalAmmount] as [String : AnyObject]
            UserStore.shared.offerIDNTotalAmmount = data
            
             self.navigationController?.pushViewController(chatViewController, animated: true)
        }
       
    }
    
}

    // MARK: - API Implementation

extension ChatMemberListingViewController {
    
    func getBookingDetails() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        //(String(pagination.currentPage+1))
        
         isFetchingData = true
        let getBookingDetalsObserver = ApiManager.shared.apiService.fetchBookingDetails(String(pagination.currentPage+1))
        let getBookingDetalsDisposable = getBookingDetalsObserver.subscribe(onNext: { (bookingPagination) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                self.isFetchingData = false
                self.pagination = bookingPagination.pagination
                if self.isRefreshed == true{
                    self.bookingDetails = bookingPagination.bookingDetails
                }else{
                    self.bookingDetails += bookingPagination.bookingDetails
                }
                self.isRefreshed = false
      
                if self.bookingDetails.count > 0{
                    self.rowCount = self.bookingDetails.count
                    self.NoChatListFoundLabel.isHidden = true
                }else{
                    self.rowCount = self.bookingDetails.count
                    self.NoChatListFoundLabel.isHidden = false
                }
                  self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                self.isFetchingData = false
                //Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        return getBookingDetalsDisposable.disposed(by: disposableBag)
    }
    
}




