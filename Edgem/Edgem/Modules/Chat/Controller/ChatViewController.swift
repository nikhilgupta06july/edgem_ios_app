//
//  ChatViewController.swift
//  Edgem
//
//  Created by Hipster on 27/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import RxSwift
import IQKeyboardManagerSwift
import Alamofire
import EasyTipView

enum VCNavigationStatus{
    case push
    case pop
    
    func description() -> Int {
        switch self{
        case .push: return 0
        case .pop: return 1
        }
    }
    
}

enum ViewType: String {
    case messageView
    case alert
    case image
    func description() -> String {
        switch self{
        case .messageView: return "Text"
        case .alert: return "alert_view"
        case .image: return "MIME"
        }
    }
}

enum progressBarStatus :Int{
    case active = 0
    case partially_active = 1
    case in_Active = -1
    case completed = 2
}

struct ProgressBarData {
    let label: String
    var status: Int
    
    mutating func setStatus(_ _status: Int){
        status = _status
    }
}

protocol ChatViewControllerDelegate: class {
    func changeVCTitle(_ title: String?, _ vcNavigationStatus: Int)
}

class ChatViewController: BaseViewController {
    
    // MARK: ----------- Properties
    
    weak var delegate: ChatViewControllerDelegate?
    
    var preferences: EasyTipView.Preferences!
    
    //var shouldDisableSocket: Bool = true
    
    /// This var will be used to detect whether offer is cancelled or not
    var isOfferCancel = false
    
    var newMessage: [ChatMessage] =  [ChatMessage]()
    var FromServer:Bool = false
    
    var isFirstTime = true
    
    var progressBarData: [ProgressBarData] = [ProgressBarData]()
    
    var collectionViewRowCount = 4
    var totalLessons = 0
    
    var chatMessageObject: ChatMessage?
    var pagination = Pagination()
    
    weak var tipView: EasyTipView?

    ///  This variable is used to store messages from comming from server
    var messagesFromServer: [ChatMessage]?

    ///  This variable is used to store array of message objects
    var chatMessages = [[ChatMessage]]()
    
   ///  This variable is used to store offer object from 'EdgemOfferViewController.swift'
    var makeOfferRequst:  MakeOfferRequst!
    
    var getMakeOfferData: ((MakeOfferRequst, Bool)->Void)?
    
    /// This var will be used to set title of EdgemOfferViewController if we came on chat vc from  EdgemOfferViewController first time
    var setTitle: ((String,Int)->Void)?
    
    var disposableBag = DisposeBag()
    
    ///  This variable is used to store offer object from 'ChatMemberListingViewController.swift'
    var bookingDetails: BookingDetails?
    
    var shouldEnableChat = false
    
    let notificationCenter = NotificationCenter.default
   
    var counterOfferPicker = UIPickerView()
    var datePicker = UIDatePicker()
    var toolBar = UIToolbar()

    var startOfWeek: Date?
    var endOfWeek: Date?
    
    var activeTextView = UITextView()
    
    var firstTimeOffer: Bool!
    var calanderVisibilityStatus: Bool = false
    
    var currentLesson = ""
    var lastLesson = ""
    var currentPosition: Int?
    var index = 0
    var timer: Timer!
    var isAnimated = false
    
    var cancleOfferBeforeTime = false
    var cancleOfferAfterTime = false
    
    var cancelOfferBefore24Hr = false
    
    var messageIDOnTutorArrived: String = ""
    
    var currentSection: Int?
    var currentRow: Int?
    
    // MARK: ----------- @IBOutlets
    
    @IBOutlet weak var messageTextViewHeight: NSLayoutConstraint!{
        didSet{
            messageTextView.layer.cornerRadius = messageTextViewHeight.constant/2.0
        }
    }
    
    @IBOutlet weak var tooTipContainerView: UIView!
    @IBOutlet weak var threeDotBtn: UIButton!
    @IBOutlet weak var editCancleHeight: NSLayoutConstraint!
    @IBOutlet weak var messageTextView: MessageTextView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var sendMessageBtn: UIButton!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
   
    @IBOutlet weak var onlineStatus: UIImageView!
    @IBOutlet weak var onlineStatusLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editCacleOfferContainerView: UIView!
    @IBOutlet weak var cancleBtnContainerView: UIView!
    @IBOutlet weak var calenderBtn: UIButton!
    @IBOutlet weak var editOfferBtn: UIButton!
    @IBOutlet weak var editOfferContainerView: UIView!
    @IBOutlet weak var cancleBtn: UIButton!
    @IBOutlet weak var chatBoxContainerView: UIView!
    @IBOutlet weak var chatBoxContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var counterOfferBtn: UIButton!
    @IBOutlet weak var counterBtnContainerView: UIView!
    @IBOutlet weak var plusBtn: UIButton!
     @IBOutlet weak var infoBtn: UIButton!
    // MARK: ----------- View LifeCycle
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        if let tipView = self.tipView{
            tipView.dismiss()
            infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        moveToReviewController()
        calanderAndChatVisibilityStatus()
        connectToSocketChannel()
        initialSetUP()
        registerCell()
        showTotalButtons()
        toolTipSetUP()
        if bookingDetails?.isCancelled != 1{
             NotificationCenter.default.addObserver( self,
                                                                              selector: #selector(connectToSocketChannel),
                                                                              name: .reconnectSocketSubscriber,
                                                                               object: nil)
        }
    
        }
    
    deinit {
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        notificationCenter.removeObserver(self)
        disconnectToSocketChannel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tooTipContainerView.isHidden = true
        
         addNotifications()
        
        var lessons = ""
        
        if let vc = self.navigationController?.getPreviousViewController(){

            if vc is EdgemOfferViewController{
                
                // ============= Count Total Lessons ============
                if let lessons = makeOfferRequst.lessons, let _totalLessons = Int(lessons){
                    self.totalLessons = _totalLessons
                }
                if let _lesson = makeOfferRequst.lessons {
                    lessons = _lesson
                }
            
            }else{
                
                // ============= Count Total Lessons ============
                if let lesson = bookingDetails?.forLessons {
                    self.totalLessons = lesson
                }
                if let _lesson = bookingDetails?.forLessons {
                    lessons = "\(_lesson)"
                }

           }
        }
        
        
        enableChatBefore24HourOfFirstLesonScheduling()
        
        collectionViewRowCount = collectionViewRowCount + self.totalLessons // ---> number of row in collection view
        
        configureProgressBarData()

        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                if let status = makeOfferRequst.status{
                    self.disableChatOnOfferCompleted(status)
                    getProgressBarCurrentStatus(status)
                }
            }else{
                if let status = bookingDetails?.status{
                    self.disableChatOnOfferCompleted(status)
                    getProgressBarCurrentStatus(status)
            
                    if bookingDetails?.isCancelled == 1{
                        activitiesOnCancleOffer()
                    }
                }
            }
        }
        
        // ----> Hit API to get messages from server
        if self.messagesFromServer == nil  {
               getAllMessages()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeNotifications()
        
        if let tipView = self.tipView{
            tipView.dismiss()
            infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }
    
    }
    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        if shouldDisableSocket {
//            disconnectToSocketChannel()
//        }
//    }
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isAnimated == false{
            animateCollectionView()
            isAnimated = true
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isAnimated == true {
             animateCollectionView()
        }
        chatViewsLayout()
        //self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
         print("memory full")
        imageCache.removeAllObjects()
    }
    
     // MARK: ----------- @IBActions
    
    
    fileprivate func activitiesOnCancleOffer() {
        
        chatBoxContainerView.isHidden = true
        
        calenderBtn.isHidden = true
        
        self.calanderVisibilityStatus  = false
        
        threeDotBtn.isHidden = true
        
        chatBoxContainerViewHeight.constant = 0
        
        editCancleHeight.constant =  0
        
        editCacleOfferContainerView.isHidden  =  true
        
         self.tooTipContainerView.isHidden = true
        
        isOfferCancel = true
        
        self.collectionView.reloadData()
        self.tableView.reloadData()
        
        let alert = UIAlertController(title: AppName, message: "This offer has been cancelled".localized, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
            
            if let vc = self.navigationController?.getPreviousViewController(){
                
                if vc is EdgemOfferViewController{
                    
                    self.makeOfferRequst.hours = "1".localized
                    self.makeOfferRequst.subjects = "Select Subject".localized
                    self.makeOfferRequst.lessons = "4".localized
                    self.makeOfferRequst.totalAmmount = "0.00"
                    self.makeOfferRequst.ammountPerHour = "Enter Amount*".localized
                    self.makeOfferRequst.offerID = nil
                    
                    self.getMakeOfferData?(self.makeOfferRequst, false)
                    
                    // self.navigationController?.popViewController(animated: true)
                    
                }else{
                    
                    // self.navigationController?.popViewController(animated: true)
                    
                }
                
            }
            
        })
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func disableChat(_ currentStatus: String) {
        
        self.messageTextView.isUserInteractionEnabled = false
        
        sendMessageBtn.isUserInteractionEnabled = false
        
        plusBtn.isUserInteractionEnabled = false
    
        infoBtn.isHidden = currentStatus == "done" ? true : false
        
        messageTextView.text = ""
        
        messageTextView.backgroundColor = cellBackgroundColor
        
        shouldEnableChat = false
    }
    
    fileprivate func enableCaht() {
        
        self.messageTextView.isUserInteractionEnabled = true
        
        plusBtn.isUserInteractionEnabled = true
        
        infoBtn.isHidden = true
        
        sendMessageBtn.isUserInteractionEnabled = true
        
        messageTextView.text = "Type your message here".localized
        
        messageTextView.backgroundColor = .white
        
        shouldEnableChat = true
    }
    
    private func enableChatBefore24HourOfFirstLesonScheduling(){
        
        if let curentStatusPos = self.currentPosition, curentStatusPos < 3{
            disableChat(self.currentLesson)
            return
        }
        
        
        var bookingTime: String = ""
        var currentStatus: String = ""
        
        if let vc = self.navigationController?.getPreviousViewController(){
            
            if vc is EdgemOfferViewController{
                
                // ============= Get Booking Time ============
                if let _bookingTime = self.makeOfferRequst?.paymentAt{
                    
                    bookingTime = _bookingTime
                    
                }
                
                if let _currentStatus = self.makeOfferRequst?.status{
                    currentStatus = _currentStatus
                }
                
            }else{
                
                if let _bookingTime = self.bookingDetails?.paymentAt{
                    
                    bookingTime = _bookingTime
                    
                }
                
                if let _currentStatus = self.bookingDetails?.status{
                    currentStatus = _currentStatus
                }
                
            }
            
            if currentStatus.contains("lesson") && currentStatus != "1lesson"{
                enableCaht()
            }else if currentStatus.contains("done"){
                disableChat(currentStatus)
            }else{
                
                if bookingTime.isEmpty  || bookingTime == " :00:00" || bookingTime == "0000-00-00 :00:00" || bookingTime == "0000-00-00 13:00:00"{
                    
                    disableChat(currentStatus)
                    
                }else{
                    bookingTime = Utilities.UTCToLocal(date: bookingTime, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "yyyy-MM-dd HH:mm:ss")
                    // if let _bookingTime = bookingTime.stringToDate(date: bookingTime, format: "yyyy-MM-dd HH:mm:ss")
                    if let _bookingTime = bookingTime.stringToDateWithOutUTC(date: bookingTime, format: "yyyy-MM-dd HH:mm:ss"){
                        
                        let today = Date()
                        let todat_str = today.dateToString(dateFormat: "yyyy-MM-dd HH:mm:ss")
                        let  today_without_utc_str = Utilities.UTCToLocal(date: todat_str, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "yyyy-MM-dd HH:mm:ss")
                        let today_without_utc_date = today_without_utc_str.stringToDateWithOutUTC(date: bookingTime, format: "yyyy-MM-dd HH:mm:ss")
                        guard let date = today_without_utc_date else{
                            disableChat(currentStatus)
                            return}
                        let timeInterval_BW_BookingTime_And_CurrentTime = _bookingTime.seconds(from: date)
                        
                        if timeInterval_BW_BookingTime_And_CurrentTime <= 86400{
                            
                            enableCaht()
                            
                        }else{
                            
                            disableChat(currentStatus)
                            
                        }
                        
                    }else{
                        
                        disableChat(currentStatus)
                        
                    }
                    
                }
                
            }
   
        }
    }
    
    fileprivate func navigateToProfile(_ id: inout Int?) {
        
        if AppDelegateConstant.user?.ID == self.makeOfferRequst.offeredByID{
            id = self.makeOfferRequst?.offeredToID
        }else{
            id = self.makeOfferRequst?.offeredByID
        }
        
        if AppDelegateConstant.user?.userType == tutorType {
            
            let studentCompleteProfileViewController = StudentCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            
            studentCompleteProfileViewController.studentID = id
            
            studentCompleteProfileViewController.isFrom = self
            
            self.navigationController?.pushViewController(studentCompleteProfileViewController, animated: true)
            
        }else{
            
            let tutorCompleteProfileViewController = TutorCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            
            tutorCompleteProfileViewController.tutorID = id
            
            tutorCompleteProfileViewController.isFrom = self
            
            self.navigationController?.pushViewController(tutorCompleteProfileViewController, animated: true)
            
        }
    }
    
    fileprivate func navigateToProfileFromChatListing(_ id: inout Int?) {
        if AppDelegateConstant.user?.ID == self.bookingDetails?.offeredByID{
            id = self.bookingDetails?.offeredToID
        }else{
            id = self.bookingDetails?.offeredByID
        }
        
        if AppDelegateConstant.user?.userType == tutorType {
            
            let studentCompleteProfileViewController = StudentCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            
            studentCompleteProfileViewController.studentID = id
            
            studentCompleteProfileViewController.isFrom = self
            
            self.navigationController?.pushViewController(studentCompleteProfileViewController, animated: true)
            
        }else{
            
            let tutorCompleteProfileViewController = TutorCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
            
            tutorCompleteProfileViewController.tutorID = id
            
            tutorCompleteProfileViewController.isFrom = self
            
            self.navigationController?.pushViewController(tutorCompleteProfileViewController, animated: true)
            
        }
    }
    
    @IBAction func didTapInfoBtn(_ sender: UIButton){
        
        let text = "Chat will be enabled 24hrs before the first lesson".localized
        
        if let tipView = self.tipView{
            
            tipView.dismiss()
            
            infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }else{
            
            let tip = EasyTipView(text: text, preferences: self.preferences, delegate: self)
            
            infoBtn.setImage(#imageLiteral(resourceName: "infoImageSelected"), for: .normal)
            
            tip.show(forView: self.infoBtn)
            
            tipView = tip
            
        }
        
    }
    
    @IBAction func didTapProfile(_ sender: UIButton) {
    
        var id: Int?

        
        if let vc = self.navigationController?.getPreviousViewController(){
            
            if vc is EdgemOfferViewController{
                
                navigateToProfile(&id)
                
            }else{
                
                navigateToProfileFromChatListing(&id)
                
            }
        }
        
    }
    
    @IBAction func didTapBackBtn(_ sender: UIButton) {
        
        //self.shouldDisableSocket = true
        
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                if let offerID = makeOfferRequst.offerID{
                     DisConnectUser.disConnectUser("\(offerID)")
                }
                if let second_vc = self.navigationController?.getSecondPreviousViewController(){
                    self.navigationController?.popToViewController(second_vc, animated: true)
                }
            }else{
                if let offerID = bookingDetails?.offerID{
                    DisConnectUser.disConnectUser("\(offerID)")
                }
                self.navigationController?.popViewController(animated: true)
            }
            
        }
    }
    
    @IBAction func didTapPlusBtn(_ sender: UIButton) {
        
         self.showImageOptions()
//
//        let uploadImageVC = UploadFromCameraLibraryDocVC.instantiateFromAppStoryboard(appStoryboard: .CommonPopUpVC)
//
//        uploadImageVC.modalPresentationStyle = .overCurrentContext
//
//        self.navigationController?.present(uploadImageVC, animated: true, completion: nil)
        
    }
    
    @IBAction func didTapCameraBtn(_ sender: UIButton) {
        if (sender.currentImage?.isEqual(#imageLiteral(resourceName: "camera")))! || (sender.hasImage(named: "camera", for: .normal) ==  true){
            print("camera")
            //return
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = .camera
                myPickerController.allowsEditing = true
                self.present(myPickerController, animated: true, completion: nil)
            }
            
        }else{
            print("message button tapped")
            if !(activeTextView.text.isEmpty){
                self.sendMessageWithText(activeTextView.text)
            }
        }
        
    }
    
    @IBAction func didTappedReportUserBtn(_ sender: UIButton) {
        
        var param: [String: String] = [String: String]()
        var userID = ""
        var userName = ""
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                if AppDelegateConstant.user?.ID == self.makeOfferRequst.offeredByID{
                    
                    if let uName = self.makeOfferRequst?.offeredToName, let uID = self.makeOfferRequst.offeredToID{
                        userName = uName
                        userID = "\(uID)"
                    }
                    
                }else{
                    
                    if let uName = self.makeOfferRequst?.offeredByName, let uID = self.makeOfferRequst.offeredByID{
                        userName = uName
                        userID = "\(uID)"
                    }
                    
                }
                
            }else{
                if AppDelegateConstant.user?.ID == self.bookingDetails?.offeredByID{
                    
                    if let uName = self.bookingDetails?.offeredToName, let uID = self.bookingDetails?.offeredToID{
                        userName = uName
                        userID = "\(uID)"
                    }
                    
                }else{
                    
                    if let uName = self.bookingDetails?.offeredByName, let uID = self.bookingDetails?.offeredByID{
                        userName = uName
                        userID = "\(uID)"
                    }
                    
                }
                
            }
        }
        
        param["userName"] = userName
        param["userID"] = userID
        let contactUSViewController = ContactUSViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        contactUSViewController.VC = self
        contactUSViewController.param = param
        self.navigationController?.pushViewController(contactUSViewController, animated: true)
    }
    
    /**
     It gets the payment time from server and calculates the time difference between current time and the time of payment.
    
     ## Important Notes ##
     1.  If booking time is not found from server then **cancleOffer** function will call which hits *cancel_offer* api
     2.  If payment time  found from server and time is before 24 hrs then **cancleOffer** function will call which hits *cancel_offer* api
     3.  If payment time  found from server and time is after 24 hrs then **cancleOffer** function will call which hits *cancel_paid_offer* api
     
     */
    fileprivate func doCancleOffer() {
        
        if let vc = self.navigationController?.getPreviousViewController(){
            
            if vc is EdgemOfferViewController{
                
                if let paymentAt =  self.makeOfferRequst?.paymentAt{
                    
                    if paymentAt.isEmpty || paymentAt == " :00:00"{
                        //cancleOfferBeforeTime = true
                        cancelOfferBefore24Hr = false
                        self.cancleOfferPopUp(AlertMessage.cancleOffer.description())
                        return
                    }
                    
                    let paymentTime = paymentAt.stringToDate(date: paymentAt, format: "yyyy-MM-dd HH:mm:ss")
                    let timeInterval_BW_PaymentTime_And_CurrentTime = Date().timeIntervalSince(paymentTime!)
                    
                    if Int(timeInterval_BW_PaymentTime_And_CurrentTime) <= 86400{
                        print("before 24 hrs \(timeInterval_BW_PaymentTime_And_CurrentTime)")
                        
                        //self.cancleOfferBeforeTime = true
                        cancelOfferBefore24Hr = true
                        self.cancleOfferPopUp(AlertMessage.cancleOfferBefore24.description())
                        
                    }else{
                        print("after 24 hrs \(timeInterval_BW_PaymentTime_And_CurrentTime)")
                        
                        //self.cancleOfferAfterTime = true
                        cancelOfferBefore24Hr = true
                        self.cancleOfferPopUp(AlertMessage.cancleOfferAfter24.description())
                    }
                }else{
                    cancelOfferBefore24Hr = false
                    self.cancleOfferPopUp(AlertMessage.cancleOffer.description())
                }
                
            }else{
                
                if let paymentAt =  self.bookingDetails?.paymentAt{
    
                    if paymentAt == " :00:00" || paymentAt.isEmpty || paymentAt == "0000-00-00 :00:00"{
                       // cancleOfferBeforeTime = false
                        cancelOfferBefore24Hr = false
                        self.cancleOfferPopUp(AlertMessage.cancleOffer.description())
                        return
                    }
                    
                    guard let paymentTime = paymentAt.stringToDate(date: paymentAt, format: "yyyy-MM-dd HH:mm:ss") else{return}
                    
                    let timeInterval_BW_PaymentTime_And_CurrentTime = paymentTime.timeIntervalSince(Date())
                    
                    if Int(timeInterval_BW_PaymentTime_And_CurrentTime) <= 86400{
                        print("before 24 hrs \(timeInterval_BW_PaymentTime_And_CurrentTime)")
                        
                        //self.cancleOfferBeforeTime = true
                        cancelOfferBefore24Hr = true
                        self.cancleOfferPopUp(AlertMessage.cancleOfferBefore24.description())
                        
                    }else{
                        print("after 24 hrs \(timeInterval_BW_PaymentTime_And_CurrentTime)")
                        
                       // self.cancleOfferAfterTime = true
                        cancelOfferBefore24Hr = true
                        self.cancleOfferPopUp(AlertMessage.cancleOfferAfter24.description())
                    }
                }else{
                    //cancleOfferBeforeTime = false
                    cancelOfferBefore24Hr = false
                    self.cancleOfferPopUp(AlertMessage.cancleOffer.description())
                }
                
            }
        }
    }
    
    @IBAction func didTappedCancleTuitionBtn(_ sender: UIButton) {
        
        if self.currentLesson == "done"{
            self.showSimpleAlertWithMessage("This offer has been completed".localized)
            return
        }
        doCancleOffer()
    }
    
    @IBAction func moreBtnTapped(_ sender: UIButton) {

        if sender.isSelected == true{
            sender.isSelected = false
            self.tooTipContainerView.isHidden = false
            
        }else{
            sender.isSelected = true
            self.tooTipContainerView.isHidden = true
        }
    
        
    
    }
    
    @IBAction func calanderBtnTapped(_ sender: UIButton) {
        if self.currentLesson == "done"{
             self.showSimpleAlertWithMessage("This offer has been completed".localized)
            return
        }
        
        ///is_rescheduled; // 0 - can schedule | 1 - request submitted | 2 - reqest Accepted | 3 - request cancel again can schedule
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                if let data = self.makeOfferRequst, data.isRescheduled == 1 || data.isRescheduled == 2{
                     var msg = ""
                    if let id = self.makeOfferRequst.schedule_cancel_by, id == AppDelegateConstant.user?.ID{
                         msg = AppDelegateConstant.user?.userType == tutorType ? "Please wait for student to accept/cancel the schedule or end tuition." : "Please wait for tutor to accept/cancel the schedule or end tuition."
                    }else{
                         msg = "Please accept/cancel  the schedule or wait for tuition end."
                    }
                    self.showSimpleAlertWithMessage(msg.localized)
                    return
                }
            }else{
                if let data = self.bookingDetails, data.isRescheduled == 1 ||  data.isRescheduled == 2{
                    var msg = ""
                    if let id = self.bookingDetails?.schedule_cancel_by, id == AppDelegateConstant.user?.ID{
                        msg = AppDelegateConstant.user?.userType == tutorType ? "Please wait for student to accept/cancel the schedule or end tuition." : "Please wait for tutor to accept/cancel the schedule or end tuition."
                    }else{
                        msg = "Please accept/cancel  the schedule or wait for tuition end."
                    }
                    self.showSimpleAlertWithMessage(msg.localized)
                    return
                }
            }
        }else{
            
            if let data = self.makeOfferRequst, data.isRescheduled == 1 || data.isRescheduled == 2{
                var msg = ""
                if let id = self.makeOfferRequst.schedule_cancel_by, id == AppDelegateConstant.user?.ID{
                    msg = AppDelegateConstant.user?.userType == tutorType ? "Please wait for student to accept/cancel the schedule or end tuition." : "Please wait for tutor to accept/cancel the schedule or end tuition."
                }else{
                    msg = "Please accept/cancel  the schedule or wait for tuition end."
                }
                self.showSimpleAlertWithMessage(msg.localized)
                return
            }else if let data = self.bookingDetails, data.isRescheduled == 1 ||  data.isRescheduled == 2{
                var msg = ""
                if let id = self.bookingDetails?.schedule_cancel_by, id == AppDelegateConstant.user?.ID{
                    msg = AppDelegateConstant.user?.userType == tutorType ? "Please wait for student to accept/cancel the schedule or end tuition." : "Please wait for tutor to accept/cancel the schedule or end tuition."
                }else{
                    msg = "Please accept/cancel  the schedule or wait for tuition end."
                }
                self.showSimpleAlertWithMessage(msg.localized)
                return
            }
            
        }
        
        configureCalanderView()
    }
    
    @objc func dateChanged(_ sender: UIDatePicker?) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = sender?.date {

             startOfWeek = date.getThisWeekStart()//date.startOfWeek
             endOfWeek = date.getThisWeekEnd()//endOfWeek

        }
    }
    
    @objc func onCancleButtonClick(){
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.view.layoutIfNeeded()
            self.toolBar.removeFromSuperview()
            self.datePicker.removeFromSuperview()
        }) { (completed) in
            self.toolBar.removeFromSuperview()
            self.datePicker.removeFromSuperview()
        }

    }
    
    @objc func onDoneButtonClick() {
        
        /// This bool var is managing wether socket should disable or not
        //shouldDisableSocket = false
    
        toolBar.removeFromSuperview()
        datePicker.removeFromSuperview()
        if let startdate = self.startOfWeek, let lastDate = self.endOfWeek {
            
            let dateRange:[String:Any] = ["startDate" : startdate,
                                                            "endDate" : lastDate]
            let selectAvailibilityViewController = SelectAvailibilityViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
            selectAvailibilityViewController.dateRange = dateRange
            selectAvailibilityViewController.currentLesson = self.currentLesson
            if let vc = self.navigationController?.getPreviousViewController(){
                if vc is EdgemOfferViewController{
                    if AppDelegateConstant.user?.ID == self.makeOfferRequst.offeredByID{
                        selectAvailibilityViewController.userID = self.makeOfferRequst?.offeredToID
                    }else{
                        selectAvailibilityViewController.userID = self.makeOfferRequst?.offeredByID
                    }
                    selectAvailibilityViewController.offerID =  self.makeOfferRequst?.offerID
                }else{
                    if AppDelegateConstant.user?.ID == self.bookingDetails?.offeredByID{
                        selectAvailibilityViewController.userID = self.bookingDetails?.offeredToID
                    }else{
                        selectAvailibilityViewController.userID = self.bookingDetails?.offeredByID
                    }
                     selectAvailibilityViewController.offerID =  self.bookingDetails?.offerID
                }
            }
            self.navigationController?.pushViewController(selectAvailibilityViewController, animated: true)
            
        }
 
    }
    
    /**
       It will sow pop which gives you option to **cancle offer**
  */
    fileprivate func cancleOfferPopUp(_ cancellationMessage: String) {
        let commonPopUP = ShowInfoCommonPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        commonPopUP.fromController = self
        commonPopUP.alertForCounter = false
        commonPopUP.customMsg = cancellationMessage
        self.present(commonPopUP, animated: true, completion: nil)
    }

    //MARK:-  This function will called when counter offer tap on ok button
    /**
     This function will called when counter offer tap on ok button
     */
    @objc func okBtnTapped(_ notification: NSNotification) {
        //counter btn tapped
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
//                self.getMakeOfferData?(self.makeOfferRequst, true)
//                self.navigationController?.popViewController(animated: true)
            }else{
                let edgemOfferVC = EdgemOfferViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
                if let bookingDetail =  self.bookingDetails {
                    
                    edgemOfferVC.makeOfferRequst.ammountPerHour      = bookingDetail.offeredRate
                    edgemOfferVC.makeOfferRequst.hours                          = "\(bookingDetail.spanOf)"
                    edgemOfferVC.makeOfferRequst.lessons                       = "\(bookingDetail.forLessons)"
                    edgemOfferVC.makeOfferRequst.levelID                        =  "\(bookingDetail.levelID)"
                    edgemOfferVC.makeOfferRequst.level                           = bookingDetail.levelName
                    edgemOfferVC.makeOfferRequst.subjects                      = bookingDetail.subjectName
                    edgemOfferVC.makeOfferRequst.subjectID                    = "\(bookingDetail.subjectID)"
                    edgemOfferVC.makeOfferRequst.totalAmmount            = bookingDetail.totalAmmount
                    edgemOfferVC.makeOfferRequst.offerID                        = bookingDetail.offerID
                    edgemOfferVC.makeOfferRequst.lockRate                     = bookingDetail.lockRate
                    edgemOfferVC.tutorId                                                     = "\(bookingDetail.offeredToID)"

                    if let negotiableStatus = UserStore.shared.negotiableStatus{
                         edgemOfferVC.makeOfferRequst.negotiableStatus = negotiableStatus
                    }else{
                        edgemOfferVC.makeOfferRequst.negotiableStatus = 0
                    }
                    edgemOfferVC.isCounterOrEditOffer = "counter"
                }
                edgemOfferVC.isFromViewController = self
                self.navigationController?.pushViewController(edgemOfferVC, animated: true)
            }
        }
    }
    
    fileprivate func editOfferFunction() {
        //edit btn tapped
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                self.getMakeOfferData?(self.makeOfferRequst, true)
                self.setTitle?("Edit Offer".localized, VCNavigationStatus.pop.description())
                self.navigationController?.popViewController(animated: true)
            }else{
                let edgemOfferVC = EdgemOfferViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
                if let bookingDetail =  self.bookingDetails {
                    //edgemOfferVC.makeOfferRequst = bookingDetail
                     //edgemOfferVC.suggestedOffer                                    = bookingDetail.offeredRate
                    edgemOfferVC.makeOfferRequst.ammountPerHour     = bookingDetail.offeredRate
                    edgemOfferVC.makeOfferRequst.hours                         = "\(bookingDetail.spanOf)"
                    edgemOfferVC.makeOfferRequst.lessons                      = "\(bookingDetail.forLessons)"
                    edgemOfferVC.makeOfferRequst.levelID                       =  "\(bookingDetail.levelID)"
                    edgemOfferVC.makeOfferRequst.level                           = bookingDetail.levelName
                    edgemOfferVC.makeOfferRequst.subjects                     = bookingDetail.subjectName
                    edgemOfferVC.makeOfferRequst.subjectID                   = "\(bookingDetail.subjectID)"
                    edgemOfferVC.makeOfferRequst.totalAmmount           = bookingDetail.totalAmmount
                    edgemOfferVC.makeOfferRequst.offerID                       = bookingDetail.offerID
                    edgemOfferVC.tutorId                                                    = "\(bookingDetail.offeredToID)"
                    edgemOfferVC.isCounterOrEditOffer                             = "edit"
                    edgemOfferVC.vcNavigationStatus                                 = VCNavigationStatus.push.description()
                }
                edgemOfferVC.isFromViewController = self
                self.navigationController?.pushViewController(edgemOfferVC, animated: true)
            }
        }
    }
    
    @IBAction func didTapEditCancleOferBtn(_ sender: UIButton) {
        
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
            
                if (AppDelegateConstant.user?.ID == self.makeOfferRequst.offeredByID) {
                    
                    if sender.tag == 1{
                        
                        doCancleOffer()
                        
                    }else{
                        
                        editOfferFunction()
                        
                    }
                }else{

                    if sender.tag == 1{
                      
                        doCancleOffer()
                        
                    }else if sender.tag == 2{
                        // Accept Offer
                        acceptOffer()
                    }else if sender.tag == 3{
                        // Counter offer
                     
                        if let vc = self.navigationController?.getPreviousViewController(){
                            if vc is EdgemOfferViewController{
                                //>>>>
                                self.getMakeOfferData?(self.makeOfferRequst, true)
                                delegate?.changeVCTitle("counter", VCNavigationStatus.pop.description())
                                self.navigationController?.popViewController(animated: true)
                            }else{
                                let edgemOfferVC = EdgemOfferViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
                                if let bookingDetail =  self.bookingDetails {
                                    edgemOfferVC.makeOfferRequst.ammountPerHour   = bookingDetail.offeredRate
                                    edgemOfferVC.makeOfferRequst.hours                       = "\(bookingDetail.spanOf)"
                                    edgemOfferVC.makeOfferRequst.lessons                    = "\(bookingDetail.forLessons)"
                                    edgemOfferVC.makeOfferRequst.levelID                     =  "\(bookingDetail.levelID)"
                                    edgemOfferVC.makeOfferRequst.level                         = bookingDetail.levelName
                                    edgemOfferVC.makeOfferRequst.subjects                   = bookingDetail.subjectName
                                    edgemOfferVC.makeOfferRequst.subjectID                 = "\(bookingDetail.subjectID)"
                                    edgemOfferVC.makeOfferRequst.totalAmmount        = bookingDetail.totalAmmount
                                    edgemOfferVC.makeOfferRequst.offerID                    = bookingDetail.offerID
                                    edgemOfferVC.makeOfferRequst.lockRate                 = bookingDetail.lockRate
                                    edgemOfferVC.tutorId                                                 = "\(bookingDetail.offeredToID)"
                                                                
                                    edgemOfferVC.makeOfferRequst.negotiableStatus = 1
                                    edgemOfferVC.isCounterOrEditOffer = "counter"
                                }
                                edgemOfferVC.isFromViewController = self
                                self.navigationController?.pushViewController(edgemOfferVC, animated: true)
                            }
                        }

                    }
                }
            }else{
    
                if (AppDelegateConstant.user?.ID == self.bookingDetails?.offeredByID) {

                    if sender.tag == 1{
                        doCancleOffer()
                    }else{
                        editOfferFunction()
                    }
                }else{

                    if sender.tag == 1{
                        // Cancle offer
                        doCancleOffer()
                    }else if sender.tag == 2{
                        // Accept Offer
                        acceptOffer()
                    }else if sender.tag == 3{
                        // Counter offer
                        if let vc = self.navigationController?.getPreviousViewController(){
                            if vc is EdgemOfferViewController{
                                //                self.getMakeOfferData?(self.makeOfferRequst, true)
                                //                self.navigationController?.popViewController(animated: true)
                            }else{
                                let edgemOfferVC = EdgemOfferViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
                                if let bookingDetail =  self.bookingDetails {
                                    edgemOfferVC.makeOfferRequst.ammountPerHour     = bookingDetail.offeredRate
                                    edgemOfferVC.makeOfferRequst.hours                         = "\(bookingDetail.spanOf)"
                                    edgemOfferVC.makeOfferRequst.lessons                       = "\(bookingDetail.forLessons)"
                                    edgemOfferVC.makeOfferRequst.levelID                       =  "\(bookingDetail.levelID)"
                                    edgemOfferVC.makeOfferRequst.level                           = bookingDetail.levelName
                                    edgemOfferVC.makeOfferRequst.subjects                     = bookingDetail.subjectName
                                    edgemOfferVC.makeOfferRequst.subjectID                   = "\(bookingDetail.subjectID)"
                                    edgemOfferVC.makeOfferRequst.totalAmmount           = bookingDetail.totalAmmount
                                    edgemOfferVC.makeOfferRequst.offerID                        = bookingDetail.offerID
                                    edgemOfferVC.makeOfferRequst.lockRate                      = bookingDetail.lockRate
                                    edgemOfferVC.tutorId                                                      = "\(bookingDetail.offeredToID)"
   
                                    edgemOfferVC.makeOfferRequst.negotiableStatus = 1
                                    edgemOfferVC.isCounterOrEditOffer = "counter"
                                }
                                edgemOfferVC.isFromViewController = self
                                self.navigationController?.pushViewController(edgemOfferVC, animated: true)
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
    // MARK: ----------- Helping Functions
    
    func moveToReviewVCFromSocket(status: String){
        
        if let vc = self.navigationController?.getPreviousViewController(){
            let leaveReviewVC = LeaveReviewViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
            
            if vc is EdgemOfferViewController{
                leaveReviewVC.oppositeUserID = self.makeOfferRequst?.offeredToID
                leaveReviewVC.offerID = self.makeOfferRequst.offerID
            }else{
                if let rateStatus = self.bookingDetails?.isRated, rateStatus == 0 && status == "done"{
                    leaveReviewVC.oppositeUserID = self.bookingDetails?.offeredToID
                    leaveReviewVC.offerID = self.bookingDetails?.offerID
                }
            }
            self.navigationController?.pushViewController(leaveReviewVC, animated: true)
            
        }
    }
    
    func moveToReviewController(){
        
        // ----> Below code will be used when offer is completed and user have not gieven his feedback/review
        //[START ]
        if let isRated = self.bookingDetails?.isRated, let status = self.bookingDetails?.status, isRated == 0 && status == "done"{
            if let vc = self.navigationController?.getPreviousViewController(){
                let leaveReviewVC = LeaveReviewViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
                
                if vc is EdgemOfferViewController{
                    leaveReviewVC.oppositeUserID = self.makeOfferRequst?.offeredToID
                    leaveReviewVC.offerID = self.makeOfferRequst.offerID
                }else{
                    leaveReviewVC.oppositeUserID = self.bookingDetails?.offeredToID
                    leaveReviewVC.offerID = self.bookingDetails?.offerID
                }
                self.navigationController?.pushViewController(leaveReviewVC, animated: true)
                
            }
        }
        //[END ]
    }

    fileprivate func loadImages(_ bookingDetails: BookingDetails) {
        
        if AppDelegateConstant.user?.ID == bookingDetails.offeredByID{
            
            let imageURL = bookingDetails.offeredToImage
            if !imageURL.isEmpty{
                GetImageFromServer.getImage(imageURL: imageURL) { (image) in
                    self.userImage.image = image
                }
            }
            
        }else{
            let imageURL = bookingDetails.offeredByImage
            if !imageURL.isEmpty{
                GetImageFromServer.getImage(imageURL: imageURL) { (image) in
                     self.userImage.image = image
                }
            }
        }
        
    }
    
    fileprivate func loadImagesFromMakeOffer(_ makeOfferRequest: MakeOfferRequst) {

        if AppDelegateConstant.user?.ID == makeOfferRequest.offeredByID{

            guard let imageURL = makeOfferRequest.offeredToImage else{return}
            if !imageURL.isEmpty{
                GetImageFromServer.getImage(imageURL: imageURL) { (image) in
                    self.userImage.image = image
                }
            }

        }else{
            guard let imageURL = makeOfferRequest.offeredByImage else{return}
            if !imageURL.isEmpty{
                GetImageFromServer.getImage(imageURL: imageURL) { (image) in
                    self.userImage.image = image
                }
            }
        }

    }
    
    fileprivate func loadImages(_ profileImageURL: String) {

        let imageProvider = ImageProvider()
        
        if let image = imageCache.image(forKey: profileImageURL){
            self.userImage.image = image
        }else{
            
            self.userImage.image = UIImage(named: "femaleIcon")
            
            if let url = URL(string: profileImageURL) {
                imageProvider.requestImage(from: url) { (image) in
                    imageCache.add(image, forKey: profileImageURL)
                    self.userImage.image = image
                }
            }
        }
        
    }
    
    
    /// This function is supposed to be used to animate progress bar appering on the top of view
    fileprivate func animateCollectionView() {
        UIView.animate(withDuration: 5, delay: 1, options: .curveLinear, animations: {
            
            if let pos = self.currentPosition{
                let indexPath = IndexPath(row: pos, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: [.centeredVertically,   .centeredHorizontally], animated: true)
            }
            
        },completion: { finished in
            
        })
    }
    
    /// UnUsed Function. Use When Needed
    func yesBtnTappedFromCommonPopVC(){
        print("yes byn tapped")
    }
    
    
    // MARK: ----------- EndTutionBtnTableViewCell Function
    func didTappedEndTutionButton(_ status: Bool, _ msgID: String, _ cell: EndTutionBtnTableViewCell, _ row: Int, _ section: Int){
        if status == true{
            endTution(msgID, cell, row, section)
        }
    }
    
     // MARK: ----------- ArrivedButtonTableViewCelll Function
    func didTappedArriveButton(_ status: Bool, _ msgID: String, _ cell: ArrivedBtnTableViewCell, _ row: Int, _ section: Int){
        if status == true{
            userHaveArrived(msgID,cell,row, section)
        }
    }
    
    // MARK: ----------- AcceptCancleTableViewCell Function
    
    func currentRowAndSection(_ currentSection: Int, _ currentRow: Int){
        print("row \(currentRow) section \(currentSection)")
        self.currentSection = currentSection
        self.currentRow = currentRow
    }
    
    func stopActivityController(){
        
        if let row = currentRow, let section = currentSection {
            let indexPath  = IndexPath(row: row, section: section)
            guard let cell = tableView.cellForRow(at: indexPath) as? RightImageTableViewCell else{
                print("cell not found")
                return
            }
            cell.activityIndicator.stopAnimating()
        }
    }
    
    /// This function will be executed after tapping Accept/Cancle button in Chat Room
    func didTapAcceptCancleBtn(_ sender: UIButton, _ msgID: String, _ cell: AcceptCancleTableViewCell, _ section: Int, _ row: Int){
        if sender.tag == 1 {
            // call accept API
            manageSchedule("\(1)", msgID,cell,section: section,row: row)
        }else{
            // call cancle API
            manageSchedule("\(0)", msgID, cell, section: section, row: row)
        }
    }
    
    fileprivate func calanderAndChatVisibilityStatus() {
        
        self.calenderBtn.isHidden = (self.calanderVisibilityStatus == true) ? false : true

    }

    // MARK: - This function will be called after successful payment
    func makeOfferDataFromBottomPopUP(makeOfferRequest: MakeOfferRequst, payBtnRow: Int, payBtnSection: Int) {
        
        // map BookingDetails class properties with MakeOfferRequst
        if let bookingDetails = self.bookingDetails{
            bookingDetails.offerID = makeOfferRequest.offerID ?? 0
            bookingDetails.subjectID =  Int(makeOfferRequest.subjectID ?? "0") ?? 0
            bookingDetails.subjectName =  makeOfferRequest.subjects ?? ""
            bookingDetails.offeredByID =  makeOfferRequest.offeredByID ?? 0
            bookingDetails.offeredByName = makeOfferRequest.offeredByName ?? ""
            bookingDetails.offeredToID =  makeOfferRequest.offeredToID ?? 0
            bookingDetails.offeredToName =  makeOfferRequest.offeredToName ?? ""
            bookingDetails.offeredRate = makeOfferRequest.lockRate ?? ""
            bookingDetails.spanOf =  Double(makeOfferRequest.hours ?? "0") ?? 0
            bookingDetails.forLessons = Int(makeOfferRequest.lessons ?? "0") ?? 0
            bookingDetails.totalAmmount =  makeOfferRequest.totalAmmount ?? ""
            bookingDetails.status =  makeOfferRequest.status ?? ""
        }else{
            self.makeOfferRequst = makeOfferRequest
        }

        PaymentStatus.shared.isPaymentDone = true
        
        calanderVisibilityStatus = true
        self.calanderAndChatVisibilityStatus()
        let indexPath = IndexPath(row: payBtnRow, section: payBtnSection)
        
        self.chatMessages[payBtnSection][payBtnRow].buttonFlag = 1
        
        tableView.reloadRows(at: [indexPath], with: .none)

    }
    
    func sendToAddCards(){
        print("send to add cards")
        let addNewCardVC = AddNewCardViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        addNewCardVC.addNewCardViewControllerDelegate = self
        self.navigationController?.pushViewController(addNewCardVC, animated: true)
    }
    
    func scrollToBottom()  {
        let point = CGPoint(x: 0, y: self.tableView.contentSize.height + self.tableView.contentInset.bottom - self.tableView.frame.height)
        if point.y >= 0{
            self.tableView.setContentOffset(point, animated: true)
        }
    }
    
    fileprivate func getNextLesson(_ currentLesson: String) -> String{
        lastLesson = "\(self.progressBarData.count-4)lesson"
        switch currentLesson {
            
        case "1lesson":   return "2lesson"
            
        case "2lesson":   return "3lesson"
            
        case "3lesson":    return "4lesson"
            
        case "4lesson":
                                    if lastLesson == "4lesson"{
                                      return "done"
                                    }
                                    return "5lesson"
            
        case "5lesson":   return "6lesson"
            
        case "6lesson":
                                    if lastLesson == "6lesson"{
                                        return "done"
                                    }
                                    return "7lesson"
            
        case "7lesson":   return "8lesson"
            
        case "8lesson":    return "done"
            
        default:                 return ""
        }
    }
    
    fileprivate func disableChatOnOfferCompleted(_ status: String){
        
        if status == "done".localized{
            self.disableChat(status)
        }
    }
    
    fileprivate func getProgressBarCurrentStatus(_ status: String){
      
        print(status)
         var pos = 0
         var isOnCompletedPos = false
        switch status{
        case "Open":                    pos = 0
        
        case "Offered":                 pos = 1

        case "PaymentPending":  pos = 2
          
        case "1lesson":                 pos = 3

        case "2lesson":                 pos = 4

        case "3lesson":                 pos = 5

        case "4lesson":                 pos = 6

        case "5lesson":                 pos = 7

        case "6lesson":                 pos = 8

        case "7lesson":                 pos = 9

        case "8lesson":                 pos = 10

        case "done":

                                                if let hours = self.bookingDetails?.forLessons{
                                                    if "\(hours)" == "4"{
                                                        pos = 7
                                                        isOnCompletedPos = true
                                                    }else if "\(hours)" == "6" {
                                                        pos = 9
                                                        isOnCompletedPos = true
                                                    }else if "\(hours)" == "8" {
                                                        pos = 11
                                                        isOnCompletedPos = true
                                                    }
                                                }
        default:
            break
        }
        
        currentLesson = status
        currentPosition = pos
        
        if (pos >= 2){
            editCancleHeight.constant = 0
            editCacleOfferContainerView.isHidden = true
        }else{
            editCancleHeight.constant = 90
            editCacleOfferContainerView.isHidden = false
        }
        
        if pos > 2 {
            self.calanderVisibilityStatus = true
           // self.messageTextView.isUserInteractionEnabled = true
           // self.sendMessageBtn.isUserInteractionEnabled = true
            PaymentStatus.shared.isPaymentDone = true
        }else{
            self.calanderVisibilityStatus = false
           // self.messageTextView.isUserInteractionEnabled = false
            //self.sendMessageBtn.isUserInteractionEnabled = false
             PaymentStatus.shared.isPaymentDone = false
        }
        
        self.calanderAndChatVisibilityStatus()
        
        for index in 0..<progressBarData.count{
            if index < pos{
                progressBarData[index].setStatus(progressBarStatus.active.rawValue)
            }else if index > pos{
                progressBarData[index].setStatus(progressBarStatus.in_Active.rawValue)
            }else if index == pos && isOnCompletedPos == true{
                progressBarData[index].setStatus(progressBarStatus.completed.rawValue)
            }else{
                progressBarData[index].setStatus(progressBarStatus.partially_active.rawValue)
            }
        }
        
        self.collectionView.reloadData()
       // collectionView?.scrollToItem(at: IndexPath(item: pos, section: 0), at: UICollectionView.ScrollPosition.right, animated: false)
      
    }
    
     /// This function will be used to disconnect app with socket and listen events from socket
    @objc func disconnectToSocketChannel(){
        
        let token = UserStore.shared.token
        var offerID: Int = 0
        if let vc = self.navigationController?.getPreviousViewController(){

            if vc is EdgemOfferViewController{
                guard let offerId = self.makeOfferRequst?.offerID else{return}
                offerID = offerId
            }else{
                guard let offerId = self.bookingDetails?.offerID else{return}
                offerID = offerId
            }

        }else{
            if let data = self.makeOfferRequst{
                guard let offerId = data.offerID else{return}
                offerID = offerId
            }else if let data = self.bookingDetails{
                offerID = data.offerID
            }
        }

        let headers = [ "Authorization":  "Bearer " + token,
                        "User": String(UserStore.shared.userID ?? 0),
                        "Offer" : String(offerID)]

        let tempHeader = ["headers":headers]


        if offerID == 0{
            print("offer id not found")
            return
        }
        let subscribeData = ["channel": "private-offer.\(String(describing: offerID))",
            "name": "subscribe",
            "auth": tempHeader] as [String : Any]

        #if DEBUG
        do {
            let json = try JSONSerialization.data(withJSONObject: subscribeData, options: [])
            let string1 = String(data: json, encoding: String.Encoding.utf8) ?? "Data could not be printed"
            print(string1)
        }catch{
            print(error.localizedDescription)
        }
        #endif

        unSubscribeChat(subscribeData)
        AppDelegateConstant.socket.disconnect()
    }
    
    /// This function will be used to connect app with socket and listen events from socket
    @objc func connectToSocketChannel(){

        let token = UserStore.shared.token
        var offerID: Int = 0
        if let vc = self.navigationController?.getPreviousViewController(){
            
            if vc is EdgemOfferViewController{
                guard let offerId = self.makeOfferRequst?.offerID else{return}
                offerID = offerId
            }else{
                guard let offerId = self.bookingDetails?.offerID else{return}
                offerID = offerId
            }
            
        }
        
        let headers = [ "Authorization":  "Bearer " + token,
                                "User": String(UserStore.shared.userID ?? 0),
                                "Offer" : String(offerID)]
        
        let tempHeader = ["headers":headers]
       

        if offerID == 0{
            print("offer id not found")
            return
        }
        let subscribeData = ["channel": "private-offer.\(String(describing: offerID))",
            "name": "subscribe",
            "auth": tempHeader] as [String : Any]
        
         #if DEBUG
        do {
            let json = try JSONSerialization.data(withJSONObject: subscribeData, options: [])
            let string1 = String(data: json, encoding: String.Encoding.utf8) ?? "Data could not be printed"
             print(string1)
        }catch{
            print(error.localizedDescription)
        }
      #endif

          subscribeChat(subscribeData)
         AppDelegateConstant.socket.connect()
    }
    
    private func unSubscribeChat(_ subscribeData: [String : Any]){
        AppDelegateConstant.socket.emitWithAck("unsubscribe", subscribeData).timingOut(after: 2) { (data) in
              print("<<<<<<< unsubscribed >>>>>>>")
            
             AppDelegateConstant.socket.off("scheduleAccept")
             AppDelegateConstant.socket.off("scheduleCancel")
             AppDelegateConstant.socket.off("scheduleMade")
             AppDelegateConstant.socket.off("offerAccepted")
             AppDelegateConstant.socket.off("offerEdit")
             AppDelegateConstant.socket.off("offerCounter")
             AppDelegateConstant.socket.off("offerDenied")
             AppDelegateConstant.socket.off("progressBar")
             AppDelegateConstant.socket.off("lastMessage")
        }
    }
    
    private func subscribeChat(_ subscribeData: [String : Any]){
        
        AppDelegateConstant.socket.emitWithAck("subscribe", subscribeData).timingOut(after: 2, callback: { (data) in
            print("<<<<<<<<<<< subscribed >>>>>>>>")
            
            
            AppDelegateConstant.socket.on("scheduleAccept") {data, ack in
                print(value: "inside scheduleAccept")
                if let tempData = (data[1] as? [String:Any]), let data = tempData["0"] as? [String:Any] {
                    
                    PrintJSONOnConsole.printJSON(dict: data as [String : AnyObject])
                    
                    if let vc = self.navigationController?.getPreviousViewController(){
                        
                        if vc is EdgemOfferViewController{
                            self.makeOfferRequst = MakeOfferRequst(with: data)
                        }else{
                            self.bookingDetails = BookingDetails(booking: data as [String:AnyObject])
                        }
                    }else{
                        if let _ = self.makeOfferRequst{
                            self.makeOfferRequst = MakeOfferRequst(with: data)
                        }else if let _ = self.bookingDetails{
                            self.bookingDetails = BookingDetails(booking: data as [String:AnyObject])
                        }
                    }
                     self.enableChatBefore24HourOfFirstLesonScheduling()
                }
            }
            
            AppDelegateConstant.socket.on("scheduleCancel") {data, ack in
                print(value: "inside scheduleCancel")
                
                if let tempData = (data[1] as? [String:Any]), let data = tempData["0"] as? [String:Any] {
                    
                    PrintJSONOnConsole.printJSON(dict: data as [String : AnyObject])
                    
                    //print(self.messagesFromServer?[(self.messagesFromServer?.count)!-2].conversationMessage)
                    self.messagesFromServer?[(self.messagesFromServer?.count)!-2].buttonFlag = 1
                    self.messagesFromServer?[(self.messagesFromServer?.count)!-1].buttonFlag = 1
                    if let vc = self.navigationController?.getPreviousViewController(){
                        
                        if vc is EdgemOfferViewController{
                            self.makeOfferRequst = MakeOfferRequst(with: data)
                        }else{
                            self.bookingDetails = BookingDetails(booking: data as [String:AnyObject])
                        }
                        
                    }else{
                        if let _ = self.makeOfferRequst{
                            self.makeOfferRequst = MakeOfferRequst(with: data)
                        }else if let _ = self.bookingDetails{
                            self.bookingDetails = BookingDetails(booking: data as [String:AnyObject])
                        }
                    }
                    self.tableView.reloadData()
                    self.enableChatBefore24HourOfFirstLesonScheduling()
                }
            }
            
            AppDelegateConstant.socket.on("scheduleMade") {data, ack in
                print(value: "inside scheduleCancel")
                
                if let tempData = (data[1] as? [String:Any]), let data = tempData["0"] as? [String:Any] {
                    
                    PrintJSONOnConsole.printJSON(dict: data as [String : AnyObject])
                    ///some time navigation controller is returning nil
                    if let vc = self.navigationController?.getPreviousViewController(){
                        
                        if vc is EdgemOfferViewController{
                            self.makeOfferRequst = MakeOfferRequst(with: data)
                        }else{
                            self.bookingDetails = BookingDetails(booking: data as [String:AnyObject])
                        }
                        
                    }else{
                        if let _ = self.makeOfferRequst{
                            self.makeOfferRequst = MakeOfferRequst(with: data)
                        }else if let _ = self.bookingDetails{
                            self.bookingDetails = BookingDetails(booking: data as [String:AnyObject])
                        }
                    }
                }
            }
            
            AppDelegateConstant.socket.on("offerAccepted") {data, ack in
                
                print("==offerAccepted=====")
                if let tempData = (data[1] as? [String:AnyObject]), let status = tempData["offer_status"] as? String{
                    
                    PrintJSONOnConsole.printJSON(dict: tempData as [String : AnyObject])
                    
                    self.editCancleHeight.constant =  0
                    self.editCacleOfferContainerView.isHidden =  true
                }
            }
            
            
            AppDelegateConstant.socket.on("offerEdit") {data, ack in
                
                if let tempData = (data[1] as? [String:Any]), let data = tempData["0"] as? [String:Any] {
                    
                    print(value: "=====inside offerEdit event=====")
                    
                    PrintJSONOnConsole.printJSON(dict: data as [String : AnyObject])
                    
                    var lessons = ""
                    self.collectionViewRowCount = 4
                    if let vc = self.navigationController?.getPreviousViewController(){
                        
                        if vc is EdgemOfferViewController{
                            self.makeOfferRequst = MakeOfferRequst(with: data)
                            
                            // ============= Count Total Lessons ============
                            if let lessons = self.makeOfferRequst.lessons, let _totalLessons = Int(lessons){
                                self.totalLessons = _totalLessons
                            }
                            if let _lesson = self.makeOfferRequst.lessons {
                                lessons = _lesson
                            }
                            
                        }else{
                            self.bookingDetails = BookingDetails(booking: data as [String:AnyObject])
                            
                            // ============= Count Total Lessons ============
                            if let lesson = self.bookingDetails?.forLessons {
                                self.totalLessons = lesson
                            }
                            if let _lesson = self.bookingDetails?.forLessons {
                                lessons = "\(_lesson)"
                            }
                            
                        }
                        
                        self.collectionViewRowCount = self.collectionViewRowCount + self.totalLessons // ---> number of row in collection view
                        
                        self.configureProgressBarData()
                        
                        if vc is EdgemOfferViewController{
                            if let status = self.makeOfferRequst.status{
                                self.disableChatOnOfferCompleted(status)
                                self.getProgressBarCurrentStatus(status)
                            }
                        }else{
                            if let status = self.bookingDetails?.status{
                                self.disableChatOnOfferCompleted(status)
                                self.getProgressBarCurrentStatus(status)
                            }
                        }
                        
                    }
                    
                }
            }
            
            
            AppDelegateConstant.socket.on("offerCounter") {data, ack in
                
                if let tempData = (data[1] as? [String:Any]), let data = tempData["0"] as? [String:Any] {
                    
                    print(value: "=====inside offerCounter event=====")
                    PrintJSONOnConsole.printJSON(dict: data as [String : AnyObject])
                    
                    if let vc = self.navigationController?.getPreviousViewController(){
                        
                        if vc is EdgemOfferViewController{
                            self.makeOfferRequst = MakeOfferRequst(with: data)
                        }else{
                            self.bookingDetails = BookingDetails(booking: data as [String:AnyObject])
                        }
                        
                        self.moveToReviewController()
                        self.initialSetUP()
                        self.showTotalButtons()
                        
                        var lessons = ""
                        
                        if let vc = self.navigationController?.getPreviousViewController(){
                            
                            if vc is EdgemOfferViewController{
                                
                                // ============= Count Total Lessons ============
                                if let lessons = self.makeOfferRequst.lessons, let _totalLessons = Int(lessons){
                                    self.totalLessons = _totalLessons
                                }
                                if let _lesson = self.makeOfferRequst.lessons {
                                    lessons = _lesson
                                }
                                
                            }else{
                                
                                // ============= Count Total Lessons ============
                                if let lesson = self.bookingDetails?.forLessons {
                                    self.totalLessons = lesson
                                }
                                if let _lesson = self.bookingDetails?.forLessons {
                                    lessons = "\(_lesson)"
                                }
                                
                                if self.bookingDetails?.isCancelled == 1{
                                    
                                    self.activitiesOnCancleOffer()
                                }
                                
                            }
                        }
                        
                        
                        self.enableChatBefore24HourOfFirstLesonScheduling()
                        
                        self.collectionViewRowCount = self.collectionViewRowCount + self.totalLessons // ---> number of row in collection view
                        
                        self.configureProgressBarData()
                        
                        if vc is EdgemOfferViewController{
                            if let status = self.makeOfferRequst.status{
                                self.disableChatOnOfferCompleted(status)
                                self.getProgressBarCurrentStatus(status)
                            }
                        }else{
                            if let status = self.bookingDetails?.status{
                                self.disableChatOnOfferCompleted(status)
                                self.getProgressBarCurrentStatus(status)
                            }
                        }
                        
                    }
                    print(data)
                }
            }
            
            AppDelegateConstant.socket.on("offerDenied") {data, ack in
                print(value: "=====inside offerDenied event=====")
                
                if let tempData = (data[1] as? [String:AnyObject]), let status = tempData["offer_status"] as? String{
                    
                    PrintJSONOnConsole.printJSON(dict: tempData as [String : AnyObject])
                    print(status)
                    if status == "denied"{
                        self.activitiesOnCancleOffer()
                    }
                }
                
                
            }
            
            AppDelegateConstant.socket.on("progressBar") {data, ack in
                
                if let tempData = (data[1] as? [String:AnyObject]), let indexData = tempData["0"] as? [String:AnyObject], let status = indexData["status"] as? String{
                    print("progress bar status: \(status)")
                    self.disableChatOnOfferCompleted(status)
                    self.getProgressBarCurrentStatus(status)
                    if status.contains("lesson") && status != "1lesson"{
                        self.enableCaht()
                         }
                    if status == "done"{
                        if let _ = self.makeOfferRequst{
                            self.makeOfferRequst.status = "done"
                        }else if let _ = self.bookingDetails{
                            self.bookingDetails?.status = "done"
                        }
                        self.moveToReviewVCFromSocket(status: status)
                    }
                }
                
            }
            
            
            AppDelegateConstant.socket.on("lastMessage") {data, ack in
                
                print(value: "lastMessage: \(data)")
                
                self.newMessage.removeAll()  //delete
                
                let tempDict = (data[1] as? NSDictionary)?.object(forKey: "0") as? NSDictionary ?? NSDictionary()
                
                if let data = (data[1] as? NSDictionary), let tempData = data.object(forKey: "0") as? NSDictionary{
                    
                    PrintJSONOnConsole.printJSON(dict: tempData as! [String : AnyObject] )
                    
                    if tempDict.count > 0{
                        
                        if let userID = UserStore.shared.userID, let senderID = tempDict["sender_id"] as? Int {
                            
                            if var msgType = tempDict["message_type"] as? Int{
                                
                                if (userID == senderID) && (msgType == MessageType.MESSAGE_TYPE_MIME || msgType == MessageType.MESSAGE_TYPE_TEXT || msgType == MessageType.MESSAGE_TYPE_DOCUMENT){
                                    // Do not append message
                                }else{
                                    
                                    if msgType == MessageType.MESSAGE_TYPE_MAKE_EDIT_OFFER{
                                        msgType = MessageType.MESSAGE_TYPE_TEXT
                                    }
                                    
                                     //[START - This block is used to hide accept/cancel button ]
                                    if (msgType == MessageType.MESSAGE_TYPE_PAY_BUTTON){
                                        self.editCancleHeight.constant =  0
                                        self.editCacleOfferContainerView.isHidden =  true
                                    }
                                     //[END - This block is used to hide accept/cancel button ]

                                    let newMessage = ChatMessage(responseDict: tempData as NSDictionary)
                                    self.FromServer = false
                                    //self.attempToAssembleMessageFromSocket(newMessage)
                                    ///----------
                                    //[START - This block is used yo identify the user user who cancel offer ]
                                    if (msgType == MessageType.MESSAGE_TYPE_OFFER_CANCEL) || ((msgType == MessageType.MESSAGE_TYPE_SCHEDULE_REQ)){
                                        if let vc = self.navigationController?.getPreviousViewController(){
                                            
                                            if vc is EdgemOfferViewController{
                                                if let id = tempDict["sender_id"] as? Int{
                                                    self.makeOfferRequst.schedule_cancel_by = id
                                                }
                                            }else{
                                                if let id = tempDict["sender_id"] as? Int{
                                                    self.bookingDetails?.schedule_cancel_by = id
                                                }
                                            }
                                            
                                        }else{
                                            if let _ = self.makeOfferRequst{
                                                if let id = tempDict["sender_id"] as? Int{
                                                    self.makeOfferRequst.schedule_cancel_by = id
                                                }
                                            }else if let _ = self.bookingDetails{
                                                if let id = tempDict["sender_id"] as? Int{
                                                    self.bookingDetails?.schedule_cancel_by = id
                                                }
                                            }
                                        }
                                    }
                                    //[END - This block is used yo identify the user user who cancel offer ]
                                    
                                   //[START - This block is used to enable calender after END TUITION]
                                    if msgType == MessageType.MESSAGE_TYPE_TUTION_HAS_ENDED{
                                        
                                        if let vc = self.navigationController?.getPreviousViewController(){
                                            
                                            if vc is EdgemOfferViewController{
                                                self.makeOfferRequst.isRescheduled = 0
                                            }else{
                                                self.bookingDetails?.isRescheduled = 0
                                            }
                                            
                                        }else{
                                            if let _ = self.makeOfferRequst{
                                                self.makeOfferRequst.isRescheduled = 0
                                            }else if let _ = self.bookingDetails{
                                                self.bookingDetails?.isRescheduled = 0
                                            }
                                        }
                                        
                                    }
                                    //[END - This block is used to enable calender after END TUITION]
                                 
                                    if AppDelegateConstant.user?.userType == "Tutor" {
                                        
                                        //serverMsgs.forEach { (msgs) in
                                        if newMessage.msgType == MessageType.MESSAGE_TYPE_PAY_BUTTON || newMessage.msgType == MessageType.MESSAGE_TYPE_FINISH_BUTTON || newMessage.msgType == MessageType.MESSAGE_TYPE_TUTOR_HAS_ARRIVED{
                                            // do not append
                                        }else{
                                            //messagesToAppend.append(msgs)
                                            self.attempToAssembleMessageFromSocket(newMessage)
                                        }
                                        //   }
                                        
                                    }else{
                                        
                                        //serverMsgs.forEach { (msgs) in
                                        if newMessage.msgType == MessageType.MESSAGE_TYPE_ADDRESS_ALERT || newMessage.msgType == MessageType.MESSAGE_TYPE_ARRIVED_BUTTON || newMessage.msgType == MessageType.MESSAGE_TYPE_TUTION_HAS_ENDED{
                                            // do not append
                                        }else{
                                            //messagesToAppend.append(msgs)
                                            if newMessage.msgType == MessageType.MESSAGE_TYPE_PAY_BUTTON{
                                                print("pay button clicked")
                                            }
                                            self.attempToAssembleMessageFromSocket(newMessage)
                                        }
                                        // }
                                        //self.attempToAssembleMessageFromSocket(newMessage)
                                    }
                                }
                            }
                            
                        }
                        
                    }
                }
                
            }
            
            
        })
    }
    
    fileprivate func sendMessageWithImagesOrDoc(imageData: Data) {
        self.newMessage.removeAll()
        self.sendMessageWithImage(imageData: imageData)
        
        var tempDict = [String:Any]()
        if let image = UIImage(data: imageData) {
            
            tempDict = ["conversation_message":"", "sender_id":UserStore.shared.userID ?? 0, "sended_on": Date().toString(dateFormat: "yyyy-MM-dd HH:mm:ss"), "doc_image": image, "message_type" :MessageType.MESSAGE_TYPE_MIME, "imageHeight" : image.size.height, "imageWidth" : image.size.width] as [String : AnyObject]
            let newMessage = ChatMessage(responseDict: tempDict as NSDictionary)
            self.messagesFromServer?.append(newMessage)
//            self.attemptToAssembleGroupedMessages()
            self.newMessage.append(newMessage)
            attemptToAssembleNewMessageInGroupedMessages( self.newMessage)
            
        }

    }
    
    fileprivate func sendMessageWithText(_ message: String){
        self.newMessage.removeAll()
        self.sendMessage(message)
        
        var tempDict = [String:Any]()
        tempDict = ["conversation_message":message, "sender_id":UserStore.shared.userID ?? 0, "sended_on": Date().toString(dateFormat: "yyyy-MM-dd HH:mm:ss"), "viewType": ViewType.messageView.description(), "message_type":ViewType.messageView.description()] as [String : AnyObject]
        let newMessage = ChatMessage(responseDict: tempDict as NSDictionary)
         self.FromServer = false
         self.messagesFromServer?.append(newMessage)
         //self.attemptToAssembleGroupedMessages()
        self.newMessage.append(newMessage)
        attemptToAssembleNewMessageInGroupedMessages( self.newMessage)
        messageTextView.text = ""
       // self.view.endEditing(true)
        
    }
    
    fileprivate func configureProgressBarData() {
        progressBarData = [ProgressBarData(label: "Open", status: progressBarStatus.active.rawValue),
                                        ProgressBarData(label: "Offered", status: progressBarStatus.partially_active.rawValue),
                                        ProgressBarData(label: "Payment", status: progressBarStatus.in_Active.rawValue)
                                        ]
        
        if self.totalLessons > 0 {
            
            for index in 1...self.totalLessons{
                switch index{
                    
                case 1 : progressBarData.append(ProgressBarData(label: "\(index)st \n Lesson", status: progressBarStatus.in_Active.rawValue))
                    
                case 2 : progressBarData.append(ProgressBarData(label: "\(index)nd \n Lesson", status: progressBarStatus.in_Active.rawValue))
                    
                case 3 : progressBarData.append(ProgressBarData(label: "\(index)rd \n Lesson", status: progressBarStatus.in_Active.rawValue))
                    
                default:
                    progressBarData.append(ProgressBarData(label: "\(index)th \n Lesson", status: progressBarStatus.in_Active.rawValue))
                }
            }
            
            progressBarData.append(ProgressBarData(label: "Completed", status: progressBarStatus.in_Active.rawValue))
        }

        
    }
    
    private func showTotalButtons() {
        
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                if (AppDelegateConstant.user?.ID == self.makeOfferRequst.offeredByID) {
                    counterBtnContainerView.isHidden = true
                    cancleBtn.setTitle("Cancel".localized, for: .normal)
                    editOfferBtn.setTitle("Edit Offer".localized, for: .normal)
                }else{
                    counterBtnContainerView.isHidden = false
                    cancleBtn.setTitle("Reject".localized, for: .normal)
                    editOfferBtn.setTitle("Accept".localized, for: .normal)
                    counterOfferBtn.setTitle("Counter Offer".localized, for: .normal)
                }
            }else{
                if (AppDelegateConstant.user?.ID == self.bookingDetails?.offeredByID) {
                    counterBtnContainerView.isHidden = true
                    cancleBtn.setTitle("Cancel".localized, for: .normal)
                    editOfferBtn.setTitle("Edit Offer".localized, for: .normal)
                }else{
                    counterBtnContainerView.isHidden = false
                    cancleBtn.setTitle("Reject".localized, for: .normal)
                    editOfferBtn.setTitle("Accept".localized, for: .normal)
                    counterOfferBtn.setTitle("Counter Offer".localized, for: .normal)
                }
            }
        }
        
    }
    
    /**
     It will be called when you **confirm** to cancel offer.
    */
    @objc func yesButtonTapped(){
        
        if cancelOfferBefore24Hr == true{
            // ---> offer cancelled before 24 hrs
            self.cancleOffer(before24hrs: true)
            
        }else if cancelOfferBefore24Hr == false{
            // ---> offer cancelled after 24 hrs
            self.cancleOffer(before24hrs: false)
            
        }
        
    }
    
    @objc func tutorHasArrived(){
       // userHaveArrived(messageIDOnTutorArrived)
    }
   
    
    @objc func userConnected(){
      
        if let vc = self.navigationController?.getPreviousViewController(){
            let userId:Int?
            if vc is EdgemOfferViewController{
                if AppDelegateConstant.user?.ID == self.makeOfferRequst.offeredByID{
                   userId = self.makeOfferRequst?.offeredToID
                }else{
                    userId = self.makeOfferRequst?.offeredByID
                }
                
            }else{
                if AppDelegateConstant.user?.ID == self.bookingDetails?.offeredByID{
                   userId = self.bookingDetails?.offeredToID
                }else{
                  userId = self.bookingDetails?.offeredByID
                }
                
            }
            
            if let id = userId{
                let image = (Global.idArrays.contains(id)) ? "user_online" : "user_offline"
                onlineStatusLabel.text = (Global.idArrays.contains(id)) ? "Online" : "Offline"
                self.onlineStatus.image = UIImage(named: image)
            }
        }
        
    }
    
    fileprivate func attemptToAssembleNewMessageInGroupedMessages(_ newMessage: [ChatMessage]) {

        var messagesToAppend = [ChatMessage]()

        if AppDelegateConstant.user?.userType == "Tutor" {

            newMessage.forEach { (msgs) in
                if msgs.msgType == MessageType.MESSAGE_TYPE_PAY_BUTTON ||   msgs.msgType == MessageType.MESSAGE_TYPE_FINISH_BUTTON || msgs.msgType == MessageType.MESSAGE_TYPE_TUTOR_HAS_ARRIVED{
                    // do not append
                }else{
                    messagesToAppend.append(msgs)
                }
               // messagesToAppend.append(msgs)
            }

        }else{

            newMessage.forEach { (msgs) in
                if msgs.msgType == MessageType.MESSAGE_TYPE_ADDRESS_ALERT || msgs.msgType == MessageType.MESSAGE_TYPE_ARRIVED_BUTTON || msgs.msgType == MessageType.MESSAGE_TYPE_TUTION_HAS_ENDED{
                    // do not append
                }else{
                    messagesToAppend.append(msgs)
                }
                 //messagesToAppend.append(msgs)
            }
        }

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: messagesToAppend.last?.sendedOn ?? "")

        let newDate = date?.reduceToMonthDayYear() ?? Date()
        print(newDate)

        let lastMessageDateStr = self.chatMessages[chatMessages.count-1][self.chatMessages[chatMessages.count-1].count - 1].sendedOn
        let lastMessageDate = dateFormatter.date(from: lastMessageDateStr ?? "")
        let newlastMessageDate = lastMessageDate?.reduceToMonthDayYear() ?? Date()

        print(newlastMessageDate)


        let section = self.chatMessages.count-1

        if newlastMessageDate == newDate{
            messagesToAppend.forEach { (chatMsg) in
                 self.chatMessages[section].append(chatMsg)
                let row = self.chatMessages[section].count - 1
                let indexPath = IndexPath(row: row, section: section)
                self.tableView.insertRows(at: [indexPath], with: .automatic)
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        }else{
                self.chatMessages.append(messagesToAppend)
                self.tableView.reloadData()
        }

        if let indexPath = self.getScrollPosition() {
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
      //  self.chatMessages[section].append(newMessage[0])
//        let row = self.chatMessages[section].count - 1
//        let indexPath = IndexPath(row: row, section: section)
//        self.tableView.insertRows(at: [indexPath], with: .automatic)
//        self.tableView.reloadRows(at: [indexPath], with: .automatic)

    }

    
    fileprivate func attempToAssembleMessageFromSocket(_ socketMessage: ChatMessage) {
        
        //======
        if socketMessage.msgType == MessageType.MESSAGE_TYPE_COUNTER_OFFER{
            
            if let vc = self.navigationController?.getPreviousViewController(){
                
                if vc is EdgemOfferViewController{
                    
                    var temp = 0
                    
                    if let offerByID = makeOfferRequst.offeredByID, let offerToID = makeOfferRequst.offeredToID{
                        temp = offerByID
                        
                        self.makeOfferRequst.offeredByID = offerToID
                        self.makeOfferRequst.offeredToID = temp
                    }
                    
                }else{
                    
                    var temp = 0
                    
                    if let offerByID = bookingDetails?.offeredByID, let offerToID = bookingDetails?.offeredToID{
                        temp = offerByID
                        
                        self.bookingDetails?.offeredByID = offerToID
                        self.bookingDetails?.offeredToID = temp
                    }
                    
                }
                
            }
            
        }
        //======
   
        
        //======
        
        
        var lastSection = self.chatMessages.count-1
        
        if lastSection >= 0{
            
            var lastRow = self.chatMessages[lastSection].count-1
            
            if lastRow >= 0 {
                
                chatMessages[lastSection].append(socketMessage)
                lastSection = self.chatMessages.count-1
                lastRow = self.chatMessages[lastSection].count-1
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: [IndexPath(row: lastRow, section: lastSection)], with: .none)
                self.tableView.endUpdates()
                // self.tableView.reloadRows(at: [IndexPath(row: lastRow, section: lastSection)], with: .none)
                self.tableView.layoutIfNeeded()
                //self.adjustInsets()
                if let indexPath = self.getScrollPosition() {
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
                
            }

        }
        
    }
    
   fileprivate func attemptToAssembleGroupedMessages() {
        var messagesToAppend = [ChatMessage]()
        guard let serverMsgs = messagesFromServer else{return }

        if AppDelegateConstant.user?.userType == "Tutor" {

            serverMsgs.forEach { (msgs) in
                if msgs.msgType == MessageType.MESSAGE_TYPE_PAY_BUTTON ||   msgs.msgType == MessageType.MESSAGE_TYPE_FINISH_BUTTON || msgs.msgType == MessageType.MESSAGE_TYPE_TUTOR_HAS_ARRIVED{
                    // do not append
                }else{
                    messagesToAppend.append(msgs)
                }
               // messagesToAppend.append(msgs)
            }

        }else{

            serverMsgs.forEach { (msgs) in
                if msgs.msgType == MessageType.MESSAGE_TYPE_ADDRESS_ALERT || msgs.msgType == MessageType.MESSAGE_TYPE_ARRIVED_BUTTON || msgs.msgType == MessageType.MESSAGE_TYPE_TUTION_HAS_ENDED{
                    // do not append
                }else{
                    messagesToAppend.append(msgs)
                }
               // messagesToAppend.append(msgs)
            }
        }

        chatMessages.removeAll()
        let groupedMessages = Dictionary(grouping: messagesToAppend) { (element) -> Date in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: element.sendedOn ?? "")

            return (date?.reduceToMonthDayYear() ?? Date())
        }

        let sortedKeys = groupedMessages.keys.sorted()
        sortedKeys.forEach { (key) in
            let values = groupedMessages[key]
            chatMessages.append(values ?? [])
        }

            tableView.reloadData()
            if let indexPath = self.getScrollPosition() {
                        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }

    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
              bottomConstraint.constant = 0
        } else {
              bottomConstraint.constant = keyboardViewEndFrame.height
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }) { (completed) in
            
            if notification.name == UIResponder.keyboardWillChangeFrameNotification {
                if let indexPath = self.getScrollPosition() {
                     self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            
            }
        }
    }
    
    fileprivate func getScrollPosition() -> IndexPath? {
        let section = self.chatMessages.count-1
       
        if let msg = self.chatMessages[safe: section]{
             let row = msg.count-1
             return IndexPath(row: row, section: section)
        }
       return nil
    }
    
    fileprivate func initialViewWhenNavigatedFromEdgemOfferVC() {
        // user name
        if AppDelegateConstant.user?.ID == self.makeOfferRequst.offeredByID{
          
            if let name = self.makeOfferRequst?.offeredToName, let subject = self.makeOfferRequst.subjects{
                 self.userName.text = name + " (" + subject + ")"
            }
           
        }else{
            
            if let name = self.makeOfferRequst?.offeredByName, let subject = self.makeOfferRequst.subjects{
                self.userName.text = name + " (" + subject + ")"
            }
//            self.userName.text = self.makeOfferRequst?.offeredByName
        }
        
        // user online status
        let image = makeOfferRequst?.onlineStatus == 1 ? #imageLiteral(resourceName: "user_online") :#imageLiteral(resourceName: "user_offline")
        onlineStatus.image = image
        onlineStatusLabel.text = (makeOfferRequst?.onlineStatus == 1) ? "Online" : "Offline"
        if let status = makeOfferRequst.status{
            print("initial setup if condition")
            self.disableChatOnOfferCompleted(status)
            getProgressBarCurrentStatus(status)
        }
        
        // user profile image
        self.loadImagesFromMakeOffer(makeOfferRequst)
        
    }
    
    func initialSetUP() {
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        
        //addNotifications()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        messageTextView.delegate = self
        messageTextView.isScrollEnabled = false
    
        messageTextView.font = UIFont(name: SourceSansProRegular, size: 15)
        messageTextView.textColor = theamGrayColor
        
        messageTextView.isUserInteractionEnabled = shouldEnableChat ? true : false
        self.sendMessageBtn.isUserInteractionEnabled = shouldEnableChat ? true : false
        
        tableView.delegate = self
        tableView.dataSource  = self
        
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        
        
        counterOfferPicker.delegate = self
        counterOfferPicker.dataSource = self

        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
        
                initialViewWhenNavigatedFromEdgemOfferVC()
                
            }else{
                
                // user name
                if AppDelegateConstant.user?.ID == self.bookingDetails?.offeredByID{
                    
                    if let name = self.bookingDetails?.offeredToName, let subject = self.bookingDetails?.subjectName{
                        self.userName.text = name + " (" + subject + ")"
                    }
                    
                    // self.userName.text = self.bookingDetails?.offeredToName
                }else{
                    
                    if let name = self.bookingDetails?.offeredByName, let subject = self.bookingDetails?.subjectName{
                        self.userName.text = name + " (" + subject + ")"
                    }
                     //self.userName.text = self.bookingDetails?.offeredByName
                }
                
                 // user online status
                let image = bookingDetails?.onlineStatus == 1 ? #imageLiteral(resourceName: "user_online") :#imageLiteral(resourceName: "user_offline")
                onlineStatusLabel.text = ( bookingDetails?.onlineStatus == 1) ? "Online" : "Offline"
                onlineStatus.image = image
                
                if let status = bookingDetails?.status{
                    print("initial setup else condition")
                    getProgressBarCurrentStatus(status)
                    disableChatOnOfferCompleted(status)
                }
                
                 // user profile image
                if let booking_detail = bookingDetails{
                      self.loadImages(booking_detail)
                }
            }
        }
    }
    
    fileprivate func didTapPayBtn(_ status: Bool, _ msgID: String?, _ payBtnCell: ChatCustomButtomTableViewCell?, _ row: Int?, section: Int?) {
        let bottomPopUpPaymentViewController = BottomPopUpPaymentViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
        bottomPopUpPaymentViewController.makeOfferRequest = self.makeOfferRequst
        bottomPopUpPaymentViewController.chatViewController = self
        bottomPopUpPaymentViewController.msgID = msgID
        bottomPopUpPaymentViewController.payBtnRow = row
        bottomPopUpPaymentViewController.modalPresentationStyle = .overCurrentContext
        bottomPopUpPaymentViewController.payBtnSection = section
        bottomPopUpPaymentViewController.payBtnCell = payBtnCell
        self.navigationController?.present(bottomPopUpPaymentViewController, animated: true, completion: nil)
    }
    
    @objc func pinch(sender:UIPinchGestureRecognizer){

        if let pinchImage = sender.view{
            
            pinchImage.layer.cornerRadius = 5
            pinchImage.contentMode = .scaleAspectFill
            pinchImage.clipsToBounds = true
            
            if sender.state == .began || sender.state == .changed {
                
             // Use below commented code if pinch is not working properly
                let currentScale = pinchImage.frame.size.width / pinchImage.bounds.size.width

                var newScale = currentScale*sender.scale
                
//                guard let view = sender.view else {return}
//
//                let pinchCenter = CGPoint(x: sender.location(in: view).x - view.bounds.midX,
//                                                                                                       y: sender.location(in: view).y - view.bounds.midY)
//
//                let transform = view.transform.translatedBy(x: pinchCenter.x, y: pinchCenter.y)
//
//                .scaledBy(x: sender.scale, y: sender.scale)
//
//                .translatedBy(x: -pinchCenter.x, y: -pinchCenter.y)
//
//                let currentScale = pinchImage.frame.size.width / pinchImage.bounds.size.width
//
//                var newScale = currentScale*sender.scale
                
                if newScale < 1 {
                    
                    newScale = 1
                    
//                    let transform = CGAffineTransform(scaleX: newScale, y: newScale)
//
//                    pinchImage.transform = transform
//
//                    sender.scale = 1
                    
                }
//                else{
//                    view.transform = transform
//
//                    sender.scale = 1
//                }
                if newScale > 6 {

                    newScale = 6

                }
                
                let transform = CGAffineTransform(scaleX: newScale, y: newScale)

                pinchImage.transform = transform

                sender.scale = 1
            } else if sender.state == .ended{
                
//                UIView.animate(withDuration: 0.3) {
//                    pinchImage.transform = CGAffineTransform.identity
//                }
                
            }
        }

    }

    var startingFrame: CGRect?
    var blackBackGround: UIView?
    var startingImageView: UIImageView?
    
      // custom zoom-out logic
    @objc func handleZoomOut(tapGesture: UITapGestureRecognizer){
      
        if let zoomOutImageView = tapGesture.view {
            
            zoomOutImageView.layer.cornerRadius = 5
            zoomOutImageView.contentMode = .scaleAspectFill
            zoomOutImageView.clipsToBounds = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                zoomOutImageView.frame = self.startingFrame!
                self.blackBackGround?.alpha = 0
            }) { (completed: Bool) in
                  zoomOutImageView.removeFromSuperview()
                 self.startingImageView?.isHidden = false
            }
    
        }
    }
    
    func showPopUPForDiscountedAmt(_ sender: UIButton){
        print(value: "show pop")
        let text = "Show here required message".localized
        
        guard let cell = sender.superview as? ChatMessageCell else {
            return // or fatalError() or whatever
        }
        
        if let tipView = self.tipView{
            
            tipView.dismiss()
            
            cell.infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }else{
            
            let tip = EasyTipView(text: text, preferences: self.preferences, delegate: self)
            
            cell.infoBtn.setImage(#imageLiteral(resourceName: "infoImageSelected"), for: .normal)
            
            tip.show(forView: cell.infoBtn)
            
            tipView = tip
            
        }
        
//        if let indexPath = self.tableView.indexPathForSelectedRow{
//
//            let cell = tableView.cellForRow(at: indexPath) as! ChatMessageCell
//
//            if let tipView = self.tipView{
//
//                tipView.dismiss()
//
//                cell.infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
//            }else{
//
//                let tip = EasyTipView(text: text, preferences: self.preferences, delegate: self)
//
//                cell.infoBtn.setImage(#imageLiteral(resourceName: "infoImageSelected"), for: .normal)
//
//                tip.show(forView: cell.infoBtn)
//
//                tipView = tip
//
//            }
//        }
        
       
    }
    // custom zoom-in logic
     func performZoomInForStartingImage(_ startingImageView: UIImageView){
    
        self.startingImageView = startingImageView
        self.startingImageView?.isHidden = true
        
        startingFrame = startingImageView.superview?.convert(startingImageView.frame, to: nil)
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.backgroundColor = .red
        zoomingImageView.contentMode = .scaleAspectFill
        zoomingImageView.image = startingImageView.image
        zoomingImageView.isUserInteractionEnabled = true
        
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        // pinch to zoom in logic
         zoomingImageView.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(self.pinch(sender:))))
        
        if let keyWindow = UIApplication.shared.keyWindow{

            blackBackGround = UIView(frame: keyWindow.frame)
            blackBackGround?.backgroundColor = .black
            blackBackGround?.alpha = 0
           
            
            keyWindow.addSubview(blackBackGround!)
            keyWindow.addSubview(zoomingImageView)
        
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackBackGround?.alpha = 1
                
                let height = self.startingFrame!.height / self.startingFrame!.width * keyWindow.frame.width
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                zoomingImageView.center = keyWindow.center
                
            }) { (completed: Bool) in
              //  zoomOutImageView.removeFromSuperview()
            }

        }
       
    }
    
}


// MARK: - Easy Tip View Delegate
extension ChatViewController: EasyTipViewDelegate{
    
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
    }
    
}

// MARK: ----------- Navigate to bottom pop with card detail

extension ChatViewController: AddNewCardViewControllerDelegate {
    func getCardDetail(_ cardDetail: StoreUserCard) {
        
        AppDelegateConstant.user?.paymentCard?.append(cardDetail)
        if let _ = AppDelegateConstant.user?.paymentCard{
            didTapPayBtn(true, nil, nil, nil, section: nil)
        }
    }
}

// MARK: ----------- UIPicker View
extension ChatViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 0
    }
    
}

// MARK: ----------- ImagePicker
extension ChatViewController {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            selectedImageFromPicker = editedImage
        }else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            if let imageData = selectedImage.jpeg(.lowMedium){
                self.sendMessageWithImagesOrDoc(imageData: imageData)
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}


 // MARK: ----------- CollectionView Datasource
extension ChatViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return progressBarData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ActivityCompletionStatusCollectionViewCell.cellIdentifire(), for: indexPath) as! ActivityCompletionStatusCollectionViewCell
        
        if isOfferCancel == true{
            cell.configureCellForCancleOffer(withProgressBarData: self.progressBarData[indexPath.item], currentIndex: indexPath.item, withTotalElement: self.progressBarData.count)
        }else{
            cell.configureCell(withProgressBarData: self.progressBarData[indexPath.item], currentIndex: indexPath.item, withTotalElement: self.progressBarData.count)
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 92, height: 90)
    }
}

// MARK: ----------- CollectionView Delegate
extension ChatViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}

// MARK: ----------- TableView Delegates/Datasource

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.chatMessages.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return chatMessages[section].count
    }
    
    fileprivate func disableBtnOnCancel(_ msgObject: ChatMessage) {
        if let vc = self.navigationController?.getPreviousViewController(){
            
            if vc is EdgemOfferViewController{
                
                if makeOfferRequst?.isCancelled == 1 || isOfferCancel == true{
                    msgObject.buttonFlag = 1
                }
                
            }else{
                
                if bookingDetails?.isCancelled == 1 || isOfferCancel == true{
                    msgObject.buttonFlag = 1
                }
                
            }
            
        }else{
            if let data = self.makeOfferRequst{
                
                if data.isCancelled == 1 || isOfferCancel == true{
                      msgObject.buttonFlag = 1
                }
                
            }else if let data = self.bookingDetails{
                if data.isCancelled == 1 || isOfferCancel == true{
                        msgObject.buttonFlag = 1
                }
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let tipView = self.tipView{
            tipView.dismiss()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let msgObject = chatMessages[indexPath.section][indexPath.row]
        if msgObject.viewType == MessageType.MESSAGE_TYPE_MAKE_EDIT_OFFER{
            msgObject.viewType =  MessageType.MESSAGE_TYPE_TEXT
        }
        switch msgObject.viewType {
            
        case MessageType.MESSAGE_TYPE_MADE_OFFER_ALERT:
            
                                        guard let cell = tableView.dequeueReusableCell(withIdentifier: StaticMessageInChatTableViewCell.cellIdentifire(), for: indexPath) as? StaticMessageInChatTableViewCell else{return UITableViewCell()}
                                        cell.alertImageView.image = #imageLiteral(resourceName: "getOffer")
                                        
                                        if AppDelegateConstant.user?.ID == msgObject.senderID {
                                            
                                            if AppDelegateConstant.user?.userType == tutorType {
                                                
                                                cell.cellTitle.text = "Woohoo you made an offer!".localized
                                                
                                                cell.descriptionLabel.text = tutor_made_offer
                                                
                                                
                                            }else{
                                                
                                                
                                                cell.cellTitle.text = "Woohoo you made an offer!".localized
                                                
                                                cell.descriptionLabel.text = student_made_offer
                                                
                                            }
                                            
                                            
                                            
                                        }else{
                                            
                                            
                                            if AppDelegateConstant.user?.userType == tutorType {
                                                
                                                cell.cellTitle.text = "Woohoo you got an offer!".localized
                                                
                                                cell.descriptionLabel.text = tutor_got_offer
                                                
                                                
                                            }else{
                                                
                                                
                                                cell.cellTitle.text = "Woohoo you got an offer!".localized
                                                
                                                cell.descriptionLabel.text = student_got_offer
                                                
                                            }
                                            
                                        }
                                         return cell
            
        case MessageType.MESSAGE_TYPE_EDIT_OFFER_ALERT:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: StaticMessageInChatTableViewCell.cellIdentifire(), for: indexPath) as? StaticMessageInChatTableViewCell else{return UITableViewCell()}
            cell.alertImageView.image = #imageLiteral(resourceName: "getOffer")
            
            if AppDelegateConstant.user?.ID == msgObject.senderID {
                
                if AppDelegateConstant.user?.userType == tutorType {
                    
                    cell.cellTitle.text = "Woohoo you edit an offer!".localized
                    
                    cell.descriptionLabel.text = tutor_made_offer.replacingOccurrences(of: "made", with: "edit")
                    
                    
                }else{
                    
                    
                    cell.cellTitle.text = "Woohoo you edit an offer!".localized
                    
                    cell.descriptionLabel.text = student_made_offer.replacingOccurrences(of: "made", with: "edit")
                    
                }
                
                
                
            }else{
                
                
                if AppDelegateConstant.user?.userType == tutorType {
                    
                    cell.cellTitle.text = "Woohoo you got an offer!".localized
                    
                    cell.descriptionLabel.text = tutor_got_offer.replacingOccurrences(of: "made", with: "edit")
                    
                    
                }else{
                    
                    
                    cell.cellTitle.text = "Woohoo you got an offer!".localized
                    
                    cell.descriptionLabel.text = student_got_offer.replacingOccurrences(of: "made", with: "edit")
                    
                }
                
            }
            return cell
            
        
        case MessageType.MESSAGE_TYPE_OFFERACCEPTED:
            
            
                                    guard let cell = tableView.dequeueReusableCell(withIdentifier: ChatMessageCell.cellIdentifire(), for: indexPath) as? ChatMessageCell else{return UITableViewCell()}
            
                                    cell.configureCell(with: chatMessages[indexPath.section][indexPath.row])
            
                                    return cell
            
        case MessageType.MESSAGE_TYPE_PAYMENT_PENDING_ALERT:
            
                                    guard let cell = tableView.dequeueReusableCell(withIdentifier: StaticMessageInChatTableViewCell.cellIdentifire(), for: indexPath) as? StaticMessageInChatTableViewCell else{return UITableViewCell()}
                                    
                                    let name = AppDelegateConstant.user?.firstName ?? ""
                                    
                                    cell.alertImageView.image = #imageLiteral(resourceName: "getOffer")
                                    
                                    
                                    if let vc = self.navigationController?.getPreviousViewController(){
                                        
                                        if vc is EdgemOfferViewController{
                                            
                                            
                                            if AppDelegateConstant.user?.ID == self.makeOfferRequst.offeredByID {
                                                
                                                if AppDelegateConstant.user?.userType == tutorType {
                                                    
                                                    cell.cellTitle.text = "Hi \(name),".localized
                                                    cell.descriptionLabel.text = student_accept_offer_shown_in_tut_scr
                                                    
                                                }else{
                                                    
                                                    cell.cellTitle.text = "Hi \(name),".localized
                                                    cell.descriptionLabel.text = tutor_accept_offer_show_in_stu_scr
                                                    
                                                    
                                                }
                                                
                                                
                                            }else{
                                                
                                                if AppDelegateConstant.user?.userType == tutorType {
                                                    
                                                    cell.cellTitle.text = "Hi \(name),".localized
                                                    cell.descriptionLabel.text = tutor_accept_offer_show_in_tut_scr
                                                    
                                                }else{
                                                    
                                                    cell.cellTitle.text = "Hi \(name),".localized
                                                    cell.descriptionLabel.text = student_accept_offer_shown_in_stu_scr
                                                    
                                                }
                                                
                                            }
                                            
                                        }else{
                                            
                                            if AppDelegateConstant.user?.ID == self.bookingDetails?.offeredByID {
                                                
                                                if AppDelegateConstant.user?.userType == tutorType {
                                                    
                                                    cell.cellTitle.text = "Hi \(name),".localized
                                                    cell.descriptionLabel.text = student_accept_offer_shown_in_tut_scr
                                                    
                                                }else{
                                                    
                                                    cell.cellTitle.text = "Hi \(name),".localized
                                                    cell.descriptionLabel.text = tutor_accept_offer_show_in_stu_scr
                                                    
                                                    
                                                }
                                                
                                                
                                            }else{
                                                
                                                if AppDelegateConstant.user?.userType == tutorType {
                                                    
                                                    cell.cellTitle.text = "Hi \(name),".localized
                                                    cell.descriptionLabel.text = tutor_accept_offer_show_in_tut_scr
                                                    
                                                }else{
                                                    
                                                    cell.cellTitle.text = "Hi \(name),".localized
                                                    cell.descriptionLabel.text = student_accept_offer_shown_in_stu_scr
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                        }
                                        
                                    }
                                    
                                    
                                    return cell

            
            
        case MessageType.MESSAGE_TYPE_PAYMENT_DONE_ALERT:
            
                                    guard let cell = tableView.dequeueReusableCell(withIdentifier: StaticMessageInChatTableViewCell.cellIdentifire(), for: indexPath) as? StaticMessageInChatTableViewCell else{return UITableViewCell()}
                                    
                                    let name = AppDelegateConstant.user?.firstName ?? ""
                                    
                                    cell.alertImageView.image = #imageLiteral(resourceName: "getOffer")
                                    
                                    if AppDelegateConstant.user?.userType != "Tutor"{
                                        
                                        cell.cellTitle.text = "Hi \(name),".localized
                                        
                                        cell.descriptionLabel.text = payment_done_msg
                                        
                                    }else{
                                        
                                        cell.cellTitle.text = "Hi \(name),".localized
                                        
                                        cell.descriptionLabel.text = payment_done_msg_tut
                                        
                                    }
            
                                    return cell
            
        case MessageType.MESSAGE_TYPE_TEXT:
          
                                        guard let cell = tableView.dequeueReusableCell(withIdentifier: ChatMessageCell.cellIdentifire(), for: indexPath) as? ChatMessageCell else{return UITableViewCell()}
                                        
                                         cell.chatViewController = self
                                        
                                        if let booking = self.bookingDetails{
                                            if AppDelegateConstant.user?.ID == booking.offeredByID{
                                                
                                                let imageURL = booking.offeredToImage
                                                if !imageURL.isEmpty{
                                                 msgObject.realImage = imageURL
                                                }
                                                
                                            }else{
                                                let imageURL = booking.offeredByImage
                                                if !imageURL.isEmpty{
                                                msgObject.realImage = imageURL
                                                }
                                            }
                                            
                                            if let disc_price = booking.discountedPrice, let disc_price_double = Double(disc_price), disc_price_double > 0{
                                                msgObject.discountedPrice = disc_price
                                            }else{
                                                msgObject.discountedPrice = ""
                                            }
                                        }else if let booking = self.makeOfferRequst{
                                            if AppDelegateConstant.user?.ID == booking.offeredByID{
                                                
                                                let imageURL = booking.offeredToImage
                                                if let url = imageURL, !url.isEmpty{
                                                     msgObject.realImage = url
                                                }
                                            }else{
                                                let imageURL = booking.offeredByImage
                                                if let url = imageURL, !url.isEmpty{
                                                    msgObject.realImage = url
                                                }
                                            }
                                            
                                            if let disc_price = booking.discountedPrice, let disc_price_double = Double(disc_price), disc_price_double > 0{
                                                msgObject.discountedPrice = disc_price
                                            }else{
                                                msgObject.discountedPrice = ""
                                            }
                                        }
                                        
                                        cell.configureCell(with: msgObject)
                                      
                                        return cell
        
        case MessageType.MESSAGE_TYPE_MIME:
            
                                    if msgObject.senderID == AppDelegateConstant.user?.ID {
                                        guard let cell = tableView.dequeueReusableCell(withIdentifier: RightImageTableViewCell.cellIdentifire(), for: indexPath) as? RightImageTableViewCell else{return UITableViewCell()}
                                        cell.configureCell(chatMessages[indexPath.section][indexPath.row], indexPath.section, indexPath.row)
                                        cell.chatViewController = self
                                        return cell
                                    }else{

                                        guard let cell = tableView.dequeueReusableCell(withIdentifier: LeftImageTableViewCell.cellIdentifire(), for: indexPath) as? LeftImageTableViewCell else{return UITableViewCell()}
                                        cell.configureCell(chatMessages[indexPath.section][indexPath.row])
                                        cell.chatViewController = self
                                        return cell

                                    }
            
            
        case MessageType.MESSAGE_TYPE_PAY_BUTTON:
            
                                        guard let cell = tableView.dequeueReusableCell(withIdentifier: ChatCustomButtomTableViewCell.cellIdentifire(), for: indexPath) as? ChatCustomButtomTableViewCell else{return UITableViewCell()}
                                        
                                        disableBtnOnCancel(msgObject)
                                        
                                        cell.configureCell(with: msgObject, indexPath.row, indexPath.section)
                                        cell.didTapPaymentBtn = { [weak self] status, msgID, row, section in
                                            self?.didTapPayBtn(status,msgID,cell, row, section: section)
                                        }
                                        return cell
            
        case MessageType.MESSAGE_TYPE_ADDRESS_ALERT:
            
                                        if AppDelegateConstant.user?.userType == "Tutor"{
                                            guard let cell = tableView.dequeueReusableCell(withIdentifier: StudentAddressTableViewCell.cellIdentifire(), for: indexPath) as? StudentAddressTableViewCell else{return UITableViewCell()}
                                            cell.configureCell(with: chatMessages[indexPath.section][indexPath.row])
                                            return cell
                                        }else{
                                            return UITableViewCell()
                                        }
        
        case MessageType.MESSAGE_TYPE_TUTION_HAS_ENDED:
            
            if AppDelegateConstant.user?.userType == "Tutor"{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: StaticMessageInChatTableViewCell.cellIdentifire(), for: indexPath) as? StaticMessageInChatTableViewCell else{return UITableViewCell()}
                
                let name = AppDelegateConstant.user?.firstName ?? ""
                
                if AppDelegateConstant.user?.userType != "Tueetor"{
                    
                    cell.cellTitle.text = "Hi \(name),".localized
                    cell.descriptionLabel.text = msgObject.conversationMessage
                    cell.alertImageView.image = #imageLiteral(resourceName: "alarm")
                }
                return cell
            }else{
                return UITableViewCell()
            }
            
        case MessageType.MESSAGE_TYPE_N_TH_LESSON_COMPLETED:
            
            //if AppDelegateConstant.user?.userType == "Tutor"{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: StaticMessageInChatTableViewCell.cellIdentifire(), for: indexPath) as? StaticMessageInChatTableViewCell else{return UITableViewCell()}
                
                let name = AppDelegateConstant.user?.firstName ?? ""
                
                cell.cellTitle.text = "Hi \(name),".localized
                cell.descriptionLabel.text = msgObject.conversationMessage
                cell.alertImageView.image = #imageLiteral(resourceName: "tution_completed_icon")
                
                return cell
          //  }
                                        
        case MessageType.MESSAGE_TYPE_SCHEDULE_REQ:
            
            var msg = ""
            
            var replace = ""
            var sufix = ""
            
             disableBtnOnCancel(msgObject)
            
            if let vc = self.navigationController?.getPreviousViewController(){

                if vc is EdgemOfferViewController{


                    if AppDelegateConstant.user?.ID == self.makeOfferRequst.schedule_cancel_by {

                        replace = who_sch_who_scr_class_prefix

                        if AppDelegateConstant.user?.userType == tutorType {

                            sufix = tut_sch_tut_scr__sch_class_sufix

                        }else{

                            sufix = stu_sch_stu_scr_class_sufix
                        }


                    }else{

                        if AppDelegateConstant.user?.userType == tutorType {

                          replace = stu_sch_tut_scr

                        }else{

                          replace = tut_sch_stu_scr

                        }

                    }

                }else{

                    if AppDelegateConstant.user?.ID == self.bookingDetails?.schedule_cancel_by {
                        
                        replace = who_sch_who_scr_class_prefix

                        if AppDelegateConstant.user?.userType == tutorType {

                            sufix = tut_sch_tut_scr__sch_class_sufix

                        }else{

                            sufix = stu_sch_stu_scr_class_sufix

                        }


                    }else{

                        if AppDelegateConstant.user?.userType == tutorType {

                            replace = stu_sch_tut_scr

                        }else{
                            
                            replace = tut_sch_stu_scr
                        }

                    }

                }

            }
            
            msg = ((msgObject.conversationMessage).replacingOccurrences(of: "DYNAMIC_DATA", with: replace)).replacingOccurrences(of: "SUFFIX", with: sufix)
                    
            let _msg = msg
            
            
            
                                    if AppDelegateConstant.user?.ID == msgObject.senderID {
                                        guard let cell = tableView.dequeueReusableCell(withIdentifier: ChatMessageCell.cellIdentifire(), for: indexPath) as? ChatMessageCell else{return UITableViewCell()}
                                        msgObject.conversationMessage = _msg
                                        cell.configureCell(with: msgObject)

                                        return cell
                                    }else{
                                         guard let cell = tableView.dequeueReusableCell(withIdentifier: AcceptCancleTableViewCell.cellIdentifire(), for: indexPath) as? AcceptCancleTableViewCell else{return UITableViewCell()}
                                        cell.chatViewController = self
                                        msgObject.conversationMessage = _msg
                                        cell.configureCell(with: msgObject, indexPath.section, indexPath.row)
                                         return cell
                                    }
            
        case MessageType.MESSAGE_TYPE_ARRIVED_BUTTON:
            
                                 disableBtnOnCancel(msgObject)
            
                                if AppDelegateConstant.user?.userType == "Tutor"{
                                    guard let cell = tableView.dequeueReusableCell(withIdentifier: ArrivedBtnTableViewCell.cellIdentifire(), for: indexPath) as? ArrivedBtnTableViewCell else{return UITableViewCell()}
                                    cell.configureCell(with: msgObject)
                                    cell.row = indexPath.row
                                    cell.section = indexPath.section
                                    cell.chatViewController = self
                                    return cell
                                }else{
                                    return UITableViewCell()
                                }
            
        case MessageType.MESSAGE_TYPE_FINISH_BUTTON:
            
            disableBtnOnCancel(msgObject)
            
            if AppDelegateConstant.user?.userType == "Tutor"{
                return UITableViewCell()
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: EndTutionBtnTableViewCell.cellIdentifire(), for: indexPath) as? EndTutionBtnTableViewCell else{return UITableViewCell()}
                cell.configureCell(with: msgObject)
                cell.row = indexPath.row
                cell.section = indexPath.section
                cell.chatViewController = self
                return cell
            }
            
        case MessageType.MESSAGE_TYPE_TUTOR_HAS_ARRIVED:
            
            if AppDelegateConstant.user?.userType != "Tutor"{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: StaticMessageInChatTableViewCell.cellIdentifire(), for: indexPath) as? StaticMessageInChatTableViewCell else{return UITableViewCell()}
                
                let name = AppDelegateConstant.user?.firstName ?? ""
                
                if AppDelegateConstant.user?.userType != "Tueetor"{
                    
                    cell.cellTitle.text = "Hi \(name),".localized
                    cell.descriptionLabel.text = msgObject.conversationMessage
                    cell.alertImageView.image = #imageLiteral(resourceName: "alarm")
                }
                return cell
            }else{
                return UITableViewCell()
            }
            
        case MessageType.MESSAGE_TYPE_DECLINED_SCHEDULE:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: StaticMessageInChatTableViewCell.cellIdentifire(), for: indexPath) as? StaticMessageInChatTableViewCell else{return UITableViewCell()}
            let name = AppDelegateConstant.user?.firstName ?? ""
            cell.alertImageView.image = #imageLiteral(resourceName: "alarm")
            cell.cellTitle.text = "Hi \(name),".localized
            cell.descriptionLabel.text = msgObject.conversationMessage
            
            return cell
            
        case MessageType.MESSAGE_TYPE_OFFER_CANCEL:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: StaticMessageInChatTableViewCell.cellIdentifire(), for: indexPath) as? StaticMessageInChatTableViewCell else{return UITableViewCell()}
            
            cell.alertImageView.image = #imageLiteral(resourceName: "getOffer")
            
            let name = AppDelegateConstant.user?.firstName ?? ""
            
            if self.currentLesson.contains("lesson"){
                
                if let vc = self.navigationController?.getPreviousViewController(){
                    
                    if vc is EdgemOfferViewController{
                        
                        if AppDelegateConstant.user?.ID == self.makeOfferRequst.schedule_cancel_by {
                            
                            if AppDelegateConstant.user?.userType != tutorType {
                                
                                cell.descriptionLabel.text = tut_cancel_bfr_tut_end_tut_scr
                                cell.cellTitle.text = "Hi \(name),".localized
                                
                            }else{
                                
                                cell.descriptionLabel.text = tut_cancel_bfr_tut_end_stu_scr
                                cell.cellTitle.text = "Hi \(name),".localized
                                
                            }
                            
                        }else{
                            
                            if AppDelegateConstant.user?.userType == tutorType {
                                
                                cell.descriptionLabel.text = stu_cancel_bfr_tut_end_tut_scr
                                cell.cellTitle.text = "Hi \(name),".localized
                                
                            }else{
                                
                                cell.descriptionLabel.text = stu_cancel_bfr_tut_end_stu_scr
                                cell.cellTitle.text = "Hi \(name),".localized
                                
                            }
                            
                        }
                        
                    }else{
                        
                        if AppDelegateConstant.user?.ID == self.bookingDetails?.schedule_cancel_by {
                            
                            if AppDelegateConstant.user?.userType != tutorType {
                                
                                cell.descriptionLabel.text = tut_cancel_bfr_tut_end_tut_scr
                                cell.cellTitle.text = "Hi \(name),".localized
                                
                            }else{
                                
                                cell.descriptionLabel.text = tut_cancel_bfr_tut_end_stu_scr
                                cell.cellTitle.text = "Hi \(name),".localized
                                
                            }
                            
                            
                        }else{
                            
                            if AppDelegateConstant.user?.userType == tutorType {
                                
                                cell.descriptionLabel.text = stu_cancel_bfr_tut_end_tut_scr
                                cell.cellTitle.text = "Hi \(name),".localized
                                
                            }else{
                                
                                cell.descriptionLabel.text = stu_cancel_bfr_tut_end_stu_scr
                                cell.cellTitle.text = "Hi \(name),".localized
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }else{
                
                if let vc = self.navigationController?.getPreviousViewController(){
                    
                    if vc is EdgemOfferViewController{
                        
                        if AppDelegateConstant.user?.ID == self.makeOfferRequst.schedule_cancel_by {
                            
                            cell.descriptionLabel.text = you_have_reject_this_offer
                            cell.cellTitle.text = "Hi \(name),".localized
                            
                        }else{
                            
                            if AppDelegateConstant.user?.userType == tutorType {
                                
                                cell.descriptionLabel.text = stu_reject_this_offer
                                cell.cellTitle.text = "Hi \(name),".localized
                                
                            }else{
                                
                                cell.descriptionLabel.text = tut_reject_this_offer
                                cell.cellTitle.text = "Hi \(name),".localized
                                
                            }
                            
                        }
                        
                    }else{
                        
                        if AppDelegateConstant.user?.ID == self.bookingDetails?.schedule_cancel_by {
                            
                            cell.descriptionLabel.text = you_have_reject_this_offer
                            cell.cellTitle.text = "Hi \(name),".localized
                            
                            
                        }else{
                            
                            if AppDelegateConstant.user?.userType == tutorType {
                                
                                cell.descriptionLabel.text = stu_reject_this_offer
                                cell.cellTitle.text = "Hi \(name),".localized
                                
                            }else{
                                
                                cell.descriptionLabel.text = tut_reject_this_offer
                                cell.cellTitle.text = "Hi \(name),".localized
                                
                            }
                            
                        }
                        
                        
                    }
                    
                }
                
            }
            
            return cell
            
        case MessageType.MESSAGE_TYPE_COUNTER_OFFER:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: StaticMessageInChatTableViewCell.cellIdentifire(), for: indexPath) as? StaticMessageInChatTableViewCell else{return UITableViewCell()}
            
            cell.alertImageView.image = #imageLiteral(resourceName: "getOffer")
            
            let name = AppDelegateConstant.user?.firstName ?? ""
            
            if let vc = self.navigationController?.getPreviousViewController(){
            
                    if vc is EdgemOfferViewController{
            
                        if AppDelegateConstant.user?.ID == self.makeOfferRequst.offeredByID {
            
                            if AppDelegateConstant.user?.userType == tutorType {
                                
                                cell.descriptionLabel.text = stu_counter_ofr_tut_scr
                                cell.cellTitle.text = "Hi \(name),".localized
            
                            }else{
                                
                                cell.descriptionLabel.text = stu_counter_ofr_stu_scr
                                cell.cellTitle.text = "Hi \(name),".localized
                                       
                            }
            
                        }else{
            
                            if AppDelegateConstant.user?.userType == tutorType {
            
                                cell.descriptionLabel.text = tutor_counter_ofr_tut_scr
                                cell.cellTitle.text = "Hi \(name),".localized
            
                            }else{
            
                                cell.descriptionLabel.text = tutor_counter_ofr_stu_scr
                                cell.cellTitle.text = "Hi \(name),".localized
            
                            }
            
                        }
            
                    }else{
            
                            if AppDelegateConstant.user?.ID == self.bookingDetails?.offeredByID {
            
                                    if AppDelegateConstant.user?.userType == tutorType {
            
                                        cell.descriptionLabel.text =  tutor_counter_ofr_tut_scr
                                        cell.cellTitle.text = "Hi \(name),".localized
            
                                    }else{
            
                                        cell.descriptionLabel.text = stu_counter_ofr_stu_scr
                                        cell.cellTitle.text = "Hi \(name),".localized
            
                                    }
            
            
                                }else{
            
                                    if AppDelegateConstant.user?.userType == tutorType {
            
                                        cell.descriptionLabel.text = stu_counter_ofr_tut_scr
                                        cell.cellTitle.text = "Hi \(name),".localized
            
                                    }else{
            
                                        cell.descriptionLabel.text = tutor_counter_ofr_stu_scr
                                        cell.cellTitle.text = "Hi \(name),".localized
            
                                    }
            
                                }
            
            
                            }
            
                        }
            
                return cell
            
            
        case MessageType.MESSAGE_TYPE_OFFER_CANCEL_DUE_TO_OFFER_NOT_ACCEPTED_24hr:
            
             guard let cell = tableView.dequeueReusableCell(withIdentifier: StaticMessageInChatTableViewCell.cellIdentifire(), for: indexPath) as? StaticMessageInChatTableViewCell else{return UITableViewCell()}
             
             cell.alertImageView.image = #imageLiteral(resourceName: "getOffer")
             
             let name = AppDelegateConstant.user?.firstName ?? ""
             
             if AppDelegateConstant.user?.userType == tutorType {
                
                cell.descriptionLabel.text = offer_not_accept_24hr_tut_scr
                cell.cellTitle.text = "Hi \(name),".localized
                
             }else{
                
                cell.descriptionLabel.text = offer_not_accept_24hr_stu_scr
                cell.cellTitle.text = "Hi \(name),".localized
                
             }
            
            return cell
            
        case MessageType.MESSAGE_TYPE_OFFER_CANCEL_DUE_TO_PAYMENT_NOT_DONE_24hr:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: StaticMessageInChatTableViewCell.cellIdentifire(), for: indexPath) as? StaticMessageInChatTableViewCell else{return UITableViewCell()}
            
            cell.alertImageView.image = #imageLiteral(resourceName: "getOffer")
            
            let name = AppDelegateConstant.user?.firstName ?? ""
            
            if AppDelegateConstant.user?.userType == tutorType {
                
                cell.descriptionLabel.text = payment_not_done_tut_scr
                cell.cellTitle.text = "Hi \(name),".localized
                
            }else{
                
                cell.descriptionLabel.text = payment_not_done_stu_scr
                cell.cellTitle.text = "Hi \(name),".localized
                
            }
            
            return cell
            
        default:

                                        return UITableViewCell()
            
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let msgObject = chatMessages[indexPath.section][indexPath.row]
        
        if msgObject.viewType == MessageType.MESSAGE_TYPE_PAYMENT_PENDING_ALERT{
            if AppDelegateConstant.user?.userType != "Tueetor"{
                return UITableView.automaticDimension
            }
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.view.endEditing(true)
        
        if self.tooTipContainerView.isHidden == false{
            self.tooTipContainerView.isHidden = true
        }
        if let tipView = self.tipView{
            tipView.dismiss()
            infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }
        
        //let object = self.chatMessages[indexPath.section][indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let firstMessageInSection = chatMessages[section].first {
            
            let label = DateHeaderLabel()
        
            let messageDate = Utilities.UTCToLocal(date: firstMessageInSection.sendedOn!, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "EEE, d MMM")
            let today = Utilities.UTCToLocal(date: Date().toString(dateFormat: "yyyy-MM-dd HH:mm:ss"), fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "EEE, d MMM")
            
            label.text = (messageDate == today) ? "Today".localized : messageDate
   
            label.translatesAutoresizingMaskIntoConstraints = false
            
            let containerView = UIView()
            containerView.backgroundColor = .clear
            containerView.addSubview(label)
            
            label.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
            label.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
            
            return containerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if indexPath.row == (messagesFromServer?.count)!-1 && pagination.currentPage != pagination.lastPage{
//            getAllMessages()
//        }
//    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.tableView{
            let visibleItems = self.tableView.visibleCells
            guard let lastCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) else{return}
            if visibleItems.contains(lastCell) && pagination.currentPage != pagination.lastPage{
                getAllMessages()
            }
        }
    }
}

// MARK: ----------- Textview Delegates

extension ChatViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {

        let size = CGSize(width: textView.frame.width, height: .infinity)
        let estimatedSize = textView.sizeThatFits(size)
        
        if estimatedSize.height > 54.0 {
             messageTextView.isScrollEnabled = true
        }else{
             messageTextViewHeight.constant = estimatedSize.height
             messageTextView.isScrollEnabled = false
        }
        
        messageTextView.layer.cornerRadius = messageTextViewHeight.constant/2.0

    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if sendMessageBtn.currentImage == #imageLiteral(resourceName: "camera")  || (sendMessageBtn.hasImage(named: "camera", for: .normal) ==  true){
            sendMessageBtn.setImage(#imageLiteral(resourceName: "sendMessage"), for: .normal)
        }
        
        if textView.textColor == theamGrayColor {
            textView.text = nil
            textView.textColor = theamBlackColor
        }
        activeTextView = textView
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if sendMessageBtn.currentImage == #imageLiteral(resourceName: "sendMessage")  && textView.text.isEmpty{
            sendMessageBtn.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
        }else{
             sendMessageBtn.setImage(#imageLiteral(resourceName: "sendMessage"), for: .normal)
        }
        if textView.text.isEmpty {
            textView.text =  shouldEnableChat ? "Type your message here".localized : ""
            textView.textColor = theamGrayColor
            textView.backgroundColor = shouldEnableChat ? .white : cellBackgroundColor
            return
        }
        activeTextView.text = textView.text
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

// MARK: ----------- API Implementation

extension ChatViewController {
    
     // [START send message api]
    fileprivate func sendMessage(_ conversationMessage: String){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        var offerID = 0
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                guard let _offerID = makeOfferRequst.offerID else{return}
                offerID = _offerID
            }else{
                guard let _offerID = bookingDetails?.offerID else{return}
                offerID = _offerID
            }
        }
        
        var loginType = ""
        
        if AppDelegateConstant.user?.userType == studentType{
                 loginType = "0"
        }else{
            if let logintype = UserStore.shared.logInType{
                  loginType = "\(logintype)"
            }
        }
        
        let params: [String : Any] = ["offer_id" : "\(offerID)",
                                                    "message" : conversationMessage,
                                                    "login_type" : loginType]
        
      //  Utilities.showHUD(forView: self.view, excludeViews: [])
    
        let userObserver = ApiManager.shared.apiService.sendMessage(params as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(message) in
            DispatchQueue.main.async {
              // Utilities.hideHUD(forView: self.view)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
     // [END send message api]
    
    // [START send message with image api]
    fileprivate func sendMessageWithImage(imageData: Data) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        var offerID = 0
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                guard let _offerID = makeOfferRequst.offerID else{return}
                offerID = _offerID
            }else{
                guard let _offerID = bookingDetails?.offerID else{return}
                offerID = _offerID
            }
        }
        
        var loginType = ""
        
        if AppDelegateConstant.user?.userType == studentType{
            loginType = "0"
        }else{
            if let logintype = UserStore.shared.logInType{
                loginType = "\(logintype)"
            }
        }
        
        let params: [String : Any] = ["offer_id" : "\(offerID)",
                                                    "login_type" : loginType]
        
        let url = "\(baseUrl)\(PathURl.SendMessage.rawValue)"
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Accept": "application/json",
            "Authorization": "Bearer \(UserStore.shared.token)"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            let fileName = "message_doc.png"
            let mimeType = imageData.mimeType
            multipartFormData.append(imageData, withName: "message_doc", fileName: fileName, mimeType: mimeType)
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in

                    DispatchQueue.main.async(execute: {
                       
                    })
                    if let _ = response.error{
                        self.showSimpleAlertWithMessage("There seems to be an error. Please try again later.".localized)
                        return
                    }
                    let json = try? JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
        
                    if let responseDict = json as? [String: AnyObject] {
                        
                        if let data = responseDict["data"] as? [String: AnyObject]{
                            print(data)
                            DispatchQueue.main.async {
                                if let imageURL = data["conversation_message"] as? String{
                                    if !imageURL.isEmpty{
                                        GetImageFromServer.getImage(imageURL: imageURL) { (image) in
                                            //image have no work here
                                             self.stopActivityController()
                                        }
                                    }
                                }
                            }
                        }
                      
                        
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                   
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            }
        }
    }
     // [END send message with image api]
    
    // [START get all messages api]
    fileprivate func getAllMessages() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        var offerID = 0
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                guard let _offerID = makeOfferRequst.offerID else{return}
                offerID = _offerID
            }else{
                guard let _offerID = bookingDetails?.offerID else{return}
                offerID = _offerID
            }
        }
        
        
        let params: [String : Any] = ["offer_id" : "\(offerID)",
            "page" : (String(pagination.currentPage+1))]
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        var tempArray = [ChatMessage]()
        if messagesFromServer != nil{
            tempArray = messagesFromServer!
        }
        self.messagesFromServer?.removeAll()
        let userObserver = ApiManager.shared.apiService.getAllMessages(params as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(chatObject) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                
                if self.isFirstTime == true{
                    tempArray = chatObject.messages.reversed()
                }else{
                    tempArray.insert(contentsOf: (chatObject.messages).reversed(), at: 0)
                }
                self.messagesFromServer = tempArray
                self.pagination = chatObject.pagination
                self.FromServer = true
                self.attemptToAssembleGroupedMessages()
                self.isFirstTime = false
                
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    // [END get all messages api]
    
    // [START cancle offer api]
    func cancleOffer(before24hrs offer_status: Bool) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let bank_creditCard = AppVariables.isCreditCardBankDetail(self)
        
        if bank_creditCard == false{
            return
        }
        
        var offerID = 0
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                 guard let _offerID = makeOfferRequst.offerID else{return}
                 offerID = _offerID
            }else{
                 guard let _offerID = bookingDetails?.offerID else{return}
                  offerID = _offerID
            }
        }
        
       
        let params: [String : Any] = ["offer_id" : "\(offerID)"]
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.cancleOffer(params as [String : AnyObject], status: offer_status)
        let userDisposable = userObserver.subscribe(onNext: {(message) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                print(value: message)
                self.showSimpleAlertWithMessage(message)
                
                
//                if let vc = self.navigationController?.getPreviousViewController(){
//                    if vc is EdgemOfferViewController{
//                        self.makeOfferRequst.hours = "1".localized
//                        self.makeOfferRequst.subjects = "Select Subject".localized
//                        self.makeOfferRequst.lessons = "4".localized
//                        self.makeOfferRequst.totalAmmount = "0.00"
//                        self.makeOfferRequst.ammountPerHour = "Enter Amount*".localized
//                        self.makeOfferRequst.offerID = nil
//                        self.getMakeOfferData?(self.makeOfferRequst, false)
//                        //self.showSimpleAlertWithMessage(message)
//                        self.navigationController?.popViewController(animated: true)
//                    }else{
//                       self.navigationController?.popViewController(animated: true)
//                    }
//                }
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
     // [END cancle offer api]
    
    // [START accept offer api]
    func acceptOffer(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let bank_creditCard = AppVariables.isCreditCardBankDetail(self)
        
        if bank_creditCard == false{
            return
        }
        
        var offerID = 0
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                guard let _offerID = makeOfferRequst.offerID else{return}
                offerID = _offerID
            }else{
                guard let _offerID = bookingDetails?.offerID else{return}
                offerID = _offerID
            }
        }
    
        let params: [String : Any] = ["offer_id" : "\(offerID)"]
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.acceptOffer(params as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(makeOfferRequestObject) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                
                self.makeOfferRequst = makeOfferRequestObject
                
                // TODO: - change bookingdetails cleass with make offer class
                self.bookingDetails?.status = makeOfferRequestObject.status!
                
                
                self.showHideEditCancleBtn()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
     // [END accept offer api]
    
     // [START Manage Shedule API]
    fileprivate func endTution(_ msgID: String, _ cell: EndTutionBtnTableViewCell, _ row: Int, _ section: Int) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        var offerID = 0
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                guard let _offerID = makeOfferRequst.offerID else{return}
                offerID = _offerID
            }else{
                guard let _offerID = bookingDetails?.offerID else{return}
                offerID = _offerID
            }
        }
        
        
        let params: [String : Any] = ["offer_id" : "\(offerID)",
                                                    "next_lesson_number" : self.getNextLesson(self.currentLesson),
                                                    "current_lesson" : self.currentLesson,
                                                    "msg_id" : msgID]
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.endTution(params as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(makeOfferRequestObject) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                self.chatMessages[section][row].buttonFlag = 1
//                cell.enableButton = false
//                cell.isUserInteractionEnabled = false
//               let indexPath = IndexPath(row: row, section: section)
//               self.tableView.reloadRows(at: [indexPath], with: .automatic)
                
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                
                cell.enableButton = true
                cell.isUserInteractionEnabled = true
                self.tableView.reloadData()
                
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
     // [END Manage Shedule API]
    
    // [START Manage Shedule API]
    fileprivate func userHaveArrived(_ msgID: String, _ cell: ArrivedBtnTableViewCell, _ row: Int, _ section: Int){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        var offerID = 0
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                guard let _offerID = makeOfferRequst.offerID else{return}
                offerID = _offerID
            }else{
                guard let _offerID = bookingDetails?.offerID else{return}
                offerID = _offerID
            }
        }
        
        
        let params: [String : Any] = ["offer_id" : "\(offerID)",
                                                     "msg_id" : msgID,
                                                     "current_lesson" : self.currentLesson]
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.TutorArrivalStatus(params as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(makeOfferRequestObject) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                self.chatMessages[section][row].buttonFlag = 1
//                cell.enableButton = false
//                cell.isUserInteractionEnabled = false
//                self.tableView.reloadData()
                //self.makeOfferRequst = makeOfferRequestObject
                
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                cell.enableButton = true
                cell.isUserInteractionEnabled = true
                self.tableView.reloadData()
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    // [END Manage Shedule API]
    
    // [START Manage Shedule API]
    fileprivate func manageSchedule(_ status: String, _ msgID: String, _ cell: AcceptCancleTableViewCell, section: Int, row: Int) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        var offerID = 0
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                guard let _offerID = makeOfferRequst.offerID else{return}
                offerID = _offerID
            }else{
                guard let _offerID = bookingDetails?.offerID else{return}
                offerID = _offerID
            }
        }
        
        
        let params: [String : Any] = ["offer_id" : "\(offerID)",
                                                    "request_status" : status,
                                                    "msg_id" : msgID]
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.manageSchedule(params as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(makeOfferRequestObject) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                self.chatMessages[section][row].buttonFlag = 1
                
                if status == "1"{
                    self.enableChatBefore24HourOfFirstLesonScheduling()
                }
                
//                cell.enableButton = false
//                cell.acceptBtn.isUserInteractionEnabled = false
//                cell.cancleBtn.isUserInteractionEnabled = false
                
//                self.tableView.reloadRows(at: [IndexPath(row: row, section: section)], with: .none)
//                self.tableView.reloadData()
               // self.makeOfferRequst = makeOfferRequestObject
            
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                cell.enableButton = true
                cell.acceptBtn.isUserInteractionEnabled = true
                cell.cancleBtn.isUserInteractionEnabled = true
                self.tableView.reloadData()
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    // [END Manage Shedule API]
    
}



extension Date {
    
    // This Week Start
    func getThisWeekStart() -> Date? {
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month, .weekday, .day, .hour], from: self) as NSDateComponents
        components.day -= components.weekday
        components.day += 2
        return Calendar.current.date(from: components as DateComponents)!
    }
    
    // This Week End
    func getThisWeekEnd() -> Date? {
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month, .weekday, .day, .hour], from: self) as NSDateComponents
        components.day -= components.weekday
        components.day += 8
        return Calendar.current.date(from: components as DateComponents)!
    }
     
    var startOfWeekForPrev: Date? {
        var gregorian = Calendar(identifier: .gregorian)
        gregorian.firstWeekday = 2
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: -6, to: sunday)
    }
    
    var endOfWeekForPrev: Date? {
        var gregorian = Calendar(identifier: .gregorian)
        gregorian.firstWeekday = 2
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 0, to: sunday)
    }
    
}


// MARK: ----------- Helping functions
extension ChatViewController {
    
    
    /// This function is used to laying out all the view appearing
    fileprivate func chatViewsLayout() {
        userImage.layer.borderWidth = 1
        userImage.layer.borderColor = theamBlackColor.cgColor
        userImage.layer.cornerRadius = userImage.bounds.width/2
        userImage.clipsToBounds = true
        
        //Add Account
        cancleBtn.layer.cornerRadius = 5
        cancleBtn.layer.borderWidth = 1
        cancleBtn.layer.borderColor = theamGrayColor.cgColor
        cancleBtn.clipsToBounds = true
        cancleBtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        cancleBtn.titleLabel?.textColor = theamBlackColor
        
        // addAccountBtncontainerView
        cancleBtnContainerView.layer.shadowColor = UIColor.black.cgColor
        cancleBtnContainerView.layer.shadowOpacity = 0.12
        cancleBtnContainerView.layer.shadowRadius = 4
        cancleBtnContainerView.layer.shadowOffset = .zero
        
        // confirm button attributes
        editOfferBtn.layer.cornerRadius = 5
        editOfferBtn.backgroundColor = buttonBackGroundColor
        editOfferBtn.clipsToBounds = true
        editOfferBtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        editOfferBtn.titleLabel?.textColor = theamBlackColor
        
        // apply button shadow
        editOfferContainerView.layer.shadowColor = UIColor.black.cgColor
        editOfferContainerView.layer.shadowOpacity = 0.12
        editOfferContainerView.layer.shadowRadius = 4
        editOfferContainerView.layer.shadowOffset = .zero
        editOfferContainerView.layer.shadowPath = UIBezierPath(rect: editOfferContainerView.bounds).cgPath
        //editOfferContainerView.layer.shouldRasterize = true
        
        //==========
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
//                counterOfferBtn.layer.cornerRadius = 5
//                //counterOfferBtn.backgroundColor = buttonBackGroundColor
//                counterOfferBtn.clipsToBounds = true
//
//                counterOfferBtn.titleLabel?.textColor = theamBlackColor
//
//
//                counterBtnContainerView.layer.shadowColor = UIColor.black.cgColor
//                counterBtnContainerView.layer.shadowOpacity = 0.12
//                counterBtnContainerView.layer.shadowRadius = 4
//                counterBtnContainerView.layer.shadowOffset = .zero
//                counterBtnContainerView.layer.shadowPath = UIBezierPath(rect: editOfferContainerView.bounds).cgPath
//                //counterBtnContainerView.layer.shouldRasterize = true
                
                counterOfferBtn.layer.cornerRadius = 5
                counterOfferBtn.layer.borderWidth = 1
                counterOfferBtn.layer.borderColor = theamGrayColor.cgColor
                counterOfferBtn.clipsToBounds = true
                counterOfferBtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
                counterOfferBtn.titleLabel?.textColor = theamBlackColor
                
                // addAccountBtncontainerView
                counterOfferBtn.layer.shadowColor = UIColor.black.cgColor
                counterOfferBtn.layer.shadowOpacity = 0.12
                counterOfferBtn.layer.shadowRadius = 4
                counterOfferBtn.layer.shadowOffset = .zero
                
            }else{
        
                // disable counter offer button
                if let negotiableStatus = bookingDetails?.negotiableStatus{
                    if negotiableStatus == 0{
                        counterOfferBtn.layer.cornerRadius = 5
                        
                        self.counterOfferBtn.isUserInteractionEnabled = false
                        
                        counterOfferBtn.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
                        counterOfferBtn.titleLabel?.textColor = theamGrayColor
                        counterOfferBtn.layer.borderWidth = 1
                        counterOfferBtn.layer.borderColor = staticMessageCellBorderColor.cgColor
                        counterOfferBtn.clipsToBounds = true
                        
                        counterBtnContainerView.layer.shadowColor = UIColor.clear.cgColor
                        
                        counterBtnContainerView.layer.shadowOpacity = 0.0
                        counterBtnContainerView.layer.shadowRadius = 0
                        counterBtnContainerView.layer.shadowOffset = .zero
                        counterBtnContainerView.layer.shadowPath = UIBezierPath(rect: editOfferContainerView.bounds).cgPath
                        //counterBtnContainerView.layer.shouldRasterize = false
                    }else{
                        
                        //Add Account
                        counterOfferBtn.layer.cornerRadius = 5
                        counterOfferBtn.layer.borderWidth = 1
                        counterOfferBtn.layer.borderColor = theamGrayColor.cgColor
                        counterOfferBtn.clipsToBounds = true
                        counterOfferBtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
                        counterOfferBtn.titleLabel?.textColor = theamBlackColor
                        
                        // addAccountBtncontainerView
                        counterOfferBtn.layer.shadowColor = UIColor.black.cgColor
                        counterOfferBtn.layer.shadowOpacity = 0.12
                        counterOfferBtn.layer.shadowRadius = 4
                        counterOfferBtn.layer.shadowOffset = .zero
                        
                        
                        
//                        counterOfferBtn.layer.cornerRadius = 5
//                        //counterOfferBtn.backgroundColor = buttonBackGroundColor
//                        counterOfferBtn.clipsToBounds = true
//
//                        counterOfferBtn.titleLabel?.textColor = theamBlackColor
//
//                        counterBtnContainerView.layer.shadowColor = UIColor.black.cgColor
//
//                        counterBtnContainerView.layer.shadowOpacity = 0.12
//                        counterBtnContainerView.layer.shadowRadius = 4
//                        counterBtnContainerView.layer.shadowOffset = .zero
//                        counterBtnContainerView.layer.shadowPath = UIBezierPath(rect: editOfferContainerView.bounds).cgPath
                        //counterBtnContainerView.layer.shouldRasterize = true
                    }
                }
                
            }
        }
        //==========
        
        counterOfferBtn.setTitle("Counter Offer".localized, for: .normal)
        counterOfferBtn?.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        counterOfferBtn.titleLabel?.textAlignment = .center
        counterOfferBtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
    
        messageTextView.textContainerInset = UIEdgeInsets(top: 8, left: 20, bottom: 8, right: 20)
        messageTextView.layer.borderWidth = 1
        messageTextView.layer.borderColor = UIColor(red: 199/255, green: 199/255, blue: 199/255, alpha: 1).cgColor
        messageTextView.clipsToBounds = true
        
        chatBoxContainerView.layer.borderWidth = 2
        chatBoxContainerView.layer.borderColor = UIColor(red: 119, green: 119, blue: 119, alpha: 1).cgColor
        chatBoxContainerView.layer.shadowOffset = CGSize(width: 0, height: -4)
        chatBoxContainerView.layer.shadowRadius = 4
        chatBoxContainerView.layer.shadowColor = UIColor.black.cgColor
        chatBoxContainerView.layer.shadowOpacity = 0.09
    }
    
    fileprivate func addNotifications() {
        
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.okBtnTapped(_:)), name: NSNotification.Name(rawValue: "OKButtonTapped"), object: nil)
        
       // NotificationCenter.default.addObserver(self, selector: #selector(tutorHasArrived), name: .tutorHasArrived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(yesButtonTapped), name: .yesBtnTapped, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(userConnected), name: .userConnected, object: nil)
        
    }
    
    fileprivate func removeNotifications() {
        
        // [START this will enable IQKeyboardManager for other controllers]
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
         // [END this will enable IQKeyboardManager for other controllers]
        
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        notificationCenter.removeObserver(self)
    }
    
    fileprivate func showHideEditCancleBtn(){
        
        if let vc = self.navigationController?.getPreviousViewController(){
            if vc is EdgemOfferViewController{
                editCancleHeight.constant = self.makeOfferRequst.status == "PaymentPending" ? 0 : 90
                editCacleOfferContainerView.isHidden  = self.makeOfferRequst.status == "PaymentPending" ? true : false
            }else{
                editCancleHeight.constant = self.bookingDetails?.status == "PaymentPending" ? 0 : 90
                editCacleOfferContainerView.isHidden = self.bookingDetails?.status == "PaymentPending" ? true : false
            }
        }
    }
    
    fileprivate func configureCalanderView() {
        
        datePicker = UIDatePicker.init()
        datePicker.backgroundColor = UIColor.white
        datePicker.minimumDate = Date()
        datePicker.autoresizingMask = .flexibleWidth
        datePicker.datePickerMode = .date
        
        datePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
        
        datePicker.frame = CGRect(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(datePicker)
        
        toolBar = UIToolbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.isTranslucent = true
        toolBar.barStyle = UIBarStyle.default
        toolBar.barTintColor = theamAppColor
        
        let cancleBtn = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.onCancleButtonClick))
        cancleBtn.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: theamBlackColor], for: .normal)
        
        let doneBtn = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.onDoneButtonClick))
        doneBtn.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: theamBlackColor], for: .normal)
 
        toolBar.items = [cancleBtn, UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), doneBtn]
      
        toolBar.sizeToFit()
        self.view.addSubview(toolBar)
        UIView.animate(withDuration: 0) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    fileprivate func registerCell() {
        tableView.register(UINib(nibName: StaticMessageInChatTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: StaticMessageInChatTableViewCell.cellIdentifire())
        tableView.register(ChatMessageCell.self, forCellReuseIdentifier: ChatMessageCell.cellIdentifire())
        tableView.register(UINib(nibName: ChatCustomButtomTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: ChatCustomButtomTableViewCell.cellIdentifire())
        tableView.register(UINib(nibName: EndTutionBtnTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: EndTutionBtnTableViewCell.cellIdentifire())
        tableView.register(UINib(nibName: StudentAddressTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: StudentAddressTableViewCell.cellIdentifire())
        tableView.register(UINib(nibName: AcceptCancleTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: AcceptCancleTableViewCell.cellIdentifire())
        tableView.register(UINib(nibName: ArrivedBtnTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: ArrivedBtnTableViewCell.cellIdentifire())
        tableView.register(UINib(nibName: LeftImageTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: LeftImageTableViewCell.cellIdentifire())
        tableView.register(UINib(nibName: RightImageTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: RightImageTableViewCell.cellIdentifire())
    }
    
    fileprivate func toolTipSetUP() {
        preferences = EasyTipView.Preferences()
        preferences.drawing.arrowPosition = .bottom
        preferences.drawing.textAlignment = .center
        preferences.drawing.font = UIFont(name: QuicksandMedium, size: 15)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.positioning.textHInset = 40
        preferences.positioning.textVInset = 10
        preferences.drawing.backgroundColor = UIColor(red: 23/255, green: 23/255, blue: 23/255, alpha: 0.80)
        preferences.animating.dismissTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1.5
        preferences.animating.dismissDuration = 1.5
        
        EasyTipView.globalPreferences = preferences
    }
}


