//
//  OfferTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 16/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class OfferTableViewCell: UITableViewCell {
    
    // MARK: ------ Properties
    var error: String?
    
   // MARK: ------ @IBOutlets
    @IBOutlet weak var cellTitleLabel: UILabel!
    @IBOutlet weak var currencySymbolLbl: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var perHourLabel: UILabel!
    @IBOutlet weak var ratestatus: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCellForOffer(_ index: Int, lock_rate_status: String, suggestedOffer: String){
        amountTextField.tag = index
        
        if lock_rate_status == "1" {
            amountTextField.isUserInteractionEnabled = false
            ratestatus.isHidden = false
            ratestatus.text = "Tutor’s rate is fixed".localized
        }else if lock_rate_status == "0"{
            amountTextField.isUserInteractionEnabled = true
            ratestatus.isHidden = false
            ratestatus.text = "Suggested offer: $\(suggestedOffer.changeStringNumberToWholeNumber())/hour".localized
        
        }else{
            ratestatus.isHidden = true
            amountTextField.isUserInteractionEnabled = true
        }
        amountTextField.text = suggestedOffer 

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        amountTextField.textColor = theamBlackColor
        amountTextField.font = UIFont(name: QuicksandRegular, size: 15)
        
        amountTextField.attributedPlaceholder = NSAttributedString(string: "Enter Amount*".localized, attributes: [NSAttributedString.Key.foregroundColor: theamBlackColor])
        
        amountTextField.layer.borderWidth = 1
        amountTextField.layer.borderColor = theamBorderGrayColor.cgColor
        amountTextField.layer.cornerRadius = 4
        amountTextField.clipsToBounds = true
        
        //ratestatus.text = "Suggested offer: $50/hour".localized
        ratestatus.font = UIFont(name: SourceSansProSemiBold, size: 15)
        ratestatus.textColor = theamGrayColor
//        ratestatus.isHidden = true
    }
    
    class func cellIdentifire() -> String{
        return "OfferTableViewCell"
    }

    func validateOffer(_ offer: String) {
        if offer.isEmpty{
            //self.error = "Please enter amount".localized
            errorLabel.text = "Please enter amount".localized
        }else if offer == "Enter Amount*".localized{
            //self.error = "Please enter amount".localized
            errorLabel.text = "Please enter amount".localized
        }else if !(offer.isValidAmmount()) {
            //self.error = "Please enter valid amount".localized
            errorLabel.text = "Please enter valid amount".localized
        }else{
           // self.error = ""
            errorLabel.text = ""
        }
    }
}
