//
//  OfferTotalAmmountTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 16/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class OfferTotalAmmountTableViewCell: UITableViewCell {
    
    var infoBtnStatus: ((UIButton)->Void)?
    
    // MARK: ------- @IBOutlets
    @IBOutlet weak var dotedView: UIView!
    @IBOutlet weak var totalAmtLabel: UILabel!
    @IBOutlet weak var approvedAmtLabel: UILabel!
    @IBOutlet weak var declinedAmtLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var ammountCut_View: UIView!
    @IBOutlet weak var infoBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dotedView.layer.addSublayer(DrawDottedLine.shared.drawDottedLines(start: CGPoint(x: dotedView.bounds.minX, y: dotedView.bounds.minY), end: CGPoint(x: ScreenWidth, y: dotedView.bounds.minY)))
    }
    
    
    func configureCell(_ index: Int, _ ammountWithOutDiscount: String, _ ammountWithDiscount: String) {
        approvedAmtLabel.text = "$\(ammountWithOutDiscount)"
        declinedAmtLabel.isHidden = true
         statusLabel.isHidden = true
         infoBtn.isHidden = true
        ammountCut_View.isHidden = true
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //approvedAmtLabel.text = "$0.00"
        //declinedAmtLabel.isHidden = true
       // statusLabel.isHidden = true
        //ammountCut_View.isHidden = true
    }
    
    class func cellIdentifire() ->String {
        return "OfferTotalAmmountTableViewCell"
    }
    
    @IBAction func didTapInfoBtn(_ sender: UIButton) {
        print("i button tapped")
        if sender.isSelected == false{
            sender.isSelected = true
        }else{
            sender.isSelected = false
        }
        infoBtnStatus?(sender)
    }
    
}
