//
//  OfferForOptionsTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 16/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class OfferForOptionsTableViewCell: UITableViewCell {
    
     // MARK: ---- Properties
    var error: String = ""
    
    // MARK: ---- @IBOutlets
    @IBOutlet weak var forLabel: UILabel!
    @IBOutlet weak var optionTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var dropDownImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        optionTextField.textColor = theamBlackColor
        optionTextField.font = UIFont(name: QuicksandRegular, size: 15)
        optionTextField.layer.borderWidth = 1
        optionTextField.layer.borderColor = theamBorderGrayColor.cgColor
        optionTextField.layer.cornerRadius = 4
        optionTextField.clipsToBounds = true
        
    }
    
    func configureCell(_ index: Int ) {
         optionTextField.tag = index
       
    }
    
    func configureCellForHours(_ index: Int){
         optionTextField.tag = index
    }
    

    class func cellIdentifire() -> String {
        return "OfferForOptionsTableViewCell"
    }
    
    func validateSubjects(_ subject: String) {
        if subject.isEmpty {
            error = "Please enter subject".localized
            errorLabel.text = "Please enter subject".localized
        }else if subject == "Select Subject"{
            error = "Please enter subject".localized
             errorLabel.text = "Please enter subject".localized
        }else{
            error = ""
            errorLabel.text = ""
        }
    }
    
    func validateLevel(_ level: String) {
        if level.isEmpty {
            error = "Please enter level".localized
            errorLabel.text = "Please enter level".localized
        }else if level == "Select Level"{
            error = "Please enter level".localized
            errorLabel.text = "Please enter level".localized
        }else{
            error = ""
            errorLabel.text = ""
        }
    }

}
