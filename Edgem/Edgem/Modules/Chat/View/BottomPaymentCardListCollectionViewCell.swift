//
//  BottomPaymentCardListCollectionViewCell.swift
//  Edgem
//
//  Created by Namespace on 15/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class BottomPaymentCardListCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Properties
    var image: UIImage!
    
     // MARK: - @IBActions
    
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var selectCardBtn: UIButton!
    @IBOutlet weak var cardName: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var cardContainerView: UIView!
    @IBOutlet weak var cvvtextField: UITextField!
    @IBOutlet weak var buttonContainerView: UIView!
    
    class func cellIdentifire()->String{
        return "BottomPaymentCardListCollectionViewCell"
    }
    
    func configureCell(with card: StoreUserCard, _ index: Int) {
        cvvtextField.tag = index
        let last4 = card.cardNumber.suffix(4)
        cardNumberLabel.text =  (String(last4))
        cardName.text = card.nameOnCard
        
        image = card.isPrimary == 1 ? #imageLiteral(resourceName: "genderSelected")  : #imageLiteral(resourceName: "genderDeselcetd")
        
        selectCardBtn.setImage(image, for: .normal)
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        cardContainerView.layer.borderWidth = 1
        cardContainerView.layer.borderColor = cellBackgroundColor.cgColor
        cardContainerView.layer.cornerRadius = 5
        
        cardContainerView.layer.shadowColor = UIColor.black.cgColor
        cardContainerView.layer.shadowOpacity = 0.12
        cardContainerView.layer.shadowOffset = CGSize(width: 0, height: 4)
        cardContainerView.layer.shadowRadius = 6
        //buttonContainerView.layer.shouldRasterize = true
        
        cvvtextField.layer.cornerRadius = 5
        cvvtextField.clipsToBounds = true
        
        buttonContainerView.layer.borderWidth = 1
        buttonContainerView.layer.borderColor = theamGrayColor.cgColor
        buttonContainerView.layer.cornerRadius = 5

    }
    
}
