//
//  ActivityCompletionStatusCollectionViewCell.swift
//  Edgem
//
//  Created by Namespace on 17/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class ActivityCompletionStatusCollectionViewCell: UICollectionViewCell {
    
    var imageName:String?
    
    // MARK: ---- @IBOutlets
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var leftLine: UIView!
    @IBOutlet weak var rightLine: UIView!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        statusLabel.textColor = theamBlackColor
    }
    
    class func cellIdentifire() -> String {
        return "ActivityCompletionStatusCollectionViewCell"
    }
    
    func configureCell(withProgressBarData progressBarData: ProgressBarData, currentIndex: Int, withTotalElement totalElement: Int){
        
        lineAppearence(currentIndex, totalElement-1)               // // ----> For showing lines

        if currentIndex > 2 && currentIndex < totalElement-1{
               self.statusLabel.attributedText = setLabel(progressBarData.label)
        }else{
               self.statusLabel.text = progressBarData.label
        }
      
        statusImage.image = statusImage(progressBarData.status)
      
    }
    
    func configureCellForCancleOffer(withProgressBarData progressBarData: ProgressBarData, currentIndex: Int, withTotalElement totalElement: Int){
        
        lineAppearence(currentIndex, totalElement-1)               // // ----> For showing lines
        
        if currentIndex > 2 && currentIndex < totalElement-1{
            self.statusLabel.attributedText = setLabel(progressBarData.label)
        }else{
            self.statusLabel.text = progressBarData.label
        }
        
       // statusImage.image = statusImage(progressBarData.status)
        
        rightLine.backgroundColor = UIColor.red//UIColor(red: 15/255, green: 215/255, blue: 18/255, alpha: 1)
        leftLine.backgroundColor =  UIColor.red//UIColor(red: 15/255, green: 215/255, blue: 18/255, alpha: 1)
        statusLabel.font = UIFont(name: QuicksandMedium, size: 15)
       statusImage.image = #imageLiteral(resourceName: "cancel_activity")
        
    }
    
    private func setLabel(_ text: String) -> NSMutableAttributedString{
        
        switch text{
        case "2nd \n Lesson":
            
            return attributedText(text)

        case "1st \n Lesson":

            return attributedText(text)

        case "3rd \n Lesson":
            
            return attributedText(text)
            
        case "4th \n Lesson":
            
            return attributedText(text)
         
        case "5th \n Lesson":
            return attributedText(text)
          
        case "6th \n Lesson":
            return attributedText(text)

        case "7th \n Lesson":
            return attributedText(text)
         
        case "8th \n Lesson":
            return attributedText(text)
         
        default:
            return attributedText(text)
        }
    }
    
    private func attributedText(_ text: String) ->NSMutableAttributedString{
        let font:UIFont? = UIFont(name: QuicksandRegular, size:15)
        let fontSuper:UIFont? = UIFont(name: QuicksandRegular, size:13)
        let attString:NSMutableAttributedString = NSMutableAttributedString(string: text, attributes: [.font:font!])
        attString.setAttributes([.font:fontSuper!,.baselineOffset:10], range: NSRange(location:1,length:2))
        return attString
    }
    
    private func lineAppearence(_ index: Int, _ lastIndex: Int) {
        if index == 0{
            rightLine.isHidden = true
            leftLine.isHidden = false
        }else if index == lastIndex {
            leftLine.isHidden = true
            rightLine.isHidden = false
        }else{
            leftLine.isHidden = false
            rightLine.isHidden = false
        }
        
    }
    
    
    
    private func statusImage(_ status: Int) -> UIImage? {
        switch status{
        case 0 :
    
            rightLine.backgroundColor = UIColor(red: 15/255, green: 215/255, blue: 18/255, alpha: 1)
            leftLine.backgroundColor =  UIColor(red: 15/255, green: 215/255, blue: 18/255, alpha: 1)
            statusLabel.font = UIFont(name: QuicksandMedium, size: 15)
            return  #imageLiteral(resourceName: "Active")
        
        case 1 :
            rightLine.backgroundColor = UIColor(red: 15/255, green: 215/255, blue: 18/255, alpha: 1)
            leftLine.backgroundColor =  UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
            statusLabel.font = UIFont(name: QuicksandMedium, size: 15)
            return  #imageLiteral(resourceName: "PartiallyActive")
            
        case -1 :
            rightLine.backgroundColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
            leftLine.backgroundColor =  UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
            statusLabel.font = UIFont(name: QuicksandRegular, size: 15)
            return  #imageLiteral(resourceName: "InActiveRadio")
            
        case 2 :
            rightLine.backgroundColor = UIColor(red: 15/255, green: 215/255, blue: 18/255, alpha: 1)
            return  #imageLiteral(resourceName: "completed")
        default:
            return nil
        }
    }
    
}
