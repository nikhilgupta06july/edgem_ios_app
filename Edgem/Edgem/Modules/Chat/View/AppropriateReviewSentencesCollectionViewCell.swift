//
//  AppropriateReviewSentencesCollectionViewCell.swift
//  Edgem
//
//  Created by Namespace on 03/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class AppropriateReviewSentencesCollectionViewCell: UICollectionViewCell {
    
     // MARK: - Properties
    
    class var reuseIdentifier: String {
        return "AppropriateReviewSentencesCollectionViewCell"
    }
    class var nibName: String {
        return "AppropriateReviewSentencesCollectionViewCell"
    }
    
    // MARK: - @IBOutlets
    
    @IBOutlet weak var reviewLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        reviewLabel.font = UIFont(name: QuicksandMedium, size: 15)
        reviewLabel.textColor = theamBlackColor
        reviewLabel.backgroundColor = .white
        reviewLabel.layer.cornerRadius = 15
        reviewLabel.layer.borderWidth = 1
        reviewLabel.layer.borderColor = staticMessageCellBorderColor.cgColor
        reviewLabel.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // reviewLabel properties
//        reviewLabel.font = UIFont(name: QuicksandMedium, size: 15)
//        reviewLabel.textColor = theamBlackColor
//        reviewLabel.backgroundColor = .white
//        reviewLabel.layer.cornerRadius = 15
//        reviewLabel.layer.borderWidth = 1
//        reviewLabel.layer.borderColor = staticMessageCellBorderColor.cgColor
//        reviewLabel.clipsToBounds = true
    }
    
     // MARK: - Helping Functions
    
    func configureCell(with sentence: String){
        self.reviewLabel.text = sentence
    }

}
