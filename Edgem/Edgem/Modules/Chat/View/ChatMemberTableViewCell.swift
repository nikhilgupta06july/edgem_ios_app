//
//  ChatMemberTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 02/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class ChatMemberTableViewCell: UITableViewCell {
    
    var userID: Int?
    
    @IBOutlet weak var offerCancelledLabel: UILabel!
    @IBOutlet weak var offerCancelledContainerView: UIView!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var timeSpanLabel: UILabel!
    @IBOutlet weak var memberNameLabel: UILabel!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var memberProfileImage: UIImageView!
    @IBOutlet weak var onlineStatus: UIImageView!
    @IBOutlet weak var unreadMessagesCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        offerCancelledLabel.font = UIFont(name: SourceSansProRegular, size: 12)
        
        unreadMessagesCount.font = UIFont(name: SourceSansProRegular, size: 15)
        unreadMessagesCount.textColor = theamBlackColor
        unreadMessagesCount.backgroundColor = theamAppColor
        memberNameLabel.font = UIFont(name: SourceSansProBold, size: 15)
        memberNameLabel.textColor = theamBlackColor
        
        lastMessageLabel.font = UIFont(name: SourceSansProRegular, size: 14)
        lastMessageLabel.textColor = theamBlackColor
        
        sepratorView.backgroundColor = theamBorderGrayColor
        
        timeSpanLabel.font = UIFont(name: SourceSansProRegular, size: 12)
        timeSpanLabel.textColor = theamGrayColor
        
        unreadMessagesCount.layer.cornerRadius =  unreadMessagesCount.frame.width/2
        unreadMessagesCount.clipsToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(userConnected), name: .userConnected, object: nil)
    }
    
    @objc func userConnected(){
        print("user online")
        
        if let userId = self.userID {
            let image = (Global.idArrays.contains(userId)) ? "user_online" : "user_offline"
            self.onlineStatus.image = UIImage(named: image)
        }

    }
    
    func configureCell(with bookingDetails: BookingDetails){
        
        if AppDelegateConstant.user?.ID == bookingDetails.offeredByID{
              memberNameLabel.text = "\(String(describing: bookingDetails.offeredToName))(\(String(describing: bookingDetails.subjectName)))"
            self.userID = bookingDetails.offeredToID
        }else{
             memberNameLabel.text = "\(String(describing: bookingDetails.offeredByName))(\(String(describing: bookingDetails.subjectName)))"
             self.userID = bookingDetails.offeredByID
        }
        
        // total unread messages
        if bookingDetails.unreadCount > 0{
             self.unreadMessagesCount.isHidden = false
             self.unreadMessagesCount.text = "\(bookingDetails.unreadCount)"
        }else{
            self.unreadMessagesCount.isHidden = true
        }
       
        
        // load profile images
        loadImages(bookingDetails)
        
        // Last message
        if bookingDetails.lastmessage.messageType == "\(MessageType.MESSAGE_TYPE_MIME)"{
              lastMessageLabel.text = "Image".localized
        }else if bookingDetails.lastmessage.messageType == "\(MessageType.MESSAGE_TYPE_ADDRESS_ALERT)"{
            
            if AppDelegateConstant.user?.userType == tutorType{
                 lastMessageLabel.text = "Student's address is:"
            }else{
                if let id = bookingDetails.schedule_cancel_by, id == AppDelegateConstant.user?.ID{
                    lastMessageLabel.text = who_sch_who_scr_class_prefix
                }else{
                    lastMessageLabel.text = tut_sch_stu_scr
                }
            }
 
        } else{
            
            let lastMessage = bookingDetails.lastmessage.conversationMessage
            
            if lastMessage.contains("DYNAMIC_DATA"){
                
                var msg = ""
                
                var replace = ""
                
                var sufix = ""
                
                if AppDelegateConstant.user?.ID == bookingDetails.schedule_cancel_by {
                    
                    replace = who_sch_who_scr_class_prefix
                    
                    if AppDelegateConstant.user?.userType == tutorType {
                        
                        sufix = tut_sch_tut_scr__sch_class_sufix
                        
                    }else{
                        
                        sufix = stu_sch_stu_scr_class_sufix
                        
                    }
                    
                    
                }else{
                    
                    if AppDelegateConstant.user?.userType == tutorType {
                        
                        replace = stu_sch_tut_scr
                        
                    }else{
                        
                        replace = tut_sch_stu_scr
                    }
                    
                }
                
               // msg = (bookingDetails.lastmessage.conversationMessage).replacingOccurrences(of: "DYNAMIC_DATA", with: replace)
                
                lastMessageLabel.text = replace//bookingDetails.lastmessage.conversationMessage
                
            }else{
        
                if bookingDetails.lastmessage.conversationMessage == "OFFER_COUNTER"{
                    lastMessageLabel.text = "Offer countered"
                }else{
                     lastMessageLabel.text =  bookingDetails.lastmessage.conversationMessage
                }
            }
            
            
        }
        
        // time span
       
        let date = bookingDetails.lastmessage.sendedOn
        if date != "" {
            if let dt1 = date.stringToDate(date: date, format: "yyyy-MM-dd HH:mm:ss"){
                timeSpanLabel.text = dt1.timeAgoDisplay()
            }
        }
        
        // online status
        
        let image = bookingDetails.onlineStatus == 1 ? #imageLiteral(resourceName: "user_online") :#imageLiteral(resourceName: "user_offline")
        onlineStatus.image = image
        
        // offer cancelled label
        if bookingDetails.isCancelled == 1 || bookingDetails.status == "done"{
            self.offerCancelledContainerView.isHidden = false
            if bookingDetails.isCancelled == 1{
                
                self.offerCancelledLabel.text = "Offer Cancelled"
                self.offerCancelledLabel.textColor = .white
                self.offerCancelledContainerView.backgroundColor = UIColor(red: 240/255, green: 0/255, blue: 0/255, alpha: 1)
                
            }else{
                
                self.offerCancelledLabel.text = "Offer Completed"
                self.offerCancelledLabel.textColor = .white
                self.offerCancelledContainerView.backgroundColor = UIColor(red: 87/255, green: 190/255, blue: 54/255, alpha: 1)
            }
        }else{
            self.offerCancelledContainerView.isHidden = true
        }
//        if bookingDetails.status == "done"{
//            self.offerCancelledContainerView.isHidden = false
//            self.offerCancelledLabel.text = "Offer Completed"
//            self.offerCancelledContainerView.backgroundColor = .green
//        }else{
//             self.offerCancelledContainerView.isHidden = true
//        }
    }
    
    fileprivate func loadImages(_ bookingDetails: BookingDetails) {
        
        if AppDelegateConstant.user?.ID == bookingDetails.offeredByID{

            let imageURL = bookingDetails.offeredToImage
            if !imageURL.isEmpty{
                GetImageFromServer.getImage(imageURL: imageURL) { (image) in
                    self.memberProfileImage.image = image
                }
            }

        }else{
            let imageURL = bookingDetails.offeredByImage
            if !imageURL.isEmpty{
                GetImageFromServer.getImage(imageURL: imageURL) { (image) in
                    self.memberProfileImage.image = image
                }
            }
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        offerCancelledContainerView.layer.cornerRadius = 2
        offerCancelledContainerView.clipsToBounds = true
        
        self.memberProfileImage.layer.borderWidth = 1
        self.memberProfileImage.layer.borderColor = theamBlackColor.cgColor
        self.memberProfileImage.layer.cornerRadius = ( self.memberProfileImage.frame.width) / 2
        self.memberProfileImage.clipsToBounds = true
        
        unreadMessagesCount.layer.cornerRadius =  unreadMessagesCount.frame.width/2
        unreadMessagesCount.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
         self.memberProfileImage.image = nil
         lastMessageLabel.text = ""
        memberNameLabel.text = ""
    }
    
    class func cellIdentifire() -> String {
        return "ChatMemberTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 83
    }

}
