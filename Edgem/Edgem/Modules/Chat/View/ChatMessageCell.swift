//
//  ChatMessageCell.swift
//  Edgem
//
//  Created by Namespace on 30/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class ChatMessageCell: UITableViewCell {

    // MARK : - Properties
    
    var chatViewController: ChatViewController?
    var isMadeOfferMessage: Bool!
    var docImage: UIImage?
    let messageLabel = UILabel()
    let bubbleBackgroundView = UIView()
    var profileImageView = UIImageView()
    var userTypeLabel = PaddingLbl()
    var infoBtn = UIButton()
    lazy var messageImageView = UIImageView()
    let timeLabel = UILabel()
    let seenStatusImage = UIImageView()
    
    lazy var pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.pinch(sender:)))
    
    var messageImageViewHeight: NSLayoutConstraint!
     var height:CGFloat = 0.0
    var leadingConstraint: NSLayoutConstraint!
    var trailConstraint: NSLayoutConstraint!
    
    var timelabelLeadingConstraint: NSLayoutConstraint!
    var timelabeltrailConstraint: NSLayoutConstraint!
    
    fileprivate func configureMadeOfferCell(_ message: ChatMessage) {

        
        if message.viewType == MessageType.MESSAGE_TYPE_TEXT || message.viewType == MessageType.MESSAGE_TYPE_OFFERACCEPTED || message.viewType == MessageType.MESSAGE_TYPE_SCHEDULE_REQ{
            
            messageLabel.isHidden = false
            messageImageView.isHidden = true
            messageImageViewHeight.isActive = false
            messageImageViewHeight = messageImageView.heightAnchor.constraint(equalToConstant: height)
            self.seenStatusImage.image = message.isRead == 1 ? #imageLiteral(resourceName: "seen_image") : #imageLiteral(resourceName: "unseen_image")
            
            if let time = message.sendedOn{
                self.timeLabel.text = Utilities.UTCToLocal(date: time, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "h:mm a")
            }
            
            if message.conversationMessage.contains("Made an offer"){
                
                infoBtn.isHidden = ((message.discountedPrice?.isEmpty)!) ? true : false
                
                if let discount = message.discountedPrice{
                      infoBtn.isHidden = discount.isEmpty ? true : false
                }
                
                var str = message.conversationMessage
                str = str.replacingOccurrences(of: "Made an offer", with: "")
                let attributedText = NSMutableAttributedString(string: "Made an offer".localized, attributes: [NSAttributedString.Key.font : UIFont(name: SourceSansProRegular, size: 15)!])
                attributedText.append(NSAttributedString(string: str, attributes: [NSAttributedString.Key.font : UIFont(name: SourceSansProRegular, size: 18)!]))
                
                messageLabel.attributedText = attributedText
                
            }else if message.conversationMessage.contains("Offer countered"){
                
                var str = message.conversationMessage
                str = str.replacingOccurrences(of: "Offer countered", with: "")
                let attributedText = NSMutableAttributedString(string: "Offer countered".localized, attributes: [NSAttributedString.Key.font : UIFont(name: SourceSansProRegular, size: 15)!])
                attributedText.append(NSAttributedString(string: str, attributes: [NSAttributedString.Key.font : UIFont(name: SourceSansProRegular, size: 18)!]))
                
                messageLabel.attributedText = attributedText
                
            }else{
                messageLabel.text = message.conversationMessage
            }
        }else{
            
            infoBtn.isHidden = true
            messageLabel.isHidden = true
            messageImageView.isHidden = false
            messageImageViewHeight = messageImageView.heightAnchor.constraint(equalToConstant: height)
            messageImageViewHeight.isActive = true
            messageImageView.image = docImage ?? nil
            
        }

    }
    
    func configureCell(with message: ChatMessage){

        infoBtn.isHidden = true
        if message.senderID == AppDelegateConstant.user?.ID {
            
            configureMadeOfferCell(message)
            
            seenStatusImage.isHidden = false
            bubbleBackgroundView.backgroundColor = UIColor(red: 251/255, green: 134/255, blue: 42/255, alpha: 1)
            messageLabel.textColor =  .white
            profileImageView.isHidden = true
            userTypeLabel.isHidden = true
            timelabelLeadingConstraint.isActive = false
            timelabeltrailConstraint.isActive = true
            leadingConstraint.isActive = false
            trailConstraint.isActive = true
            bubbleBackgroundView.layer.shadowOffset = CGSize(width: -6, height: 6)
            if #available(iOS 11.0, *) {
                bubbleBackgroundView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner]
            } else {
                // Fallback on earlier versions
            }
            
        }else{

            configureMadeOfferCell(message)
            
            seenStatusImage.isHidden = true
            bubbleBackgroundView.backgroundColor = .white
            messageLabel.textColor =  theamBlackColor
            timelabelLeadingConstraint.isActive = true
            timelabeltrailConstraint.isActive = false
            leadingConstraint.isActive = true
            trailConstraint.isActive = false
            profileImageView.isHidden = false
            //userTypeLabel.isHidden = false
            /// [START logic to show user type label]
            
            if UserStore.shared.selectedUserType != tutorType {
                
                userTypeLabel.isHidden = true
                
                userTypeLabel.text = ""
                
            }else{
                
               // if UserStore.shared.selectedUserType == parentType{
                
                if let loginType  = message.loginType, loginType != 1 && loginType != 2{
                    
                    userTypeLabel.isHidden = true
                    
                    userTypeLabel.text = ""
                    
                }else{
                    
                    userTypeLabel.text = message.loginType == 1 ? "PARENT".localized : "CHILD".localized
                    
                    userTypeLabel.isHidden = false
                    
                }
                    
//                    if message.loginType != 1 && UserStore.shared.logInType  != 2{
//
//                        userTypeLabel.isHidden = true
//
//                        userTypeLabel.text = ""
//
//                    }else{
//
//                        userTypeLabel.text = message.loginType == 1 ? "PARENT".localized : "CHILD".localized
//
//                        userTypeLabel.isHidden = false
//
//                    }
              //  }
                
            }

            /// [END logic to show user type label]
            
            loadProfileImages(message)
            bubbleBackgroundView.layer.shadowOffset = CGSize(width: 6, height: 6)
            if #available(iOS 11.0, *) {
                bubbleBackgroundView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    fileprivate func loadProfileImages(_ message: ChatMessage) {
        
        let imageProvider = ImageProvider()
        if let image = imageCache.image(forKey: "\(String(describing: message.realImage))"){
            self.profileImageView.image = image
        }else{
            profileImageView.image = UIImage(named: "femaleIcon")
            
            if let imageURL = message.realImage{
                if let url = URL(string: imageURL) {
                    imageProvider.requestImage(from: url) { (image) in
                        imageCache.add(image, forKey: "\(String(describing: message.realImage))")
                        self.profileImageView.image = image
                    }
                }
            }
        }
        
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        cellViewLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func cellIdentifire() -> String {
        return "ChatMessageCell"
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.height = 0.0
        messageLabel.text = ""
        docImage = nil
        messageImageView.image = nil
        messageImageViewHeight.constant = 0.0
        seenStatusImage.image = nil

    }
    
}


extension ChatMessageCell {
    
    @objc func handleZoomTap(tapGesture: UITapGestureRecognizer){
        let imageView = tapGesture.view as? UIImageView
        self.chatViewController?.performZoomInForStartingImage(imageView!)
    }
    
    @objc func pinch(sender:UIPinchGestureRecognizer){
        
        if sender.state == .began || sender.state == .changed {
            let currentScale = self.messageImageView.frame.size.width / self.messageImageView.bounds.size.width
            let newScale = currentScale*sender.scale
            let transform = CGAffineTransform(scaleX: newScale, y: newScale)
            self.messageImageView.transform = transform
            sender.scale = 1
        }
        
    }
    
    @objc func buttonAction(sender: UIButton!) {
        self.chatViewController?.showPopUPForDiscountedAmt(sender)
    }
    
    fileprivate func cellViewLayout() {
        
        backgroundColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1)
        self.contentView.backgroundColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1)
     
        self.selectionStyle = .none
        
        bubbleBackgroundView.backgroundColor = UIColor(red: 251/255, green: 134/255, blue: 42/255, alpha: 1)
        bubbleBackgroundView.layer.cornerRadius = 15
        
        bubbleBackgroundView.layer.shadowColor = UIColor.black.cgColor
        bubbleBackgroundView.layer.shadowOpacity = 0.17
        bubbleBackgroundView.layer.shadowRadius = 2.7
        
        bubbleBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(bubbleBackgroundView)
        messageLabel.numberOfLines = 0
        messageLabel.font = UIFont(name: SourceSansProRegular, size: 15)
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(messageLabel)
        
        timeLabel.font = UIFont(name: SourceSansProRegular, size: 15)
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(timeLabel)
        
        seenStatusImage.contentMode = .scaleAspectFit
        seenStatusImage.layer.masksToBounds = true
        seenStatusImage.translatesAutoresizingMaskIntoConstraints = false
        addSubview(seenStatusImage)
        
        infoBtn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        infoBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        infoBtn.translatesAutoresizingMaskIntoConstraints = false
        addSubview(infoBtn)
        
        userTypeLabel.layer.cornerRadius = 3
        userTypeLabel.layer.masksToBounds = true
        userTypeLabel.textColor = .white
        userTypeLabel.backgroundColor = theamBlackColor
        userTypeLabel.font = UIFont(name: SourceSansProBold, size: 8)
        userTypeLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(userTypeLabel)
        
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.image = #imageLiteral(resourceName: "maleIcon")
        profileImageView.layer.cornerRadius = 15
        profileImageView.layer.masksToBounds = true
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(profileImageView)
        
        messageImageView.isUserInteractionEnabled = true
        messageImageView.contentMode = .scaleAspectFill
        messageImageView.layer.cornerRadius = 5
        messageImageView.layer.masksToBounds = true
        messageImageView.translatesAutoresizingMaskIntoConstraints = false
        
        messageImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomTap)))
        
        bubbleBackgroundView.addSubview(messageImageView)
        
        let constraint = [
            
                          messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: 32),
                          messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -32),
                          messageLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 200),
                          
                          infoBtn.widthAnchor.constraint(equalToConstant: 15),
                          infoBtn.heightAnchor.constraint(equalToConstant: 15),
                          infoBtn.topAnchor.constraint(equalTo: bubbleBackgroundView.topAnchor, constant: 8),
                          infoBtn.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 0),
//
                          userTypeLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
                          userTypeLabel.bottomAnchor.constraint(equalTo: profileImageView.topAnchor, constant: -5),
                          
                          profileImageView.widthAnchor.constraint(equalToConstant: 30),
                          profileImageView.heightAnchor.constraint(equalToConstant: 30),
                          profileImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
                          profileImageView.bottomAnchor.constraint(equalTo: bottomAnchor),
                          
                         seenStatusImage.topAnchor.constraint(equalTo: bubbleBackgroundView.bottomAnchor, constant: 5),
                         seenStatusImage.widthAnchor.constraint(equalToConstant: 20),
                         seenStatusImage.heightAnchor.constraint(equalToConstant: 12),
 
            bubbleBackgroundView.topAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -16),
            bubbleBackgroundView.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -16),
            bubbleBackgroundView.bottomAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 16),
            bubbleBackgroundView.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 16),
            
            messageImageView.leftAnchor.constraint(equalTo: bubbleBackgroundView.leftAnchor, constant: 8),
            messageImageView.topAnchor.constraint(equalTo: bubbleBackgroundView.topAnchor, constant: 8),
            messageImageView.rightAnchor.constraint(equalTo: bubbleBackgroundView.rightAnchor, constant: -8),
            // messageImageView.heightAnchor.constraint(equalToConstant: 300),
            messageImageView.bottomAnchor.constraint(equalTo: bubbleBackgroundView.bottomAnchor, constant: -8),
            
            timeLabel.topAnchor.constraint(equalTo: bubbleBackgroundView.bottomAnchor, constant: 5),
            timeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            timeLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 200),
            timeLabel.trailingAnchor.constraint(equalTo: seenStatusImage.leadingAnchor, constant: -6)
        ]
        
        NSLayoutConstraint.activate(constraint)
        
        leadingConstraint = messageLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 28)
        leadingConstraint.isActive = false
        
        trailConstraint = messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -28)
        trailConstraint.isActive = true

        timelabelLeadingConstraint = timeLabel.leadingAnchor.constraint(equalTo: bubbleBackgroundView.leadingAnchor)
        timelabelLeadingConstraint.isActive = true
        
        timelabeltrailConstraint = timeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -40)
        timelabeltrailConstraint.isActive = false
        
        messageImageViewHeight = messageImageView.heightAnchor.constraint(equalToConstant: height)
        messageImageViewHeight.isActive = false
    }
    
}
