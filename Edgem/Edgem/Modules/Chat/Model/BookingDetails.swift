//
//  BookingDetails.swift
//  Edgem
//
//  Created by Namespace on 02/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class BookingDetails: Unboxable {
    
    // Properties
    var offerID: Int
    var subjectID: Int
    var levelID: Int
    var subjectName: String
    var levelName: String
    var offeredByID: Int
    var offeredByName: String
    var offeredByImage: String
    var offeredToID: Int
    var offeredToName: String
    var offeredToImage: String
    var offeredRate: String
    var spanOf: Double
    var forLessons: Int
    var totalAmmount: String
    var status: String
    var onlineStatus: Int
    var lastmessage: LastMessageUnboxable
    var unreadCount: Int
    var isCancelled: Int
    var paymentAt: String?
    var negotiableStatus: Int?
    var isRated: Int?
    var lockRate: String?
    var bookingTime: String?
    var schedule_cancel_by: Int?
    var discountedPrice: String?
    var isRescheduled: Int?
    
    init(booking: [String:AnyObject]){
        self.offerID = booking["offer_id"] as! Int
        self.levelID = booking["level_id"] as? Int ??  Int(booking["level_id"] as! String) ?? 0
        self.levelName = booking["level_title"]  as? String ?? ""
        self.subjectID =  booking["subject_id"] as? Int ??  Int(booking["subject_id"] as! String) ?? 0
        self.subjectName =  booking["subject_name"]  as? String ?? ""
        self.offeredByID =  booking["offered_by_id"] as!  Int
        self.offeredByName =  booking["offered_by_name"] as! String
        self.offeredToID =  booking["offered_to_id"] as! Int
        self.offeredToName =  booking["offered_to_name"] as! String
        self.offeredRate =  booking["rate_offered"] as! String
        self.spanOf =  booking["span_of"] as? Double ??  Double(booking["span_of"] as! String) ?? 0
        self.forLessons =  booking["for_lessons"] as? Int ??  Int(booking["for_lessons"] as! String) ?? 0
        self.totalAmmount =  booking["total_amount"] as! String
        self.status =  booking["status"] as! String
        self.lastmessage =   LastMessageUnboxable(lastMessage: booking["last_message"] as! [String:AnyObject])
        self.offeredByImage =  booking["offered_by_real_image"] as! String
        self.offeredToImage =  booking["offered_to_real_image"] as! String
        self.onlineStatus =  booking["online_status"] as! Int
        self.unreadCount = booking["un_read_count"] as! Int
        self.isCancelled = booking["is_cancelled"] as! Int
        self.paymentAt = booking["booking_time"] as? String
       // self.bookingTime =  booking["booking_time"] as? String//self.paymentAt = booking["payment_at"] as? String
        self.negotiableStatus = booking["non_negotiable"] as? Int
        self.isRated = booking[" is_rated"] as? Int
        self.lockRate =  booking["lockRate"] as? String
        self.schedule_cancel_by =  booking["schedule_cancel_by"] as? Int
        self.discountedPrice =  booking["discount_price"] as? String
        self.isRescheduled = booking["is_rescheduled"] as? Int
    }
    
    init(booking:BookingDetails){
        self.offerID = booking.offerID
        self.subjectID = booking.subjectID
        self.subjectName = booking.subjectName
        self.offeredByID = booking.offeredByID
        self.offeredByName = booking.offeredByName
        self.offeredToID = booking.offeredToID
        self.offeredToName = booking.offeredToName
        self.offeredRate = booking.offeredRate
        self.spanOf = booking.spanOf
        self.forLessons = booking.forLessons
        self.totalAmmount = booking.totalAmmount
        self.status = booking.status
        self.lastmessage = booking.lastmessage
        self.offeredByImage = booking.offeredByImage
        self.offeredToImage = booking.offeredToImage
        self.onlineStatus = booking.onlineStatus
        self.unreadCount = booking.unreadCount
        self.isCancelled = booking.isCancelled
        self.paymentAt = booking.paymentAt
        self.negotiableStatus = booking.negotiableStatus
        self.isRated = booking.isRated
        self.lockRate = booking.lockRate
        self.bookingTime = booking.bookingTime
        self.schedule_cancel_by = booking.schedule_cancel_by
        self.discountedPrice = booking.discountedPrice
        self.levelID = booking.levelID
        self.levelName = booking.levelName
        self.isRescheduled = booking.isRescheduled
    }
    
    required init(unboxer: Unboxer) throws {
        self.offerID = unboxer.unbox(key: "offer_id") ?? 0
        self.subjectID = unboxer.unbox(key: "subject_id") ?? 0
        self.subjectName = unboxer.unbox(key: "subject_name") ?? ""
        self.offeredByID = unboxer.unbox(key: "offered_by_id") ?? 0
        self.offeredByName = unboxer.unbox(key: "offered_by_name") ?? ""
        self.offeredToID = unboxer.unbox(key: "offered_to_id") ?? 0
        self.offeredToName = unboxer.unbox(key: "offered_to_name") ?? ""
        self.offeredRate = unboxer.unbox(key: "rate_offered") ?? ""
        self.spanOf = unboxer.unbox(key: "span_of") ?? 0
        self.forLessons = unboxer.unbox(key: "for_lessons") ?? 0
        self.totalAmmount = unboxer.unbox(key: "total_amount") ?? ""
        self.status = unboxer.unbox(key: "status") ?? ""
        self.offeredToImage = unboxer.unbox(key: "offered_to_real_image") ?? ""
        self.offeredByImage = unboxer.unbox(key: "offered_by_real_image") ?? ""
        self.lastmessage = try unboxer.unbox(key: "last_message")
        self.onlineStatus = try unboxer.unbox(key: "online_status")
        self.unreadCount = try unboxer.unbox(key: "un_read_count")
        self.isCancelled = try unboxer.unbox(key: "is_cancelled")
        self.paymentAt =  unboxer.unbox(key: "booking_time")
        self.negotiableStatus = unboxer.unbox(key: "non_negotiable")
        self.isRated = unboxer.unbox(key: "is_rated")
        self.lockRate = unboxer.unbox(key: "lockRate")
        self.bookingTime = unboxer.unbox(key: "booking_time")
        self.schedule_cancel_by = unboxer.unbox(key: "schedule_cancel_by")
         self.levelID = try unboxer.unbox(key: "level_id") ?? 0
        self.discountedPrice = unboxer.unbox(key: "discount_price")
        self.levelName = try unboxer.unbox(key: "level_title") ?? ""
        self.isRescheduled = try unboxer.unbox(key: "is_rescheduled") ?? 0
    }

}

