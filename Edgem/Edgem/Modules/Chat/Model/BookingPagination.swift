//
//  BookingPagination.swift
//  Edgem
//
//  Created by Namespace on 02/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class BookingPagination: Unboxable{

    var bookingDetails: [BookingDetails]
    var pagination: Pagination
    
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        bookingDetails = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
        pagination = Pagination()
    }

    required init(unboxer: Unboxer) throws {
        self.bookingDetails = try unboxer.unbox(key: "bookings")
        self.pagination = try unboxer.unbox(key: "pagination")
        hasMoreToLoad = bookingDetails.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: BookingPagination) {
        
        switch self.paginationType {
        case .new, .reload:
            self.bookingDetails = [];
            self.bookingDetails = newPaginationObject.bookingDetails
            self.paginationType = .old
        case .old:
            self.bookingDetails += newPaginationObject.bookingDetails
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }


}


