//
//  File.swift
//  Edgem
//
//  Created by Namespace on 19/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation

struct MakeOfferRequst  : Codable {
    
    
    var offerID: Int?
    var ammountPerHour: String?
    var hours: String?
    var subjects: String?
    var subjectID: String?
    var levelID: String?
    var lessons: String?
    var level: String?
    var lockRate: String?
    var tutorName: String?
    var totalAmmount: String?
    var status: String?
    var offeredByID: Int?
    var offeredByName: String?
    var offeredToID: Int?
    var offeredToName: String?
    var negotiableStatus: Int?
    var lastmessage: LastMessage?
    var onlineStatus: Int?
    var offeredByImage: String?
    var offeredToImage: String?
    var paymentAt: String?
    var bookingTime: String?
    var schedule_cancel_by: Int?
    var isCancelled: Int?
    var discountedPrice: String?
    var isRescheduled: Int?

    enum CodingKeys: String, CodingKey {
        case offerID = "offer_id"
        case ammountPerHour = "rate_offered"
        case hours = "span_of"
        case subjects = "subject_name"
        case subjectID = "subject_id"
        case levelID = "level_id"
        case level = "level_title"
        case lessons = "for_lessons"
        case lockRate = "lockRate"
        case totalAmmount = "total_amount"
        case tutorName = "tutor_name"
        case status = "status"
        case offeredByID = "offered_by_id"
        case offeredByName = "offered_by_name"
        case offeredToID = "offered_to_id"
        case offeredToName = "offered_to_name"
        case lastmessage = "last_message"
        case onlineStatus = "online_status"
        case offeredByImage = "offered_by_real_image"
        case offeredToImage = "offered_to_real_image"
        case paymentAt = "booking_time"
       // case bookingTime = "booking_time"
        case  negotiableStatus = "non_negotiable"
        case  schedule_cancel_by = "schedule_cancel_by"
        case isCancelled = "is_cancelled"
        case discountedPrice = "discount_price"
        case isRescheduled = "is_rescheduled"
    }
    
    init(with dict: [String:Any]){
        
        offerID = dict["offer_id"] as? Int
        subjects = dict["subject_name"] as? String
        subjectID = dict["subject_id"] as? String
        ammountPerHour = dict["rate_offered"] as? String
        hours   = dict["span_of"] as? String
        lessons = dict["for_lessons"] as? String
        level = dict["level_title"] as? String
        levelID = dict["level_id"] as? String
        totalAmmount = dict["total_amount"] as? String
        status = dict["status"] as? String
        offeredByID = dict["offered_by_id"] as? Int
        offeredByName = dict["offered_by_name"] as? String
        offeredToID = dict["offered_to_id"] as? Int
        offeredToName = dict["offered_to_name"] as? String
        lastmessage = dict["last_message"] as? LastMessage
        onlineStatus = dict["online_status"] as? Int
        offeredByImage = dict["offered_by_real_image"] as? String
        offeredToImage = dict["offered_to_real_image"] as? String
        bookingTime =  dict["booking_time"] as? String
        paymentAt = dict["booking_time"] as? String
        negotiableStatus = dict["non_negotiable"] as? Int
        schedule_cancel_by = dict["schedule_cancel_by"] as? Int
        isCancelled = dict["is_cancelled"] as? Int
        discountedPrice = dict["discount_price"] as? String
        isRescheduled = dict["is_rescheduled"] as? Int
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        offerID = try values.decodeIfPresent(Int.self, forKey: .offerID)
        subjects = try values.decodeIfPresent(String.self, forKey: .subjects)
        subjectID = try values.decodeIfPresent(String.self, forKey: .subjectID)
        tutorName = try values.decodeIfPresent(String.self, forKey: .tutorName)
        ammountPerHour = try values.decodeIfPresent(String.self, forKey: .ammountPerHour)
        hours   = try values.decodeIfPresent(String.self, forKey: .hours  )
        lessons = try values.decodeIfPresent(String.self, forKey: .lessons)
        level =  try values.decodeIfPresent(String.self, forKey: .level)
        levelID =  try values.decodeIfPresent(String.self, forKey: .levelID)
        if let levelId = try values.decodeIfPresent(String.self, forKey: .levelID){
            levelID = levelId
        }else{
            levelID = "\(try values.decodeIfPresent(Int.self, forKey: .levelID) ?? 0)"
        }
        totalAmmount = try values.decodeIfPresent(String.self, forKey: .totalAmmount)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        offeredByID = try values.decodeIfPresent(Int.self, forKey: .offeredByID)
        offeredByName = try values.decodeIfPresent(String.self, forKey: .offeredByName)
        offeredToID = try values.decodeIfPresent(Int.self, forKey: .offeredToID)
        offeredToName = try values.decodeIfPresent(String.self, forKey: .offeredToName)
        lastmessage = try values.decodeIfPresent(LastMessage.self, forKey: .lastmessage)
        onlineStatus = try values.decodeIfPresent(Int.self, forKey: .onlineStatus)
        offeredByImage = try values.decodeIfPresent(String.self, forKey: .offeredByImage)
        offeredToImage = try values.decodeIfPresent(String.self, forKey: .offeredToImage)
        paymentAt = try? values.decode(String.self, forKey: .paymentAt)
        negotiableStatus = try? values.decode(Int.self, forKey: .negotiableStatus)
        schedule_cancel_by = try? values.decode(Int.self, forKey: .schedule_cancel_by)
        isCancelled = try? values.decode(Int.self, forKey: .isCancelled)
        discountedPrice =  try? values.decode(String.self, forKey: .discountedPrice)
        isRescheduled = try? values.decode(Int.self, forKey: .isRescheduled)
        //levels =  try? values.decode([Level], forKey: .levels)
        //bookingTime = try? values.decode(String.self, forKey: .bookingTime)
    }
    
    init(){
        
    }
    
    func getOfferRequset()->[String:String]{
        var param = [String : String]()
        var _hours = ""
        var _lessons = ""
        var _rate = ""
        var _negotiableStatus = ""
        var _subjectID = ""
        var _levelID = ""
        var _discountedPrice = ""
        if let hrs = self.hours {
            if hrs.contains("hours"){
                _hours = hrs.replacingOccurrences(of: "hours", with: "").filter{ !" \n\t\r".contains($0)}
            }else if hrs.contains("hour"){
                 _hours = hrs.replacingOccurrences(of: "hour", with: "").filter{ !" \n\t\r".contains($0)}
            }else{
                _hours = hrs
            }
            
            if _hours == "1.0"{
                _hours = "1"
            }else if _hours == "1.5"{
                _hours = "1.5"
            }else if _hours == "2.0"{
                _hours = "2"
            }
        }
        
        if let less = self.lessons {
            _lessons = less.replacingOccurrences(of: "lessons", with: "").filter{!" \n\t\r".contains($0)}
        }
        
        if let subjectID = self.subjectID {
            _subjectID = subjectID
        }
        
        if let levelID = self.levelID {
            _levelID = levelID
        }
        
        if let rate = self.ammountPerHour {
            _rate = rate.filter{!" \n\t\r".contains($0)}
        }
        
        if let negotiableStatus = self.negotiableStatus{
            _negotiableStatus = "\(negotiableStatus)"
        }
        
        param = [
            "rate_offered"            : _rate ,
            "span_of"                   : _hours ,
            "subject_id"               : "\(_subjectID)",
            "for_lessons"              : _lessons ,
            "total_amount"          : self.totalAmmount ?? "",
            "non_negotiable"      : _negotiableStatus,
            "discount_price"        :  self.discountedPrice ?? "\(0)",
            "level_id"                   : _levelID
            ] 
        
        return param
    }
    
     func getStatus() -> String{
        return status ?? ""
    }
    
}
