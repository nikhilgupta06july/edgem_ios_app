//
//  LastMessageUnboxable.swift
//  Edgem
//
//  Created by Namespace on 01/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class LastMessageUnboxable: Unboxable{
   
    var conversationMessage: String
    var isRead: Int
    var messageType: String
    var messageID: Int
    var realImage: String
    var sendedOn: String
    var senderID: Int
    var userThumb: String
    var timeStamp: Int = 0
    
    init(lastMessage: [String:AnyObject]){
        
        self.conversationMessage = lastMessage["conversation_message"] as! String
        self.isRead = lastMessage["is_read"] as? Int ?? Int( lastMessage["is_read"] as! String) ?? 0
        self.messageType = lastMessage["message_type"] as? String ?? String(lastMessage["message_type"] as! Int)
        self.messageID = lastMessage["msg_id"] as? Int ?? Int( lastMessage["msg_id"] as! String) ?? 0//lastMessage["msg_id"] as! Int
        self.realImage = lastMessage["real_image"] as! String
        self.sendedOn = lastMessage["sended_on"] as! String
        self.senderID =  lastMessage["sender_id"] as? Int ?? Int( lastMessage["sender_id"] as! String) ?? 0//lastMessage["sender_id"] as! Int
        self.userThumb = lastMessage["user_thumb"] as! String
        timeStamp = Int(getTimeStamp(self.sendedOn))
    }
    
    required init(unboxer: Unboxer) throws {
        self.conversationMessage = unboxer.unbox(key: "conversation_message") ?? ""
        self.isRead = unboxer.unbox(key: "is_read") ?? 0
        self.messageType = unboxer.unbox(key: "message_type") ?? ""
        self.messageID = unboxer.unbox(key: "msg_id") ?? 0
        self.realImage = unboxer.unbox(key: "real_image") ?? ""
        self.sendedOn = unboxer.unbox(key: "sended_on") ?? ""
        self.senderID = unboxer.unbox(key: "sender_id") ?? 0
        self.userThumb = unboxer.unbox(key: "user_thumb") ?? ""
        timeStamp = Int(getTimeStamp(self.sendedOn))
    }
    
    func getTimeStamp(_ timeStr: String) -> TimeInterval{
        
        if !timeStr.isEmpty{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateString = dateFormatter.date(from: timeStr)
            return dateString!.timeIntervalSince1970
        }else{
            return 0.0
        }
       
    }
}

