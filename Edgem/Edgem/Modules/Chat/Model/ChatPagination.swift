//
//  ChatPagination.swift
//  Edgem
//
//  Created by Namespace on 24/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class ChatPagination: Unboxable {
    var messages: [ChatMessage]
    var pagination: Pagination
    
    required init(unboxer: Unboxer) throws {
        self.messages = try unboxer.unbox(key: "offer_chat")
        self.pagination = try unboxer.unbox(key: "pagination")
    }
    
}
