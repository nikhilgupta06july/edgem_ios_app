//
//  MessageType.swift
//  Edgem
//
//  Created by Namespace on 14/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation

class MessageType {
    
    public static let MESSAGE_TYPE_TEXT                                                                                                   = 0
    public static let MESSAGE_TYPE_MIME                                                                                                  = 1
    public static let MESSAGE_TYPE_MADE_OFFER_ALERT                                                                        = 2
    public static let MESSAGE_TYPE_PAY_BUTTON                                                                                     = 3
    public static let MESSAGE_TYPE_PAYMENT_PENDING_ALERT                                                             = 4
    public static let MESSAGE_TYPE_PAYMENT_DONE_ALERT                                                                   = 5
    public static let MESSAGE_TYPE_ADDRESS_ALERT                                                                               = 6
    public static let MESSAGE_TYPE_DOCUMENT                                                                                       = 7
    public static let MESSAGE_TYPE_ARRIVED_BUTTON                                                                             = 8
    public static let MESSAGE_TYPE_FINISH_BUTTON                                                                                = 9
    public static let MESSAGE_TYPE_SCHEDULE_REQ                                                                                = 10
    public static let MESSAGE_TYPE_OFFERACCEPTED                                                                              = 11
    public static let MESSAGE_TYPE_N_TH_LESSON_COMPLETED                                                           = 12
    public static let MESSAGE_TYPE_TUTION_HAS_ENDED                                                                       = 13
    public static let MESSAGE_TYPE_TUTOR_HAS_ARRIVED                                                                      = 14
    public static let MESSAGE_TYPE_DECLINED_SCHEDULE                                                                      = 15
    public static let MESSAGE_TYPE_OFFER_CANCEL                                                                                = 16
    public static let MESSAGE_TYPE_COUNTER_OFFER                                                                             = 17
    public static let MESSAGE_TYPE_OFFER_CANCEL_DUE_TO_PAYMENT_NOT_DONE_24hr               = 18
    public static let MESSAGE_TYPE_OFFER_CANCEL_DUE_TO_OFFER_NOT_ACCEPTED_24hr            = 19
    public static let MESSAGE_TYPE_EDIT_OFFER_ALERT                                                                          = 20
    public static let MESSAGE_TYPE_MAKE_EDIT_OFFER                                                                           = 21
}
