//
//  TutorSubjects.swift
//  Edgem
//
//  Created by Namespace on 03/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class TutorSubjects: Unboxable{
    
    var title: String
    var subjectID: String
   // var subjectRate: String
    var lock_rate: String
    var levels: [Level]
    
    required init(unboxer: Unboxer) throws {
        self.title = try unboxer.unbox(key: "title")
        self.subjectID = try unboxer.unbox(key: "subject_id")
        //self.subjectRate = try unboxer.unbox(key: "subject_rate")
        self.lock_rate = try unboxer.unbox(key: "lock_rate")
        self.levels = try unboxer.unbox(key: "level")
    }
    
}
