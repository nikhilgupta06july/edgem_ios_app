
//
//  PaymentStatus.swift
//  Edgem
//
//  Created by Namespace on 25/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation

struct PaymentStatus{
    
    static var shared = PaymentStatus()
    var isPaymentDone: Bool = false
    
    private init(){}
    
}
