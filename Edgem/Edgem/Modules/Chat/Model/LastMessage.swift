//
//  LastMessage.swift
//  Edgem
//
//  Created by Namespace on 28/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation

struct LastMessage: Codable{
    
    var conversationMessage: String?
    var isRead: Int?
    var messageType: Int?
    var messageID: Int?
    var realImage: String?
    var sendedOn: String?
    var senderID: Int?
    var userThumb: String?
    
    enum CodingKeys: String, CodingKey {
        
        case conversationMessage = "conversation_message"
        case isRead = "is_read"
        case messageType = "message_type"
        case messageID = "msg_id"
        case realImage = "real_image"
        case sendedOn = "sended_on"
        case senderID = "sender_id"
        case userThumb = "user_thumb"
        
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        conversationMessage = try values.decodeIfPresent(String.self, forKey: .conversationMessage)
        isRead = try values.decodeIfPresent(Int.self, forKey: .isRead)
        messageType = try values.decodeIfPresent(Int.self, forKey: .messageType)
        messageID = try values.decodeIfPresent(Int.self, forKey: .messageID)
        realImage = try values.decodeIfPresent(String.self, forKey: .realImage)
        sendedOn   = try values.decodeIfPresent(String.self, forKey: .sendedOn  )
        senderID = try values.decodeIfPresent(Int.self, forKey: .senderID)
        userThumb = try values.decodeIfPresent(String.self, forKey: .userThumb)
        
    }
    
    init(with dict: [String:Any]){
        
        conversationMessage = dict["conversation_message"] as? String//try values.decodeIfPresent(String.self, forKey: .conversationMessage)
        isRead = dict["is_read"] as? Int//try values.decodeIfPresent(Int.self, forKey: .isRead)
        messageType = dict["messageType"] as? Int//try values.decodeIfPresent(Int.self, forKey: .messageType)
        messageID =  dict["msg_id"] as? Int//try values.decodeIfPresent(Int.self, forKey: .messageID)
        realImage = dict["real_image"] as? String//try values.decodeIfPresent(String.self, forKey: .realImage)
        sendedOn   = dict["sended_on"] as? String//try values.decodeIfPresent(String.self, forKey: .sendedOn  )
        senderID = dict["sender_id"] as? Int//try values.decodeIfPresent(Int.self, forKey: .senderID)
        userThumb = dict["user_thumb"] as? String//try values.decodeIfPresent(String.self, forKey: .userThumb)
        
    }
    
}


