//
//  ChatMessages.swift
//  Edgem
//
//  Created by Namespace on 04/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
//import Unbox

struct ChatMessages{
    
//    let msgID: Int
//    let msgType: String
//    let conversationMessage: String
//    let isRead: String
//    let sendedOn: String
//    let senderID: Int
//    let realImage: String
//    let userThumb: String
    let text: String
    let inComming: Bool
    let date: Date
    let image: UIImage?
    let messageType: String
    let  imageHeight: CGFloat?
    let imageWidth: CGFloat?
    
//    required init(unboxer: Unboxer) throws {
//         self.msgID = try unboxer.unbox(key: "msg_id")
//         self.msgType = try unboxer.unbox(key: "message_type")
//         self.conversationMessage = try unboxer.unbox(key: "conversation_message")
//         self.isRead = try unboxer.unbox(key: "is_read")
//         self.sendedOn = try unboxer.unbox(key: "sended_on")
//         self.senderID = try unboxer.unbox(key: "sender_id")
//         self.realImage = try unboxer.unbox(key: "real_image")
//         self.userThumb = try unboxer.unbox(key: "user_thumb")
//
//         self.text = try unboxer.unbox(key: "text")
//         self.inComming = try unboxer.unbox(key: "inComming")
//         self.date = try unboxer.unbox(key: "date")
//         self.imageHeight = try unboxer.unbox(key: "imageHeight")
//         self.image = try unboxer.unbox(key: "image")
//         self.imageWidth = try unboxer.unbox(key: "imageWidth")
//         self.messageType = try unboxer.unbox(key: "messageType")
//    }
    
}

