//
//  WeeklyAvailibility.swift
//  Edgem
//
//  Created by Namespace on 13/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox


class WeeklyAvailibility: Unboxable {
    
    var selectedTimeSlots: DayAvailability
    
    required init(unboxer: Unboxer) throws {
        selectedTimeSlots = try unboxer.unbox(key: "calendar")
    }
}


class DayAvailability: Unboxable {
     var mon: [TimeAvailability]
     var tue: [TimeAvailability]
     var wed: [TimeAvailability]
     var thus: [TimeAvailability]
     var fri: [TimeAvailability]
     var sat: [TimeAvailability]
     var sun: [TimeAvailability]

    required init(unboxer: Unboxer) throws {
        mon = try unboxer.unbox(key: "MON")
        tue = try unboxer.unbox(key: "TUE")
        wed = try unboxer.unbox(key: "WED")
        thus = try unboxer.unbox(key: "THU")
        fri = try unboxer.unbox(key: "FRI")
        sat = try unboxer.unbox(key: "SAT")
        sun = try unboxer.unbox(key: "SUN")
    }
}

class TimeAvailability: Unboxable {
    var time: Int
    var type: Int
    var isEditable: Bool
    
    required init(unboxer: Unboxer) throws {
        time = try unboxer.unbox(key: "time")
        type = try unboxer.unbox(key: "type")
        isEditable = false
    }
}



