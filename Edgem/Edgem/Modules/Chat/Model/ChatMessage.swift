//
//  ChatMessage.swift
//  Edgem
//
//  Created by Namespace on 16/05/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class ChatMessage: Unboxable{
        let msgID: Int
        let msgType: Int
        var conversationMessage: String
        let isRead: Int
        var sendedOn: String?
        let senderID: Int
        var realImage: String?
        var buttonFlag: Int
        var discountedPrice: String?
        var viewType: Int?
    
        var staticCellTitle:String?
        var staticCellDescription:String?
    
        var  imageHeight: CGFloat?
        var imageWidth: CGFloat?
        var localImage: UIImage?
    
        var loginType: Int?
        //let userThumb: String
    
    init() {
        self.msgID = 0
        self.msgType = 0
        self.conversationMessage = ""
        self.isRead = 0
        self.sendedOn = ""
        self.senderID = 0
        self.realImage = ""
        self.viewType = 0
        
        self.imageWidth = 0
        self.imageHeight = 0
        self.localImage = nil
        self.buttonFlag = 0
        self.discountedPrice = ""
    }
    
    init(responseDict: NSDictionary){
        
        self.msgID = responseDict.value(forKey: "msg_id") as? Int ?? 0
        self.msgType = responseDict.value(forKey: "message_type") as? Int ?? 0
        self.conversationMessage = responseDict.value(forKey: "conversation_message") as? String ?? ""
        self.isRead = responseDict.value(forKey: "is_read") as? Int ?? 0
        self.sendedOn = responseDict.value(forKey: "sended_on") as? String ?? ""
        self.senderID = responseDict.value(forKey: "sender_id") as? Int ?? 0
        self.realImage = responseDict.value(forKey: "real_image") as? String ?? ""
        self.viewType = responseDict.value(forKey: "message_type") as? Int ?? 0
        self.imageHeight = responseDict.value(forKey: "imageHeight") as? CGFloat ?? 0
        self.imageWidth = responseDict.value(forKey: "imageWidth") as? CGFloat ?? 0
        self.localImage = responseDict.value(forKey: "doc_image") as? UIImage ?? nil
        self.buttonFlag =  responseDict.value(forKey: "button_flag") as? Int ?? 0
        self.discountedPrice = responseDict.value(forKey: "discount_price") as? String ?? ""
        self.loginType = responseDict.value(forKey: "login_type") as? Int
    }
    
        required init(unboxer: Unboxer) throws {
             self.msgID = try unboxer.unbox(key: "msg_id")
             self.msgType = try unboxer.unbox(key: "message_type")
             self.viewType =  try unboxer.unbox(key: "message_type")
             self.conversationMessage = try unboxer.unbox(key: "conversation_message")
             self.isRead = try unboxer.unbox(key: "is_read")
             self.sendedOn = try? unboxer.unbox(key: "sended_on")
             self.senderID = try unboxer.unbox(key: "sender_id")
             self.realImage = try unboxer.unbox(key: "real_image")
             self.imageHeight =  unboxer.unbox(key: "imageHeight")
             self.imageWidth =   unboxer.unbox(key: "imageWidth")
             self.buttonFlag = try unboxer.unbox(key: "button_flag")
             self.discountedPrice = try unboxer.unbox(key: "discount_price")
             self.loginType = try unboxer.unbox(key: "login_type")
        }
    
    init(message:ChatMessage){
        self.msgID = message.msgID
        self.msgType = message.msgType
        self.conversationMessage = message.conversationMessage
        self.isRead = message.isRead
        self.sendedOn = message.sendedOn
        self.senderID = message.senderID
        self.realImage = message.realImage
        
        self.viewType = message.viewType
        self.staticCellTitle = message.staticCellTitle
        self.staticCellDescription = message.staticCellDescription
        self.imageHeight = message.imageHeight
        self.imageWidth = message.imageWidth
        self.buttonFlag = message.buttonFlag
        self.discountedPrice = message.discountedPrice
        self.loginType = message.loginType
    }
    
    
}


