//
//  AddLevelSubjectForStudent.swift
//  Edgem
//
//  Created by Namespace on 10/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class AddLevelSubjectForStudent: Unboxable{

    // PROPERTIES
    var level: [Level]?
    var subjects: [Subject]?
    
    required init(unboxer: Unboxer) throws {
        self.level = unboxer.unbox(key: "level")
        self.subjects = unboxer.unbox(key: "subjects")
    }
    
    
    
}
