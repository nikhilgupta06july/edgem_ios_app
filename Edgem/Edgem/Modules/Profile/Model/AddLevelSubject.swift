//
//  AddLevelSubject.swift
//  Edgem
//
//  Created by Hipster on 25/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class AddLevelSubject: Unboxable{
    
    // PROPERTIES
    var selectedSubjects = [Subject]()
    var selectedLevels = [Level]()
    var otherSubject: String!
    
    // FUNCTIONS
    init(){
         otherSubject = ""
    }
    
    required init(unboxer: Unboxer) throws {
        
        if let subject_level = unboxer.dictionary["subject_level"] as? [String:AnyObject], let levelArr = subject_level["level"] as? [[String:AnyObject]], let subjectArr = subject_level["subjects"] as? [[String:AnyObject]]{
 
            for level in levelArr{
                let level: Level = try unbox(dictionary: level)
                selectedLevels.append(level)
            }
            
            for subs in subjectArr{
                let sub: Subject = try unbox(dictionary: subs)
                selectedSubjects.append(sub)
            }
            
        }
        
    }
    
    func copyValuesFromObject(_ filterParams: AddLevelSubject){
        
        self.selectedSubjects = filterParams.selectedSubjects
        self.selectedLevels = filterParams.selectedLevels
         self.otherSubject = filterParams.otherSubject
        
    }
    
    func getFilterParamsDict()->[String:String]{
        var filterDict = [String: String]()
        
        var subIDs = ""
        for i in 0..<selectedSubjects.count{
            if i != selectedSubjects.count-1{
                subIDs += String(selectedSubjects[i].id)
                subIDs += ","
            }else{
                subIDs += String(selectedSubjects[i].id)
            }
        }
        if !subIDs.isEmpty {
            filterDict["subjects"] = subIDs
        }
        
        var levelIDs = ""
        for i in 0..<selectedLevels.count{
            if i != selectedLevels.count-1{
                levelIDs += String(selectedLevels[i].id)
                levelIDs += ","
            }else{
                levelIDs += String(selectedLevels[i].id)
            }
        }
        if !levelIDs.isEmpty {
            filterDict["level"] = levelIDs
        }
        
        return filterDict
    }
    
    
}
