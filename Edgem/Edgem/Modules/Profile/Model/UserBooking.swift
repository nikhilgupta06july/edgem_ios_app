//
//  UserBooking.swift
//  Edgem
//
//  Created by Namespace on 12/06/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class UserBooking: Unboxable {
    
    var selectedTimeSlots = [
        "MON"   : ["0", "0", "0","0", "0", "0","0"],
        "TUE"    :  ["0", "0", "0","0","0", "0", "0"],
        "WED"   : ["0", "0", "0","0", "0", "0", "0"],
        "THU"    : ["0", "0", "0", "0", "0", "0", "0"],
        "FRI"      : ["0", "0", "0", "0", "0", "0", "0"],
        "SAT"     : ["0", "0", "0", "0", "0", "0", "0"],
        "SUN"    : ["0", "0", "0", "0", "0", "0", "0"],
        ]
    
    required init(unboxer: Unboxer) throws {
        selectedTimeSlots = try unboxer.unbox(key: "bookings")
        
        
    }
}
