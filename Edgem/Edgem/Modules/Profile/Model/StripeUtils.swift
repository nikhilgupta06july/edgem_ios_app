//
//  StripeUtils.swift
//  Edgem
//
//  Created by Namespace on 23/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Stripe

class StripeUtils{
    
    static let shared = StripeUtils()
    private init(){}
    
    var stripeTools = StripeTools()
    var customerID: String?
    
    let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
    var dataTask: URLSessionDataTask?
    
    //createUser
    func createUser(card: STPCardParams, completion: @escaping (_ success: Bool) -> Void) {
        
        //Stripe iOS SDK will gave us a token to make APIs call possible
        stripeTools.generateToken(card: card) { (token) in
            if(token != nil) {
                
                //request to create the user
                let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/customers")! as URL)
                
                //params array where you can put your user informations
                var params = [String:String]()
//                if let email = AppDelegateConstant.user?.email{
//                     params["email"] = email//"test@test.test"
//                }
               params["email"] = AppDelegateConstant.user?.email//"test@test.test"
                
                //transform this array into a string
                var str = ""
                params.forEach({ (key, value) in
                    str = "\(str)\(key)=\(value)&"
                })
                
                //basic auth
                request.setValue(self.stripeTools.getBasicAuth(), forHTTPHeaderField: "Authorization")
                
                //POST method, refer to Stripe documentation
                request.httpMethod = "POST"
                
                request.httpBody = str.data(using: String.Encoding.utf8)
                
                //create request block
                self.dataTask = self.defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
                    
                    //get returned error
                    if let error = error {
                        print(error)
                        completion(false)
                    }
                    else if let httpResponse = response as? HTTPURLResponse {
                        //you can also check returned response
                        if(httpResponse.statusCode == 200) {
                            if let data = data {
                                let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers)// try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as [String:Any]
                                if let id = (json as! [String:AnyObject])["id"] as? String{
                                    self.customerID = id
                                    guard let cus_id = AppDelegateConstant.user?.btCustomerID else{return}
                                    self.createCard(stripeId: id, card: card) { (success) in
                                        completion(true)
                                    }
                                }
                                //serialize the returned datas an get the customerId
//                                if let id = json["id"]  {
//                                    self.customerId = id
//                                    self.createCard(id, card: card) { (success) in
//                                        completion(success: true)
//                                    }
//                                }
                            }
                        }
                        else {
                            completion(false)
                        }
                    }
                }
                
                //launch request
                self.dataTask?.resume()
            }
        }
    }
    
    //create card for given user
    func createCard(stripeId: String, card: STPCardParams, completion: @escaping (_ cardDetail: [String:AnyObject]?) -> Void) {
        
        self.customerID = stripeId
        
        stripeTools.generateToken(card: card) { (token) in
            if(token != nil) {
                let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/customers/\(stripeId)/sources")! as URL)
                
                //token needed
                var params = [String:String]()
                params["source"] = token!.tokenId
                
                var str = ""
                params.forEach({ (key, value) in
                    str = "\(str)\(key)=\(value)&"
                })
                
                //basic auth
                request.setValue(self.stripeTools.getBasicAuth(), forHTTPHeaderField: "Authorization")
                
                request.httpMethod = "POST"
                
                request.httpBody = str.data(using: String.Encoding.utf8)
                
                self.dataTask = self.defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
                    
                    if let error = error {
                        print(error)
                        completion(nil)
                    }
                    else if let data = data {
                        let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        let cardDetail = (json as! [String:AnyObject])
                        completion(cardDetail)
                    }
                }
                
                self.dataTask?.resume()
            }else{
                 completion(nil)
            }
        }
    }
    
    //get user card list
    func getCardsList(completion: @escaping (_ cardArr: [[String:AnyObject]]?) -> Void) {
        
        //request to create the user
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/customers/\(self.customerID!)/sources?object=card")! as URL)
        
        //basic auth
        request.setValue(self.stripeTools.getBasicAuth(), forHTTPHeaderField: "Authorization")
        
        //POST method, refer to Stripe documentation
        request.httpMethod = "GET"
        
        // Hit API in Background thread
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
            //create request block
            self.dataTask = self.defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
                
                //get returned error
                if let error = error {
                    print(error)
                    completion(nil)
                }
                else if let httpResponse = response as? HTTPURLResponse {
                    //you can also check returned response
                    if(httpResponse.statusCode == 200) {
                        if let data = data {
                            // converting data type to a JSON
                            let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            // getting array of cards from JSON
                            let cardsArray =  (json as! [String:AnyObject])["data"] as? [[String:AnyObject]]
                            
                            completion(cardsArray)
                        }
                    }
                    else {
                        completion(nil)
                    }
                }
            }
            
            //launch request
            self.dataTask?.resume()
        }
        
    }
    
    func removeCardFromStripe(customerID cus_id: String, cardID card_id: String, completion: @escaping (_ deletedCarddetail: [String:AnyObject]?) -> Void){
        
        //request to create the user
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.stripe.com/v1/customers/\(cus_id)/sources/\(card_id)")! as URL)
        
        //basic auth
        request.setValue(self.stripeTools.getBasicAuth(), forHTTPHeaderField: "Authorization")
        
        //POST method, refer to Stripe documentation
        request.httpMethod = "DELETE"
        
        //create request block
        self.dataTask = self.defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            
            //get returned error
            if let error = error {
                print(error)
              //  completion(nil)
            }
            else if let httpResponse = response as? HTTPURLResponse {
                //you can also check returned response
                if(httpResponse.statusCode == 200) {
                    if let data = data {
                        // converting data type to a JSON
                        let json = try! JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        // getting deleted card detail from JSON
                        let deletedCarddetail = json as! [String:AnyObject]
                    
                      completion(deletedCarddetail)
                    }
                }
                else {
                    completion(nil)
                }
            }
        }
        
        //launch request
        self.dataTask?.resume()
        
    }

}
