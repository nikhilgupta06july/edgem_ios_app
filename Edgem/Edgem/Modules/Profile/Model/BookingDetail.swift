//
//  BookingDetail.swift
//  Edgem
//
//  Created by Namespace on 28/08/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox


class BookingInfo: Unboxable {
    
    var bookingID: Int
    var bookingSerial: String
    var bookingDate: String
    var bookingDay: String
    var bookingTimeSlot: String
    var bookingSubjectName: String
    var bookingTutorName: String
    
    required init(unboxer: Unboxer) throws {
        self.bookingID                     = unboxer.unbox(key: "booking_id") ?? 0
        self.bookingSerial                = unboxer.unbox(key: "booking_serial") ?? ""
        self.bookingDate                 = unboxer.unbox(key: "booked_on") ?? ""
        self.bookingDay                   = unboxer.unbox(key: "day_name") ?? ""
        self.bookingTimeSlot           = unboxer.unbox(key: "time_slot") ?? ""
        self.bookingSubjectName    = unboxer.unbox(key: "subject_name") ?? ""
        self.bookingTutorName        = unboxer.unbox(key: "tutor_name") ?? ""
    }
    
    init(_ booking: BookingInfo) {
        self.bookingID                      = booking.bookingID
        self.bookingSerial                 = booking.bookingSerial
        self.bookingDate                  = booking.bookingDate
        self.bookingDay                   = booking.bookingDay
        self.bookingTimeSlot           = booking.bookingTimeSlot
        self.bookingSubjectName    = booking.bookingSubjectName
        self.bookingTutorName        = booking.bookingTutorName
    }

}

