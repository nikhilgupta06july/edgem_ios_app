//
//  StripeTools.swift
//  Edgem
//
//  Created by Namespace on 23/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Stripe

struct StripeTools{
    
    //store stripe secret key
    private var stripeSecret = SECRET_API_KEY
    
    //generate token each time you need to get an api call
    func generateToken(card: STPCardParams, completion: @escaping (_ token: STPToken?) -> Void) {
        STPAPIClient.shared().createToken(withCard: card) { (token: STPToken?, error: Error?) in
            if let token = token {
                completion(token)
            }
            else {
                print(error as Any)
                if let err = error?.localizedDescription{
                    UserStore.shared.stripeError = err
                }
                completion(nil)
            }
        }
    }
    
    func getBasicAuth() -> String{
        return "Bearer \(self.stripeSecret)"
    }
}
