//
//  EmailAndContact.swift
//  Edgem
//
//  Created by Namespace on 20/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation

struct EmailAndContact{
    var email: String?
    var contact: String?
    var countryCode: String?
    
    init(_ email: String?, _ contact: String?, _ countryCode: String?){
        self.email = email
        self.contact = contact
        self.countryCode = countryCode
    }
}
