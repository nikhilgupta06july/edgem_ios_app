//
//  SchoolList.swift
//  Edgem
//
//  Created by Namespace on 08/08/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class SchoolList: Unboxable{

    // Properties
    var schoolName: [String]
    
    required init(unboxer: Unboxer) throws {
        self.schoolName = try unboxer.unbox(key: "school_list")
    }
    
    
}


