//
//  Rating.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 13/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class RatingPagination: Unboxable {
    var rating: [Rating]
    var pagination: Pagination
    
     required init(unboxer: Unboxer) throws {
        self.rating = try unboxer.unbox(key: "rating_data")
        self.pagination = try unboxer.unbox(key: "pagination")
    }
    
}

class Rating: Unboxable {
    
    var ratedById: Int
    var ratedBy: String
    var imageUrl: String
    var comment: String
    var rating: CGFloat
    var ratedAt: String
    
    required init(unboxer: Unboxer) throws {
        self.ratedById = unboxer.unbox(key: "rated_by_id") ?? 0
        self.ratedBy = unboxer.unbox(key: "rated_by") ?? ""
        self.imageUrl = unboxer.unbox(key: "real_image") ?? ""
        self.comment = unboxer.unbox(key: "comment") ?? ""
        self.rating = unboxer.unbox(key: "rating") ?? 0.0
        self.ratedAt = unboxer.unbox(key: "rated_at") ?? ""
    }
    
    
    init(_ rating: Rating) {
        self.ratedById = rating.ratedById
        self.ratedBy = rating.ratedBy
        self.imageUrl = rating.imageUrl
        self.comment = rating.comment
        self.rating = rating.rating
        self.ratedAt = rating.ratedAt
    }
}



