//
//  StoreUserCard.swift
//  Edgem
//
//  Created by Hipster on 27/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation
import Unbox

class StoreUserCard: Unboxable, Equatable{
   
    // Properties
    
    var cardID: String!
    var cardNumber: String!
    var nameOnCard: String!
    var expiryMonth: String!
    var expiryYear: String!
    var cardImage: String!
    var isPrimary: Int!
    var braintree_nonce: String!
    var brand: String?
    
    // Functions
    init(){
        
       // self.cardID                    =           ""
        self.nameOnCard           =           ""
        self.expiryMonth            =          ""
        self.expiryYear               =          ""
        self.cardImage                =           ""
        //self.isPrimary
        
    }
    
    init(storeUserCard: StoreUserCard){
        self.cardID                      =           storeUserCard.cardID
        self.cardNumber            =           storeUserCard.cardNumber
        self.nameOnCard           =           storeUserCard.nameOnCard
        self.expiryMonth            =           storeUserCard.expiryMonth
        self.expiryYear                =           storeUserCard.expiryYear
        self.cardImage                =           storeUserCard.cardImage
        self.brand                       =           storeUserCard.brand
        //self.isPrimary
    }
    
    init(data: [String:AnyObject]){
        self.cardID                                           =             data["id"] as? String
        self.cardNumber                                  =            data["last4"] as? String
        self.nameOnCard                                =             data["name"] as? String
        self.brand                                            =             data["brand"] as? String
        
        if let expMonth = (data["exp_month"] as? Int){
            self.expiryMonth = String(expMonth)
        }
        if let expYear = (data["exp_year"] as? Int){
            self.expiryYear = String(expYear)
        }
    }
    
    required init(unboxer: Unboxer) throws {
        
        self.cardID                  =            unboxer.unbox(key: "card_id")
        self.cardNumber        =            unboxer.unbox(key: "card_number")
        self.nameOnCard       =            unboxer.unbox(key: "name_on_card")
        self.expiryMonth         =           unboxer.unbox(key: "expiry_month")
        self.expiryYear             =           unboxer.unbox(key: "expiry_year")
        self.cardImage            =            unboxer.unbox(key: "card_image")
        self.isPrimary              =            unboxer.unbox(key: "is_primary")
        self.braintree_nonce  =             unboxer.unbox(key: "braintree_nonce")
        self.brand                   =             unboxer.unbox(key: "brand")
    }
    
    static func == (lhs: StoreUserCard, rhs: StoreUserCard) -> Bool {
        return lhs.cardID == rhs.cardID && lhs.cardNumber == rhs.cardNumber && lhs.nameOnCard == rhs.nameOnCard && lhs.expiryMonth == rhs.expiryMonth && lhs.expiryYear == rhs.expiryYear 
    }
    
    func getStoreUserCard() -> [String : String]{
        
         var filterDict = [String: String]()
        
        filterDict = [
            
        "card_number"          :      ("\(cardNumber ?? "")"),
        "name_on_card"        :      ("\(nameOnCard ?? "")"),
        "expiry_month"          :      ("\(expiryMonth ?? "")"),
        "expiry_year"              :      ("\(expiryYear ?? "")")

        ]
        
         return filterDict
    }
    
}
