//
//  ContactUs.swift
//  Edgem
//
//  Created by Hipster on 27/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import Foundation


class ContactUS{
    
    // Properties
    var subjects:[String]!
    var description: String!
    
    // Functions
    init(){
        subjects = ["General", "Reporting a Tutor", "Reporting a Student"]
        description = ""
    }
    
    func resetContactUs(){
    subjects = ["General", "Reporting a Tutor", "Reporting a Student"]
    description = ""
    }
}
