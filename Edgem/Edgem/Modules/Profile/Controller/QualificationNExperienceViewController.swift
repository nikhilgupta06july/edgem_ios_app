//
//  QualificationNExperienceViewController.swift
//  Edgem
//
//  Created by Namespace on 16/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift

class QualificationNExperienceViewController: BaseViewController {
    
    // MARK: --------- Properties

    var activeTextField: UITextField!
    
    var year: [Int] = Array()
    var newYearArr: [String] = Array()
    var fromYear: Int = 0
    var toYear: Int = 0
    let yearPicker = UIPickerView()
    let date = Date()
    let calendar = Calendar.current
    
    let sectionTitles = ["Select Your Qualifications".localized, "Experience".localized, "Education".localized, "Certificates".localized]
    
    var disposableBag = DisposeBag()
    var imageURLsArr: [String] = Array()
    var rowCount = 0
    var experienceRowCount = 0
    var user = User()
    
    var docsImage:[UIImage] = Array(){
        didSet{
            documentImage = docsImage
        }
    }
    
    var documentImage: [UIImage] = [UIImage]() {
        didSet{
            tableView.reloadData()
        }
    }
    
    var documents: [Documents] = Array()

    var education = Education()
    
    var userEducations: [Education] = Array(){
        didSet{
             rowCount = userEducations.count
        }
    }

    var userQualification: [Qualification] = Array()
    
    var experience = Experience()
    
    var userExperience: [Experience] = Array(){
        didSet{
            experienceRowCount = userExperience.count
        }
    }
    
    // MARK: --------- @IBOutlets
    
    @IBOutlet weak var tableView: UITableView!  {
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerCell()
        loadImagesInDocumentCell()
        fetchUserEducations()
        fetchUserQualifications()
        fetchUserExperience()
        self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        yearPicker.delegate = self
        toYear = calendar.component(.year, from: date)
        fromYear = toYear - 49
        for year in fromYear...toYear{
            self.year.append(year)
        }
        
        newYearArr = self.year.map { String("\($0)")}
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // fetchUserQualifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        imageCache.removeAllObjects()
    }
    
     // MARK: --------- Private Functions
    
    func registerCell(){
        
        tableView.register(UINib(nibName: CommonExpandCollapsTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: CommonExpandCollapsTableViewCell.cellIdentifire())
        
        tableView.register(UINib(nibName: SelectYourPropertyTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: SelectYourPropertyTableViewCell.cellIdentifire())
        
    
    }
    
    fileprivate func loadImagesInDocumentCell(){
        
        if let user = AppDelegateConstant.user, let userDocuments = user.documents{
            self.documentImage.removeAll()
            if userDocuments.count > 0 {
                self.documents.removeAll()
                self.documents = userDocuments.map{return $0}
                self.documentImage = documents.map {imageObject in
                    var docImage =  UIImage()
                    let imageProvider = ImageProvider()
                    if let image = imageCache.image(forKey: imageObject.id){
                        docImage = image
                         self.docsImage.append(image)
                       
                    }else{
                        if let url = URL(string: imageObject.name){
                            imageProvider.requestImage(from: url, completion: { (image) in
                                imageCache.add(image, forKey: imageObject.id)
                                docImage = image
                                self.docsImage.append(image)
                             
                            })
                        }
                    }
                    return docImage
                }
            }
        }
    }
    
    fileprivate func fetchUserEducations() {
        
        if let user = AppDelegateConstant.user, let educations = user.educations{
            if educations.count > 0{
                self.userEducations = educations.map{
                   $0.isUpdated = true
                    return $0
                }
                self.rowCount = userEducations.count
            }
        }
        print(userEducations)
    }
    
    fileprivate func fetchUserQualifications() {
        
        if let qualifications = AppDelegateConstant.qualification{
            if let selectedQualifications = AppDelegateConstant.user?.qualifications {
                 var tempQualifications = [Qualification]()
                if qualifications.count > 0{
                    tempQualifications = qualifications
                    for index in 0..<qualifications.count{
                            for s_index in 0..<selectedQualifications.count{
                                if qualifications[index].id == selectedQualifications[s_index].id{
                                    tempQualifications[index].isSelected = true
                                    tempQualifications[index].totalExperience = selectedQualifications[s_index].totalExperience
                                }
                            }
                    }
                }
                self.userQualification = tempQualifications
            }
           
        }
        
    }
    
    fileprivate func fetchUserExperience() {
        
        if let user = AppDelegateConstant.user, let experiences = user.experiences{
            if experiences.count > 0{
                self.userExperience = experiences.map{
                    $0.isUpdated = true
                    return $0
                }
                self.experienceRowCount = userExperience.count
            }
        }
        print(userExperience)
    }
    
    
    func isDataValid() {
        
        if user.qualifications == nil {
            self.showSimpleAlertWithMessage("Please select at least one qualification".localized)
            return
        }
        
    }
    
    func didTapAddRemoveButton(_ tag: Int, _ index: Int, _ section: Int){
        print(tag)
        print(index)
        print(section)
        if QualificationNExperienceVCSectionType.Experience.rawValue == section{
            if tag == 5{                    //------> remove index from array
                
                if index < userExperience.count{
                    userExperience.remove(at: index)
                    let indexSet = IndexSet(arrayLiteral: 1)
                    tableView.reloadSections(indexSet, with: .none)
                }
                
            }else if tag == 6{          //------> add index from array
                
                userExperience.append(Experience())
                let indexSet = IndexSet(arrayLiteral: 1)
                tableView.reloadSections(indexSet, with: .none)
                
            }
            
        }else{
        if tag == 5{                    //------> remove index from array
            
            if index < userEducations.count{
                userEducations.remove(at: index)
                let indexSet = IndexSet(arrayLiteral: 2)
                tableView.reloadSections(indexSet, with: .none)
            }
            
        }else if tag == 6{          //------> add index from array
            
            userEducations.append(Education())
            let indexSet = IndexSet(arrayLiteral: 2)
            tableView.reloadSections(indexSet, with: .none)
            
        }
    }
    }
    
    // Update table view according content
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let indexPath = IndexPath(row: 0, section: 0)
        guard let cell = tableView.cellForRow(at: indexPath) as? SelectYourPropertyTableViewCell else{return}
        cell.secondTableView.layer.removeAllAnimations()
        cell.secondTableViewHeight.constant = cell.secondTableView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
        
    }
    
    func updateSecondTableViewHeight(_ status: Bool){
        let indexPath = IndexPath(row: 0, section: 0)
        guard let cell = tableView.cellForRow(at: indexPath) as? SelectYourPropertyTableViewCell else{return}
        cell.secondTableView.layer.removeAllAnimations()
        cell.secondTableViewHeight.constant = cell.secondTableView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
            self.tableView.reloadData()
        }
    }
    
    func didTapDocument(_ image: UIImage){
        self.imageTapped(image: image)
    }
    
    func imageTapped(image:UIImage){
        
        let containerView = UIView()
        containerView.frame = UIScreen.main.bounds
        containerView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.7)
        containerView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage(_:)))
        containerView.addGestureRecognizer(tap)
        self.view.addSubview(containerView)
        
        let imageHoldetView = UIView()
        imageHoldetView.backgroundColor = UIColor.white
        imageHoldetView.frame = CGRect(x: 50, y: 150, width: ScreenWidth-100, height: ScreenHeight-300)
        imageHoldetView.isUserInteractionEnabled = false
        imageHoldetView.layer.cornerRadius = 15
        imageHoldetView.clipsToBounds = true

        containerView.addSubview(imageHoldetView)
        
        
        let newImageView = UIImageView(image: image)
        newImageView.frame = CGRect(x: 12, y: 12, width: imageHoldetView.bounds.width-24, height: imageHoldetView.bounds.height-24)
        newImageView.backgroundColor = .clear
        newImageView.contentMode = .scaleAspectFill
        newImageView.clipsToBounds = true
        imageHoldetView.addSubview(newImageView)

    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    func selectedQualificationsNExperience(qualification: [Qualification], experience: String){
        print(qualification, experience)
        //params["qualifications"] = qualification
        user.qualifications = qualification
        user.totalExperience = experience
    tableView.reloadSections([QualificationNExperienceVCSectionType.Experience.rawValue], with: .none)
    }
    
    func updateDataIntoTextField(_ textField: UITextField, fromCell: Bool, index:Int, section: Int){
        if QualificationNExperienceVCSectionType.Experience.rawValue == section{
            if let tempExperience = userExperience[safe: index]{
                switch textField.tag {
                case 1 :
                    fromCell ? (textField.text = tempExperience.instituteName) : (tempExperience.instituteName = textField.text!)
                case 2 :
                    fromCell ? (textField.text = tempExperience.subjectName) : ( tempExperience.subjectName = textField.text!)
                case 3 :
                    fromCell ? (textField.text = tempExperience.fromYear) : ( tempExperience.fromYear = textField.text!)
                case 4 :
                    fromCell ? (textField.text = tempExperience.toYear) : ( tempExperience.toYear = textField.text!)
                default:
                    break
                }
                tempExperience.isUpdated = false
                userExperience[index] = tempExperience
            }
        }else{
        if let tempEducation = userEducations[safe: index]{
            switch textField.tag {
            case 1 :
                fromCell ? (textField.text = tempEducation.instituteName) : (tempEducation.instituteName = textField.text!)
            case 2 :
                fromCell ? (textField.text = tempEducation.certificateName) : ( tempEducation.certificateName = textField.text!)
            case 3 :
                fromCell ? (textField.text = tempEducation.fromYear) : ( tempEducation.fromYear = textField.text!)
            case 4 :
                fromCell ? (textField.text = tempEducation.toYear) : ( tempEducation.toYear = textField.text!)
            default:
                break
            }
               tempEducation.isUpdated = false
               userEducations[index] = tempEducation
        }
        }

        if !fromCell{
            //tempEducationObjects.append(education)
        }
     
    }
    
    func getEducations() -> [[String:AnyObject]] {
        var params = [[String:AnyObject]]()
        if userEducations.count > 0{
            for index in 0..<userEducations.count {
             var tempParams = [String:AnyObject]()
                if userEducations[index].isUpdated == false{
                    tempParams["education_id"] = userEducations[index].id as AnyObject
                    tempParams["institute_name"] = userEducations[index].instituteName as AnyObject
                    tempParams["year_from"] = userEducations[index].fromYear as AnyObject
                    tempParams["certificate_name"] = userEducations[index].certificateName as AnyObject
                    tempParams["year_to"] = userEducations[index].toYear as AnyObject
                    
                    params.append(tempParams)
                }
            }
        }
        return params
    }
    
    func getExperience() -> [[String:AnyObject]] {
        var params = [[String:AnyObject]]()
        if userExperience.count > 0{
            for index in 0..<userExperience.count {
                var tempParams = [String:AnyObject]()
                if userExperience[index].isUpdated == false{
                    tempParams["experience_id"] = userExperience[index].id as AnyObject
                    tempParams["institute_name"] = userExperience[index].instituteName as AnyObject
                    tempParams["year_from"] = userExperience[index].fromYear as AnyObject
                    tempParams["subject_name"] = userExperience[index].subjectName as AnyObject
                    tempParams["year_to"] = userExperience[index].toYear as AnyObject
                    
                    params.append(tempParams)
                }
            }
        }
        return params
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton){
        addQualificationNExperience()
    }
    
}

     // MARK: --------- TextField Methods

extension QualificationNExperienceViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
        }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let pointInTable: CGPoint = textField.convert(textField.bounds.origin, to: self.tableView)
        let cellIndexPath = self.tableView.indexPathForRow(at: pointInTable)
        let currentSection = cellIndexPath?.section
        if let indexPath = cellIndexPath?.row{
            updateDataIntoTextField(textField, fromCell: false, index: indexPath, section: currentSection!)
        }

    }
    
}


     // MARK: --------- Table view methods
    
extension QualificationNExperienceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case QualificationNExperienceVCSectionType.SelectYourQualifications.rawValue:
            return 1
        case QualificationNExperienceVCSectionType.Experience.rawValue:
            if user.totalExperience == ""{
                return 0
            }
            return userExperience.count > 0 ? experienceRowCount : 1
        case QualificationNExperienceVCSectionType.Education.rawValue:
            return userEducations.count > 0 ? rowCount : 1
        case QualificationNExperienceVCSectionType.Certificates.rawValue:
            return documentImage.count > 0  ? 2 : 1
        default:
            return 0
        }
//        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section{
            
        case QualificationNExperienceVCSectionType.SelectYourQualifications.rawValue :
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SelectYourPropertyTableViewCell.cellIdentifire(), for: indexPath) as? SelectYourPropertyTableViewCell else{
                return UITableViewCell()
            }
            if userQualification.count > 0{
                cell.configureCell(with: userQualification, sectionTitles[indexPath.section])
            }else{
                cell.configureCell(with: [Qualification](), sectionTitles[indexPath.section])
            }
            cell.selectedQualificatioNExperience = { [weak self] (selectedQualifications, experience) in
                self!.selectedQualificationsNExperience(qualification: selectedQualifications, experience: experience)
            }
            cell.updateSecondTableViewHeight = {[weak self] status in
                print(status)
                self?.updateSecondTableViewHeight(status)
            }

            return cell
        case QualificationNExperienceVCSectionType.Experience.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonExpandCollapsTableViewCell.cellIdentifire(), for: indexPath) as? CommonExpandCollapsTableViewCell else{
                return UITableViewCell()
            }
            cell.firstTextField.delegate = self
            cell.secondTextField.delegate = self
            cell.thirdTextField.delegate = self
            cell.thirdTextField.inputView = yearPicker
            cell.fourthTextField.delegate = self
            cell.fourthTextField.inputView = yearPicker
            
            if userExperience.count > 0{
                cell.configureCellForExperience(with: userExperience[indexPath.row], sectionTitles[indexPath.section], indexPath.row+1, lastIndex: experienceRowCount, section: indexPath.section )
            }else{
                userExperience.append(Experience())
                cell.configureCellForExperience(with: userExperience[indexPath.row], sectionTitles[indexPath.section], indexPath.row+1, lastIndex: 1, section: indexPath.section)
            }
            updateDataIntoTextField(cell.firstTextField, fromCell: true, index: indexPath.row, section: indexPath.section)
            updateDataIntoTextField(cell.secondTextField, fromCell: true, index: indexPath.row, section: indexPath.section)
            updateDataIntoTextField(cell.thirdTextField, fromCell: true, index: indexPath.row, section: indexPath.section)
            updateDataIntoTextField(cell.fourthTextField, fromCell: true, index: indexPath.row, section: indexPath.section)
            
            cell.onDidTapAddRemoveBtn = { [weak self] (tag,index,section) in
                self?.didTapAddRemoveButton(tag, index, section)
            }
            return cell
            
        case QualificationNExperienceVCSectionType.Education.rawValue:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonExpandCollapsTableViewCell.cellIdentifire(), for: indexPath) as? CommonExpandCollapsTableViewCell else{
                return UITableViewCell()
            }
            cell.firstTextField.delegate = self
            cell.secondTextField.delegate = self
            cell.thirdTextField.delegate = self
            cell.thirdTextField.inputView = yearPicker
            cell.fourthTextField.delegate = self
            cell.fourthTextField.inputView = yearPicker
            
            if userEducations.count > 0{
                cell.configureCell(with: userEducations[indexPath.row], sectionTitles[indexPath.section], indexPath.row+1, lastIndex: rowCount, section: indexPath.section )
            }else{
                userEducations.append(Education())
                cell.configureCell(with: userEducations[indexPath.row], sectionTitles[indexPath.section], indexPath.row+1, lastIndex: 1, section: indexPath.section)
            }
            updateDataIntoTextField(cell.firstTextField, fromCell: true, index: indexPath.row, section: indexPath.section)
            updateDataIntoTextField(cell.secondTextField, fromCell: true, index: indexPath.row, section: indexPath.section)
            updateDataIntoTextField(cell.thirdTextField, fromCell: true, index: indexPath.row, section: indexPath.section)
            updateDataIntoTextField(cell.fourthTextField, fromCell: true, index: indexPath.row, section: indexPath.section)
       
            cell.onDidTapAddRemoveBtn = { [weak self] (tag,index,section) in
                self?.didTapAddRemoveButton(tag, index, section)
            }
            return cell
            
        case QualificationNExperienceVCSectionType.Certificates.rawValue :
        
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: AttachDocumentsTableViewCell.cellidentifire(), for: indexPath) as! AttachDocumentsTableViewCell
                cell.configureCell(sectionTitles[indexPath.section])
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: DocumentsTableViewCell.cellIdentifire(), for: indexPath) as! DocumentsTableViewCell
            cell.onDidTapDocumentsCell = { [weak self]  image in
                print(image)
                self?.didTapDocument(image)
            }
            
           // if let image  = documentImage{
           // cell.configureCell(with: documentImage)
           //
            
      //  }
        
        return cell
            
        default:
            return UITableViewCell()
        }
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == QualificationNExperienceVCSectionType.Certificates.rawValue{
            if indexPath.row == 0{
                self.showImageOptions()
            }
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
        
}

// MARK: --------- Picker view delegate

extension QualificationNExperienceViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == yearPicker{
            return self.newYearArr.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == yearPicker{
            return newYearArr[row]
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == yearPicker{

            activeTextField.text = newYearArr[row]
        }
        
    }
}

// MARK: ---------- Image Picker Delegates

extension QualificationNExperienceViewController{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
         let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage

        if let imageData = image.jpeg(.secondLowest){
            
            self.documentImage.append(image)
            
            uploadProfilePicture(imageData: imageData)
            
            dismiss(animated:true, completion: nil)
            
        }


    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let cico = url as URL
        print(cico)
        print(url)
        
        print(url.lastPathComponent)
        
        print(url.pathExtension)
    }
    
}

// MARK: ---------- API Implementation

extension QualificationNExperienceViewController{
    
    func uploadProfilePicture(imageData: Data) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let url =   "\(baseUrl)\(PathURl.userDocumentUpload.rawValue)"
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Accept": "application/json",
            "Authorization": "Bearer \(UserStore.shared.token)"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in

            let fileName = "doc_file.png"
            let mimeType = imageData.mimeType
            
            multipartFormData.append(imageData, withName: "doc_file", fileName: fileName, mimeType: mimeType)
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    //                    print("Succesfully uploaded")
                    DispatchQueue.main.async(execute: {
                        Utilities.hideHUD(forView: self.view)
                    })
                    if let _ = response.error{
                        self.showSimpleAlertWithMessage("There seems to be an error. Please try again later.".localized)
                        return
                    }
                    let json = try? JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                    if let responseDict = json as? [String: AnyObject], let message = responseDict["message"] as? String {
                        
                            if let data = responseDict["data"] as? [String: AnyObject] {
                                if let imageUrls = (data["attachement_file"] as? String){
                                   self.imageURLsArr.append(imageUrls)
                                }
                               
                            }
                    
                        self.showSimpleAlertWithMessage(message)
                        
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    Utilities.hideHUD(forView: self.view)
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            }
        }
    }
    
    func addQualificationNExperience(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        isDataValid()
        var params = user.getQualificationsnEducations()
        let educationParams = getEducations()
        let experienceParams = getExperience()
        params["attachement_file"] = self.imageURLsArr
        params["educations"] = educationParams
        params["experience"] = experienceParams
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let loginObserver = ApiManager.shared.apiService.addTutorQualificationNExperiences(params as [String : AnyObject])
        let loginDisposable = loginObserver.subscribe(onNext: {(user) in
            DispatchQueue.main.async{
                  Utilities.hideHUD(forView: self.view)
                 AppDelegateConstant.user = user
                let commonPopUP = ShowInfoCommonPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                commonPopUP.fromController = self
               commonPopUP.showAlert = true
                self.navigationController?.present(commonPopUP, animated: true, completion: nil)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        loginDisposable.disposed(by: disposableBag)
    }
    
}
