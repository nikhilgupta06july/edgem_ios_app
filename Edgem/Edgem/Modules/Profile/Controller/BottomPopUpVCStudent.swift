//
//  BottomPopUpVCStudent.swift
//  Edgem
//
//  Created by Namespace on 04/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol BottomPopUpVCStudentDelegate: class{
    func studentApplyBtnTapped(_ sortBy: String)
}

class BottomPopUpVCStudent: BaseViewController {
    
    // MARK: --------- Properties
    
    let sectionTitles = ["Preferred".localized, "Tuition Rate".localized, "Ratings".localized]
    var sectionTitlesArr: [String] = Array()
    let rowTitles1 = ["","Highest to Lowest ".localized, "Lowest to Highest ".localized]
    let rowTitles2 = ["","Highest to Lowest".localized, "Lowest to Highest".localized]
    var selectedProperty: [String] = Array()
    
    weak var bottomPopUpVCStudentDelegate: BottomPopUpVCStudentDelegate?
    
    // MARK: --------- @IBOutlet
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    @IBOutlet weak var popUPTitle: UILabel!
    @IBOutlet weak var popUpDescriptionTitle: UILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var arcView: UIView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var resetBtnHolderView: UIView!
    @IBOutlet weak var applyBtnHolderView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
         self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
         arcSetUP()
         registerCell()
       
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        tableView.layer.removeAllAnimations()
        tableViewHeight.constant = tableView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        
        popUPTitle.text = "Sort By"
        
        resetButton.layer.cornerRadius = 5
        resetButton.layer.borderWidth = 1
        resetButton.layer.borderColor = theamGrayColor.cgColor
        resetButton.clipsToBounds = true
        resetButton.setTitle("Reset".localized, for: .normal)
        applyButton.setTitle("Apply".localized, for: .normal)

        //resetButton.setTitle("Reset".localized, for: .normal)
        resetButton.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        resetButton.titleLabel?.textColor = theamBlackColor
        
        // Reset button shadow properties
        resetBtnHolderView.layer.shadowColor = UIColor.black.cgColor
        resetBtnHolderView.layer.shadowOpacity = 0.12
        resetBtnHolderView.layer.shadowRadius = 4
        resetBtnHolderView.layer.shadowOffset = .zero
        resetBtnHolderView.layer.shadowPath = UIBezierPath(rect: resetBtnHolderView.bounds).cgPath
        
        // apply button attributes
        applyButton.layer.cornerRadius = 5
        applyButton.backgroundColor = buttonBackGroundColor
        applyButton.clipsToBounds = true
        
        //applyButton.setTitle("Apply".localized, for: .normal)
        applyButton.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        applyButton.titleLabel?.textColor = theamBlackColor
        
        
        // apply button shadow
        applyBtnHolderView.layer.shadowColor = UIColor.black.cgColor
        applyBtnHolderView.layer.shadowOpacity = 0.12
        applyBtnHolderView.layer.shadowRadius = 4
        applyBtnHolderView.layer.shadowOffset = .zero
        applyBtnHolderView.layer.shadowPath = UIBezierPath(rect: applyBtnHolderView.bounds).cgPath
        
    }
    

    // MARK: ------ Private Functions
    
    private func registerCell(){
        tableView.register(UINib(nibName: "CommonCheckBoxCell", bundle: nil), forCellReuseIdentifier: CommonCheckBoxCell.cellIdentifire())
    }
    
    fileprivate func arcSetUP(){
        
        let frame = arcView.frame
        let path = UIBezierPath()
        
        let p1 = CGPoint(x:0,y:frame.height)
        let p3 = CGPoint(x:ScreenWidth,y:frame.height)
        
        path.move(to: p1)
        path.addQuadCurve(to: p3, controlPoint: CGPoint(x: ScreenWidth/2, y: -frame.height))
        
        let line = CAShapeLayer()
        line.path = path.cgPath;
        line.strokeColor = UIColor.white.cgColor
        line.fillColor = UIColor.white.cgColor
        arcView.layer.addSublayer(line)
        
    }
    
    fileprivate func selectProperty(_ sortByProperty: String){
        
        let sortByProperty = sortByProperty
        let index = selectedProperty.index { (property) -> Bool in
            return property == sortByProperty
        }
        if let subIndex = index, subIndex > -1{
            selectedProperty.remove(at: subIndex)
        }else{
            selectedProperty.removeAll()
            selectedProperty.append(sortByProperty)
        }
        
    }
    
    fileprivate func configureSelectedCellImage(_ sortByProperty: String) -> Bool{
        
        let sortByProperty = sortByProperty
        var isSelected = false
        let index = selectedProperty.index{ (property)->Bool in
            return property == sortByProperty
        }
        if let subindex = index, subindex > -1{
            isSelected = true
        }
        return isSelected
    }
    
    // MARK: --------- @IBActions
    @IBAction func dismissPopUpTapped(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    
    @IBAction func resetBtnTapped(_ sender: UIButton) {

        self.selectedProperty.removeAll()
        tableView.reloadData()
        
    }
    
    
    @IBAction func applyBtnTapped(_ sender: UIButton) {
        var sortBy = ""
        if selectedProperty.count > 0{
            sortBy = selectedProperty[0]
        }
        bottomPopUpVCStudentDelegate?.studentApplyBtnTapped(sortBy)
        self.dismissPopUp()
    }
    
}

extension BottomPopUpVCStudent: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitlesArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section != 0{
            return rowTitles1.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section{
            
        case BottomPopUpVCSectionType.Preferred.rawValue:
            
                                                                                                 let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire(), for: indexPath) as! CommonCheckBoxCell
                                                                                                 
                                                                                                 let isSelected = configureSelectedCellImage(sectionTitlesArr[indexPath.row])
                                                                                                 
                                                                                                 cell.configureCell(with: sectionTitles[indexPath.section], isSelected)
                                                                                                 
                                                                                                return cell
            
        case BottomPopUpVCSectionType.TutionRate.rawValue:
            
                                                                                                if indexPath.row == 0{
                                                                                                    
                                                                                                    let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire(), for: indexPath) as! CommonCheckBoxCell
                                                                                                    let isSelected = false
                                                                                                    cell.configureCell(with: sectionTitles[indexPath.section], isSelected)
                                                                                                    cell.sepratorView.isHidden = true
                                                                                                    cell.checkBoxButton.isHidden = true
                                                                                                    cell.isUserInteractionEnabled = false
                                                                                                    return cell
                                                                                                    
                                                                                                }else if indexPath.row == 1{
                                                                                                    
                                                                                                    let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire(), for: indexPath) as! CommonCheckBoxCell
                                                                                                    cell.checkBoxButton.isHidden = false
                                                                                                    cell.sepratorView.isHidden = true
                                                                                                    
                                                                                                     let isSelected = configureSelectedCellImage(rowTitles1[indexPath.row])
                                                                                                    
                                                                                                    cell.configureCellWith(with: rowTitles1[indexPath.row], isSelected)

                                                                                                    return cell
                                                                                                    
                                                                                                }else if indexPath.row == 2{
                                                                                                    
                                                                                                    let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire(), for: indexPath) as! CommonCheckBoxCell
                                                                                                    cell.checkBoxButton.isHidden = false
                                                                                                    cell.sepratorView.isHidden = false
                                                                                                    
                                                                                                    let isSelected = configureSelectedCellImage(rowTitles1[indexPath.row])
                                                                                                   
                                                                                                    cell.configureCellWith(with: rowTitles1[indexPath.row], isSelected)
                                                                                                   
                                                                                                    return cell
                                                                                                    
                                                                                                }
            

            
        case BottomPopUpVCSectionType.Ratings.rawValue:
                                                                                                if indexPath.row == 0{
                
                                                                                                    let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire(), for: indexPath) as! CommonCheckBoxCell
                                                                                                    let isSelected = false
                                                                                                    cell.configureCell(with: sectionTitles[indexPath.section], isSelected)
                                                                                                    cell.sepratorView.isHidden = true
                                                                                                    cell.checkBoxButton.isHidden = true
                                                                                                    cell.isUserInteractionEnabled = false
                                                                                                    return cell
                
                                                                                                }else if indexPath.row == 1{
                
                                                                                                    let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire(), for: indexPath) as! CommonCheckBoxCell
                                                                                                    cell.sepratorView.isHidden = true
                                                                                                    cell.checkBoxButton.isHidden = false
                                                                                            
                                                                                                    let isSelected = configureSelectedCellImage(rowTitles2[indexPath.row])
                                                                                                    
                                                                                                    cell.configureCellWith(with: rowTitles2[indexPath.row], isSelected)
                                                                                                 
                                                                                                    return cell
                
                                                                                            }else if indexPath.row == 2{
                
                                                                                                    let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire(), for: indexPath) as! CommonCheckBoxCell
                                                                                                    cell.sepratorView.isHidden = false
                                                                                                    cell.checkBoxButton.isHidden = false
                                                                                                    
                                                                                                    let isSelected = configureSelectedCellImage(rowTitles2[indexPath.row])
                                                                                                    
                                                                                                    cell.configureCellWith(with: rowTitles2[indexPath.row], isSelected)

                                                                                                    return cell
                
            }
            
            
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CommonCheckBoxCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(indexPath.row)
        switch indexPath.section{
            
        case BottomPopUpVCSectionType.Preferred.rawValue:
            
                                                                                                selectProperty(sectionTitlesArr[indexPath.row])
            
        case BottomPopUpVCSectionType.TutionRate.rawValue:
            
                                                                                            if indexPath.row == 0{
                                                                                                
                                                                                                break
                
                                                                                            }else if indexPath.row == 1{
                                                                                                
                                                                                                    selectProperty(rowTitles1[indexPath.row])
                
                                                                                            }else if indexPath.row == 2{
                                                                                                
                                                                                                   selectProperty(rowTitles1[indexPath.row])
                                                                                                
                                                                                            }
            
            
        case BottomPopUpVCSectionType.Ratings.rawValue:
                                                                                                if indexPath.row == 0{
                
                                                                                                    break
                
                                                                                                }else if indexPath.row == 1{
                                                                                                    
                                                                                                     selectProperty(rowTitles2[indexPath.row])
                                                                                                    
                                                                                                }else if indexPath.row == 2{
                                                                                        
                                                                                                    selectProperty(rowTitles2[indexPath.row])
                                                                                                    
                                                                                            }
            
            
        default:
            break
        }
        
         tableView.reloadData()
    }
    
}
