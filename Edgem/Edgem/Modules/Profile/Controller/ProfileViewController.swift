//
//  ProfileViewController.swift
//  Edgem
//
//  Created by Hipster on 27/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit
import FirebaseAuth
import GoogleSignIn
import FBSDKLoginKit
import RxSwift
import Alamofire
import BitlySDK


class ProfileViewController: BaseViewController {
    
    var tutorImage: UIImage?

    @IBOutlet weak var tableView: UITableView!
    
    
    var titles = ["Personal Information".localized,
                        UserStore.shared.selectedUserType != tutorType ?
                        "Level & Subjects".localized :
                        "Qualifications & Experience".localized,
                        UserStore.shared.selectedUserType != tutorType ?
                        "Credit Cards".localized :
                        "Subjects & Rates".localized,
                        "Calendar".localized,
                        "Change Password".localized,
                        "My QR Code".localized,
                        "Terms and Conditions".localized,
                        "Privacy Policy".localized,
                        "Contact Us".localized.capitalized,
                        "Logout".localized]
    
    var images = ["details", UserStore.shared.selectedUserType != tutorType ? "levelSubjects" : "qualification", UserStore.shared.selectedUserType != tutorType ? "card" : "levelSubjects", "calendar", "password", "qrCode", "t&c", "privacy", "helpCenter", "logout"]
    var disposableBag = DisposeBag()
//    var isFirstTime = true
    var profileImage: UIImage!
    var firebaseAuth: Auth!
    var bitlyStr = ""
    
    var addLevelSubject = AddLevelSubject()
    
    private let refreshControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if AppDelegateConstant.user?.userType == tutorType{
            
            titles = ["Personal Information".localized,
                      UserStore.shared.selectedUserType != tutorType ?
                        "Level & Subjects".localized :
                        "Qualifications & Experience".localized,
                      UserStore.shared.selectedUserType != tutorType ?
                        "Credit Cards".localized :
                        "Subjects & Rates".localized,
                      "Credit Cards".localized,
                      "Calendar".localized,
                      "Change Password".localized,
                      "My QR Code".localized,
                      "Terms and Conditions".localized,
                      "Privacy Policy".localized,
                      "Contact Us".localized.capitalized,
                      "Logout".localized]
            images = ["details", UserStore.shared.selectedUserType != tutorType ? "levelSubjects" : "qualification", UserStore.shared.selectedUserType != tutorType ? "card" : "levelSubjects", "card", "calendar", "password", "qrCode", "t&c", "privacy", "helpCenter", "logout"]
            
        }else{
            titles = ["Personal Information".localized,
                      UserStore.shared.selectedUserType != tutorType ?
                        "Level & Subjects".localized :
                        "Qualifications & Experience".localized,
                      UserStore.shared.selectedUserType != tutorType ?
                        "Credit Cards".localized :
                        "Subjects & Rates".localized,
                      "Calendar".localized,
                      "Change Password".localized,
                      "My QR Code".localized,
                      "Terms and Conditions".localized,
                      "Privacy Policy".localized,
                      "Contact Us".localized.capitalized,
                      "Logout".localized]
            
            images = ["details", UserStore.shared.selectedUserType != tutorType ? "levelSubjects" : "qualification", UserStore.shared.selectedUserType != tutorType ? "card" : "levelSubjects", "calendar", "password", "qrCode", "t&c", "privacy", "helpCenter", "logout"]
        }

        generateBitlyUrl()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        imageCache.removeAllObjects()
    }
    

    
    
//    @objc func refreshData(_ sender: Any) {
//        if UserStore.shared.isLoggedIn == true {
//            DispatchQueue.main.async {
////                self.fetchUserDetails()
//            }
//        }
//        if let refreshControl = sender as? UIRefreshControl {
//            refreshControl.endRefreshing()
//        }
//    }
    

}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfileHeaderTableViewCell.cellIdentifier(), for: indexPath) as! ProfileHeaderTableViewCell
            cell.configureCell()
            cell.tutorProfileImage = { [weak self] tutorImage in
                if self?.tutorImage != nil {
                      self!.tutorImage = tutorImage
                }
            }
            if AppDelegateConstant.user?.userType == tutorType{
                cell.editProfileBtn.addTarget(self, action: #selector(showImageOptions), for: .touchUpInside)
                cell.editProfileImageBtn.addTarget(self, action: #selector(showImageOptions), for: .touchUpInside)
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileOptionTableViewCell.cellIdentifier(), for: indexPath) as! ProfileOptionTableViewCell
        cell.configureCellWithTitle(titles[indexPath.row - 1], andImage: images[indexPath.row - 1])
        cell.disclosureIconImageView.image = UIImage(named: indexPath.row < (AppDelegateConstant.user?.userType == tutorType ? 7 : 6) ? "edit" : "right-arrow-dark")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row != 0 else {
            return 119.0
        }
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if AppDelegateConstant.user?.userType == tutorType {
            
            switch indexPath.row {
            case 0:
                
                let tutorCompleteProfileViewController = TutorCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                tutorCompleteProfileViewController.tutorID = UserStore.shared.userID
                tutorCompleteProfileViewController.isFrom = self
                tutorCompleteProfileViewController.tutorImage = self.tutorImage
                self.navigationController?.pushViewController(tutorCompleteProfileViewController, animated: true)
                
            case 1:
                
                let tutorProfileVC = TutorProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                navigationController?.pushViewController(tutorProfileVC, animated: true)
                
            case 2:
                
                let vc = QualificationsNExperiencesVC.instantiateFromAppStoryboard(appStoryboard: .Profile)
                self.navigationController?.pushViewController(vc, animated: true)
                
            case 3:
                
                let subjectRateVC = SubjectRateViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                self.navigationController?.pushViewController(subjectRateVC, animated: true)
                
            case 4:
                
                let creditCardsListVC = CreditCardsListViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                navigationController?.pushViewController(creditCardsListVC, animated: true)

            case 5:
                
                
                let calendarViewController = CalenderViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                navigationController?.pushViewController(calendarViewController, animated: true)
       

            case 6:
                
                let changePasswordViewController = ChangePasswordViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                navigationController?.pushViewController(changePasswordViewController, animated: true)

            case 7:
                
                let myQRCodeViewController = QRViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                myQRCodeViewController.bitlyStr = self.bitlyStr
                navigationController?.pushViewController(myQRCodeViewController, animated: true)
               
            case 8:
                
                 pushToWebViewWithURL(terms_conditions, title: titles[indexPath.row - 1])
              
            case 9:
                
                pushToWebViewWithURL(PrivacyPolicy, title: titles[indexPath.row - 1])
                
            case 10:
                
                pushToWebViewWithURL(ContactUs, title: titles[indexPath.row - 1])

            
            case 11:
                
                let alert = UIAlertController(title: AppName, message: "SIGNOUT".localized, preferredStyle: .alert)
                let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default, handler: { (action) in
                    self.firebaseAuth = Auth.auth()
                    do {
                        try self.firebaseAuth.signOut()
                    } catch let signOutError as NSError {
                        print ("Error signing out: %@", signOutError)
                    }
                    GIDSignIn.sharedInstance()?.signOut()
                    let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
                    fbLoginManager.logOut()
                    //self.navigationController?.popToRootViewController(animated: true)
                    self.logout()
                    //Utilities.logoutUser()
                    //self.navigateToLandingScreen()
                    
                })
                let noAction = UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
                alert.addAction(noAction)
                alert.addAction(yesAction)
                self.present(alert, animated: true, completion: nil)
                
            default:
                break
            }
            
        }else{
            
            switch indexPath.row {
            case 0:
                
                let studentCompleteProfileViewController = StudentCompleteProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                studentCompleteProfileViewController.studentID = UserStore.shared.userID
                studentCompleteProfileViewController.isFrom = self
                self.navigationController?.pushViewController(studentCompleteProfileViewController, animated: true)
                
            case 1:
                
                let studentProfileVC = StudentProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                navigationController?.pushViewController(studentProfileVC, animated: true)
                
            case 2:
                
                let levelSubjectVC = LevelSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                self.navigationController?.pushViewController(levelSubjectVC, animated: true)
                
            case 3:
                
                let creditCardsListVC = CreditCardsListViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                navigationController?.pushViewController(creditCardsListVC, animated: true)
                
            case 4:
                
                let calendarViewController = CalenderViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                navigationController?.pushViewController(calendarViewController, animated: true)
                
            case 5:
                let changePasswordViewController = ChangePasswordViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                navigationController?.pushViewController(changePasswordViewController, animated: true)
                
            case 6:
                let myQRCodeViewController = QRViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                myQRCodeViewController.bitlyStr = self.bitlyStr
                navigationController?.pushViewController(myQRCodeViewController, animated: true)
                
            case 7:
                pushToWebViewWithURL(terms_conditions, title: titles[indexPath.row - 1])
            case 8:
                pushToWebViewWithURL(PrivacyPolicy, title: titles[indexPath.row - 1])
            case 9:
                pushToWebViewWithURL(ContactUs, title: titles[indexPath.row - 1])
            case 10:
                let alert = UIAlertController(title: AppName, message: "SIGNOUT".localized, preferredStyle: .alert)
                let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default, handler: { (action) in
                    self.firebaseAuth = Auth.auth()
                    do {
                        try self.firebaseAuth.signOut()
                    } catch let signOutError as NSError {
                        print ("Error signing out: %@", signOutError)
                    }
                    GIDSignIn.sharedInstance()?.signOut()
                    let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
                    fbLoginManager.logOut()
                    //self.navigationController?.popToRootViewController(animated: true)
                    self.logout()
                    //Utilities.logoutUser()
                    //self.navigateToLandingScreen()
                    
                })
                let noAction = UIAlertAction(title: AlertButton.no.description(), style: .default, handler: nil)
                alert.addAction(noAction)
                alert.addAction(yesAction)
                self.present(alert, animated: true, completion: nil)
            default:
                break
            }
            
        }
    
    }
}

//MARK: - Image Picker Delegate

extension ProfileViewController{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
       // let imageData = image.pngData()
        if let imageData = image.jpeg(.secondLowest){
            uploadProfilePicture(imageData: imageData)
        }
        //uploadProfilePicture(imageData: imageData!)
        dismiss(animated:true, completion: nil)
    }
}

//MARK: ----------------- API Implementation

extension ProfileViewController{
    
    func generateBitlyUrl(){

        guard let userID = AppDelegateConstant.user?.ID else {return}
        guard let userType = AppDelegateConstant.user?.userType else {return}
        
        let userDetail = "\(sharingBaseURL)?type=profile&id=\(userID)&user_type=\(userType)"//"\(sharingBaseURL)redirect_url?type=profile&id=\(userID)&user_type=\(userType)"
        
        
        Bitly.shorten(userDetail) { (response, error) in
        if error == nil{
            self.bitlyStr = response?.bitlink ?? userDetail
        }
        }
    }
    
    // LogOutUser
    fileprivate func logout(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
         let deviceToken = UserStore.shared.deviceToken 
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let logoutUserObserver = ApiManager.shared.apiService.UserLogout(deviceToken)
        let logoutUserDisposable = logoutUserObserver.subscribe(onNext: { (msg) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
               
                Utilities.logoutUser()
                self.showSimpleAlertWithMessage(msg)
                self.navigateToLandingScreen()
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
            }
        })
        return logoutUserDisposable.disposed(by: disposableBag)
        
    }
    
    // Get Student and Levels
    func getStudentLavelNSubject(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else{return}
        
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let getlevelAndSubject = ApiManager.shared.apiService.getStudentLavelNSubject("\(userID)")
        let getlevelAndSubjectDisposable = getlevelAndSubject.subscribe(onNext: { (levelNSubjects) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                self.addLevelSubject = levelNSubjects
                let levelSubjectVC = LevelSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                levelSubjectVC.addLevelSubject = self.addLevelSubject
                self.navigationController?.pushViewController(levelSubjectVC, animated: true)
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
            }
        })
        return getlevelAndSubjectDisposable.disposed(by: disposableBag)
        
    }
    
    func uploadProfilePicture(imageData: Data) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
//        let paramDict = [String: String]()
        
        let url =   "\(baseUrl)\(PathURl.UploadTutorPicture.rawValue)"
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Accept": "application/json",
            "Authorization": "Bearer \(UserStore.shared.token)"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            for (key, value) in paramDict {
//                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
//            }
            let fileName = "profile_picture.png"
            let mimeType = imageData.mimeType
            
            multipartFormData.append(imageData, withName: "profile_image", fileName: fileName, mimeType: mimeType)
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
//                    print("Succesfully uploaded")
                    DispatchQueue.main.async(execute: {
                        Utilities.hideHUD(forView: self.view)
                    })
                    if let _ = response.error{
                        self.showSimpleAlertWithMessage("There seems to be an error. Please try again later.".localized)
                        return
                    }
                    let json = try? JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                    if let responseDict = json as? [String: AnyObject], let message = responseDict["message"] as? String {
                        
                        if let data = responseDict["data"] as? [String: AnyObject]{
                           AppDelegateConstant.user?.imageURL = data["real_image"] as? String
                            self.tableView.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .none)
                        }
                       self.showSimpleAlertWithMessage(message)
                      
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    Utilities.hideHUD(forView: self.view)
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            }
        }
    }
    
}
