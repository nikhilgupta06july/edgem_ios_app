//
//  StudentCompleteProfileViewController.swift
//  Edgem
//
//  Created by Namespace on 08/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift
import EasyTipView

class StudentCompleteProfileViewController: BaseViewController {
    
    // MARK: ------ Properties
    
    weak var tipView: EasyTipView?
    var preferences: EasyTipView.Preferences!
    
    var studentID: Int?
    var userData: User?
    
    var disposableBag = DisposeBag()
    
    var currentIndex = 1
    var rating = [Rating]()
    var pagination = Pagination.init()
    
    var firstName = ""
    var lastName = ""
    var studentRating = ""
    var image: UIImage?
    var isFrom: UIViewController?
    
    var commonSubjectOfStudentAndTutor: [Subject] = Array()
    
    // MARK: ------ IBOutlets
    
    @IBOutlet weak var settingIcon: UIButton!{
        didSet{
            settingIcon.isHidden = studentID != UserStore.shared.userID
        }
    }
    
    @IBOutlet weak var distanceLabelValue: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var distanceLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var BottomContainerView: UIView!
    @IBOutlet weak var offerButton: UIButton!
    @IBOutlet weak var offerHolderView: UIView!
    @IBOutlet weak var noRecordLbl: UILabel!
    @IBOutlet weak var studentProfileIcon: UIImageView!
    @IBOutlet weak var studentName: UILabel!
    @IBOutlet weak var verifiedIcon: UIImageView!
    @IBOutlet weak var verifiedLabel: UILabel!
    @IBOutlet weak var totalRatingLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var aboutButton: UIButton!
    @IBOutlet weak var ratingButton: UIButton!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var onlineStatus: UIImageView!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        preferences = EasyTipView.Preferences()
        preferences.drawing.arrowPosition = .bottom
        preferences.drawing.textAlignment = .center
        preferences.drawing.font = UIFont(name: QuicksandMedium, size: 15)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.positioning.textHInset = 40
        preferences.positioning.textVInset = 10
        preferences.drawing.backgroundColor = UIColor(red: 23/255, green: 23/255, blue: 23/255, alpha: 0.80)
        preferences.animating.dismissTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1.5
        preferences.animating.dismissDuration = 1.5
        
        EasyTipView.globalPreferences = preferences
        
        offerButton.backgroundColor = UIColor(red: 255/255, green: 210/255, blue: 113/255, alpha: 0.4)
        
        distanceLabelHeight.constant = 0
        distanceLabel.isHidden = true
        distanceLabelValue.isHidden = true
        
        setTabButtons(currentIndex)
        
        DispatchQueue.main.async {
            if let id = self.studentID{
                self.setUPView()
                self.getUserDetails(id)
                self.rating = []
                self.getUserRatings(id)
                
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(userConnected), name: .userConnected, object: nil)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let tipView = self.tipView{
            tipView.dismiss()
            // toolTipBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //----> This will manage the appearence of makeOffer button
        if isFrom is ProfileViewController  || isFrom is ChatViewController{
            bottomContainerViewHeight.constant = 0
            BottomContainerView.isHidden = true
            if let tipView = self.tipView{
                tipView.dismiss()
            }
        }else{
            bottomContainerViewHeight.constant = 90
            BottomContainerView.isHidden = false
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let tipView = self.tipView{
            tipView.dismiss()
            // toolTipBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // profile Icon
        
//        studentProfileIcon.layer.cornerRadius = studentProfileIcon.frame.width/
        
        // student name
        studentName.textColor = theamBlackColor
        studentName.font = UIFont(name: QuicksandBold, size: 20)
        
        // verified Label
        verifiedLabel.text = "Verified".localized
        verifiedLabel.textColor = theamGrayColor
        verifiedLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        
        // totalRating Label
        totalRatingLabel.textColor = theamBlackColor
        verifiedLabel.font = UIFont(name: QuicksandMedium, size: 15)
        
        // ratingLabel
//        ratingLabel.text = "Rating".localized
        ratingLabel.textColor = theamGrayColor
        verifiedLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        
       // aboutButton
        aboutButton.setTitle("About".localized, for: .normal)
        
        // bottomContainerView
        BottomContainerView.layer.borderWidth = 2
        BottomContainerView.layer.borderColor = UIColor(red: 119, green: 119, blue: 119, alpha: 1).cgColor
        BottomContainerView.layer.shadowOffset = CGSize(width: 0, height: -4)
        BottomContainerView.layer.shadowRadius = 4
        BottomContainerView.layer.shadowColor = UIColor.black.cgColor
        BottomContainerView.layer.shadowOpacity = 0.09
        
        // confirm button attributes
        offerButton.layer.cornerRadius = 5
       // offerButton.backgroundColor = buttonBackGroundColor
        offerButton.clipsToBounds = true
        
        offerButton.setTitle("Make Offer".localized, for: .normal)
        offerButton.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
//        offerButton.titleLabel?.textColor = theamBlackColor
        
        // apply button shadow
        offerHolderView.layer.cornerRadius = 5
        offerHolderView.layer.shadowColor = UIColor.black.cgColor
        offerHolderView.layer.shadowOpacity = 0.12
        offerHolderView.layer.shadowRadius = 4
        offerHolderView.layer.shadowOffset = .zero
        offerHolderView.layer.shadowPath = UIBezierPath(rect: offerHolderView.bounds).cgPath
    }
    
    // MARK: ----- @IBActions
    
    @IBAction func didTapedOfferBtn(_ sender: UIButton) {
        //return
        if let user = self.userData {
            let edgemOfferVC = EdgemOfferViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
            edgemOfferVC.userData = user
            edgemOfferVC.commonSubjectOfStudentAndTutor = self.commonSubjectOfStudentAndTutor
            self.navigationController?.pushViewController(edgemOfferVC, animated: true)
        }else{
            return
        }
        
    }
    
    @IBAction func tabButtonsTapped(_ sender: UIButton) {
        setTabButtons(sender.tag)
        showTransitionInView(sender.tag)
    }
    
    @IBAction func settingButtonTapped(_ sender: Any) {
        let settingsViewController = SettingsViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        self.navigationController?.pushViewController(settingsViewController, animated: true)
    }
    
    // MARK: Private Functions
    
    fileprivate func popUpForNoMatchingSubject() {
        let text = "No matching subjects found.".localized
        
        if let tipView = self.tipView{
            
            tipView.dismiss()
            
        }else{
            
            if isFrom is ProfileViewController  || isFrom is ChatViewController{
           
                // do nothing
                
            }else{
               
                let tip = EasyTipView(text: text, preferences: self.preferences, delegate: self)
                
                tip.show(forView: self.offerHolderView)
                
                tipView = tip
                
            }
            
        }
    }
    
    @objc func userConnected(){
        print("user online")
        
        if let userId = self.studentID {
            let image = (Global.idArrays.contains(userId)) ? "user_online" : "user_offline"
            self.onlineStatus.image = UIImage(named: image)
        }
        
    }
    
    fileprivate func compareSubjects(_ userSubjectsArray: [Subject], _ selectedUserSubjectsArray: [Subject]){
        
        let array1 = userSubjectsArray
        let array2 = selectedUserSubjectsArray
        
        let userSubjectsArrayStr = array1.map{$0.name}
        let selectedUserSubjectsArrayStr = array2.map({$0.name})
        
        let commonElements = Array(Set(userSubjectsArrayStr).intersection(Set(selectedUserSubjectsArrayStr)))
        print(commonElements)
        if commonElements.count > 0{
            selectedUserSubjectsArray.forEach { (subject) in
                for subject_common in commonElements{
                    if subject_common! == subject.name{
                        self.commonSubjectOfStudentAndTutor.append(subject)
                    }
                }
            }
        }
        if userSubjectsArrayStr.intersects(with: selectedUserSubjectsArrayStr) {
            enableDisableMakeOfferBtn(true)
        } else {
            enableDisableMakeOfferBtn(false)
        }
    }
    
    
    fileprivate func enableDisableMakeOfferBtn(_ status: Bool){
        
        if status == false{
            offerButton.isUserInteractionEnabled = false
            offerButton.backgroundColor = UIColor(red: 255/255, green: 210/255, blue: 113/255, alpha: 0.4)
            offerButton.titleLabel?.textColor = UIColor(r: 139, g: 139, b: 139, a: 1)
            popUpForNoMatchingSubject()
        }else{
            offerButton.isUserInteractionEnabled = true
            offerButton.backgroundColor = theamAppDarkColor
            offerButton.titleLabel?.textColor = theamBlackColor
        }
        
    }
    
    private func setTabButtons(_ forIndex: Int){
        self.noRecordLbl.isHidden = true
        aboutButton.titleLabel?.font = UIFont(name: forIndex == 1 ? QuicksandBold : QuicksandRegular, size: 18)
        ratingButton.titleLabel?.font = UIFont(name: forIndex == 2 ? QuicksandBold : QuicksandRegular, size: 18)
        
    }
    
    func showTransitionInView(_ forIndex: Int){
        
         let movingX = forIndex<currentIndex ? ScreenWidth : -ScreenWidth
         currentIndex = forIndex
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
            self.tableView.transform = CGAffineTransform.identity.translatedBy(x:movingX, y: 0)
            self.sepratorView.transform = CGAffineTransform.identity.translatedBy(x:CGFloat(forIndex-1)*(ScreenWidth/2), y: 0)
            
        },completion: { finished in
            self.noRecordLbl.isHidden = forIndex == 1 || self.rating.count>0
            self.tableView.transform = CGAffineTransform.identity.translatedBy(x:0, y: 0)
            self.tableView.reloadData()
        })
    }
    
    func setUPView(){
        
        if isFrom is ProfileViewController {
            
            let userType = UserStore.shared.selectedUserType
            
            if userType == "Parent"{
                
                if  UserStore.shared.logInType == 1{
                     if let fName = AppDelegateConstant.user?.parentFirstName{
                                    firstName = fName
                     }
                    if let lName = AppDelegateConstant.user?.parentLastName{
                                    lastName = lName
                     }
                } else if  UserStore.shared.logInType == 2{
                     if let fName = AppDelegateConstant.user?.firstName{
                                    firstName = fName
                     }
                    if let lName = AppDelegateConstant.user?.lastName{
                                    lastName = lName
                     }
                }
            }else{
                if let firstName = AppDelegateConstant.user?.firstName{
                      self.firstName = firstName
                }
                if let lastName = AppDelegateConstant.user?.lastName{
                    self.lastName = lastName
                }

            }
            
            // name
            self.studentName.text = self.firstName + " " + self.lastName
            
            // profile image
            if let imageURL = AppDelegateConstant.user?.imageURL{
                let imageProvider = ImageProvider()
                guard let url = URL(string: imageURL) else{return}
                imageProvider.requestImage(from:  url) { image in
                    self.studentProfileIcon.image = image
                }
            }else{
                studentProfileIcon.image = UIImage(named: "femaleIcon")
            }
            
              // rating
            
           
            if let rating = AppDelegateConstant.user?.ratings, let rating_in_num = Int(rating){
                if rating_in_num > 0{
                     self.totalRatingLabel.text = "\(rating)/5"
                     ratingLabel.text = "Rating".localized
                }else{
                     self.totalRatingLabel.text = ""
                     self.ratingLabel.text = "No ratings".localized
                }
                
            }
            
            let image = AppDelegateConstant.user?.onlineStatus == 1 ? #imageLiteral(resourceName: "user_online") : #imageLiteral(resourceName: "user_offline")
            self.onlineStatus.image = image
            
        }else{
            // name
            if let userData = self.userData {
                self.studentName.text = userData.firstName + " " + userData.lastName
                
                // profile image
                if let imageURL = userData.imageURL{
                    let imageProvider = ImageProvider()
                    guard let url = URL(string: imageURL) else{return}
                    imageProvider.requestImage(from:  url) { image in
                        self.studentProfileIcon.image = image
                    }
                }else{
                    studentProfileIcon.image = UIImage(named: "femaleIcon")
                }
                
                // rating
                if let rating = (userData.ratings),let rating_in_num = Int(rating) {
                    
                    if rating_in_num > 0{
                        self.totalRatingLabel.text = "\(rating)/5"
                        ratingLabel.text = "Rating".localized
                    }else{
                        self.totalRatingLabel.text = ""
                        self.ratingLabel.text = "No ratings".localized
                    }
                    
                }
                
                let image = userData.onlineStatus == 1 ? #imageLiteral(resourceName: "user_online") : #imageLiteral(resourceName: "user_offline")
                self.onlineStatus.image = image
                
                // Distance
                if let distance = userData.distance{
                    distanceLabelHeight.constant = 57
                    distanceLabel.isHidden = false
                    distanceLabelValue.isHidden = false
                    self.distanceLabelValue.text = distance
                }
            }
        }
        
    }
    
}

extension StudentCompleteProfileViewController: EasyTipViewDelegate{
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        // toolTipBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
    }
}

extension StudentCompleteProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard (userData != nil) else {
            return 0
        }
        return currentIndex == 1 ? 3 : rating.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard currentIndex == 1 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: RatingTableViewCell.cellIdentifire(), for: indexPath) as! RatingTableViewCell
            if rating.count > 0{
                cell.configureCellWithRating(rating[indexPath.row])
            }
           
            return cell
            
        }
        if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: AvailabilityTableViewCell.cellIdentifier(), for: indexPath) as! AvailabilityTableViewCell
            cell.reloadCalendarViewData((userData?.availibility)!)
            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: StudentProfileAboutTableViewCell.cellIdentifire(), for: indexPath) as! StudentProfileAboutTableViewCell
        guard indexPath.row == 0 else{
            cell.configureCellWithSubject((userData?.subjects)!)
            return cell
        }
            if let data = userData, let lavel = data.level{
                  cell.configureCellWithLevel(lavel)
            }
      
        return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let tipView = self.tipView{
            tipView.dismiss()
            // toolTipBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }
    }
    
    
}

// MARK: ------ API Implementation
extension StudentCompleteProfileViewController{
    
    func getUserDetails(_ userID: Int){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let dict: [String : Any] = ["id" : "\(userID)"]
        
        let userObserver = ApiManager.shared.apiService.getUserDetails(dict as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(user) in
            DispatchQueue.main.async {
                                
                Utilities.hideHUD(forView: self.view)
                
                self.userData = user
                if let selectedUserSubject = user.subjects,  let loginUserSubject = AppDelegateConstant.user?.subjects{
                    self.compareSubjects(selectedUserSubject, loginUserSubject)
                }
                self.setUPView()
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    
    
    func getUserRatings(_ userID: Int){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let dict: [String : Any] = ["id" : "\(userID)", "page": pagination.currentPage+1]
        
        let userObserver = ApiManager.shared.apiService.getUserRating(dict as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(ratingPagination) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                
                print(ratingPagination)
                self.pagination = ratingPagination.pagination
                self.rating += ratingPagination.rating
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
}

