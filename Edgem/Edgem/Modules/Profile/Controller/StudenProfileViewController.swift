//
//  StudentProfileViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 21/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

enum StudentProfileTextFieldType: Int {
    case firstName = 0, lastName,  gender, birthday, emailAddress, contactNumber
}
enum StudentParentProfileTextFieldType: Int{
    case parentFirstName = 0, parentLastName,  relationship, parentEmail, parentContactNumber
}
enum StudentProfileAddressTextFieldType: Int{
    case postalCode = 0, addressLine1,  addressLine2
}

class StudentProfileViewController: BaseViewController {

    @IBOutlet weak var headerTitle: UILabel!{
        didSet{
            headerTitle.text = GeneralString.personalInformation.description()
        }
    }
    @IBOutlet weak var tabButtonOne: UIButton!
    @IBOutlet weak var tabButtonTwo: UIButton!
    @IBOutlet weak var tabButtonThree: UIButton!
    @IBOutlet weak var tabLineImg: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var saveBtn: UIButton!
    
    var currentIndex = 1
    var userData: User!
    var editedUser: User!
    var disposableBag = DisposeBag()
    var activeTextField: UITextField!
    let countryCodePicker = UIPickerView()
    let relationWithChildPicker = UIPickerView()
    
    var parentTitle = ["First Name*", "Last Name*", "Relationship*", "Email*", "Parent's Contact Number*"]
    var childTitles = ["First Name*", "Last Name*", "Gender*", "Birthday*", "Email*", "Child's Contact Number"]
    var addressTitles = ["Postal code*", "Address line 1*", "Address line 2"]
    var countryCodeOptions = ["+65"]
    
     //var emailAndContact: EmailAndContact?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if let _user = AppDelegateConstant.user{
//            userData = _user
//        }
//        editedUser = User.init(userData)
        // Do any additional setup after loading the view.
        setupViewAccording(UserStore.shared.selectedUserType)
        initialSetUpForCountryCodePicker()

//        let otpVC = OTPViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
//        otpVC.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let _user = AppDelegateConstant.user{
            userData = _user
        }
        editedUser = User.init(userData)
        self.tableView.reloadData()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        /// AppVariables.emailAndContact is going to be used when we are going to update email/contact. This is important to assign nil when it is updated so that  AppVariables.emailAndContact did not contains any memory
        
        //AppVariables.emailAndContact = nil
    }
    
     //MARK:- @IBActions
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        navigateBack(sender: sender)
    }
    
    @IBAction func tabButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        setTabButtons(sender.tag)
        showTransitionInView(sender.tag)
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
         //self.showSimpleAlertWithMessage("Work in progress")
        updateUserInfoData()
        return
    }
    //MARK:- Helping Function
    func setupViewAccording(_ userType: String){

        tabButtonOne.setTitle(GeneralString.parent.description() , for: .normal)
        tabButtonTwo.setTitle(GeneralString.child.description() , for: .normal)
        tabButtonThree.setTitle(GeneralString.address.description() , for: .normal)
      
        setTabButtons(currentIndex)
    }
    
    func setTabButtons(_ forIndex: Int){
        
        tabButtonOne.titleLabel?.font = UIFont(name: forIndex == 1 ? QuicksandBold : QuicksandRegular, size: 18)
        tabButtonTwo.titleLabel?.font = UIFont(name: forIndex == 2 ? QuicksandBold : QuicksandRegular, size: 18)
        tabButtonThree.titleLabel?.font = UIFont(name: forIndex == 3 ? QuicksandBold : QuicksandRegular, size: 18)
        
    }
    
    func showTransitionInView(_ forIndex: Int){
        
        let movingX = forIndex<currentIndex ? ScreenWidth : -ScreenWidth
        let lineMovingX = CGFloat(forIndex-1)*(ScreenWidth/3)
        
        currentIndex = forIndex
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
            
            self.tableView.transform = CGAffineTransform.identity.translatedBy(x:movingX, y: 0)
            self.tabLineImg.transform = CGAffineTransform.identity.translatedBy(x:lineMovingX, y: 0)
            
            
        },completion: { finished in
            
            self.tableView.transform = CGAffineTransform.identity.translatedBy(x:0, y: 0)
            self.tableView.reloadData()
            
        })
        
    }
    
    func getLatLngForZip(zipCode: String) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        var request: NSMutableURLRequest? = nil
        let baseUrl = BaseUrlForGoogleMap
        let apikey = GoogleApiKey
        //"\(baseUrl)address=\(zipCode)&key=\(apikey)&components=country:SG"
        //"https://maps.googleapis.com/maps/api/geocode/json?address="+zipCode+"&country=SG&sensor=false&key=\(apikey)"
        let url = "\(baseUrl)address=\(zipCode)&key=\(apikey)&components=country:SG"
        if let aText = URL(string: url) {
            request = NSMutableURLRequest(url: aText)
        }
        request?.httpMethod = "POST"
        var response: URLResponse?
        var responseData: Data? = nil
        Utilities.showHUD(forView: self.view, excludeViews: [])
        if let aRequest = request {
            responseData = try? NSURLConnection.sendSynchronousRequest(aRequest as URLRequest, returning: &response)
        }
        var resSrt: String? = nil
        if let aData = responseData {
            resSrt = String(data: aData, encoding: .ascii)
        }
        
        print("got response==\(resSrt ?? "")")
        
        var dict: [String : AnyObject]? = nil
        if let aData = responseData {
            dict = try! JSONSerialization.jsonObject(with: aData, options: .mutableContainers) as? [String : AnyObject]
        }
        let arr = (dict as! [String:AnyObject])["results"] as! NSArray
        if arr.count > 0{
            let lataddre = ((((((dict as! [String:AnyObject])["results"] as! NSArray)[0]) as! [String:AnyObject])["geometry"] as! [String:AnyObject])["location"] as! [String:AnyObject])["lat"] as! Double
            let longaddr = ((((((dict as! [String:AnyObject])["results"] as! NSArray)[0]) as! [String:AnyObject])["geometry"] as! [String:AnyObject])["location"] as! [String:AnyObject])["lng"] as! Double
            getAddressForLatLng(latitude: "\(lataddre)", longitude: "\(longaddr)", zipCode: zipCode)
        }else{
            Utilities.hideHUD(forView: self.view)
            self.invalidPostalCodeEntered()
            self.showSimpleAlertWithMessage("You have entered an invalid postal code".localized)
            
        }
        
        
    }
    
    func getAddressForLatLng(latitude: String, longitude: String, zipCode: String) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let url = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?address=\(latitude),\(longitude)&key=\(GoogleApiKey)&country=SG&sensor=false")
        let data = NSData(contentsOf: url! as URL)
        if let data = data{
            let string1 = String(data: data as Data, encoding: String.Encoding.utf8) ?? "Data could not be printed"
            print(string1)
        }
        if data != nil {
            let json = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            print(json)
            Utilities.hideHUD(forView: self.view)
            var address = ""
            if let result = json["results"] as? NSArray   {
                if result.count > 0 {
                    for index in 0..<result.count{
                        if let result = result[index] as? [String:AnyObject]{
                            if let addressComponents = result["address_components"] as? NSArray{
                                for addressComponentsIndex in 0..<addressComponents.count{
                                    if let components = addressComponents[addressComponentsIndex] as? [String:AnyObject] {
                                        if let componentTypes = components["types"] as? NSArray{
                                            for componentTypeIndex in 0..<componentTypes.count{
                                                if let type = componentTypes[componentTypeIndex] as? String{
                                                    if type == "postal_code" && components["long_name"] as! String == zipCode{
                                                        print("postal_code key existed and address : \(String(describing:  result["formatted_address"]))")
                                                        if let addr =  result["formatted_address"] as? String {
                                                            address = addr
                                                        }
                                                    }else{
                                                        print("postal_code key not existed")
                                                    }
                                                }
                                                
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //----
            let addresss = editedUser.userAddress
            if address != ""{
               
                let addressComponent = address.components(separatedBy: ",")
                
                if addressComponent.count > 1{
                    print (address)
            
                    addresss?[0].addressLine1 = addressComponent[0]
                    var addr2: [String] = Array()
                    for index in 1..<addressComponent.count{
                        addr2.append(addressComponent[index])
                    }
                    addresss?[0].addressLine2 = addr2.joined(separator: ",")
                }else if  addressComponent.count == 1 && addressComponent[0] != ""{
                    addresss?[0].addressLine1 = addressComponent[0]
                    addresss?[0].addressLine2 = ""
                }
                
                 addresss?[0].lattitude = latitude
                 addresss?[0].longitude = longitude
                self.tableView.reloadData()
            }else{
                self.showSimpleAlertWithMessage("You have entered an invalid postal code".localized)
                addresss?[0].addressLine1 = ""
                addresss?[0].addressLine2 = ""
                self.tableView.reloadData()
            }
            
        }
    }

}

// MARK: -------- Protocols

//extension StudentProfileViewController: OTPViewControllerProtocol{
//    func returnEmailOrContact(_ emailOrContact: String) {
//        print(emailOrContact)
//    }
//}

extension StudentProfileViewController: RefreshButtonTappedDelegate{
    
    func refreshBtnTapped() {
        for index in 0..<addressTitles.count {
          
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case 0:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.customTextField.text = ""
        
                }
            case 1:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                 cell.customTextField.text = ""
                }
                
            case 2:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.customTextField.text = ""
                }
                
            default:
                break
            }
        }
    }
    
    
    /// This function will be called, if user entered wrong postal code, so we have to assing blank to 'address line 1' and 'address line 2'
    fileprivate func invalidPostalCodeEntered(){
        
        let address = editedUser.userAddress
        address?[0].addressLine1 = ""
        address?[0].addressLine2 = ""
        
        for index in 0..<addressTitles.count {
            
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case 1:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.customTextField.text = ""
                }
                
            case 2:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.customTextField.text = ""
                }
                
            default:
                break
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updatedText.count <= 6
    }
}

extension StudentProfileViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return currentIndex == 1 ? 5 : currentIndex == 2 ? 6 : currentIndex == 3 ? 3 : 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if currentIndex == 1{
                return indexPath.row == parentTitle.count-1 ? LoadEdgemDualTextFieldTableViewCell(indexPath, parentTitle[indexPath.row]) : LoadEdgemTextFieldTableViewCell(indexPath, parentTitle[indexPath.row])
            }else if currentIndex == 2{
                return indexPath.row == childTitles.count-1 ? LoadEdgemDualTextFieldTableViewCell(indexPath, childTitles[indexPath.row]) : LoadEdgemTextFieldTableViewCell(indexPath, childTitles[indexPath.row])
            }else{
                return LoadEdgemTextFieldTableViewCell(indexPath, addressTitles[indexPath.row])
            }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func LoadEdgemTextFieldTableViewCell(_ indexPath: IndexPath, _ titleText: String) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EdgemTextFieldTableViewCell.cellIdentifier(), for: indexPath) as? EdgemTextFieldTableViewCell  else{return UITableViewCell()}
        
        cell.configureUpdateProfileCellForRowAtIndex(indexPath.row, withText: titleText, currentIndex: currentIndex)
        

            if currentIndex == 1{
                updateDataIntoParentProfileTextField(cell.customTextField, fromCell: true)
                configureParentProfileTextField(cell.customTextField)
            }else if currentIndex == 2{
                updateDataIntoChildProfileTextField(cell.customTextField, fromCell: true)
                configureProfileTextField(cell.customTextField)
            }else{
                updateDataIntoProfileAddressTextField(cell.customTextField, fromCell: true)
                configureProfileAddressTextField(cell.customTextField)
            }
        cell.refreshDelegate = self
        cell.selectionStyle = .none
        return cell
    }
    func LoadEdgemDualTextFieldTableViewCell(_ indexPath: IndexPath, _ titleText: String) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: EdgemDualTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemDualTextFieldTableViewCell
        cell.configureUpdateProfileCellForRowAtIndex(title: ["+65",titleText], index: indexPath.row)
        
            if currentIndex == 1{
                updateDataIntoParentProfileTextField(cell.textFieldOne, fromCell: true)
                updateDataIntoParentProfileTextField(cell.textFieldTwo, fromCell: true)
                
                configureParentProfileTextField(cell.textFieldOne)
                configureParentProfileTextField(cell.textFieldTwo)
            }else if currentIndex == 2{
                updateDataIntoChildProfileTextField(cell.textFieldOne, fromCell: true)
                updateDataIntoChildProfileTextField(cell.textFieldTwo, fromCell: true)
                configureProfileTextField(cell.textFieldOne)
                configureProfileTextField(cell.textFieldTwo)
            }
       
        cell.selectionStyle = .none
        return cell
    }
    
    
    
    //MARK:- Update Data in fields
    func updateDataIntoParentProfileTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
        
        switch customTextField.tag {
            
        case StudentParentProfileTextFieldType.parentFirstName.rawValue:
            fromCell ? (customTextField.text = editedUser.parentFirstName) : (editedUser.parentFirstName = customTextField.text!)
            break
        case StudentParentProfileTextFieldType.parentLastName.rawValue:
            fromCell ? (customTextField.text = editedUser.parentLastName) : (editedUser.parentLastName = customTextField.text!)
            break
        case StudentParentProfileTextFieldType.relationship.rawValue:
            fromCell ? (customTextField.text = editedUser.relationwithchild) : (editedUser.relationwithchild = customTextField.text!)
            break
        case StudentParentProfileTextFieldType.parentEmail.rawValue:
            //fromCell ? (customTextField.text = editedUser.parentEmail) : (editedUser.parentEmail = customTextField.text!)
            
            if fromCell {
                if let emailAndContactObject = AppVariables.emailAndContact_parent, let email = emailAndContactObject.email{
                    customTextField.text     = email
                    editedUser.parentEmail = email
                }else{
                    (customTextField.text = editedUser.parentEmail)
                }
            }else{
                (editedUser.parentEmail = customTextField.text!)
            }
            break
           
        case StudentParentProfileTextFieldType.parentContactNumber.rawValue:
//            if customTextField.title?.contains("Contact") ?? false {
//                fromCell ? (customTextField.text = editedUser.parentContactNumber) : (editedUser.parentContactNumber = customTextField.text!)
//            } else {
//                var countryCode = ""
//                if let code = editedUser.parentContactCode{
//                    let charset = CharacterSet(charactersIn: "+")
//                    if code.rangeOfCharacter(from: charset) != nil {
//                        countryCode = code
//                    }else{
//                        countryCode = "+"+code
//                    }
//
//                }
//                fromCell ? (customTextField.text = countryCode) : (editedUser.parentContactCode = customTextField.text!)
//            }
            if customTextField.title?.contains("Contact") ?? false {
                fromCell ? (customTextField.text = editedUser.parentContactNumber) : (editedUser.parentContactNumber = customTextField.text!)
                if fromCell {
                    if let emailAndContactObject = AppVariables.emailAndContact_parent, let contact = emailAndContactObject.contact{
                        customTextField.text = contact
                        editedUser.parentContactNumber = contact
                        //AppVariables.emailAndContact = nil
                    }else{
                        (customTextField.text = editedUser.parentContactNumber)
                        
                    }
                }else{
                    (editedUser.parentContactNumber = customTextField.text!)
                }
            } else {
                var countryCode = ""
                if let code = editedUser.parentContactCode{
                    let charset = CharacterSet(charactersIn: "+")
                    if code.rangeOfCharacter(from: charset) != nil {
                        countryCode = code
                    }else{
                        countryCode = "+"+code
                    }
                    
                }
                
                if fromCell {
                    if let emailAndContactObject = AppVariables.emailAndContact_parent, let countryCode = emailAndContactObject.countryCode{
                        customTextField.text = countryCode
                        editedUser.parentContactCode = countryCode
                        // AppVariables.emailAndContact = nil
                    }else{
                        (customTextField.text = countryCode)
                    }
                }else{
                    (editedUser.parentContactCode = customTextField.text!)
                }
                
                //fromCell ? (customTextField.text = countryCode) : (editedUser.countryContactCode = customTextField.text!)
            }
            
            break
        default:
            break
            
        }
    }
    
    func updateDataIntoChildProfileTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
        
        switch customTextField.tag {
        case StudentProfileTextFieldType.firstName.rawValue:
            fromCell ? (customTextField.text = editedUser.firstName) : (editedUser.firstName = customTextField.text!)
            break
        case StudentProfileTextFieldType.lastName.rawValue:
            fromCell ? (customTextField.text = editedUser.lastName) : (editedUser.lastName = customTextField.text!)
            break
        case StudentProfileTextFieldType.gender.rawValue:
            fromCell ? (customTextField.text = editedUser.gender) : (editedUser.gender = customTextField.text!)
            break
        case StudentProfileTextFieldType.birthday.rawValue:
            fromCell ? (customTextField.text = editedUser.birthday) : (editedUser.birthday = customTextField.text!)
            break
        case StudentProfileTextFieldType.emailAddress.rawValue:
         //   fromCell ? (customTextField.text = editedUser.email) : (editedUser.email = customTextField.text!)
            if fromCell {
                if let emailAndContactObject = AppVariables.emailAndContact, let email = emailAndContactObject.email{
                    customTextField.text = email
                    editedUser.email = email
                }else{
                    (customTextField.text = editedUser.email)
                }
            }else{
                    (editedUser.email = customTextField.text!)
            }
            break
        case StudentProfileTextFieldType.contactNumber.rawValue:
            if customTextField.title?.contains("Contact") ?? false {
                fromCell ? (customTextField.text = editedUser.contactNumber) : (editedUser.contactNumber = customTextField.text!)
                if fromCell {
                    if let emailAndContactObject = AppVariables.emailAndContact, let contact = emailAndContactObject.contact{
                        customTextField.text = contact
                        editedUser.contactNumber = contact
                        //AppVariables.emailAndContact = nil
                    }else{
                        (customTextField.text = editedUser.contactNumber)
                        
                    }
                }else{
                    (editedUser.contactNumber = customTextField.text!)
                }
            } else {
                var countryCode = ""
                if let code = editedUser.countryContactCode{
                    let charset = CharacterSet(charactersIn: "+")
                    if code.rangeOfCharacter(from: charset) != nil {
                       countryCode = code
                    }else{
                         countryCode = "+"+code
                    }
                    
                }
                
                if fromCell {
                    if let emailAndContactObject = AppVariables.emailAndContact, let countryCode = emailAndContactObject.countryCode{
                        customTextField.text = countryCode
                        editedUser.countryContactCode = countryCode
                       // AppVariables.emailAndContact = nil
                    }else{
                        (customTextField.text = countryCode)
                    }
                }else{
                    (editedUser.countryContactCode = customTextField.text!)
                }
            
                //fromCell ? (customTextField.text = countryCode) : (editedUser.countryContactCode = customTextField.text!)
            }
            
            break
        default:
            break
        }
    }
    
    func updateDataIntoProfileAddressTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
        
        let address = editedUser.userAddress
        switch customTextField.tag {
        case StudentProfileAddressTextFieldType.postalCode.rawValue:
            fromCell ? (customTextField.text = address?[0].postalCode) : (address?[0].postalCode = customTextField.text!)
            break
        case StudentProfileAddressTextFieldType.addressLine1.rawValue:
            fromCell ? (customTextField.text = address?[0].addressLine1) : (address?[0].addressLine1 = customTextField.text!)
            break
        case StudentProfileAddressTextFieldType.addressLine2.rawValue:
            fromCell ? (customTextField.text = address?[0].addressLine2) : (address?[0].addressLine2 = customTextField.text!)
            break
        default:
            break
        }
    }
    /*
     case StudentParentProfileTextFieldType.parentFirstName.rawValue:
     fromCell ? (customTextField.text = editedUser.parentFirstName) : (editedUser.parentFirstName = customTextField.text!)
     break
     case StudentParentProfileTextFieldType.parentLastName.rawValue:
     fromCell ? (customTextField.text = editedUser.parentLastName) : (editedUser.parentLastName = customTextField.text!)
     break
     case StudentParentProfileTextFieldType.relationship.rawValue:
     fromCell ? (customTextField.text = editedUser.relationwithchild) : (editedUser.relationwithchild = customTextField.text!)
     break
     */
    
    //MARK:- Configure text fields
    func configureParentProfileTextField(_ customTextField: EdgemCustomTextField) {
        
        customTextField.inputView = nil
        //customTextField.backgroundColor = AppDelegateConstant.user?.userType == parentType ? UIColor.white : theamBorderGrayColor
        customTextField.placeholderColor = AppDelegateConstant.user?.userType == parentType ? theamBlackColor : theamGrayColor
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        customTextField.hideImage()
        
        switch customTextField.tag {
            
        case StudentParentProfileTextFieldType.parentFirstName.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.textFont = textFieldDefaultFont
            break
        case StudentParentProfileTextFieldType.parentLastName.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.textFont = textFieldDefaultFont
            break
        case StudentParentProfileTextFieldType.relationship.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.textFont = textFieldDefaultFont
            break
        case StudentParentProfileTextFieldType.parentEmail.rawValue:
            customTextField.delegate = self
            customTextField.isUserInteractionEnabled = AppDelegateConstant.user?.userType == parentType ? true : false
            customTextField.keyboardType = .emailAddress
            break
        case StudentParentProfileTextFieldType.parentContactNumber.rawValue:
            customTextField.delegate = self
            customTextField.isUserInteractionEnabled = AppDelegateConstant.user?.userType == parentType ? true : false
            if customTextField.title?.contains("Contact") ?? false{
                customTextField.keyboardType = .phonePad
            } else{
                customTextField.inputView = countryCodePicker
                customTextField.icon = "drop-down-arrow"
                customTextField.showImage()
                customTextField.keyboardType = .phonePad
                customTextField.tintColor = UIColor.clear
            }
            break
        default:
            break
        }
    }
    
    func configureProfileTextField(_ customTextField: EdgemCustomTextField) {
        customTextField.inputView = nil
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        switch customTextField.tag {
        case StudentProfileTextFieldType.firstName.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.backgroundColor = UIColor.white
            customTextField.textFont = textFieldDefaultFont
            break
        case StudentProfileTextFieldType.lastName.rawValue:
             customTextField.isUserInteractionEnabled = false
             customTextField.backgroundColor = UIColor.white
            customTextField.textFont = textFieldDefaultFont
            break
        case StudentProfileTextFieldType.gender.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.backgroundColor = UIColor.white
            customTextField.textFont = textFieldDefaultFont
            break
        case StudentProfileTextFieldType.birthday.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.backgroundColor = UIColor.white
            customTextField.textFont = textFieldDefaultFont
            break
        case StudentProfileTextFieldType.emailAddress.rawValue:
            customTextField.delegate = self
            customTextField.isUserInteractionEnabled = true
             customTextField.backgroundColor = UIColor.white
            customTextField.keyboardType = .emailAddress
            break
        case StudentProfileTextFieldType.contactNumber.rawValue:
            customTextField.delegate = self
            customTextField.isUserInteractionEnabled = true
             customTextField.backgroundColor = UIColor.white
            if customTextField.title?.contains("Contact") ?? false{
                customTextField.keyboardType = .phonePad
            } else{
                customTextField.inputView = countryCodePicker
                customTextField.icon = "drop-down-arrow"
                customTextField.showImage()
                customTextField.keyboardType = .phonePad
                customTextField.tintColor = UIColor.clear
            }
            break
        default:
            break
        }
    }
    
    
    func configureProfileAddressTextField(_ customTextField: EdgemCustomTextField) {
        customTextField.delegate = self
        customTextField.isUserInteractionEnabled = true
        customTextField.inputView = nil
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        switch customTextField.tag {
        case StudentProfileAddressTextFieldType.postalCode.rawValue:
            customTextField.keyboardType = .numberPad
             customTextField.backgroundColor = UIColor.white
            break
        case StudentProfileAddressTextFieldType.addressLine1.rawValue:
            customTextField.keyboardType = .asciiCapable
             customTextField.backgroundColor = UIColor.white
            customTextField.icon = "edit"
            //customTextField.isUserInteractionEnabled = false
            //customTextField.textFont = textFieldDefaultFont
            break
        case StudentProfileAddressTextFieldType.addressLine2.rawValue:
             customTextField.backgroundColor = UIColor.white
            customTextField.keyboardType = .asciiCapable
            customTextField.icon = "edit"
//            customTextField.isUserInteractionEnabled = false
//            customTextField.textFont = textFieldDefaultFont
            break
        default:
            break
        }
    }
    
    //MARK:- Checking Data Validation
    func isParentProfileDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        for index in 0..<parentTitle.count {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case 0:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateFirstName(text: editedUser.firstName)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 1:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateLastName(text: editedUser.lastName)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 2:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateRelationWithChild(text: editedUser.relationwithchild)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 3:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateEmail(text: editedUser.email)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 4:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateContact(text: editedUser.contactNumber)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            default:
                break
            }
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }
    
    func isChildProfileDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        for index in 0..<childTitles.count {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case 0:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateFirstName(text: editedUser.firstName)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 1:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateLastName(text: editedUser.lastName)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 2:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateGenderName(text: editedUser.gender)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 3:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateBirthday(text: editedUser.birthday)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 4:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateEmail(text: editedUser.email)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
                
            default:
                break
            }
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }
    
    func isAddressDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        for index in 0..<addressTitles.count {
            let address = editedUser.userAddress
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case 0:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validatePostalCode(text: address?[0].postalCode ?? "")
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 1:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateAddress1(text: address?[0].addressLine1 ?? "")
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
                
            default:
                break
            }
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }
    
}

extension StudentProfileViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! EdgemCustomTextField
        
        if  activeTextField.text?.contains("+") == true{
            managePickerView()
        }
        
        if currentIndex == 1{
            print("parent field")
             AppVariables.update_Parent_EmailAndContact_Or_Child = "Parent"
            switch textField.tag{
                case StudentParentProfileTextFieldType.parentEmail.rawValue:
                    
                    self.view.endEditing(true)
                    let verifyEmail = VerifyEmailAndContactViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                    verifyEmail.VC_TITLE = "Verify your email".localized
                    verifyEmail.email_or_contact = 0
                    self.navigationController?.pushViewController(verifyEmail, animated: true)
                
                case StudentParentProfileTextFieldType.parentContactNumber.rawValue:
                    
                    let verifyEmail = VerifyEmailAndContactViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                    verifyEmail.VC_TITLE = "Verify your Contact".localized
                    verifyEmail.email_or_contact = 1
                    self.navigationController?.pushViewController(verifyEmail, animated: true)
               
            default:
                break
            }

        }else if currentIndex == 2{
            print("child field")
            AppVariables.update_Parent_EmailAndContact_Or_Child = "Student"
            switch textField.tag{
                
            case StudentProfileTextFieldType.emailAddress.rawValue:
                
                 self.view.endEditing(true)
                 let verifyEmail = VerifyEmailAndContactViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                 verifyEmail.VC_TITLE = "Verify your email".localized
                 verifyEmail.email_or_contact = 0
                 self.navigationController?.pushViewController(verifyEmail, animated: true)
                break
        
            case StudentProfileTextFieldType.contactNumber.rawValue:

                let verifyEmail = VerifyEmailAndContactViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                verifyEmail.VC_TITLE = "Verify your Contact".localized
                verifyEmail.email_or_contact = 1
                self.navigationController?.pushViewController(verifyEmail, animated: true)

                break
                
            default :
                break
                
            }
        }else{
            print("address")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

        if currentIndex == 1{
                updateDataIntoParentProfileTextField(textField as! EdgemCustomTextField, fromCell: false)
            }else if currentIndex == 2{
                updateDataIntoChildProfileTextField(textField as! EdgemCustomTextField, fromCell: false)
            }else{
            
             if textField.text?.isValidPostalCode() ?? false{
                
                updateDataIntoProfileAddressTextField(textField as! EdgemCustomTextField, fromCell: false)
                //self.editedUser.userAddress?[0].postalCode = textField.text
                
             }else if  (textField.text?.isEmpty)!{
                self.refreshBtnTapped()
                return
             }else{
                return
            }
//                updateDataIntoProfileAddressTextField(textField as! EdgemCustomTextField, fromCell: false)
                 let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
                print(currentIndexPath)
            
                //-----
            switch textField.tag {
                
            case StudentProfileAddressTextFieldType.postalCode.rawValue:
                
                let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
                cell.validatePostalCode(text: textField.text ?? "")
                
                if textField.text?.isValidPostalCode() ?? false{
                    
                    getLatLngForZip(zipCode: textField.text ?? "")
                    
                }else{
                    
                    let address = editedUser.userAddress
                    address?[0].postalCode = ""
                    address?[0].addressLine1 = ""
                    address?[0].addressLine2 = ""

                }
                
            case StudentProfileAddressTextFieldType.addressLine1.rawValue:
                
                let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
                cell.validateAddress1(text: textField.text ?? "")
                
            default:
                break
            }
            
                //-----
            
            }
       
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
}

// MARK: UIPickerView Delegate/Datasource

extension StudentProfileViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    
    func initialSetUpForCountryCodePicker(){
        countryCodePicker.delegate = self
        countryCodePicker.dataSource = self
        countryCodePicker.tag = 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        let textField = activeTextField as! EdgemCustomTextField
        countryCodePicker.reloadAllComponents()
        countryCodePicker.reloadInputViews()
        if textField.tag == 1 {
            textField.inputView = countryCodePicker
            let index = countryCodeOptions.index(of: editedUser.countryContactCode)
            if let _index = index {
                self.countryCodePicker.selectRow(_index, inComponent: 0, animated: true)
                activeTextField.text = countryCodeOptions[_index]
            } else {
                countryCodePicker.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = countryCodeOptions[0]
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryCodeOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryCodeOptions[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        activeTextField.text = countryCodeOptions[row]
    }
    
    
}


// MARK:-------  API Implementation
extension StudentProfileViewController{
    
    func updateUserInfoData(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        guard let userID = UserStore.shared.userID else {return}
        var params = [String:String]()
        
        //========
        
        var dict = [String:String]()
        dict = [
            
            "contact_number_parent"  : editedUser.parentContactNumber,
            "parent_email"     : editedUser.parentEmail,
            "country_code_parent" : "65",
            
            "email" :  editedUser.email,
            "country_code" : "65",
            "contact_number": editedUser.contactNumber,
            
            "user_id" : "\(userID)"
        ]
        
        if let userAddress = editedUser.userAddress,  userAddress.count > 0{

            dict["addres_line1"] = userAddress[0].addressLine1
            dict["addres_line2"] = userAddress[0].addressLine2
            dict["lattitude"]        = userAddress[0].lattitude
            dict["longitude"]      = userAddress[0].longitude
            dict["postcode"]      = userAddress[0].postalCode
            
        }
        
        let updateUserInfoObserver = ApiManager.shared.apiService.updateUserInfo(dict as [String : AnyObject])
        
        let updateUserInfoDisposable = updateUserInfoObserver.subscribe(onNext: {(userData) in
            
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
            
                //self.showSimpleAlertWithMessage(userData)
                self.editedUser = userData
                
                AppDelegateConstant.user = userData
                
                self.tableView.reloadData()
                
                //self.showSimpleAlertWithMessage("Your personal information has been successfully updated.".localized)
                self.showSimpleAlertWithMessgeAndNavigateBack("Your personal information has been successfully updated.".localized)
                
            }
            
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        updateUserInfoDisposable.disposed(by: disposableBag)
        
    }
}
