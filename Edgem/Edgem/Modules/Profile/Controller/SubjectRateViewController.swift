//
//  SubjectRateViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 16/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift
import AMPopTip

class SubjectRateViewController: BaseViewController {
    
    // MARK: ------- Properties
    var userDetail: User!
    var subjectListing = [Subject]()
    var searchSubjects = [Subject]()
    var isSearching = false
    var rowCount = 0
    var levelCount = 0
    var disposableBag = DisposeBag()
    var pagination = Pagination()
    var editedIndex = IndexPath()
    let popTip   = PopTip()
    let setUpToolTip = SetUpToolTip()
    
    
    
    // MARK: ------- @IBOutlets
    @IBOutlet weak var searchHolderView: UIView!
    @IBOutlet weak var disableView: UIImageView!{
        didSet{
            disableSaveButton(isDisable: true)
        }
    }
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var searchSubjectTextfield: UITextField!
    @IBOutlet weak var searchSubjectcontaineView: UIView!{
        didSet{
            searchSubjectcontaineView.layer.borderWidth = 1
            searchSubjectcontaineView.layer.borderColor  = theamBorderGrayColor.cgColor
            
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noRecordLbl: UILabel!{
        didSet{
            noRecordLbl.isHidden = true
        }
    }
    
    // MARK: ------- ViewLife Cycle
    
    fileprivate func isIDExist(_ id: String) ->(Bool,Subject?)?{
    
        if let users_subjects = self.userDetail.subjects{
            
            for subjects in users_subjects{
                
              //  return (subjects.id == id) ? (true,subjects) : (false,subjects)
                if subjects.id == id {
                    return (true,subjects)
                }
                
            }
            
        }
        return (false,nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor  = theamBorderGrayColor.cgColor
        
        pagination = Pagination.init()
        subjectListing = []
        registerCell()
        if let _user = AppDelegateConstant.user{
            userDetail = _user
        }
        
        // Fetch Levels
        if let levels = AppDelegateConstant.levelPagination?.levels, levels.count <= 0{
            fetchLevels()
        }
        
        if let levels = AppDelegateConstant.levelPagination?.levels{
            self.levelCount = levels.count
        }else{
            print(value: "Level not found")
        }
        
        if let subjects = AppDelegateConstant.subjectListing{
            
            updateUI(subjects)
            
        }else{
             fetchSubjectsListing()
        }
        
       
        setUpToolTip.initialSetUp(popTip)
        
    }
    
    func registerCell(){
        
        tableView.register(UINib(nibName: "CommonCheckBoxCell", bundle: nil), forCellReuseIdentifier: CommonCheckBoxCell.cellIdentifire())
    }
    
    func disableSaveButton(isDisable: Bool){
        disableView.isHidden = !isDisable
    }
    
    fileprivate func updateUI(_ subjects: [Subject]) {
        var tempSubjectListing = [Subject]()
        
        subjects.forEach { (subject) in
            
            let status = self.isIDExist(subject.id)
            
            if let _status = status{
                
                if _status.0 == true{
                    _status.1!.isSelected = true
                    tempSubjectListing.append(_status.1!)
                    
                }else{
                    subject.levels.removeAll()
                    AppDelegateConstant.levelPagination?.levels.forEach({ (level) in
                        let tempLevel = Level(level.id, level.name)
                        tempLevel.price = level.price
                        tempLevel.isSelected = false
                        subject.levels.append(tempLevel)
                    })
                    subject.isSelected = false
                    subject.subjectRate = ""
                    subject.lockRate = "0"
                    tempSubjectListing.append(subject)
                    
                }
            }
        }
        
        self.subjectListing = tempSubjectListing
    }
    
    fileprivate func isDataValid(_ selectedSub: [Subject])->Bool {
        if selectedSub.count == 0{
            self.showSimpleAlertWithMessage("Please select at least one subject.".localized)
            return false
        } else if selectedSub.count > 0{
            
            for sub in selectedSub {
                
                var isAllLevelPriceBlank = 0
                
                sub.levels.forEach({ (level) in
                    
                    if let price = level.price{
                        
                        if (price as NSString).doubleValue <= 0.0{
                            print("ammount should be greater than zero")
                        }else{
                            isAllLevelPriceBlank += 1
                        }
                        
                    }
                    
                })
                if isAllLevelPriceBlank == 0 {
                    self.showSimpleAlertWithMessage("Please enter at least one level price.".localized)
                    return false
                }
            }
        }
        return true
    }
    
//    func isDataValid() -> (isValid: Bool, type: Int){
//        self.view.endEditing(true)
//        var subject_price_status: (isValid: Bool, type: Int)?
//
//        let selectedArr = isSearching ? self.searchSubjects.filter {$0.isSelected} : self.subjectListing.filter {$0.isSelected}
//
//
//        for subject in selectedArr{
//            let rates = ((subject.subjectRate!.replacingOccurrences(of: "$", with: "")) as NSString).doubleValue
//            print(value: rates)
//            if  rates <= 0 {
//                 subject_price_status = (isValid: false, type: 2)
//            }
//        }
//
//        let errorOne = selectedArr.filter {($0.name == "Languages" || $0.name == "Others" || $0.name == "Music")}.filter { $0.enteredName.isEmpty }
//        let errorTwo = selectedArr.filter { ($0.subjectRate?.isEmpty)! }
//        let status = !((errorOne.count>0) || (errorTwo.count>0))
//
//        if status == false {
//             return(isValid: status, type: 1)
//        }else if let status = subject_price_status, status.isValid == false{
//              return(isValid: false, type: 2)
//        }else{
//              return(isValid: true, type: 0)
//        }
//
//
//
//    }
    
    //  MARK: ------- @IBActions
    @IBAction func searchSubjects(_ sender: UITextField) {
        //print(sender.text!)
        
        if sender.text?.count != 0{
            isSearching = true
            searchSubjects = self.subjectListing.filter{ $0.name.lowercased().contains(sender.text!.lowercased()) }
        }else{
            isSearching = false
            searchSubjects.removeAll()
        }
        tableView.reloadData()
        
        //=====
//        self.searchSubjects.removeAll()
//        isSearching = true
//        if sender.text?.count != 0{
//            for subject in subjectListing{
//                if let subjectToSearch = sender.text{
//                    let range = subject.name.lowercased().range(of: subjectToSearch, options: .caseInsensitive, range: nil, locale: nil)
//                    if range != nil{
//                        self.searchSubjects.append(subject)
//                    }
//                }
//            }
//        }else{
//            isSearching = false
//            searchSubjects.removeAll()
//            //self.searchSubjects = self.subjectListing
//        }
//        //numberOfRows()
//        tableView.reloadData()
        //=====
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        
      //  if isDataValid().isValid{
            
        let selectedSub = isSearching ? self.searchSubjects.filter{ $0.isSelected } : self.subjectListing.filter{ $0.isSelected }
            
        if  isDataValid(selectedSub) == false{
            return
        }

            var param = [String:Any]()
            var parameters = [[String: AnyObject]]()
            for item in selectedSub{
                var parameterSub = [String: Any]()
                
                parameterSub["id"] = item.id
                parameterSub["lock_rate"] = item.lockRate ?? "0"
                parameterSub["other_subject"] = item.enteredName
                
                var levelParams = [[String: AnyObject]]()
                item.levels.forEach { (level) in
                    var levelDict = [String: String]()
                    levelDict["id"] = level.id
                    levelDict["price"] = level.price! == "0.0" ? "" : level.price!
                    levelParams.append(levelDict as [String : AnyObject])
                }
                parameterSub["level"] = levelParams
                parameters.append(parameterSub as [String : AnyObject])
            }
        
        param["subject"] = parameters
        
        //print(param as [String : AnyObject])
        //PrintJSONOnConsole.printJSON(dict: param as [String : AnyObject])
       SaveTutorSubjectsRate(params: param as [String : AnyObject])

    }
    
}

extension SubjectRateViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return isSearching ? searchSubjects.count : subjectListing.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let subject = isSearching ? searchSubjects[section] : subjectListing[section]
        
        if subject.isSelected{
            if subject.isOption == "1" || subject.name == "Music" || subject.id == "17" ||  subject.id == "19" || subject.id == "20"{
                rowCount = 3 + levelCount
            }else{
                 rowCount = 2 + levelCount
            }
        }else{
            rowCount = 1
        }
        return rowCount
       // return subject.isSelected ? 8 : 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? CheckBoxCellInSubjectAndRate.cellHeight() : UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let subject = isSearching ? searchSubjects[indexPath.section] : subjectListing[indexPath.section]
        
        guard indexPath.row != 0 else{
            let cell = tableView.dequeueReusableCell(withIdentifier: CheckBoxCellInSubjectAndRate.cellIdentifire()) as! CheckBoxCellInSubjectAndRate
            cell.configureCellWithSubject(textString: subject.name, isSelected: subject.isSelected, index: indexPath.row)
            return cell
        }
        
        if subject.id == "17" || subject.id == "19" || subject.id == "20"{
        
            guard indexPath.row != 1 else{
                  let cell = tableView.dequeueReusableCell(withIdentifier: OtherSubjectTextFieldInSubjectAndRate.cellIdentifire()) as! OtherSubjectTextFieldInSubjectAndRate
                  cell.configureCell(with: subject)
                  cell.otherSubjectTextField.delegate = self
                  updateData(cell.otherSubjectTextField, section: indexPath.section, fromCell: true)
                  return cell
            }
            
            guard indexPath.row != 2 else{
                let cell = tableView.dequeueReusableCell(withIdentifier: SwitchButtonInSubjectAndRate.cellIdentifire()) as! SwitchButtonInSubjectAndRate
                cell.delegate = self
                cell.configureCell(with: subject.lockRate, indexPath.section)
                cell.switchBtnStatus = {[weak self] status, index in
                    self?.switchBtnStatus(status: status, index: index)
                }
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: LavelWithRateInSubjectAndRate.cellIdentifire()) as! LavelWithRateInSubjectAndRate
            if let level = subject.levels[safe: indexPath.row-3]{
                cell.configureCell(with: level.name, index: indexPath.row-3)
                cell.levelRateTextField.delegate = self
                cell.levelRateTextField.keyboardType = .numberPad
                updateData(cell.levelRateTextField, section: indexPath.section, fromCell: true)
            }
            
            return cell
            
        }else{
            
            guard indexPath.row != 1 else{
                let cell = tableView.dequeueReusableCell(withIdentifier: SwitchButtonInSubjectAndRate.cellIdentifire()) as! SwitchButtonInSubjectAndRate
                cell.delegate = self
                cell.configureCell(with: subject.lockRate, indexPath.section)
                cell.switchBtnStatus = {[weak self] status, index in
                    self?.switchBtnStatus(status: status, index: index)
                }
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: LavelWithRateInSubjectAndRate.cellIdentifire()) as! LavelWithRateInSubjectAndRate
            if let level = subject.levels[safe: indexPath.row-2]{
                cell.configureCell(with: level.name, index: indexPath.row-2)
                cell.levelRateTextField.delegate = self
                cell.levelRateTextField.keyboardType = .numberPad
                updateData(cell.levelRateTextField, section: indexPath.section, fromCell: true)
            }
            
            return cell
            
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard indexPath.row != 0 else{
            
            let subject = isSearching ? searchSubjects[indexPath.section] : subjectListing[indexPath.section]
            
            subject.isSelected = !subject.isSelected
            
            //disableSaveButton(isDisable: false)
            
            tableView.reloadSections([indexPath.section], with: .automatic)
            
            return
        }
        
    }
    
    func switchBtnStatus(status: Bool, index: Int) {
        print(status, index)
        let subject = isSearching ? searchSubjects[index] : subjectListing[index]
        subject.lockRate = status == true ? "1" : "0"
    }
    
}

extension SubjectRateViewController: UITextFieldDelegate{
    
    func updateData(_ customTextField: UITextField, section: Int, fromCell: Bool){
        
         let subject = isSearching ? searchSubjects[section] : subjectListing[section]
        
        if customTextField.tag == 100 || customTextField.tag == 101 || customTextField.tag == 102{
            
                fromCell ? (customTextField.text = subject.enteredName) : (subject.enteredName = customTextField.text!)
            
        }else{
            
            for (index,level) in subject.levels.enumerated(){
                if customTextField.tag == index{
                    
                    if fromCell{
                        
                        if let _price = level.price{
                            
                            let price_in_double = (_price as NSString).doubleValue
                            
                            if price_in_double > 0 {
                                customTextField.text = "$"+price_in_double.isWholeNumber()
                            }else{
                                customTextField.text  = ""
                                //self.showSimpleAlertWithMessage("Amount should be greater than zero.".localized)
                            }
                        }
                        
                    }else{
                        
                        subject.levels[index].price = customTextField.text?.replacingOccurrences(of: "$", with: "")
                        
                    }
                }
            }
            
        }
        

    }
    
    func updateDataIntoTextField(_ customTextField: EdgemCustomTextField, section: Int, fromCell: Bool){
        let subject = isSearching ? searchSubjects[section] : subjectListing[section]
        
        switch customTextField.tag {
        case 1:
            fromCell ? (customTextField.text = subject.enteredName) : (subject.enteredName = customTextField.text!)
        case 2:
            fromCell ? (customTextField.text = subject.subjectRate!) : (subject.subjectRate = customTextField.text!)
        default:
            break
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let indexPoint = textField.convert(textField.bounds.origin, to: self.tableView)
        editedIndex = self.tableView.indexPathForRow(at: indexPoint) ?? IndexPath()
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        if textField.tag != 100 && textField.tag != 101 && textField.tag != 102{
            textField.text = textField.text == "" ? "$" + textField.text! : textField.text!
            return (textField.text?.count)! < UserNameMaxLimit
        }
        return true

//        switch textField.tag {
//        case 1:
//            return (textField.text?.count)! < NameMaxLimit
//        case 2:
//            textField.text = textField.text == "" ? "$ " + textField.text! : textField.text!
//            return (textField.text?.count)! < UserNameMaxLimit
//        default:
//            return true
//        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        updateData(textField, section: editedIndex.section, fromCell: false)
        
//        updateDataIntoTextField(textField as! EdgemCustomTextField, section:(editedIndex.section), fromCell: false)
//
//        let currentIndexPath = IndexPath(row: textField.tag, section: (editedIndex.section))
//
//        if let cell = tableView.cellForRow(at: currentIndexPath) as? DoubleTextFieldTableViewCell{
//            let subject = isSearching ? searchSubjects[editedIndex.section] : subjectListing[editedIndex.section]
//
//            switch textField.tag {
//            case 1:
//                subject.errorOne = cell.validateField(text: subject.enteredName, type: subject.name)
//            case 2:
//                subject.errorTwo = cell.validateField(text: subject.subjectRate!, type: subject.name)
//            default:
//                break
//            }
//
//        }
        
         tableView.reloadSections([editedIndex.section], with: .automatic)
//        tableView.beginUpdates()
//        tableView.endUpdates()
    }
}



//MARK: ----------------- API Implementation

extension SubjectRateViewController{
    
    func fetchSubjectsListing(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let subjectObserver = ApiManager.shared.apiService.fetchSubjects(pagination.currentPage+1)
        let subjectDisposable = subjectObserver.subscribe(onNext: { (subjectPagination) in
            
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                
                self.pagination = Pagination.init()
                self.subjectListing += subjectPagination.subjects
                
                AppDelegateConstant.subjectListing = subjectPagination.subjects
                
                self.updateUI(subjectPagination.subjects)
                
//                var subjects = subjectPagination.subjects
//
//                var music_Index: Int?
//                var art_index: Int?
//
//                for (index, subject) in subjects.enumerated() {
//                    if subject.id == "17"{
//                        music_Index = index
//                    }else if subject.id == "18"{
//                        art_index = index
//                    }
//                }
//
//                if let m_index = music_Index, let a_index = art_index{
//                    subjects.swapAt(m_index, a_index)
//                }
//
//               self.subjectListing = subjects
                
                for tempItem in self.userDetail.subjects!{
                    for item in self.subjectListing{
                        
                        if (item.id == tempItem.id){
                            item.isSelected = (item.id == tempItem.id)
                            item.subjectRate = (item.id == tempItem.id) ? tempItem.subjectRate : item.subjectRate
                            item.lockRate = (item.id == tempItem.id) ? tempItem.lockRate : item.lockRate
                        }
                        
                    }
                }
                
                self.noRecordLbl.isHidden = self.subjectListing.count > 0
                self.tableView.reloadData()
                
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
            }
        })
        subjectDisposable.disposed(by: disposableBag)
    }
    
    func SaveTutorSubjectsRate(params: [String: AnyObject]){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let postObserver = ApiManager.shared.apiService.postTutorSubjectsRate(params)
        let postDisposal = postObserver.subscribe(onNext: { (message) in
            
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                self.showSimpleAlertWithMessage(message)
                //self.disableSaveButton(isDisable: true)
            }
            
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
            }
        })
        postDisposal.disposed(by: disposableBag)
    }
    
    func fetchLevels(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        //Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
            let levelObserver = ApiManager.shared.apiService.fetchLevels(1)
            let levelDisposable = levelObserver.subscribe(onNext: { (paginationObject) in
                AppDelegateConstant.levelPagination = LevelPagination()
                AppDelegateConstant.levelPagination?.appendDataFromObject(paginationObject)
            }, onError: { (error) in
                DispatchQueue.main.async {
                    //Utilities.hideHUD(forView: self.view)
                }
            })
            return levelDisposable.disposed(by: self.disposableBag)
        }
    }
}

extension SubjectRateViewController:SwitchButtonInSubjectAndRateDelegate{
  
    func popTipTapped(senderFrame: CGRect, cell: SwitchButtonInSubjectAndRate,btn:UIButton) {
        
        self.showPopToolTipForButton(btn, popMessage: ToolTipString.subjectInfo.description())

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.hidePopToolTipForButton()
    }
    
}
