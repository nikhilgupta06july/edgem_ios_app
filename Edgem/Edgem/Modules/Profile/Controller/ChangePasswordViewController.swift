//
//  ChangePasswordViewController.swift
//  Edgem
//
//  Created by Hipster on 22/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class ChangePasswordViewController: BaseViewController {
    
    // MARK: ------- Properties
    
     let labelFields = ["Current password".localized,"New password".localized, "Confirm new password".localized]
     var userRegistration = UserRegistration()
     var isSecureEntry: Bool = true
     var disposableBag = DisposeBag()
    
     // MARK: ------- @IBOutlets
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var saveButton: UIButton!
    

      // MARK: ------- Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
        // MARK: ------- @IBActions
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        navigateBack(sender: sender)
        
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        
        print("save button clicked")
        changePassword()
        
    }
    
    
     // MARK: ------- Private Functions
    
    func configureTextField(_ customTextField: EdgemCustomTextField, _ isSecureEntry: Bool) {
        
        customTextField.delegate = self
        customTextField.inputView = nil
        customTextField.keyboardType = .asciiCapable
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        customTextField.isSecureTextEntry = isSecureEntry
        
    }
    
    
    // DATA VALIDATION FUNCTIONALITY
    func isDataValid() -> (isValid: Bool, message: String){
        
        var errorMessages: [String] = []
        
        if userRegistration.password.isEmpty, userRegistration.newPassword.isEmpty, userRegistration.confirmPassword.isEmpty {
            
            return (isValid: false, message: "")
            
        } else {
            
            for index in 1..<labelFields.count {
                
                let currentIndexPath = IndexPath.init(row: index, section: 0)
                
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    
                    switch index {
                        
                    case ChangePasswordTextFieldType.currentPassword.rawValue             :  cell.validateProfilePassword(text: userRegistration.password)
                        
                    case  ChangePasswordTextFieldType.newPassword.rawValue                 :  cell.validateNewPassword(text: userRegistration.newPassword)
                        
                    case ChangePasswordTextFieldType.confirmPassword.rawValue            :  cell.validateConfirmPassword(text: userRegistration.confirmPassword, password: userRegistration.newPassword)
                       
                    default :  break
                       
                        
                    }
                    let message = cell.errorLabel.text ?? ""
                    if !message.isEmptyString() {
                        errorMessages.append(message)
                    }
                }
                
            }
            
            tableView.beginUpdates()
            tableView.endUpdates()
            
            let isValidData = errorMessages.count == 0
            
            return (isValid: isValidData, message: isValidData ? "" : errorMessages[0])
        }
    }
    
    
    // UPDATE TEXTFIELD
    fileprivate func updateDataIntoTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool){
        
        switch customTextField.tag {
            
            case ChangePasswordTextFieldType.currentPassword.rawValue    :     fromCell ? ( customTextField.text = userRegistration.password ) : ( userRegistration.password = customTextField.text! )
            
            
            case ChangePasswordTextFieldType.newPassword.rawValue         :    fromCell ? ( customTextField.text = userRegistration.newPassword ) : ( userRegistration.newPassword = customTextField.text! )
            
            
            case ChangePasswordTextFieldType.confirmPassword.rawValue   :     fromCell ? ( customTextField.text = userRegistration.confirmPassword ) : ( userRegistration.confirmPassword = customTextField.text! )
            
            default:
            break
            
        }
        
        if userRegistration.password == "" || userRegistration.newPassword == "" || userRegistration.confirmPassword == ""{
            
            showHideSaveButton(false)
            if fromCell == false{
                tableView.beginUpdates()
                tableView.endUpdates()
            }
            
        }
    }
    
    // APPEARENCE FUNCTIONALITY OF SAVE BUTTON
    func showHideSaveButton(_ status: Bool) {
        if status == false{
            saveButton.isUserInteractionEnabled = false
            saveButton.backgroundColor = UIColor(red: 254/255, green: 241/255, blue: 187/255, alpha: 1)
        }else{
            saveButton.isUserInteractionEnabled = true
            saveButton.backgroundColor = theamAppColor
        }
    }
    
}

// MARK: ------- ShowHideTableViewCell Delegates

extension ChangePasswordViewController: ShowHideTableViewCellDelegate{
    
    func showPasswordBtnTapped(_ isSelected: Bool){
        
        if isSelected == false{
            isSecureEntry = true
        }else{
            isSecureEntry = false
        }
        tableView.reloadData()
        
    }
    
}

// MARK: ------- TableView Delegate/DataSource

extension ChangePasswordViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         guard indexPath.row == 3 else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: EdgemTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemTextFieldTableViewCell
            
            cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
            configureTextField(cell.customTextField, isSecureEntry)
            updateDataIntoTextField(cell.customTextField, fromCell: true)
            
            cell.selectionStyle = .none
            return cell
            
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ShowHideTableViewCell.cellIdentifire(), for: indexPath) as! ShowHideTableViewCell
        cell.showPasswordTableViewCellDelegate = self
        return cell

}
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }

    
}

//MARK: ------- TextField Delegate

extension ChangePasswordViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.textColor = theamBlackColor
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        
        updateDataIntoTextField(textField as! EdgemCustomTextField, fromCell: false)
        
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
        
        switch textField.tag {
            
            case ChangePasswordTextFieldType.currentPassword.rawValue   :   cell.validateProfilePassword(text: textField.text ?? "")
            
            case ChangePasswordTextFieldType.newPassword.rawValue        :   cell.validateNewPassword(text: textField.text ?? "")
            
            case ChangePasswordTextFieldType.confirmPassword.rawValue   :   cell.validateConfirmPassword(text: textField.text ?? "", password: userRegistration.newPassword)
            
            default : break
            
        }

        
        if  userRegistration.password != "" && userRegistration.newPassword != "" && userRegistration.confirmPassword != "" {
           showHideSaveButton(isDataValid().isValid)
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            return false
        }
        return true
    }
    
}

//MARK: ------- API Implementation

extension ChangePasswordViewController{
    
    // SAVE PASSWORD
    func changePassword(){
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        guard let userID = UserStore.shared.userID else{return}
        let newPassword = userRegistration.password
        let confirmPassword = userRegistration.confirmPassword
        let params = ["user_id" : userID, "current_password" : newPassword, "password":confirmPassword] as [String:AnyObject]
        
        let changePasswordObserver = ApiManager.shared.apiService.changePassword(params)
        let changePasswordDisposable = changePasswordObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
//                let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
//                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
//                alert.addAction(okAction)
//                self.present(alert, animated: true, completion: nil)
                
                // Saving password in keychain
             
                do {
                    // This is a new account, create a new keychain item with the account name.
                    let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.serviceName,
                                                            account: UserStore.shared.userEmail ?? "",
                                                            accessGroup: KeychainConfiguration.accessGroup)
                    
                    // Save the password for the new item.
                    try passwordItem.savePassword(self.userRegistration.confirmPassword)
                    UserStore.shared.hasLoginKey = true
                } catch {
                    UserStore.shared.hasLoginKey = false
                    fatalError("Error updating keychain - \(error)")
                }
                
                let changePwdConfirmVC = ChangePasswordConfirmViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
                self.navigationController?.pushViewController(changePwdConfirmVC, animated: true)
                
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        changePasswordDisposable.disposed(by: disposableBag)
    }
    
}


