//
//  QRViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 21/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift
//import BitlySDK

class QRViewController: BaseViewController {
    
        @IBOutlet weak var imgQRCode: UIImageView!
    
        
        var qrcodeImage: CIImage!
        var qrImage: UIImage!
        var myAppShareURL: URL?
        var bitlyStr = ""
    
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view, typically from a nib.
            createQRCode()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
    @IBAction func optionBtnTapped(_ sender: UIButton) {
        
        let commonPopUP = ShowInfoCommonPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        commonPopUP.delegate = self
        commonPopUP.fromController = self
        self.present(commonPopUP, animated: true, completion: nil)
      
    }
   
    // MARK: IBAction method implementation
        
    func createQRCode(){
        
            if qrcodeImage == nil {
                self.myAppShareURL = URL(string: bitlyStr)
                
                let data = bitlyStr.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
                
                let filter = CIFilter(name: "CIQRCodeGenerator")
                
                filter?.setValue(data, forKey: "inputMessage")
                filter?.setValue("Q", forKey: "inputCorrectionLevel")
                
                self.qrcodeImage = filter?.outputImage
                self.displayQRCodeImage()
                
            }else {
                imgQRCode.image = nil
                qrcodeImage = nil
            }
    }
        
        
//        @IBAction func changeImageViewScale(sender: AnyObject) {
//            imgQRCode.transform = CGAffineTransform(scaleX: CGFloat(slider.value), y: CGFloat(slider.value))
//        }
    
        
        // MARK: Custom method implementation
        
        func displayQRCodeImage() {
            let scaleX = imgQRCode.frame.size.width / qrcodeImage.extent.size.width
            let scaleY = imgQRCode.frame.size.height / qrcodeImage.extent.size.height
            
            let transformedImage = qrcodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
            
            imgQRCode.image = UIImage(ciImage: transformedImage)
            captureScreenshot()
        }

    

}

extension QRViewController: ShowInfoCommonPopUpDelegate{
    
    func tableViewCellSelected(_ indexPath: IndexPath) {
        guard let selectedImage = qrImage else {
            print("Image not found!")
            return
        }
        if indexPath.row == 0{
            
            
            var shareText = ""
            var firstName = ""
            var lastName = ""
            if let fName = AppDelegateConstant.user?.firstName{
                firstName = fName
            }
            if let lName = AppDelegateConstant.user?.lastName{
                lastName = lName
            }
            if AppDelegateConstant.user?.userType == "Tutor"{
                if let appURL = myAppShareURL{
                    shareText = "Hi, I am \(firstName) \(lastName) and I am an active tutor on Edgem. You may find more information about me via this link \n \(appURL)"
                }
            }else{
                if let appURL = myAppShareURL{
                     shareText = "Hi, I am \(firstName) \(lastName) and I am an active student on Edgem. You may find more information about me via this link \n \(appURL)"
                }
            }
            
//            let vc = UIActivityViewController(activityItems: [shareText, selectedImage], applicationActivities: [])
            let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        
            present(vc, animated: true, completion: nil)
            
        }
//        else if indexPath.row == 1{
//
//            UIImageWriteToSavedPhotosAlbum(selectedImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
//        }
    }

    //Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            print(error.localizedDescription)
        } else {
//            QR code saved successfully in Edgem folder
            showSimpleAlertWithMessage("Your QR image has been saved to your photos.")
        }
    }
    
    func captureScreenshot(){
        let layer = imgQRCode.layer//UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        // Creates UIImage of same size as view
        UIGraphicsBeginImageContextWithOptions(imgQRCode.frame.size, false, scale);
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        qrImage = screenshot!
    }
    
}
