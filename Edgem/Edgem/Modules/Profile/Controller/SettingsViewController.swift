//
//  SettingsViewController.swift
//  Edgem
//
//  Created by Namespace on 24/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import UserNotifications
import BiometricAuthentication
import RxSwift

class SettingsViewController: BaseViewController {
    
    var lebels = ["Notifications", "Face ID","Touch ID"]
    var ignoreTheSwitchAction = false
    var disposableBag = DisposeBag()
    var biometricTitleForTouch = ""
    var biometricTitleForFace = ""
    var showTouchID = false
    var rowCount = 1
    
    @IBOutlet weak var tableView: UITableView!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        configureTouchID()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setNotificationsOn), name: .deviceTokenReceived,
                                               object: nil)
    }
    
    @objc func setNotificationsOn() {
      
            DispatchQueue.main.async {
                let cell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? SettingOptionsTableViewCell
                cell?.switch.setOn(true, animated: true)
            }
    }
    
    func reverseTheToggle(_ toggleSwitch: UISwitch) {
        let isOn = toggleSwitch.isOn
        ignoreTheSwitchAction  = true
        toggleSwitch.setOn(!isOn, animated: false)
    }
    
    func askForNotificationPermissions() {
        
        UserStore.shared.notificationPermissionsAsked = true

        if #available(iOS 10.0, *) {
            
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings { (settings) in
                
                switch settings.authorizationStatus {
                    
                case .notDetermined :
                    
                    AppDelegateConstant.registerForPushNotifications()
                    UserStore.shared.isNotificationUnAuthorized = false
                    // Authorization request has not been made yet
                    
                case .denied:
                    UserStore.shared.isNotificationUnAuthorized = true
                    self.showNotificationsAlert()
                    // User has denied authorization.
                    // You could tell them to change this in Settings
                    
                case .authorized:
                    UserStore.shared.isNotificationUnAuthorized = false
                    // User has given authorization.
                    
                default:
                    break
                }
                
            }
            
        } else {
            // Fallback on earlier versions
            if UIApplication.shared.isRegisteredForRemoteNotifications {
                UserStore.shared.isNotificationUnAuthorized = false
                AppDelegateConstant.registerForPushNotifications()
            } else {
                UserStore.shared.isNotificationUnAuthorized = true
            }
        }
    }
    
    func showNotificationsAlert() {
        let alert = UIAlertController(title: AlertMessage.notificationPermissionTitle.description(), message: AlertMessage.notificationPermissionMessage.description(), preferredStyle: .alert)
        let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default) { (action) in
           // self.navigateToSettingsPage()
        }
        let laterAction = UIAlertAction(title: AlertButton.later.description(), style: .cancel, handler: nil)
        alert.addAction(okAction)
        alert.addAction(laterAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func toggleSwitched(_ sender: UISwitch) {
        guard ignoreTheSwitchAction == false else {
            ignoreTheSwitchAction = false
            return
        }
        if sender.tag == 1 {
            toggleBiometrics()
        } else if sender.tag == 2 {
            toggleBiometrics()
        }else {
            changeNotificationSettings(sender)
        }
    }
    
    func toggleBiometrics() {
        BioMetricAuthenticator.authenticateWithBioMetrics(reason: "", success: {
            guard let username = UserStore.shared.userEmail, !username.isEmpty else {
                return
            }
            let isEnabled = UserStore.shared.isBioMetricEnabledByUser
            UserStore.shared.isBioMetricEnabledByUser = !isEnabled
            UserStore.shared.touchIDEmail = (UserStore.shared.isBioMetricEnabledByUser == true) ? UserStore.shared.userEmail : ""
        }, failure: { [weak self] (error) in
            print("Fail")
            
            // do nothing on canceled
            if error == .canceledByUser || error == .canceledBySystem {
                return
            } else if error == .biometryNotAvailable {
                // device does not support biometric (face id or touch id) authentication
                DispatchQueue.main.async(execute: {
                    self?.showSimpleAlertWithMessage(error.message())
                })
            } else if error == .fallback {
                // show alternatives on fallback button clicked
                // here we're entering username and password
                
            } else if error == .biometryNotEnrolled {
                // No biometry enrolled in this device, ask user to register fingerprint or face
                //Settings page
            } else if error == .biometryLockedout {
                // Biometry is locked out now, because there were too many failed attempts.
                // Need to enter device passcode to unlock.
                
                // show passcode authentication
            } else {
                // show error on authentication failed
                DispatchQueue.main.async(execute: {
                    self?.showSimpleAlertWithMessage(error.message())
                })
            }
        })
        
    }
    
    func configureTouchID() {
        
        if BioMetricAuthenticator.canAuthenticate() && UserStore.shared.hasLoginKey {
            self.showTouchID = true
            if BioMetricAuthenticator.shared.faceIDAvailable() {
                self.biometricTitleForFace = "Face ID".localized.uppercased()
                rowCount = rowCount + 1
            }
            if BioMetricAuthenticator.shared.touchIDAvailable() {
                 self.biometricTitleForFace = "Touch ID".localized.uppercased()
                 rowCount = rowCount + 1
            }
        } else {
            self.showTouchID = false
        }
    }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0 :
            
                    let cell = tableView.dequeueReusableCell(withIdentifier: SettingOptionsTableViewCell.cellIdentifire(), for: indexPath) as! SettingOptionsTableViewCell
                    cell.configureCell(indexPath.row, "Notifications".localized)
                    cell.toggleStatus = { [weak self] sender in
                        self!.toggleSwitched(sender)
                    }
                    cell.switch.setOn((!UserStore.shared.isNotificationUnAuthorized && UserStore.shared.isNotificationsStatus), animated: false)
                    return cell
            
        case 1 :
            
                let cell = tableView.dequeueReusableCell(withIdentifier: SettingOptionsTableViewCell.cellIdentifire(), for: indexPath) as! SettingOptionsTableViewCell
                cell.configureCell(indexPath.row, "Touch ID")
                cell.toggleStatus = { [weak self] sender in
                self!.toggleSwitched(sender)
                }
                  cell.switch.setOn(UserStore.shared.isBioMetricEnabledByUser, animated: true)
                return cell
            
        case 2 :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingOptionsTableViewCell.cellIdentifire(), for: indexPath) as! SettingOptionsTableViewCell
            cell.configureCell(indexPath.row, "Face ID")
            cell.toggleStatus = { [weak self] sender in
                self!.toggleSwitched(sender)

            }
            
             cell.switch.setOn(UserStore.shared.isBioMetricEnabledByUser, animated: true)
            return cell
            
        default:
           return UITableViewCell()
            
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SettingOptionsTableViewCell.cellHeight()
    }

}

extension SettingsViewController{
    
    func changeNotificationSettings(_ toggleSwitch: UISwitch) {
        
         return // ------ delete this when api completed
        
        if UserStore.shared.isNotificationUnAuthorized == true, toggleSwitch.isOn  {
            reverseTheToggle(toggleSwitch)
            askForNotificationPermissions()
            return
        }
        
        if !isConnectedToNetwork(){
            reverseTheToggle(toggleSwitch)
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else {
            reverseTheToggle(toggleSwitch)
            return
        }
        
        let deviceName = UIDevice.current.name
        guard !UserStore.shared.deviceToken.isEmpty else {
            reverseTheToggle(toggleSwitch)
            return
        }
        guard let uniqueIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            reverseTheToggle(toggleSwitch)
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let notificationObserver: Observable<String>
        if toggleSwitch.isOn == false{
            notificationObserver = ApiManager.shared.apiService.toggleNotificationStatusOff("off", userID: String(userID))
        }else{
            notificationObserver = ApiManager.shared.apiService.toggleNotifications(toggleSwitch.isOn, deviceName: deviceName, userId: String(userID), deviceId: uniqueIdentifier, deviceToken: UserStore.shared.deviceToken)
        }
        let notificationDisposable = notificationObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                UserStore.shared.isNotificationsStatus = toggleSwitch.isOn
                self.showSimpleAlertWithMessage(message)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                let isON = toggleSwitch.isOn
                toggleSwitch.setOn(!isON, animated: true)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                          self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        notificationDisposable.disposed(by: disposableBag)
    }
}
