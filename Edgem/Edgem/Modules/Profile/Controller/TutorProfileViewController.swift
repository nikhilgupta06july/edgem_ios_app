//
//  TutorProfileViewController.swift
//  Edgem
//
//  Created by Namespace on 14/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift
import GooglePlaces

class TutorProfileViewController: BaseViewController {
    
     // MARK: --------- Properties
    
    let labelField = ["First Name*", "Last Name*", "Gender*", "Birthday*", "Email Address*","Contact Number*", "Home Address*", "Work Address"]
    var rowCount = 0
    var userData: User!
    var editedUser: User!
    var disposableBag = DisposeBag()
    var countryCodeOptions = ["+65"]
    let countryCodePicker = UIPickerView()
    var activeTextField: UITextField!
    var userAddresses = [UserAddress]()
    var selectedIndex = 0
    
    // MARK: --------- @IBOutlets
    
    @IBOutlet weak var titleVC: UILabel!{
        didSet{
            titleVC.text = GeneralString.personalInformation.description()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var saveBtnHolderView: UIView!
    @IBOutlet weak var saveBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.rowCount = labelField.count
        initialSetUpForCountryCodePicker()
        
//        let otpVC = OTPViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
//        otpVC.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let _user = AppDelegateConstant.user{
            userData = _user
            editedUser = User.init(userData)
            tableView.reloadData()
        }
        
        if let email = AppVariables.emailAndContact?.email{
            editedUser.email = email
            self.tableView.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
            AppVariables.emailAndContact?.email = nil
        }
        
        if let contact = AppVariables.emailAndContact?.contact{
            editedUser.contactNumber = contact
            self.tableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .automatic)
            AppVariables.emailAndContact?.contact = nil
        }
        
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // BottomHolderView
        bottomContainerView.layer.borderWidth = 0.8
        bottomContainerView.layer.borderColor = UIColor(hue: 23/255, saturation: 23/255, brightness: 23/255, alpha: 0.3).cgColor
        
        //Add Account
        saveBtn.layer.cornerRadius = 5
        saveBtn.clipsToBounds = true
        saveBtn.setTitle("Save".localized, for: .normal)
        saveBtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        saveBtn.titleLabel?.textColor = theamBlackColor
        
        // addAccountBtncontainerView
        saveBtnHolderView.layer.shadowColor = UIColor.black.cgColor
        saveBtnHolderView.layer.shadowOpacity = 0.12
        saveBtnHolderView.layer.shadowRadius = 4
        saveBtnHolderView.layer.shadowOffset = .zero
        saveBtnHolderView.layer.shadowPath = UIBezierPath(rect: saveBtnHolderView.bounds).cgPath
        
    }
    
    // MARK: --------- @IBActions
    
    @IBAction func didTappedsavebtn(_ sender: UIButton) {

        var dict = [String:Any]()
        
        dict = [
            "country_code" : "65",
            "contact_number" : (editedUser.contactNumber) ?? "",
            "email" : editedUser.email,
        ]
        
        if let userAddresses = editedUser.userAddress{
            if userAddresses.count > 0 {
                
                userAddresses.forEach({ (address) in
                    var addressDict = [String:AnyObject]()
                    addressDict = ["address_location" : address.addressLocation ?? "",
                                   "lattitude" : address.lattitude ?? "",
                                   "longitude" : address.longitude ?? ""] as [String : AnyObject]
                    
                    if address.addressLabel.contains("Home") == true{
                        dict["home_address"] = addressDict
                    }else if address.addressLabel.contains("Work") == true {
                        dict["work_address"] = addressDict
                    }
                })
            }
        }
        

        
        //let param = (editedUser.getEditedTutorDetail())
        print(value: dict)
        
        
      
        updateTutorInfoData(dict as! [String:AnyObject])
        
    }
    
    // MARK: ------- Private Functions

    //
    fileprivate func getAppropriateAddressLabel(labelFromServer label: String) -> String{
        switch label{
        case "Work","work","Work Address" :
            return "Work Address*"
        
        case "Home","home","Home Address" :
            return "Home Address*"
        default:
            return "Other Address*"
        }
    }
    
    // Present the Autocomplete view controller when the button is pressed.
    func autocompleteClicked() {
        
        self.view.endEditing(true)
        
        let vc = ConfirmLocationViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
        vc.referredVC = self
        self.navigationController?.pushViewController(vc, animated: true)
    
    }
    
    func addressFromConfirmLocationVC(_ address: LocationData?){
        print(address)
        if let addr = address{
            
            if  selectedIndex == 6 {
                editedUser.userAddress?[0].addressLocation = "\(addr.address)"
                editedUser.userAddress?[0].lattitude = "\(addr.lattitude)"
                editedUser.userAddress?[0].longitude = "\(addr.longitude)"
                editedUser.userAddress?[0].addressLabel = "Home"
                let indexPath = IndexPath(row: 6, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .none)
            }else if selectedIndex == 7{
                
                let addressObject = [  "address_location" : "\(addr.address)",
                    "lattitude" : "\(addr.lattitude)",
                    "longitude" : "\(addr.longitude)"
                ]
                let userAddress = UserAddress(addressObject: addressObject)
                editedUser.userAddress?.append(userAddress)
                editedUser.userAddress?[1].addressLocation = "\(addr.address)"
                editedUser.userAddress?[1].lattitude = "\(addr.lattitude)"
                editedUser.userAddress?[1].longitude = "\(addr.longitude)"
                editedUser.userAddress?[1].addressLabel = "Work"
                
                let indexPath = IndexPath(row: 7, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: .none)
            }
        }
    }
    
    func configureTextField(_ customTextField: EdgemCustomTextField) {
        
        customTextField.delegate = self
        customTextField.isSecureTextEntry = false
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        customTextField.inputView = nil
        
        switch customTextField.tag {
        case TutorProfileTextFieldType.firstName.rawValue:
            
            customTextField.keyboardType = .asciiCapable
            customTextField.autocapitalizationType = .words
            customTextField.isUserInteractionEnabled = false
   
        case TutorProfileTextFieldType.lastName.rawValue:
            
            customTextField.keyboardType = .asciiCapable
            customTextField.autocapitalizationType = .words
            customTextField.isUserInteractionEnabled = false

        case TutorProfileTextFieldType.gender.rawValue:
//            customTextField.icon = "drop-down-arrow"
//            customTextField.showImage()
//            customTextField.inputView = genderPickerView
//            customTextField.tintColor = UIColor.clear
             customTextField.isUserInteractionEnabled = false
            break
        case TutorProfileTextFieldType.birthday.rawValue:
//            //customTextField.icon = "drop-down-arrow"
//            //customTextField.showImage()
//            customTextField.inputView = datePicker
//            customTextField.tintColor = UIColor.clear
             customTextField.isUserInteractionEnabled = false
            break
        case TutorProfileTextFieldType.emailAddress.rawValue:
            
            customTextField.keyboardType = .emailAddress
             customTextField.isUserInteractionEnabled = true
            
        case TutorProfileTextFieldType.dual.rawValue:
            if customTextField.title == "Contact Number*".localized {
                customTextField.keyboardType = .phonePad
            } else{
                customTextField.inputView = countryCodePicker
                customTextField.icon = "drop-down-arrow"
                
                customTextField.showImage()
                customTextField.keyboardType = .phonePad
                customTextField.tintColor = UIColor.clear
            }
            
        case TutorProfileTextFieldType.homeAddress.rawValue:
            
            customTextField.keyboardType = .asciiCapable
            customTextField.autocapitalizationType = .words
             //customTextField.isUserInteractionEnabled = false
            
        case TutorProfileTextFieldType.workAddress.rawValue:
            
            customTextField.keyboardType = .asciiCapable
            customTextField.autocapitalizationType = .words
           // customTextField.isUserInteractionEnabled = false
            
        default:
            customTextField.keyboardType = .asciiCapable
        }
        
    }
    
    func updateLabel(_ customTextField: EdgemCustomTextField, _ label: UILabel){
        switch customTextField.tag {
            
        case TutorProfileTextFieldType.homeAddress.rawValue:
            if let _ = editedUser.userAddress?[safe: 0] {
               let addr1 =  editedUser.userAddress?[0].addressLabel
                if let addrLabel = addr1{
                    label.text = self.getAppropriateAddressLabel(labelFromServer: addrLabel)
                }
            }
            
            break
        case TutorProfileTextFieldType.workAddress.rawValue:
            if let _ = editedUser.userAddress?[safe: 1] {
                if let _ = editedUser.userAddress?[safe: 0] {
                    let addr2 =  editedUser.userAddress?[1].addressLabel
                    if let addrLabel = addr2{
                        label.text = self.getAppropriateAddressLabel(labelFromServer: addrLabel)
                    }
                }
            }
            break
        default:
            break
        }
    }
    
    func updateDataIntoTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
        
        switch customTextField.tag {
            
        case  TutorProfileTextFieldType.firstName.rawValue:
            fromCell ? (customTextField.text = editedUser.firstName) : (editedUser.firstName = customTextField.text!)
            break
        case TutorProfileTextFieldType.lastName.rawValue:
            fromCell ? (customTextField.text = editedUser.lastName) : (editedUser.lastName = customTextField.text!)
            break
        case TutorProfileTextFieldType.gender.rawValue:
            fromCell ? (customTextField.text = editedUser.gender) : (editedUser.gender = customTextField.text!)
            break
        case TutorProfileTextFieldType.birthday.rawValue:
            fromCell ? (customTextField.text = editedUser.birthday) : (editedUser.birthday = customTextField.text!)
            break
        case TutorProfileTextFieldType.emailAddress.rawValue:
            fromCell ? (customTextField.text = editedUser.email) : (editedUser.email = customTextField.text!)
            break
        case TutorProfileTextFieldType.dual.rawValue:
            if customTextField.title?.contains("Contact") ?? false {
                fromCell ? (customTextField.text = editedUser.contactNumber) : (editedUser.contactNumber = customTextField.text!)
            } else {
                fromCell ? (customTextField.text = "+" + editedUser.countryContactCode) : (editedUser.countryContactCode = customTextField.text!)
            }
        case TutorProfileTextFieldType.homeAddress.rawValue:
            if let _ = editedUser.userAddress?[safe: 0] {
                 fromCell ? (customTextField.text = editedUser.userAddress?[0].addressLocation) : (editedUser.userAddress?[0].addressLocation = customTextField.text!)
            }
           
            break
        case TutorProfileTextFieldType.workAddress.rawValue:
            if let _ = editedUser.userAddress?[safe: 1] {
                fromCell ? (customTextField.text = editedUser.userAddress?[1].addressLocation) : (editedUser.userAddress?[1].addressLocation = customTextField.text!)
            }
            break
        default:
            break
        }
        
        if  editedUser.firstName == "" || editedUser.lastName == "" || editedUser.gender == "" || editedUser.birthday == "" || editedUser.email == "" || editedUser.contactNumber == ""{
            showHideNextButton(false)
            if fromCell == false{
                tableView.beginUpdates()
                tableView.endUpdates()
            }
        }
        
    }
    
    func isDataValid() -> (isValid: Bool, message: String){
        
        var errorMessages: [String] = []
        
        if editedUser.firstName.isEmpty, editedUser.lastName.isEmpty, editedUser.gender.isEmpty,  editedUser.birthday.isEmpty,  editedUser.email.isEmpty, editedUser.contactNumber.isEmpty  {
            return (isValid: false, message: "")
        } else {
            for index in 0..<labelField.count {
                
                let currentIndexPath = IndexPath.init(row: index, section: 0)
                
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    
                    switch index {
                        
                    case TutorProfileTextFieldType.firstName.rawValue: cell.validateFirstName(text: editedUser.firstName)
                        
                    case TutorProfileTextFieldType.lastName.rawValue: cell.validateLastName(text: editedUser.lastName)
                        
                    case TutorProfileTextFieldType.gender.rawValue: cell.validateGenderName(text: editedUser.gender)
                        
                    case TutorProfileTextFieldType.birthday.rawValue: cell.validateBirthday(text: editedUser.birthday)
                        
                    case TutorProfileTextFieldType.emailAddress.rawValue: cell.validateEmail(text: editedUser.email)
                        
                    case TutorProfileTextFieldType.dual.rawValue: cell.validateContact(text: editedUser.contactNumber)
                        
                    case TutorProfileTextFieldType.homeAddress.rawValue: break//cell.validate
                        
                    case TutorProfileTextFieldType.workAddress.rawValue: break//cell.validate
                        
                    default:
                        break
                        
                    }
                    let message = cell.errorLabel.text ?? ""
                    if !message.isEmptyString() {
                        errorMessages.append(message)
                    }
                }
                
            }
            tableView.beginUpdates()
            tableView.endUpdates()
            let isValidData = errorMessages.count == 0
            return (isValid: isValidData, message: isValidData ? "" : errorMessages[0])
        }
    }
    
    func showHideNextButton(_ status: Bool) {
        
        if status == false{
            saveBtn.isUserInteractionEnabled = false
            saveBtn.backgroundColor = UIColor(white: 1, alpha: 0.60)
        }else{
            saveBtn.isUserInteractionEnabled = true
            saveBtn.backgroundColor = theamAppColor
        }
        
    }
    
}

 // MARK: --------- table view methods

extension TutorProfileViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row != 5 else{
            let cell = tableView.dequeueReusableCell(withIdentifier: EdgemDualTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemDualTextFieldTableViewCell
           cell.configureCellWithTitles(title: ["+65",labelField[indexPath.row]], index: indexPath.row)
            configureTextField(cell.textFieldOne)
            updateDataIntoTextField(cell.textFieldOne, fromCell: true)
            configureTextField(cell.textFieldTwo)
            updateDataIntoTextField(cell.textFieldTwo, fromCell: true)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: EdgemTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemTextFieldTableViewCell
        cell.configureCellForRowAtIndexForTutor(indexPath.row, withText: labelField[indexPath.row])
        configureTextField(cell.customTextField)
        updateDataIntoTextField(cell.customTextField, fromCell: true)
        updateLabel(cell.customTextField, cell.customTextField.titleLabel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 4 {
            let verifyEmail = VerifyEmailAndContactViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            verifyEmail.VC_TITLE = "Verify your email".localized
            verifyEmail.email_or_contact = 0
            self.navigationController?.pushViewController(verifyEmail, animated: true)
        }else if  indexPath.row == 5{
            let verifyEmail = VerifyEmailAndContactViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
             verifyEmail.VC_TITLE = "Verify your Contact".localized
             verifyEmail.email_or_contact = 1
            self.navigationController?.pushViewController(verifyEmail, animated: true)
        }else if indexPath.row == 6{
            selectedIndex = 6
            autocompleteClicked()
        }else if indexPath.row == 7{
             selectedIndex = 7
            autocompleteClicked()
        }

    }
    
}

 // MARK: -------- Protocols

extension TutorProfileViewController: OTPViewControllerProtocol{
    func returnEmailOrContact(_ emailOrContact: String) {
        print(emailOrContact)
    }
}

 // MARK: --------- Text Field methods
extension TutorProfileViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        activeTextField = textField as! EdgemCustomTextField
        
        self.view.endEditing(true)

        switch textField.tag {

        case TutorProfileTextFieldType.dual.rawValue:
            
            let verifyEmail = VerifyEmailAndContactViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            verifyEmail.VC_TITLE = "Verify your Contact".localized
            verifyEmail.email_or_contact = 1
            self.navigationController?.pushViewController(verifyEmail, animated: true)
            
        case TutorProfileTextFieldType.emailAddress.rawValue:
//
//            let verifyController = VerifyEmailAndContactViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
//            self.navigationController?.pushViewController(verifyController, animated: true)
//            break
            
            let verifyEmail = VerifyEmailAndContactViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
            verifyEmail.VC_TITLE = "Verify your email".localized
            verifyEmail.email_or_contact = 0
            self.navigationController?.pushViewController(verifyEmail, animated: true)
            
        case TutorProfileTextFieldType.homeAddress.rawValue:
            selectedIndex = 6
            autocompleteClicked()

        case TutorProfileTextFieldType.workAddress.rawValue:
            selectedIndex = 7
            autocompleteClicked()

        default:
            break
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

        updateDataIntoTextField(textField as! EdgemCustomTextField, fromCell: false)
        
        let currentIndexPath = IndexPath.init(row: textField.tag, section: 0)
        
        switch textField.tag {
            
        case TutorProfileTextFieldType.firstName.rawValue:
                                                                                        let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
                                                                                        cell.validateFirstName(text: textField.text ?? "")
            
        case TutorProfileTextFieldType.lastName.rawValue:
                                                                                        let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
                                                                                        cell.validateLastName(text: textField.text ?? "")
            
        case TutorProfileTextFieldType.gender.rawValue:
                                                                                        let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
                                                                                        cell.validateGenderName(text: textField.text ?? "")
            
        case TutorProfileTextFieldType.birthday.rawValue:
                                                                                        let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
                                                                                        cell.validateBirthday(text: textField.text ?? "")
            
        case TutorProfileTextFieldType.emailAddress.rawValue:
                                                                                        let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
                                                                                        cell.validateEmail(text: textField.text ?? "")
        case TutorProfileTextFieldType.dual.rawValue :
                                                                                         let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemDualTextFieldTableViewCell
                                                                                         let customField = textField as! EdgemCustomTextField
                                                                                         if customField.title == "Contact Number*".localized {
                                                                                            cell.validatePhone(text: textField.text ?? "")
                                                                                         } else {
                                                                                           cell.validateCountryCode(text: textField.text ?? "")
                                                                                        }
            
        case TutorProfileTextFieldType.homeAddress.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
            if let _ = editedUser.userAddress?[safe: 0] {
                let addr1 =  editedUser.userAddress?[0].addressLabel
                cell.customTextField.titleLabel.text = addr1
            }
                                                                                        break//cell.validateLastName(text: textField.text ?? "")
            
        case TutorProfileTextFieldType.workAddress.rawValue:
            let cell = tableView.cellForRow(at: currentIndexPath) as! EdgemTextFieldTableViewCell
            if let _ = editedUser.userAddress?[safe: 1] {
                let addr2 =  editedUser.userAddress?[1].addressLabel
                 cell.customTextField.titleLabel.text = addr2
            }
                                                                                        break//cell.validateLastName(text: textField.text ?? "")
            
        default:
            break
        }
        
        if editedUser.firstName != "" && editedUser.lastName != "" && editedUser.gender != "" && editedUser.birthday != "" && editedUser.email != "" || editedUser.contactNumber != ""{
            showHideNextButton(isDataValid().isValid)
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
    
}

//MARK:  ----------------- GMSAutocompleteViewControllerDelegate Delegates Methods

extension TutorProfileViewController: GMSAutocompleteViewControllerDelegate{
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if  selectedIndex == 6 {
            editedUser.userAddress?[0].addressLocation = "\(place.name)"
            editedUser.userAddress?[0].lattitude = "\(place.coordinate.latitude)"
            editedUser.userAddress?[0].longitude = "\(place.coordinate.longitude)"
            let indexPath = IndexPath(row: 6, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }else if selectedIndex == 7{

            let addressObject = [  "address_location" : "\(place.name)",
                                                "lattitude" : "\(place.coordinate.latitude)",
                                                "longitude" : "\(place.coordinate.longitude)"
                                             ]
            let userAddress = UserAddress(addressObject: addressObject)
            editedUser.userAddress?.append(userAddress)
            editedUser.userAddress?[1].addressLocation = "\(place.name)"
            editedUser.userAddress?[1].lattitude = "\(place.coordinate.latitude)"
            editedUser.userAddress?[1].longitude = "\(place.coordinate.longitude)"
            
            let indexPath = IndexPath(row: 7, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }

        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

// MARK: UIPickerView Delegate/Datasource

extension TutorProfileViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    
    func initialSetUpForCountryCodePicker(){
        countryCodePicker.delegate = self
        countryCodePicker.dataSource = self
        countryCodePicker.tag = 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        let textField = activeTextField as! EdgemCustomTextField
        countryCodePicker.reloadAllComponents()
        countryCodePicker.reloadInputViews()
        if textField.tag == 1 {
            textField.inputView = countryCodePicker
            let index = countryCodeOptions.index(of: editedUser.countryContactCode)
            if let _index = index {
                self.countryCodePicker.selectRow(_index, inComponent: 0, animated: true)
                activeTextField.text = countryCodeOptions[_index]
            } else {
                countryCodePicker.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = countryCodeOptions[0]
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryCodeOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryCodeOptions[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        activeTextField.text = countryCodeOptions[row]
    }
    
    
}

extension TutorProfileViewController {
    
    func updateTutorInfoData(_ param: [String : AnyObject]){
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])

        let updateUserInfoObserver = ApiManager.shared.apiService.updateTutorInfo(param)
        let updateUserInfoDisposable = updateUserInfoObserver.subscribe(onNext: {(userData) in
            
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                self.editedUser = userData
                AppDelegateConstant.user = userData
                self.tableView.reloadData()
                //self.showSimpleAlertWithMessage("Your personal information has been successfully updated.".localized)
                self.showSimpleAlertWithMessgeAndNavigateBack("Your personal information has been successfully updated.".localized)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        updateUserInfoDisposable.disposed(by: disposableBag)
        
    }
}
