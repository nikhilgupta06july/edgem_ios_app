//
//  ChangePasswordConfirmViewController.swift
//  Edgem
//
//  Created by Namespace on 08/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class ChangePasswordConfirmViewController: BaseViewController {
    
    // MARK: -------Properties
    
    // MARK: -------@IBOutlets
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var VCTitle: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var doneBtnContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    // MARK: -------@IBActions
    
    @IBAction func saveBtnTapped(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: -------Private Functions
    
    private func initialSetUP(){
        
        // message label
        messageLabel.text = "Your password has been successfully updated".localized
    
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        doneBtnContainerView.layer.cornerRadius = 4
        doneBtnContainerView.layer.shadowColor = UIColor.black.cgColor
        doneBtnContainerView.layer.shadowOpacity = 0.12
        doneBtnContainerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        doneBtnContainerView.layer.shadowRadius = 4
     
        saveButton.setTitle("Done".localized, for: .normal)
        saveButton.layer.cornerRadius = 5
        
    }
    
}
