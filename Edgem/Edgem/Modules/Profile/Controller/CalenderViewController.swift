//
//  CalenderViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 05/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import SpreadsheetView
import RxSwift


class CalenderViewController: BaseViewController {
        
        @IBOutlet weak var tabButtonOne: UIButton!
        @IBOutlet weak var tabButtonTwo: UIButton!
        @IBOutlet weak var tabLineImg: UIImageView!
        @IBOutlet weak var saveBtn: UIButton!
        @IBOutlet weak var saveBtnContainerView: UIView!
        @IBOutlet weak var calendarSpreadSheet: SpreadsheetView!
        @IBOutlet weak var saveBtnContainerViewHeight: NSLayoutConstraint!
    
    
        var headers = ["".localized.uppercased(), "MON".localized, "TUE".localized, "WED".localized, "THU".localized, "FRI".localized, "SAT".localized, "SUN".localized]
    
        var data = [[String]]()
    
        var schedules = ["MON": [String](), "TUE": [String](), "WED": [String](), "THU": [String](), "FRI": [String](), "SAT": [String](), "SUN": [String]()]
    
        /// this variable is used to find 'id' corresponding to day to get booking detail
        var daySchedule:[(String,String)] = [(String,String)]()
    
        var schedulesCount = 0
        var isEditable = true
        var currentIndex = 1
        var movingX: CGFloat = 0.0
        var disposableBag = DisposeBag()
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
//            getDataArrayFromSchedule()
//            calendarSpreadSheet.delegate = self
//            calendarSpreadSheet.dataSource = self
//            calendarSpreadSheet.isDirectionalLockEnabled = true
//            calendarSpreadSheet.register(HeaderSpreadsheetCell.self, forCellWithReuseIdentifier: String(describing: HeaderSpreadsheetCell.self))
//            calendarSpreadSheet.register(TextCell.self, forCellWithReuseIdentifier: String(describing: TextCell.self))
//            calendarSpreadSheet.register(CheckBoxCell.self, forCellWithReuseIdentifier: String(describing: CheckBoxCell.self))
            
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            if let _user = appDelegate.user{
////                userRegistration = _user
//            }
            getUserAvailibility()
            setTabButtons(currentIndex)
        }
    
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(true)
           // calendarSpreadSheet.scrollToItem(at: IndexPath.init(row: 6, column: 0), at: .top, animated: true)
        }
        
        @IBAction func tabButtonTapped(_ sender: UIButton) {
            
            if sender.tag == 1 {
                self.getUserAvailibility()
            }else{
                self.getUserBooking()
            }
            
            setTabButtons(sender.tag)
            showTransitionInView(sender.tag)
        }
        
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        
        var schedulesDict = ["MON": [String](), "TUE": [String](), "WED": [String](), "THU": [String](), "FRI": [String](), "SAT": [String](), "SUN": [String]()]
        var count = 0
        for i in 9...20 {
            for j in 1...7 {
                if data[i-9][j] == "1" {
                    let day = getDayBasedOnIndex(index: j)
                    var currentData = schedulesDict[day]
                    currentData!.append("\(i)")
                    schedulesDict[day] = currentData
                    count += 1
                }
            }
        }
        self.schedules = schedulesDict
        self.schedulesCount = count
        
        postUserAvailibility()
//        delegate?.slotsSelected(slots: schedulesDict, count: count)
//        self.navigateBack(sender: sender)
    }
    
    
        
        //MARK:- Helping Function
    
    fileprivate func refreshData(){
        
        getDataArrayFromSchedule()
        calendarSpreadSheet.delegate = self
        calendarSpreadSheet.dataSource = self
        calendarSpreadSheet.isDirectionalLockEnabled = true
        calendarSpreadSheet.register(HeaderSpreadsheetCell.self, forCellWithReuseIdentifier: String(describing: HeaderSpreadsheetCell.self))
        calendarSpreadSheet.register(TextCell.self, forCellWithReuseIdentifier: String(describing: TextCell.self))
        calendarSpreadSheet.register(CheckBoxCell.self, forCellWithReuseIdentifier: String(describing: CheckBoxCell.self))
        
    }
    
    func getDayBasedOnIndex(index: Int) -> String {
        switch index {
        case 1:
            return "MON"
        case 2:
            return "TUE"
        case 3:
            return "WED"
        case 4:
            return "THU"
        case 5:
            return "FRI"
        case 6:
            return "SAT"
        case 7:
            return "SUN"
        default:
            return ""
        }
    }
    
    func setTabButtons(_ forIndex: Int){
            
            tabButtonOne.titleLabel?.font = UIFont(name: forIndex == 1 ? QuicksandBold : QuicksandRegular, size: 18)
            tabButtonTwo.titleLabel?.font = UIFont(name: forIndex == 2 ? QuicksandBold : QuicksandRegular, size: 18)
            
        }
        
        func showTransitionInView(_ forIndex: Int){
            
            if forIndex<currentIndex {
                movingX = ScreenWidth
                isEditable = true
                
                UIView.animate(withDuration: 0.5, animations: {
                    
                }) { (status) in
                    self.saveBtnContainerViewHeight.constant = 90
                    self.saveBtnContainerView.isHidden = false
                }
            }else{

                movingX = -ScreenWidth
                isEditable = false
                
                UIView.animate(withDuration: 0.5, animations: {
                    
                }) { (status) in
                    self.saveBtnContainerViewHeight.constant = 0
                    self.saveBtnContainerView.isHidden = true
                }

            }
           // let movingX = forIndex<currentIndex ? ScreenWidth : -ScreenWidth
           // isEditable = forIndex<currentIndex ? true : false
            
            currentIndex = forIndex
            
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveLinear, animations: {
                
                self.calendarSpreadSheet.transform = CGAffineTransform.identity.translatedBy(x:self.movingX, y: 0)
                self.tabLineImg.transform = CGAffineTransform.identity.translatedBy(x:CGFloat(forIndex-1)*(ScreenWidth/2), y: 0)
                
                
            },completion: { finished in
                
                self.calendarSpreadSheet.transform = CGAffineTransform.identity.translatedBy(x:0, y: 0)
//                self.tableView.reloadData()
                
            })
            
        }
    
    func getDataArrayFromSchedule() {
        
        var completeData = [[String]]()
        let weeksArray = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
        for i in 9...20 {
            var hourSchedule = [String]()
            if i > 9 {
                hourSchedule.append("\(i):00")
            } else {
                hourSchedule.append("0\(i):00")
            }
            for day in weeksArray {
                let daysSchedule = schedules[day]
                if daysSchedule != nil , daysSchedule!.contains("\(i)") {
                    hourSchedule.append("1")
                } else {
                    hourSchedule.append("0")
                }
            }
            completeData.append(hourSchedule)
        }
        data = completeData
    }
    
//        func getDataArrayFromSchedule() {
//            
//            var completeData = [[String]]()
//     
//            let weeksArray = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
//        
//            for i in 9...20 {
//                 var hourSchedule = [String]()
//                 var ids_booking:[String] = [String]()
//                if i > 9 {
//                    hourSchedule.append("\(i):00")
//                } else {
//                    hourSchedule.append("0\(i):00")
//                }
//                for day in weeksArray {
//                    var daysSchedule: [String] = Array()
//                   // var ids_booking:[String] = [String]()
//                    var daysSchedule_booking: [String] = Array()
//                    var _daySchedule: [(String, String)] = [(String,String)]()
//                    
//                    if currentIndex == 2{
//                        if let schedules = self.schedules[day], schedules.count > 0 {
//                            
//                            for index in 0..<schedules.count {
//                                if index%2 != 0{
//                                    daysSchedule.append(schedules[index])
//                                    //daysSchedule_booking.append(schedules[index])
//                                }else{
//                                    ids_booking.append(schedules[index])
//                                }
//                            }
//                        }
//                        
//                        if ids_booking.count > 0{
//                            for index in 0..<ids_booking.count{
//                                
//                                let schedule = ((daysSchedule[index],ids_booking[index]))
//                                self.daySchedule.append(schedule)
//                            }
//                        }
//
//                    }else{
//                        daysSchedule = schedules[day] ?? []
//                    }
//                    
//                    if daysSchedule.contains("\(i)") {
//                        hourSchedule.append("1")
//                    } else {
//                        hourSchedule.append("0")
//                    }
//                }
////
////                if ids_booking.count > 0{
////                    for i in 0..<ids_booking.count{
////                        _daySchedule.append((daysSchedule_booking[i],ids_booking[i]))
////                    }
////                }
//         
//               // self.daySchedule = _daySchedule
//                completeData.append(hourSchedule)
//            }
//            data = completeData
//        }
        
}

extension CalenderViewController: SpreadsheetViewDataSource, SpreadsheetViewDelegate {

    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return self.headers.count
    }

    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return data.count + 1
    }

    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return 50
        } else {
            return (ScreenWidth - 60)/7
        }
    }

    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if case 0 = row {
            return 45
        } else {
            return 35
        }
    }

    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }

    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        if case 0 = indexPath.row {
            let cell = calendarSpreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: HeaderSpreadsheetCell.self), for: indexPath) as! HeaderSpreadsheetCell
            cell.label.text = headers[indexPath.column]
            cell.gridlines.left = .none
            cell.gridlines.right = .none
            cell.gridlines.top = .none
            cell.gridlines.left = .none
            cell.gridlines.bottom = .none
            return cell
        } else {
            let dataString = data[indexPath.row - 1][indexPath.column]

            if indexPath.column == 0 {
                let cell = calendarSpreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: TextCell.self), for: indexPath) as! TextCell
                cell.label.text = dataString
                cell.label.backgroundColor = UIColor.clear
                cell.gridlines.left = .none
                cell.gridlines.top = .none
                cell.gridlines.right = .none
                cell.gridlines.bottom = .none

                return cell
            } else {
                let cell = calendarSpreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: CheckBoxCell.self), for: indexPath) as! CheckBoxCell
                if currentIndex == 2{
                    cell.imageView.image = UIImage(named: dataString == "0" ? "inactive_avalibility" : (dataString == "1" ? "largeBooking" : "inactive_avalibility"))
                }else{
                cell.imageView.image = UIImage(named: dataString == "0" ? "inactive_avalibility" : (dataString == "1" ? "active_avalibility" : "calendar_active"))
                }
                cell.gridlines.left = .none
                cell.gridlines.top = .none
                cell.gridlines.right = .none
                cell.gridlines.bottom = .none

                return cell
            }
        }
    }

    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
        
//        if isEditable == false{
//            return
//        }
        
        if currentIndex == 2{
            
            if indexPath.row != 0 || indexPath.column != 0 {
                guard let cellValue = data[safe: indexPath.row - 1]?[safe:indexPath.column] else{
                    print(value: "cellValue is not found")
                    return
                }
                let cell = spreadsheetView.cellForItem(at: indexPath) as! CheckBoxCell
                print(cellValue)
                if cellValue == "0" {
                    // do nothing
                } else {

//                    let bookingInfoVC = BookingDetailVC.instantiateFromAppStoryboard(appStoryboard: .Profile)
//                    bookingInfoVC.modalPresentationStyle = .overCurrentContext
//                   self.navigationController?.present(bookingInfoVC, animated: true, completion: nil)
                    
                }
            }
            
        }else{
            
            if indexPath.row != 0 || indexPath.column != 0 {
                guard let cellValue = data[safe: indexPath.row - 1]?[safe:indexPath.column] else{
                    print(value: "cellValue is not found")
                    return
                }
                let cell = spreadsheetView.cellForItem(at: indexPath) as! CheckBoxCell
                if cellValue == "0" {
                    data[indexPath.row - 1][indexPath.column] = "1"
                    cell.imageView.image = #imageLiteral(resourceName: "active_avalibility")
                } else {
                    data[indexPath.row - 1][indexPath.column] = "0"
                    cell.imageView.image = #imageLiteral(resourceName: "inactive_avalibility")
                }
            }
        }
    }

}


//extension CalenderViewController{//: CalendarDetailViewControllerDelegate {


//    func calendarDidUpdate(schedules: [String : [Schedule]], userCalendarEvents: [String : [UserEvent]]) {
//        self.schedules = schedules
//        self.userEvents = userCalendarEvents
//        data = getDataArrayFromSchedules(self.schedules, events: self.userEvents)
//        shouldReloadDataOnViewDidAppear = true
//    }
//}

extension CalenderViewController {
    
    
    fileprivate func postUserAvailibility(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else { return }
        
        var availibilityDict = [String : AnyObject]()
        
        availibilityDict = [
            "user_id"   : String(userID) as AnyObject
        ]
     
        let A_MON = schedules["MON"]!
        availibilityDict["MON"] = A_MON.joined(separator: ",") as AnyObject
        
        let A_TUE = schedules["TUE"]!
        availibilityDict["TUE"] = A_TUE.joined(separator: ",") as AnyObject
        
        let A_WED = schedules["WED"]!
        availibilityDict["WED"] = A_WED.joined(separator: ",") as AnyObject
        
        let A_THU = schedules["THU"]!
        availibilityDict["THU"] = A_THU.joined(separator: ",") as AnyObject
        
        let A_FRI = schedules["FRI"]!
        availibilityDict["FRI"] = A_FRI.joined(separator: ",") as AnyObject
        
        let A_SAT = schedules["SAT"]!
        availibilityDict["SAT"] = A_SAT.joined(separator: ",") as AnyObject
        
        let A_SUN = schedules["SUN"]!
        availibilityDict["SUN"] = A_SUN.joined(separator: ",") as AnyObject
        
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let userObserver = ApiManager.shared.apiService.postUserAvailibility(availibilityDict)
        let userDisposable = userObserver.subscribe(onNext: {(userAvailibility) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                
                self.schedules = userAvailibility.selectedTimeSlots
                self.refreshData()
                self.showSimpleAlertWithMessage("Your calendar has been successfully updated.".localized)
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    
    func getUserAvailibility() {
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else { return }

       Utilities.showHUD(forView: self.view, excludeViews: [])

        let calendarObserver = ApiManager.shared.apiService.getUserAvailibility("\(userID)")
        let calendarDisposable = calendarObserver.subscribe(onNext: {(userAvailibility) in
            
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                self.schedules = userAvailibility.selectedTimeSlots
                self.refreshData()
            }
            
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                 self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                       self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        calendarDisposable.disposed(by: disposableBag)
    }
    
    func getUserBooking() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else { return }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let calendarObserver = ApiManager.shared.apiService.getUserBooking("\(userID)")
        let calendarDisposable = calendarObserver.subscribe(onNext: {(userAvailibility) in
            
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                self.schedules = userAvailibility.selectedTimeSlots
                self.refreshData()
            }
            
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        calendarDisposable.disposed(by: disposableBag)
    }
}
