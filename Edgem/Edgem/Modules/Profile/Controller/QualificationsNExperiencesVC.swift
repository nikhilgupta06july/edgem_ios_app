//
//  QualificationsNExperiencesVC.swift
//  Edgem
//
//  Created by Namespace on 29/07/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import CropViewController


enum CellType{
    case Experience
    case Education
    
    func description()->Int{
        switch self{
        case .Experience: return 0
        case .Education : return 1
        }
    }
}

class QualificationsNExperiencesVC: BaseViewController {
    
    // MARK: - Properties
    
    var section0RowCount: Int = 0
    var section1RowCount: Int = 0
    var section2RowCount: Int = 1
    var section3RowCount: Int = 2
    
    var year: [Int] = Array()
    var newYearArr: [String] = Array()
    var fromYear: Int = 0
    var toYear: Int = 0
    let yearPicker = UIPickerView()
    let date = Date()
    let calendar = Calendar.current
    var fromYearTextField: UITextField!
    var toYearTextField: UITextField!
    var currentSection: Int?
    var currentRow: Int?
    
    var isQualificationSelected = false
    
    var disposableBag = DisposeBag()
    
    let sectionTitles = ["Select Your Qualifications".localized, "Experience".localized, "Education".localized, "Certificates".localized]
    
    var userQualification: [Qualification] = Array()
    
    var tempQualification: [Qualification] = Array()
    
    var userEducations: [Education] = Array(){
        didSet{
           section2RowCount = userEducations.count
        }
    }
    
    var experienceRowCount = 0
    
    var userExperience: [Experience] = [Experience](){
        didSet{
            section1RowCount = userExperience.count
        }
    }
    
    var documentImage: [UIImage] = [UIImage]() {
        didSet{
           let indexPath = IndexPath(row: 1, section: 3)
           self.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    var imageURLsArr: [String] = Array()
    
    var user = User()
    
    /// This variable will be used to track wether to show experience section or not
    var qualificationCellSelectedStatus: Bool? = false {
        didSet{
            print(value: qualificationCellSelectedStatus)
            
            if qualificationCellSelectedStatus == false{
                section1RowCount = 0
                
                if userExperience.count > 0, let lastExperienceCellData = userExperience.last{
                    
                    if  (lastExperienceCellData.instituteName == nil) ||  (lastExperienceCellData.subjectName == nil) || (lastExperienceCellData.fromYear == nil) || (lastExperienceCellData.toYear == nil){
                        
                        self.userExperience.removeAll()
                    }
                
                }
               
            }else{
                section1RowCount = self.userExperience.count > 0 ?  self.userExperience.count : 1
    
            }
            
            let sectionIndex = IndexSet(integer: 1)
            tableView.reloadSections(sectionIndex, with: .none)
        }
    }

    // MARK: - @IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var buttomContainerView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    
    // MARK: - View LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        isQualificationSelected = false
        initialSetUP()
        registerCell()
        
        if self.user.qualifications == nil {
            self.user.qualifications = [Qualification]()
        }
        loadImagesInDocumentCell()
        fetchUserQualifications()
        fetchUserExperience()
        fetchUserEducations()
        yearSetUP()
        updateUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateStudentAccountViewController.okBtnTapped(_:)), name: NSNotification.Name(rawValue: "OKButtonTapped"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }

     // MARK: - @IBActions
    
    @objc func okBtnTapped(_ notification: NSNotification) {
        print(value: "Ok Btn tapped")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapSaveBtn(_ sender: UIButton){
        addQualificationNExperience()
    }
    
    // MARK: -Helping Functions
    
    func getEducations() -> [[String:AnyObject]] {
        var params = [[String:AnyObject]]()
        if userEducations.count > 0{
            for index in 0..<userEducations.count {
                var tempParams = [String:AnyObject]()
                if userEducations[index].isUpdated == false{
                    tempParams["education_id"] = userEducations[index].id as AnyObject
                    tempParams["institute_name"] = userEducations[index].instituteName as AnyObject
                    tempParams["year_from"] = userEducations[index].fromYear as AnyObject
                    tempParams["certificate_name"] = userEducations[index].certificateName as AnyObject
                    tempParams["year_to"] = userEducations[index].toYear as AnyObject
                    
                    params.append(tempParams)
                }
            }
        }
        return params
    }
    
    func getExperience() -> [[String:AnyObject]] {
        var params = [[String:AnyObject]]()
        if userExperience.count > 0{
            for index in 0..<userExperience.count {
                var tempParams = [String:AnyObject]()
                if userExperience[index].isUpdated == false{
                    tempParams["experience_id"] = userExperience[index].id as AnyObject
                    tempParams["institute_name"] = userExperience[index].instituteName as AnyObject
                    tempParams["year_from"] = userExperience[index].fromYear as AnyObject
                    tempParams["subject_name"] = userExperience[index].subjectName as AnyObject
                    tempParams["year_to"] = userExperience[index].toYear as AnyObject
                    
                    params.append(tempParams)
                }
            }
        }
        return params
    }
    
    func updateDataIntoTextField(_ textField: UITextField, fromCell: Bool, index:Int, section: Int){
        
        if QualificationNExperienceVCSectionType.Experience.rawValue == section{
            
            if let tempExperience = userExperience[safe: index]{
                
                switch textField.tag {
                    
                case 1 :
                    fromCell ? (textField.text = tempExperience.instituteName) : (tempExperience.instituteName = textField.text!)
                case 2 :
                    fromCell ? (textField.text = tempExperience.subjectName) : ( tempExperience.subjectName = textField.text!)
                case 3 :
                    fromCell ? (textField.text = tempExperience.fromYear) : ( tempExperience.fromYear = textField.text!)
                case 4 :
                    fromCell ? (textField.text = tempExperience.toYear) : ( tempExperience.toYear = textField.text!)
                default:
                    break
                }
                
                tempExperience.isUpdated = false
                
                userExperience[index] = tempExperience
            }
        }else{
            if let tempEducation = userEducations[safe: index]{
                
                switch textField.tag {
                    
                case 1 :
                    fromCell ? (textField.text = tempEducation.instituteName) : (tempEducation.instituteName = textField.text!)
                case 2 :
                    fromCell ? (textField.text = tempEducation.certificateName) : ( tempEducation.certificateName = textField.text!)
                case 3 :
                    fromCell ? (textField.text = tempEducation.fromYear) : ( tempEducation.fromYear = textField.text!)
                case 4 :
                    fromCell ? (textField.text = tempEducation.toYear) : ( tempEducation.toYear = textField.text!)
                default:
                    break
                    
                }
                tempEducation.isUpdated = false
                
                userEducations[index] = tempEducation
            }
        }
        
        if !fromCell{
            //tempEducationObjects.append(education)
        }
        
    }

    /// This function will be called after taping qualification sectection button from 'QualificationsListTableViewCell'
    ///
    /// - Parameters:
    ///   - section: This is the section of selected qualification subject. This will be always 0
    ///   - row: This is the row of selected qualification subject.
    ///   - status: qualification sectection button selection status
    func didTapQualificationCell(_ section: Int, _ row: Int, _ status: Bool, _ qualification: Qualification?, _ counter: String){
        
        isQualificationSelected = true
 
        // This will help to track whether to show experience section or not. if true,  experience section will be visible else   experience section will be hide
        self.qualificationCellSelectedStatus = status
        
        // current indexPath
        let IndeXPath = IndexPath(row: row, section: section)
    
        self.user.totalExperience = counter
        if status == true{
            if let qua = qualification{
                self.user.qualifications?.append(qua)
                
                self.tempQualification.append(qua)
            }
        }else{
            if let qua = qualification{
                self.user.qualifications = self.user.qualifications?.filter{$0.id != qua.id}
                
                self.tempQualification = (self.user.qualifications?.filter{$0.id != qua.id})!
            }
        }
        

        
         //qualification object for selected row
       self.userQualification[IndeXPath.row].isSelected = status
        
        // update only selected row
        UIView.performWithoutAnimation {
            let loc = tableView.contentOffset
            tableView.reloadRows(at: [IndeXPath], with: .none)
            tableView.contentOffset = loc
        }
       
    }
    
    func didTapQualificationWithoutExperienceCell(_ status: Bool, _ qualification: Qualification?){
        
         isQualificationSelected = true
        
        if let qua = qualification{
            if status == true{
                //if let qua = qualification{
                    self.user.qualifications?.append(qua)
                
                   self.tempQualification.append(qua)
                
            }else{
               // if let qua = qualification{
                    self.user.qualifications = self.user.qualifications?.filter{$0.id != qua.id}
                
                    self.tempQualification = (self.user.qualifications?.filter{$0.id != qua.id})!
                //}
            }
           // self.user.qualifications?.append(qua)
        }
    }
    
    func didTapAddremoveExperience(_ identifire: Int, _ section: Int, _ row: Int, _ from: Int){
        
        var indexPath: IndexPath!
      
        if identifire == 6 {
            // add experience
            
            if from == 0{
                
                if let lastExperienceCellData = userExperience.last{
                    
                    if  (lastExperienceCellData.instituteName == nil) ||  (lastExperienceCellData.subjectName == nil) || (lastExperienceCellData.fromYear == nil) || (lastExperienceCellData.toYear == nil){
                        
                        self.showSimpleAlertWithMessage("Please fill in all blanks".localized)
                        return
                        
                    }
                    
                    if (lastExperienceCellData.fromYear != nil || !lastExperienceCellData.fromYear!.isEmpty) && (lastExperienceCellData.toYear != nil || !lastExperienceCellData.toYear!.isEmpty){
                        
                        if let fromYear = lastExperienceCellData.fromYear, let fromYearInt = Int(fromYear), let toYear = lastExperienceCellData.toYear, let toYearInt = Int(toYear){
                            
                            if fromYearInt > toYearInt{
                                
                                self.showSimpleAlertWithMessage("Invalid Year".localized)
                                
                                return
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
                  self.section1RowCount += 1
                
                  userExperience.append(Experience())
                
            }else{
                
                if let lastEducationCellData = userEducations.last{
                    
                    if (lastEducationCellData.instituteName.isEmpty) ||  (lastEducationCellData.certificateName.isEmpty) || (lastEducationCellData.fromYear.isEmpty) || (lastEducationCellData.toYear.isEmpty) {
                        
                        self.showSimpleAlertWithMessage("Please fill all the details".localized)
                        
                        return
                    }
                    
                    if (!lastEducationCellData.fromYear.isEmpty && !lastEducationCellData.toYear.isEmpty){
                        
                        
                        let fromYearInt = Int(lastEducationCellData.fromYear)
                        let toYearInt = Int(lastEducationCellData.toYear)
                        
                        if fromYearInt! > toYearInt!{
                            
                            self.showSimpleAlertWithMessage("Invalid Year".localized)
                            
                            return
                            
                        }
                    }
                }
                
                 self.section2RowCount += 1
                
                 userEducations.append(Education())
                
            }
          
             indexPath = IndexPath(row: row+1, section: section)
            
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
           
            tableView.reloadSections([indexPath.section], with: .none)
            
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            
        }else if identifire == 5{
            
            // remove experience
            
            if from == 0{
                
                self.section1RowCount -= 1
                userExperience.removeLast()
            }else{
                self.section2RowCount -= 1
                userEducations.removeLast()
            }
            
             indexPath = IndexPath(row: row, section: section)
            
            self.tableView.beginUpdates()
            
            self.tableView.deleteRows(at: [indexPath], with: .none)
            
            indexPath = IndexPath(row: row-1, section: section)
            
            self.tableView.reloadRows(at: [indexPath], with: .none)
            
            self.tableView.endUpdates()
            
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
          
        }
    }
    
    fileprivate func fetchUserQualifications() {
        
        if let qualifications = AppDelegateConstant.qualification{
            if let selectedQualifications = AppDelegateConstant.user?.qualifications {
                var tempQualifications = [Qualification]()
                if qualifications.count > 0{
                    tempQualifications = qualifications
                    for index in 0..<qualifications.count{
                        for s_index in 0..<selectedQualifications.count{
                            if qualifications[index].id == selectedQualifications[s_index].id{
                                tempQualifications[index].isSelected = true
                                tempQualifications[index].totalExperience = selectedQualifications[s_index].totalExperience
                            }
                        }
                    }
                }
                self.qualificationCellSelectedStatus = tempQualifications.count>0 ? tempQualifications[0].isSelected : false
                self.userQualification = tempQualifications
                //self.user.qualifications = tempQualifications
                self.section0RowCount =  self.userQualification.count
            }
            
        }
        
    }
    
    fileprivate func fetchUserExperience() {
        
        if let user = AppDelegateConstant.user, let experiences = user.experiences{
            if experiences.count > 0{
                self.userExperience = experiences.map{
                    $0.isUpdated = true
                    return $0
                }
                self.experienceRowCount = userExperience.count
                self.section1RowCount = userExperience.count
            }
        }
        print(userExperience)
    }
    
    fileprivate func fetchUserEducations() {
        
        if let user = AppDelegateConstant.user, let educations = user.educations{
            if educations.count > 0{
                self.userEducations = educations.map{
                    $0.isUpdated = true
                    return $0
                }
               self.section2RowCount = userEducations.count
            }
        }
        print(userEducations)
    }
    
    func didTapDocument(_ image: UIImage){
        self.imageTapped(image: image)
    }
    
    func imageTapped(image:UIImage){
        
        let containerView = UIView()
        containerView.frame = UIScreen.main.bounds
        containerView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.7)
        containerView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage(_:)))
        containerView.addGestureRecognizer(tap)
        self.view.addSubview(containerView)
        
        let imageHoldetView = UIView()
        imageHoldetView.backgroundColor = UIColor.white
        imageHoldetView.frame = CGRect(x: 50, y: 150, width: ScreenWidth-100, height: ScreenHeight-300)
        imageHoldetView.isUserInteractionEnabled = false
        imageHoldetView.layer.cornerRadius = 15
        imageHoldetView.clipsToBounds = true
        
        containerView.addSubview(imageHoldetView)
        
        
        let newImageView = UIImageView(image: image)
        newImageView.frame = CGRect(x: 12, y: 12, width: imageHoldetView.bounds.width-24, height: imageHoldetView.bounds.height-24)
        newImageView.backgroundColor = .clear
        newImageView.contentMode = .scaleAspectFill
        newImageView.clipsToBounds = true
        imageHoldetView.addSubview(newImageView)
        
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    fileprivate func loadImagesInDocumentCell(){
        
        if let userDocs = AppDelegateConstant.user?.documents, userDocs.count > 0{
            
            userDocs.forEach { (doc) in
                
                if doc.name != nil{
                    
                    if let image = imageCache.image(forKey: doc.name) {
                        
                        self.documentImage.append(image)
                        
                    }else{
                        
                        let imageProvider = ImageProvider()
                        
                        if let imageURL = doc.name, !imageURL.isEmpty, let url = URL(string: imageURL) {
                            
                            imageProvider.requestImage(from: url, completion: { (image) in
                                
                                //self.activityIndicator.stopAnimating()
                                imageCache.add(image, forKey: doc.name)
                                
                                self.documentImage.append(image)
                                
                            })
                            
                        }
                        
                    }
                    
                }
        
            }
            
        }
        
    }
    
    fileprivate func isDataValid() -> Bool {
        
//        if  isQualificationSelected == true{
//            
//            if user.qualifications == nil  || user.qualifications?.count == 0{
//                self.showSimpleAlertWithMessage("Please select at least one qualification".localized)
//                return false
//            }
//            
////            if self.userQualification != nil && self.userQualification.count > 0{
////                for qua in userQualification{
////                    if qua.isSelected == true{
////                        break
////                    }
////                }
////
////            }
//            
//        }else{
//            
//            for qualification in self.userQualification{
//                
//                if qualification.isSelected == true{
//                    self.user.qualifications?.append(qualification)
//                }
//            }
//            
//            if user.qualifications == nil  || user.qualifications?.count == 0{
//                self.showSimpleAlertWithMessage("Please select at least one qualification".localized)
//                return false
//            }
//            
//        }
        
        for qualification in self.userQualification{
            
            if qualification.isSelected == true{
                self.user.qualifications?.append(qualification)
            }
        }
        
        if user.qualifications == nil  || user.qualifications?.count == 0{
            self.showSimpleAlertWithMessage("Please select at least one qualification".localized)
            return false
        }
        
        if userExperience.count > 0, let lastExperienceCellData = userExperience.last{
            
            if  (lastExperienceCellData.instituteName == nil) ||  (lastExperienceCellData.subjectName == nil) || (lastExperienceCellData.fromYear == nil) || (lastExperienceCellData.toYear == nil){
                
                self.showSimpleAlertWithMessage("Please fill in all blanks".localized)
                
                return false
                
            }
            
            if (lastExperienceCellData.fromYear != nil || !lastExperienceCellData.fromYear!.isEmpty) && (lastExperienceCellData.toYear != nil || !lastExperienceCellData.toYear!.isEmpty){
                
                if let fromYear = lastExperienceCellData.fromYear, let fromYearInt = Int(fromYear), let toYear = lastExperienceCellData.toYear, let toYearInt = Int(toYear){
                    
                    if fromYearInt > toYearInt{
                        
                        self.showSimpleAlertWithMessage("Invalid year".localized)
                        
                        return false
                        
                    }
                    
                }
                
            }
            
            
        }
        
        if userEducations.count > 0, let lastEducationCellData = userEducations.last{
            
            if  (lastEducationCellData.instituteName.isEmpty) ||  (lastEducationCellData.certificateName.isEmpty) || (lastEducationCellData.fromYear.isEmpty) || (lastEducationCellData.toYear.isEmpty){
                
                self.showSimpleAlertWithMessage("Please fill in all blanks".localized)
                
                return false
                
            }
            
            if (!lastEducationCellData.fromYear.isEmpty && !lastEducationCellData.toYear.isEmpty){
                
                
                let fromYearInt = Int(lastEducationCellData.fromYear)
                let toYearInt = Int(lastEducationCellData.toYear)
                
                if fromYearInt! > toYearInt!{
                    
                    self.showSimpleAlertWithMessage("Invalid year".localized)
                    
                    return false
                    
                }
            }
        }
        return true
        
    }
    
}

// MARK: ---------- Image Picker Delegates

extension QualificationsNExperiencesVC: CropViewControllerDelegate{
    
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.documentImage.append(image)
        
        if let imageData = image.jpeg(.secondLowest){
            
        uploadProfilePicture(imageData: imageData)
//        tableView.reloadData()
//        isImageSelected = true
        dismiss(animated:true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
            dismiss(animated:true, completion: {
                
                let cropViewController = CropViewController(image: image)
                cropViewController.delegate = self
                cropViewController.aspectRatioPickerButtonHidden = true
//                cropViewController.customAspectRatio = CGSize(width:300, height:300)
                cropViewController.aspectRatioLockEnabled = true
                self.present(cropViewController, animated: true, completion: nil)
                
            })
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let cico = url as URL
        print(cico)
        print(url)
        
        print(url.lastPathComponent)
        
        print(url.pathExtension)
    }
    
}

// MARK: - Text Field Delegates/DataSources

extension QualificationsNExperiencesVC: UITextFieldDelegate{
    
    fileprivate func propertiesForSearchVC(_ searchVC: SearchSubjectViewController, _ cellIndexPath: IndexPath?, _ textField: UITextField) {
        searchVC.section = currentSection
        
        searchVC.row = cellIndexPath?.row
        
        searchVC.textFieldTag = textField.tag
        
        searchVC.qualificationsNExperiencesVC = self
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let pointInTable: CGPoint = textField.convert(textField.bounds.origin, to: self.tableView)
        
        let cellIndexPath = self.tableView.indexPathForRow(at: pointInTable)
        
        currentSection = cellIndexPath?.section
        
        if currentSection == QualificationNExperienceVCSectionType.Experience.rawValue{
            
            if textField.tag == 1{
                
//                self.view.endEditing(true)
//
//                let searchVC = SearchSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
//
//                searchVC.modalPresentationStyle = .overCurrentContext
//
//                propertiesForSearchVC(searchVC, cellIndexPath, textField)
//
//                searchVC.searchFor = schoolType
//
//                self.navigationController?.present(searchVC, animated: true, completion: nil)
                
            }else if textField.tag == 2{
                
//                self.view.endEditing(true)
//
//                let searchVC = SearchSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
//
//                searchVC.modalPresentationStyle = .overCurrentContext
//
//                propertiesForSearchVC(searchVC, cellIndexPath, textField)
//
//                searchVC.searchFor = subjectType
//
//                self.navigationController?.present(searchVC, animated: true, completion: nil)
                
            }else if textField.tag == 3 {
                
                currentRow = 3
                
                fromYearTextField = textField
                
            }else if textField.tag == 4 {
                
                 currentRow = 4
                
                 toYearTextField = textField
                
            }
            
        }else if currentSection == QualificationNExperienceVCSectionType.Education.rawValue{
            if textField.tag == 1{
                
//                self.view.endEditing(true)
//
//                let searchVC = SearchSubjectViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
//
//                searchVC.modalPresentationStyle = .overCurrentContext
//
//                propertiesForSearchVC(searchVC, cellIndexPath, textField)
//
//                searchVC.searchFor = schoolType
//
//                self.navigationController?.present(searchVC, animated: true, completion: nil)
                
            }else if textField.tag == 3 {
                
                currentRow = 3
                
                fromYearTextField = textField
                
            }else if textField.tag == 4 {
                
                currentRow = 4
                
                toYearTextField = textField
                
            }
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let pointInTable: CGPoint = textField.convert(textField.bounds.origin, to: self.tableView)
        
        let cellIndexPath = self.tableView.indexPathForRow(at: pointInTable)
        
        let currentSection = cellIndexPath?.section
        
        if let indexPath = cellIndexPath?.row{
            
            updateDataIntoTextField(textField, fromCell: false, index: indexPath, section: currentSection!)
            
        }

    }
    
}

// MARK: - Table View Delegates/Datasources
extension QualificationsNExperiencesVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            
            let label = SelectQualificationLabel()

            label.text = "Select Your Qualifications"
            
            label.translatesAutoresizingMaskIntoConstraints = false
            
            let containerView = UIView()
            containerView.backgroundColor = .white
            containerView.addSubview(label)
            
           // containerView.leadingAnchor.constraint(equalTo: label.leadingAnchor, constant: 30).isActive = true
            label.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 30).isActive = true
           // label.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
            label.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
            
            
            
            return containerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 50
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return section0RowCount
        }else if section == 1{
            return section1RowCount
        }else if section == 2{
            return section2RowCount
        }else{
            return section3RowCount
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case  QualificationNExperienceVCSectionType.SelectYourQualifications.rawValue :
            
            if indexPath.row == 0{
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: QualificationsListTableViewCell.cellIdentifire(), for: indexPath)  as? QualificationsListTableViewCell else{
                    return UITableViewCell()
                }
                
                cell.configureCell(with: indexPath.row, section: indexPath.section, row: indexPath.row, qualification: self.userQualification[indexPath.row], title: sectionTitles[indexPath.section])
                
                cell.selectedQualificationProperty = { [weak self] section, row, status, qualification,counter in
                    self?.didTapQualificationCell(section, row, status, qualification ?? nil, counter)
                }
                
                cell.counterValue = { [weak self] counter in
                    self?.userQualification[indexPath.row].totalExperience = counter
                    self?.user.totalExperience = counter
                }
                return cell
                
            }else{
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: QualificationListWithoutExperienceTableViewCell.cellIdentifire(), for: indexPath)  as? QualificationListWithoutExperienceTableViewCell else{
                    return UITableViewCell()
                }
                
                cell.configureCell(qualification: self.userQualification[indexPath.row], row: indexPath.row, section: indexPath.section)
                
                cell.selectedQualificationProperty = { [weak self]  status,qualification,row,section in
                    self?.didTapQualificationWithoutExperienceCell(status,qualification ?? nil)
                }
                
                return cell
                
            }
            
            
        case  QualificationNExperienceVCSectionType.Experience.rawValue :
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonExperienceNEducationTableViewCell.cellIdentifire(), for: indexPath) as? CommonExperienceNEducationTableViewCell else{
                return UITableViewCell()
            }
            
            cell.textField1.delegate = self
            cell.textField2.delegate = self
            cell.textField3.delegate = self
            cell.textField3.inputView = yearPicker
            cell.textField4.delegate = self
            cell.textField4.inputView = yearPicker
            
            if userExperience.count > 0{
                cell.configureCellWithExperience(with: userExperience[indexPath.row], sectionTitles[indexPath.section], indexPath.row+1, lastIndex: section1RowCount, section: indexPath.section, from: CellType.Experience.description() )
            } else{
                userExperience.append(Experience())
                cell.configureCellWithExperience(with: userExperience[indexPath.row], sectionTitles[indexPath.section], indexPath.row+1, lastIndex: 1, section: indexPath.section, from: CellType.Experience.description())
            }
            
            updateDataIntoTextField(cell.textField1, fromCell: true, index: indexPath.row, section: indexPath.section)
            updateDataIntoTextField(cell.textField2, fromCell: true, index: indexPath.row, section: indexPath.section)
            updateDataIntoTextField(cell.textField3, fromCell: true, index: indexPath.row, section: indexPath.section)
            updateDataIntoTextField(cell.textField4, fromCell: true, index: indexPath.row, section: indexPath.section)
            
            cell.did_Tap_Add_Remove_Experience = { [weak self] identifire, section, row, from in
                self?.didTapAddremoveExperience(identifire, section, row, from)
            }
            return cell
            
        case  QualificationNExperienceVCSectionType.Education.rawValue :
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CommonExperienceNEducationTableViewCell.cellIdentifire(), for: indexPath) as? CommonExperienceNEducationTableViewCell else{ return UITableViewCell() }
            
            cell.textField1.delegate = self
            cell.textField2.delegate = self
            cell.textField3.delegate = self
            cell.textField3.inputView = yearPicker
            cell.textField4.delegate = self
            cell.textField4.inputView = yearPicker
            
            if userEducations.count > 0{
                
                cell.configureCellWithEducation(with: userEducations[indexPath.row], "Education".localized, indexPath.row+1, lastIndex: section2RowCount, section: indexPath.section, from: CellType.Education.description())
                
            }else{
                
                userEducations.append(Education())
                
                cell.configureCellWithEducation(with: userEducations[indexPath.row], "Education".localized, indexPath.row+1, lastIndex: 1, section: indexPath.section, from: CellType.Education.description())
                
            }
            
            updateDataIntoTextField(cell.textField1, fromCell: true, index: indexPath.row, section: indexPath.section)
            updateDataIntoTextField(cell.textField2, fromCell: true, index: indexPath.row, section: indexPath.section)
            updateDataIntoTextField(cell.textField3, fromCell: true, index: indexPath.row, section: indexPath.section)
            updateDataIntoTextField(cell.textField4, fromCell: true, index: indexPath.row, section: indexPath.section)
            
            cell.did_Tap_Add_Remove_Experience = { [weak self] identifire, section, row, from in
                self?.didTapAddremoveExperience(identifire, section, row, from)
            }
            
            return cell
            
        case  QualificationNExperienceVCSectionType.Certificates.rawValue :
            
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: AttachDocumentsTableViewCell.cellidentifire(), for: indexPath) as! AttachDocumentsTableViewCell
                cell.delegate = self
                cell.configureCell(sectionTitles[indexPath.section])
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: DocumentsTableViewCell.cellIdentifire(), for: indexPath) as! DocumentsTableViewCell
            
            cell.onDidTapDocumentsCell = { [weak self]  image in
                print(image)
                self?.didTapDocument(image)
            }
            
            cell.configureCell(with: documentImage)
            
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == QualificationNExperienceVCSectionType.Certificates.rawValue {
            
            if indexPath.row == 0{
                self.showImageOptions()
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return self.qualificationCellSelectedStatus == true ? 180 : 55
            } else{
                return 55
            }
        }else{
            return UITableView.automaticDimension
        }
    }
    
}


// MARK: - Helping Functions
extension QualificationsNExperiencesVC{
    
    ///
    
    func UpdateCellWithSubjectOrSchhol(_ row: Int, _ section: Int, _ name: String, _ textFieldIdentifire: Int){
        
        if section == 1{
            
            if self.userExperience.count > 0{
               
                
                let userExp = self.userExperience[row]
                
                let indexPath = IndexPath(row: row, section: section)
                
                guard let cell = tableView.cellForRow(at: indexPath) as? CommonExperienceNEducationTableViewCell else {return}
                
                if textFieldIdentifire == 1{
                    
                    userExp.instituteName = name
                    
                    updateDataIntoTextField(cell.textField1, fromCell: true, index: row, section: section)
                    
                }else if textFieldIdentifire == 2{
                    
                    userExp.subjectName = name
                    
                    updateDataIntoTextField(cell.textField2, fromCell: true, index: row, section: section)
                    
                }
            }
           
        }else if section == 2{
            
            
            let userEdu = self.userEducations[row]
            
            let indexPath = IndexPath(row: row, section: section)
            
            guard let cell = tableView.cellForRow(at: indexPath) as? CommonExperienceNEducationTableViewCell else {return}
            
            if textFieldIdentifire == 1{
                
                userEdu.instituteName = name
                
                updateDataIntoTextField(cell.textField1, fromCell: true, index: row, section: section)
                
            }
        }
        
    }
    
    /// This func allow us to use table view functions in this class
    fileprivate func initialSetUP(){
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    /// This func allow us to use all xib classes in this class
    fileprivate func registerCell() {
        tableView.register(UINib(nibName: QualificationsListTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: QualificationsListTableViewCell.cellIdentifire())
        tableView.register(UINib(nibName: CommonExperienceNEducationTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: CommonExperienceNEducationTableViewCell.cellIdentifire())
        tableView.register(UINib(nibName: QualificationListWithoutExperienceTableViewCell.cellIdentifire(), bundle: nil), forCellReuseIdentifier: QualificationListWithoutExperienceTableViewCell.cellIdentifire())
    }
    
    fileprivate func yearSetUP() {
        
        yearPicker.delegate = self
        
        toYear = calendar.component(.year, from: date)
        
        fromYear = toYear - 49
        
        for year in fromYear...toYear{
            self.year.append(year)
        }
        
        var year_arr = self.year.map {$0}
        
        year_arr = year_arr.sorted{$0 > $1}
        
        newYearArr = year_arr.map { String("\($0)")}
        
        
    }
    
    private func updateUI(){
        
        bottomContainerView.layer.borderWidth = 1
        bottomContainerView.layer.borderColor = staticMessageCellBorderColor.cgColor
        
        buttomContainerView.layer.cornerRadius = 5
        buttomContainerView.clipsToBounds = true
        
    }
    
}

// MARK: --------- Picker view delegate

extension QualificationsNExperiencesVC: UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == yearPicker{
            return self.newYearArr.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == yearPicker{
            return newYearArr[row]
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == yearPicker{
            
            if let section = currentSection, section == 1, let _row = currentRow {
                if _row == 3 {
                     fromYearTextField.text = newYearArr[row]
                }else{
                    toYearTextField.text = newYearArr[row]
                }
            }else if let section = currentSection, section == 2 , let _row = currentRow{
                if _row == 3 {
                    fromYearTextField.text = newYearArr[row]
                }else{
                    toYearTextField.text = newYearArr[row]
                }
            }
           
            
        }
        
    }
}

// MARK: - API Implementation

extension QualificationsNExperiencesVC{
    
    // [START upload image to server]
    func uploadProfilePicture(imageData: Data) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let url =   "\(baseUrl)\(PathURl.userDocumentUpload.rawValue)"
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Accept": "application/json",
            "Authorization": "Bearer \(UserStore.shared.token)"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            let fileName = "doc_file.png"
            
            let mimeType = imageData.mimeType
            
            multipartFormData.append(imageData, withName: "doc_file", fileName: fileName, mimeType: mimeType)
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            
            switch result{
                
            case .success(let upload, _, _):
                
                    upload.responseJSON { response in
                   
                        DispatchQueue.main.async(execute: {
                            
                            Utilities.hideHUD(forView: self.view)
                            
                        })
                                                                
                    if let _ = response.error{
                        
                        self.showSimpleAlertWithMessage("There seems to be an error. Please try again later.".localized)
                        
                        return
                        
                        }
                                                            
                        let json = try? JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                                                            
                        if let responseDict = json as? [String: AnyObject], let message = responseDict["message"] as? String {
                                                                                                
                        
                            if let data = responseDict["data"] as? [String: AnyObject] {
                                                                                                    
                                if let imageUrls = (data["attachement_file"] as? String){
                                                                                                        
                                    self.imageURLsArr.append(imageUrls)
                                    
                                }
                            
                            }
                        
                        self.showSimpleAlertWithMessage(message)
                        
                    }
                }
            case .failure(let error):
                
                DispatchQueue.main.async(execute: {
                    
                    Utilities.hideHUD(forView: self.view)
                                                    
                    if let error_ = error as? ResponseError {
                                                        
                        self.showSimpleAlertWithMessage(error_.description())
                        
                     } else {
                                                        
                        if !error.localizedDescription.isEmpty {
                                                            
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                            
                            }
                        
                        }
                    })
                
                }
            }
    }
    // [END upload image to server]
    
     // [START save data to server]
    func addQualificationNExperience(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        if  isDataValid() == false{
            return
        }
        
        if self.userQualification.count > 0{
            let moe_qua = self.userQualification.filter{$0.id == "1"}
            if moe_qua.count > 0{
                user.totalExperience = moe_qua[0].totalExperience
            }
        }
        var params = user.getQualificationsnEducations()
        
//        if userQualification.count > 0{
//            let qualificationWithExperience = userQualification.filter{$0.id == }
//        }
        
        let educationParams = getEducations()
        let experienceParams = getExperience()
        params["attachement_file"] = self.imageURLsArr
        params["educations"] = educationParams
        params["experience"] = experienceParams
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let loginObserver = ApiManager.shared.apiService.addTutorQualificationNExperiences(params as [String : AnyObject])
        
        let loginDisposable = loginObserver.subscribe(onNext: {(user) in
            
                                                            DispatchQueue.main.async{
                                                                                                    
                                                                Utilities.hideHUD(forView: self.view)
                                                                
                                                                self.tempQualification.removeAll()
                                                                                                    
                                                                AppDelegateConstant.user = user
                                                                                                    
                                                                let commonPopUP = ShowInfoCommonPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                                                                                                    
                                                                commonPopUP.fromController = self
                                                                                                    
                                                                commonPopUP.showAlert = true
                                                                                                    
                                                                self.navigationController?.present(commonPopUP, animated: true, completion: nil)
                                                                
                                                            }
            
                                                    }, onError: {(error) in
                                                                                                    
                                                            DispatchQueue.main.async(execute: {
                                                                                                        
                                                                Utilities.hideHUD(forView: self.view)
                                                                                                        
                                                                if let error_ = error as? ResponseError {
                                                                                                            
                                                                    self.showSimpleAlertWithMessage(error_.description())
                                                                                                            
                                                                } else {
                                                                                                            
                                                                     if !error.localizedDescription.isEmpty {
                                                                                                                
                                                                    self.showSimpleAlertWithMessage(error.localizedDescription)
                                                                                                                
                                                                    }
                                                                                                            
                                                                }
                                                                                                        
                                                        })
                                                                                                    
                                                    })
        
        loginDisposable.disposed(by: disposableBag)
    }
     // [END save data to server]
}

extension QualificationsNExperiencesVC: AttachDocumentsTableViewCellDelegate{
    
    func popTipTapped(senderFrame: CGRect, cell: AttachDocumentsTableViewCell,btn:UIButton) {
        
        self.showPopToolTipForButton(btn, popMessage: ToolTipString.info.description())
        
        if btn.isSelected == false{
            btn.isSelected = true
            btn.setImage(UIImage(named: "infoImageSelected"), for: .normal)
        }else{
            btn.isSelected = false
            let image = #imageLiteral(resourceName: "info_gray")
            btn.setImage(image, for: .normal)
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.hidePopToolTipForButton()
    }
    
}
