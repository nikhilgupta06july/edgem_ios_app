//
//  UpdateProfileViewController.swift
//  Edgem
//
//  Created by Phaninder Kumar on 31/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift
import GooglePlaces
import CoreLocation

enum UpdateProfileTextFieldType: Int {
    case firstName = 0, lastName,  gender, birthday, emailAddress, contactNumber
}
enum UpdateParentProfileTextFieldType: Int{
    case parentFirstName = 0, parentLastName,  relationship, parentEmail, parentContactNumber
}
enum UpdateProfileAddressTextFieldType: Int{
    case postalCode = 0, addressLine1,  addressLine2
}

class UpdateProfileViewController: BaseViewController {

    @IBOutlet weak var headerTitle: UILabel!{
        didSet{
            headerTitle.text = GeneralString.personalInformation.description()
        }
    }
    @IBOutlet weak var tabButtonOne: UIButton!
    @IBOutlet weak var tabButtonTwo: UIButton!
    @IBOutlet weak var tabButtonThree: UIButton!
    @IBOutlet weak var tabLineImg: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var saveBtn: UIButton!
    
    var currentIndex = 1
    var userData: User!
    var editedUser: User!
    var disposableBag = DisposeBag()
        let genderPickerView = UIPickerView()
    let datePicker = UIDatePicker()
    var activeTextField: UITextField!
    let countryCodePicker = UIPickerView()
    
    var parentTitle = ["First Name*", "Last Name*", "Relationship*", "Email*", "Parent's Contact Number*"]
    var childTitles = ["First Name*", "Last Name*", "Gender*", "Birthday*", "Email*", "Child's Contact Number"]
    var userTitles = ["First Name*", "Last Name*", "Gender*", "Birthday*", "Email*", "\(UserStore.shared.selectedUserType)'s Contact Number*"]
    var addressTitles = ["Postal code*", "Address line 1*", "Address line 2"]
    var countryCodeOptions = ["+65"]
    
    //MARK:- Life Cycle Function
    override func viewDidLoad() {
        super.viewDidLoad()
        if let _user = AppDelegateConstant.user{
            userData = _user
        }
        editedUser = User.init(userData)
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
        
        setupViewAccording(UserStore.shared.selectedUserType)
        initialSetUpForCountryCodePicker()
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        navigateBack(sender: sender)
    }
    
    @IBAction func tabButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        setTabButtons(sender.tag)
        showTransitionInView(sender.tag)
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        switch UserStore.shared.selectedUserType {
        case parentType:
            if currentIndex == 1{
                let validationInformation = isParentProfileDataValid()
                guard validationInformation.isValid else {
                    self.showSimpleAlertWithMessage(validationInformation.message)
                    return
                }
            }else if currentIndex == 2{
                let validationInformation = isChildProfileDataValid()
                guard validationInformation.isValid else {
                    self.showSimpleAlertWithMessage(validationInformation.message)
                    return
                }
            }else{
                let validationInformation = isAddressDataValid()
                guard validationInformation.isValid else {
                    self.showSimpleAlertWithMessage(validationInformation.message)
                    return
                }
            }
        default:
            if currentIndex == 1{
                let validationInformation = isProfileDataValid()
                guard validationInformation.isValid else {
                    self.showSimpleAlertWithMessage(validationInformation.message)
                    return
                }
            }else{
                let validationInformation = isAddressDataValid()
                guard validationInformation.isValid else {
                    self.showSimpleAlertWithMessage(validationInformation.message)
                    return
                }
            }
        }
//        updateUserInfoData()
    }
    
    //MARK:- Helping Function
    func setupViewAccording(_ userType: String){
        
        tabButtonThree.isHidden = userType != parentType
        if userType == parentType{
         tabButtonOne.setTitle(GeneralString.parent.description() , for: .normal)
         tabButtonTwo.setTitle(GeneralString.child.description() , for: .normal)
         tabButtonThree.setTitle(GeneralString.address.description() , for: .normal)
        }else if userType == studentType{
            tabButtonOne.setTitle(GeneralString.student.description() , for: .normal)
            tabButtonTwo.setTitle(GeneralString.address.description() , for: .normal)
        }else{
            tabButtonOne.setTitle(GeneralString.tutor.description() , for: .normal)
            tabButtonTwo.setTitle(GeneralString.address.description() , for: .normal)
        }
        setTabButtons(currentIndex)
    }
    
    func setTabButtons(_ forIndex: Int){
        
        tabButtonOne.titleLabel?.font = UIFont(name: forIndex == 1 ? QuicksandBold : QuicksandRegular, size: 18)
        tabButtonTwo.titleLabel?.font = UIFont(name: forIndex == 2 ? QuicksandBold : QuicksandRegular, size: 18)
        tabButtonThree.titleLabel?.font = UIFont(name: forIndex == 3 ? QuicksandBold : QuicksandRegular, size: 18)
        
    }
    
    func showTransitionInView(_ forIndex: Int){
        
        let movingX = forIndex<currentIndex ? ScreenWidth : -ScreenWidth
        let lineMovingX = CGFloat(forIndex-1)*(ScreenWidth/(UserStore.shared.selectedUserType != parentType ? 2 : 3))
        
       currentIndex = forIndex
        
       UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
        
        self.tableView.transform = CGAffineTransform.identity.translatedBy(x:movingX, y: 0)
        self.tabLineImg.transform = CGAffineTransform.identity.translatedBy(x:lineMovingX, y: 0)
       
        
        },completion: { finished in
            
          self.tableView.transform = CGAffineTransform.identity.translatedBy(x:0, y: 0)
             self.tableView.reloadData()
            
        })
        
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        //activeTextField.text = Utilities.shared.getFormattedDate(sender.date, dateFormat: "dd-MMM-yyyy")
        activeTextField.text = Utilities.getFormattedDate(sender.date, dateFormat: "dd-MMM-yyyy")
    }
    
}

extension UpdateProfileViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch UserStore.shared.selectedUserType {
        case parentType:
            return currentIndex == 1 ? 5 : currentIndex == 2 ? 6 : currentIndex == 3 ? 3 : 0
        case studentType:
            return currentIndex == 1 ? 6 : currentIndex == 2 ? 3 : 0
        case tutorType:
            return currentIndex == 1 ? 6 : currentIndex == 2 ? 3 : 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch UserStore.shared.selectedUserType {
        case parentType:
            if currentIndex == 1{
                return indexPath.row == parentTitle.count-1 ? LoadEdgemDualTextFieldTableViewCell(indexPath, parentTitle[indexPath.row]) : LoadEdgemTextFieldTableViewCell(indexPath, parentTitle[indexPath.row])
            }else if currentIndex == 2{
                return indexPath.row == childTitles.count-1 ? LoadEdgemDualTextFieldTableViewCell(indexPath, childTitles[indexPath.row]) : LoadEdgemTextFieldTableViewCell(indexPath, childTitles[indexPath.row])
            }else{
              return LoadEdgemTextFieldTableViewCell(indexPath, addressTitles[indexPath.row])
            }
        default:
            if currentIndex == 1{
                return indexPath.row == userTitles.count-1 ? LoadEdgemDualTextFieldTableViewCell(indexPath, userTitles[indexPath.row])  : LoadEdgemTextFieldTableViewCell(indexPath, userTitles[indexPath.row])
            }else{
                return LoadEdgemTextFieldTableViewCell(indexPath, addressTitles[indexPath.row])
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func LoadEdgemTextFieldTableViewCell(_ indexPath: IndexPath, _ titleText: String) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EdgemTextFieldTableViewCell.cellIdentifier(), for: indexPath) as? EdgemTextFieldTableViewCell  else{return UITableViewCell()}
        cell.configureUpdateProfileCellForRowAtIndex(indexPath.row, withText: titleText, currentIndex: currentIndex)
        
        switch UserStore.shared.selectedUserType {
        case parentType:
            if currentIndex == 1{
                updateDataIntoParentProfileTextField(cell.customTextField, fromCell: true)
                configureParentProfileTextField(cell.customTextField)
            }else if currentIndex == 2{
                updateDataIntoChildProfileTextField(cell.customTextField, fromCell: true)
                configureProfileTextField(cell.customTextField)
            }else{
                updateDataIntoProfileAddressTextField(cell.customTextField, fromCell: true)
                configureProfileAddressTextField(cell.customTextField)
            }
        default:
            if currentIndex == 1{
                updateDataIntoProfileTextField(cell.customTextField, fromCell: true)
                configureProfileTextField(cell.customTextField)
            }else{
                updateDataIntoProfileAddressTextField(cell.customTextField, fromCell: true)
                configureProfileAddressTextField(cell.customTextField)
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
    func LoadEdgemDualTextFieldTableViewCell(_ indexPath: IndexPath, _ titleText: String) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: EdgemDualTextFieldTableViewCell.cellIdentifier(), for: indexPath) as! EdgemDualTextFieldTableViewCell
        cell.configureUpdateProfileCellForRowAtIndex(title: ["+65",titleText], index: indexPath.row)
        
        switch UserStore.shared.selectedUserType {
        case parentType:
            if currentIndex == 1{
                updateDataIntoParentProfileTextField(cell.textFieldOne, fromCell: true)
                updateDataIntoParentProfileTextField(cell.textFieldTwo, fromCell: true)
                
                configureParentProfileTextField(cell.textFieldOne)
                configureParentProfileTextField(cell.textFieldTwo)
            }else if currentIndex == 2{
                updateDataIntoChildProfileTextField(cell.textFieldOne, fromCell: true)
                updateDataIntoChildProfileTextField(cell.textFieldTwo, fromCell: true)
                configureProfileTextField(cell.textFieldOne)
                configureProfileTextField(cell.textFieldTwo)
            }
        default:
            if currentIndex == 1{
                updateDataIntoProfileTextField(cell.textFieldOne, fromCell: true)
                updateDataIntoProfileTextField(cell.textFieldTwo, fromCell: true)
                configureProfileTextField(cell.textFieldOne)
                configureProfileTextField(cell.textFieldTwo)
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    
    //MARK:- Update Data in fields
    func updateDataIntoParentProfileTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
        
        switch customTextField.tag {
        case UpdateParentProfileTextFieldType.parentFirstName.rawValue:
            fromCell ? (customTextField.text = editedUser.firstName) : (editedUser.firstName = customTextField.text!)
            break
        case UpdateParentProfileTextFieldType.parentLastName.rawValue:
            fromCell ? (customTextField.text = editedUser.lastName) : (editedUser.lastName = customTextField.text!)
            break
        case UpdateParentProfileTextFieldType.relationship.rawValue:
            fromCell ? (customTextField.text = editedUser.relationwithchild) : (editedUser.relationwithchild = customTextField.text!)
            break
        case UpdateParentProfileTextFieldType.parentEmail.rawValue:
            fromCell ? (customTextField.text = editedUser.email) : (editedUser.email = customTextField.text!)
            break
        case UpdateParentProfileTextFieldType.parentContactNumber.rawValue:
            if customTextField.title?.contains("Contact") ?? false {
                fromCell ? (customTextField.text = editedUser.contactNumber) : (editedUser.contactNumber = customTextField.text!)
            } else {
                fromCell ? (customTextField.text = editedUser.countryContactCode) : (editedUser.countryContactCode = customTextField.text!)
            }
            
            break
        default:
            break
        }
    }
    
    func updateDataIntoChildProfileTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
        
        switch customTextField.tag {
        case UpdateProfileTextFieldType.firstName.rawValue:
            fromCell ? (customTextField.text = editedUser.firstName) : (editedUser.firstName = customTextField.text!)
            break
        case UpdateProfileTextFieldType.lastName.rawValue:
            fromCell ? (customTextField.text = editedUser.lastName) : (editedUser.lastName = customTextField.text!)
            break
        case UpdateProfileTextFieldType.gender.rawValue:
            fromCell ? (customTextField.text = editedUser.gender) : (editedUser.gender = customTextField.text!)
            break
        case UpdateProfileTextFieldType.birthday.rawValue:
            fromCell ? (customTextField.text = editedUser.birthday) : (editedUser.birthday = customTextField.text!)
            break
        case UpdateProfileTextFieldType.emailAddress.rawValue:
            fromCell ? (customTextField.text = editedUser.email) : (editedUser.email = customTextField.text!)
            break
        case UpdateProfileTextFieldType.contactNumber.rawValue:
            if customTextField.title?.contains("Contact") ?? false {
               fromCell ? (customTextField.text = editedUser.contactNumber) : (editedUser.contactNumber = customTextField.text!)
            } else {
               fromCell ? (customTextField.text = editedUser.countryContactCode) : (editedUser.countryContactCode = customTextField.text!)
            }
            
            break
        default:
            break
        }
    }
    
    func updateDataIntoProfileTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
        
        switch customTextField.tag {
        case UpdateProfileTextFieldType.firstName.rawValue:
            fromCell ? (customTextField.text = editedUser.firstName) : (editedUser.firstName = customTextField.text!)
            break
        case UpdateProfileTextFieldType.lastName.rawValue:
            fromCell ? (customTextField.text = editedUser.lastName) : (editedUser.lastName = customTextField.text!)
            break
        case UpdateProfileTextFieldType.gender.rawValue:
            fromCell ? (customTextField.text = editedUser.gender) : (editedUser.gender = customTextField.text!)
            break
        case UpdateProfileTextFieldType.birthday.rawValue:
            fromCell ? (customTextField.text = editedUser.birthday) : (editedUser.birthday = customTextField.text!)
            break
        case UpdateProfileTextFieldType.emailAddress.rawValue:
            fromCell ? (customTextField.text = editedUser.email) : (editedUser.email = customTextField.text!)
            break
        case UpdateProfileTextFieldType.contactNumber.rawValue:
            if customTextField.title?.contains("Contact") ?? false {
               fromCell ? (customTextField.text = editedUser.contactNumber) : (editedUser.contactNumber = customTextField.text!)
            } else {
                fromCell ? (customTextField.text = editedUser.countryContactCode) : (editedUser.countryContactCode = customTextField.text!)
            }
            
            break
        default:
            break
        }
    }
    
    func updateDataIntoProfileAddressTextField(_ customTextField: EdgemCustomTextField, fromCell: Bool) {
        
        let address = editedUser.userAddress
        switch customTextField.tag {
        case UpdateProfileAddressTextFieldType.postalCode.rawValue:
            fromCell ? (customTextField.text = address?[0].postalCode) : (address?[0].postalCode = customTextField.text!)
            break
        case UpdateProfileAddressTextFieldType.addressLine1.rawValue:
            fromCell ? (customTextField.text = address?[0].addressLine1) : (address?[0].addressLine1 = customTextField.text!)
            break
        case UpdateProfileAddressTextFieldType.addressLine2.rawValue:
            fromCell ? (customTextField.text = address?[0].addressLine2) : (address?[0].addressLine2 = customTextField.text!)
            break
        default:
            break
        }
    }
    
    //MARK:- Configure text fields
    func configureParentProfileTextField(_ customTextField: EdgemCustomTextField) {
        customTextField.inputView = nil
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        customTextField.hideImage()
        switch customTextField.tag {
        case UpdateParentProfileTextFieldType.parentFirstName.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.textFont = textFieldDefaultFont
            break
        case UpdateParentProfileTextFieldType.parentLastName.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.textFont = textFieldDefaultFont
            break
        case UpdateParentProfileTextFieldType.relationship.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.textFont = textFieldDefaultFont
            break
        case UpdateParentProfileTextFieldType.parentEmail.rawValue:
            customTextField.delegate = self
            customTextField.isUserInteractionEnabled = true
            customTextField.keyboardType = .emailAddress
            break
        case UpdateParentProfileTextFieldType.parentContactNumber.rawValue:
            customTextField.delegate = self
            customTextField.isUserInteractionEnabled = true
            if customTextField.title?.contains("Contact") ?? false{
                customTextField.keyboardType = .phonePad
            } else{
                customTextField.inputView = countryCodePicker
                customTextField.icon = "drop-down-arrow"
                customTextField.showImage()
                customTextField.keyboardType = .phonePad
                customTextField.tintColor = UIColor.clear
            }
            break
        default:
            break
        }
    }
    
    func configureProfileTextField(_ customTextField: EdgemCustomTextField) {
        customTextField.inputView = nil
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        switch customTextField.tag {
        case UpdateProfileTextFieldType.firstName.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.textFont = textFieldDefaultFont
            break
        case UpdateProfileTextFieldType.lastName.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.textFont = textFieldDefaultFont
            break
        case UpdateProfileTextFieldType.gender.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.textFont = textFieldDefaultFont
            break
        case UpdateProfileTextFieldType.birthday.rawValue:
            customTextField.isUserInteractionEnabled = false
            customTextField.textFont = textFieldDefaultFont
            break
        case UpdateProfileTextFieldType.emailAddress.rawValue:
            customTextField.delegate = self
            customTextField.isUserInteractionEnabled = true
            customTextField.keyboardType = .emailAddress
            break
        case UpdateProfileTextFieldType.contactNumber.rawValue:
            customTextField.delegate = self
            customTextField.isUserInteractionEnabled = true
            if customTextField.title?.contains("Contact") ?? false{
                customTextField.keyboardType = .phonePad
            } else{
                customTextField.inputView = countryCodePicker
                customTextField.icon = "drop-down-arrow"
                customTextField.showImage()
                customTextField.keyboardType = .phonePad
                customTextField.tintColor = UIColor.clear
            }
            break
        default:
            break
        }
    }
    
    
    func configureProfileAddressTextField(_ customTextField: EdgemCustomTextField) {
        customTextField.delegate = self
        customTextField.isUserInteractionEnabled = true
        customTextField.inputView = nil
        customTextField.autocorrectionType = UITextAutocorrectionType.no
        switch customTextField.tag {
        case UpdateProfileAddressTextFieldType.postalCode.rawValue:
            customTextField.keyboardType = .numberPad
            break
        case UpdateProfileAddressTextFieldType.addressLine1.rawValue:
            customTextField.keyboardType = .asciiCapable
            break
        case UpdateProfileAddressTextFieldType.addressLine2.rawValue:
            customTextField.keyboardType = .asciiCapable
            break
        default:
            break
        }
    }
    
    //MARK:- Checking Data Validation
    func isParentProfileDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        for index in 0..<parentTitle.count {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case 0:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateFirstName(text: editedUser.firstName)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 1:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateLastName(text: editedUser.lastName)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 2:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateRelationWithChild(text: editedUser.relationwithchild)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 3:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateEmail(text: editedUser.email)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 4:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateContact(text: editedUser.contactNumber)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            default:
                break
            }
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }
    
    func isChildProfileDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        for index in 0..<childTitles.count {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case 0:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateFirstName(text: editedUser.firstName)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 1:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateLastName(text: editedUser.lastName)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 2:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateGenderName(text: editedUser.gender)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 3:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateBirthday(text: editedUser.birthday)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 4:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateEmail(text: editedUser.email)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            
            default:
                break
            }
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }
    
    func isProfileDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        for index in 0..<userTitles.count {
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case 0:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateFirstName(text: editedUser.firstName)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 1:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateLastName(text: editedUser.lastName)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 2:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateGenderName(text: editedUser.gender)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 3:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateBirthday(text: editedUser.birthday)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 4:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateEmail(text: editedUser.email)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 5:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell{
                    cell.validateContact(text: editedUser.contactNumber)
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            default:
                break
            }
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }
    
    func isAddressDataValid() -> (isValid: Bool, message: String) {
        var errorMessages: [String] = []
        for index in 0..<addressTitles.count {
            let address = editedUser.userAddress
            let currentIndexPath = IndexPath.init(row: index, section: 0)
            switch index {
            case 0:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validatePostalCode(text: address?[0].postalCode ?? "")
                    errorMessages.append(cell.errorLabel.text ?? "")
                }
            case 1:
                if let cell = tableView.cellForRow(at: currentIndexPath) as? EdgemTextFieldTableViewCell {
                    cell.validateAddress1(text: address?[0].addressLine1 ?? "")
                    errorMessages.append(cell.errorLabel.text ?? "")
                }

            default:
                break
            }
        }
        let filteredErrorMessages = errorMessages.filter { (message) -> Bool in
            message.count > 0
        }
        tableView.beginUpdates()
        tableView.endUpdates()
        let isValidData = filteredErrorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : filteredErrorMessages[0])
    }

}

extension UpdateProfileViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as! EdgemCustomTextField
        if  activeTextField.text?.contains("+") == true{
            managePickerView()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch UserStore.shared.selectedUserType {
        case parentType:
            if currentIndex == 1{
                updateDataIntoParentProfileTextField(textField as! EdgemCustomTextField, fromCell: false)
            }else if currentIndex == 2{
                updateDataIntoChildProfileTextField(textField as! EdgemCustomTextField, fromCell: false)
            }else{
                updateDataIntoProfileAddressTextField(textField as! EdgemCustomTextField, fromCell: false)
            }
        default:
            if currentIndex == 1{
                updateDataIntoProfileTextField(textField as! EdgemCustomTextField, fromCell: false)
            }else{
                updateDataIntoProfileAddressTextField(textField as! EdgemCustomTextField, fromCell: false)
            }
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
}

// MARK: UIPickerView Delegate/Datasource

extension UpdateProfileViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    
    func initialSetUpForCountryCodePicker(){
        countryCodePicker.delegate = self
        countryCodePicker.dataSource = self
        countryCodePicker.tag = 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        let textField = activeTextField as! EdgemCustomTextField
        countryCodePicker.reloadAllComponents()
        countryCodePicker.reloadInputViews()
        if textField.tag == 1 {
            textField.inputView = countryCodePicker
            let index = countryCodeOptions.index(of: editedUser.countryContactCode)
            if let _index = index {
                self.countryCodePicker.selectRow(_index, inComponent: 0, animated: true)
                activeTextField.text = countryCodeOptions[_index]
            } else {
                countryCodePicker.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = countryCodeOptions[0]
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryCodeOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countryCodeOptions[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        activeTextField.text = countryCodeOptions[row]
    }
    
    
}

extension UpdateProfileViewController{
    
    func updateUserInfoData(){
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        //            let userId = _userID
        //            let password = _editedUser.password
        let params = ["user_id" : UserStore.shared.userID ?? 0,
                      "first_name" : "trrut"] as [String:AnyObject]
        
        
        //        user_id:47
        //        last_name:Verma
        //        dob:1996-2-28
        //        relation:Father
        //        first_name_parent:Vimal Prakash
        //        last_name_parent:Verma
        //        email_parent:nayan@hipster-inc.com
        //        mobile_parent:7985953278
        //        addres_line1:Flat No - 66, La-Palace Prem Nagar
        //        lattitude:27.875299
        //        longitude:78.927674
        
        let updateUserInfoObserver = ApiManager.shared.apiService.updateUserInfo(params)
        let updateUserInfoDisposable = updateUserInfoObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            //self.showSimpleAlertWithMessage(message)
            self.tableView.reloadData()
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        updateUserInfoDisposable.disposed(by: disposableBag)
        
    }
}
