//
//  AddNewCardViewController.swift
//  Edgem
//
//  Created by Hipster on 27/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift
import Stripe


protocol AddNewCardViewControllerDelegate: class{
    func getCardDetail(_ cardDetail: StoreUserCard)
}

class AddNewCardViewController: BaseViewController {
    
    // MARK: --------- Properties
    
    //var stripeUtils = StripeUtils()
    
     var previousTextFieldContent: String?
     var previousSelection: UITextRange?
    
     var storeUserCardObject = StoreUserCard()
    
    weak var addNewCardViewControllerDelegate:AddNewCardViewControllerDelegate?
    
     var errorDict = ["CardNumber": "", "CardHolderName": "", "Month": "", "Year": "", "CVV": ""]
    
     var month = [ "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
    
     var year: [Int] = Array()
     var newYearArr: [String] = Array()
    
     let date = Date()
     let calendar = Calendar.current
    
     var fromYear: Int = 0
     var toYear: Int = 0
    
     var activeTextField: UITextField!
    
     var selectedMonth: String?
     var selecetedYear: String?
    
     let monthPicker = UIPickerView()
     let yearPicker = UIPickerView()
    
    var disposableBag = DisposeBag()
    
    // MARK: --------- @IBOutlets
    
    @IBOutlet weak var cardNumberTextField: EdgemCustomTextField!
    @IBOutlet weak var cardNumberErrorLabel: UILabel!
    @IBOutlet weak var cardHolderNameTextField: EdgemCustomTextField!
    @IBOutlet weak var cardHolderNameErrorLabel: UILabel!
    @IBOutlet weak var MMTextField: EdgemCustomTextField!
    @IBOutlet weak var YYTextField: EdgemCustomTextField!
  //  @IBOutlet weak var CVVTextField: EdgemCustomTextField!
    @IBOutlet weak var MYCvErrorLabel: UILabel!
    @IBOutlet weak var controllerTitleLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
     @IBOutlet weak var cardHolderView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var addCardBtn: UIButton!
    
    // MARK: --------- View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fromYear = calendar.component(.year, from: date)
        toYear = fromYear + 20
        for year in fromYear...toYear{
            self.year.append(year)
        }
        
        newYearArr = self.year.map { String("\($0)".dropFirst(2))}
    
        initialSetUP()
        createMonthPicker()
        createYearPicker()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
         //updateTextFied(textField as! EdgemCustomTextField)
        if storeUserCardObject.cardNumber == "" || storeUserCardObject.nameOnCard == "" || storeUserCardObject.expiryMonth == "" ||  storeUserCardObject.expiryYear == ""{
            //showHideAddCardButton(false)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        cardHolderView.clipsToBounds = true
        
        containerView.layer.cornerRadius = 4
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.1
        containerView.layer.shadowOffset = CGSize.zero
        containerView.layer.shadowRadius = 4
        //containerView.layer.shadowPath = UIBezierPath(rect: containerView.bounds).cgPath
        //containerView.layer.shouldRasterize = true
        
    }
    
    // MARK: --------- @IBActions
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigateBack(sender: sender)
    }
    
    @IBAction func customTextFieldAction(_ sender: EdgemCustomTextField) {
        if sender.tag == 1{
            
            var targetCursorPosition = 0
            if let startPosition = sender.selectedTextRange?.start {
                targetCursorPosition = sender.offset(from: sender.beginningOfDocument, to: startPosition)
            }
            
            var cardNumberWithoutSpaces = ""
            if let text = sender.text {
                cardNumberWithoutSpaces = self.removeNonDigits(string: text, andPreserveCursorPosition: &targetCursorPosition)
            }
            
            if cardNumberWithoutSpaces.count > 16 {
                sender.text = previousTextFieldContent
                sender.selectedTextRange = previousSelection
                return
            }
            
            let cardNumberWithSpaces = self.insertCreditCardSpaces(cardNumberWithoutSpaces, preserveCursorPosition: &targetCursorPosition)
            sender.text = cardNumberWithSpaces
            
            if let targetPosition = sender.position(from: sender.beginningOfDocument, offset: targetCursorPosition) {
                sender.selectedTextRange = sender.textRange(from: targetPosition, to: targetPosition)
            }
            
            let (type, formatted, valid) = checkCardNumber(input: "\(sender.text!)")
            
            changeCardImage("\(type)")
//            print(type,formatted,valid)
            
        }
        
    }
    
    @IBAction func addBtnTapped(_ sender: UIButton) {
        
        addCard()
        
    }
    
    // MARK: --------- Private Functions
    
    func initialSetUP(){
        
       // datePicker.datePickerMode = .date
       // datePicker.maximumDate = Date()
        //datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
        
        // cardNumberTextField properties
        
        cardNumberTextField.tag = 1
        cardNumberTextField.delegate = self
        cardNumberTextField.placeholder = "Card Number"
        cardNumberTextField.title = "Card Number"
        cardNumberTextField.textFont = textFieldMediumFont
        cardNumberTextField.placeholderFont = textFieldDefaultFont
        cardNumberTextField.titleFont = textFieldDefaultFont
        cardNumberTextField.keyboardType = .numberPad
        
        cardNumberErrorLabel.numberOfLines = 1
        // cardHolderNameTextField properties
        
        cardHolderNameTextField.tag = 2
        cardHolderNameTextField.delegate = self
        cardHolderNameTextField.placeholder = "Card Holder’s Name"
        cardHolderNameTextField.title = "Card Holder’s Name"
        cardHolderNameTextField.textFont = textFieldMediumFont
        cardHolderNameTextField.placeholderFont = textFieldDefaultFont
        cardHolderNameTextField.titleFont = textFieldDefaultFont
        cardHolderNameTextField.keyboardType = .asciiCapable
        
        // MM properties
        
        MMTextField.titleFormatter = { (text: String) -> String in
            return text.uppercased()
        }
        YYTextField.titleFormatter = { (text: String) -> String in
            return text.uppercased()
        }
        
//        CVVTextField.titleFormatter = { (text: String) -> String in
//            return text.uppercased()
//        }
        
        MMTextField.tag = 3
        MMTextField.delegate = self
        MMTextField.placeholder = "MM"
        MMTextField.title = "MM".capitalized
        MMTextField.textFont = textFieldMediumFont
        MMTextField.placeholderFont = textFieldDefaultFont
        MMTextField.titleFont = textFieldDefaultFont
       // MMTextField.inputView = datePicker
        MMTextField.keyboardType = .numberPad
        
        // YY properties
        YYTextField.tag = 4
        YYTextField.delegate = self
        YYTextField.placeholder = "YY"
        YYTextField.title = "YY"
        YYTextField.textFont = textFieldMediumFont
        YYTextField.placeholderFont = textFieldDefaultFont
        YYTextField.titleFont = textFieldDefaultFont
        //YYTextField.inputView = datePicker
        YYTextField.keyboardType = .numberPad
        
        // CVV properties
//        CVVTextField.tag = 5
//        CVVTextField.delegate = self
//        CVVTextField.placeholder = "CVV"
//        CVVTextField.title = "CVV"
//        CVVTextField.textFont = textFieldMediumFont
//        CVVTextField.placeholderFont = textFieldDefaultFont
//        CVVTextField.titleFont = textFieldDefaultFont
//        CVVTextField.keyboardType = .numberPad
//        CVVTextField.isSecureTextEntry = true
        
        
    }
    
    func changeCardImage(_ cardType: String){
        var imageName = ""
        print(value: cardType)
        switch cardType{
        case CardTypes.Visa.description() : imageName = "Visa"
            
        case CardTypes.Amex.description() : imageName = "Amex"
        
        case CardTypes.Diners.description() : imageName = "Diners"
            
        case CardTypes.Discover.description() : imageName = "Discover"
            
        case CardTypes.Elo.description() : imageName = "Elo"
            
        case CardTypes.Hipercard.description() : imageName = "Hipercard"
            
        case CardTypes.JCB.description() : imageName = "JCB"
            
        case CardTypes.MasterCard.description() : imageName = "MasterCard"
            
        case CardTypes.UnionPay.description() : imageName = "UnionPay"
            
        case CardTypes.Meastro.description() : imageName = "Meastro"
            
        case CardTypes.Unknown.description() : imageName = ""
            
        default:
            imageName = ""
        }
      cardImage.image = UIImage(named: imageName)
    }
    
    func removeNonDigits(string: String, andPreserveCursorPosition cursorPosition: inout Int) -> String {
        var digitsOnlyString = ""
        let originalCursorPosition = cursorPosition
        
        for i in Swift.stride(from: 0, to: string.count, by: 1) {
            let characterToAdd = string[string.index(string.startIndex, offsetBy: i)]
            if characterToAdd >= "0" && characterToAdd <= "9" {
                digitsOnlyString.append(characterToAdd)
            }
            else if i < originalCursorPosition {
                cursorPosition -= 1
            }
        }
        
        return digitsOnlyString
    }
    
    func insertCreditCardSpaces(_ string: String, preserveCursorPosition cursorPosition: inout Int) -> String {
        // Mapping of card prefix to pattern is taken from
        // https://baymard.com/checkout-usability/credit-card-patterns
        
        // UATP cards have 4-5-6 (XXXX-XXXXX-XXXXXX) format
        let is456 = string.hasPrefix("1")
        
        // These prefixes reliably indicate either a 4-6-5 or 4-6-4 card. We treat all these
        // as 4-6-5-4 to err on the side of always letting the user type more digits.
        let is465 = [
            // Amex
            "34", "37",
            
            // Diners Club
            "300", "301", "302", "303", "304", "305", "309", "36", "38", "39"
            ].contains { string.hasPrefix($0) }
        
        // In all other cases, assume 4-4-4-4-3.
        // This won't always be correct; for instance, Maestro has 4-4-5 cards according
        // to https://baymard.com/checkout-usability/credit-card-patterns, but I don't
        // know what prefixes identify particular formats.
        let is4444 = !(is456 || is465)
        
        var stringWithAddedSpaces = ""
        let cursorPositionInSpacelessString = cursorPosition
        
        for i in 0..<string.count {
            let needs465Spacing = (is465 && (i == 4 || i == 10 || i == 15))
            let needs456Spacing = (is456 && (i == 4 || i == 9 || i == 15))
            let needs4444Spacing = (is4444 && i > 0 && (i % 4) == 0)
            
            if needs465Spacing || needs456Spacing || needs4444Spacing {
                stringWithAddedSpaces.append(" ")
                
                if i < cursorPositionInSpacelessString {
                    cursorPosition += 1
                }
            }
            
            let characterToAdd = string[string.index(string.startIndex, offsetBy:i)]
            stringWithAddedSpaces.append(characterToAdd)
        }
        
        return stringWithAddedSpaces
    }
    
    func updateTextFied(_ textField: EdgemCustomTextField){
        
        switch textField.tag{
            
        case AddNewCardTextFieldType.CardNumber.rawValue:
            storeUserCardObject.cardNumber = (String(textField.text!.filter { !" \n\t\r".contains($0) }))
            
        case AddNewCardTextFieldType.CardHolderName.rawValue:
             storeUserCardObject.nameOnCard = textField.text!
            
        case AddNewCardTextFieldType.Month.rawValue:
            storeUserCardObject.expiryMonth = textField.text!
            
        case AddNewCardTextFieldType.Year.rawValue:
            storeUserCardObject.expiryYear = textField.text!
            
        case AddNewCardTextFieldType.CVV.rawValue: break
          // print( textField.text!)
            
        default:
            break
        }
        
        if storeUserCardObject.cardNumber == "" || storeUserCardObject.nameOnCard == "" || storeUserCardObject.expiryMonth == "" ||  storeUserCardObject.expiryYear == ""{
            //showHideAddCardButton(false)
        }
        
    }
    
    func showHideAddCardButton(_ status: Bool) {
        
        if status == false{
            addCardBtn.isUserInteractionEnabled = false
            addCardBtn.backgroundColor = theamAppGrayColor//UIColor(white: 1, alpha: 0.60)
        }else{
            addCardBtn.isUserInteractionEnabled = true
            addCardBtn.backgroundColor = theamAppColor//UIColor.white
        }
    }
    
    func isDataValid() -> (isValid: Bool, message: String){

        var errorMessages: [String] = []

        for index in 1...4{

            switch index{
            case AddNewCardTextFieldType.CardNumber.rawValue :
                                                                                                        validateCardNumber(storeUserCardObject.cardNumber)
                                                                                                        errorDict["CardNumber"] = cardNumberErrorLabel.text ?? ""
                                                                                                        let message = cardNumberErrorLabel.text ?? ""
                                                                                                        if !(message.isEmpty){
                                                                                                            errorMessages.append(message)
                                                                                                        }
                
            case AddNewCardTextFieldType.CardHolderName.rawValue :
                                                                                                        validateCardHolderName(storeUserCardObject.nameOnCard)
                                                                                                        errorDict["CardHolderName"] = cardHolderNameErrorLabel.text ?? ""
                                                                                                        let message = cardHolderNameErrorLabel.text ?? ""
                                                                                                        if !(message.isEmpty){
                                                                                                            errorMessages.append(message)
                }
                
            default:
                break
            }
        }
        let isValidData = errorMessages.count == 0
        return (isValid: isValidData, message: isValidData ? "" : errorMessages[0])
    }
    
    
    // Validation on Text Field
    
    func validateCardNumber(_ cardNumber: String){
        
        if cardNumber.isEmpty || cardNumber.count < 13{
             self.cardNumberErrorLabel.text = "Please enter atleast 13 numbers"
        }else if(luhnCheck(number: (String(cardNumber.filter { !" \n\t\r".contains($0) })))) == false{
            self.cardNumberErrorLabel.text = "Please enter valid card number"
        }else{
             self.cardNumberErrorLabel.text = ""
        }
    
    }
    
    // ---- validate card holder name
    func validateCardHolderName(_ cardHolderName: String){
        
        if (cardHolderName.isEmpty){
            self.cardHolderNameErrorLabel.text = "Please enter Card Holder Name".localized
        }else{
             self.cardHolderNameErrorLabel.text = "".localized
        }
        
    }
    
    // ---- validate CVV
    func validateCVV(_ textField: EdgemCustomTextField){
        
        if (textField.text?.isEmpty)!{
            self.MYCvErrorLabel.text = "Please enter CVV".localized
        }else{
            self.MYCvErrorLabel.text = "".localized
        }
        
    }
    
    // create month picker
    func createMonthPicker(){
        monthPicker.delegate = self
        MMTextField.inputView = monthPicker
    }
    
    // create Year picker
    func createYearPicker(){
        yearPicker.delegate = self
        YYTextField.inputView = yearPicker
    }
    
}

 // MARK: --------- Picker view delegate

extension AddNewCardViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        
        let textField = activeTextField as! EdgemCustomTextField
        
        monthPicker.reloadAllComponents()
        monthPicker.reloadInputViews()
        
        yearPicker.reloadAllComponents()
        yearPicker.reloadInputViews()
        
 
        if textField.tag == 3 {
    
            let index = month.index(of: storeUserCardObject.expiryMonth)
            if let _index = index {
                self.monthPicker.selectRow(_index, inComponent: 0, animated: true)
            } else {
                monthPicker.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = month[0]
            }
            
        }else if textField.tag == 4{
            
            let index = newYearArr.index(of: storeUserCardObject.expiryYear)
            if let _index = index {
                self.yearPicker.selectRow(_index, inComponent: 0, animated: true)
            } else {
                yearPicker.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text = newYearArr[0]
            }
            
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == monthPicker{
            return self.month.count
        }else if pickerView == yearPicker{
            return self.newYearArr.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //return month[row]
        if pickerView == monthPicker{
            return month[row]
        }else if pickerView == yearPicker{
            return newYearArr[row]
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == monthPicker{
            
            selectedMonth = month[row]
            MMTextField.text = month[row]
            
        }else if pickerView == yearPicker{
            
            selecetedYear = newYearArr[row]
            YYTextField.text = newYearArr[row]
        }

    }
    
}

 // MARK: --------- Text Field Delegate

extension AddNewCardViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        activeTextField = textField as! EdgemCustomTextField

        if activeTextField.tag == AddNewCardTextFieldType.Month.rawValue ||
            activeTextField.tag == AddNewCardTextFieldType.Year.rawValue{
            managePickerView()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        updateTextFied(textField as! EdgemCustomTextField)
        
        switch textField.tag{
            
        case AddNewCardTextFieldType.CardNumber.rawValue: self.validateCardNumber(textField.text!)
       // print("Card Number")
            
        case AddNewCardTextFieldType.CardHolderName.rawValue: self.validateCardHolderName(textField.text!)
           // print("Card Number")
            
        case AddNewCardTextFieldType.Month.rawValue: break
           // print("Card Number")
            
        case AddNewCardTextFieldType.Year.rawValue: break
           // print("Card Number")
            
        case AddNewCardTextFieldType.CVV.rawValue: self.validateCVV(textField as! EdgemCustomTextField)
            //print("Card Number")
            
        default:
            break
        }
        
        if storeUserCardObject.cardNumber != "" && storeUserCardObject.nameOnCard != "" && storeUserCardObject.expiryMonth != "" && storeUserCardObject.expiryYear != "" {
            //showHideAddCardButton(isDataValid().isValid)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if (textField.text?.isEmpty)! && string == " " {
            return false
        }

        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }

        switch textField.tag{
            case 1:
                        previousTextFieldContent = textField.text;
                        previousSelection = textField.selectedTextRange;
                        return true
            
            case 5: return (textField.text?.count)! < 4
        default:
            return true
        }
    
   }
 
}

extension AddNewCardViewController{
    
    enum CardType: String {
        case Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay, Meastro
        
        static let allCards = [Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay, Meastro]
        
        var regex : String {
            switch self {
            case .Amex:
                return "^3[47][0-9]{5,}$"
            case .Visa:
                return "^4[0-9]{6,}([0-9]{3})?$"
            case .MasterCard:
                return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
            case .Diners:
                return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
            case .Discover:
                return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
            case .JCB:
                return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
            case .UnionPay:
                return "^(62|88)[0-9]{5,}$"
            case .Hipercard:
                return "^(606282|3841)[0-9]{5,}$"
            case .Elo:
                return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
            case .Meastro:
                return "^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}$"
            default:
                return ""
            }
        }
    }
    
    func matchesRegex(regex: String!, text: String!) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let nsString = text as NSString
            let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
            return (match != nil)
        } catch {
            return false
        }
    }
    
    func luhnCheck(number: String) -> Bool {
        var sum = 0
        let digitStrings = number.characters.reversed().map { String($0) }
        
        for tuple in digitStrings.enumerated() {
            guard let digit = Int(tuple.element) else { return false }
            let odd = tuple.offset % 2 == 1
            
            switch (odd, digit) {
            case (true, 9):
                sum += 9
            case (true, 0...8):
                sum += (digit * 2) % 9
            default:
                sum += digit
            }
        }
        
        return sum % 10 == 0
    }
    
    func checkCardNumber(input: String) -> (type: CardType, formatted: String, valid: Bool) {
        // Get only numbers from the input string
        let numberOnly = input.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        
        var type: CardType = .Unknown
        var formatted = ""
        var valid = false
        
        // detect card type
        for card in CardType.allCards {
            if (matchesRegex(regex: card.regex, text: numberOnly)) {
                type = card
                break
            }
        }
        
        // check validity
        valid = luhnCheck(number: numberOnly)
        
        // format
        var formatted4 = ""
        for character in numberOnly.characters {
            if formatted4.characters.count == 4 {
                formatted += formatted4 + " "
                formatted4 = ""
            }
            formatted4.append(character)
        }
        
        formatted += formatted4 // the rest
        
        // return the tuple
        return (type, formatted, valid)
    }
}


// MARK: ------- API Implementation

extension AddNewCardViewController{
    
    func addCard(){
        
       // Initiate the card
        let stripCard = STPCardParams()
        
        // expiry year
         guard var expYear = (storeUserCardObject.expiryYear) else{return}
         expYear = "\(20)\(expYear)"
         guard let expYearInt = UInt(expYear) else {return}
        
         // expiry month
        guard let expMonth = UInt(storeUserCardObject.expiryMonth) else{return}
        
        guard let nameOnCard = storeUserCardObject.nameOnCard else{return}
    
        // Send the card info to Strip to get the token
        stripCard.number = storeUserCardObject.cardNumber
        stripCard.expMonth = expMonth
        stripCard.expYear = expYearInt
        //stripCard.cvc = "123"
        stripCard.name = nameOnCard
       
    
    
         guard let cus_id = AppDelegateConstant.user?.btCustomerID else{return}
        
         Utilities.showHUD(forView: self.view, excludeViews: [])
        
            StripeUtils.shared.createCard(stripeId: cus_id, card: stripCard) { (cardDetail) in
          
                 Utilities.hideHUD(forView: self.view)
                
                if let cardDetail = cardDetail{
                    // getting card data from stripe
                    
                    if let err =  cardDetail["error"] as? [String:AnyObject], let code = err["code"] as? String, code == "card_declined"{
                        guard let msg = err["message"] as? String else{ return  }
                         self.showSimpleAlertWithMessage(msg)
                        return
                    }
                    
                     let  cardData = StoreUserCard(data: cardDetail)
                     cardData.cardNumber = self.storeUserCardObject.cardNumber
                    let alert = UIAlertController(title: AppName, message: "Card successfully added.".localized, preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                        self.addNewCardViewControllerDelegate?.getCardDetail(cardData)
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                }else{
                    if let err = UserStore.shared.stripeError{
                         self.showSimpleAlertWithMessage(err)
                    }else{
                         self.showSimpleAlertWithMessage("Something went wrong!".localized)
                    }
                    
                }

                
        }
        
    }
    
    func serviceToAddCards(_ nonce: String){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
         var filterDict = self.storeUserCardObject.getStoreUserCard()
        filterDict["braintree_nonce"] = nonce
        guard let userID = UserStore.shared.userID else{return}
        filterDict["user_id"] = "\(userID)"
        let addPaymentCardsObserver = ApiManager.shared.apiService.addPaymentCards(filterDict as [String : AnyObject])
        let addPaymentCardsDisposable = addPaymentCardsObserver.subscribe(onNext: {(cardData) in
            
            Utilities.hideHUD(forView: self.view)
            DispatchQueue.main.async {
                //AppDelegateConstant.user?.paymentCard = cardData
                let alert = UIAlertController(title: AppName, message: "User's card information added successfully!".localized, preferredStyle: .alert)
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                    self.addNewCardViewControllerDelegate?.getCardDetail(cardData[0])
                    self.navigationController?.popViewController(animated: true)
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        },onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        }
        )
        addPaymentCardsDisposable.disposed(by: disposableBag)
    }
   
}
