//
//  WebViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 21/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var contactUSBtn: UIView!
    
    var webView: WKWebView!
    var link: URL!
    var titleString: String!

    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: CGRect.init(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight - 66.5), configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        holderView.backgroundColor = UIColor.orange
        holderView.addSubview(webView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        titleLabel.text = titleString ?? ""
        contactUSBtn.isHidden = !(titleLabel.text == "Contact Us")
        
        let myRequest = URLRequest(url: link)
        webView.load(myRequest)
    }
    
    @IBAction func contactUSBtnTapped(_ sender: UIButton) {
        let contactUSViewController = ContactUSViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        navigationController?.pushViewController(contactUSViewController, animated: true)
    }
    

}

extension WebViewController: WKUIDelegate, WKNavigationDelegate {
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
    
}
