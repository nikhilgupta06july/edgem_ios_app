//
//  ContactUSViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 21/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class ContactUSViewController: BaseViewController {
    
    
    // MARK: --------- Properties
    //var subject:[String] = ["General"]
    var contactUS = ContactUS()
    var disposableBag = DisposeBag()
    
    var VC: UIViewController?
    var param: [String : String]?
    
    // MARK: --------- @IBOutlets
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var cancleContainerView: UIView!
    @IBOutlet weak var submitContainerView: UIView!
    @IBOutlet weak var cancleBtn: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    
     // MARK: --------- View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
         NotificationCenter.default.addObserver(self, selector: #selector(ContactUSViewController.okBtnTapped(_:)), name: NSNotification.Name(rawValue: "OKButtonTapped"), object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // container view properties
        containerView.layer.borderWidth = 0.8
        containerView.layer.borderColor  = cellBackgroundColor.cgColor
        
        // cancle button properties
        cancleBtn.layer.cornerRadius = 5
        cancleBtn.layer.borderWidth = 1
        cancleBtn.layer.borderColor = buttonBorderColor.cgColor
        cancleBtn.clipsToBounds = true

        cancleBtn.setTitle("Cancel".localized, for: .normal)
        cancleBtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        cancleBtn.titleLabel?.textColor = theamBlackColor
        
        // cancle button shadow properties
        cancleContainerView.layer.cornerRadius = 5
        cancleContainerView.layer.shadowColor = UIColor.black.cgColor
        cancleContainerView.layer.shadowOpacity = 0.12
        cancleContainerView.layer.shadowRadius = 4
        cancleContainerView.layer.shadowOffset = .zero
        cancleContainerView.layer.shadowPath = UIBezierPath(rect: cancleContainerView.bounds).cgPath
        
        // submit button properties
        submitButton.layer.cornerRadius = 5
        submitButton.clipsToBounds = true
        submitButton.backgroundColor = buttonBackGroundColor
        
        submitButton.setTitle("Submit".localized, for: .normal)
        submitButton.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        submitButton.titleLabel?.textColor = theamBlackColor
        
        // submit button shadow properties
        submitContainerView.layer.cornerRadius = 5
        submitContainerView.layer.shadowColor = UIColor.black.cgColor
        submitContainerView.layer.shadowOpacity = 0.12
        submitContainerView.layer.shadowRadius = 4
        submitContainerView.layer.shadowOffset = .zero
        submitContainerView.layer.shadowPath = UIBezierPath(rect: submitContainerView.bounds).cgPath
        
    }
    
    
    // MARK: --------- @IBActions
    
    @IBAction func cancleBtnTapped(_ sender: UIButton) {
         self.view.endEditing(true)
        
        if let vc = VC, vc is ChatViewController {
            self.navigationController?.popViewController(animated: true)
        }else{
            contactUS.resetContactUs()
            tableView.reloadData()
        }
    }
    
    @IBAction func submitBtnTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if  contactUS.description.isEmpty{
            
            let currentIndexPath = IndexPath.init(row: 1, section: 0)
            let cell = tableView.cellForRow(at: currentIndexPath) as! ContactUsDescriptionTableViewCell
            cell.validateDescription(text: "")
            
            tableView.beginUpdates()
            tableView.endUpdates()
            
            return
        }
        submitContactUsData()
        
    }
    
    // MARK: --------- @Private Functions
    
    @objc func okBtnTapped(_ notification: NSNotification) {
        print ("Ok Btn tapped")
        self.navigationController?.popViewController(animated: true)
    }
    
}

    // MARK: --------- Delegates Methods

extension ContactUSViewController: BottomPopUpViewControllerDelegate{
    
    func sortByApplyButtonTapped(_ sortBy: String) {
        print(sortBy)
       // self.subject[0] = sortBy
        contactUS.subjects.removeAll()
        contactUS.subjects.append(sortBy)
        tableView.reloadData()
    }
    
}

    // MARK: --------- TableView Delegates/DataSources

extension ContactUSViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.row != 0 else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchSubjectTableViewCell.cellIdentifire(), for: indexPath) as? SearchSubjectTableViewCell  else{
                fatalError("Could not load SearchSubjectTableViewCell")
            }
            //let subject = [Subject]()
            
            if let vc = VC, vc is ChatViewController {
                let msg =  (UserStore.shared.selectedUserType == "Tutor") ? contactUS.subjects![2] : contactUS.subjects![1]
                cell.configureCellWithInfo("What do you need help with".localized, msg)
                cell.isUserInteractionEnabled = false
            }else{
                 cell.isUserInteractionEnabled = true
                 cell.configureCellWithInfo("What do you need help with".localized, contactUS.subjects![0])
            }
           
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ContactUsDescriptionTableViewCell.cellIdentifire(), for: indexPath) as! ContactUsDescriptionTableViewCell
       // cell.descriptionTextView.text = contactUS.description
        cell.configureCell(contactUS.description)
        cell.descriptionTextView.delegate = self
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return indexPath.row == 0 ? SearchSubjectTableViewCell.cellHeight() : ContactUsDescriptionTableViewCell.cellHeight()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let bottomPopUpVC = BottomPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        bottomPopUpVC.bottomPopUpViewControllerDelegate = self
        bottomPopUpVC.userType = "Any"
        bottomPopUpVC.sortByPropertiesArr = ["General".localized, " Account".localized, "Security", "Payments".localized, "Technical Support".localized, "Reporting a Tutor".localized, "Reporting a Student".localized]

        self.navigationController?.present(bottomPopUpVC, animated: true, completion: nil)
        
    }

}

    // MARK: --------- TextView Delegates

extension ContactUSViewController: UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        
        let indexPath = IndexPath(row: 1, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! ContactUsDescriptionTableViewCell
        cell.placeholderLabel.isHidden = !textView.text.isEmpty
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {

        contactUS.description = textView.text ?? ""
        let currentIndexPath = IndexPath.init(row: 1, section: 0)
        let cell = tableView.cellForRow(at: currentIndexPath) as! ContactUsDescriptionTableViewCell
        cell.validateDescription(text: textView.text!)
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}

    // MARK: --------- API Implementation

extension ContactUSViewController{
    
    // SUBMIT CONTACTUS DATA
    func submitContactUsData(){
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        guard let userID = UserStore.shared.userID else{return}
        
        var subject = ""
        var description = ""
        
        if let vc = VC, vc is ChatViewController {
             let msg =  (UserStore.shared.selectedUserType == "Tutor") ? contactUS.subjects![2] : contactUS.subjects![1]
             subject = msg
            
            // description
            if let param = self.param {
   
                if let uName = param["userName"], let uID =  param["userID"], let desc = contactUS.description{
                     description = "UserID:-\(String(describing: uID))\nUserName:- \(String(describing: uName))\n\(String(describing: desc))"
                }
               
            }
        }else{
             subject = contactUS.subjects[0]
             description = contactUS.description
        }
        

        
        let params = ["user_id" : userID, "subject" : subject, "description":description as Any] as [String:AnyObject]
        
        let submitContactUsObserver = ApiManager.shared.apiService.submitContactUs(params)
        let submitContactUsDisposable = submitContactUsObserver.subscribe(onNext: {(message) in
            Utilities.hideHUD(forView: self.view)
            
            DispatchQueue.main.async {
//                let alert = UIAlertController(title: AppName, message: message, preferredStyle: .alert)
//                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: nil)
//                alert.addAction(okAction)
//                self.present(alert, animated: true, completion: nil)
                
                let commonPopUP = ShowInfoCommonPopUpViewController.instantiateFromAppStoryboard(appStoryboard: .Main)
                commonPopUP.fromController = self
                commonPopUP.customMsg = message
                self.present(commonPopUP, animated: true, completion: nil)
                
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        submitContactUsDisposable.disposed(by: disposableBag)
    }
    
}






