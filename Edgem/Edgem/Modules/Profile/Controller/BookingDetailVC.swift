//
//  BookingDetailVC.swift
//  Edgem
//
//  Created by Namespace on 28/08/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class BookingDetailVC: BaseViewController {
    
    // MARK: - Properties
    
    var bookingID: String?
    var disposableBag = DisposeBag()
    
    // MARK: - @IBOutlets
    
    @IBOutlet weak var offerStatusLbl: UILabel!
    @IBOutlet weak var bookinIDLbl: UILabel!
    @IBOutlet weak var dateTitleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeTitleLabel: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var subjectLbl: UILabel!
    @IBOutlet weak var subject: UILabel!
    @IBOutlet weak var studentTitleLbl: UILabel!
    @IBOutlet weak var studentLbl: UILabel!
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    @IBOutlet weak var okBtn: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - @IBActions
    
    @IBAction func didTapOkBtn(_ sender: UIButton) {
        self.dismissPopUp()
    }
    // MARK: - Helping Functions

}

// MARK: API Implementaion
extension BookingDetailVC{
    
    private func getBookingInfo(_ bookingID: String){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        let params: [String : Any] = ["booking_id" : bookingID]
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            
            let userObserver = ApiManager.shared.apiService.getBookingDetail(params as [String : AnyObject])
            
            let userDisposable = userObserver.subscribe(onNext: {(bookingInfo) in
                DispatchQueue.main.async {
                    print(value: bookingInfo)
                }
            }, onError: {(error) in
                DispatchQueue.main.async(execute: {
                    if let error_ = error as? ResponseError {
                        self.showSimpleAlertWithMessage(error_.description())
                    } else {
                        if !error.localizedDescription.isEmpty {
                            self.showSimpleAlertWithMessage(error.localizedDescription)
                        }
                    }
                })
            })
            userDisposable.disposed(by: self.disposableBag)
        }
        
    }
}
