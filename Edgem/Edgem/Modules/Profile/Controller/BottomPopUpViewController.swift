//
//  BottomPopUpViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 21/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol BottomPopUpViewControllerDelegate{
    func sortByApplyButtonTapped(_ sortBy: String)
}

class BottomPopUpViewController: BaseViewController {
    
    // MARK: --------- Properties
    
    let sortByProperties = ["","Furthest to Nearest", "Nearest to Furthest"]
    let sortByPropertysStudent = ["", "Furthest to Nearest", "Nearest to Furthest"]
    let sectionTitle = ["Distance"]
    var sortByPropertiesArr: [String] = Array()
    var selectedProperty: [String] = Array()
    var rowCount = 0
    var userType = ""
    var bottomPopUpViewControllerDelegate: BottomPopUpViewControllerDelegate?
    
    // MARK: --------- @IBOutlet

    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    @IBOutlet weak var popUPTitle: UILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var arcView: UIView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var resetBtnHolderView: UIView!
    @IBOutlet weak var applyBtnHolderView: UIView!
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        self.tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        arcSetUP()
        registerCell()

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dismissPopUp), name: .dismissPopUP,
                                               object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        rowCount = sortByPropertiesArr.count > 0 ?  sortByPropertiesArr.count : 0

    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        tableView.layer.removeAllAnimations()
        let maxHeight = ScreenHeight-200
        tableViewHeight.constant = maxHeight<tableView.contentSize.height ? maxHeight : tableView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
            self.loadViewIfNeeded()
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Reset button properties
        
        resetButton.layer.cornerRadius = 5
        resetButton.layer.borderWidth = 1
        resetButton.layer.borderColor = theamGrayColor.cgColor
        resetButton.clipsToBounds = true
        if userType == "Any"{
             resetButton.setTitle("Cancel".localized, for: .normal)
             applyButton.setTitle("Done".localized, for: .normal)
        }else{
             resetButton.setTitle("Reset".localized, for: .normal)
             applyButton.setTitle("Apply".localized, for: .normal)
        }
        //resetButton.setTitle("Reset".localized, for: .normal)
        resetButton.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        resetButton.titleLabel?.textColor = theamBlackColor
        
        
        
        // Reset button shadow properties
        resetBtnHolderView.layer.shadowColor = UIColor.black.cgColor
        resetBtnHolderView.layer.shadowOpacity = 0.12
        resetBtnHolderView.layer.shadowRadius = 4
        resetBtnHolderView.layer.shadowOffset = .zero
        resetBtnHolderView.layer.shadowPath = UIBezierPath(rect: resetBtnHolderView.bounds).cgPath
        
        // apply button attributes
        applyButton.layer.cornerRadius = 5
        applyButton.backgroundColor = buttonBackGroundColor
        applyButton.clipsToBounds = true
        
        //applyButton.setTitle("Apply".localized, for: .normal)
        applyButton.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        applyButton.titleLabel?.textColor = theamBlackColor
        
        
        // apply button shadow
        applyBtnHolderView.layer.shadowColor = UIColor.black.cgColor
        applyBtnHolderView.layer.shadowOpacity = 0.12
        applyBtnHolderView.layer.shadowRadius = 4
        applyBtnHolderView.layer.shadowOffset = .zero
        applyBtnHolderView.layer.shadowPath = UIBezierPath(rect: applyBtnHolderView.bounds).cgPath
        
    }
    
    // MARK: --------- Private Functions
    
    @objc override func dismissPopUp() {
         self.dismiss(animated: true)
    }
    
    func arcSetUP(){
        
        let frame = arcView.frame
        let path = UIBezierPath()
        
        let p1 = CGPoint(x:0,y:frame.height)
        let p3 = CGPoint(x:ScreenWidth,y:frame.height)
        
        path.move(to: p1)
        path.addQuadCurve(to: p3, controlPoint: CGPoint(x: ScreenWidth/2, y: -frame.height))
        
        let line = CAShapeLayer()
        line.path = path.cgPath;
        line.strokeColor = UIColor.white.cgColor
        line.fillColor = UIColor.white.cgColor
        arcView.layer.addSublayer(line)
        
    }
    
    func registerCell(){
        tableView.register(UINib(nibName: "CommonCheckBoxCell", bundle: nil), forCellReuseIdentifier: CommonCheckBoxCell.cellIdentifire())
    }
    
    //var sortByApplyButtonTapped: ((String)->Void)?
   
    // MARK: --------- @IBActions
    
    @IBAction func dismissPopUpTapped(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func resetBtnTapped(_ sender: UIButton) {
        if userType == "Any"{
            if selectedProperty.count > 0{
                  selectedProperty[0] = "General".localized
            }
             self.dismiss(animated: true)
        }else{
            selectedProperty.removeAll()
        }
        
        tableView.reloadData()
    }
    
    @IBAction func applyBtnTapped(_ sender: UIButton) {
       print("apply btn tapped")
        var sortBy = ""
        if selectedProperty.count > 0{
            sortBy = selectedProperty[0]
        }
        
        //let data = ["sort_by" : sortBy] as [String : AnyObject]
        bottomPopUpViewControllerDelegate?.sortByApplyButtonTapped(sortBy)
        //NotificationCenter.default.post(name: .sortByBtnTapped, object: nil, userInfo:data)
        self.dismissPopUp()
    }

}

extension BottomPopUpViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: CommonCheckBoxCell.cellIdentifire(), for: indexPath) as! CommonCheckBoxCell
        
        if userType == "Tutor"{
            
            if indexPath.section == 0{
                
                if indexPath.row == 0{
                    cell.isUserInteractionEnabled = false
                    cell.sepratorView.isHidden = true
                    cell.configureCellWithSortByTitles(sectionTitle[indexPath.section], false)
                }else {
                    cell.sepratorView.isHidden = true
                    let sortByPropertyStudent = sortByPropertiesArr[indexPath.row]
                    
                    var isSelected = false
                    let index = selectedProperty.index{ (property)->Bool in
                        return property == sortByPropertyStudent
                    }
                    if let subindex = index, subindex > -1{
                        isSelected = true
                    }
                    
                    cell.configureCellWithSortBy(sortByPropertyStudent, isSelected)
                }
                
            }
            
        }else if userType == "Student" ||  userType == "Parent"{
            
            let sortByProperty = sortByPropertiesArr[indexPath.row]
            var isSelected = false
            let index = selectedProperty.index{ (property)->Bool in
                return property == sortByProperty
            }
            if let subindex = index, subindex > -1{
                isSelected = true
            }
            cell.configureCellWithSortBy(sortByProperty, isSelected)
            
        } else if userType == "Any"{
            
            let sortByProperty = sortByPropertiesArr[indexPath.row]
            var isSelected = false
            let index = selectedProperty.index{ (property)->Bool in
                return property == sortByProperty
            }
            if let subindex = index, subindex > -1{
                isSelected = true
            }
            self.popUPTitle.text = "Select".localized
            cell.configureCellWithProperties(sortByProperty, isSelected)
            
        }

        
         return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CommonCheckBoxCell.cellHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(indexPath.row)
        if userType == "Tutor"{
            
            let sortByProperty = sortByPropertiesArr[indexPath.row]//sortByPropertysStudent[indexPath.row]
            let index = selectedProperty.index { (property) -> Bool in
                return property == sortByProperty
            }
            if let subIndex = index, subIndex > -1{
                selectedProperty.remove(at: subIndex)
            }else{
                selectedProperty.removeAll()
                selectedProperty.append(sortByProperty)
            }

        }else if userType == "Student" || userType == "Parent"{
            
            let sortByProperty = sortByPropertiesArr[indexPath.row]//sortByProperties[indexPath.row]
            let index = selectedProperty.index { (property) -> Bool in
                return property == sortByProperty
            }
            if let subIndex = index, subIndex > -1{
                selectedProperty.remove(at: subIndex)
            }else{
                selectedProperty.removeAll()
                selectedProperty.append(sortByProperty)
            }
            
        }else if userType == "Any"{
            
            let sortByProperty = sortByPropertiesArr[indexPath.row]//sortByProperties[indexPath.row]
            let index = selectedProperty.index { (property) -> Bool in
                return property == sortByProperty
            }
            if let subIndex = index, subIndex > -1{
                selectedProperty.remove(at: subIndex)
            }else{
                selectedProperty.removeAll()
                selectedProperty.append(sortByProperty)
            }
            
        }
        tableView.reloadData()
        
    }
    
}
