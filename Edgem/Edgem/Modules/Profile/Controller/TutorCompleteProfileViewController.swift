//
//  TutorCompleteProfileViewController.swift
//  Edgem
//
//  Created by Namespace on 14/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift
import EasyTipView
import BitlySDK

class TutorCompleteProfileViewController: BaseViewController {
    
     // MARK: -------- Properties

    weak var tipView: EasyTipView?
    var preferences: EasyTipView.Preferences!
    
    var tutorID:Int?
    var tutorImage: UIImage?
    var tutorRating: String?
    var userData: User?
    var tutor: Tutor?
    var rating = [Rating]()
    var pagination = Pagination.init()
    var firstName = ""
    var lastName = ""
    var isFrom: UIViewController?
    var isFromNotification = false
    var disposableBag = DisposeBag()
    var currentIndex = 1
    var isViewController: UIViewController?
    
    var qrcodeImage: CIImage!
    var qrImage: UIImage!
    var myAppShareURL: URL?
    
    let imgQRCode = UIImageView()
    
    var commonSubjectOfStudentAndTutor: [Subject] = Array()

    // MARK: -------- @IBoutlets
    @IBOutlet weak var settingIcon: UIButton!{
        didSet{
           settingIcon.isHidden = tutorID != UserStore.shared.userID
        }
    }
    
    @IBOutlet weak var distanceViewHeight: NSLayoutConstraint!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var distanceLabelValue: UILabel!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var bottomContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var noRecordLbl: UILabel!
    @IBOutlet weak var tutorProfileIcon: UIImageView!
    @IBOutlet weak var tutorName: UILabel!
    @IBOutlet weak var verifiedIcon: UIImageView!
    @IBOutlet weak var verifiedLabel: UILabel!
    @IBOutlet weak var totalRatingLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var aboutButton: UIButton!
    @IBOutlet weak var ratingButton: UIButton!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var onlineStatus: UIImageView!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var offerHolderView: UIView!
    @IBOutlet weak var offerBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!  {
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
         offerBtn.backgroundColor = UIColor(red: 255/255, green: 210/255, blue: 113/255, alpha: 0.4)
        
        preferences = EasyTipView.Preferences()
        preferences.drawing.arrowPosition = .bottom
        preferences.drawing.textAlignment = .center
        preferences.drawing.font = UIFont(name: QuicksandMedium, size: 15)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.positioning.textHInset = 40
        preferences.positioning.textVInset = 10
        preferences.drawing.backgroundColor = UIColor(red: 23/255, green: 23/255, blue: 23/255, alpha: 0.80)
        preferences.animating.dismissTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1.5
        preferences.animating.dismissDuration = 1.5
        
        EasyTipView.globalPreferences = preferences
        
        distanceViewHeight.constant = 0
        distanceLabel.isHidden = true
        distanceLabelValue.isHidden = true
        
        self.infoBtn.isHidden = true
        
        setTabButtons(currentIndex)
        
        self.tutorProfileIcon.image = self.tutorImage
        setUPView()
        DispatchQueue.main.async {
            if let id = self.tutorID{
                self.getUserDetails(id)
                self.rating = []
                self.getUserRatings(id)
            }
        }
        
         NotificationCenter.default.addObserver(self, selector: #selector(userConnected), name: .userConnected, object: nil)
        
        tableView.register(UINib(nibName: SubjectDropDownCellInProfile.cellIdentifire(), bundle: nil), forCellReuseIdentifier: SubjectDropDownCellInProfile.cellIdentifire())
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let tipView = self.tipView{
            tipView.dismiss()
            // toolTipBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        imageCache.removeAllObjects()
    }
    
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // TODO: --------------Change the Condition according Tutor Id and not according VC also If Tutor's subject and Student subject missmatch then also button will be hide

        if isViewController is DashboardViewController && AppDelegateConstant.user?.userType == "Tutor" {
            bottomContainerViewHeight.constant = 0
            bottomContainerView.isHidden = true
        }else if isFrom is ProfileViewController || isFrom is ChatViewController{
            bottomContainerViewHeight.constant = 0
            bottomContainerView.isHidden = true
        }else if isFromNotification == true{
            bottomContainerViewHeight.constant = 0
            bottomContainerView.isHidden = true
        }else{
            bottomContainerViewHeight.constant = 90
            bottomContainerView.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let tipView = self.tipView{
            tipView.dismiss()
           // toolTipBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // student profile icon
//        tutorProfileIcon.layer.cornerRadius = tutorProfileIcon.frame.width/2
//        tutorProfileIcon.clipsToBounds = true
        
        // active view

        // tutor name
        tutorName.textColor = theamBlackColor
        tutorName.font = UIFont(name: QuicksandBold, size: 20)
        
        // verified Label
       // verifiedLabel.text = "Verified".localized
        verifiedLabel.textColor = theamGrayColor
        verifiedLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        
        // totalRating Label
        totalRatingLabel.textColor = theamBlackColor
        verifiedLabel.font = UIFont(name: QuicksandMedium, size: 15)
        
        // ratingLabel
        //ratingLabel.text = "Rating".localized
        ratingLabel.textColor = theamGrayColor
        ratingLabel.font = UIFont(name: SourceSansProRegular, size: 13)
        
        // aboutButton
        aboutButton.setTitle("About".localized, for: .normal)
        //aboutButton.titleLabel?.font = UIFont (name: QuicksandBold, size: 18)
        
        // bottomContainerView
        bottomContainerView.layer.borderWidth = 2
        bottomContainerView.layer.borderColor = UIColor(red: 119, green: 119, blue: 119, alpha: 1).cgColor
        bottomContainerView.layer.shadowOffset = CGSize(width: 0, height: -4)
        bottomContainerView.layer.shadowRadius = 4
        bottomContainerView.layer.shadowColor = UIColor.black.cgColor
        bottomContainerView.layer.shadowOpacity = 0.09
        
        // confirm button attributes
        offerBtn.layer.cornerRadius = 5
       // offerBtn.backgroundColor = buttonBackGroundColor
        offerBtn.clipsToBounds = true
        
        offerBtn.setTitle("Make Offer".localized, for: .normal)
        offerBtn.titleLabel?.font = UIFont(name: QuicksandMedium, size: 18)
        //offerBtn.titleLabel?.textColor = theamBlackColor
        
        // apply button shadow
        offerHolderView.layer.cornerRadius = 5
        offerHolderView.layer.shadowColor = UIColor.black.cgColor
        offerHolderView.layer.shadowOpacity = 0.12
        offerHolderView.layer.shadowRadius = 4
        offerHolderView.layer.shadowOffset = .zero
        offerHolderView.layer.shadowPath = UIBezierPath(rect: offerHolderView.bounds).cgPath
        
    }
    

    // MARK: -------- @IBActons
    
    
    @IBAction func didTappedInfoBtn(_ sender: UIButton) {
     // popTip.show(text: "Hello", direction: .down, maxWidth: 200, in: self.contentView, from: sender.frame)
        self.showSimpleAlertWithMessage("No matching subjects found".localized)
    }
    
    @IBAction func tabButtonsTapped(_ sender: UIButton) {
        setTabButtons(sender.tag)
        showTransitionInView(sender.tag)
    }
    
    func generateBitlyUrl(){
        
        guard let userID = self.tutorID else {return}
        let userType = tutorType
        
        let userDetail = "\(sharingBaseURL)?type=profile&id=\(userID)&user_type=\(userType)"
     
        print(value: userDetail)
        
        Bitly.shorten(userDetail) { (response, error) in
            if error == nil{
               let bitlyStr = response?.bitlink ?? userDetail
    
                let url =  URL(string: bitlyStr)
                
                if let _url = url {
                    let msg = "Hi, I am \(self.firstName) \(self.lastName) and I am an active Tutor on Edgem. You may find more information about me via this link \n \(_url)".localized
                    
                    let activityController = UIActivityViewController(activityItems: [msg], applicationActivities: nil)
                    
                    self.present(activityController, animated: true, completion: nil)
                }
               
            }
        }
    }
    
    @IBAction func settingButtonTapped(_ sender: Any) {
        let settingsViewController = SettingsViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        self.navigationController?.pushViewController(settingsViewController, animated: true)
        
        /// Use this function to share profile
       // generateBitlyUrl()
    }
    
    
    @IBAction func didTapedOfferBtn(_ sender: UIButton) {
        if let user = self.userData {
            let edgemOfferVC = EdgemOfferViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
            edgemOfferVC.userData = user
            edgemOfferVC.commonSubjectOfStudentAndTutor = self.commonSubjectOfStudentAndTutor
            self.navigationController?.pushViewController(edgemOfferVC, animated: true)
        }else{
            return
        }

    }
    
     // MARK: -------- Private Functions
    
    @objc func userConnected(){
        print("user online")
        
        if let userId = self.tutorID {
            let image = (Global.idArrays.contains(userId)) ? "user_online" : "user_offline"
            self.onlineStatus.image = UIImage(named: image)
        }
        
    }
    
    fileprivate func compareSubjects(_ userSubjectsArray: [Subject], _ selectedUserSubjectsArray: [Subject]){
        
        let array1 = userSubjectsArray
        let array2 = selectedUserSubjectsArray
        
        let userSubjectsArrayStr = array1.map{$0.name}
        let selectedUserSubjectsArrayStr = array2.map({$0.name})
        
        let commonElements = Array(Set(userSubjectsArrayStr).intersection(Set(selectedUserSubjectsArrayStr)))
        print(commonElements)
        if commonElements.count > 0{
            userSubjectsArray.forEach { (subject) in
                for subject_common in commonElements{
                    if let sub_comm = subject_common, sub_comm ==  subject.name{
                         self.commonSubjectOfStudentAndTutor.append(subject)
                    }
                }
            }
        }
        if userSubjectsArrayStr.intersects(with: selectedUserSubjectsArrayStr) {
            enableDisableMakeOfferBtn(true)
            infoBtn.isHidden = true
        } else {
           self.infoBtn.isHidden = true
            enableDisableMakeOfferBtn(false)
        }
    }

//    fileprivate func compareSubjects(_ userSubjectsArray: [Subject], _ selectedUserSubjectsArray: [Subject]){
//
//        let array1 = userSubjectsArray
//        let array2 = selectedUserSubjectsArray
//
//        let userSubjectsArrayStr = array1.map{$0.name}
//        let selectedUserSubjectsArrayStr = array2.map({$0.name})
//
//        let commonElements = Array(Set(userSubjectsArrayStr).intersection(Set(selectedUserSubjectsArrayStr)))
//        print(commonElements)
//        if commonElements.count > 0{
//            selectedUserSubjectsArray.forEach { (subject) in
//                for subject_common in commonElements{
//                    if subject_common! == subject.name{
//                        self.commonSubjectOfStudentAndTutor.append(subject)
//                    }
//                }
//            }
//        }
//
//        if userSubjectsArrayStr.intersects(with: selectedUserSubjectsArrayStr) {
//            enableDisableMakeOfferBtn(true)
//        } else {
//            enableDisableMakeOfferBtn(false)
//        }
//    }
    

    fileprivate func popUpForNoMatchingSubject() {
        let text = "No matching subjects found.".localized
        
        if let tipView = self.tipView{
            
            tipView.dismiss()
            
        }else{
            
            let tip = EasyTipView(text: text, preferences: self.preferences, delegate: self)
            
            tip.show(forView: self.offerHolderView)
            
            tipView = tip
            
        }
    }
    
    fileprivate func enableDisableMakeOfferBtn(_ status: Bool){
       
        if status == false{
            offerBtn.isUserInteractionEnabled = false
            offerBtn.backgroundColor = UIColor(red: 255/255, green: 210/255, blue: 113/255, alpha: 0.4)
            offerBtn.titleLabel?.textColor = UIColor(r: 139, g: 139, b: 139, a: 1)
            
            if isViewController is DashboardViewController && AppDelegateConstant.user?.userType == "Tutor" || isFrom is ProfileViewController || isFrom is ChatViewController || isFromNotification == true{
                // do nothing
            }else{
                  popUpForNoMatchingSubject()
            }
          
        }else{
            offerBtn.isUserInteractionEnabled = true
            offerBtn.backgroundColor = theamAppDarkColor
            offerBtn.titleLabel?.textColor = theamBlackColor
        }
        
    }
    
    func setUPView(){
        
        if isFrom is ProfileViewController {
            if let f_Name = AppDelegateConstant.user?.firstName{
                firstName = f_Name
            }
            if let l_Name = AppDelegateConstant.user?.lastName{
                lastName = l_Name
            }
            self.tutorName.text  = firstName + " " +  lastName
            
            // profile image
            if let imageURL = AppDelegateConstant.user?.imageURL{
                let imageProvider = ImageProvider()
                guard let url = URL(string: imageURL) else{return}
                imageProvider.requestImage(from:  url) { image in
                    self.tutorProfileIcon.image = image
                }
            }else{
                tutorProfileIcon.image = UIImage(named: "femaleIcon")
            }
            
             // rating
            if let rating = AppDelegateConstant.user?.ratings, let rating_in_num = Int(rating){
                
                if rating_in_num > 0{
                    self.totalRatingLabel.text = "\(rating)/5"
                    ratingLabel.text = "Rating".localized
                }else{
                    self.totalRatingLabel.text = ""
                    self.ratingLabel.text = "No ratings".localized
                }
            }
            
            // status
            if let status = AppDelegateConstant.user?.status{
                self.verifiedLabel.text = status
            }
            
            let image = AppDelegateConstant.user?.onlineStatus == 1 ? #imageLiteral(resourceName: "user_online") : #imageLiteral(resourceName: "user_offline")
            self.onlineStatus.image = image
            
            //self.distanceLabelValue.text = AppDelegateConstant.user?.distance ?? ""
            
        }else{
            
            if let tutorData = self.tutor{
                 // name
                 self.tutorName.text = tutorData.firstName //+ " " + tutorData.lastName
                
                 // profile image
                
                // [START Profile Image]
                let imageStr =  tutorData.realImage
                if imageStr != ""{
                    GetImageFromServer.getImage(imageURL: imageStr) { (image) in
                        self.tutorProfileIcon.image = image
                    }
                }else{
                    self.tutorProfileIcon.image  = UIImage(named: "femaleIcon")
                    }
                // [END Profile Image]
                
                  //self.distanceLabelValue.text =  ?? ""
            
            }

            if let userData = userData {
                
                /// We can not see our own distance, thats why we have to hide it
                if userData.ID == AppDelegateConstant.user?.ID{
                    self.distanceLabel.isHidden = true
                    self.distanceLabelValue.isHidden = true
                }else{
                    // Distance
                    if let distance = userData.distance{
                        distanceViewHeight.constant = 57
                        distanceLabel.isHidden = false
                        distanceLabelValue.isHidden = false
                        self.distanceLabelValue.text = distance
                    }
                }
                
                // name
                self.tutorName.text = userData.firstName //+ " " + userData.lastName
                
                // profile image
                
                // [START Profile Image]
                if let imageStr =  userData.imageURL{
                    if imageStr != ""{
                        GetImageFromServer.getImage(imageURL: imageStr) { (image) in
                            self.tutorProfileIcon.image = image
                        }
                    }else{
                        self.tutorProfileIcon.image  = UIImage(named: "femaleIcon")
                    }
                }
          
                // [END Profile Image]
                
                // Rating
                if let rating = (userData.ratings),let rating_in_num = Int(rating){
                    
                    if rating_in_num > 0{
                        self.totalRatingLabel.text = "\(rating)/5"
                        ratingLabel.text = "Rating".localized
                    }else{
                        self.totalRatingLabel.text = ""
                        self.ratingLabel.text = "No ratings".localized
                    }
                    
                }
                
                  // status
                if let status = userData.status{
                     self.verifiedLabel.text = status
                }
                
                // online status
                let image = userData.onlineStatus == 1 ? #imageLiteral(resourceName: "user_online") : #imageLiteral(resourceName: "user_offline")
                self.onlineStatus.image = image
                }

        }

    }
    
    private func setTabButtons(_ forIndex: Int){
        self.noRecordLbl.isHidden = true
        aboutButton.titleLabel?.font = UIFont(name: forIndex == 1 ? QuicksandBold : QuicksandRegular, size: 18)
        ratingButton.titleLabel?.font = UIFont(name: forIndex == 2 ? QuicksandBold : QuicksandRegular, size: 18)
    }
    
    func updateCell(){
        print("update")
        self.tableView.beginUpdates()
        self.tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .automatic)
        self.tableView.endUpdates()
    }
    
    func showTransitionInView(_ forIndex: Int){
        
        let movingX = forIndex<currentIndex ? ScreenWidth : -ScreenWidth
        currentIndex = forIndex
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
            self.tableView.transform = CGAffineTransform.identity.translatedBy(x:movingX, y: 0)
            self.sepratorView.transform = CGAffineTransform.identity.translatedBy(x:CGFloat(forIndex-1)*(ScreenWidth/2), y: 0)
            
        },completion: { finished in
            self.noRecordLbl.isHidden = forIndex == 1 || self.rating.count>0
            self.tableView.transform = CGAffineTransform.identity.translatedBy(x:0, y: 0)
            self.tableView.reloadData()
        })
    }
    
}

extension TutorCompleteProfileViewController: EasyTipViewDelegate{
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
       // toolTipBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
    }
}

extension TutorCompleteProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func updateHeight() {
        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard (userData != nil) else {
            return 0
        }
        return currentIndex == 1 ? 5 : rating.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard currentIndex == 1 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: RatingTableViewCell.cellIdentifire(), for: indexPath) as! RatingTableViewCell
            cell.configureCellWithRating(rating[indexPath.row])
            return cell
            
        }
        if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: AvailabilityTableViewCell.cellIdentifier(), for: indexPath) as! AvailabilityTableViewCell
            
            cell.reloadCalendarViewData((userData?.availibility)!)
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: SubjectDropDownCellInProfile.cellIdentifire(), for: indexPath) as! SubjectDropDownCellInProfile
            cell.configureCell(with: userData?.subjects ?? nil)
            cell.secondTableView.reloadData()
            
            cell.tutorCompleteProfileViewController = self
            cell.secondTableView.invalidateIntrinsicContentSize()
            cell.secondTableView.layoutIfNeeded()
            self.updateHeight()
             return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: TutorProfileAboutTableViewCell.cellIdentifire(), for: indexPath) as! TutorProfileAboutTableViewCell
            
            if indexPath.row == 0{
                cell.configureCellWithTutorQualification((userData?.qualifications)!)
            }else if indexPath.row == 1{
                cell.configureCellWithTutorExperience((userData?.experiences)!)
            }else if indexPath.row == 2{
                if let edication = userData?.educations{
                    cell.configureCellWithTutorEducation(edication)
                }
            }
//            else if indexPath.row == 3{
//                cell.configureCellWithTutorsSubject((userData?.subjects)!)
//            }
           
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if currentIndex == 1{
            if  indexPath.row == 1{
                if let experience = self.userData?.experiences {
                    return experience.count > 0 ? UITableView.automaticDimension : 0
                }
            }
        }

        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let tipView = self.tipView{
            tipView.dismiss()
            // toolTipBtn.setImage(#imageLiteral(resourceName: "info_gray"), for: .normal)
        }
    }
    
}

// MARK: ------ API Implementation
extension TutorCompleteProfileViewController {
    
    func getUserDetails(_ userID: Int) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let dict: [String : Any] = ["id" : "\(userID)"]
        
        let userObserver = ApiManager.shared.apiService.getUserDetails(dict as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(user) in
            DispatchQueue.main.async {
                
                Utilities.hideHUD(forView: self.view)
                print(user)
                self.userData = user
                if let selectedUserSubject = user.subjects,  let loginUserSubject = AppDelegateConstant.user?.subjects{
                    self.compareSubjects(selectedUserSubject, loginUserSubject)
                }
                
                self.setUPView()
                self.tableView.reloadData()
                
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
    
    
    func getUserRatings(_ userID: Int){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let dict: [String : Any] = ["id" : "\(userID)", "page": pagination.currentPage+1]
        
        let userObserver = ApiManager.shared.apiService.getUserRating(dict as [String : AnyObject])
        let userDisposable = userObserver.subscribe(onNext: {(ratingPagination) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                
                print(ratingPagination)
                self.pagination = ratingPagination.pagination
                self.rating += ratingPagination.rating
                self.tableView.reloadData()
            }
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    self.showSimpleAlertWithMessage(error_.description())
                } else {
                    if !error.localizedDescription.isEmpty {
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                    }
                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }
}



