//
//  LevelSubjectViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 16/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class LevelSubjectViewController: BaseViewController {
    
    // MARK: ------- Properties
    var addLevelSubject: AddLevelSubject!
    var userData: User!
    var selectedSubjects = [Subject]()
    var selectedLevels = [Level]()
    var disposableBag = DisposeBag()
    
    // MARK: ------- @IBOutlets

    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: ------- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let _user = AppDelegateConstant.user{
            userData = _user
            if let selectedLevels = _user.level {
                 self.selectedLevels = selectedLevels.map {$0}
            }
            if let selectedSubjects = _user.subjects {
                self.selectedSubjects = selectedSubjects.map {$0}
            }
        }
        print(selectedLevels)
        print(selectedSubjects)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        if UserStore.shared.selectedUserType  == "Tutor"{
//
//        }else{
//            getStudentLavelNSubject()
//        }
    }
    
    
    //  MARK: ------- @IBActions
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        navigateBack(sender: sender)
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        saveLevelAndSubjects()
    }

}

extension LevelSubjectViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchSubjectTableViewCell.cellIdentifire(), for: indexPath) as? SearchSubjectTableViewCell  else{
            fatalError("Could not load SearchSubjectTableViewCell")
        }
        
        if indexPath.row == 0{
            
        cell.configureCellWithLevels("Level".localized, self.selectedLevels)
            
        }else{
            
         cell.configureCellWithSubjectsList("Subjects".localized, self.selectedSubjects)
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row{
            
        case LevelSubjectTableViewType.Level.rawValue :
            
            let subjectlListingVC = SubjectListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            subjectlListingVC.subjectListingViewControllerDelegate = self
            subjectlListingVC.screenType = .Level
            subjectlListingVC.userData = self.userData
            subjectlListingVC.isFrom = self
            subjectlListingVC.addLevelSubject = self.addLevelSubject
            self.navigationController?.pushViewController(subjectlListingVC, animated: true)
            
        case LevelSubjectTableViewType.Subjects.rawValue :
            
            let subjectlListingVC = SubjectListingViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
            subjectlListingVC.subjectListingViewControllerDelegate = self
            subjectlListingVC.screenType = .Subjects
            subjectlListingVC.isFrom = self
            subjectlListingVC.userData = self.userData
//            if let level_subject = self.addLevelSubject{
//                 subjectlListingVC.addLevelSubject = level_subject
//            }else {
//                if selectedSubjects.count > 0{
//                    subjectlListingVC.addLevelSubject.selectedSubjects = self.selectedSubjects
//                }
//            }
            if selectedSubjects.count > 0{
                subjectlListingVC.selectedSubjects = self.selectedSubjects
            }
            self.navigationController?.pushViewController(subjectlListingVC, animated: true)
            
        default:
            break
        }
        
    }
    
    
}

 //MARK: -----------------SubjectListingViewController's Delegates Methods

extension LevelSubjectViewController: SubjectListingViewControllerDelegate{
    
    func selectedSubject(_ selectedSubs: [Subject]) {
        
        if selectedSubs.count>0{
            for index in 0..<selectedSubs.count{
                if selectedSubs[index].name == "Others".localized{
                    selectedSubs[index].enteredName = userData.otherSubject
                }else if selectedSubs[index].name == "Languages".localized{
                    selectedSubs[index].enteredName = userData.language
                }else if selectedSubs[index].name == "Music".localized{
                    selectedSubs[index].enteredName = userData.music
                }
            }
        }
        
        self.selectedSubjects = selectedSubs
        tableView.reloadData()
    }
    
    func selectedLevel(_ selectedLevls: [Level]) {
        
        self.selectedLevels = selectedLevls
        tableView.reloadData()
       
    }
    
    func selectedQualification(_ selectedQuals: [Qualification]) {
        print("Qualification")
    }

}


//MARK: ----------------- API Implementation

extension LevelSubjectViewController{

    // Save Student and Levels
    
    func saveLevelAndSubjects() {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
       
         var filterDict = [String: AnyObject]()
        
         var levelIDs = ""
         for i in 0..<selectedLevels.count{
            if i != selectedLevels.count-1{
                levelIDs += String(selectedLevels[i].id)
                levelIDs += ","
            }else{
                levelIDs += String(selectedLevels[i].id)
            }
        }
         if !levelIDs.isEmpty {
            filterDict["level"] = levelIDs as AnyObject
         }else{
             self.showSimpleAlertWithMessage("Please select at least one level".localized)
            return
        }
        
        var dict = [[String:AnyObject]]()
        
        if selectedSubjects.count > 0 {
            
            selectedSubjects.forEach { (subject) in
              
                var subjectDict = [String:AnyObject]()
                
                subjectDict = ["subject_id"        :  subject.id,
                                        "other_subject"  :  subject.enteredName] as [String : AnyObject]
                
                dict.append(subjectDict)
            }
            
        }else{
                        self.showSimpleAlertWithMessage("Please select at least one subject".localized)
            
                         return
        }
        
        //var dict = addLevelSubject.getFilterParamsDict()
       // guard let userID = UserStore.shared.userID else{return}
        //filterDict["user_id"]    =   "\(userID)" as AnyObject
        filterDict["subject_offered"] = dict as AnyObject
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let filterObserver = ApiManager.shared.apiService.saveLevelAndSubject(filterDict as [String : AnyObject])
        let fileterDisposable = filterObserver.subscribe(onNext: { (userLavelAndSubject) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                
                if let subjects = userLavelAndSubject.subjects{
                    self.selectedSubjects = subjects
                }
                
                if let levels = userLavelAndSubject.level{
                  self.selectedLevels = levels
                }
                AppDelegateConstant.user?.subjects = self.selectedSubjects
                AppDelegateConstant.user?.level = self.selectedLevels
                //self.showSimpleAlertWithMessage("Your Level & Subjects have been added successfully.".localized)
                self.showSimpleAlertWithMessgeAndNavigateBack("Your Level & Subjects have been added successfully.".localized)
                self.tableView.reloadData()
            }
        }, onError: { (error) in
            Utilities.hideHUD(forView: self.view)
        })
        return fileterDisposable.disposed(by: disposableBag)
    }
    
    
    
    func getStudentLavelNSubject(){
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        guard let userID = UserStore.shared.userID else{return}
        
    
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let getlevelAndSubject = ApiManager.shared.apiService.getStudentLavelNSubject("\(userID)")
        let getlevelAndSubjectDisposable = getlevelAndSubject.subscribe(onNext: { (levelNSubjects) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
                print(levelNSubjects)
                self.addLevelSubject = levelNSubjects
                //AppDelegateConstant.user =
                self.tableView.reloadData()
            }
        }, onError: { (error) in
            DispatchQueue.main.async {
                Utilities.hideHUD(forView: self.view)
            }
        })
        return getlevelAndSubjectDisposable.disposed(by: disposableBag)
        
        }
    
      // Save Student and Levels
    
    }

