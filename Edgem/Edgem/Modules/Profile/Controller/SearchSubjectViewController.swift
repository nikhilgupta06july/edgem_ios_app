//
//  SearchSubjectViewController.swift
//  Edgem
//
//  Created by Namespace on 07/08/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import DropDown
import RxSwift

class SearchSubjectViewController: BaseViewController {
    
    // MARK: - Properties
    let dropDown = DropDown()
    
    var qualificationsNExperiencesVC: QualificationsNExperiencesVC?
   
    var schoolName = ""
    
    var section: Int!
    var row: Int!
    var textFieldTag: Int!
    var searchFor: String!
    
    var _city_addresses:[String] = []{
        didSet{
            if _city_addresses.count > 0{
                configureDropDown()
                self.dropDown.show()
            }else{
                self.dropDown.hide()
            }
            dropDown.reloadAllComponents()
        }
    }
    
    var disposableBag = DisposeBag()

    // MARK: - @IBOutlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var textContainerView: UIView!
    @IBOutlet weak var subjectNotFoundLabel: UITextField!
    
    // MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
         configureUI()
         configureDropDown()
        
    }
    
     // MARK: - @IBActions
    @IBAction func didTapCancleBtn(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func searchSchool(_ sender: UITextField) {
        
        // search schools
        if self.searchFor == schoolType {
            
            if (sender.text?.count)! >= 3{
               
                let searchStr = sender.text
                
                getPlaces(searchString: searchStr!)
                
            }else if (sender.text?.count)! == 0{
                
                if self.dropDown.isHidden == false{
                    
                    self.dropDown.hide()
                    
                }
                
            }
            
        }else if self.searchFor == subjectType{
            
            if (sender.text?.count)! > 0{
                
                 let searchStr = sender.text?.NoWhiteSpace
                
                 searchSubjects(searchString: searchStr!)
                
            }else if (sender.text?.count)! == 0{
                
                if self.dropDown.isHidden == false{
                    
                    self.dropDown.hide()
                    
                }
                
            }
           
        }
        
//        if (sender.text?.count)! >= 3{
//
//            let searchStr = sender.text//.NoWhiteSpace
//
//            if self.searchFor == schoolType {
//                // search schools
//                  getPlaces(searchString: searchStr!)
//
//            }else if self.searchFor == subjectType {
//                // search subjects
//                searchSubjects(searchString: searchStr!)
//
//            }else if self.searchFor == certificateType {
//                // search certificates
//
//            }
//
//
//        }else if (sender.text?.count)! == 0{
//
//            if self.dropDown.isHidden == false{
//                  self.dropDown.hide()
//            }
//
//        }
        
    }
    
     // MARK: - Healping Functions
    
    fileprivate func configureUI(){
    
        self.textContainerView.layer.cornerRadius = 15
        self.containerView.clipsToBounds = true
        
        self.subjectNotFoundLabel.isHidden = true
        
        searchTextField.placeholder = "Search \(searchFor ?? "")"
        
        searchTextField.becomeFirstResponder()
        
    }

    fileprivate func configureDropDown(){
        
       let appearance = DropDown.appearance()
        
        appearance.cellHeight = 72
        appearance.backgroundColor = UIColor(r: 242, g: 242, b: 242, a: 1)//UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor(r: 217, g: 217, b: 217, a: 1)//UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
        
        dropDown.anchorView = containerView
        dropDown.width = containerView.frame.width
        dropDown.bottomOffset = CGPoint(x: 0, y: containerView.bounds.height)
        dropDown.dataSource = _city_addresses
        dropDown.direction = .bottom
        
        
        dropDown.cellNib = UINib(nibName: "DropDownTableViewCell", bundle: nil)
        
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            
            guard let cell = cell as? DropDownTableViewCell else { return }
            
            cell.locationImage.isHidden = true
            
            cell.sepratorView.backgroundColor = UIColor(r: 228, g: 228, b: 228, a: 1)
            
            // Setup your custom UI components
            if self._city_addresses.count>0{
                
                cell.addressLabel.text = self._city_addresses[index]
                
            }
            
        }
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
             self.view.endEditing(true)
            
            self.schoolName = item
            
            self.updateDataInTextField(true)
            
            self.qualificationsNExperiencesVC?.UpdateCellWithSubjectOrSchhol(self.row, self.section, item, self.textFieldTag)
            
             self.dismiss(animated: true)
         
        }
        
    }
    
    func updateDataInTextField(_ fromDropDown: Bool){
        
        fromDropDown ? (searchTextField.text = schoolName) : (schoolName = searchTextField.text!)
        
        searchTextField.resignFirstResponder()
       
    }
    
}

extension SearchSubjectViewController: UISearchDisplayDelegate{
    
    private func searchSubjects(searchString: String){
        if let user = AppDelegateConstant.user, let subjects = user.subjects{
            
            if subjects.count>0 {
                
                let subjectName = subjects.compactMap{$0.name}
                
                let filteredStrings = subjectName.filter({(item: String) -> Bool in
                    
                    let stringMatch = item.lowercased().range(of: searchString.lowercased())//item.lowercaseString.rangeOfString(searchString.lowercased())
                    
                    return stringMatch != nil ? true : false
                })
                
                self._city_addresses = filteredStrings
                
                 self.subjectNotFoundLabel.isHidden = true
                
            }else{
                
                self._city_addresses = []
                
                self.subjectNotFoundLabel.isHidden = false
                
            }
            
        }
    }
    
    private func getPlaces(searchString: String) {
        
        if !isConnectedToNetwork(){
            self.showNoInternetMessage()
            return
        }
        
        var params = [String:AnyObject]()
    
        params["keyword"] = searchString as AnyObject
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let loginObserver = ApiManager.shared.apiService.getSchoolList(params as [String : AnyObject])
        
        let loginDisposable = loginObserver.subscribe(onNext: {(cityList) in
            
            DispatchQueue.main.async{
                
                Utilities.hideHUD(forView: self.view)
                
                self._city_addresses = cityList.schoolName
                
                if cityList.schoolName.count > 0{
                    self.subjectNotFoundLabel.isHidden = true
                }else{
                    self.subjectNotFoundLabel.isHidden = false
                }
               
            }
        }, onError: {(error) in
            
            DispatchQueue.main.async(execute: {
                
                Utilities.hideHUD(forView: self.view)
                
                if let error_ = error as? ResponseError {
                    
                    self.showSimpleAlertWithMessage(error_.description())
                    
                } else {
                    
                    if !error.localizedDescription.isEmpty {
                        
                        self.showSimpleAlertWithMessage(error.localizedDescription)
                        
                    }
                    
                }
                
            })
            
        })
        
        loginDisposable.disposed(by: disposableBag)
    }
    
}
