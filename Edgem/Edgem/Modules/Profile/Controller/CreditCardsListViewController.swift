//
//  CreditCardsListViewController.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 18/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import RxSwift

class CreditCardsListViewController:  BaseViewController {
    
    var activeTextField: UITextField!
    var index =  0{
        didSet{
            let indexPath = IndexPath(row: 1, section: 0)
            tableView.beginUpdates()
            tableView.reloadRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        }
    }
    var paymentCards:[StoreUserCard] = Array(){
        didSet{
            DispatchQueue.main.async {
                self.noRecordStack.isHidden = self.paymentCards.count > 0 ? true : false
                self.addCardBtn.isHidden = self.paymentCards.count >= 3 ? true : false
                self.addCardButton.isHidden = self.paymentCards.count >= 3 ? true : false
                self.tableView.reloadData()
            }
        }
    }
    
     var user: User?
    // var stripeUtils = StripeUtils()
    
    var disposableBag = DisposeBag()
    
    //  MARK: ------ @IBOutlets
    
    @IBOutlet weak var topViewOfTable: UIView!{
        didSet{
            topViewOfTable.isHidden = !(paymentCards.count>0)
        }
    }
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    @IBOutlet weak var noRecordStack: UIStackView!
    @IBOutlet weak var addCardBtn: UIButton!
    
    @IBOutlet weak var addCardButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let userCards = AppDelegateConstant.user?.paymentCard{
            paymentCards = userCards
        }
      
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
          fetchCardsDetailFromStripe()
    }
    
    //  MARK: ------ @IBActions
    
    @IBAction func addCardButtonTapped(_ sender: UIButton) {
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        
        let addNewCardVC = AddNewCardViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        addNewCardVC.addNewCardViewControllerDelegate = self
        self.navigationController?.pushViewController(addNewCardVC, animated: true)
        
    }
    
      //  MARK: ------ Private Functions
    
    
    /// This function will get card details from stripe
    fileprivate func fetchCardsDetailFromStripe(){
    
        guard let cus_id = AppDelegateConstant.user?.btCustomerID else{return}
        
        // initialige customer id in StripeUtils which is needed to get card list
        //self.stripeUtils.customerID = cus_id
        StripeUtils.shared.customerID = cus_id
        
        StripeUtils.shared.getCardsList { (result) in
            // check card array available or not
            if let result = result{
                // check user have any card or not
                if result.count > 0{
                    // temp StoreUserCard variable
                    var storeUserCardObject =  [StoreUserCard]()
                    
                    result.forEach({ (cardData) in
                           storeUserCardObject.append(StoreUserCard(data: cardData))
                    })
                    
                    DispatchQueue.main.async {
                        if (self.paymentCards != storeUserCardObject) {
                            self.paymentCards = storeUserCardObject
                            AppDelegateConstant.user?.paymentCard = storeUserCardObject
                            self.tableView.reloadData()
                        }
                    }
                }else{
                     print(value: "user does not have any card")
                }
                
            }else{
                print(value: "Card arr not available")
            }
        }
    }
    
    /// This function will delete cards from stripe
    func deleteCards(_ card: StoreUserCard, index: Int) {
        
        // initialize alert default controller
        let alert = UIAlertController(title: AppName, message: "Are you sure you want to remove this card?".localized, preferredStyle: .alert)
        
        // on tapping yes button
        let yesAction = UIAlertAction(title: AlertButton.yes.description(), style: .default, handler: { (action) in
            
            // customer id
            guard let cus_id = AppDelegateConstant.user?.btCustomerID else{return}
            
            // card id
            guard let card_id = card.cardID else{return}
            
             Utilities.showHUD(forView: self.view, excludeViews: [])
            
            StripeUtils.shared.removeCardFromStripe(customerID: cus_id, cardID: card_id) { (deletedCardDetail) in
            DispatchQueue.main.async {
 
                Utilities.hideHUD(forView: self.view)
                
                // On succes we show pop-up for successful deletion
                let alert = UIAlertController(title: AppName, message: "Card successfully deleted.".localized, preferredStyle: .alert)
        
                // getting card-id from stripe
                if let cardDetail = deletedCardDetail, let cardID = cardDetail["id"] as? String{
                    
                    //remove deleted card from local array
                    AppDelegateConstant.user?.paymentCard?.removeAll{$0.cardID == cardID}
                    
                    // updating current view
                    if let userCards = AppDelegateConstant.user?.paymentCard{
                        self.paymentCards = userCards
                        if self.paymentCards.count > 1 {
                            self.index = index-1
                        }else if self.paymentCards.count == index{
                            
                        }
                    }
                }
                // on tap ok button
                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
            }
        })
        let noAction = UIAlertAction(title: AlertButton.no.description(), style: .cancel, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
        
        

    }
    
    func configureTextField(_ textField: EdgemCustomTextField){
        textField.isUserInteractionEnabled = false
        textField.delegate = self
    }

}

extension CreditCardsListViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        activeTextField = textField as! EdgemCustomTextField
         managePickerView()
    }
}

// MARK: --------- Delegates

extension CreditCardsListViewController: PaymentCardTableViewCellDelegate{
    
    func indexOfCard(_ index: Int) {
        self.index = index
    }
    
    
}


// MARK: --------- Delegates

extension CreditCardsListViewController: AddNewCardViewControllerDelegate {
    
    func getCardDetail(_ cardDetail: StoreUserCard) {
        
        AppDelegateConstant.user?.paymentCard?.append(cardDetail)
        if let userCards = AppDelegateConstant.user?.paymentCard{
            paymentCards = userCards
        }
        
    }

}

// MARK: --------- Table View Methods

extension CreditCardsListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentCards.count > 0 ? 2 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentCardTableViewCell.cellIdentifire(), for: indexPath) as? PaymentCardTableViewCell  else{
                fatalError("Could not load PaymentCardTableViewCell")
            }
            cell.deletePaymentCard = { [weak self] card, index in
                self?.deleteCards(card,index: index)
            }
            cell.paymentCardTableViewCellDelegate = self
            cell.configureCell(with: paymentCards)
            return cell
        }else{
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentCardDetailTableViewCell.cellIdentifire(), for: indexPath) as? PaymentCardDetailTableViewCell  else{
                fatalError("Could not load PaymentCardTableViewCell")
            }
            if self.index < paymentCards.count{
                  cell.configureCell(with: paymentCards[self.index])
            }
            configureTextField(cell.cardNumberTextField)
            configureTextField(cell.cardHolderNameTextField)
            configureTextField(cell.MMTextField)
            configureTextField(cell.YYTextField)
            //configureTextField(cell.CVVTextField)
            
            cell.monthPicker.delegate = self
            cell.yearPicker.delegate = self
            
            return cell
            
        }
      
    }
}

// MARK: --------- Picker view delegate

extension CreditCardsListViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        
        let indexPath = IndexPath(row: 1, section: 0)
        guard let cell = tableView.cellForRow(at: indexPath) as? PaymentCardDetailTableViewCell else{return}
        
        let textField = activeTextField as! EdgemCustomTextField
        
        cell.monthPicker.reloadAllComponents()
        cell.monthPicker.reloadInputViews()
        
         cell.yearPicker.reloadAllComponents()
         cell.yearPicker.reloadInputViews()
        
        
        if textField.tag == 3 {
            
            let index =  cell.month.index(of: cell.storeUserCardObject.expiryMonth)
            if let _index = index {
                 cell.monthPicker.selectRow(_index, inComponent: 0, animated: true)
            } else {
                 cell.monthPicker.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text =  cell.month[0]
            }
            
        }else if textField.tag == 4{
            
            let index =  cell.newYearArr.index(of: cell.storeUserCardObject.expiryYear)
            if let _index = index {
                cell.yearPicker.selectRow(_index, inComponent: 0, animated: true)
            } else {
                 cell.yearPicker.selectRow(0, inComponent: 0, animated: true)
                activeTextField.text =  cell.newYearArr[0]
            }
            
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        let indexPath = IndexPath(row: 1, section: 0)
        guard let cell = tableView.cellForRow(at: indexPath) as? PaymentCardDetailTableViewCell else{fatalError()}
        
        if pickerView == cell.monthPicker{
            return cell.month.count
        }else if pickerView == cell.yearPicker{
            return cell.newYearArr.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //return month[row]
        let indexPath = IndexPath(row: 1, section: 0)
        guard let cell = tableView.cellForRow(at: indexPath) as? PaymentCardDetailTableViewCell else{fatalError()}
        
        if pickerView ==  cell.monthPicker{
            return  cell.month[row]
        }else if pickerView ==  cell.yearPicker{
            return  cell.newYearArr[row]
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let indexPath = IndexPath(row: 1, section: 0)
        guard let cell = tableView.cellForRow(at: indexPath) as? PaymentCardDetailTableViewCell else{fatalError()}
        
        if pickerView ==  cell.monthPicker{
            
             cell.selectedMonth =  cell.month[row]
             cell.MMTextField.text =  cell.month[row]
            
        }else if pickerView ==  cell.yearPicker{
            
             cell.selecetedYear =  cell.newYearArr[row]
             cell.YYTextField.text =  cell.newYearArr[row]
        }
        
    }
}


// MARK: ------- API Implementation

extension CreditCardsListViewController {
    
//    func removeCards(_ cardID: Int, _ index: Int){
//
//        if !isConnectedToNetwork(){
//            self.showNoInternetMessage()
//            return
//        }
//
//
//        let removeCardsObserver = ApiManager.shared.apiService.removeUserPaymentCardDetails("\(cardID)")
//        let removeCardsDisposable = removeCardsObserver.subscribe(onNext: {(msg) in
//
//            Utilities.hideHUD(forView: self.view)
//            DispatchQueue.main.async {
//
//                let alert = UIAlertController(title: AppName, message: msg.localized, preferredStyle: .alert)
//                let okAction = UIAlertAction(title: AlertButton.ok.description(), style: .default, handler: { (action) in
//                    AppDelegateConstant.user?.paymentCard?.removeAll{$0.cardID == cardID}
//                    if let userCards = AppDelegateConstant.user?.paymentCard{
//                        self.paymentCards = userCards
//                        if self.paymentCards.count > 1 {
//                            self.index = index-1
//                        }else if self.paymentCards.count == index{
//
//                        }
//                    }
//                })
//                alert.addAction(okAction)
//                self.present(alert, animated: true, completion: nil)
//            }
//        },onError: {(error) in
//            DispatchQueue.main.async(execute: {
//                Utilities.hideHUD(forView: self.view)
//                if let error_ = error as? ResponseError {
//                    self.showSimpleAlertWithMessage(error_.description())
//                } else {
//                    if !error.localizedDescription.isEmpty {
//                        self.showSimpleAlertWithMessage(error.localizedDescription)
//                    }
//                }
//            })
//        }
//        )
//        removeCardsDisposable.disposed(by: disposableBag)
//    }
    
//    func fetchCardsDetail(){
//
//        if !isConnectedToNetwork(){
//            self.showNoInternetMessage()
//            return
//        }
//
//        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
//        dispatchQueue.async{
//            //Time consuming task here
//            let userObserver = ApiManager.shared.apiService.fetchUserPaymentCardDetails()
//            let userDisposable = userObserver.subscribe(onNext: {(cards) in
//                DispatchQueue.main.async {
//
//                    if (self.paymentCards != cards) {
//                        self.paymentCards = cards
//                        AppDelegateConstant.user?.paymentCard = cards
//                        self.tableView.reloadData()
//
//                    }
//                }
//            }, onError: {(error) in
//                DispatchQueue.main.async(execute: {
//                    if let error_ = error as? ResponseError {
//                        self.showSimpleAlertWithMessage(error_.description())
//                    } else {
//                        if !error.localizedDescription.isEmpty {
//                            self.showSimpleAlertWithMessage(error.localizedDescription)
//                        }
//                    }
//                    // self.fetchDashboard()
//                })
//            })
//            userDisposable.disposed(by: self.disposableBag)
//        }
//
//
//    }

}

