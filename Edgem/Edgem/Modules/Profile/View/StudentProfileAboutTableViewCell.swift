//
//  StudentProfileAboutTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 11/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class StudentProfileAboutTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dashedView: UIView!
    
    @IBOutlet weak var dualLabelOne: UILabel!
    @IBOutlet weak var dualLabelTwo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      drawDottedLine(start: CGPoint(x: dashedView.bounds.minX, y: dashedView.bounds.minY), end: CGPoint(x: ScreenWidth, y: dashedView.bounds.minY), view: dashedView)
    }
    
    class func cellIdentifire()->String {
    return "StudentProfileAboutTableViewCell"
    }
    
    func configureCellWithLevel(_ levels: [Level]){
        titleLabel.text = "Level"
        if levels.count > 0{
            let levelTitle = levels.map { bullet + " " + $0.name! }
            self.contentLabel.text = levelTitle.joined(separator: "\n")
        }else{
            self.contentLabel.text = "No Level"
        }
    }
    
    func configureCellWithSubject(_ subjects: [Subject]){
        titleLabel.text = "Subject"
        if subjects.count > 0{
            let subjectTitle = subjects.map { $0.name! }
            self.contentLabel.text = subjectTitle.joined(separator: "\n")
        }else{
            self.contentLabel.text = "No Subject"
        }
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView){
        let shapelayer = CAShapeLayer()
        shapelayer.strokeColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1).cgColor
        shapelayer.lineWidth = 1
        shapelayer.lineDashPattern = [7, 3]
        
        let path =  CGMutablePath()
        path.addLines(between: [p0, p1])
        shapelayer.path = path
        view.layer.addSublayer(shapelayer)
        
    }

}
