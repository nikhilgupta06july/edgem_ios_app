//
//  CheckBoxCell.swift
//  Edgem
//
//  Created by Namespace on 10/09/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class CheckBoxCellInSubjectAndRate: UITableViewCell {
    
    
    // MARK: - Properties
    
    // MARK:- @IBOutlets
    
    @IBOutlet weak var subjectNameLabel: UILabel!
    @IBOutlet weak var checkBoxBtn: UIButton!
    @IBOutlet weak var sepratorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCellWithSubject(textString: String, isSelected: Bool, index: Int){
        
        self.subjectNameLabel.text = textString
        
        self.checkBoxBtn.tag = index
        
        self.backgroundColor = isSelected ? cellBackgroundColor : UIColor.white
  
        self.checkBoxBtn.setImage((isSelected ? #imageLiteral(resourceName: "selected") : #imageLiteral(resourceName: "checkboxInactive")), for: .normal)

    }
    
    class func cellIdentifire() -> String{
        return "CheckBoxCellInSubjectAndRate"
    }
    
    class func cellHeight() -> CGFloat{
        return 55
    }
}
