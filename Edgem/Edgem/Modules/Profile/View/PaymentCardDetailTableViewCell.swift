//
//  PaymentCardDetailTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 26/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class PaymentCardDetailTableViewCell: UITableViewCell {
    
    // MARK: ---------- Properties
    
    var previousTextFieldContent: String?
    var previousSelection: UITextRange?
    
    var errorDict = ["CardNumber": "", "CardHolderName": "", "Month": "", "Year": "", "CVV": ""]
    
    var month = [ "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
    
    var year: [Int] = Array()
    var newYearArr: [String] = Array()
    
    let date = Date()
    let calendar = Calendar.current
    
    var fromYear: Int = 0
    var toYear: Int = 0
    
    var activeTextField: UITextField!
    
    var selectedMonth: String?
    var selecetedYear: String?
    
    let monthPicker = UIPickerView()
    let yearPicker = UIPickerView()
    
    var storeUserCardObject = StoreUserCard()
    
    //MARK: ---------- @IBOutlets
    
    @IBOutlet weak var cardNumberTextField: EdgemCustomTextField!
    @IBOutlet weak var cardNumberErrorLabel: UILabel!
    @IBOutlet weak var cardHolderNameTextField: EdgemCustomTextField!
    @IBOutlet weak var cardHolderNameErrorLabel: UILabel!
    @IBOutlet weak var MMTextField: EdgemCustomTextField!
    @IBOutlet weak var YYTextField: EdgemCustomTextField!
   // @IBOutlet weak var CVVTextField: EdgemCustomTextField!
    @IBOutlet weak var MYCvErrorLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        MMTextField.titleFormatter = { (text: String) -> String in
            return text.uppercased()
        }
        YYTextField.titleFormatter = { (text: String) -> String in
            return text.uppercased()
        }
        
//        CVVTextField.titleFormatter = { (text: String) -> String in
//            return text.uppercased()
//        }
        
        fromYear = calendar.component(.year, from: date)
        toYear = fromYear + 20
        for year in fromYear...toYear{
            self.year.append(year)
        }
        
        newYearArr = self.year.map { String("\($0)".dropFirst(2))}
        
        initialSetUP()
        createMonthPicker()
        createYearPicker()
        
    }
    
    func configureCell(with cardDetail: StoreUserCard){
        
        let last4 = cardDetail.cardNumber!.suffix(4)
        cardNumberTextField.text = "****  ****  ****  \(last4)"
        cardHolderNameTextField.text = cardDetail.nameOnCard
        if let month = cardDetail.expiryMonth{
            MMTextField.text  = month.count == 1 ? "0\(month)" : month
        }
        //MMTextField.text = cardDetail.expiryMonth
        if let yr = cardDetail.expiryYear{
            let year = Int(yr)! % 100
            YYTextField.text = yr.count == 4 ? "\(year)" : yr
        }
        //YYTextField.text = cardDetail.expiryYear
      
    }

    class func cellIdentifire() -> String {
        return "PaymentCardDetailTableViewCell"
    }
    
    func initialSetUP(){
        
        cardNumberTextField.tag = 1
        cardNumberTextField.placeholder = "Card Number"
        cardNumberTextField.title = "Card Number"
        cardNumberTextField.textFont = textFieldMediumFont
        cardNumberTextField.placeholderFont = textFieldDefaultFont
        cardNumberTextField.titleFont = textFieldDefaultFont
        cardNumberTextField.keyboardType = .numberPad
        
        // cardHolderNameTextField properties
        
        cardHolderNameTextField.tag = 2
        cardHolderNameTextField.placeholder = "Card Holder’s Name"
        cardHolderNameTextField.title = "Card Holder’s Name"
        cardHolderNameTextField.textFont = textFieldMediumFont
        cardHolderNameTextField.placeholderFont = textFieldDefaultFont
        cardHolderNameTextField.titleFont = textFieldDefaultFont
        cardHolderNameTextField.keyboardType = .asciiCapable
        
        // MM properties
        MMTextField.tag = 3
        MMTextField.placeholder = "MM"
        MMTextField.title = "MM".capitalized
        MMTextField.textFont = textFieldMediumFont
        MMTextField.placeholderFont = textFieldDefaultFont
        MMTextField.titleFont = textFieldDefaultFont
        // MMTextField.inputView = datePicker
        MMTextField.keyboardType = .numberPad
        
        // YY properties
        YYTextField.tag = 4
        YYTextField.placeholder = "YY"
        YYTextField.title = "YY"
        YYTextField.textFont = textFieldMediumFont
        YYTextField.placeholderFont = textFieldDefaultFont
        YYTextField.titleFont = textFieldDefaultFont
        //YYTextField.inputView = datePicker
        YYTextField.keyboardType = .numberPad
        
        // CVV properties
//        CVVTextField.tag = 5
//        CVVTextField.placeholder = "CVV"
//        CVVTextField.title = "CVV"
//        CVVTextField.textFont = textFieldMediumFont
//        CVVTextField.placeholderFont = textFieldDefaultFont
//        CVVTextField.titleFont = textFieldDefaultFont
//        CVVTextField.keyboardType = .numberPad
//        CVVTextField.isSecureTextEntry = true
        
        
    }
    
    // create month picker
    func createMonthPicker(){
        //monthPicker.delegate = self
        MMTextField.inputView = monthPicker
    }
    
    // create Year picker
    func createYearPicker(){
       // yearPicker.delegate = self
        YYTextField.inputView = yearPicker
    }
    
    func removeNonDigits(string: String, andPreserveCursorPosition cursorPosition: inout Int) -> String {
        var digitsOnlyString = ""
        let originalCursorPosition = cursorPosition
        
        for i in Swift.stride(from: 0, to: string.count, by: 1) {
            let characterToAdd = string[string.index(string.startIndex, offsetBy: i)]
            if characterToAdd >= "0" && characterToAdd <= "9" {
                digitsOnlyString.append(characterToAdd)
            }
            else if i < originalCursorPosition {
                cursorPosition -= 1
            }
        }
        
        return digitsOnlyString
    }
    
    func insertCreditCardSpaces(_ string: String, preserveCursorPosition cursorPosition: inout Int) -> String {
        // Mapping of card prefix to pattern is taken from
        // https://baymard.com/checkout-usability/credit-card-patterns
        
        // UATP cards have 4-5-6 (XXXX-XXXXX-XXXXXX) format
        let is456 = string.hasPrefix("1")
        
        // These prefixes reliably indicate either a 4-6-5 or 4-6-4 card. We treat all these
        // as 4-6-5-4 to err on the side of always letting the user type more digits.
        let is465 = [
            // Amex
            "34", "37",
            
            // Diners Club
            "300", "301", "302", "303", "304", "305", "309", "36", "38", "39"
            ].contains { string.hasPrefix($0) }
        
        // In all other cases, assume 4-4-4-4-3.
        // This won't always be correct; for instance, Maestro has 4-4-5 cards according
        // to https://baymard.com/checkout-usability/credit-card-patterns, but I don't
        // know what prefixes identify particular formats.
        let is4444 = !(is456 || is465)
        
        var stringWithAddedSpaces = ""
        let cursorPositionInSpacelessString = cursorPosition
        
        for i in 0..<string.count {
            let needs465Spacing = (is465 && (i == 4 || i == 10 || i == 15))
            let needs456Spacing = (is456 && (i == 4 || i == 9 || i == 15))
            let needs4444Spacing = (is4444 && i > 0 && (i % 4) == 0)
            
            if needs465Spacing || needs456Spacing || needs4444Spacing {
                stringWithAddedSpaces.append(" ")
                
                if i < cursorPositionInSpacelessString {
                    cursorPosition += 1
                }
            }
            
            let characterToAdd = string[string.index(string.startIndex, offsetBy:i)]
            stringWithAddedSpaces.append(characterToAdd)
        }
        
        return stringWithAddedSpaces
    }
    
    
    @IBAction func textFieldAction(_ sender: EdgemCustomTextField) {
        if sender.tag == 1{
            
            var targetCursorPosition = 0
            if let startPosition = sender.selectedTextRange?.start {
                targetCursorPosition = sender.offset(from: sender.beginningOfDocument, to: startPosition)
            }
            
            var cardNumberWithoutSpaces = ""
            if let text = sender.text {
                cardNumberWithoutSpaces = self.removeNonDigits(string: text, andPreserveCursorPosition: &targetCursorPosition)
            }
            
            if cardNumberWithoutSpaces.count > 19 {
                sender.text = previousTextFieldContent
                sender.selectedTextRange = previousSelection
                return
            }
            
            let cardNumberWithSpaces = self.insertCreditCardSpaces(cardNumberWithoutSpaces, preserveCursorPosition: &targetCursorPosition)
            sender.text = cardNumberWithSpaces
            
            if let targetPosition = sender.position(from: sender.beginningOfDocument, offset: targetCursorPosition) {
                sender.selectedTextRange = sender.textRange(from: targetPosition, to: targetPosition)
            }
            
            //let (type, formatted, valid) = checkCardNumber(input: "\(sender.text!)")
            //changeCardImage("\(type)")
            //print(type,formatted,valid)
            
        }
        
    }
    
}
