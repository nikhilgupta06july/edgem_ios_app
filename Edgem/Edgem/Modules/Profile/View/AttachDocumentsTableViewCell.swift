//
//  AttachDocumentsTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 16/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import AMPopTip

protocol AttachDocumentsTableViewCellDelegate {
    func popTipTapped(senderFrame:CGRect,cell:AttachDocumentsTableViewCell,btn:UIButton)
}

class AttachDocumentsTableViewCell: UITableViewCell {

    // MARK: ----------- Properties
    
    let popTip = PopTip()
    let setUpToolTip = SetUpToolTip()
    var direction = PopTipDirection.up
    var topRightDirection = PopTipDirection.up
    
     var delegate:AttachDocumentsTableViewCellDelegate?
    
      // MARK: ----------- @IBActions
    
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var sectionHeaderTitle: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var innerContainerView: UIView!
    @IBOutlet weak var attachCertificate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setUpToolTip.initialSetUp(popTip)
        
        popTip.appearHandler = { popTip in
            //self.infoButton.setImage(UIImage(named: "infoImageSelected"), for: .normal)
        }
        popTip.dismissHandler = { popTip in
            self.infoButton.setImage(UIImage(named: "infoImage"), for: .normal)
        }
    }

    class func cellidentifire() ->String {
        return "AttachDocumentsTableViewCell"
    }
    
    func configureCell(_ title: String) {
        sectionHeaderTitle.text = title.localized
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // containerView shadow properties
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.1
        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerView.layer.shadowRadius = 4
        containerView.layer.masksToBounds = false
    }

    @IBAction func infoButtonTapped(_ sender: UIButton) {
        
        delegate?.popTipTapped(senderFrame: sender.frame, cell: self, btn: sender)
        
        //delegate?.popTipTapped(senderFrame: sender.frame, cell: self, btn: sender)
    
//        popTip.show(text: ToolTipString.info.description(), direction: .up, maxWidth: self.contentView.frame.width-40, in: self.innerContainerView, from: sender.frame)
//        if sender.isSelected == false{
//            sender.isSelected = true
//            self.infoButton.setImage(UIImage(named: "infoImageSelected"), for: .normal)
//        }else{
//             sender.isSelected = false
//            self.infoButton.setImage(UIImage(named: "infoImageSelected"), for: .normal)
//        }
        
    }
}
