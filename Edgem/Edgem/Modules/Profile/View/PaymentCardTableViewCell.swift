//
//  PaymentCardTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 26/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol PaymentCardTableViewCellDelegate: class{
    func indexOfCard(_ index: Int)
}

class PaymentCardTableViewCell: UITableViewCell {
    
    // MARK: ------Properties
    
    weak var paymentCardTableViewCellDelegate: PaymentCardTableViewCellDelegate?
    private var indexOfCellBeforeDragging = 0
    
    var paymentCard: [StoreUserCard] = Array(){
        didSet{
            collectionView.reloadData()
        }
    }
    
    var deletePaymentCard: ((StoreUserCard, Int)->Void)?
    
      // MARK: ------@IBOutlets
    
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let button:UIButton = UIButton(frame: CGRect(x: (ScreenWidth/2)-30, y: 185, width: 60, height: 60))
        button.backgroundColor = .clear
        button.setTitle("Button", for: .normal)
        button.setImage(UIImage(named: "deleteCards"), for: .normal)
        button.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        self.addSubview(button)
        
        collectionViewFlowLayout.minimumLineSpacing = 0
       
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//         configureCollectionViewLayoutItemSize()
//    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
           configureCollectionViewLayoutItemSize()
    }
    
    

    

    class func cellIdentifire() -> String{
        return "PaymentCardTableViewCell"
    }
    
    func configureCell(with card: [StoreUserCard]) {
        self.paymentCard = card
    }
    
    private func calculateSectionInset() -> CGFloat {
        return 30
    }
    
    @objc func buttonClicked(){

        let card = self.paymentCard[indexOfMajorCell()]
        deletePaymentCard?(card, indexOfMajorCell())
        
    }
    
    private func configureCollectionViewLayoutItemSize() {
        let inset: CGFloat = calculateSectionInset() // This inset calculation is some magic so the next and the previous cells will peek from the sides. Don't worry about it
        collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        
        collectionViewFlowLayout.itemSize = CGSize(width: collectionViewFlowLayout.collectionView!.frame.size.width - inset * 2, height: collectionViewFlowLayout.collectionView!.frame.size.height-24)
        collectionViewFlowLayout.collectionView!.reloadData()
    }
    
    fileprivate func indexOfMajorCell() -> Int {
        let itemWidth = collectionViewFlowLayout.itemSize.width
        let proportionalOffset = collectionViewFlowLayout.collectionView!.contentOffset.x / itemWidth
        let index = Int(round(proportionalOffset))
        let safeIndex = max(0, min(paymentCard.count - 1, index))
        return safeIndex
    }

}

extension PaymentCardTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paymentCard.count > 0 ? paymentCard.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PaymentCardCollectionViewCell.cellIdentifire(), for: indexPath) as! PaymentCardCollectionViewCell
        cell.configureCell(with: paymentCard[indexPath.item])
        return cell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        indexOfCellBeforeDragging = indexOfMajorCell()
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        // Stop scrollView sliding:
        targetContentOffset.pointee = scrollView.contentOffset
        
        // calculate where scrollView should snap to:
        let indexOfMajorCell = self.indexOfMajorCell()
        
        // calculate conditions:
        let swipeVelocityThreshold: CGFloat = 0.5 // after some trail and error
        let hasEnoughVelocityToSlideToTheNextCell = indexOfCellBeforeDragging + 1 < paymentCard.count && velocity.x > swipeVelocityThreshold
        let hasEnoughVelocityToSlideToThePreviousCell = indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
        let majorCellIsTheCellBeforeDragging = indexOfMajorCell == indexOfCellBeforeDragging
        let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)
        
        if didUseSwipeToSkipCell {
            
            let snapToIndex = indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
            let toValue = collectionViewFlowLayout.itemSize.width * CGFloat(snapToIndex)
            
            // Damping equal 1 => no oscillations => decay animation:
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: velocity.x, options: .allowUserInteraction, animations: {
                scrollView.contentOffset = CGPoint(x: toValue, y: 0)
                scrollView.layoutIfNeeded()
            }, completion: nil)
            
        } else {
            // This is a much better way to scroll to a cell:
            let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
            collectionViewFlowLayout.collectionView!.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        paymentCardTableViewCellDelegate?.indexOfCard(indexOfMajorCell())
    }
    
    

}

