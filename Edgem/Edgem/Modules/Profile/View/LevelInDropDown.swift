//
//  LevelInDropDown.swift
//  Edgem
//
//  Created by Namespace on 19/09/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class LevelInDropDown: UITableViewCell {

    @IBOutlet weak var levelTitleLabel: UILabel!
    @IBOutlet weak var levelRateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func cellIdentifire()->String{
        return "LevelInDropDown"
    }
    
    func configureCell(With level: Level){
        levelTitleLabel.text = level.name+":"
        if let price = level.price{
            levelRateLabel.text = "$"+((price as NSString).doubleValue).isWholeNumber()+"/hr"
        }
    }
    
    class func cellHeight()->CGFloat{
        return 30
    }
    
    

}
