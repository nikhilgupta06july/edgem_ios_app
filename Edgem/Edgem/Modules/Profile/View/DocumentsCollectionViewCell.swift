//
//  ShowDocumentsCollectionViewCell.swift
//  Edgem
//
//  Created by Namespace on 16/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class DocumentsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var documentImage: UIImageView!
    @IBOutlet weak var imageHolderView: UIView!
    
    class func cellIdentifire() -> String{
        return "DocumentsCollectionViewCell"
    }
    
    func configureCell(with image: UIImage){
        activityIndicator.hidesWhenStopped = true
        if image.size.width > 0{
            activityIndicator.stopAnimating()
        }else{
             activityIndicator.startAnimating()
        }
        self.documentImage.image = image
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        documentImage.image = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageHolderView.layer.cornerRadius = 4
        imageHolderView.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1).cgColor
        imageHolderView.layer.shadowOpacity = 0.9
        imageHolderView.layer.shadowOffset = .zero
        imageHolderView.layer.shadowRadius = 4
        imageHolderView.layer.masksToBounds = false
       
        
    }
    
    
    
}
