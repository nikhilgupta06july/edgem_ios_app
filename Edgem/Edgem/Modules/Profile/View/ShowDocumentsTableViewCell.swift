//
//  ShowDocumentsTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 16/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class ShowDocumentsTableViewCell: UITableViewCell {
    
    // MARK: ------- Properties
    var documentImage: [UIImage] = Array(){
        didSet{
            //collectionView.reloadData()
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!{
        
        didSet{
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func configureCell(with documentImage: [UIImage]){
        //self.documentImage.append(documentImage)
         self.documentImage = documentImage.map {return $0}
         collectionView.reloadData()
        
    }
    
    class func cellIdentifire() -> String{
        return "ShowDocumentsTableViewCell"
    }

}

extension ShowDocumentsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.documentImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ShowDocumentsCollectionViewCell.cellIdentifire(), for: indexPath) as! ShowDocumentsCollectionViewCell
        cell.configureCell(with: self.documentImage[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 213, height: 108)
    }
   
}

