//
//  ProfileHeaderTableViewCell.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 19/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//
import UIKit

class ProfileHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var editProfileBtn: UIButton!
    @IBOutlet weak var editProfileImageBtn: UIButton!
    @IBOutlet weak var profileHolderView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userTypeLbl: UILabel!
    
    var tutorProfileImage: ((UIImage)->Void)?
    
    class func cellIdentifier() -> String {
        return "ProfileHeaderTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        profileHolderView.layer.borderWidth = 2.0
        profileHolderView.layer.borderColor = theamBlackColor.cgColor
    }
    
    func configureCell(){
        
        editProfileBtn.isHidden = UserStore.shared.selectedUserType != tutorType
        
        userTypeLbl.text = UserStore.shared.selectedUserType
        
      // [START User Name]
        var firstName = ""
        var lastName = ""
//        if let fname = AppDelegateConstant.user?.firstName{
//            firstName = fname
//        }
//        if let lName = AppDelegateConstant.user?.lastName{
//            lastName = lName
//        }
//        nameLabel.text = "\(firstName) \(lastName)"
        
        let userType = UserStore.shared.selectedUserType
    
        if userType == "Parent"{
            
            if  UserStore.shared.logInType == 1{
                 if let fName = AppDelegateConstant.user?.parentFirstName{
                                firstName = fName
                 }
                if let lName = AppDelegateConstant.user?.parentLastName{
                                lastName = lName
                 }
            } else if  UserStore.shared.logInType == 2{
                 if let fName = AppDelegateConstant.user?.firstName{
                                firstName = fName
                 }
                if let lName = AppDelegateConstant.user?.lastName{
                                lastName = lName
                 }
            }
        }else{
            if let fName = AppDelegateConstant.user?.firstName{
                firstName = fName
            }
            if let lName = AppDelegateConstant.user?.lastName{
                lastName = lName
            }
            
        }
        
        // name
        nameLabel.text = "\(firstName) \(lastName)"
        // [END User Name]

        // [START Profile Image]
        if let imageStr = AppDelegateConstant.user?.imageURL, imageStr != ""{
            GetImageFromServer.getImage(imageURL: imageStr) { (image) in
                self.profileImageView.image = image
            }
        }else{
                profileImageView.image = UIImage(named: "femaleIcon")
        }
        // [END Profile Image]

    }
    
}
