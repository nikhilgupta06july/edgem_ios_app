//
//  ShowHideTableViewCellTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 04/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol ShowHideTableViewCellDelegate{
    func showPasswordBtnTapped(_ isSelected: Bool)
}

class ShowHideTableViewCell: UITableViewCell {
    
    var showPasswordTableViewCellDelegate: ShowHideTableViewCellDelegate?

    @IBOutlet weak var eyeButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        eyeButton.setTitle("Show password", for: .normal)
        eyeButton.setImage(UIImage(named: "open_eye"), for: .normal)
    }

    class func cellIdentifire()->String{
        return "ShowHideTableViewCell"
    }
    
    class func cellHeight()-> CGFloat{
        return 100
    }
    
    @IBAction func showPasswordBtnTapped(_ sender: UIButton) {
        
        if sender.isSelected == false{
            sender.isSelected = true
            
            eyeButton.setTitle("Hide password", for: .normal)
            eyeButton.setImage(UIImage(named: "close_eye"), for: .normal)
            
        }else{
            sender.isSelected = false
            
            eyeButton.setTitle("Show password", for: .normal)
            eyeButton.setImage(UIImage(named: "open_eye"), for: .normal)
            
        }
        
        showPasswordTableViewCellDelegate?.showPasswordBtnTapped(sender.isSelected)
    }

}
