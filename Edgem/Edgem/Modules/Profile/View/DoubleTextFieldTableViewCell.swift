//
//  DoubleTextFieldTableViewCell.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 16/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import AMPopTip

protocol DoubleTextFieldCellDelegate {
    func popTipTapped(senderFrame:CGRect,cell:DoubleTextFieldTableViewCell,btn:UIButton)
}

class DoubleTextFieldTableViewCell: UITableViewCell {
    
    let popTip = PopTip()
    let setUpToolTip = SetUpToolTip()
    //    var direction = PopTipDirection.up
    //    var topRightDirection = PopTipDirection.down
    var index: Int!
    var delegate:DoubleTextFieldCellDelegate?
    
    var switchBtnStatus: ((Bool, Int)->())?
    
    @IBOutlet weak var `switch`: UISwitch!
    @IBOutlet weak var customTextFieldOne: EdgemCustomTextField!
    @IBOutlet weak var customTextFieldTwo: EdgemCustomTextField!
    @IBOutlet weak var errorLabelOne: UILabel!
    @IBOutlet weak var errorLabelTwo: UILabel!
    @IBOutlet weak var infoBtn: UIButton!
    @IBOutlet weak var lockRateLabel: UILabel!
    @IBOutlet weak var switchContainerView: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpToolTip.initialSetUp(popTip)
        
        popTip.appearHandler = { popTip in
            self.infoBtn.setImage(UIImage(named: "tooltip_selected"), for: .normal)
        }
        popTip.dismissHandler = { popTip in
            self.infoBtn.setImage(UIImage(named: "tooltip_deselected"), for: .normal)
        }
    }
    
    class func cellIdentifire()->String {
        return "DoubleTextFieldTableViewCell"
    }
    
    func configureCellWithSubjectRateData(_ subject: Subject, _ index: Int){
        var text = ""//(subject.name == "Languages") ? "Enter a language*" : (subject.name == "Others") ? "Enter your subject*" : ""
        
        if (subject.name == "Languages"){
            text = "Enter a language*"
        }else if (subject.name == "Others"){
             text = "Enter your subject*"
        }else if (subject.name == "Music"){
             text = "Enter your instruments*"
        }
        
        self.index = index
        
        customTextFieldTwo.titleFormatter = { (text: String) -> String in
            return text
        }
        
        customTextFieldOne.tag = 1
        customTextFieldOne.placeholder = text
        customTextFieldOne.title = text
        customTextFieldOne.textFont = titleMediumFont
        customTextFieldOne.placeholderFont = textFieldMediumFont
        customTextFieldOne.titleFont = textFieldDefaultFont
        customTextFieldOne.keyboardType = .asciiCapable
        customTextFieldOne.isHidden = !((subject.name == "Languages") || (subject.name == "Others") || (subject.name == "Music"))
        errorLabelOne.text = subject.errorOne
        
        let enterAmmount = "Please enter amount (rate/hr)*"
        
        
        customTextFieldTwo.tag = 2
        customTextFieldTwo.placeholder = enterAmmount
        customTextFieldTwo.title = enterAmmount
        customTextFieldTwo.textFont = titleMediumFont
        customTextFieldTwo.placeholderFont = textFieldMediumFont
        customTextFieldTwo.titleFont = textFieldDefaultFont
        customTextFieldTwo.keyboardType = .numberPad
        errorLabelTwo.text = subject.errorTwo
        
        print(subject.lockRate)
        if subject.lockRate == "1" {
            `switch`.isOn = true
        }else{
            `switch`.isOn = false
        }
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func validateField(text: String, type: String)->String {
        if text.isEmptyString() {
            errorLabelOne.text = (type == "Languages") ? "Enter a language*" : (type == "Others") ? "Enter your subject*" : ValidationErrorMessage.sessionRate.description()
        }else {
            errorLabelOne.text = ""
            
        }
        return errorLabelOne.text!
    }
    
    @IBAction func didTapSwitch(_ sender: UISwitch) {
        print(sender.isOn)
        switchBtnStatus?(sender.isOn, self.index)
    }
    
    @IBAction func didtapInfoBtn(_ sender: UIButton) {
        
        delegate?.popTipTapped(senderFrame: sender.frame, cell: self, btn: sender)
        
        
        /*
         popTip.show(text: ToolTipString.info.description(), direction: .up, maxWidth: self.contentView.frame.width-40, in: self.switchContainerView, from: sender.frame)
         //        self.superview!.bringSubviewToFront(popTip)
         if sender.isSelected == false{
         sender.isSelected = true
         self.infoBtn.setImage(UIImage(named: "infoImageSelected"), for: .normal)
         }else{
         sender.isSelected = false
         self.infoBtn.setImage(UIImage(named: "infoImageSelected"), for: .normal)
         }
         */
        
    }
    
}
