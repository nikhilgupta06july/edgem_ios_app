//
//  OtherSubjectTextFieldInSubjectAndRate.swift
//  Edgem
//
//  Created by Namespace on 10/09/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class OtherSubjectTextFieldInSubjectAndRate: UITableViewCell {
    

    @IBOutlet weak var otherSubjectTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(with subject: Subject){

        
        if (subject.name == "Languages"){
            otherSubjectTextField.tag = 101
            otherSubjectTextField.placeholder = "Enter a language*".localized
        }else if (subject.name == "Others"){
            otherSubjectTextField.tag = 102
            otherSubjectTextField.placeholder = "Enter your subject*".localized
        }else if (subject.name == "Music"){
            otherSubjectTextField.tag = 100
            otherSubjectTextField.placeholder = "Enter your instruments*".localized
        }
        
    }
    
    class func cellIdentifire() -> String{
        return "OtherSubjectTextFieldInSubjectAndRate"
    }
    
    class func cellHeight() -> CGFloat{
        return 55
    }

}
