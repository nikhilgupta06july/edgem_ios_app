//
//  LavelWithRateInSubjectAndRate.swift
//  Edgem
//
//  Created by Namespace on 10/09/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class LavelWithRateInSubjectAndRate: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet weak var levelNameLbl: UILabel!
    @IBOutlet weak var levelRateTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    class func cellIdentifire() -> String{
        return "LavelWithRateInSubjectAndRate"
    }
    
    class func cellHeight() -> CGFloat{
        return 55
    }
    
    func configureCell(with title: String, index: Int){
        self.levelNameLbl.text = title
        self.levelRateTextField.tag = index
    }

}
