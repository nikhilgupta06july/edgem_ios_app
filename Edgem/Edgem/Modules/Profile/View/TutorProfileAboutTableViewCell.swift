//
//  TutorProfileAboutTableViewCell.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 16/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class TutorProfileAboutTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dashedView: UIView!
    
    @IBOutlet weak var dualLabelOne: UILabel!
    @IBOutlet weak var dualLabelTwo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        drawDottedLine(start: CGPoint(x: dashedView.bounds.minX, y: dashedView.bounds.minY), end: CGPoint(x: ScreenWidth, y: dashedView.bounds.minY), view: dashedView)
    }
    
    class func cellIdentifire()->String {
        return "TutorProfileAboutTableViewCell"
    }
  
    func configureCellWithTutorQualification(_ qualifications: [Qualification]){
        titleLabel.text = "Qualification"
        self.dualLabelOne.text = ""
        self.dualLabelTwo.text = ""
        if qualifications.count > 0{
            let qualificationTitle = qualifications.map { $0.getAttributedQualification() }
            let str = NSMutableAttributedString()
            for item in qualificationTitle{
                str.append(item)
            }
            self.contentLabel.attributedText = str
            self.dualLabelOne.text = ""
            self.dualLabelTwo.text = ""
        }else{
        self.contentLabel.text = "No Qualification\n"
        }
    }
    

    func configureCellWithTutorExperience(_ experiences: [Experience]){
        titleLabel.text = "Experience"
        self.dualLabelOne.text = ""
        self.dualLabelTwo.text = ""
        if experiences.count > 0{
            let experienceTitle = experiences.map { $0.getAttributedExperience() }
            let str = NSMutableAttributedString()
            for item in experienceTitle{
                str.append(item)
            }
            self.contentLabel.attributedText = str
            self.dualLabelOne.text = ""
            self.dualLabelTwo.text = ""
        }else{
        self.contentLabel.text = "No Experience\n"
        }
    }
    
    func configureCellWithTutorEducation(_ educations: [Education]){
        titleLabel.text = "Education"
        self.dualLabelOne.text = ""
        self.dualLabelTwo.text = ""
        if educations.count > 0{
            let educationTitle = educations.map { $0.getAttributedEducation() }
            let str = NSMutableAttributedString()
            for item in educationTitle{
                str.append(item)
            }
            self.contentLabel.attributedText = str
        }else{
            self.contentLabel.text = "No Education\n"
        }
    }
    
    func configureCellWithTutorsSubject(_ subjects: [Subject]){
        titleLabel.text = "Subject"
        self.contentLabel.text = ""
        if subjects.count > 0{
            let subjectTitle = subjects.map { $0.name ?? "" + ":" }
            self.dualLabelOne.text = subjectTitle.joined(separator: "\n") + "\n"
            
            
//            let subjectPrice = subjects.map { $0.subjectRate! + "/hour" }
            
            let subjectPrice = subjects.map { $0.lockRate == "1" ? $0.subjectRate! + "/hour (Locked)" : $0.subjectRate! + "/hour"}
            
            self.dualLabelTwo.text = subjectPrice.joined(separator: "\n") + "\n"
        }else{
            self.contentLabel.text = "No Subject\n"
        }
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView){
        let shapelayer = CAShapeLayer()
        shapelayer.strokeColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1).cgColor
        shapelayer.lineWidth = 1
        shapelayer.lineDashPattern = [7, 3]
        
        let path =  CGMutablePath()
        path.addLines(between: [p0, p1])
        shapelayer.path = path
        view.layer.addSublayer(shapelayer)
        
    }
    
}
