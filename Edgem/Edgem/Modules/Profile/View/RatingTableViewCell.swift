//
//  RatingTableViewCell.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 12/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import SwiftyStarRatingView

class RatingTableViewCell: UITableViewCell {

    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var ratingView: SwiftyStarRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    class func cellIdentifire()->String {
        return "RatingTableViewCell"
    }
    
    func configureCellWithRating(_ ratingData: Rating) {
        nameLabel.text = ratingData.ratedBy
        descLabel.text = ratingData.comment
        ratingView.value = ratingData.rating
       // timeLabel.text = ratingData.ratedAt
        
        // time span
        
        let date = ratingData.ratedAt
        if date != "" {
            if let dt1 = date.stringToDate(date: date, format: "yyyy-MM-dd HH:mm:ss"){
                timeLabel.text = dt1.timeAgoDisplay()
            }
        }
    }
}
