//
//  SubjectDropDownCellInProfile.swift
//  Edgem
//
//  Created by Namespace on 19/09/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class SubjectDropDownCellInProfile: UITableViewCell {
    
    // MARK: - Properties
    var sectionCount = 0
    var subjects: [Subject]?{
        didSet{
            guard let sub = subjects else{return}
            sectionCount = sub.count
            secondTableView.reloadData()
        }
    }
    
    var tempSubject: [Subject]?
    
    weak var tutorCompleteProfileViewController: TutorCompleteProfileViewController?
    
    // MARK: - @IBOutlets
    @IBOutlet weak var dashedView: UIView!
    @IBOutlet weak var secondTableView: UITableView!
    @IBOutlet weak var secondTableViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        drawDottedLines()
        initialSetUP()
        registerCustomCell()

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
          self.secondTableViewHeight.constant = self.secondTableView.contentSize.height
    }

    // Mark: - Private Functions
    
    fileprivate func registerCustomCell(){
        secondTableView.register(UINib(nibName: SubjectInDropDown.cellIdentifire(), bundle: nil), forCellReuseIdentifier: SubjectInDropDown.cellIdentifire())
        secondTableView.register(UINib(nibName: LevelInDropDown.cellIdentifire(), bundle: nil), forCellReuseIdentifier: LevelInDropDown.cellIdentifire())
    }
    
    fileprivate func drawDottedLines(){
        drawDottedLine(start: CGPoint(x: dashedView.bounds.minX, y: dashedView.bounds.minY), end: CGPoint(x: ScreenWidth, y: dashedView.bounds.minY), view: dashedView)
    }
    
    fileprivate func initialSetUP(){
        // table view dalegates/datasources
        secondTableView.delegate = self
        secondTableView.dataSource = self
    }
    
    class func cellIdentifire()->String{
        return "SubjectDropDownCellInProfile"
    }
    
    func configureCell(with subjects: [Subject]?){
        self.subjects = subjects
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView){
        let shapelayer = CAShapeLayer()
        shapelayer.strokeColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1).cgColor
        shapelayer.lineWidth = 1
        shapelayer.lineDashPattern = [7, 3]
        
        let path =  CGMutablePath()
        path.addLines(between: [p0, p1])
        shapelayer.path = path
        view.layer.addSublayer(shapelayer)
        
    }
    
}

extension SubjectDropDownCellInProfile: UITableViewDelegate, UITableViewDataSource{
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        self.secondTableView.reloadData()
        
        // if the table view is the last UI element, you might need to adjust the height
        let size = CGSize(width: targetSize.width,
                          height: secondTableView.frame.origin.y + secondTableView.contentSize.height)
        return size
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if  let subject = subjects?[section]{
            
            /// Since some subject's level price may be 0 so ve can not show those level. To discard those levels we use following logic
            var levels = [Level]()
           // return subject.isSelected ? subject.levels.count+1 : 1
            if subject.isSelected{
                for level in subject.levels{
                    if (level.price! as NSString).doubleValue > 0{
                        levels.append(level)
                    }
                }
                subject.levels = levels
                return subject.levels.count+1
            }else{
                return 1
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let subject = self.subjects?[indexPath.section] else{return UITableViewCell()}
        
        guard indexPath.row != 0 else{
            let cell = tableView.dequeueReusableCell(withIdentifier: SubjectInDropDown.cellIdentifire(), for: indexPath) as! SubjectInDropDown
            if let subs = self.subjects{
                cell.configureCell(With: subs[indexPath.section])
                
                                if subject.isSelected {
                                    UIView.animate(withDuration: 0.8) {
                                        cell.toggleBtn.transform     = CGAffineTransform(rotationAngle: CGFloat.pi)
                                        cell.rangeLabel.isHidden    = true
                                        cell.lockedStatus.isHidden  = true
                                    }
                                }else{
                                    UIView.animate(withDuration: 0.8) {
                                         cell.toggleBtn.transform     = .identity
                                         cell.rangeLabel.isHidden    = false
                                         cell.lockedStatus.isHidden  = false
                                    }
                                }
                
            }
            return cell
        }

        guard let cell = tableView.dequeueReusableCell(withIdentifier: LevelInDropDown.cellIdentifire(), for: indexPath) as? LevelInDropDown else{
            return UITableViewCell()
        }
        if let level = subject.levels[safe: indexPath.row-1]{
             cell.configureCell(With: level)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0{
            if  let subject = subjects?[indexPath.section]{
                
                subject.isSelected = !subject.isSelected

               // let cell = secondTableView.cellForRow(at: indexPath) as! SubjectInDropDown

                secondTableView.reloadSections([indexPath.section], with: .automatic)
                
                self.secondTableViewHeight.constant = self.secondTableView.contentSize.height
                
                self.setNeedsLayout()
                
//                if subject.isSelected == true{
//                    UIView.animate(withDuration: 0.5) {
//                        cell.toggleBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
//                    }
//                }else{
//                    UIView.animate(withDuration: 0.5) {
//                        cell.toggleBtn.transform = .identity
//                    }
//                }
//
//                secondTableView.reloadRows(at: [IndexPath(row: indexPath.row, section: indexPath.section)], with: .automatic)
               
                tutorCompleteProfileViewController?.updateCell()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
             return 35
        }
       return 30
    }
    
    
}
