//
//  ContactUsDescriptionTableViewCell.swift
//  Edgem
//
//  Created by Hipster on 26/02/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class ContactUsDescriptionTableViewCell: UITableViewCell {
    
    // MARK: -------Properties
    
     var placeholderLabel : UILabel!
    
    // MARK: ------- @IBOutlets
    
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var errorLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialSetUp()
    }
    
    class func cellIdentifire()->String{
        return "ContactUsDescriptionTableViewCell"
    }
    
    class func cellHeight()->CGFloat{
        return 250
    }
    
    func initialSetUp(){
        
        cellTitle.text = "Description".localized
        
        placeholderLabel = UILabel()
        placeholderLabel.text = "Type your message here".localized
        placeholderLabel.font = UIFont(name: QuicksandRegular, size: 15)
        placeholderLabel.sizeToFit()
        descriptionTextView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 16, y: (descriptionTextView.font?.pointSize)! / 2)
        placeholderLabel.textColor = theamBlackColor
        placeholderLabel.isHidden = !descriptionTextView.text.isEmpty
        
    }
    
    func configureCell(_ desc: String){
        descriptionTextView.text = desc
         placeholderLabel.isHidden = !desc.isEmpty
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        descriptionTextView.layer.cornerRadius = 4
        descriptionTextView.layer.borderWidth = 1
        descriptionTextView.layer.borderColor = theamBorderGrayColor.cgColor
        descriptionTextView.clipsToBounds = true
    }
    
    // MARK: Description
    func validateDescription(text: String) {
        if text.isEmptyString() {
            errorLabel.text = "Please write description".localized
        } else {
            errorLabel.text = ""
        }
    }

}
