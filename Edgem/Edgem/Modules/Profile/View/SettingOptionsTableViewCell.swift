//
//  SettingOptionsTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 24/04/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class SettingOptionsTableViewCell: UITableViewCell {
    
    var toggleStatus: ((UISwitch)->Void)?
    
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var `switch`: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        titleLabel.textColor = theamBlackColor
        titleLabel.font = UIFont(name: QuicksandMedium, size: 15)
        
        holderView.layer.shadowColor = UIColor.black.cgColor
        holderView.layer.shadowOpacity = 0.1
        holderView.layer.shadowOffset = CGSize(width: 0, height: 2)
        holderView.layer.shadowRadius = 1
        holderView.layer.masksToBounds = false
    }
    
    func configureCell(_ index: Int, _ title: String){
        titleLabel.text = title
        `switch`.tag = index
    }
    
    class func cellIdentifire() -> String {
        return "SettingOptionsTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 70.0
    }
    
    @IBAction func switchStatus(_ sender: UISwitch) {
        toggleStatus?(sender)
    }
    

}
