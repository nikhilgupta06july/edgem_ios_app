//
//  PaymentCardCollectionViewCell.swift
//  Edgem
//
//  Created by Namespace on 26/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class PaymentCardCollectionViewCell: UICollectionViewCell {
    
    // MARK: ----- @IBOutlets
    
    @IBOutlet weak var cardimage: UIImageView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cardHolderName: UILabel!
    @IBOutlet weak var cardExpiryYearLabel: UILabel!
    
    class func cellIdentifire() -> String{
        return "PaymentCardCollectionViewCell"
    }
    
    func configureCell(with card: StoreUserCard) {
      
        let last4 = card.cardNumber!.suffix(4)
        cardNumberLabel.text =  insertSpace(String(last4))
        cardHolderName.text = card.nameOnCard
        var mm = ""
        var yy = ""
        if let month = card.expiryMonth{
            mm  = month.count == 1 ? "0\(month)" : month
        }
        //MMTextField.text = cardDetail.expiryMonth
        if let yr = card.expiryYear{
            let year = Int(yr)! % 100
            yy = yr.count == 4 ? "\(year)" : yr
        }
        cardExpiryYearLabel.text = mm + "/" + yy
        
        // cardimage
        if let brand = card.brand{
            cardimage.image = UIImage(named: brand)
        }
    }
    
    func insertSpace(_ string: String) -> String {
    
        let s = string
        let cardLast4Digits = String(s.enumerated().map { $0 > 0 && $0 % 1 == 0 ? [" ", $1] : [$1]}.joined())
        return cardLast4Digits
        
    }
    
}
