//
//  SwitchButtonInSubjectAndRate.swift
//  Edgem
//
//  Created by Namespace on 10/09/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

protocol SwitchButtonInSubjectAndRateDelegate {
    func popTipTapped(senderFrame:CGRect,cell:SwitchButtonInSubjectAndRate,btn:UIButton)
}

class SwitchButtonInSubjectAndRate: UITableViewCell {
    
    var delegate:SwitchButtonInSubjectAndRateDelegate?
    var switchBtnStatus: ((Bool, Int)->())?
     var index: Int!
    
    @IBOutlet weak var `switch`: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    class func cellIdentifire() -> String{
        return "SwitchButtonInSubjectAndRate"
    }
    
    class func cellHeight() -> CGFloat{
        return 80
    }
    
    func configureCell(with lockRateStatus: String, _ index: Int){
        
        self.index = index
        if lockRateStatus == "1" {
            `switch`.isOn = true
        }else{
            `switch`.isOn = false
        }
    }
    
    @IBAction func didtapInfoBtn(_ sender: UIButton) {
        delegate?.popTipTapped(senderFrame: sender.frame, cell: self, btn: sender)
    }
    
    @IBAction func didTapSwitch(_ sender: UISwitch) {
        switchBtnStatus?(sender.isOn, self.index)
    }

}
