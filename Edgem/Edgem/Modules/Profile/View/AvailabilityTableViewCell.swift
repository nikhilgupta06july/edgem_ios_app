//
//  AvailabilityTableViewCell.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 14/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit
import SpreadsheetView


class AvailabilityTableViewCell: UITableViewCell {

    var headers = ["".localized.uppercased(), "MON".localized, "TUE".localized, "WED".localized, "THU".localized, "FRI".localized, "SAT".localized, "SUN".localized]
    
    var data = [[String]]()
    var schedules1: DayAvailability?
    
    @IBOutlet weak var calendarSpreadSheet: SpreadsheetView!
    @IBOutlet weak var calenderViewHeight: NSLayoutConstraint!
    
    class func cellIdentifier() -> String {
        return "AvailabilityTableViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        calendarSpreadSheet.delegate = self
        calendarSpreadSheet.dataSource = self
        //calendarSpreadSheet.isScrollEnabled = false
        calendarSpreadSheet.isDirectionalLockEnabled = true
        calendarSpreadSheet.register(HeaderSpreadsheetCell.self, forCellWithReuseIdentifier: String(describing: HeaderSpreadsheetCell.self))
        calendarSpreadSheet.register(TextCell.self, forCellWithReuseIdentifier: String(describing: TextCell.self))
        calendarSpreadSheet.register(CheckBoxCell.self, forCellWithReuseIdentifier: String(describing: CheckBoxCell.self))
    }

//    func reloadCalendarViewData(_ calendarData:[[String]]){
//        data = calendarData
//        calenderViewHeight.constant = CGFloat(45 + 35*data.count)
//        calendarSpreadSheet.reloadData()
//    }
    
    func reloadCalendarViewData(_ calendarData:DayAvailability){
        data = getDataArrayFromSchedules(calendarData)
        calenderViewHeight.constant = CGFloat(45 + 35*data.count)
        calendarSpreadSheet.reloadData()
    }
    
    func getDataArrayFromSchedules(_ schedules1:DayAvailability?) -> [[String]] {
        var completeData = [[String]]()
        let weeksArray = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
        for i in 9...20 {
            var hourSchedule = [String]()
            if i > 9 {
                hourSchedule = ["\(i):00"]
            } else {
                hourSchedule = ["0\(i):00"]
            }
            for day in weeksArray {
                
                let daysSchedule = day == weeksArray[0] ? schedules1?.mon :
                    day == weeksArray[1] ? schedules1?.tue :
                    day == weeksArray[2] ? schedules1?.wed :
                    day == weeksArray[3] ? schedules1?.thus :
                    day == weeksArray[4] ? schedules1?.fri :
                    day == weeksArray[5] ? schedules1?.sat :
                    schedules1?.sun
                
                let filteredSchedules = daysSchedule?.filter({ (schedule) -> Bool in
                    return "\(schedule.time)" == String(i)
                })
                
                if let schedulesArray = filteredSchedules, schedulesArray.count > 0 {
                    for scheduleArr in schedulesArray{
                        hourSchedule.append("\(scheduleArr.type)")
                    }
                }else {
                    hourSchedule.append("")
                }
            }
            completeData.append(hourSchedule)
        }
        return completeData
    }

}

extension AvailabilityTableViewCell: SpreadsheetViewDataSource, SpreadsheetViewDelegate {
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return self.headers.count
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return data.count+1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return 50
        } else {
            return (ScreenWidth - 90)/7
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if case 0 = row {
            return 45
        } else {
            return 35
        }
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        if case 0 = indexPath.row {
            let cell = calendarSpreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: HeaderSpreadsheetCell.self), for: indexPath) as! HeaderSpreadsheetCell
            cell.label.text = headers[indexPath.column]
            cell.gridlines.left = .none
            cell.gridlines.right = .none
            cell.gridlines.top = .none
            cell.gridlines.left = .none
            cell.gridlines.bottom = .none
            return cell
        } else {
            let dataString = data[indexPath.row - 1][indexPath.column]
            
            if indexPath.column == 0 {
                let cell = calendarSpreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: TextCell.self), for: indexPath) as! TextCell
                cell.label.text = dataString
                cell.label.backgroundColor = UIColor.clear
                cell.gridlines.left = .none
                cell.gridlines.top = .none
                cell.gridlines.right = .none
                cell.gridlines.bottom = .none
                
                return cell
            } else {
                let cell = calendarSpreadSheet.dequeueReusableCell(withReuseIdentifier: String(describing: CheckBoxCell.self), for: indexPath) as! CheckBoxCell
                cell.imageView.image = UIImage(named: dataString == "0" ? "active_avalibility" : (dataString == "1" ? "largeBooking" : "inactive_avalibility"))
                cell.gridlines.left = .none
                cell.gridlines.top = .none
                cell.gridlines.right = .none
                cell.gridlines.bottom = .none
                
                return cell
            }
        }
    }
    
}
