//
//  SubjectInDropDown.swift
//  Edgem
//
//  Created by Namespace on 19/09/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class SubjectInDropDown: UITableViewCell {
    
    var rotate = 0
    
    @IBOutlet weak var toggleBtn: UIButton!
    @IBOutlet weak var rangeLabel: UILabel!
    @IBOutlet weak var subjectTitleLabel: UILabel!
    @IBOutlet weak var lockedStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rangeLabel.font = UIFont(name: SourceSansProRegular, size: 15)
    }

    class func cellIdentifire()->String{
        return "SubjectInDropDown"
    }
    
    func configureCell(With subject: Subject){
        subjectTitleLabel.text  = subject.name
        rangeLabel.text           = GetSubjectsRange.get(from: subject.levels)//getRangeOfRates(subject)
        
        if  subject.lockRate == "1" {
            lockedStatus.text = "(Locked)"
        }else{
            lockedStatus.text = ""
        }
        
    }
    
    func getRangeOfRates(_ subject: Subject) -> String{
        
        var upperRate = ""
        var lowerRate  = ""
        
        let rates_in_double = subject.levels.map{ _level -> Double  in
        
            if let _price = _level.price{
                return (_price as NSString).doubleValue
            }
            return 0.0
            }.filter{ $0 != 0.0}
    
        
        let sorted_rates = rates_in_double.sorted { (v1, v2) -> Bool in
            v1 > v2
        }
        
        print(sorted_rates)
        
        if sorted_rates.count > 1{
            upperRate = sorted_rates[0].isWholeNumber()
            lowerRate  =  sorted_rates[sorted_rates.count-1].isWholeNumber()
            
            if upperRate == lowerRate{
                return "$\(upperRate)/hr"
            }else{
                 return "$\(lowerRate)-$\(upperRate)/hr"
            }
        }else if sorted_rates.count == 1{
            upperRate = sorted_rates[0].isWholeNumber()
            return "$\(upperRate)/hr"
        }
        return ""
        
    }
    
    class func cellHeight()->CGFloat{
        return 44
    }
    
}
