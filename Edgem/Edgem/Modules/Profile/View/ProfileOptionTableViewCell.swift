//
//  ProfileOptionTableViewCell.swift
//  Edgem
//
//  Created by Kalpana_Hipster on 19/01/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class ProfileOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var disclosureIconImageView: UIImageView!
    
    class func cellIdentifier() -> String {
        return "ProfileOptionTableViewCell"
    }
    
    class func cellHeight() -> CGFloat {
        return 74.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        holderView.layer.shadowColor = UIColor.black.cgColor
        holderView.layer.shadowOpacity = 0.1
        holderView.layer.shadowOffset = CGSize(width: 0, height: 2)
        holderView.layer.shadowRadius = 1
        holderView.layer.masksToBounds = false
    }
    
    func configureCellWithTitle(_ title: String, andImage image: String) {
        titleLabel.text = title
        iconImageView.image = UIImage(named: image)
    }
    
}
