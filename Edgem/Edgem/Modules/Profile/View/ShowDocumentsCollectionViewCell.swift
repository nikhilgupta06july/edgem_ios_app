//
//  ShowDocumentsCollectionViewCell.swift
//  Edgem
//
//  Created by Namespace on 16/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class ShowDocumentsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var documentImage: UIImageView!
    @IBOutlet weak var imageHolderView: UIView!
    
    class func cellIdentifire() -> String{
        return "ShowDocumentsCollectionViewCell"
    }
    
    func configureCell(with image: UIImage){
        self.documentImage.image = image
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageHolderView.layer.cornerRadius = 4
        imageHolderView.layer.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5).cgColor
        imageHolderView.layer.shadowOpacity = 0.1
        imageHolderView.layer.shadowOffset = .zero
        imageHolderView.layer.shadowRadius = 4
        imageHolderView.layer.masksToBounds = false
        
    }
    
    
    
}
