//
//  ShowDocumentsTableViewCell.swift
//  Edgem
//
//  Created by Namespace on 16/03/19.
//  Copyright © 2019 Hipster. All rights reserved.
//

import UIKit

class DocumentsTableViewCell: UITableViewCell {
    
    // MARK: ------- Properties
    var documentImage: [UIImage] = Array(){
        didSet{
            //collectionView.reloadData()
        }
    }
    
    var onDidTapDocumentsCell: ((UIImage)->Void)?
    
    @IBOutlet weak var collectionView: UICollectionView!{
        
        didSet{
            collectionView.delegate = self
            collectionView.dataSource = self
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func configureCell(with documentImage: [UIImage]){
        //self.documentImage.append(documentImage)
        self.documentImage = documentImage.map {return $0}
        collectionView.reloadData()
       // collectionView.scrollToItem(at: IndexPath(item: documentImage.count-1, section: 0), at: .left, animated: true)
        
    }
    
    class func cellIdentifire() -> String{
        return "DocumentsTableViewCell"
    }
    
}

extension DocumentsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.documentImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DocumentsCollectionViewCell.cellIdentifire(), for: indexPath) as! DocumentsCollectionViewCell
        cell.configureCell(with: self.documentImage[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewFlowLayOut = collectionViewLayout as! UICollectionViewFlowLayout
        collectionViewFlowLayOut.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        
        var width: CGFloat = 150
        var height: CGFloat = 180
        
        let maxImageWidthPort: CGFloat = 150
        let maxImageHeightPort: CGFloat = 180
        
        let maxImageWidthLandscape: CGFloat = 180
        let maxImageHeightLandscape: CGFloat = 120
                
        let image = documentImage[indexPath.row]
        let imageHeight = image.size.height
        let imageWidth = image.size.width
        
        if imageWidth > imageHeight{
            //landscape
            width = imageWidth > maxImageWidthLandscape ? maxImageWidthLandscape : imageWidth
            height = imageHeight > maxImageHeightLandscape ? maxImageHeightLandscape : imageHeight
        }else if imageWidth < imageHeight{
            // portrait
            width = imageWidth > maxImageWidthPort ? maxImageWidthPort : imageWidth
            height = imageHeight > maxImageHeightPort ? maxImageHeightPort : imageHeight
        }else{
            //square
        }

        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        onDidTapDocumentsCell?(self.documentImage[indexPath.item])
    }
    
    
}

