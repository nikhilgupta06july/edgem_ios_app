//
//  BaseTabBarViewController.swift
//  Edgem
//
//  Created by Hipster on 27/12/18.
//  Copyright © 2018 Hipster. All rights reserved.
//

import UIKit

class BaseTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.delegate = self
        
        self.tabBar.barTintColor = UIColor.white
        self.tabBar.tintColor = theamBlackColor//UIColor.init(red: 23, green: 23, blue: 23, alpha: 1.0)
        self.hidesBottomBarWhenPushed = true
        
        let appearance = UITabBarItem.appearance()
        appearance.setTitleTextAttributes([.font: tabBarTitleFont] , for: .normal)
        
        setUPTabBar()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //print("Screen Type : \(UIDevice.current.screenType)")
        var tabFrame: CGRect = self.tabBar.frame
        let height:CGFloat = UIDevice.current.screenType.rawValue.contains("X") ? 95.0 : 63
        tabFrame.size.height = height
        
        tabBar.layer.borderWidth = 2
        tabBar.layer.borderColor = UIColor(red: 119, green: 119, blue: 119, alpha: 1).cgColor
        tabBar.layer.shadowOffset = CGSize(width: 0, height: -4)
        tabBar.layer.shadowRadius = 4
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOpacity = 0.09
        
    }
    
    private func setUPTabBar(){
        let homeViewController = DashboardViewController.instantiateFromAppStoryboard(appStoryboard: .Dashboard)
        homeViewController.tabBarItem = UITabBarItem(title: "Home".localized, image: UIImage(named: "homeInActive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "homeActive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        let nav1 = BaseNavigationViewController(rootViewController: homeViewController)
        
        let searchViewController = SearchViewController.instantiateFromAppStoryboard(appStoryboard: .Search)
        searchViewController.tabBarItem = UITabBarItem(title: "Search".localized, image: UIImage(named: "searchInActive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "searchActive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        let nav2 = BaseNavigationViewController(rootViewController: searchViewController)
        
        let chatMemberListingViewController = ChatMemberListingViewController.instantiateFromAppStoryboard(appStoryboard: .Chat)
        chatMemberListingViewController.tabBarItem = UITabBarItem(title: "Chat".localized, image: UIImage(named: "chatInActive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "chatActive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        let nav3 = BaseNavigationViewController(rootViewController: chatMemberListingViewController)
        
        let activityViewController = ActivityViewController.instantiateFromAppStoryboard(appStoryboard: .Activity)
        activityViewController.tabBarItem = UITabBarItem(title: "Activity".localized, image: UIImage(named: "activityInactive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "activityActive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        let nav4 = BaseNavigationViewController(rootViewController: activityViewController)
        
        let profileViewController = ProfileViewController.instantiateFromAppStoryboard(appStoryboard: .Profile)
        profileViewController.tabBarItem = UITabBarItem(title: "Profile".localized, image: UIImage(named: "profileInActive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "profileActive")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        let nav5 = BaseNavigationViewController(rootViewController: profileViewController)
        
        viewControllers = [nav1, nav2, nav3, nav4, nav5]
        
        for item in self.tabBar.items!{
            item.imageInsets = UIEdgeInsets(top: -2, left: 0, bottom: 2, right: 0)
            item.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -1)
        }
    }
}


extension BaseTabBarViewController: UITabBarControllerDelegate{
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        switch viewController.children[0]{
        case is DashboardViewController               : AppVariables.chatListUserDetail = nil
        case is SearchViewController                      : AppVariables.chatListUserDetail = nil
        case is ChatMemberListingViewController :  tabBarController.tabBar.items![2].badgeValue = nil
                                                                               break
        case is ActivityViewController                      :
                                                                               AppVariables.chatListUserDetail = nil
                                                                               tabBarController.tabBar.items![3].badgeValue = nil
        case is ProfileViewController                        : AppVariables.chatListUserDetail = nil
        default:
            break
        }
    }
}
